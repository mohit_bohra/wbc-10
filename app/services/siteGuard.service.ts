import {Injectable} from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Http, Headers } from '@angular/http';
import { Constant } from '../components/constants/constant';
import { Observable } from 'rxjs';
import { ReturnStatement } from '../../node_modules/@angular/compiler/src/output/output_ast';


@Injectable()
export class SiteGuardService implements CanActivate {

  objConstant = new Constant();
  configManagementurl: string;
  configManagementType: string;
  configManagementapiKey: string;
  configManagementauthorization: string;

  constructor(private  router: Router, private _http: Http) {
    //config
    this.configManagementurl = this.objConstant.configManagementurl;
    this.configManagementType = this.objConstant.configManagementType;
    this.configManagementapiKey = this.objConstant.configManagementapiKey;
    this.configManagementauthorization = this.objConstant.configManagementauthorization;

  }
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
     //console.log(route.url[0].path);
     console.log(route);
     var orgName = (route.url[1] === undefined) ? '1' : route.url[1].path;
     var val = route.url[0].path;
    
     if(val=='orders'){
       val = 'order';
     }
     else if(val = "customerselection"){         
         val = 'all';
     }
     console.log('Parameters : ' + orgName + " , " + val);
     if(this.MaintainanceStatus(orgName, val)){
        sessionStorage.setItem('site', "true");
         return true;
     } else {
      //this.router.navigate(['/error/sitedown']);      
      sessionStorage.setItem('site', "false");
       return true;
     }
     //console.log('status1 : ' + stat);
     //return false;
  }
  MaintainanceStatus(orgName: string, val:string){   
    return this.siteMaintance().subscribe(userdata =>{
          console.log(userdata);

        if (userdata.maintenanceDetails[0].moduleName == 'all') {

            if (userdata.maintenanceDetails[0].status.toLowerCase() == 'no') {
                //this.router.navigate(['/error/sitedown']);
                return false;
                // this.siteMaintanceFlg = true;
                // this.siteMSg = userdata.maintenanceDetails[0].message + ' ' + userdata.maintenanceDetails[0].end;
            } else {
                // this.siteMaintanceFlg = false;
                this.siteMaintance(orgName, val).subscribe(userData =>{
                    console.log(userData);
                    console.log(userData.maintenanceDetails[0].message);
                    console.log(userData.maintenanceDetails[0].start);
                    console.log(userData.maintenanceDetails[0].status);
                    if (userData.maintenanceDetails[0].moduleName == val) {
                        if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                            //this.router.navigate(['/error404']);
                            return false;
                            // this.siteMaintanceFlg = true;
                            // this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                        } else {
                            return true;
                            //this.siteMaintanceFlg = false;
                        }
                    } else {
                        return true;
                        // this.siteMaintanceFlg = false;
                    }   
                    
                }, error => {
                        console.log(error);
                        if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                           // this.router.navigate(['/error404']);
                            return false;
                            //this.siteMaintanceFlg = true;
                            //this.siteMSg = "Site is under maintenance";
                        } 
                        else {
                            return true;
                           // this.siteMaintanceFlg = false;
                        }
                        
                    });
            }
        } else {
            return true;            
        }

     });
     
     
    //console.log('status :' + data);
    //return false;
};
  
  siteMaintance(orgName: any = '1', val: any = 'all') {
 
  let addedHeader = new Headers();
        addedHeader.append('Authorization', this.configManagementauthorization);

    let url_call = atob(this.configManagementurl) + 'customers/' + orgName + '/config?filtername=getPortalMaintenanceStatus&filtervalue=' + val + '&apikey=' + this.configManagementapiKey;

        // let url_call ='app/js/sitemaintance.json'
              return this._http.get(url_call, { headers: addedHeader }).map(
                  res => res.json()
                  
              )
              .catch((error) => Observable.throw(
                (error.json())
              ));

   
  }
}