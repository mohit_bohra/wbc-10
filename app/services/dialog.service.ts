import { Injectable, Component } from "@angular/core";
import { Router } from "@angular/router";

@Injectable()
export class DialogService {

  constructor(public router: Router){}

  confirm(message?: string) {
    const confirmation = window.confirm(message);

      if(sessionStorage.getItem('unsavedGroup')=='true' && confirmation == true){
        window.location.reload(true);
        sessionStorage.removeItem('unsavedGroup');
      }else if(sessionStorage.getItem('unsavedEditProduct')=='true' && confirmation == true){
        sessionStorage.removeItem('unsavedEditProduct');
        window.top.close();
      }else if(sessionStorage.getItem('orders')=='true' && sessionStorage.getItem('unsavedChanges')=='true' && confirmation == true){
        sessionStorage.removeItem('unsavedChanges');
        sessionStorage.removeItem('orders');
        let customerName = sessionStorage.getItem('customername');
        this.router.navigate(['orders',customerName])
      }else if(sessionStorage.getItem('orders')=='true' && sessionStorage.getItem('unsavedChanges')=='true' && confirmation == false){
        let updateOrgType = sessionStorage.getItem('prevOrgSelected');
        sessionStorage.setItem('orgType',updateOrgType);
        sessionStorage.removeItem('orders');
        sessionStorage.removeItem('customername');
        sessionStorage.setItem('changeCustomer','false');
      }else if(sessionStorage.getItem('unsavedChanges')=='true' && sessionStorage.getItem('logout')=='true' && confirmation == true){
        sessionStorage.removeItem('unsavedChanges');
        sessionStorage.clear();
        this.router.navigate(['/']);
      }else if(sessionStorage.getItem('unsavedChanges')=='true' && sessionStorage.getItem('logout')=='true' && confirmation == false){
        sessionStorage.removeItem('logout');
      }else{
        return confirmation;
      }
  }
} 