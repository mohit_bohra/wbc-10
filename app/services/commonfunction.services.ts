import { Injectable } from '@angular/core';

declare let saveAs: any;
@Injectable()
export class commonfunction {
    errorMsgList: any = {};
    errorCount: number = 0;
    isInvalid: any;

    constructor() {
    }

    // Return formated date.
    /*
    format
        1. DD/mm/yyyy
        2. dd/mm/yyyy h:i
        3. dd/mm/yyyy h:i:s
        4. dd/mm/yyyy h:i:s:ms
        5. yyyy/mm/dd
        11. h:i:s
    */
    dateFormator(dateStr: string, format: number, separator: string = "/") {
        var df: any;
        if (dateStr != undefined && dateStr != null && dateStr.length > 0) {
            var d = new Date(dateStr);
            var sec = d.getUTCSeconds() < 10 ? "0" + d.getUTCSeconds() : d.getUTCSeconds();
            var ms = d.getUTCMilliseconds() < 10 ? "0" + d.getUTCMilliseconds() : d.getUTCMilliseconds();

            switch (format) {
                case 1:
                    df = this.formatDate(dateStr, separator);
                    break;
                case 2:
                    df = this.formatDate(dateStr) + " " + this.formatTime(dateStr);
                    break;
                case 9:
                    df = this.formatDate(dateStr) + " " + this.formatTimeforOrderTracker(dateStr);
                    break;
                case 8:
                    df = this.formatDate(dateStr) + " " + this.formatTimeforOrder(dateStr);
                    break;
                case 3:
                    df = this.formatDate(dateStr) + " " + this.formatTime(dateStr) + ":" + sec;
                    break;
                case 4:
                    df = this.formatDate(dateStr) + " " + this.formatTime(dateStr) + ":" + sec + ":" + ms;
                    break;
                case 5:
                    var yyyy = d.getFullYear();
                    var mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1); // getMonth() is zero-based
                    var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
                    df = yyyy + "/" + mm + "/" + dd;
                    break;
                case 6:
                    var d = new Date(dateStr);
                    var yyyy = d.getFullYear();
                    var mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1); // getMonth() is zero-based
                    var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
                    // return dd + "/" + mm + "/" + yyyy;
                    return yyyy + '-' + mm + '-' + dd;
                case 7:
                    var d = new Date(dateStr);
                    var yyyy = d.getFullYear();
                    var mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1); // getMonth() is zero-based
                    var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
                    // return dd + "/" + mm + "/" + yyyy;
                    return yyyy + '-' + mm + '-' + dd + " " + this.formatTime(dateStr) + ":" + sec + ":" + ms;
                case 10:
                    var d = new Date(dateStr);
                    var yyyy = d.getFullYear();
                    var mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1); // getMonth() is zero-based
                    var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
                    // return dd + "/" + mm + "/" + yyyy;
                    return dd + '/' + mm + '/' + yyyy;
                case 12:
                    
                    var dateSP = dateStr.split(" ");
                    var datefull = dateSP[0].split("/");
                    var timefull = dateSP[1].split(":");

                    var cyy = (datefull[2]);
                    var cmm = (datefull[1]);
                    var cdd = (datefull[0]);

                    var chh = (timefull[0]);
                    var cmin = (timefull[1]);
                    var css = ((timefull[2] !== undefined) ? timefull[2] : '00');
                    
                    
                   // var checkDate = new Date(cyy,(cmm-1),cdd,chh,cmin,css);
                    /*var checkDate = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
                                    date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()));*/
                    
                    //debugger;
                    var momenttm = require('moment-timezone');                   
                    

                    //var checkDate: string = cdd + "/" + cmm + "/" + cyy + " " + chh + ":" + cmin + ":" + css;
                    var checkDate: string = cyy + "-" + cmm + "-" + cdd + "T" + chh + ":" + cmin + ":" + css +'Z';
                    
                    //var sdt = momenttm.tz(checkDate,"Europe/London").format('DD/MM/YYYY HH:mm:ss');

                    var sdt = this.formatDate(checkDate);
                    var st = this.formatTimeforOrder(checkDate);

                    return sdt+ " " + st;
                case 13:
                   // debugger;
                    var dateSP = dateStr.split(" ");
                    var nDate = dateSP[0]+'T'+dateSP[1]+'Z';

                    

                    var sdt = this.formatDate(nDate);

                    //var st = this.formatTimeforOrder(nDate);

                    var momenttm = require('moment-timezone');

                    var d1 = momenttm.tz(nDate,"Europe/London").format('HH:mm:ss');

                    return sdt+ " " + d1;

            }
            return df;
        } else {
            return '-';
        }
    }

    getDateString(isoDateStr: string, separator: string = "/") {
        console.log(isoDateStr);
        let arr = isoDateStr.split("T");
        let dateArr = arr[0].split("-");
        let timeArr = arr[1].split(":");

        // var d = new Date(isoDateStr);
        // var yyyy = d.getUTCFullYear();
        // var mm = d.getUTCMonth() < 9 ? "0" + (d.getUTCMonth() + 1) : (d.getUTCMonth() + 1); // getMonth() is zero-based
        // var dd = d.getUTCDate() < 10 ? "0" + d.getUTCDate() : d.getUTCDate();
        // var hh = d.getUTCHours() < 10 ? "0" + d.getUTCHours() : d.getUTCHours();
        // var min = d.getUTCMinutes() < 10 ? "0" + d.getUTCMinutes() : d.getUTCMinutes();
        // var ss = d.getUTCSeconds() < 10 ? "0" + d.getUTCSeconds() : d.getUTCSeconds();
        return dateArr[2] + separator + dateArr[1] + separator + dateArr[0] + " " + timeArr[0] + ":" + timeArr[1] + ":00";
    }

    // Return formated date
    formatDate(createdAtDate: string, separator: string = "/") {
        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
            if (createdAtDate.indexOf('/') >= 0) {
                return createdAtDate;
            } else {
                var d = new Date(createdAtDate);
                var yyyy = d.getUTCFullYear();
                var mm = d.getUTCMonth() < 9 ? "0" + (d.getUTCMonth() + 1) : (d.getUTCMonth() + 1); // getMonth() is zero-based
                var dd = d.getUTCDate() < 10 ? "0" + d.getUTCDate() : d.getUTCDate();
                return dd + separator + mm + separator + yyyy;
            }
        } else {
            return '-';
        }

    }

    formatTimeforOrder(createdAtDate: string) {

        //debugger
    
        var momenttm = require('moment-timezone');

        /*var odt = momenttm.tz('2019-03-28T14:36:21Z',"Europe/London").format();
        var sdt = momenttm.tz('2019-03-31T14:36:21Z',"Europe/London").format();
        var tdt = momenttm.tz('2019-04-01T14:36:21Z',"Europe/London").format();
        
        var od = new Date(odt);
        var sd = new Date(sdt);
        var td = new Date(tdt);

        var oh = od.getUTCHours();
        var sh = sd.getUTCHours();
        var th = td.getUTCHours();

        var odt = momenttm.tz('2019-03-28T14:36:21Z',"Europe/London").format('HH:mm:ss');
        var sdt = momenttm.tz('2019-03-31T14:36:21Z',"Europe/London").format('hh:mm:ss');
        var tdt = momenttm.tz('2019-04-01T14:36:21Z',"Europe/London").format('hh:mm:ss');

        var opieces = odt.split(':');
        var spieces = sdt.split(':');
        var tpieces = tdt.split(':');

        var oh = opieces[0];
        var sh = spieces[0];
        var th = tpieces[0];*/


        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
           /* var d = new Date(createdAtDate);
            var hh = (d.getUTCHours() + 1) < 10 ? "0" + (d.getUTCHours() + 1) : (d.getUTCHours() + 1);
            var min = d.getUTCMinutes() < 10 ? "0" + d.getUTCMinutes() : d.getUTCMinutes();*/

            var d = momenttm.tz(createdAtDate,"Europe/London").format('HH:mm:ss');

            var opieces = d.split(':');

            var hh = opieces[0];
            var min = opieces[1];
            var sec = opieces[2];

            return hh + ":" + min + ":" + sec;
        } else {
            return '-';
        }
    }

    // Return formated time
    formatTime(createdAtDate: string) {
       
        var momenttm = require('moment-timezone');

        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
            /*var d = new Date(createdAtDate);
            var hh = d.getUTCHours() < 10 ? "0" + d.getUTCHours() : d.getUTCHours();
            var min = d.getUTCMinutes() < 10 ? "0" + d.getUTCMinutes() : d.getUTCMinutes();*/

            var d = momenttm.tz(createdAtDate,"Europe/London").format('HH:mm:ss');

            var opieces = d.split(':');

            var hh = opieces[0];
            var min = opieces[1];

            return hh + ":" + min;
        } else {
            return '-';
        }
    }

    formatTimeforOrderTracker(createdAtDate: string) {

        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
            var d = new Date(createdAtDate);
            var hh = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();

            var min = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
            return hh + ":" + min;
        } else {
            return '-';
        }
    }
    // return today date
    todayDateStrFunction() {
        var today = new Date();
        var mmm = today.getUTCMonth() < 9 ? "0" + (today.getUTCMonth() + 1) : (today.getUTCMonth() + 1); // getMonth() is zero-based
        var hrs = today.getUTCHours() + 1 < 10 ? "0" + today.getUTCHours() + 1 : today.getUTCHours() + 1; // getHours() is zero-based
        var min = today.getUTCMinutes() < 10 ? "0" + today.getUTCMinutes() : today.getUTCMinutes(); // getMinutes() is zero-based
        var dt = today.getUTCDate() < 10 ? "0" + today.getUTCDate() : today.getUTCDate(); // getDate() is zero-based
        var sec = today.getUTCSeconds() < 10 ? "0" + today.getUTCSeconds() : today.getUTCSeconds(); // getSeconds() is zero-based
        return today.getUTCFullYear() + '-' + mmm + '-' + dt + 'T' + hrs + ":" + min + ":" + sec + 'Z';
    }

    // return isodate
    isodate(dateStr: any) {
        if (dateStr.length < 20) {
            var formatd = dateStr.split(" ");
            console.log(dateStr);
            console.log(formatd);
            if (typeof formatd[1] === 'undefined') {
                formatd[1] = "00:00";
            }
            var formatedDateArr = formatd[0].split("/");
            console.log(formatedDateArr);
            return formatedDateArr[2] + '-' + formatedDateArr[1] + '-' + formatedDateArr[0] + 'T' + formatd[1] + ':00Z';
        } else {
            var dd = new Date(dateStr).toString();
            var formatdd = dd.split(" ");
            // Fri Jul 21 2017 08:54:00 GMT+0100 (GMT Daylight Time)
            var months = [" ", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            // var mm = months.indexOf(formatdd[1]);
            var mmm = months.indexOf(formatdd[1]) < 10 ? "0" + (months.indexOf(formatdd[1])) : (months.indexOf(formatdd[1])); // getMonth() is zero-based

            return formatdd[3] + '-' + mmm + '-' + formatdd[2] + 'T' + formatdd[4] + 'Z';
        }
    }

    getISOfromDDMMYYYYFromDateOnly(dt: any) {
        var formatdd = dt.split("/");
        return formatdd[2] + '-' + formatdd[1] + '-' + formatdd[0] + 'T00:00:00Z';
    }

    getISOfromDDMMYYYY(dt: any) {
        var dt1 = dt.split(" ");
        var formatdd = dt[0].split("/");
        if (dt[1] != undefined) {
            return formatdd[2] + '-' + formatdd[1] + '-' + formatdd[0] + 'T' + dt[1] + 'Z';
        } else {
            return formatdd[2] + '-' + formatdd[1] + '-' + formatdd[0] + 'T00:00:00Z';
        }

    }

    /*  function name - getActivateCheckbox
        param details:
            index - line number
            value - indicates accept and reject radio button identifier
            eleName - tab name  
        Created date - 
    */
    getActivateCheckbox(index: any, value: any, eleName: string) {
        var userTestStatus: Array<{ id: number, name: string }> = Array(
            { "id": 0, "name": "Main" },
            { "id": 1, "name": "Additional" },
            { "id": 2, "name": "Warehouse" },
            { "id": 3, "name": "Basic" },
            { "id": 4, "name": "Nutritional" },
            { "id": 5, "name": "Safety" },
            { "id": 6, "name": "Battery" },
            { "id": 7, "name": "Dimensions" },
            { "id": 8, "name": "Financials" },
            { "id": 9, "name": "Identifiers" },
            { "id": 10, "name": "Descriptional" },
            { "id": 11, "name": "BWS" },
            { "id": 12, "name": "Summary" },
            { "id": 13, "name": "RRPnett" },
            { "id": 14, "name": "TotalCost" },
            { "id": 15, "name": "MUC" },
            { "id": 16, "name": "StandardMarkup" },
            { "id": 17, "name": "ProductPricing" },
            { "id": 18, "name": "ProductInfo" },
            { "id": 19, "name": "CostInfo" },
            { "id": 20, "name": "TotalCostMargin" },
            { "id": 21, "name": "NonPromotionalWSP" },
            { "id": 22, "name": "PromotionalWSP" },
            { "id": 23, "name": "Claim" }
        );
        var eleId;
        for (var i = 0; i < userTestStatus.length; i++) {
            if (userTestStatus[i].name != eleName) {
                console.log(eleName);
                eleId = value + "CheckboxFor" + userTestStatus[i].name + "Input" + index;
                var element = <HTMLInputElement>document.getElementById(eleId);
                if (typeof (element) != 'undefined' && element != null) {
                    if ((<HTMLInputElement>document.getElementById(eleId)).checked == false) {
                        (<HTMLInputElement>document.getElementById(eleId)).checked = true;
                        console.log('set');
                    } else {
                        (<HTMLInputElement>document.getElementById(eleId)).checked = false;
                        console.log('unset');
                    }
                }
            }
        }
        //alert(id);            
    }

    // export data
    exportData(exportable: any, filename: any) {
        var blob = new Blob([document.getElementById(exportable).innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, filename + ".xls");
    }

    getUserSession() {
        var today = new Date();
        console.log("Today " + today);
        var mmm = today.getUTCMonth() < 9 ? "0" + (today.getUTCMonth() + 1) : (today.getUTCMonth() + 1); // getMonth() is zero-based
        var hrs = today.getUTCHours() < 9 ? "0" + (today.getUTCHours() + 1) : (today.getUTCHours() + 1); // getHours() is zero-based
        var min = today.getUTCMinutes() < 9 ? "0" + (today.getUTCMinutes() + 1) : today.getUTCMinutes(); // getMinutes() is zero-based
        var dt = today.getUTCDate() < 9 ? "0" + today.getUTCDate() : today.getUTCDate(); // getDate() is zero-based
        var sec = today.getUTCSeconds() < 9 ? "0" + today.getUTCSeconds() : today.getUTCSeconds(); // getSeconds() is zero-based
        //return today.getUTCFullYear() + '-' + mmm + '-' + dt + 'T' + hrs + ":" + min + ":" + sec + 'Z';
        var ms = today.getUTCMilliseconds() < 9 ? "0" + today.getUTCMilliseconds() : today.getUTCMilliseconds();
        return today.getUTCFullYear() + '-' + mmm + '-' + dt + " " + hrs + ":" + min + ":" + sec + ":" + ms;
    }

    //     getActivateCheckBoxForGreenOnly(index: any, value: any, eleName: string){
    //         var userTestStatus: Array<{ id: number, name: string }> = Array(
    //             {"id":20,"name":"ProductPricing"}
    //         );

    //         var eleId;
    //         for (var i = 0; i < userTestStatus.length; i++) {
    //             eleId = value + "CheckboxFor" + userTestStatus[i].name + "Input" + index;
    //             var element = <HTMLInputElement>document.getElementById(eleId);
    //             if (typeof (element) != 'undefined' && element != null) {
    //                 if ((<HTMLInputElement>document.getElementById(eleId)).checked == false) {
    //             }
    //         }
    //     }
    // }

    validateInputText(txt2Validate: any, validationId: any, errorId: string, fieldName: string, isRequired: boolean = true, elementId: string = null, highlightElement: boolean = false) {
        if (txt2Validate.target.value != "") {
            console.log(validationId);
            let pattern;
            switch (validationId) {
                case 0: //free text
                    break;
                case 1:
                    pattern = /^[a-zA-Z0-9]+[a-zA-Z0-9\/&().!:' ]+$/; //a-z A-Z 0-9 / & () . ! : '
                    break;
                case 2:
                    pattern = /^[a-zA-Z0-9]+[a-zA-Z0-9\/&().!: ]+$/; //a-z A-Z 0-9 / & () . ! :
                    break;
                case 3:
                    pattern = /^[a-zA-Z0-9]+[a-zA-Z0-9 ]+$/; //a-z A-Z 0-9
                    break;
                case 4:
                    pattern = /^[a-zA-Z]+[a-zA-Z()' ]+$/; //a-z A-Z '()
                    break;
                case 5:
                    pattern = /^[a-zA-Z]+$/; //a-z A-Z
                    break;
                case 6:
                    pattern = /^[a-zA-Z0-9]+[a-zA-Z 0-9]+$/; //a-z A-Z 0-9
                    break;
                case 7: // For email
                    pattern = /^[a-zA-Z]+[a-zA-Z.1-9]+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/; //For email
                    break;
                case 8: // For Numeric with space and dash
                    pattern = /^[0-9 -]+$/; //For Numeric only
                    break;
                case 9: // For Numeric
                    pattern = /^[0-9]*$/; //For Numeric only
                    break;
                case 10: //for allocation priority code
                    pattern = /^\b[P][0-9]{1,}\b$/;
                    break;
                case 11: // For Yand N only and  7char
                    pattern = /^[YN]{7}$/;
                    break;
                case 12 :
                    break;
                case 13:
                    pattern = /^(?!0$)/; 
                break;
            }

            // let inputChar = String.fromCharCode(event.charCode);
            // console.log(event.charCode);
            if (validationId == 0) {
                if (highlightElement) {
                    document.getElementById(elementId).classList.remove('error-border-red');
                } else {
                    document.getElementById(errorId).innerHTML = "";
                    document.getElementById(errorId).classList.remove('red');
                }
                delete this.errorMsgList[errorId];
                if (--this.errorCount < 0) {
                    this.errorCount = 0;
                }
            } else if (pattern.test(txt2Validate.target.value)) {
                console.log("valid");
                if (highlightElement) {
                    document.getElementById(elementId).classList.remove('error-border-red');
                } else {
                    document.getElementById(errorId).innerHTML = "";
                    document.getElementById(errorId).classList.remove('red');
                }
                delete this.errorMsgList[errorId];
                if (--this.errorCount < 0) {
                    this.errorCount = 0;
                }
            } else {
                console.log("invalid");
                if (validationId == 11) {
                    if (highlightElement) {
                        document.getElementById(elementId).classList.add('error-border-red');
                    } else {
                        document.getElementById(errorId).innerHTML = fieldName;
                    }
                    this.errorMsgList[errorId] = fieldName;
                } else {
                    console.log("validationId : "+validationId);
                    console.log("highlightElement : "+highlightElement);
                    if (highlightElement) {
                        document.getElementById(elementId).classList.add('error-border-red');
                    } else {
                        document.getElementById(errorId).innerHTML = "Invalid " + fieldName;
                    }
                    this.errorMsgList[errorId] = "Invalid " + fieldName;
                }
                if (highlightElement) {
                    document.getElementById(elementId).classList.add('error-border-red');
                } else {
                    document.getElementById(errorId).classList.add('red');
                }
                ++this.errorCount;
            }
        } else {
            if (isRequired) {
                console.log("Empty");
                if (validationId == 11) {
                    if (!highlightElement) {
                        document.getElementById(errorId).innerHTML = fieldName;
                    }
                    this.errorMsgList[errorId] = fieldName;
                } else {
                    if (!highlightElement) {
                        document.getElementById(errorId).innerHTML = fieldName + " is required.";
                    }
                    this.errorMsgList[errorId] = fieldName + " is required.";
                }
                if (highlightElement) {
                    console.log("Empty : "+elementId);
                    document.getElementById(elementId).classList.add('error-border-red');
                    console.log("Empty : 12313213");
                } else {
                    document.getElementById(errorId).classList.add('red');
                }
            }
        }
    }
}