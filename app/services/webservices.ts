import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

export class Webservices {
    constructor(public _http: Http) {}

    /*
        method = get / put / post / patch
        localURL = local service url
        apigeeURL = actual api url (coming from servce)
        endpoint = query param used
        apiKey = URL API Key
        authorisations = URL authorisation
        environment = lOCAL / CIT / SIT / UAT / PROD
        tactical
        json4PutPatch
    */
    getService(url_call:string, addedHeader: any, environment: string){
        if(environment == 'LOCAL'){
            return this._http.get(url_call).map((res) => res.json()).catch((error) => Observable.throw(error.json()));
        } else {
            return this._http.get(url_call, { headers: addedHeader }).map((res) => res.json()).catch((error) => Observable.throw(error.json()));
        }
        
    }

    putService(url_call:string, addedHeader: any, json4PutPatch:any) {
        return this._http.put(url_call, json4PutPatch, { headers: addedHeader }).map((res) => res.json()).catch((error) => Observable.throw(error));
    }

    postService(url_call:string, addedHeader: any, json4PutPatch:any) {
        return this._http.post(url_call, json4PutPatch, { headers: addedHeader }).map((res) => res.json()).catch((error) => Observable.throw(error));
    }

    patchService(url_call:string, addedHeader: any, json4PutPatch:any) {
        return this._http.patch(url_call, json4PutPatch, { headers: addedHeader }).map(res => console.log(res)).catch((error) => Observable.throw(error));
    }

    callService(method:string = 'get', localURL: string, apigeeURL:string, endpoint: string, apiKey:string, authorisations:string, environment:string = 'LOCAL', tactical:boolean = false, json4PutPatch:any){
        let url = "";
        if(environment == 'LOCAL'){
            url = localURL + endpoint;
        } else {
            url = apigeeURL + endpoint;
        }
        let addedHeader = new Headers();
        if(environment != 'LOCAL' && !tactical) {
            if(url.indexOf("?") == -1){
                url += "?apikey="+apiKey;
            } else {
                url += "&apikey="+apiKey;
            }
            addedHeader.append('Authorization', authorisations);
        } else if(environment == 'PROD') {  //tmp solutions for the production
            url += "&apikey="+apiKey;
            addedHeader.append('Authorization', authorisations);
        }
        addedHeader.append('Content-Type', 'application/json');
        let response:any;
        switch(method){
            case 'get':
                response = this.getService(url,addedHeader, environment);
                break;
            case 'put':
                response = this.putService(url,addedHeader, json4PutPatch);
                break;
            case 'post':
                response = this.postService(url,addedHeader, json4PutPatch);
                break;
            case 'patch':
                response = this.patchService(url,addedHeader, json4PutPatch);
                break;
        }
        return response;
    }
}