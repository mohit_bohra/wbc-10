import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
//import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Constant } from '../components/constants/constant';
import { Webservices } from './webservices';
import 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class Services extends Webservices {

    claimUrl: string;
    claimAuth: string;
    configUrl : string;
    configAuth : string;
    loginapi: any;
    envn: string;
    tactocal: boolean = false;
    rbacktactical: boolean = false;
    claimTactical: boolean = false;
    configTactical: boolean = false;
    claimapikey: string;

    orderUrl: string;
    orderapiKeyType: string;
    orderApiKey: string;
    orderAuthorization: string;
    productUrl: string;
    productapiKeytype: string;
    productApiKey: string;
    productAuthorization: string;
    catalogueurl: string;
    catalogueType: string;
    catalogueapiKey: string;
    catalogueauthorization: string;
    cataloguemanagementurl: string;
    cataloguemanagementType: string;
    cataloguemanagementapiKey: string;
    cataloguemanagementauthorization: string;
    fileserviceurl: string;
    fileserviceType: string;
    fileserviceapiKey: string;
    fileserviceauthorization: string;
    priceManagementUrl: string;
    priceManagementapiKeyType: string;
    priceManagementapiKey: string;
    priceManagementauthorization: string;
    reportmanagementurl: string;
    reportmanagementType: string;
    reportmanagementapiKey: string;
    reportmanagementauthorization: string;
    configManagementurl: string;
    configManagementType: string;
    configManagementapiKey: string;
    configManagementauthorization: string;
    fulfilmenturl: string;
    fulfilmentType: string;
    fulfilmentapiKey: string;
    fulfilmentauthorization: string;
    cataloguetrackerurl: string;
    cataloguetrackerType: string;
    cataloguetrackerapiKey: string;
    cataloguetrackerauthorization: string;
    salesconfigrurl: string;
    salesconfigType: string;
    salesconfigapiKey: string;
    salesconfigauthorization: string;
    authorizationUrl: string;
    authorizationType: string;
    authorizationApiKey: string;
    authauthorization: string;
    storemanagementurl: string;
    storemanagementapiKey: string;
    storemanagementType: string;
    storemanagementauthorization: string;
    objConstant = new Constant();
    catalogTypes : string;
    uploadDealsFileType: string;

    constructor(public _http: Http) {
        super(_http);
        this.loginapi = this.objConstant.LOGINAPI;
        this.envn = this.objConstant.ENVIRNMNT;
        this.tactocal = this.objConstant.tactocal;
        this.rbacktactical = this.objConstant.rbacktactical;
        this.claimTactical = this.objConstant.claimTactical;
        this.configTactical = this.objConstant.configTactical;
        this.claimapikey = this.objConstant.claimApikey; // OCz10LUivxT4ZixZVuR2MXwMX5JFQCAQ
        this.claimUrl = this.objConstant.claimUrl;
        
        // Basic T0N6MTBMVWl2eFQ0Wml4WlZ1UjJNWHdNWDVKRlFDQVE6T2J0bmtOYXE0RXh3WkdSMw==
        this.claimAuth = this.objConstant.claimAuth;
        this.getAllurls();
    }

    getAllurls() {
        this.orderUrl = this.objConstant.orderUrl
        this.orderapiKeyType = this.objConstant.orderapiKeyType;
        this.orderApiKey = this.objConstant.orderApiKey;
        this.orderAuthorization = this.objConstant.orderAuthorization;

        //
        this.productUrl = this.objConstant.productUrl;
        this.productapiKeytype = this.objConstant.productapiKeytype;
        this.productApiKey = this.objConstant.productApiKey;
        this.productAuthorization = this.objConstant.productAuthorization;

        //
        this.catalogueurl = this.objConstant.catalogueurl;
        this.catalogueType = this.objConstant.catalogueType;
        this.catalogueapiKey = this.objConstant.catalogueapiKey;
        this.catalogueauthorization = this.objConstant.catalogueauthorization;

        //catalgoue
        this.cataloguemanagementurl = this.objConstant.cataloguemanagementurl;
        this.cataloguemanagementType = this.objConstant.cataloguemanagementType;
        this.cataloguemanagementapiKey = this.objConstant.cataloguemanagementapiKey;
        this.cataloguemanagementauthorization = this.objConstant.cataloguemanagementauthorization;

        //file
        this.fileserviceurl = this.objConstant.fileserviceurl;
        this.fileserviceType = this.objConstant.fileserviceType;
        this.fileserviceapiKey = this.objConstant.fileserviceapiKey;
        this.fileserviceauthorization = this.objConstant.fileserviceauthorization;

        //
        this.priceManagementUrl = this.objConstant.priceManagementUrl;
        this.priceManagementapiKeyType = this.objConstant.priceManagementapiKeyType;
        this.priceManagementapiKey = this.objConstant.priceManagementapiKey;
        this.priceManagementauthorization = this.objConstant.priceManagementauthorization;

        //
        this.reportmanagementurl = this.objConstant.reportmanagementurl;
        this.reportmanagementType = this.objConstant.reportmanagementType;
        this.reportmanagementapiKey = this.objConstant.reportmanagementapiKey;
        this.reportmanagementauthorization = this.objConstant.reportmanagementauthorization;

        //config
        this.configManagementurl = this.objConstant.configManagementurl;
        this.configManagementType = this.objConstant.configManagementType;
        this.configManagementapiKey = this.objConstant.configManagementapiKey;
        this.configManagementauthorization = this.objConstant.configManagementauthorization;

        //
        this.fulfilmenturl = this.objConstant.fulfilmenturl;
        this.fulfilmentType = this.objConstant.fulfilmentType;
        this.fulfilmentapiKey = this.objConstant.fulfilmentapiKey;
        this.fulfilmentauthorization = this.objConstant.fulfilmentauthorization;

        //catalogue
        this.cataloguetrackerurl = this.objConstant.cataloguetrackerurl;
        this.cataloguetrackerType = this.objConstant.cataloguetrackerType;
        this.cataloguetrackerapiKey = this.objConstant.cataloguetrackerapiKey;
        this.cataloguetrackerauthorization = this.objConstant.cataloguetrackerauthorization;

        //sales
        this.salesconfigrurl = this.objConstant.salesconfigrurl;
        this.salesconfigType = this.objConstant.salesconfigType;
        this.salesconfigapiKey = this.objConstant.salesconfigapiKey;
        this.salesconfigauthorization = this.objConstant.salesconfigauthorization;

        //
        this.authorizationUrl = this.objConstant.authorizationUrl;
        this.authorizationType = this.objConstant.authorizationType;
        this.authorizationApiKey = this.objConstant.authorizationApiKey;
        this.authauthorization = this.objConstant.authauthorization;

        this.storemanagementurl = this.objConstant.storemanagementurl;

        this.storemanagementapiKey = this.objConstant.storemanagementapiKey;
        this.storemanagementauthorization = this.objConstant.storemanagementauthorization;
    }

    siteMaintanceCall(orgName: any = '1', val: any = 'all') {

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.configManagementauthorization);


        let url_call = atob(this.configManagementurl) + 'customers/' + orgName + '/config?filtername=getPortalMaintenanceStatus&filtervalue=' + val + '&apikey=' + this.configManagementapiKey;

        // let url_call ='app/js/sitemaintance.json'
        return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error) => Observable.throw(
            (error.json())));
    }

    getAllCatagory(orgName: any = '1', val: any = 'all') {

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.configManagementauthorization);

        //let url_call ='app/js/validation.json'
        let url_call = atob(this.configManagementurl) + 'customers/' + orgName + '/config?filtername=getAllCatagory&filtervalue=' + val + '&apikey=' + this.configManagementapiKey;


        return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error) => Observable.throw(
            (error.json())));
        // console.log(url_call);
    }

    // login apu call
    authLogin(username: string, password: string) {
        //  var data = 'grant_type=password&username=' + username + '&password=' + password;
        var userdata = { "username": username, "password": password };
        var data = JSON.stringify(userdata);
        sessionStorage.setItem('usrjsn', btoa(data));
        let addedHeader = new Headers();
        //     headerss.append('Authorization', 'Basic ' + this.apibasicauth);
        addedHeader.append('Content-Type', 'application/json');
        let loginurl = this.loginapi + 'login';
        console.log(loginurl);
        return this._http.post(loginurl, data, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
    }


    // login apu call
    resetPassword(username: string) {
        //  var data = 'grant_type=password&username=' + username + '&password=' + password;
        var userdata = { "username": username, "password": "" };
        var data = JSON.stringify(userdata);
        let addedHeader = new Headers();
        //     headerss.append('Authorization', 'Basic ' + this.apibasicauth);
        addedHeader.append('Content-Type', 'application/json');
        let loginurl = this.loginapi + 'login';
        console.log(loginurl);
        // loginurl = 'http://www.mocky.io/v2/5b0e9ebe3200004c00c19b67';
        //return this._http.get(loginurl, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

        return this._http.post(loginurl, data, { headers: addedHeader }).map(res => res.json()).catch((error: Response) => this.handleSigninError(error));
    }
    handleSigninError(res: any) {
        console.log(res);
        return res.json();
    }

    authLoginTracker(orgType: any, groupId: any, userId: any, startDate: any = "", endTime: any = "") {
        var jsonString = {
            "customerid": "default",
            "userId": userId,
            "userLoginDetails": [{
                "start": startDate,
                "end": endTime
            }]
        }

        var endpoint = "";
        endpoint = 'customers/' + orgType + '/groups/' + groupId + '/users/' + userId + '/authorisations?method=add';
        return this.callService('put', 'http://172.16.245.86:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, jsonString)
    }

    authLoginTrackerforKillSession(orgType: any, groupId: any, userId: any, startDate: any = "", endTime: any = "") {
        var jsonString = {
            "customerid": "default",
            "userId": userId,
            "userLoginDetails": [{
                "start": startDate,
                "end": endTime
            }]
        }

        var endpoint = "";
        endpoint = 'customers/' + orgType + '/groups/' + groupId + '/users/' + userId + '/authorisations?method=terminate';
        return this.callService('put', 'http://172.16.245.86:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, jsonString);
    }
    // fetch data of order based on order id

    getSingleOrder(orgName: string, orderid: string) {
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.orderAuthorization);

        let url_call = atob(this.orderUrl) + 'customers/' + orgName + '/orders/' + orderid + '?apikey=' + this.orderApiKey;
        console.log(url_call);
        return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
            error.json()));

    }

    // fetch data of order based on order id

    getSingleProduct(orgName: string, orderid: string) {

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.orderAuthorization);

        let url_call = atob(this.orderUrl) + 'customers/' + orgName + '/orders/' + orderid + '?apikey=' + this.orderApiKey;
        return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
            error.json()));

    }

    // fetch data of items from order

    getOrderItems(orgName: string, orderid: string) {

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.orderAuthorization);
        // for IE it is required
        //let url_call ='app/js/order.json'
        let url_call = atob(this.orderUrl) + 'customers/' + orgName + '/orders/' + orderid + '/items?apikey=' + this.orderApiKey;
        return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
            error.json()));


    }

    //fetch validation message for each item

    getOrderValidationMessage(orgName: string, orderid: string) {
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.configManagementauthorization);

        //let url_call ='app/js/validation.json'
        let url_call = atob(this.configManagementurl) + 'customers/' + orgName + '/config?apikey=' + this.configManagementapiKey;


        return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
            error.json()));
        // console.log(url_call);


    }

    getOrderValidationMessageforperiodic(orgName: string) {
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.configManagementauthorization);

        //let url_call ='app/js/validation.json'
        let url_call = atob(this.configManagementurl) + 'customers/' + orgName + '/config?apikey=' + this.configManagementapiKey;


        return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
            error.json()));
        // console.log(url_call);


    }

    // fetch data of order based on search criteria 
    getReleventOrders(orgName: string, indexkey1: any, indexvalue1: any, indexkey2: any, indexvalue2: any, filterkey: any, filtervalue: any) {

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.orderAuthorization);
        // for IE it is required
        let url_call;
        if (filterkey == '' && indexkey2 == '') {
            url_call = atob(this.orderUrl) + 'customers/' + orgName + '/orders?apikey=' + this.orderApiKey + '&indexkey=' + indexkey1 + '&indexvalue=' + indexvalue1;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));

        } else if (filterkey == '' && indexkey2 != '') {
            url_call = atob(this.orderUrl) + 'customers/' + orgName + '/orders?apikey=' + this.orderApiKey + '&indexkey=' + indexkey1 + '&indexvalue=' + indexvalue1 + '&filterkey=' + indexkey2 + '&filtervalue=' + indexvalue2;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            url_call = atob(this.orderUrl) + 'customers/' + orgName + '/orders?apikey=' + this.orderApiKey + '&indexkey=' + indexkey1 + '&indexvalue=' + indexvalue1 + '&filterkey=' + filterkey + '&filtervalue=' + filtervalue;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }

    }

    //for catalogue Enquiry redevelopment - Start

    getAllMasterData(orgName: string) {
        let addedHeader = new Headers();
        // addedHeader.append('Authorization', 'Basic ' + this.catalogueauth);
        if (this.catalogueauthorization == null) {
            this.catalogueauthorization = 'Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==';
        }
        addedHeader.append('Authorization', this.catalogueauthorization);
        if (this.catalogueapiKey == null) {
            this.catalogueapiKey = 'LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz';
        }
        let url_call = atob(this.catalogueurl) + 'customers/@all/catalogues/dbmaster/items?apikey=' + this.catalogueapiKey + '&filtervalue=' + orgName;
        // let url_call = 'https://dev-api.morrisons.com/wholesaleorder/v1/customers/@all/catalogues/dbmaster/items?apikey=' + this.catalogueapiKey + '&filtervalue=' + orgName;
        return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
    }

    getCatalogueItem(orgName: string, catalogueQuery: string, catalogueType: string, start: any, limit: any) {
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.catalogueauthorization);
        // console.log(catalogueid);
        let url_call = '';
        var baseUrl;
        baseUrl = atob(this.catalogueurl);
        if (typeof catalogueQuery === 'undefined' || catalogueQuery == null) {
            url_call = baseUrl + 'customers/' + orgName + '/catalogues/' + catalogueType + '/items?start=' + start + '&limit=' + limit + '&apikey=' + this.catalogueapiKey;

            url_call = baseUrl + 'customers/' + orgName + '/catalogues/' + catalogueType + '/items?start=' + start + '&limit=' + limit + '&apikey=' + this.catalogueapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            url_call = baseUrl + 'customers/' + orgName + '/catalogues/' + catalogueType + '/items?query=' + catalogueQuery + '&start=' + start + '&limit=' + limit + '&apikey=' + this.catalogueapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }
    }
    getStoreItem(customerName: string, storeid: string) {
        // storeid = "5672";
        var endpoint = 'customers/' + customerName + '/stores/' + storeid;
        return this.callService('get', '', this.storemanagementurl, endpoint, this.storemanagementapiKey, this.storemanagementauthorization, this.envn, this.rbacktactical, '');

    }

    getStoreItemLatest(customerName: string, storeid: string) {
        // storeid = "5672";
        var endpoint = 'customers/' + customerName + '/stores/' + storeid + '/latest';
        return this.callService('get', '', this.storemanagementurl, endpoint, this.storemanagementapiKey, this.storemanagementauthorization, this.envn, this.rbacktactical, '');
        // let url_call = 'app/js/store.json';
        // return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
    }

    saveStoreInfo(orgName: string, productString: string, storeId: number) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        console.log(this.catalogueauthorization);
        headers.append('Authorization', this.storemanagementauthorization);
        let url_call = '';
        url_call = this.storemanagementurl + 'customers/' + orgName + '/stores/' + storeId + '?apikey=' + this.storemanagementapiKey;
        console.log(url_call);
        return this._http.patch(url_call, productString, { headers: headers }).map(res => console.log(res));
    }

    addStoreInfo(orgName: string, productString: any, storeId: number) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        console.log(this.catalogueauthorization);
        headers.append('Authorization', this.storemanagementauthorization);
        let url_call = '';
        url_call = this.storemanagementurl + 'customers/' + orgName + '/stores' + '?apikey=' + this.storemanagementapiKey;
        console.log(url_call);
        return this._http.post(url_call, productString, { headers: headers }).map(res => console.log(res));
    }

    // product detail using item id
    getProductDetailsUsingItemId(ItemId: string) {
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.productAuthorization);
        let url_call = atob(this.productUrl) + 'items/' + ItemId + '?apikey=' + this.productApiKey;
        return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

    }

    // fetch data of order based on search criteria (i.e product code)
    getProductOrders(orgName: string, indexkey: any, indexvalue: any) {
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.orderAuthorization);
        // for IE it is required
        let url_call;
        url_call = atob(this.orderUrl) + 'customers/' + orgName + '/orders?apikey=' + this.orderApiKey + '&indexkey=' + indexkey + '&indexvalue=' + indexvalue;
        return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
            error.json()));

    }

    // single product detail from catalogue service
    getSingleCatalogueItemDetails(orgName: string, catalogueType: string, catalogueid: string) {

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.catalogueauthorization);
        let url_call = '';
        url_call = atob(this.catalogueurl) + 'customers/' + orgName + '/catalogues/' + catalogueType + '/items/' + catalogueid + '?apikey=' + this.catalogueapiKey;
        return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
            error.json()));

    }

    // add product service call

    addNewProductToCatalogue(orgName: string, catalogueType: string, productString: string) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', this.catalogueauthorization);
        let url_call = '';
        url_call = atob(this.catalogueurl) + 'customers/' + orgName + '/catalogues/' + catalogueType + '/items?apikey=' + this.catalogueapiKey;
        return this._http.post(url_call, productString, { headers: headers }).map(res => console.log(res));

    }
    editProductToCatalogue(orgName: string, catalogueType: string, productString: string) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        console.log(this.catalogueauthorization);
        headers.append('Authorization', this.catalogueauthorization);
        let url_call = '';
        url_call = atob(this.catalogueurl) + 'customers/' + orgName + '/catalogues/' + catalogueType + '/items?apikey=' + this.catalogueapiKey;
        console.log(url_call);
        return this._http.patch(url_call, productString, { headers: headers }).map(res => console.log(res));


    }

    generateFulRangeExport() {
        let url_call = 'https://httpbin.org/get'; // dummy url need to replace with service url for this
        return this._http.get(url_call).map(res => console.log(res));
    }

    getSignedURL(orgName: string, filenameui: any, catalogueType: string ) {
        var obj1 = '{"userEmail":"' + sessionStorage.getItem('email') + '","type":"' + catalogueType + '","fileName":"' + filenameui + '"}';

        var data = obj1;
        console.log(data);
        console.log("data : " + data);
        let addedHeader = new Headers();
        addedHeader.append('Content-Type', 'application/json');
        addedHeader.append('Authorization', this.cataloguemanagementauthorization);
        var baseUrl = atob(this.cataloguemanagementurl);
        if (this.envn == 'LOCAL') {
            let getPreSignedUrl = baseUrl + 'customers/' + orgName + '/files';
            return this._http.put(getPreSignedUrl, data).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            let getPreSignedUrl = baseUrl + 'customers/' + orgName + '/files?apikey=' + this.cataloguemanagementapiKey;
            return this._http.put(getPreSignedUrl, data, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }
        //let getPreSignedUrl = 'https://ecsdevwholesalecatalogue-862611289.eu-west-1.elb.amazonaws.com/wholesaleweb/cataloguemanagement/v1/customers/amazon/files';

    }
     getSignedURLForDeal(orgName: string, filenameui: any, catalogueType: string ) {
        var obj1 = '{"userEmail":"' + sessionStorage.getItem('email') + '","type":"' + catalogueType + '","fileName":"' + filenameui + '"}';

        var data = obj1;
        console.log(data);
        console.log("data : " + data);
        let addedHeader = new Headers();
        addedHeader.append('Content-Type', 'application/json');
        addedHeader.append('Authorization', this.cataloguemanagementauthorization);
        var baseUrl = atob(this.cataloguemanagementurl);
        if (this.envn == 'LOCAL') {
            let getPreSignedUrl = baseUrl + 'customers/' + orgName + '/files';
            return this._http.put(getPreSignedUrl, data).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            let getPreSignedUrl = baseUrl + 'customers/' + orgName + '/files?apikey=' + this.cataloguemanagementapiKey;
            return this._http.put(getPreSignedUrl, data, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }
        //let getPreSignedUrl = 'https://ecsdevwholesalecatalogue-862611289.eu-west-1.elb.amazonaws.com/wholesaleweb/cataloguemanagement/v1/customers/amazon/files';

    }

    /*getSignedURL(orgName: string, filenameui: any) {
        var data = atob(sessionStorage.getItem('usrjsn'));
        console.log("data : " + data);
        let addedHeader = new Headers();
        addedHeader.append('Content-Type', 'application/json');

        let getPreSignedUrl = this.loginapi + 'presignedurl';
        return this._http.post(getPreSignedUrl, data, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
    }*/


    getCataloguePendingRequests(orgName: string, channels: string, requesttypes: string, teamname: string, requestid: string) {
        // let addedHeader = new Headers();
        // addedHeader.append('Authorization', this.orderAuthorization);
        // for IE it is required
        // let url_call = atob(sessionStorage.getItem('newpendingrequest')) + 'customers/' + orgName + '/channels/' + channels + '/requesttypes/' + requesttypes + '/teams/' + teamname + '/requests/' + requestid;
        let url_call_dummy = "app/js/cm_swagger_0.1.json";
        return this._http.get(url_call_dummy, {}).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
    }

    /*putFileToS3(presignedUrl: string, body: any) {
        let addheaders = new Headers();
        addheaders.append('Content-Type', 'multipart/form-data');
        addheaders.append('Access-Control-Allow-Origin', '*');
        addheaders.append('Access-Control-Allow-Headers', 'Content-Type');
        addheaders.append('Access-Control-Allow-Methods', '*');
        addheaders.append('Authorization', this.fileserviceauthorization);
        console.log(addheaders);
        return this._http.put(presignedUrl, body, { headers: addheaders }).map(res => "{}").catch((error: any) => Observable.throw(error));
    }*/

    putdataincatalogueTracker(trackerStr: any, orgName: any) {
        let addedHeader = new Headers();
        if (this.envn != 'LOCAL') {
            addedHeader.append('Authorization', this.cataloguetrackerauthorization);
        }
        addedHeader.append('Content-Type', 'application/json');
        var baseUrl = atob(this.cataloguetrackerurl);
        let url_call = '';
        if (this.envn == 'LOCAL') {
            url_call = 'http://172.16.245.86:3004/wholesale/v1/customers/' + orgName + '/audit';
        } else if (this.tactocal == true) {
            url_call = baseUrl + 'audit/customers/' + orgName + '/audit';
        } else {
            url_call = baseUrl + 'customers/' + orgName + '/audit?apikey=' + this.cataloguetrackerapiKey;
        }

        return this._http.put(url_call, trackerStr, { headers: addedHeader }).map(res => "{}").catch((error: any) => Observable.throw(error));
    }

    putFileToS3(presignedUrl: string, body: any) {
        let addheaders = new Headers();
        addheaders.append('Content-Type', 'multipart/form-data');
        addheaders.append('Access-Control-Allow-Origin', '*');
        addheaders.append('Access-Control-Allow-Headers', 'Content-Type');
        addheaders.append('Access-Control-Allow-Methods', '*');
        console.log(addheaders);
        return this._http.put(presignedUrl, body, {}).map(res => "{}").catch((error: any) => Observable.throw(error));
    }

    getTeamwisePendingRequest(orgName: string, channels: string, requesttypes: string, teamname: string, requestid: string, detailid: string) {
        // let addedHeader = new Headers();
        // let url_call = atob(sessionStorage.getItem('newpendingrequest')) + 'customers/' + orgName + '/channels/' + channels + '/requesttypes/' + requesttypes + '/teams/' + teamname + '/requests/' + requestid + '/details/' + detailid;
        let url_call_dummy = '';

        if (teamname == 'datateam') {
            url_call_dummy = "app/js/cm_swagger_datateam.json";
        } else if (teamname == 'comteam') {
            url_call_dummy = "app/js/cm_swagger_comteam.json";
        } else {
            url_call_dummy = "app/js/cm_swagger_supteam.json";
        }
        return this._http.get(url_call_dummy, {}).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
    }

    getsupplychainreivewdata(orgName: string, channels: string, requesttypes: string, requestid: string, batchId: string, limit: any) {
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.catalogueauthorization);

        //var baseUrl = "app/js/cm_swagger_comteam.json";
        if (this.envn == 'LOCAL') {
            var baseUrl = 'http://172.16.245.86:3004/wholesale/v1/range/';
            let url_call = baseUrl + 'customers/' + orgName + '/channels/' + channels + '/batches/' + batchId + '/range?start=' + limit;
            // let url_call = baseUrl;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            let url_call = atob(this.cataloguemanagementurl) + 'customers/' + orgName + '/channels/' + channels + '/batches/' + batchId + '/range?start=' + limit + '&apikey=' + this.cataloguemanagementapiKey;;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }

        // let url_call = baseUrl + 'customers/' + orgName + '/channels/' + channels + '/batches/' + batchId + '/range?start=' + limit;
    }

    // This method uses an HTTP call to the URL where the data(json) resides. On calling this function in any component, the json is returned, which one has to store in a variable
    getSupplyChainReviewDataPrint(orgName: string, channels: string, requesttypes: string, requestid: string, batchId: string, limit: any) {
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.catalogueauthorization);

        var baseUrl = '';
        if (this.envn == 'LOCAL') {
            baseUrl = 'http://172.16.245.86:3004/wholesale/v1/range/';
            let url_call = baseUrl + 'customers/' + orgName + '/channels/' + channels + '/batches/@all/range';

            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            let url_call = atob(this.cataloguemanagementurl) + 'customers/' + orgName + '/channels/' + channels + '/batches/@all/range?apikey=' + this.cataloguemanagementapiKey;

            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }

    }

    updateSupplyChainReviewData(orgName: string, prodString: string) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', this.catalogueauthorization);
        let url_call = '';

        //let baseUrl = atob(this.cataloguemanagementurl);
        if (this.envn == 'LOCAL') {
            let baseUrl = 'http://172.16.245.86:3004/wholesale/v1/range/';
            url_call = baseUrl + 'customers/' + orgName + '/range?reviewer=' + sessionStorage.getItem('name');
            return this._http.patch(url_call, prodString, { headers: headers }).map(res => console.log(res));
        } else {
            url_call = atob(this.cataloguemanagementurl) + 'customers/' + orgName + '/range?apikey=' + this.cataloguemanagementapiKey + '&reviewer=' + sessionStorage.getItem('name');
            return this._http.patch(url_call, prodString, { headers: headers }).map(res => console.log(res));
        }
    }


    getViewListOfExportsDownload(orgName: string, channels: string) {
        let headers = new Headers();
        headers.append('Authorization', this.cataloguemanagementauthorization);
        let url_call = ''; // cataloguemanagementurl

        if (this.envn == 'LOCAL') {
            var baseUrl = 'http://172.16.245.86:3004/wholesale/v1/';
            url_call = baseUrl + 'customers/' + orgName + '/channels/' + channels + '/batches/@all/range?start=@all';
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else if (this.tactocal == true) {
            url_call = 'https://wholesalecataloguemgmt.' + this.envn.toLowerCase() + '.np.morconnect.com/wholesale/v1/range/' + 'customers/' + orgName + '/channels/' + channels + '/listofexport/range';
            // url_call = atob(this.cataloguemanagementurl) + 'customers/' + orgName + '/channels/' + channels + '/batches/@all/range?start=@all?apikey=' + this.cataloguemanagementapiKey;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
            // return this._http.get(url_call,{ headers: headers } ).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            url_call = atob(this.cataloguemanagementurl) + 'customers/' + orgName + '/channels/' + channels + '/listofexport/range?apikey=' + this.cataloguemanagementapiKey;
            return this._http.get(url_call, { headers: headers }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
            // return this._http.get(url_call,{ headers: headers } ).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }

    }

    getBatchLines(orgName: string, channels: string, requesttypes: string, teamname: string, requestid: string, detailid: string) {
        // let addedHeader = new Headers();
        // let url_call = atob(sessionStorage.getItem('newpendingrequest')) + 'customers/' + orgName + '/channels/' + channels + '/requesttypes/' + requesttypes + '/teams/' + teamname + '/requests/' + requestid + '/details/' + detailid;
        let url_call_dummy = 'app/js/cm_swagger_all_lines.json';
        return this._http.get(url_call_dummy, {}).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
    }

    // get full range export data
    getFullRangeExport(orgName: string, channels: string, requesttypes: string, teamname: string, requestid: string, detailid: string) {
        let addedHeader = new Headers();
        let url_call = '';
        addedHeader.append('Authorization', this.cataloguemanagementauthorization);
        // url_call = atob(sessionStorage.getItem('newpendingrequest')) + 'customers/' + orgName + '/channels/' + channels + '/requesttypes/' + requesttypes + '/teams/' + teamname + '/requests/' + requestid + '/details/' + detailid;
        // let url_call_dummy = 'app/js/cm_swagger_full_range_export.json';
        if (this.envn == 'LOCAL') {
            var baseUrl = 'http://172.16.245.86:3004/wholesale/v1/range/';
            url_call = baseUrl + 'customers/' + orgName + '/channels/' + channels + '/exports/@all/range';
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            //url_call = 'https://wholesalecataloguemgmt.'+ this.envn.toLowerCase() +'.np.morconnect.com/wholesale/v1/range/'  + 'customers/' + orgName + '/channels/' + channels + '/exports/@all/range';
            url_call = atob(this.cataloguemanagementurl) + 'customers/' + orgName + '/channels/' + channels + '/exports/@all/range?apikey=' + this.cataloguemanagementapiKey;
            //url_call = 'https://ecssitwholesalecatalogue-1115913134.eu-west-1.elb.amazonaws.com/wholesale/v1/range/customers/amazon/channels/default/listofexport/range';
            //return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }


    }

    addNewCreateReq(orgApiName: any, newRequset: any) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', this.catalogueauthorization);
        let url_call = '';
        url_call = atob(this.cataloguemanagementurl) + 'requests?apikey=' + this.cataloguemanagementapiKey;

        // return this._http.post(url_call, newRequset, { headers: headers }).map(res => res.json());
        url_call = "app/js/cm_swagger_supteam.json";
        return this._http.get(url_call, {}).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));


    }

    getNewProductRequest(orgName: string, channels: string, requesttypes: string, teamname: string, requestid: string) {
        // let addedHeader = new Headers();
        let url_call = atob(this.cataloguemanagementurl) + 'customers/' + orgName + '/channels/' + channels + '/requesttypes/' + requesttypes + '/teams/' + teamname + '/requests/' + requestid + '?apikey=' + this.cataloguemanagementapiKey;
        let url_call_dummy = url_call;
        url_call_dummy = "app/js/cm_newProductRequest.json";
        return this._http.get(url_call_dummy, {}).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
    }

    //generateCatalogue
    generateCatalogue(orgName: string, catalogueType: string, selectionDate?: string) {
        var data = atob(sessionStorage.getItem('usrjsn'));
        console.log("data : " + data);
        var obj2 = JSON.parse(data);
        if (selectionDate !== undefined) {
            var obj1 = '{"customer":"' + orgName + '","catalogueName":"' + catalogueType + '","selectionDate":"' + selectionDate + '"}';
        } else {
            var obj1 = '{"customer":"' + orgName + '","catalogueName":"' + catalogueType + '"}';
        }
        var test = JSON.parse(obj1);
        for (var key in obj2) {
            test[key] = obj2[key];
        }
        var res = JSON.stringify(test);
        data = res;
        console.log(data);
        let addedHeader = new Headers();

        addedHeader.append('Content-Type', 'application/json');
        let url_call = this.loginapi + 'newcataloguerequest';
        // let url_call_dummy = url_call;

        //remove below two lines 
        // url_call_dummy = "http://jsonplaceholder.typicode.com/posts";
        // data = "{title: 'foo',body: 'bar',userId: 1}";

        return this._http.post(url_call, data, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

    }

    getlistofCatalogeFiles(orgName: string, catalogueQuery: string, catalogueType: string) {
        let addedHeader = new Headers();
        console.log(this.catalogueauthorization);
        addedHeader.append('Authorization', this.catalogueauthorization);
        // console.log(catalogueid);  
        if (this.envn == 'LOCAL') {
            let url_call = this.loginapi + 'customers/' + orgName + '/catalogues/' + catalogueType + '/viewcatalogfiles';
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            let url_call = this.loginapi + 'customers/' + orgName + '/catalogues/' + catalogueType + '/viewcatalogfiles?apikey=' + this.catalogueapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }
        // url_call = 'app/js/cm_swagger_all_lines.json'
    }

    getfulfilmentDetailsDataforAmazon(orgName: any, currDate: any, shipToDeliverAt: any, catagoryStr: any, uomval: any, shipFromLocationId: any, channel: any) {
        let url_call = '';
        let base_url = '';

        let uomstr = 'uom';
        console.log("uom value");
        console.log(uomval);
        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.86:3004/wholesale/v1/fulfilment';
        } else if (this.tactocal == true) {
            base_url = atob(this.fulfilmenturl) + 'fulfilment/';
        } else {
            base_url = atob(this.fulfilmenturl);
        }

        if (channel == 'All' || channel == '' || channel == undefined) {
            channel = '@all';
        }
        if (shipFromLocationId == 'All' || shipFromLocationId == '' || shipFromLocationId == undefined) {
            shipFromLocationId = '@all';
        }
        base_url = base_url + 'customers/' + orgName + '/categories/' + catagoryStr + '/fulfilment?filtername=' + shipToDeliverAt + '&filtervalue=' + currDate + '&indexkey=' + uomstr + '&indexValue=' + uomval + '&shipFromLocationId=' + shipFromLocationId + '&channel=' + channel;
        url_call = base_url;

        if (this.envn == 'LOCAL') {
            url_call = 'app/js/fulfilmentlvlone.json';
            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.tactocal == true) {
            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            console.log(url_call);
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }
    }
    getfulfilmentDetailsData(orgName: any, currDate: any, shipToDeliverAt: any, catagoryStr: any, uomval: any, shipFromLocationId: any) {
        let url_call = '';
        let base_url = '';

        let uomstr = 'uom';
        console.log("uom value");
        console.log(uomval);
        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.86:3004/wholesale/v1/fulfilment';
        } else if (this.tactocal == true) {
            base_url = atob(this.fulfilmenturl) + 'fulfilment/';
        } else {
            base_url = atob(this.fulfilmenturl);
        }

        if (shipFromLocationId == 'All' || shipFromLocationId == '' || shipFromLocationId == undefined) {
            shipFromLocationId = '@all';
        }
        base_url = base_url + 'customers/' + orgName + '/categories/' + catagoryStr + '/fulfilment?filtername=' + shipToDeliverAt + '&filtervalue=' + currDate + '&indexkey=' + uomstr + '&indexValue=' + uomval + '&shipFromLocationId=' + shipFromLocationId;
        url_call = base_url;

        if (this.envn == 'LOCAL') {
            url_call = 'app/js/fulfilmentlvlone.json';
            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.tactocal == true) {
            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            console.log(url_call);
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }
    }


    getfulfilmentDetailsTwo(orgName: any, currDate: any, shipToDeliverAt: any, catagoryStr: any, uomval: any, productId: any, start: any, shipFromLocationId: any, channel: any) {
        let url_call = '';
        let base_url = '';
        let uomstr = 'uom';
        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.97:3004/wholesale/v1/fulfilment/';
        } else if (this.tactocal == true) {
            base_url = atob(this.fulfilmenturl) + 'fulfilment/';
        } else {
            base_url = atob(this.fulfilmenturl);
        }

        if (shipFromLocationId == 'All' || shipFromLocationId == '' || shipFromLocationId == undefined) {
            shipFromLocationId = '@all';
        }

        if (channel == 'All' || channel == '' || channel == undefined) {
            channel = '@all';
        }
        if (orgName == 'amazon') {
            base_url = base_url + 'customers/' + orgName + '/categories/' + catagoryStr + '/items/' + productId + '/fulfilment?filtername=' + shipToDeliverAt + '&filtervalue=' + currDate + '&indexkey=' + uomstr + '&indexValue=' + uomval + '&shipFromLocationId=' + shipFromLocationId + '&start=' + start + '&channel=' + channel;
        }
        if (orgName != 'amazon') {
            base_url = base_url + 'customers/' + orgName + '/categories/' + catagoryStr + '/items/' + productId + '/fulfilment?filtername=' + shipToDeliverAt + '&filtervalue=' + currDate + '&indexkey=' + uomstr + '&indexValue=' + uomval + '&shipFromLocationId=' + shipFromLocationId + '&start=' + start;
        }

        url_call = base_url;

        if (this.envn == 'LOCAL') {
            console.log(url_call);
            url_call = 'app/js/fulfilmentleveltwo.json'
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.tactocal == true) {
            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //let url_call ='app/js/order.json'
            //  url_call = url_call ;
            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            console.log(url_call);
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }
    }

    getfulfilmentDataforAmazon(orgName: any, currDate: any, shipToDeliverAt: any, shipFromLocationId: any, channel: any) {
        let url_call = '';
        let base_url = '';
        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.97:3004/wholesale/v1/fulfilment/';
        } else if (this.tactocal == true) {
            base_url = atob(this.fulfilmenturl) + 'fulfilment/';
        } else {
            base_url = atob(this.fulfilmenturl);
        }

        if (shipFromLocationId == 'All' || shipFromLocationId == '' || shipFromLocationId == undefined) {
            shipFromLocationId = '@all';
        }
        if (channel == 'All' || channel == '' || channel == undefined) {
            channel = '@all';
        }
        base_url = base_url + 'customers/' + orgName + '/fulfilment?filtername=' + shipToDeliverAt + '&filtervalue=' + currDate + '&indexkey=' + shipFromLocationId + '&indexvalue=@all&channel=' + channel;
        url_call = base_url;

        if (this.envn == 'LOCAL') {
            // url_call = url_call;
            url_call = 'app/js/fulfilment.json'
            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.tactocal == true) {

            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }


    }

    getfulfilmentData(orgName: any, currDate: any, shipToDeliverAt: any, shipFromLocationId: any) {
        let url_call = '';
        let base_url = '';
        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.97:3004/wholesale/v1/fulfilment/';
        } else if (this.tactocal == true) {
            base_url = atob(this.fulfilmenturl) + 'fulfilment/';
        } else {
            base_url = atob(this.fulfilmenturl);
        }

        if (shipFromLocationId == 'All' || shipFromLocationId == '' || shipFromLocationId == undefined) {
            shipFromLocationId = '@all';
        }

        base_url = base_url + 'customers/' + orgName + '/fulfilment?filtername=' + shipToDeliverAt + '&filtervalue=' + currDate + '&indexkey=' + shipFromLocationId + '&indexvalue=@all';
        url_call = base_url;

        if (this.envn == 'LOCAL') {
            // url_call = url_call;
            url_call = 'app/js/fulfilment.json'
            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.tactocal == true) {

            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }


    }

    getNewFulFillmentDataforAmazon(orgName: string, currDate: any, shipToDeliverAt: any, shipFromLocationIdText: any, shipFromLocationId: any, channeltext: any, channel: any) {
        let url_call = '';
        let base_url = '';
        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.97:3004/wholesale/v1/fulfilment/';
        } else if (this.tactocal == true) {
            base_url = atob(this.fulfilmenturl) + 'fulfilment/';
        } else {
            base_url = atob(this.fulfilmenturl);
        }
        if (shipFromLocationId == 'All' || shipFromLocationId == '' || shipFromLocationId == undefined) {
            shipFromLocationId = '@all';
        }
        if (channel == 'All' || channel == '' || channel == undefined) {
            channel = '@all';
        }
        base_url = base_url + 'customers/' + orgName + '/fulfilment?filtername=' + shipToDeliverAt + '&filtervalue=' + currDate + '&indexkey=' + shipFromLocationIdText + '&indexvalue=' + shipFromLocationId + channeltext + channel;
        url_call = base_url;

        if (this.envn == 'LOCAL') {
            url_call = url_call;
            url_call = 'app/js/fulfilment.json'
            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.tactocal == true) {
            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }

    }

    //getfullfilment data for amazon
    getNewFulFillmentData(orgName: string, currDate: any, shipToDeliverAt: any, shipFromLocationIdText: any, shipFromLocationId: any) {
        let url_call = '';
        let base_url = '';
        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.97:3004/wholesale/v1/fulfilment/';
        } else if (this.tactocal == true) {
            base_url = atob(this.fulfilmenturl) + 'fulfilment/';
        } else {
            base_url = atob(this.fulfilmenturl);
        }
        if (shipFromLocationId == 'All' || shipFromLocationId == '' || shipFromLocationId == undefined) {
            shipFromLocationId = '@all';
        }
        base_url = base_url + 'customers/' + orgName + '/fulfilment?filtername=' + shipToDeliverAt + '&filtervalue=' + currDate + '&indexkey=' + shipFromLocationIdText + '&indexvalue=' + shipFromLocationId;
        url_call = base_url;

        if (this.envn == 'LOCAL') {
            url_call = url_call;
            url_call = 'app/js/fulfilment.json'
            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.tactocal == true) {
            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }

    }

    getShipFromLocationIdList(orgName: string, currDate: any, shipToDeliverAt: any, getAllShipFromLocationId: any) {
        let url_call = '';
        let base_url = '';
        if (this.envn == 'LOCAL') {
            // base_url = 'http://172.16.245.97:3004/wholesale/v1/fulfilment/';
            base_url = 'http://172.16.245.97:3004/wholesale/v1/config/';
        } else if (this.tactocal == true) {
            base_url = atob(this.fulfilmenturl) + 'config/';
        } else {
            base_url = atob(this.fulfilmenturl);
        }

        //base_url = base_url + 'customers/' + orgName + '/fulfilment?filtername=' + shipToDeliverAt + '&filtervalue=' + currDate + '&indexkey=' + getAllShipFromLocationId + '&indexvalue=@all';
        base_url = base_url + 'customers/' + orgName + '/config?filtername=' + getAllShipFromLocationId;
        url_call = base_url; console.log(url_call);

        if (this.envn == 'LOCAL') {
            //url_call = url_call;
            url_call = 'app/js/shiprfromlocationId1.json'
            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.tactocal == true) {
            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            let addedHeader = new Headers();
            addedHeader.append('Authorization', this.fulfilmentauthorization);
            url_call = url_call + '&apikey=' + this.fulfilmentapiKey;
            //url_call = url_call ;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }
    }

    getProductPrice(orgName: string, priceId: string, start: number) {

        let url_call = '';
        let base_url = '';
        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.86:3004/wholesale/v1/price/';

        } else {
            if (this.tactocal == true) {
                base_url = atob(this.priceManagementUrl) + 'price/';
            } else {
                base_url = atob(this.priceManagementUrl);
            }

        }
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.priceManagementauthorization);
        // console.log(catalogueid);
        if (priceId != null) {
            url_call = base_url + 'customers/' + orgName + '/price?priceid=' + priceId;
        } else {
            url_call = base_url + 'customers/' + orgName + '/price?start=' + start;
        }

        if (this.envn == 'LOCAL') {
            url_call = url_call;
            url_call = 'app/js/productidentification.json';
            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.tactocal == true) {
            url_call = url_call + '&apikey=' + this.priceManagementapiKey;
            //url_call = url_call ;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            url_call = url_call + '&apikey=' + this.priceManagementapiKey;
            //url_call = url_call ;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }


    }

    getProductPricePrintDownload(orgName: string) {
        let url_call = '';
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.priceManagementauthorization);
        let base_url = "";

        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.86:3004/wholesale/v1/price/';
        } else {
            base_url = atob(this.priceManagementUrl);
        }

        if (this.envn == "LOCAL") {
            //var base_url = 'http://172.16.245.88:3004/wholesale/v1/';
            url_call = base_url + 'customers/' + orgName + '/price';
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.tactocal == true) {
            url_call = base_url + 'price/customers/' + orgName + '/price?apikey=' + this.priceManagementapiKey;;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            url_call = base_url + 'customers/' + orgName + '/price?apikey=' + this.priceManagementapiKey;;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }

    }

    /* getlistofUploadedCatalogeFiles(orgName: string, start: any, limit: any) {

       limit = 20;
       let addedHeader = new Headers();
       addedHeader.append('Authorization', this.cataloguemanagementauthorization);
       console.log("catalogueid");
       let url_call = '';
       var baseUrl = atob(this.cataloguemanagementurl);
       if (this.envn == 'LOCAL') {
           baseUrl = 'http://172.16.245.86:3004/wholesale/v1/';
           url_call = baseUrl + 'customers/' + orgName + '/files?type=catalogue&offset=' + start + '&limit=' + limit;
           return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
               error.json()));
       } else {
           url_call = baseUrl + 'customers/' + orgName + '/files?type=catalogue&offset=' + start + '&limit=' + limit + '&apikey=' + this.cataloguemanagementapiKey;
           return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
               error.json()));

       }
   } */

    getlistofUploadedCatalogeFiles(orgName: string, start: any, limit: any) {

        //limit = 20;
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.cataloguetrackerauthorization);
        console.log("catalogueid");
        let url_call = '';
        var baseUrl = atob(this.cataloguetrackerurl);

        // bolt changes
        if(sessionStorage.getItem('orgType') === 'amazon' && sessionStorage.getItem('catalogueType')!= 'Core'){
            this.catalogTypes = sessionStorage.getItem('catalogueType');
        }
        else{
            this.catalogTypes = 'catalogue';
        }
     
        this.catalogTypes = this.catalogTypes.toLowerCase();

        if (this.envn == 'LOCAL') {
            baseUrl = 'http://172.16.245.86:3004/wholesale/v1/audit/';
            url_call = baseUrl + 'customers/' + orgName + '/audit?filtername=type&filtervalue=' + this.catalogTypes + '&start=' + start + '&limit=' + limit;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.tactocal == true) {
            url_call = baseUrl + 'audit/customers/' + orgName + '/audit?filtername=type&filtervalue=' + this.catalogTypes + '&start=' + start + '&limit=' + limit + '&apikey=' + this.cataloguetrackerapiKey;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));

        } else {
            url_call = baseUrl + 'customers/' + orgName + '/audit?filtername=type&filtervalue=' + this.catalogTypes + '&start=' + start + '&limit=' + limit + '&apikey=' + this.cataloguetrackerapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));

        }
    }

    fileReportDownload(orgName: string, reportTypeId: any, reportId: any) {
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.reportmanagementauthorization);

        if (this.envn == 'LOCAL') {
            var base_url = 'http://172.16.245.86:3004/wholesale/v1/';
            let url_call = base_url + 'report/customers/' + orgName + '/reporttypes/' + reportTypeId + '/reports/' + reportId + '?';
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else if (this.envn == 'DEV') {
            let url_call = atob(this.reportmanagementurl) + 'customers/' + orgName + '/reporttypes/' + reportTypeId + '/reports/' + reportId + '?apikey=' + this.reportmanagementapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            let url_call = atob(this.reportmanagementurl) + 'customers/' + orgName + '/reporttypes/' + reportTypeId + '/reports/' + reportId + '?apikey=' + this.reportmanagementapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }
    }


    getFilePath(downloadType: string, orgName: string, catalogueType: string) {
        let path: string = "";
        switch (downloadType) {
            case "report":
                path = "reports/" + orgName + "/";
                break;
            case "catalogue":
                path = "wholesaleinboundcatalogue/archive/";
                break;
            case "fullrange":
                path = "wholesaleoutbound/archive/";
                break;
            case "catalogueExport":
                path = "wholesaleexportcatalogue/" + orgName + "/" + catalogueType + "/";
                break;
            default:
                path = "";
                break;
        }
        return path;
    }

    fileReportDownloadUsingPresignedURL(orgName: string, filename: any, downloadType: string, actionType: string, catalogueType: string = "", filepath: string = "") {
        console.log("in filereport dwn");
        let data: any = JSON.parse(atob(sessionStorage.getItem('usrjsn')));
        let data1: any = {};
        let path: string = "";
        console.log(filepath);
        if (filepath != '') {
            path = filepath;
        } else {
            path = this.getFilePath(downloadType, orgName, catalogueType);
        }
        data1 = { "username": data.username, "password": data.password, "customer": orgName, "fileName": filename, "path": path }

        console.log("data : " + JSON.stringify(data1));
        let addedHeader = new Headers();
        addedHeader.append('Content-Type', 'application/json');
        let getPreSignedUrl = "";

        console.log("Header : " + JSON.stringify(addedHeader));
        getPreSignedUrl = this.loginapi + actionType + '/presignedurl';
        console.log("URL : " +  getPreSignedUrl);
        return this._http.post(getPreSignedUrl, data1, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
    }

    fileDownloadFromFileService(fileName: string) {
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.fileserviceauthorization);
        var data = fileName;
        // console.log(catalogueid);
        let url_call = '';
        if (this.envn == 'LOCAL') {
            url_call = atob(this.fileserviceurl) + '/files/' + fileName + '?method=read';
            return this._http.post(url_call, data).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            url_call = atob(this.fileserviceurl) + '/files/' + fileName + '?method=read&apikey=' + this.fileserviceapiKey + '&mode=multiple&folder=wholesaleinboundcatalogue/archive/';
            return this._http.post(url_call, data, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }

    }

    fileDownloadFromFileServicelist(fileName: string) {

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.fileserviceauthorization);
        var data = fileName;
        // console.log(catalogueid);
        let url_call = '';
        if (this.envn == 'LOCAL') {
            url_call = atob(this.fileserviceurl) + '/files/' + fileName + '?method=read';
            return this._http.post(url_call, data).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            url_call = atob(this.fileserviceurl) + '/files/' + fileName + '?method=read&apikey=' + this.fileserviceapiKey;
            return this._http.post(url_call, data, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }
    }

    updateProductPricingPatch(orgName: string, prodString: string) {
        let addedHeader = new Headers();
        addedHeader.append('Content-Type', 'application/json');

        let url_call = '';
        var base_url = '';

        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.88:3004/wholesale/v1/';
        } else {
            base_url = atob(this.priceManagementUrl);
        }

        if (this.envn == 'LOCAL') {
            //baseUrl = 'http://172.16.245.88:3004/wholesale/v1/';
            url_call = base_url + 'customers/' + orgName + '/price';
            return this._http.patch(url_call, prodString, { headers: addedHeader }).map(res => console.log(res));
        } else if (this.tactocal == true) {
            //baseUrl = atob(this.priceManagementUrl);
            addedHeader.append('Authorization', this.priceManagementauthorization);
            url_call = base_url + 'price/customers/' + orgName + '/price?apikey=' + this.priceManagementapiKey;
            return this._http.patch(url_call, prodString, { headers: addedHeader }).map(res => console.log(res));
        } else {
            //baseUrl = atob(this.priceManagementUrl);
            addedHeader.append('Authorization', this.priceManagementauthorization);
            url_call = base_url + 'customers/' + orgName + '/price?apikey=' + this.priceManagementapiKey;
            return this._http.patch(url_call, prodString, { headers: addedHeader }).map(res => console.log(res));
        }
    }

    displaySearchData(orgName: string, manList: any, deptList: any) {
        let url_call = "";
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.catalogueauthorization);

        var base_url = '';
        if (this.envn == 'LOCAL') {
            base_url = 'app/js/pricingSearch.json';
            //url_call = "app/js/priceSearch_new.json";
            url_call = base_url;
            console.log(url_call);
            return this._http.get(url_call, {}).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.envn == 'DEV' || this.envn == 'SIT' || this.envn == 'UAT') {
            url_call = 'https://sit-api.morrisons.com/wholesaleorder/v1' + '/customers/@all/catalogues/@all/items?apikey=' + this.catalogueapiKey + '&manufacturerList=' + manList + '&departmentList=' + deptList;
            console.log(url_call);
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            url_call = atob(this.catalogueurl) + '/customers/@all/catalogues/@all/items?apikey=' + this.catalogueapiKey + '&manufacturerList=' + manList + '&departmentList=' + deptList;
            console.log(url_call);
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }
    }

    getReports(orgName: string, reportTypeId: any, start: number) {
        var base_url = '';
        let url_call = '';

        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.84:3004/wholesale/v1/';
        } else {
            base_url = atob(this.reportmanagementurl);
        }

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.reportmanagementauthorization);

        if (reportTypeId != null) {
            url_call = base_url + 'customers/' + orgName + '/reporttypes/' + reportTypeId + '/reports?offset=' + start + "&";
        } else {
            url_call = base_url + 'customers/' + orgName + '/reporttypes?';
        }

        if (this.envn == 'LOCAL') {
            if (reportTypeId != null) {
                url_call = base_url + 'report/customers/' + orgName + '/reporttypes/' + reportTypeId + '/reports?offset=' + start + "&";
            } else {
                url_call = base_url + 'report/customers/' + orgName + '/reporttypes?';
                //url_call = "app/js/reportTypes.json";
            }
            url_call = url_call;
            console.log("get url");
            console.log(url_call);

            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.envn == 'DEV') {
            url_call = url_call + 'apikey=' + this.reportmanagementapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            url_call = url_call + 'apikey=' + this.reportmanagementapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }

    }

    // getRequestReport(orgName: string, reportTypeId: any) {
    //     var base_url = '';
    //     let url_call = '';

    //     if (this.envn == 'LOCAL') {
    //         base_url = 'http://172.16.245.86:3004/wholesale/v1/';
    //     } else {
    //         base_url = atob(this.reportmanagementurl);
    //     }

    //     let addedHeader = new Headers();
    //     addedHeader.append('Authorization', this.reportmanagementauthorization);


    //     if (reportTypeId != null) {
    //         url_call = base_url + 'customers/' + orgName + '/reports/@all?reporttypeid=' + reportTypeId + '&plotNewReport=@all' +'&';
    //     } else {
    //         url_call = base_url + 'customers/' + orgName + '/report?';
    //     }
    //     if (this.envn == 'LOCAL') {
    //         url_call = url_call;
    //         console.log(url_call);
    //         return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
    //             error.json()));
    //     } else if(this.envn == 'DEV') {
    //         url_call = url_call + 'apikey=' + this.reportmanagementapiKey;
    //         return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
    //             error.json()));
    //     } else {
    //         url_call = url_call + 'apikey=' + this.reportmanagementapiKey;
    //         return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
    //             error.json()));
    //     }
    // }

    getRequestReport(orgName: string, reportTypeId: any) {
        var url_call = 'app/js/reportRequest.json';
        return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
    }

    getReportsDownload(orgName: string, reportTypeId: any) {
        var base_url = '';
        let url_call = '';

        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.86:3004/wholesale/v1/report/';
        } else {
            base_url = atob(this.reportmanagementurl);
        }

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.reportmanagementauthorization);

        if (reportTypeId != null) {
            //url_call = base_url + 'customers/' + orgName + '/reports/@all?reporttypeid=' + reportTypeId + '&offset=@all';
            url_call = base_url + 'customers/' + orgName + '/reporttypes/' + reportTypeId + '/reports?offset=@all&';
        } else {
            // url_call = base_url + 'customers/' + orgName + '/newreport';
        }

        if (this.envn == 'LOCAL') {
            url_call = url_call;
            console.log("download url");
            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.envn == 'DEV') {
            url_call = url_call + '&apikey=' + this.reportmanagementapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            url_call = url_call + '&apikey=' + this.reportmanagementapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }
    }

    submitReportRequest(orgName: string, reportTypeId: number, finalJSON: any) {
        var base_url = "";
        let url_call = "";
        console.log(finalJSON);
        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.86:3004/wholesale/v1/';
        } else {
            base_url = atob(this.reportmanagementurl);
        }

        let addedHeader = new Headers();
        addedHeader.append('Content-Type', 'application/json');

        if (this.envn == 'LOCAL') {
            url_call = base_url + 'customers/' + orgName + '/reporttypes/' + reportTypeId + '/reports';
            console.log("post url");
            console.log(url_call);
            return this._http.post(url_call, finalJSON, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.envn == 'DEV') {
            addedHeader.append('Authorization', this.reportmanagementauthorization);
            url_call = base_url + 'customers/' + orgName + '/reporttypes/' + reportTypeId + '/reports?apikey=' + this.reportmanagementapiKey;
            return this._http.post(url_call, finalJSON, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else {
            addedHeader.append('Authorization', this.reportmanagementauthorization);
            url_call = base_url + 'customers/' + orgName + '/reporttypes/' + reportTypeId + '/reports?apikey=' + this.reportmanagementapiKey;
            return this._http.post(url_call, finalJSON, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }
    }

    /* getProductRepricing(orgName: string) {
         var base_url = "";
         var url_call = "";
 
         let addedHeader = new Headers();
         addedHeader.append('Authorization', this.priceManagementauthorization);
 
         
 
         if (this.envn == 'LOCAL') {
             //base_url = 'http://172.16.245.88:3004/wholesale/v1/';
            //url_call = base_url + 'customers/' + orgName + '/price';
            url_call = "app/js/pricing.json";
             console.log(url_call);
             return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
         } else {
             base_url = atob(this.priceManagementUrl);
             url_call = base_url + 'customers/' + orgName + '/price?apikey=' + this.priceManagementapiKey;
            // url_call = "app/js/repricing1.json";
             return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
         }
     }*/

    // prodic repricing 
    getprodicProductReprising(orgName: string, catalogueType: string, brandFlg: string, start: any = "@all") {
        var base_url = "";
        var url_call = "";
        var startStr = "";
        console.log("11");
        if (start != "@all") {
            startStr += '&start=' + start;
        }

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.priceManagementauthorization);
        console.log("22");
        if (this.envn == 'LOCAL') {
            console.log("33");
            base_url = 'http://172.16.245.86:3004/wholesale/v1/price/';
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/periodicrepricings';
            if (brandFlg != "") {
                url_call += "?brandflag=" + brandFlg + startStr;
            }
            url_call = "app/js/periodicrepricing.json";

            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

        } else if (this.tactocal == true) {
            console.log("44");
            base_url = 'https://wholesalecataloguemgmt.' + this.envn.toLowerCase() + '.np.morconnect.com/wholesale/v1/price/'
            //base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/periodicrepricings?apikey=' + this.priceManagementapiKey;
            if (brandFlg != "") {
                url_call += "&brandflag=" + brandFlg + startStr;
            }
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

        } else {
            console.log("55");

            base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/periodicrepricings?apikey=' + this.priceManagementapiKey;
            if (brandFlg != "") {
                url_call += "&brandflag=" + brandFlg + startStr;
            }
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

        }
    }

    // periodic repricing patch 
    updateperiodicRepricing(orgName: string, data: any, catalogueType: string) {
        let addedHeader = new Headers();
        console.log(data);
        addedHeader.append('Content-Type', 'application/json');
        let url_call = '';
        let base_url = '';
        if (this.envn == 'LOCAL') {
            let base_url = 'http://172.16.245.86:3004/wholesale/v1/price/';
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/periodicrepricings';
            return this._http.put(url_call, data, { headers: addedHeader });
        } else if (this.envn == 'DEV') {
            url_call = atob(this.priceManagementUrl) + 'customers/' + orgName + '/catalogues/' + catalogueType + '/periodicrepricings';
            return this._http.put(url_call, data, { headers: addedHeader });
        } else if (this.tactocal == true) {

            base_url = 'https://wholesalecataloguemgmt.' + this.envn.toLowerCase() + '.np.morconnect.com/wholesale/v1/price/'
            //base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/periodicrepricings?apikey=' + this.priceManagementapiKey;
            //  url_call = "app/js/pricing.json";      
            return this._http.put(url_call, data, { headers: addedHeader });

        } else {
            addedHeader.append('Authorization', this.priceManagementauthorization);
            base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/periodicrepricings?apikey=' + this.priceManagementapiKey;
            return this._http.put(url_call, data, { headers: addedHeader });
        }
    }


    // product repricing
    getProductRepricing(orgName: string, catalogueType: string, brandFlg: string) {
        var base_url = "";
        var url_call = "";

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.priceManagementauthorization);

        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.86:3004/wholesale/v1/price/';
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/repricings';

            // url_call = "app/js/pricing.json";

            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

        }
        else if (this.envn == 'DEV') {
            base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/repricings?apikey=' + this.priceManagementapiKey;
            if (brandFlg != "") {
                url_call += "&brandflag=" + brandFlg;
            }
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }
        else if (this.tactocal == true) {

            base_url = 'https://wholesalecataloguemgmt.' + this.envn.toLowerCase() + '.np.morconnect.com/wholesale/v1/price/'
            //base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/repricings?apikey=' + this.priceManagementapiKey;
            if (brandFlg != "") {
                url_call += "&brandflag=" + brandFlg;
            }
            // url_call = "app/js/pricing.json";   
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

        }
        else {
            //  base_url = 'https://wholesalecataloguemgmt.' + this.envn.toLowerCase() + '.np.morconnect.com/wholesale/v1/price/'

            base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/repricings?apikey=' + this.priceManagementapiKey;
            if (brandFlg != "") {
                url_call += "&brandflag=" + brandFlg;
            }
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

        }
    }
    // product repricing
    getFamilyMemberDataforRepricing(orgName: string, catalogueType: string, brandFlg: string, filterKey: string, filtervalue: string) {
        var base_url = "";
        var url_call = "";

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.priceManagementauthorization);

        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.86:3004/wholesale/v1/price/';
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/repricings';

            // url_call = "app/js/pricing.json";

            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

        }
        else if (this.envn == 'DEV') {
            base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/repricings?apikey=' + this.priceManagementapiKey;
            if (brandFlg != "") {
                url_call += "&brandflag=" + brandFlg + "&filtername=" + filterKey + "&filtervalue=" + filtervalue;
            }
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        }
        else if (this.tactocal == true) {

            base_url = 'https://wholesalecataloguemgmt.' + this.envn.toLowerCase() + '.np.morconnect.com/wholesale/v1/price/'
            //base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/repricings?apikey=' + this.priceManagementapiKey;
            if (brandFlg != "") {
                url_call += "&brandflag=" + brandFlg + "&filtername=" + filterKey + "&filtervalue=" + filtervalue;
            }
            // url_call = "app/js/pricing.json";   
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

        }
        else {
            //  base_url = 'https://wholesalecataloguemgmt.' + this.envn.toLowerCase() + '.np.morconnect.com/wholesale/v1/price/'

            base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/repricings?apikey=' + this.priceManagementapiKey;
            if (brandFlg != "") {
                url_call += "&brandflag=" + brandFlg + "&filtername=" + filterKey + "&filtervalue=" + filtervalue;
            }
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

        }
    }

    updateRepricing(orgName: string, data: any, catalogueType: string) {
        let headers = new Headers();
        console.log(data);
        headers.append('Content-Type', 'application/json');
        let url_call = '';
        let base_url = '';
        if (this.envn == 'LOCAL') {
            let base_url = 'http://172.16.245.86:3004/wholesale/v1/price/';
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/repricings';
            return this._http.put(url_call, data).map(res => res.json());
        } else if (this.envn == 'DEV') {
            url_call = atob(this.priceManagementUrl) + 'customers/' + orgName + '/catalogues/' + catalogueType + '/repricings';
            return this._http.put(url_call, data, { headers: headers }).map(res => res.json());
        } else if (this.tactocal == true) {

            base_url = 'https://wholesalecataloguemgmt.' + this.envn.toLowerCase() + '.np.morconnect.com/wholesale/v1/price/'
            //base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/repricings?apikey=' + this.priceManagementapiKey;
            //  url_call = "app/js/pricing.json";      
            return this._http.put(url_call, data, { headers: headers }).map(res => res.json());

        } else {

            headers.append('Authorization', this.priceManagementauthorization);
            base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/repricings?apikey=' + this.priceManagementapiKey;
            return this._http.put(url_call, data, { headers: headers }).map(res => res.json());
        }
    }


    getTradingGroups(orgName: string, channel: string) {
        var base_url = "";
        var url_call = "";

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.priceManagementauthorization);

        //url_call = "app/js/productConfig.json";

        if (this.envn == 'LOCAL') {
            //channel = 'default';
            base_url = 'http://172.16.245.86:3004/wholesale/v1/price/';
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + channel + '/priceconfigs';
            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else if (this.tactocal == true) {
            base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'price/customers/' + orgName + '/catalogues/' + channel + '/priceconfigs?apikey=' + this.priceManagementapiKey;
            //url_call = base_url + '&apikey=' + this.priceManagementapiKey;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + channel + '/priceconfigs?apikey=' + this.priceManagementapiKey;
            //url_call = base_url + '&apikey=' + this.priceManagementapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }

        /*return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
            error.json()));*/
    }

    getFamilyMemberData(orgName: string, catalogueType: string, brandFlg: string, start: any = "@all", filterkey: any, leadprod: any) {
        var base_url = "";
        var url_call = "";
        var startStr = "";
        console.log("11");
        if (start != "@all") {
            startStr += '&start=' + start;
        }

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.priceManagementauthorization);
        console.log("22");
        if (this.envn == 'LOCAL') {
            console.log("33");
            base_url = 'http://172.16.245.86:3004/wholesale/v1/price/';
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/periodicrepricings';
            if (brandFlg != "") {
                url_call += "?brandflag=" + brandFlg + startStr + "&filterkey=" + filterkey + "&filtervalue=" + leadprod;
            }
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

        } else if (this.tactocal == true) {
            base_url = 'https://wholesalecataloguemgmt.' + this.envn.toLowerCase() + '.np.morconnect.com/wholesale/v1/price/'
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/periodicrepricings?apikey=' + this.priceManagementapiKey;
            if (brandFlg != "") {
                url_call += "&brandflag=" + brandFlg + startStr + "&filterkey=" + filterkey + "&filtervalue=" + leadprod;
            }
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

        } else {
            base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/periodicrepricings?apikey=' + this.priceManagementapiKey;
            if (brandFlg != "") {
                url_call += "&brandflag=" + brandFlg + startStr + "&filterkey=" + filterkey + "&filtervalue=" + leadprod;
            }
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));

        }
    }

    getProductRepricingSalesData(orgName: string, legacy = "", pin = "") {
        var base_url = "";
        var url_call = "";

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.salesconfigauthorization);
        let items;
        //url_call = "app/js/productConfig.json";
        if (orgName == 'amazon' || orgName == 'mccolls') {
            items = pin;
        } else {
            items = legacy;
        }
        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.84:3004/wholesale/v1/sales/';
            url_call = base_url + 'customers/' + orgName + '/items/1/sale';
            // url_call = "app/js/productRepricingSales.json";
            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else if (this.tactocal == true) {
            base_url = atob(this.salesconfigrurl) + 'sales/';
            // url_call = base_url + 'items/customers/' + orgName + '/items/' + items + '?apikey=' + this.salesconfigapiKey;
            url_call = base_url + 'customers/' + orgName + '/items/' + items + '/sales?filtername=&filtervalue=&apikey=' + this.salesconfigapiKey;
            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            base_url = atob(this.salesconfigrurl);
            url_call = base_url + 'customers/' + orgName + '/items/' + items + '/sales?filtername=&filtervalue=&apikey=' + this.salesconfigapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }
    }

    getPricingStatus(orgName: string, attrbuteValue: string, lockStatus: string = "", attributename = "") {
        var base_url = "";
        var url_call = "";

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.priceManagementauthorization);
        let urlStr: string = "";
        if (attrbuteValue != "") {
            urlStr = "&attributeValue=" + attrbuteValue;
        }

        if (attributename == "") {
            attributename = "procured_pricing_status";
        } else if (attributename == "periodic_repricing_status") {
            urlStr = "&attributeValue=Y";
        }
        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.86:3004/wholesale/v1/price/';

            url_call = base_url + 'customers/' + orgName + '/pricestatuscount?attributename=' + attributename + urlStr;
            if (lockStatus != "") {
                url_call += "&lockstatus=" + lockStatus;
            }
            console.log(url_call);
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else if (this.tactocal == true) {
            base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'price/customers/' + orgName + '/pricestatuscount?apikey=' + this.priceManagementapiKey + '&attributename=' + attributename + urlStr;
            if (lockStatus != "") {
                url_call += "&lockstatus=" + lockStatus;
            }
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            base_url = atob(this.priceManagementUrl);
            url_call = base_url + 'customers/' + orgName + '/pricestatuscount?apikey=' + this.priceManagementapiKey + '&attributename=' + attributename + urlStr;
            if (lockStatus != "") {
                url_call += "&lockstatus=" + lockStatus;
            }
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }
    }

    savePricingConfigurations(orgName: string, pricingConf: any, catalogueType: string) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url_call = '';
        if (this.envn == 'LOCAL') {
            let base_url = 'http://172.16.245.86:3004/wholesale/v1/price/';
            url_call = base_url + 'customers/' + orgName + '/catalogues/' + catalogueType + '/priceconfigs';
            return this._http.put(url_call, pricingConf).map(res => console.log(res));
        } else if (this.tactocal == true) {
            // headers.append('Authorization', this.priceManagementauthorization);
            url_call = atob(this.priceManagementUrl) + 'price/customers/' + orgName + '/catalogues/' + catalogueType + '/priceconfigs?apikey=' + this.catalogueapiKey;
            return this._http.put(url_call, pricingConf, { headers: headers }).map(res => console.log(res));
        } else {
            headers.append('Authorization', this.priceManagementauthorization);
            url_call = atob(this.priceManagementUrl) + 'customers/' + orgName + '/catalogues/' + catalogueType + '/priceconfigs?apikey=' + this.catalogueapiKey;
            return this._http.put(url_call, pricingConf, { headers: headers }).map(res => console.log(res));
        }
    }

    getRoleBasedGroups1(orgName: string) {//2470
        var base_url = "";
        var url_call = "";

        base_url = atob(this.authorizationUrl);

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.authauthorization);

        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.86:3004/wholesale/v1/';
            url_call = base_url + 'customers/' + orgName + '/groups/@all/authorisations';
            url_call = 'app/js/rbac1.json'
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else if (this.rbacktactical == true) {
            url_call = base_url + 'customers/' + orgName + '/groups/@all/authorisations?apikey=' + this.authorizationApiKey;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            url_call = base_url + 'customers/' + orgName + '/groups/@all/authorisations?apikey=' + this.authorizationApiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }
    }

    getRoleBasedGroups(orgName: string) {//2471
        var endpoint = "";
        endpoint = 'customers/' + orgName + '/groups/@all/authorisations';
        return this.callService('get', 'http://172.16.245.86:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, '');
    }

    getGroupUser(orgName: string, groupId: any) {//2794
        var base_url = "";
        var url_call = "";

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.authauthorization);

        base_url = atob(this.authorizationUrl);

        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.86:3004/wholesale/v1/';
            url_call = base_url + 'customers/' + orgName + '/groups/' + groupId + '/authorisations';
            //url_call = 'app/js/rbac2.json'
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else if (this.rbacktactical == true) {
            url_call = base_url + 'customers/' + orgName + '/groups/' + groupId + '/authorisations?apikey=' + this.authorizationApiKey;
            // url_call = 'app/js/User.json'
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            url_call = base_url + 'customers/' + orgName + '/groups/' + groupId + '/authorisations?apikey=' + this.authorizationApiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }
    }

    getUserRoleGroups(orgName: string, grpId: string, usrID: any) {//2795
        var base_url = "";
        var url_call = "";

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.authauthorization);

        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.86:3004/wholesale/v1/';
            url_call = base_url + 'customers/' + orgName + '/groups/' + grpId + '/users/' + usrID + '/authorisations';
            // url_call ='app/js/UserRole.json'
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else if (this.rbacktactical == true) {
            base_url = atob(this.authorizationUrl);
            url_call = base_url + 'customers/' + orgName + '/groups/' + grpId + '/users/' + usrID + '/authorisations?apikey=' + this.authorizationApiKey;
            // let url_call ='app/js/UserRole.json'
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            base_url = atob(this.authorizationUrl);
            url_call = base_url + 'customers/' + orgName + '/groups/' + grpId + '/users/' + usrID + '/authorisations?apikey=' + this.authorizationApiKey;
            // let url_call ='app/js/UserRole.json'
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }
    }


    getUserPermission(orgName: string, grpId: any, usrID: any, role: any) {//2800
        var base_url = "";
        var url_call = "";

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.authauthorization);

        if (this.envn == 'LOCAL') {
            base_url = 'http://172.16.245.84:3004/wholesale/v1/';
            url_call = base_url + 'customers/' + orgName + '/groups/' + grpId + '/users/' + usrID + '/roles/' + role + '/authorisations';
            //url_call ='app/js/UserRole.json'
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else if (this.rbacktactical == true) {
            base_url = atob(this.authorizationUrl);
            url_call = base_url + 'customers/' + orgName + '/groups/' + grpId + '/users/' + usrID + '/roles/@all/authorisations?apikey=' + this.authorizationApiKey;
            // let url_call ='app/js/UserRole.json'
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        } else {
            base_url = atob(this.authorizationUrl);
            url_call = base_url + 'customers/' + orgName + '/groups/' + grpId + '/users/' + usrID + '/roles/@all/authorisations?apikey=' + this.authorizationApiKey;

            // let url_call ='app/js/UserRole.json'
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error.json()));
        }
    }



    getGroupRoles(orgName: string, grpId: string) {//2471

        var endpoint = "";
        endpoint = 'customers/' + orgName + '/groups/' + grpId + '/users/@all/authorisations';
        return this.callService('get', 'http://172.16.245.86:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, '');
    }

    getViewRoleUsersdata(customerName: any, groupId: any, roleId: any) {
        var endpoint = "";
        endpoint = 'customers/' + customerName + '/groups/' + groupId + '/roles/' + roleId + '/authorisations?view=user';
        return this.callService('get', 'http://172.16.245.86:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, '');
    }

    createAuthorisationGroupUser(orgName: string, groupId: string, finalJson: any) {
        console.log("data : ");
        console.log(finalJson);
        let addedHeader = new Headers();
        if (this.envn != 'LOCAL') {
            addedHeader.append('Authorization', this.authauthorization);
        }
        addedHeader.append('Content-Type', 'application/json');
        var baseUrl = atob(this.authorizationUrl);
        let url_call = '';
        if (this.envn == 'LOCAL') {
            url_call = 'http://172.16.245.86:3004/wholesale/v1/customers/' + orgName + '/groups/' + groupId + '/authorisations';
        } else if (this.tactocal == true) {
            url_call = baseUrl + 'customers/' + orgName + '/groups/' + groupId + '/authorisations';
        } else {
            url_call = baseUrl + 'customers/' + orgName + '/groups/' + groupId + '/authorisations?apikey=' + this.authorizationApiKey;
        }

        return this._http.put(url_call, finalJson, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(error));
    }

    createAuthorisationGroup(orgName: string, finalJson: any) {
        var endpoint = "";
        endpoint = 'customers/' + orgName + '/groups/@all/authorisations';
        return this.callService('put', 'http://172.16.245.86:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, finalJson);
    }

    updateManageGroupRole(orgName: string, groupId: any, finalJson: any) {
        var endpoint = "";
        console.log(finalJson);
        endpoint = 'customers/' + orgName + '/groups/' + groupId + '/users/@all/authorisations';
        return this.callService('put', 'http://172.16.245.37:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, finalJson);
    }

    searchUser(customer: string, searchString: string) {
        var endpoint = "";
        endpoint = 'customers/' + customer + '/groups/0/authorisations?view=search&searchValue=' + searchString;
        return this.callService('get', 'http://172.16.245.84:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, '');
    }

    getViewUserAmendments(customer: string, groupId: any, userId: any) {
        var endpoint = "";
        endpoint = 'customers/' + customer + '/groups/' + groupId + '/users/' + userId + '/authorisations?view=audit';
        return this.callService('get', 'http://172.16.245.86:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, '')
    }


    updateUserRoleStatus(orgName: string, groupID: any, userId: any, finalJson: any) {
        var endpoint = "";
        endpoint = 'customers/' + orgName + '/groups/' + groupID + '/users/' + userId + '/authorisations';
        return this.callService('put', 'http://172.16.245.86:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, finalJson);
    }

    getGroupRoleCustomers(orgType: any, groupId: any, roleId: any) {
        var endpoint = "";
        //customers/mccolls/groups/1/roles/1/authorisations?view=customer
        endpoint = 'customers/' + orgType + '/groups/' + groupId + '/roles/' + roleId + '/authorisations?view=customer';
        return this.callService('get', 'http://172.16.245.84:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, '')
    }

    updateGroupRoleCustomers(orgType: string, groupId: any, roleId: any, finalJSON: any) {
        var endpoint = "";
        endpoint = 'customers/' + orgType + '/groups/' + groupId + '/roles/' + roleId + '/authorisations?view=customer';
        return this.callService('put', 'http://172.16.245.84:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, finalJSON);
    }


    getManageRolePermission(customer: string, groupId: any, userId: any, roleId: string) {
        var endpoint = "";
        endpoint = 'customers/' + customer + '/groups/' + groupId + '/users/' + userId + '/roles/' + roleId + '/authorisations?view=customer';
        return this.callService('get', 'http://172.16.245.37:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, '')
    }
    updateManageRolePermissionGroup(customer: string, groupId: any, userId: any, roleId: string, finalJson: any) {
        var endpoint = "";
        endpoint = 'customers/' + customer + '/groups/' + groupId + '/users/' + userId + '/roles/' + roleId + '/authorisations';
        return this.callService('put', 'http://172.16.245.86:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, finalJson);
    }


    getUserHistory(customer: string, groupId: any, userId: any) {
        var endpoint = "";
        endpoint = 'customers/' + customer + '/groups/' + groupId + '/users/' + userId + '/authorisations?view=history';
        return this.callService('get', 'http://172.16.245.56:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, '')
    }

    getUserHistoryLiveUser(customer: string, groupId: any, userId: any) {
        var endpoint = "";
        endpoint = 'customers/' + 'mccolls' + '/groups/' + '@all' + '/users/' + '@all' + '/authorisations?view=history';
        return this.callService('get', 'http://172.16.245.56:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, '')
    }
    checkDuplicate(customer: string, filtername: string, filtervalue: string) {
        var endpoint = "";
        endpoint = 'customers/' + customer + '/groups/0/authorisations?view=validation&filterName=' + filtername + '&filterValue=' + filtervalue;
        return this.callService('get', 'http://172.16.245.86:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, '')
    }

    checkDuplicateCred(customer: string, filtername: string, filtervalue: string) {
        var endpoint = "";
        // customers/mccolls/groups/1/authorisations?view=validation&filterName=logon&filterValue=Ae2325kh 
        endpoint = 'customers/' + customer + '/groups/1/authorisations?view=validation&filterName=' + filtername + '&filterValue=' + filtervalue;
        return this.callService('get', 'http://172.16.245.86:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, '')
    }

    updateOrderClaim(customer: any, patchJson: any) {
        var endpoint = 'customers/' + customer + '/claims?requestedBy=portal';
        return this.callService('patch', 'https://wholesale-core.dev.np.morconnect.com/wholesale-claims/wholesale/v1/', this.claimUrl, endpoint, this.claimapikey, this.claimAuth, this.envn, this.claimTactical, patchJson);
    }

    getOrderClaims(customer: string, autoDecision: any, manualDecision: any, status: any, event: any = 'pageload', offset: any, searchString: any) {
        var endpoint = "";
        console.log(event);
        console.log(autoDecision);
        console.log(manualDecision);
        var offsetStr = "";

        if (offset != "@all") {
            offsetStr += '&offset=' + offset;
        }
        if (event == 'pageload') {
            // https://sit-api.morrisons.com/wholesale/v1/customers/mccolls/claims?view=portal&apikey=OCz10LUivxT4ZixZVuR2MXwMX5JFQCAQ

            endpoint = 'customers/' + customer + '/claims?view=portal' + offsetStr;
        } else {
            if (autoDecision != '' && manualDecision == '' && status == '') {
                endpoint = 'customers/' + customer + '/claims?view=portal&autostatus=' + autoDecision + offsetStr;
            } else if (autoDecision == '' && manualDecision != '' && status == '') {
                endpoint = 'customers/' + customer + '/claims?view=portal&manStatus=' + manualDecision + offsetStr;
            }
            else if (autoDecision != '' && manualDecision != '' && status == '') {
                endpoint = 'customers/' + customer + '/claims?view=portal&autostatus=' + autoDecision + '&manStatus=' + manualDecision + offsetStr;
            }
            else if (autoDecision == '' && manualDecision == '' && status != '') {
                endpoint = 'customers/' + customer + '/claims?view=portal' + offsetStr + '&status=' + status;
            }
            else if (autoDecision != '' && manualDecision == '' && status != '') {
                endpoint = 'customers/' + customer + '/claims?view=portal&autostatus=' + autoDecision + offsetStr + '&status=' + status;
            }
            else if (autoDecision == '' && manualDecision != '' && status != '') {
                endpoint = 'customers/' + customer + '/claims?view=portal&manStatus=' + manualDecision + offsetStr + '&status=' + status;
            } else if (autoDecision != '' && manualDecision != '' && status != '') {
                endpoint = 'customers/' + customer + '/claims?view=portal&autostatus=' + autoDecision + '&manStatus=' + manualDecision + offsetStr + '&status=' + status;
            } else if (autoDecision == '' && manualDecision == '' && status == '' && searchString == undefined) {
                endpoint = 'customers/' + customer + '/claims?view=portal' + offsetStr;
            }
            else if (autoDecision == '' && manualDecision == '' && status == '' && searchString == "") {
                endpoint = 'customers/' + customer + '/claims?view=portal' + offsetStr;
            }
            else if (autoDecision == '' && manualDecision == '' && status == '' && searchString != undefined) {
                endpoint = 'customers/' + customer + '/claims?view=portal' + offsetStr + '&searchString=' + searchString;

            }


        }
        // endpoint = 'app/js/claim_json_get.json';
        //this.envn = 'LOCAL';
        //return this.callService('get','', '', endpoint, '', '','', false,'');
        return this.callService('get', 'https://wholesale-core.dev.np.morconnect.com/wholesale-claims/wholesale/v1/', this.claimUrl, endpoint, this.claimapikey, this.claimAuth, this.envn, this.claimTactical, '');
        // return this.callService('get', 'https://wholesale-core.dev.np.morconnect.com/wholesale-claims/wholesale/v1/', 'https://wholesale-core.dev.np.morconnect.com/wholesale-claims/wholesale/v1/', endpoint, this.orderApiKey, this.orderAuthorization, this.envn, this.claimTactical, '');
    }


    validateUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        var endpoint = 'customers/' + customerName + '/groups/' + loggedInUserGroup + '/users/' + loggedInUserId + '/authorisations?view=session&sessionid=' + sessionStorage.getItem('usdi');
        //var endpoint = 'app/js/userSession.json';
        //return this.callService('get','', endpoint, endpoint, this.authorizationApiKey, this.authauthorization,'LOCAL', this.rbacktactical,'')
        return this.callService('get', 'http://172.16.245.86:3004/wholesale/v1/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, '')

    }

    getAllOrderLinesReport(customerName: any, reportTypeID: any, postJSON: any) {

        let addedHeader = new Headers();
        if (this.envn != 'LOCAL') {
            addedHeader.append('Authorization', this.authauthorization);
        }
        addedHeader.append('Content-Type', 'application/json');
        var baseUrl = atob(this.authorizationUrl);
        let url_call = '';
        if (this.envn == 'LOCAL') {
            url_call = 'http://172.16.245.56:3004/wholesale/v1/report/' + customerName + '/reporttypes/' + reportTypeID + '/reports';
        } else if (this.tactocal == true) {
            url_call = baseUrl + 'customers/' + customerName + '/reporttypes/' + reportTypeID + '/reports';
        } else {
            url_call = baseUrl + 'customers/' + customerName + '/reporttypes/' + reportTypeID + '/reports?apikey=' + this.authorizationApiKey;
        }

        return this._http.post(url_call, postJSON, { headers: addedHeader }).map(res => res.json()).catch((error) => Observable.throw(
            error.json()));



        /* var endpoint = 'customers/'+customerName+'/reporttypes/'+reportTypeID+'/reports';
        return this.callService('post', 'http://172.16.245.56:3004/wholesale/v1/report/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, postJSON) */
    }
    getOrderTracker(customerName: any, value: any) {
        var endpoint = 'customers/' + customerName + '/items/@all/sales?filtername=ordertracker&filtervalue=' + value;
        return this.callService('get', 'http://172.16.245.56:3004/wholesale/v1/report/', atob(this.authorizationUrl), endpoint, this.authorizationApiKey, this.authauthorization, this.envn, this.rbacktactical, '')

    }

    getConfigCutomerImgs(customerName: any) {
        // https://sit-api.morrisons.com/wholesale/v1/customers/all/config?apikey=LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz&filtername=customerConfig&filtervalue=customerIcons  

        var endpoint = 'customers/' + customerName + '/config?filtername=customerConfig&filtervalue=customerIcons';
        return this.callService('get', 'http://172.16.245.56:3004/wholesale/v1/report/', atob(this.configManagementurl), endpoint, this.configManagementapiKey, this.configManagementauthorization, this.envn, this.rbacktactical, '');
    }

    getConfigRemarkMenu(customerName: any) {
        var endpoint = 'customers/' + customerName + '/config?filtername=customerConfig';
        return this.callService('get', 'http://172.16.245.56:3004/wholesale/v1/report/', atob(this.configManagementurl), endpoint, this.configManagementapiKey, this.configManagementauthorization, this.envn, this.rbacktactical, '');
    }

    getAllocationPriority(customerName: any, filterName: string, filterValue: string) {
        var endpoint = 'customers/' + customerName + '/config?filtername=' + filterName + '&filtervalue=' + filterValue;
        return this.callService('get', '', atob(this.configManagementurl), endpoint, this.configManagementapiKey, this.configManagementauthorization, this.envn, this.rbacktactical, '');
    }

    getsupplyingDepot(customerName: any, filterName: string, filterValue: string) {
        var endpoint = 'customers/' + customerName + '/config?filtername=' + filterName + '&filtervalue=' + filterValue;
        return this.callService('get', '', atob(this.configManagementurl), endpoint, this.configManagementapiKey, this.configManagementauthorization, this.envn, this.rbacktactical, '');
    }

    saveSupplyingDepot(customerName: any, filterName: string = "", filterValue: string = "@all", putJSON: any) {
        let addedHeader = new Headers();
        //addedHeader.append('Authorization', this.authauthorization);
        addedHeader.append('Content-Type', 'application/json');

        var url_call;
        /*if (this.configTactical) {
            url_call = 'https://wholesalecataloguemgmt.sit.np.morconnect.com/wholesale/v1/config/customers/' + customerName + '/config?filtername=' + filterName + '&filtervalue=' + filterValue;
        } else {*/
        addedHeader.append('Authorization', this.authauthorization);
        url_call = atob(this.configManagementurl) + 'customers/' + customerName + '/config?filtername=' + filterName + '&filtervalue=' + filterValue + "&apikey=" + this.configManagementapiKey;
        //}
        //url_call = 'https://wholesalecataloguemgmt.sit.np.morconnect.com/wholesale/v1/config/customers/mccolls/config?filtername='+filterName+'&filtervalue='+filterValue;


        return this._http.put(url_call, putJSON, { headers: addedHeader }).map(res => res.json()).catch((error) => Observable.throw(error.json()));
    }


    getstoreList(customerName: any,start:any, limit:any) {
        var endpoint = 'customers/' + customerName + '/stores?&offset='+start+'&limit='+limit+'';
        return this.callService('get', '', this.storemanagementurl, endpoint, this.storemanagementapiKey, this.storemanagementauthorization, this.envn, this.rbacktactical, '');
    }
    getsinglestore(customerName: any, storeid: any) {
        var endpoint = 'customers/' + customerName + '/stores/' + storeid + '?';
        return this.callService('get', '', this.storemanagementurl, endpoint, this.storemanagementapiKey, this.storemanagementauthorization, this.envn, this.rbacktactical, '');
    }
    getAllStoreRevisionId(customerName: any, storeid: any) {
        var endpoint = 'customers/' + customerName + '/stores/' + storeid + '/revisions?';
        return this.callService('get', '', this.storemanagementurl, endpoint, this.storemanagementapiKey, this.storemanagementauthorization, this.envn, this.rbacktactical, '');
    }
    getRevisionByStoreId(customerName: string, storeid: string, revisionId: any) {
        // storeid = "5672";
        var endpoint = 'customers/' + customerName + '/stores/' + storeid + '/revisions/' + revisionId;
        return this.callService('get', '', this.storemanagementurl, endpoint, this.storemanagementapiKey, this.storemanagementauthorization, this.envn, this.rbacktactical, '');

    }
    saveAllocationPriority(customerName: any, filterName: string = "", filterValue: string = "@all", finalJSON: any) {
        let addedHeader = new Headers();
        //addedHeader.append('Authorization', this.authauthorization);
        addedHeader.append('Content-Type', 'application/json');

        var url_call;
        /*if (this.configTactical) {
            url_call = 'https://wholesalecataloguemgmt.sit.np.morconnect.com/wholesale/v1/config/customers/' + customerName + '/config?filtername=' + filterName + '&filtervalue=' + filterValue;
        } else {*/
        addedHeader.append('Authorization', this.authauthorization);
        url_call = atob(this.configManagementurl) + 'customers/' + customerName + '/config?filtername=' + filterName + '&filtervalue=' + filterValue + "&apikey=" + this.configManagementapiKey;
        //}
        // endpoint = 'customers/' + customerName + '/config?filtername='+filterName+'&filtervalue='+filterValue;
        return this._http.put(url_call, finalJSON, { headers: addedHeader }).map(res => res.json()).catch((error) => Observable.throw(error.json()));
    }

    //get effective date based data
    getEffectiveDateBasedData(orgName: string, catalogueType: string, catalogueid: string, effectiveDate: any) {

        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.catalogueauthorization);
        let url_call = '';
        url_call = atob(this.catalogueurl) + 'customers/' + orgName + '/catalogues/' + catalogueType + '/items/' + catalogueid + '/ondate/' + effectiveDate + '?apikey=' + this.catalogueapiKey;
        return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
            error.json()));

    }

    //get uploaded range files
    getlistofUploadedRangeFiles(orgName: string, start: any, limit: any) {
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.cataloguetrackerauthorization);
        console.log("catalogueid");
        let url_call = '';
        var baseUrl = atob(this.cataloguetrackerurl);
        if (this.envn == 'LOCAL') {
            baseUrl = 'http://172.16.245.86:3004/wholesale/v1/audit/';
            url_call = baseUrl + 'customers/' + orgName + '/audit?filtername=type&filtervalue=cloud&start=' + start + '&limit=' + limit;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.tactocal == true) {
            url_call = baseUrl + 'audit/customers/' + orgName + '/audit?filtername=type&filtervalue=cloud&start=' + start + '&limit=' + limit + '&apikey=' + this.cataloguetrackerapiKey;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));

        } else {
            url_call = baseUrl + 'customers/' + orgName + '/audit?filtername=type&filtervalue=cloud&start=' + start + '&limit=' + limit + '&apikey=' + this.cataloguetrackerapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));

        }
    }
    //get uploaded promotions files
    getlistofUploadedPromotionFiles(orgName: string, start: any, limit: any) {
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.cataloguetrackerauthorization);
        console.log("catalogueid");
        let url_call = '';
        var baseUrl = atob(this.cataloguetrackerurl);
        this.uploadDealsFileType = 'deals';
        if(orgName.trim() === 'amazon'){
            this.uploadDealsFileType = 'storepickdeals';
        }
        if (this.envn == 'LOCAL') {
            baseUrl = 'http://172.16.245.86:3004/wholesale/v1/audit/';
            url_call = baseUrl + 'customers/' + orgName + '/audit?filtername=type&filtervalue='+this.uploadDealsFileType+'&start=' + start + '&limit=' + limit;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.tactocal == true) {
            url_call = baseUrl + 'audit/customers/' + orgName + '/audit?filtername=type&filtervalue='+this.uploadDealsFileType+'&start=' + start + '&limit=' + limit + '&apikey=' + this.cataloguetrackerapiKey;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));

        } else {
            url_call = baseUrl + 'customers/' + orgName + '/audit?filtername=type&filtervalue='+this.uploadDealsFileType+'&start=' + start + '&limit=' + limit + '&apikey=' + this.cataloguetrackerapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));

        }
    }
    //put file to cloud
    putFileToCloud(presignedUrl: string, body: any) {
        let addheaders = new Headers();
        addheaders.append('Content-Type', 'multipart/form-data');
        addheaders.append('Access-Control-Allow-Origin', '*');
        addheaders.append('Access-Control-Allow-Headers', 'Content-Type');
        addheaders.append('Access-Control-Allow-Methods', '*');
        console.log(addheaders);
        return this._http.put(presignedUrl, body, {}).map(res => "{}").catch((error: any) => Observable.throw(error));
    }

    //put file to store catalogue and get the status
    putFileToUpdateStatus(presignedUrl: string, body: any) {
        let addheaders = new Headers();
        addheaders.append('Content-Type', 'multipart/form-data');
        addheaders.append('Access-Control-Allow-Origin', '*');
        addheaders.append('Access-Control-Allow-Headers', 'Content-Type');
        addheaders.append('Access-Control-Allow-Methods', '*');
        console.log(addheaders);
        return this._http.put(presignedUrl, body, {}).map(res => "{}").catch((error: any) => Observable.throw(error));
    }

    //get uploaded range files
    getlistofUploadedStoreFiles(orgName: string, start: any, limit: any) {
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.cataloguetrackerauthorization);
        console.log("catalogueid");
        let url_call = '';
        var baseUrl = atob(this.cataloguetrackerurl);
        if (this.envn == 'LOCAL') {
            baseUrl = 'http://172.16.245.86:3004/wholesale/v1/audit/';
            url_call = baseUrl + 'customers/' + orgName + '/audit?filtername=type&filtervalue=store&start=' + start + '&limit=' + limit;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));
        } else if (this.tactocal == true) {
            url_call = baseUrl + 'audit/customers/' + orgName + '/audit?filtername=type&filtervalue=store&start=' + start + '&limit=' + limit + '&apikey=' + this.cataloguetrackerapiKey;
            return this._http.get(url_call).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));

        } else {
            url_call = baseUrl + 'customers/' + orgName + '/audit?filtername=type&filtervalue=store&start=' + start + '&limit=' + limit + '&apikey=' + this.cataloguetrackerapiKey;
            return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
                error.json()));

        }
    }

    getAllCatalogueTypes(customerName: any, filterName: string, filterValue: string) {
        var endpoint = 'customers/' + customerName + '/config?filtername=' + filterName + '&filtervalue=' + filterValue;
        return this.callService('get', '', atob(this.configManagementurl), endpoint, this.configManagementapiKey, this.configManagementauthorization, this.envn, this.rbacktactical, '');
    }

    addMaxOrderInfo(orgName: string, productString: any) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        console.log(this.catalogueauthorization);
        headers.append('Authorization', this.configManagementauthorization);
        let url_call = '';
        var baseUrl = atob(this.configManagementurl);
        //url_call = this.storemanagementurl + 'customers/' + orgName + '/stores' + '?apikey=' + this.storemanagementapiKey;
        url_call = baseUrl + 'customers/' + orgName + '/configurations/maxOrderValidation?apikey=' + this.configManagementapiKey;
        console.log(url_call);
        return this._http.post(url_call, productString, { headers: headers }).map(res => console.log(res));
    }

    getAllMaxOrderValidationRS(orgName: string){
        let addedHeader = new Headers();
        addedHeader.append('Authorization', this.configManagementauthorization);
        let url_call = '';
        var baseUrl = atob(this.configManagementurl);
        url_call = baseUrl+'customers/'+orgName +'/configurations/maxOrderValidation?apikey=' + this.configManagementapiKey;
        return this._http.get(url_call, { headers: addedHeader }).map(res => res.json()).catch((error: any) => Observable.throw(
            error.json()));

    }
}