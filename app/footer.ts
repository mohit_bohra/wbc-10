import { Component } from '@angular/core';
import { Constant } from './components/constants/constant';

@Component({
  selector: 'fountain-footer',
  templateUrl: 'footer.html'
})
export class FooterComponent {
  objConstant = new Constant();
  contactUsUrl: any;
  whoImgUrl: any;
  constructor() {
    // Img url path and url
    this.contactUsUrl = this.objConstant.CONTACT_US;
    this.whoImgUrl = this.objConstant.WHOIMG_URL;
  }   
}

