import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'lineBreakStr'})
export class lineBreakStr implements PipeTransform {
  transform(value: string, limit: number = 10): string {
    let newStr: string = "";
    let regex= new RegExp("(.{"+limit+"})", "g");
    //console.log(regex);
    newStr = value.replace(regex, "$1<br>");
    //console.log(newStr);
    return newStr;
  }
}