import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditProductComponent } from './editproduct.component';
import { CanDeactivateGuard } from '../../../../services/can-deactivate-guard.service';

// route Configuration
const editProductRoutes: Routes = [
  { path: '', component: EditProductComponent, canDeactivate: [CanDeactivateGuard] },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(editProductRoutes)],
  exports: [RouterModule]
})
export class EditProductComponentRoutes { }
