import { NgModule } from '@angular/core';
import { EditProductComponentRoutes } from './editproduct.routes';
import { EditProductComponent } from './editproduct.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../../global/global.component';
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/primeng';

// import { DatepickerModule } from 'ng2-bootstrap';
import { DatepickerModule } from '../../../datepicker/ng2-bootstrap';
@NgModule({
  imports: [
    EditProductComponentRoutes,
    CommonModule,
    FormsModule,
    DatepickerModule.forRoot(),
    CalendarModule
  ],
  exports: [],
  declarations: [EditProductComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class EditProductModule { }
