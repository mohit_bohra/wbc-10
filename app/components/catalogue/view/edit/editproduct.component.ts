import { Component, ViewChild, HostListener } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { GlobalComponent } from '../../../global/global.component';
import { Constant } from '../../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DialogService } from '../../../../services/dialog.service';

@Component({
    templateUrl: 'editproduct.component.html',
    providers: [commonfunction]
})
export class EditProductComponent {
    @ViewChild('editproduct') editproduct:any;
    objConstant = new Constant();
    whoImgUrl: any;
    catalogueId: string;
    errorMsg: boolean;
    errorMsgString: string;
    showLoader: boolean;
    printHeader: boolean;
    organisationList: any;

    pageState: number = 8;
    customerName: string;
    customerDispayName: string;
    style: string;
    orgimgUrl: string;
    orgApiName: string;
    productDetails: any;

    productName: any;
    mapLen: any;
    homeUrl: any;
    private _sub: any;
    productMaps: any;
    productLookups: any;
    productPrices: any;
    productmin: any;
    productAnyIdentifier: any;

    catalogueType: string;
    cataloguemasterListLength: any;
    cataloguedetailListLength: any;
    catalogueDetailList: any;

    catalogueTypeMaster: any;
    availabilityMaster: any;
    WorkflowMaster: any;
    mapTypesMaster: any;
    financialTypesMaster: any;
    financialCurrenciesMaster: any;
    financialTypesMasterDisplay: any;
    financialCurrencies: any;
    mapTypesMasterDisplay: any;
    prodImgUrl: any;
    productDesc: any;
    prodType: any
    prodAvailability: any;
    prodWorkflow: any;
    isDataAvail: boolean;
    itemNumber: any;
    catalogueTypes: any;
    availability: any;
    workflow: any;
    minDate: any;
    testDate: any;
    availabilityStatus: string;
    showSuccessModel: boolean;
    lookupTypesMaster: any;
    lookupTypesMasterDisplay: any;
    prodIdentifierRowHTML: any;
    todayDateStr: string;
    tempCalId: any;
    calDefaultDate: Date;

    identifierRow: number = 1;
    pricingRow: number = 1;
    attributeRow: number = 1;
    allowWrite: boolean;

    imageUrl: string;
    imgName: string = "";
    allowAddCatalogueAttribute:boolean = false;
    msgJSON: any = {};
    terminateMsg: any;
    terminateMsgForInactiveUser:any;
    terminateFlg: boolean = false;
    unsavedChange: boolean = false;
    unsavedDateChange: boolean = false;

    constructor(private _postsService: Services, private gFunct: commonfunction, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, private formBuilder: FormBuilder, public dialogService: DialogService) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;

        // set default date null to calender
        this.errorMsgString = '';
        this.errorMsg = false;
        this.terminateMsg = this.objConstant.userSessionTerminatedMsg;
        this.terminateMsgForInactiveUser = this.objConstant.userSessionTerminatedMsgForInactiveUser;
        this.editproduct = this.formBuilder.group({});
    }

    canDeactivate(): Promise<boolean> | boolean {
        if (!this.unsavedChange || sessionStorage.getItem('unsavedChanges')!=='true') {
            return true;
        }
        return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
    }

    // function works on page load
    ngOnInit() {
        this._globalFunc.logger("Loading edit catalogue screen.");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {
            this.allowWrite = false;
            this.printHeader = true;
            this.showLoader = false;
            this.isDataAvail = false;
            this.allowAddCatalogueAttribute = false;
            // window scroll function
            this._globalFunc.autoscroll();
            this.minDate = new Date();
            this.todayDateStr = this._cfunction.todayDateStrFunction();
            // set page state variable
            this.pageState = 8;
            sessionStorage.setItem('pageState', '999');
            document.getElementById('diableLogo').style.cursor = "not-allowed";
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string, catalogueType: string, catalogueId: string, anyIdentifier: string, itemNumber: any }) => {
                this.customerName = params.orgType;
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                this.catalogueId = params.catalogueId;
                this.productAnyIdentifier = params.anyIdentifier;
                // this.productmin = params.min;
                let cusType = params.orgType;
                this.catalogueType = params.catalogueType;
                this.itemNumber = params.itemNumber;

                let orgImgUrl;
                let orgApiNameTemp;
                let writePermission;

                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
                this.imgName = this.getCustomerImage(params.orgType);

                // list of org assigned to user
                // this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));
                // // this.disabledCusChange = false;

                // document.getElementById('orderArrowSpan').style.display = 'block';
                // document.getElementById('orderMob').style.display = 'block';
                // document.getElementById('orderRaiseMob').style.display = 'none';

                // document.getElementById('catalogueMob').style.display = 'block';
                // document.getElementById('catalogueArrowSpan').style.display = 'none';
                // document.getElementById('catalogueRaiseMob').style.display = 'none';
                // document.getElementById('reportsMob').style.display = 'block';
                // document.getElementById('permissonsMob').style.display = 'block';

                // this.organisationList.forEach(function (item: any) {
                //     if (item.orgType === cusType) {
                //         // logic for permission set
                //         orgImgUrl = item.imgUrl;
                //         orgApiNameTemp = item.orgName;

                //         if ((item.permissions.menuPermission.orders).toLowerCase() == 'none') {
                //             //orders = true;
                //             document.getElementById('orderMob').style.display = 'none';
                //             // add disa class for orderRaiseMob
                //             document.getElementById('orderRaiseMob').style.display = 'none';

                //         } else {
                //             // write logic to show order
                //         }

                //         if ((item.permissions.menuPermission.raiseNewOrder).toLowerCase() == 'none') {
                //             document.getElementById('orderArrowSpan').style.display = 'none';
                //             document.getElementById('orderRaiseMob').style.display = 'none';
                //         } else {
                //             // write logic to show raise new order
                //         }

                //         if ((item.permissions.menuPermission.catalogueEnquiry).toLowerCase() == 'none') {
                //             document.getElementById('catalogueMob').style.display = 'none';
                //             document.getElementById('catalogueRaiseMob').style.display = 'none';
                //         } else {
                //             // write logic to show add new product
                //         }

                //         if ((item.permissions.catalogAdminPermission.addNewProducts).toLowerCase() == 'none') {
                //             document.getElementById('catalogueArrowSpan').style.display = 'none';
                //             document.getElementById('catalogueRaiseMob').style.display = 'none';
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.menuPermission.reports).toLowerCase() == 'none') {
                //             document.getElementById('reportsMob').style.display = 'none';
                //         }

                //         if ((item.permissions.menuPermission.accessMgmt).toLowerCase() == 'none') {
                //             document.getElementById('permissonsMob').style.display = 'none';
                //         }

                //         if ((item.permissions.catalogAdminPermission.addMasterData).toLowerCase() == 'w') {
                //             document.getElementById('prodPriAddRow').style.display = 'block';
                //             document.getElementById('prodAttrAddRow').style.display = 'block';
                //             document.getElementById('prodMapAddRow').style.display = 'block';
                //             writePermission = true;
                //         } else {
                //             document.getElementById('prodPriAddRow').style.display = 'none';
                //             document.getElementById('prodAttrAddRow').style.display = 'none';
                //             document.getElementById('prodMapAddRow').style.display = 'none';
                //             writePermission = false;
                //         }
                //     }
                // });

                this.orgimgUrl = orgImgUrl;
                this.orgApiName = params.orgType.toLowerCase(); //(orgApiNameTemp == 'rontec') ? 'rontec' : orgApiNameTemp;
                this.allowWrite = writePermission;
                console.log(this.allowWrite);
                this.getCatalogueItemDetails();
            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);

            // logic for resize pop up 
            window.onresize = () => {
                if (screen == "desktop") {
                    document.getElementById('prouctDetailTitle').style.display = 'block';
                    document.getElementById('productDetailHeader').style.display = 'block';
                    document.getElementById('productDetailImg').style.display = 'block';
                    if (window.innerWidth < this._globalFunc.stdWidth) {
                        // this.orderResult = false;
                    } else {
                        //  this.orderResult = true;
                    }
                } else {
                    if (window.innerWidth > this._globalFunc.stdWidth) {
                        //  this.orderResult = true;
                        //document.getElementById("resultModalClose").click();
                    } else {
                        //  this.orderResult = false;
                    }
                }
                screen = this._globalFunc.getDevice(window.innerWidth);
            };
        }
    }

    // navigate function
    redirect(page: any, orgType: any) {
        this._globalFunc.logger("Redirecting to "+page);
        this._router.navigate([page, this.customerName]);
    }

    redirect2Detail() {
        this._globalFunc.logger("Redirecting to catalogue detail screen.");
        document.getElementById('editConfirmationModal').style.display = 'none';
        // catalogue/mccolls/main/12605185
        this._router.navigate(["catalogue", this.customerName, this.catalogueType, this.catalogueId]);
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
        this._globalFunc.logger("Unloading Customer selection screen.");
        this._globalFunc.sentLoggerBatchToServer();
    }

    // key press event to disable special charater
    keyPress(event: any) {
        this._globalFunc.alphaNumeric(event);
    }

    keyPressWithSpace(event: any) {
        this._globalFunc.alphaNumericWithSpace(event);
    }

    keyPressNumericWithDecimal(event: any) {
        this._globalFunc.numericWithDecimal(event);
    }

    keyPressNumericWithDecimalRestrict(event: any, val: any, elementId: any) {
        val = (<HTMLInputElement>document.getElementById(elementId)).value;
        if (parseFloat(val) == val) {
            var valArr = val.split('.');
            if (valArr.length > 2 || (typeof valArr[1] != 'undefined' && valArr[1].length > 2) || valArr[0] == "") {
                val = val.substr(0, val.length - 1);
                val = parseFloat(val).toFixed(2);
                (<HTMLInputElement>document.getElementById(elementId)).value = val;
            } else {
                if (parseFloat(val) != val) {
                    val = val.substr(0, val.length - 1);
                    (<HTMLInputElement>document.getElementById(elementId)).value = val;
                }
            }
        } else {
            (<HTMLInputElement>document.getElementById(elementId)).value = "";
        }
    }

    getSingleProductDetail(value: any) {
        this.showLoader = true;
        this._globalFunc.logger("Product detail service");
        this._postsService.getProductDetailsUsingItemId(value).subscribe(data => {
            if (data != null && data != undefined) {
                this.showLoader = false;
                this.productName = data.itemDescription;
                if (data.imageUrl.length > 0) {
                    this.prodImgUrl = data.imageUrl ? data.imageUrl[0].url : this.whoImgUrl + "noimage.jpg";
                } else {
                    this.prodImgUrl = this.whoImgUrl + "noimage.jpg";
                }

                this.prodType = data.catalogue;
                this.prodAvailability = data.availability;
                this.prodWorkflow = data.workflow;

                this.getAllMasterData();
            }
        }
            , err => {
                this._globalFunc.logger("Product detail service failed");
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
        );
    }

    getAllMasterData() {
        this.showLoader = true;
        this._globalFunc.logger("catalogue master data service");
        this._postsService.getAllMasterData(this.orgApiName).subscribe(data => {
            this.showLoader = false;
            if (data != null && data != undefined) {
                if (data.availabilityStatusList != undefined) {
                    this.availabilityMaster = data.availabilityStatusList;
                }

                if (data.workflowStatuses != undefined) {
                    this.WorkflowMaster = data.workflowStatuses;
                }

                if (data.financialTypes != undefined) {
                    this.financialTypesMaster = data.financialTypes;
                    this.financialTypesMasterDisplay = this.financialTypesMaster;

                    for (let k = 0; k < this.financialTypesMasterDisplay.length; k++) {
                        let financialTypesItem = this.financialTypesMasterDisplay[k];
                        let val = this.getProductPriceValue(financialTypesItem.financialType);
                        let dateVal = this.getProductPricingDateValue(financialTypesItem.financialType);

                        let cusrrencyVal = this.getProductCurrencyValue(financialTypesItem.financialType);
                        financialTypesItem['id'] = 'pricing_' + financialTypesItem.financialType;
                        financialTypesItem['value'] = val;
                        financialTypesItem['oldvalue'] = val;
                        financialTypesItem['oldcurrency'] = cusrrencyVal;
                        financialTypesItem['currency_id'] = "currency_" + financialTypesItem.financialType;
                        financialTypesItem['currency_value'] = cusrrencyVal;
                        financialTypesItem['date_id'] = "date_pricing_" + financialTypesItem.financialType;
                        financialTypesItem['date_value'] = dateVal;
                        financialTypesItem['date_value_old'] = dateVal;

                        financialTypesItem['financialType1'] = '';
                        financialTypesItem['newRow'] = false;
                    }
                }

                if (data.financialCurrencies != undefined) {
                    this.financialCurrencies = data.financialCurrencies;
                }

                if (data.mapTypes != undefined) {

                    this.mapTypesMaster = data.mapTypes;
                    this.mapTypesMasterDisplay = this.mapTypesMaster;

                    for (let j = 0; j < this.mapTypesMasterDisplay.length; j++) {
                        let mapTypeItem = this.mapTypesMasterDisplay[j];
                        mapTypeItem['id'] = 'identifier_' + mapTypeItem.mapName;
                        let val = this.getProductMapValue(mapTypeItem.mapName);
                        let dateVal = this.getProductStartDateValue(mapTypeItem.mapName);
                        mapTypeItem['value'] = val;
                        mapTypeItem['oldvalue'] = val;
                        mapTypeItem['date_id'] = 'date_identifier_' + mapTypeItem.mapName;
                        mapTypeItem['date_value'] = dateVal;
                        mapTypeItem['date_value_old'] = dateVal;
                        mapTypeItem['mapName1'] = '';
                        mapTypeItem['newRow'] = false;
                    }
                }

                if (data.lookupTypes != undefined) {

                    this.lookupTypesMaster = data.lookupTypes;
                    this.lookupTypesMasterDisplay = this.lookupTypesMaster;

                    for (let j = 0; j < this.lookupTypesMasterDisplay.length; j++) {
                        let lookupTypeItem = this.lookupTypesMasterDisplay[j];
                        lookupTypeItem['id'] = 'lookup_' + lookupTypeItem.lookupName;
                        let val = this.getProductLookupValue(lookupTypeItem.lookupName);
                        let dateVal = this.getProductLookupStartDateValue(lookupTypeItem.lookupName);
                        lookupTypeItem['value'] = val;
                        lookupTypeItem['oldvalue'] = val;
                        lookupTypeItem['date_id'] = 'date_lookup_' + lookupTypeItem.lookupName;
                        lookupTypeItem['date_value'] = dateVal;
                        lookupTypeItem['date_value_old'] = dateVal;

                        lookupTypeItem['lookupName1'] = '';
                        lookupTypeItem['newRow'] = false;

                    }
                }
                this.isDataAvail = true;
                this.showLoader = false;

            }
        }
            , err => {
                this._globalFunc.logger("catalogue master data service failed");
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
        );
    }
    sortFunction(a: any, b: any) {
        if (a.id === b.id) {
            return 0;
        }
        else {
            return (a.id < b.id) ? -1 : 1;
        }
    }

    getCatalogueItemDetails() {
        this.showLoader = true;
        this._globalFunc.logger("catalogue items details service");
        this._postsService.getSingleCatalogueItemDetails(this.orgApiName, this.catalogueType, this.catalogueId).subscribe(data => {
            this.showLoader = false;
            this.productDetails = data;
            this.availabilityStatus = this.productDetails.availability;
            this.workflow = this.productDetails.workflow;
            this.productMaps = data.maps;
            this.productLookups = data.lookups;
            this.productPrices = data.prices;
            this.productDesc = data.description;
            var temp = new Array;
            console.log(this.productMaps);
            if (data.maps.length > 0) {
                for (let k = 0; k < data.maps.length; k++) {
                    var dataMapType = JSON.stringify(data.maps[k].type).toUpperCase();
                    if (dataMapType == '"EAN"') {
                        var tempObjean = { 'id': 0, 'jdata': data.maps[k].values[0].value };
                        temp.push(tempObjean);
                    } else {
                        if (dataMapType == '"MIN"') {
                            var tempObjmin = { 'id': 1, 'jdata': data.maps[k].values[0].value };
                            temp.push(tempObjmin);
                        } else {
                            if (dataMapType == '"PIN"') {
                                var tempObjpin = { 'id': 2, 'jdata': data.maps[k].values[0].value };
                                temp.push(tempObjpin);
                            }
                        }
                    }
                }
                // this.productAnyIdentifier = data.maps[0].values[0].value;
            }
            console.log(temp);
            var sorted = temp.sort(this.sortFunction);
            console.log(sorted);
            this.productAnyIdentifier = sorted[0].jdata;
            // if (this.productAnyIdentifier != undefined)
            this.getSingleProductDetail(this.productAnyIdentifier);
        }
            , err => {
                this._globalFunc.logger("catalogue items details service failed");
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
        );
    }

    toggaleCalender(calId: any) {
        if (document.getElementById(calId).style.display == "none") {
            document.getElementById(calId).style.display = "block";
        } else {
            document.getElementById(calId).style.display = "none";
        }
    }

    getProductMapValue(value: string) {
        let val = '';
        for (let i = 0; i < this.productMaps.length; i++) {
            if (this.productMaps[i].type == value) {
                val = this.productMaps[i].values[0].value;
                break;
            }
        }
        return val;
    }

    getProductLookupValue(value: string) {
        let val = '';
        for (let i = 0; i < this.productLookups.length; i++) {
            if (this.productLookups[i].type == value) {
                val = this.productLookups[i].values[0].value;
                break;
            }
        }
        return val;
    }

    getProductPriceValue(value: string) {
        let val = '';
        for (let i = 0; i < this.productPrices.length; i++) {
            if (this.productPrices[i].type == value) {
                val = this.productPrices[i].values[0].value;
                break;
            }
        }
        return val;
    }
    
    getProductStartDateValue(value: string) {
        let val = '';

        for (let i = 0; i < this.productMaps.length; i++) {
            if (this.productMaps[i].type == value) {
                val = this.productMaps[i].values[0].start;
                break;
            }
        }

        if (val != '') {
            return this.gFunct.dateFormator(val, 1);
        }
    }
    
    getProductLookupStartDateValue(value: string) {
        let val = '';

        for (let i = 0; i < this.productLookups.length; i++) {
            if (this.productLookups[i].type == value) {
                val = this.productLookups[i].values[0].start;
                break;
            }
        }
        if (val != '') {
            return this.gFunct.dateFormator(val, 1);
        }
        else {
            return val;
        }

    }
    
    getProductPricingDateValue(value: string) {
        let val = '';

        for (let i = 0; i < this.productPrices.length; i++) {
            if (this.productPrices[i].type == value) {
                val = this.productPrices[i].values[0].start;
                break;
            }
        }

        if (val != '') {
            return this.gFunct.dateFormator(val, 1);
        }
        else {
            return val;
        }
    }

    getProductCurrencyValue(value: string) {
        let val = '';
        for (let i = 0; i < this.productPrices.length; i++) {
            if (this.productPrices[i].type == value) {
                val = this.productPrices[i].values[0].currency;
                break;
            }
        }
        return val;
    }

    editProductToCatalogue(value: any) {
        this.unsavedChange = false;
        this.unsavedDateChange = false;
        sessionStorage.setItem('unsavedChanges','false');
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
        this._globalFunc.logger("Generating final product string for edit product");
        event.preventDefault();
        var dateISOMap: any;
        var todayDateSringLocal = this._cfunction.todayDateStrFunction();

        var that = this;

        var newProductString = '{"items":[';
        var productValue = '{';

        productValue += '"availability":"' + value.availabilityStatus + '"';
        productValue += ',"workflow": "' + value.workflow + '"';
        productValue += ',"id":' + this.catalogueId;
        productValue += ',"description":"' + value.productDesc + '"';
        productValue += ',"customer":"' + this.orgApiName + '"';
        productValue += ',"catalogue":"' + value.catalogueType + '"';
        productValue += ',"weight":false';
        productValue += ',"maps":[';

        // let lenMaps = this.mapTypesMasterDisplay.length;

        this.mapTypesMasterDisplay.forEach(function (mapTypeItem: any) {
            var mapVal = (<HTMLInputElement>document.getElementById('identifier_' + mapTypeItem.mapName)).value;
            var dateMap = (<HTMLInputElement>document.getElementById('date_identifier_' + mapTypeItem.mapName)).value;

            if ((<HTMLInputElement>document.getElementById('date_identifier_' + mapTypeItem.mapName)).value != '') {
                dateISOMap = that._cfunction.isodate(dateMap);
            } else {
                dateISOMap = todayDateSringLocal;
            }

            if ((mapVal != '' || mapTypeItem['oldvalue'] != '') && ((mapVal != mapTypeItem['oldvalue']) || (dateMap != mapTypeItem['date_value_old']))) {
                if ((<HTMLInputElement>document.getElementById('identifier_' + mapTypeItem.mapName)).value != "" && mapTypeItem.mapName != "") {
                    productValue += '{';
                    productValue += '"type":"' + mapTypeItem.mapName + '"';
                    productValue += ',"values":[{';
                    productValue += '"value": "' + mapVal + '"';
                    // productValue += ',"start": "' + nn.substring(0,19) + 'Z"';
                    productValue += ',"start": "' + dateISOMap + '"';
                    productValue += ',"end":"' + "2999-12-31T00:00:00Z" + '"';
                    productValue += '}]},';
                }
            }
        });

        // logic for added new row
        for (var n = 1; n < this.identifierRow; n++) {
            if ((<HTMLInputElement>document.getElementById('identifierKey' + n)) && (<HTMLInputElement>document.getElementById('identifierValue' + n))) {
                if ((<HTMLInputElement>document.getElementById('identifierKey' + n)).value != "" && (<HTMLInputElement>document.getElementById('identifierValue' + n)).value != "") {
                    if ((<HTMLInputElement>document.getElementById('identifierDateKey' + n)).value != '') {
                        dateISOMap = that._cfunction.isodate((<HTMLInputElement>document.getElementById('identifierDateKey' + n)).value);
                    } else {
                        dateISOMap = todayDateSringLocal;
                    }
                    productValue += '{';
                    productValue += '"type":"' + (<HTMLInputElement>document.getElementById('identifierKey' + n)).value + '"';
                    productValue += ',"values":[{';
                    productValue += '"value": "' + (<HTMLInputElement>document.getElementById('identifierValue' + n)).value + '"';
                    // productValue += ',"start": "' + nn.substring(0,19) + 'Z"';
                    productValue += ',"start": "' + dateISOMap + '"';
                    productValue += ',"end":"' + "2999-12-31T00:00:00Z" + '"';
                    productValue += '}]},';
                }
            }
        }
        if (productValue[productValue.length - 1] == ',') {
            productValue = productValue.substr(0, productValue.length - 1);
        }

        productValue += ']';
        productValue += ',"prices":[';
        var dateISOPrice: any;
        var currencyVal;
        this.financialTypesMasterDisplay.forEach(function (financialTypesMasterItem: any) {
            if ((<HTMLInputElement>document.getElementById('pricing_' + financialTypesMasterItem.financialType)).value != "" && financialTypesMasterItem.financialType != "") {
                var priceVal = (<HTMLInputElement>document.getElementById('pricing_' + financialTypesMasterItem.financialType)).value;
                var datePrice = (<HTMLInputElement>document.getElementById('date_pricing_' + financialTypesMasterItem.financialType)).value;

                if ((<HTMLInputElement>document.getElementById('date_pricing_' + financialTypesMasterItem.financialType)).value != '') {
                    dateISOPrice = that._cfunction.isodate(datePrice);
                } else {
                    dateISOPrice = todayDateSringLocal;
                }

                if ((<HTMLInputElement>document.getElementById('currency_' + financialTypesMasterItem.financialType)).value != "") {
                    currencyVal = (<HTMLInputElement>document.getElementById('currency_' + financialTypesMasterItem.financialType)).value;
                } else {
                    if (financialTypesMasterItem.financialType == 'cs') {
                        currencyVal = 'none';
                    } else {
                        currencyVal = 'gbp';
                    }
                }

                if ((priceVal != '' || financialTypesMasterItem['oldvalue'] != '') && financialTypesMasterItem.financialType != undefined && ((priceVal != financialTypesMasterItem['oldvalue']) || (datePrice != financialTypesMasterItem['date_value_old']) || (currencyVal != financialTypesMasterItem['oldcurrency']))) {
                    productValue += '{';
                    productValue += '"type":"' + financialTypesMasterItem.financialType + '"';
                    productValue += ',"values":[{';
                    productValue += '"currency": "' + currencyVal + '"';
                    productValue += ',"value": ' + priceVal;
                    // productValue += ',"start": "' + n.substring(0,19) + 'Z"';
                    productValue += ',"start": "' + dateISOPrice + '"';
                    productValue += ',"end":"' + "2999-12-31T00:00:00Z" + '"';
                    productValue += '}]},';
                }
            }
        });
        // logic for added new row
        for (var n = 1; n < this.pricingRow; n++) {
            if ((<HTMLInputElement>document.getElementById('pricingKey' + n)) && (<HTMLInputElement>document.getElementById('pricingValue' + n))) {
                if ((<HTMLInputElement>document.getElementById('pricingKey' + n)).value != "" && (<HTMLInputElement>document.getElementById('pricingValue' + n)).value != "") {
                    if ((<HTMLInputElement>document.getElementById('pricingDateKey' + n)).value != '') {
                        dateISOPrice = that._cfunction.isodate((<HTMLInputElement>document.getElementById('pricingDateKey' + n)).value);
                    } else {
                        dateISOPrice = todayDateSringLocal;
                    }

                    productValue += '{';
                    productValue += '"type":"' + (<HTMLInputElement>document.getElementById('pricingKey' + n)).value + '"';
                    productValue += ',"values":[{';
                    productValue += '"currency": "' + (<HTMLInputElement>document.getElementById('pricingCurrencyKey' + n)).value + '"';
                    productValue += ',"value": ' + (<HTMLInputElement>document.getElementById('pricingValue' + n)).value;
                    // productValue += ',"start": "' + n.substring(0,19) + 'Z"';
                    productValue += ',"start": "' + dateISOPrice + '"';
                    productValue += ',"end":"' + "2999-12-31T00:00:00Z" + '"';
                    productValue += '}]},';
                }
            }
        }

        if (productValue[productValue.length - 1] == ',') {
            productValue = productValue.substr(0, productValue.length - 1);
        }

        productValue += ']';
        productValue += ',"lookups":[';
        var dateISOLookup;

        this.lookupTypesMasterDisplay.forEach(function (lookupTypeItem: any) {
            var lookupVal = (<HTMLInputElement>document.getElementById('lookup_' + lookupTypeItem.lookupName)).value;
            var dateLookup = (<HTMLInputElement>document.getElementById('date_lookup_' + lookupTypeItem.lookupName)).value;

            if ((<HTMLInputElement>document.getElementById('date_lookup_' + lookupTypeItem.lookupName)).value != '') {
                dateISOLookup = that._cfunction.isodate(dateLookup);
            } else {
                dateISOLookup = todayDateSringLocal;
            }

            if ((lookupVal != '' || lookupTypeItem['oldvalue'] != '') && lookupTypeItem.lookupName != undefined && ((lookupVal != lookupTypeItem['oldvalue']) || (dateLookup != lookupTypeItem['date_value_old']))) {
                if ((<HTMLInputElement>document.getElementById('lookup_' + lookupTypeItem.lookupName)).value != "" && lookupTypeItem.lookupName != "") {
                    productValue += '{';
                    productValue += '"type":"' + lookupTypeItem.lookupName + '"';
                    productValue += ',"values":[{';
                    productValue += '"value": "' + lookupVal + '"';
                    productValue += ',"start": "' + dateISOLookup + '"';
                    productValue += ',"end":"' + "2999-12-31T00:00:00Z" + '"';
                    productValue += '}]},';
                }
            }
        });
        // logic for added new row
        for (var n = 1; n < this.attributeRow; n++) {
            if ((<HTMLInputElement>document.getElementById('attributeKey' + n)) && (<HTMLInputElement>document.getElementById('attributeValue' + n))) {
                if ((<HTMLInputElement>document.getElementById('attributeKey' + n)).value != "" && (<HTMLInputElement>document.getElementById('attributeValue' + n)).value != "") {
                    if ((<HTMLInputElement>document.getElementById('attributeDateKey' + n)).value != '') {
                        dateISOLookup = that._cfunction.isodate((<HTMLInputElement>document.getElementById('attributeDateKey' + n)).value);
                    } else {
                        dateISOLookup = todayDateSringLocal;
                    }
                    productValue += '{';
                    productValue += '"type":"' + (<HTMLInputElement>document.getElementById('attributeKey' + n)).value + '"';
                    productValue += ',"values":[{';
                    productValue += '"value": "' + (<HTMLInputElement>document.getElementById('attributeValue' + n)).value + '"';
                    // productValue += ',"start": "' + nn.substring(0,19) + 'Z"';
                    productValue += ',"start": "' + dateISOLookup + '"';
                    productValue += ',"end":"' + "2999-12-31T00:00:00Z" + '"';
                    productValue += '}]},';
                }
            }
        }

        if (productValue[productValue.length - 1] == ',') {
            productValue = productValue.substr(0, productValue.length - 1);
        }

        productValue += ']';
        productValue += '}';

        newProductString += productValue + ']}';

        console.log("Product String :" + newProductString);
        var checkStr = JSON.parse(newProductString);
        let isProductIdentifierAvail;
        if (checkStr.items[0].maps.length > 0) {
            for (let i = 0; i < checkStr.items[0].maps.length; i++) {
                if (checkStr.items[0].maps[i].values[0].value.length > 0) {
                    isProductIdentifierAvail = true;
                    break;
                }
            }
        } else {
            isProductIdentifierAvail = true;
        }

        if (isProductIdentifierAvail) {
            this.showLoader = true;
            this._globalFunc.logger("Edit catalogue posting JSON service");
            this._postsService.editProductToCatalogue(this.orgApiName, this.catalogueType, newProductString).subscribe(data => {
                this.showLoader = false;
                this.showSuccessModel = true;
                let sessionModal = document.getElementById('editConfirmationModal');
                sessionModal.classList.add('in');
                sessionModal.classList.remove('out');
                document.getElementById('editConfirmationModal').style.display = 'block';
            }
                , err => {
                    this._globalFunc.logger("Edit catalogue posting JSON service failed");
                    this.showLoader = false;
                    this._router.navigate(['/error404']);
                }
            );
        } else {
            // this.mapLen = checkStr.items[0].maps.length;
            document.getElementById('addProdErrMsg').style.display = 'block';
            this.errorMsgString = "Please enter at least one product identifier";
            this._globalFunc.autoscroll();
            setTimeout(() => {
                document.getElementById('addProdErrMsg').style.display = 'none';
            }, 5000);
        }

    }
    openCalender(id: any) {
        //this.calDefaultDate = currentDate;
        if ((<HTMLInputElement>document.getElementById(id)).value != "") {
            var dtStr = (<HTMLInputElement>document.getElementById(id)).value.split("/");
            this.calDefaultDate = new Date(dtStr[2] + "-" + dtStr[1] + "-" + dtStr[0]);
        }

        this.tempCalId = id;
        // let calModal = document.getElementById('calenderModal');
        document.getElementById('calenderModal').style.display = 'block';
    }

    CalOnDateSelection(value: any) {
        var dd = new Date(value).toString();
        var formatdd = dd.split(" ");
        // Fri Jul 21 2017 08:54:00 GMT+0100 (GMT Daylight Time)
        var months = [" ", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        // var mm = months.indexOf(formatdd[1]);
        var mmm = months.indexOf(formatdd[1]) < 10 ? "0" + (months.indexOf(formatdd[1])) : (months.indexOf(formatdd[1])); // getMonth() is zero-based
        var dtStr = formatdd[2] + "/" + mmm + "/" + formatdd[3];

        (<HTMLInputElement>document.getElementById(this.tempCalId)).value = dtStr;
        this.unsavedDateChange=true;
        document.getElementById('calenderModal').style.display = "none";
    }

    closeDateModelBox() {
        this.unsavedDateChange =false;
        document.getElementById('calenderModal').style.display = "none";
    }
    clearDateModelBox() {
        this.unsavedDateChange =false;
        (<HTMLInputElement>document.getElementById(this.tempCalId)).value = null;
        document.getElementById('calenderModal').style.display = "none";
    }

    //Add New row for Map
    addProdMapNewRow() {
        this.unsavedChange = true;
        sessionStorage.setItem('unsavedChanges','true');
        this._globalFunc.logger("Add product map row");
        var z = document.createElement('tr');
        z.innerHTML = "<td><input required class='ht33 orders-heading avail-text textbox-wid-bor' id='identifierKey" + this.identifierRow + "' name='identifierKey" + this.identifierRow + "' maxlength='15'></td>"
            + "<td><input required class='ht33 orders-heading avail-text textbox-wid-bor' id='identifierValue" + this.identifierRow + "' name='identifierValue" + this.identifierRow + "' maxlength='15' oncopy='return false' oncut='return false' onpaste='return false' ></td>"
            + "<td id='identifierDateTDKey" + this.identifierRow + "'><div class='input-group calendar-input-grp avail-text textbox-wid-bor'><input placeholder='DD/MM/YYYY' value='' id='identifierDateKey" + this.identifierRow + "' name='identifierDateKey" + this.identifierRow + "' readonly class='form-control selectdate bg-transparent' required><span class='input-group-addon calendar-addon bg-transparent' ><i class='glyphicon glyphicon-calendar cust-calender'></i></span></div></td>"
            + "<td><div class='dis-inlineblc padleft-5' id='identifierDeleteKey" + this.identifierRow + "' name='identifierDeleteKey" + this.identifierRow + "'><span class='glyphicon glyphicon-trash text-green padd-top-11 font-size16px' ></span></div>"
            + "</td>";
        document.getElementById('identifierBody').appendChild(z);
        var rowNumber = this.identifierRow;
        var contest = this;

        document.getElementById('identifierKey' + rowNumber).onkeypress = function ($event) {
            contest.keyPress($event);
        }
        document.getElementById('identifierValue' + rowNumber).onkeypress = function ($event) {
            contest.keyPress($event);
        }
        document.getElementById('identifierDateTDKey' + rowNumber).onclick = function () {
            contest.openCalender('identifierDateKey' + rowNumber);
        }
        document.getElementById('identifierDeleteKey' + rowNumber).onclick = function () {
            contest.deleteProdNewRow('identifierDeleteKey' + rowNumber);
        }
        this.identifierRow = this.identifierRow + 1;
    }

    //Add New row for Pricing
    addProdPriceNewRow() {
        this.unsavedChange = true;
        sessionStorage.setItem('unsavedChanges','true');
        this._globalFunc.logger("Add Price row");
        // financialCurrencies
        var selCurriencies = '<div class="col-xs-12 col-md-12 col-lg-12 pad-left-right"><span class="up_arrow glyphicon glyphicon-menu-down top fontsize-25 top3 rt5px"></span><select required class="bg-none ht33 orders-heading avail-text textbox-wid-bor" id="pricingCurrencyKey' + this.pricingRow + '" name="pricingCurrencyKey' + this.pricingRow + '"><option value="" selected>Please Select</option>';

        this.financialCurrencies.forEach(function (fin: any) {
            selCurriencies += '<option   [value]="' + fin.financialCurrency + '" >' + fin.financialCurrency + '</option> ';
        });
        selCurriencies += "</select></div>";

        var z = document.createElement('tr');
        z.innerHTML = "<td><input required class='ht33 orders-heading avail-text textbox-wid-bor' id='pricingKey" + this.pricingRow + "' name='pricingKey" + this.pricingRow + "' maxlength='15'></td>"
            + "<td>" + selCurriencies + "</td>"
            + "<td><input required class='ht33 orders-heading avail-text textbox-wid-bor' id='pricingValue" + this.pricingRow + "' name='pricingValue" + this.pricingRow + "' maxlength='15' oncopy='return false' oncut='return false' onpaste='return false' ></td>"
            + "<td id='pricingDateTDKey" + this.pricingRow + "'><div class='input-group calendar-input-grp avail-text textbox-wid-bor'><input placeholder='DD/MM/YYYY' value='' id='pricingDateKey" + this.pricingRow + "' name='pricingDateKey" + this.pricingRow + "' readonly class='form-control selectdate bg-transparent' required><span class='input-group-addon calendar-addon bg-transparent' ><i class='glyphicon glyphicon-calendar cust-calender'></i></div></td>"
            + "<td><div class='dis-inlineblc padleft-5' id='pricingDeleteKey" + this.pricingRow + "' name='pricingDeleteKey" + this.pricingRow + "'><span class='glyphicon glyphicon-trash text-green padd-top-11 font-size16px' ></span></div>"
            + "</td>";
        document.getElementById('pricingBody').appendChild(z);
        var rowNumber = this.pricingRow;
        var contest = this;
        document.getElementById('pricingKey' + rowNumber).onkeypress = function ($event) {
            contest.keyPress($event);
        }
        document.getElementById('pricingValue' + rowNumber).onkeyup = function ($event) {
            contest.keyPressNumericWithDecimalRestrict($event, (<HTMLInputElement>document.getElementById("pricingValue" + rowNumber)).value, "pricingValue" + rowNumber);
        }
        document.getElementById('pricingDateTDKey' + rowNumber).onclick = function () {
            contest.openCalender('pricingDateKey' + rowNumber);
        }
        document.getElementById('pricingDeleteKey' + rowNumber).onclick = function () {
            contest.deleteProdNewRow('pricingDeleteKey' + rowNumber);
        }
        this.pricingRow = this.pricingRow + 1;
    }
    //Add New row for Lookup
    addProdLookupNewRow() {
        this.unsavedChange = true;
        sessionStorage.setItem('unsavedChanges','true');
        this._globalFunc.logger("Add lookup row");
        var z = document.createElement('tr');
        z.innerHTML = "<td><input required class='ht33 orders-heading avail-text textbox-wid-bor' id='attributeKey" + this.attributeRow + "' name='attributeKey" + this.attributeRow + "' maxlength='15'></td>"
            + "<td><input required class='ht33 orders-heading avail-text textbox-wid-bor' id='attributeValue" + this.attributeRow + "' name='attributeValue" + this.attributeRow + "' maxlength='15' oncopy='return false' oncut='return false' onpaste='return false' ></td>"
            + "<td id='attributeDateTDKey" + this.attributeRow + "'><div class='input-group calendar-input-grp avail-text textbox-wid-bor'><input placeholder='DD/MM/YYYY' value='' id='attributeDateKey" + this.attributeRow + "' name='attributeDateKey" + this.attributeRow + "' readonly class='form-control selectdate bg-transparent' required><span class='input-group-addon calendar-addon bg-transparent' ><i class='glyphicon glyphicon-calendar cust-calender'></i></div></td>"
            + "<td><div class='dis-inlineblc padleft-5' id='attributeDeleteKey" + this.attributeRow + "' name='attributeDeleteKey" + this.attributeRow + "'><span class='glyphicon glyphicon-trash text-green padd-top-11 font-size16px' ></span></div>"

            + "</td>";
        document.getElementById('attributeBody').appendChild(z);
        var rowNumber = this.attributeRow;
        var contest = this;
        document.getElementById('attributeKey' + rowNumber).onkeypress = function ($event) {
            contest.keyPress($event);
        }
        document.getElementById('attributeValue' + rowNumber).onkeypress = function ($event) {
            contest.keyPressWithSpace($event);
        }
        document.getElementById('attributeDateTDKey' + rowNumber).onclick = function () {
            contest.openCalender('attributeDateKey' + rowNumber);
        }
        document.getElementById('attributeDeleteKey' + rowNumber).onclick = function () {
            contest.deleteProdNewRow('attributeDeleteKey' + rowNumber);
        }
        this.attributeRow = this.attributeRow + 1;
    }

    //Delete New row for Map 
    deleteProdNewRow1(value: any) {
        var list = document.getElementById(value);
        list.parentElement.parentElement.remove();
    }

    deleteProdNewRow(value: any) {
        this.unsavedChange = false;
        sessionStorage.setItem('unsavedChanges','false');
        let list = document.getElementById(value).parentNode.parentNode;
        list.parentNode.removeChild(list);
    }

    closeTab() {
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
        if(this.unsavedChange || this.unsavedDateChange){
            sessionStorage.setItem('unsavedEditProduct','true');
            this.canDeactivate();
        }else{
            window.close();
        }
    }

    getCustomerImage(custName:string){
        return this._globalFunc.getCustomerImage(custName);
    }

    getUserSession(customerName:any, loggedInUserGroup:any, loggedInUserId:any){
        this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData:any) => {
            if(!userData.isSessionActive) { // false
                // User session has expired.
                if(userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser){
                    console.log("Session terminated");
                    this.terminateFlg = true;

                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                }
                else{
                    sessionStorage.clear();
                   this._router.navigate(['']);
                }
            } else {
                if(userData.sessionMessage == this.objConstant.userSessionTerminatedMsg){
                    console.log("Session terminated");
                    this.terminateFlg = true;

                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                }else{
                    this.terminateFlg = false;
                    this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                    let changeOrgAvailable: any = [];
                    let orgImgUrl = "";
                    let orgApiNameTemp = "";
                    this.organisationList.forEach(function (item: any) {
                        if (item.customerName !== customerName) {
                            changeOrgAvailable.push(item);
                        } else {
                            orgImgUrl = item.customerName+".png";
                            orgApiNameTemp = item.customerName;
                        }
                    });
                    
                    this.orgimgUrl = orgImgUrl;
                    this.orgApiName = orgApiNameTemp;
    
                    console.log("this.organisationList234");
                    console.log(this.organisationList);
                    let that = this;
                    let orderEnquiry = true;
                    let claimsReview = true;
                    let catalogueManagementMenuPermission = false;
                    let editCatalogueEntry = false;
                    let addCatalogueAttribute = false;
                    this.organisationList.forEach(function (organisation: any) {
                        
                        if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                            console.log("organisation : "+that.customerName);
                            console.log(organisation);
                            organisation.permissionRoleList.forEach(function(permRoleList: any){
                                console.log("11");
                                console.log(permRoleList);
                                permRoleList.permissionDetailsList.forEach(function(permRoleDtlList: any){
                                    console.log("22");
                                    if(permRoleDtlList.permissionGroup == 'catalogueManagement'){
                                        permRoleDtlList.permissionNameList.forEach(function(permList: any){
                                            if(permList.permissionName == 'catalogueManagement'){// && permList.permissionStatus == 'active'){
                                                catalogueManagementMenuPermission = true;
                                                console.log("catalogueManagement permission");
                                            }
    
                                            switch(permList.permissionName){
                                                case "editCatalogueEntry":
                                                    console.log("edit catalogue permission");
                                                    editCatalogueEntry = true;
                                                    //alert(filfilment);
                                                    break;
                                                case "addCatalogueAttribute":
                                                    console.log("add catalogue attribute permission");
                                                    addCatalogueAttribute = true;
                                                    //alert(filfilment);
                                                    break;
                                            }
                                        });
                                    }
                                });
                            });
                        }
                    });
                    that = null;
                    if(!editCatalogueEntry){ // catalogue management permission not given
                        // redirect to order
                        // this._router.navigate(["orders",customerName]);
                    }
    
                    this.allowWrite = editCatalogueEntry;
                    this.allowAddCatalogueAttribute = addCatalogueAttribute;
                }
            }
            //alert(this.disabledfilfilment);
        });
    }
    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }

    onChange(){
        this.unsavedChange = true;
        sessionStorage.setItem('unsavedChanges','true');
    }
}