import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
// declare let jsPDF:any;


@Component({
    selector: 'product-detail',
    templateUrl: 'productdetail.component.html',
    providers: [commonfunction]
})
export class ProductDetailComponent {

    objConstant = new Constant();
    whoImgUrl: any;

    catalogueId: string;
    productmin: string;
    productAnyIdentifier: string;
    catalogueType: string;
    errorMsg: boolean;
    showLoader: boolean;
    printHeader: boolean;
    organisationList: any;

    pageState: number = 6;
    customerName: string;
    customerDispayName: string;
    style: string;
    orgimgUrl: string;
    orgApiName: string;
    noImgPath: string;
    productDetails: any;

    product: any;
    productType: any;

    homeUrl: any;
    private _sub: any;
    productMaps: any;
    productPrices: any;
    productAttributes: any;
    allowWrite: boolean;
    imageUrl: string;
    imgName: string = "";

    terminateFlg: boolean= false;

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _cfunction: commonfunction, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation) {

        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;

        this.errorMsg = false;
    }

    // function works on page load

    ngOnInit() {
        this._globalFunc.logger("Loading Catalogue detail screen.");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._globalFunc.logger("Catalogue detail user session not found.");
            this._router.navigate(['']);
        } else {
            
            this.terminateFlg = true;
            this.allowWrite = false;
            this.printHeader = true;
            this.showLoader = false;

            // window scroll function
            this._globalFunc.autoscroll();
            this.productAnyIdentifier = '';
            this.noImgPath = this.whoImgUrl + "noimage.jpg";

            this.imageUrl = this.noImgPath;
            // set page state variable
            this.pageState = 6;
            sessionStorage.setItem('pageState', '999');
            document.getElementById('diableLogo').style.cursor = "not-allowed";
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string, catalogueType: string, catalogueId: string }) => {
                this.customerName = params.orgType;
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                this.catalogueId = params.catalogueId;
                //  this.productmin = params.min;
                this.catalogueType = params.catalogueType;
                let cusType = params.orgType;

                let orgImgUrl;
                let orgApiNameTemp;
                let writePermission;

                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
                this.imgName = this.getCustomerImage(params.orgType);

                // list of org assigned to user
                //this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));
                // this.disabledCusChange = false;

                // document.getElementById('orderArrowSpan').style.display = 'block';
                // document.getElementById('orderMob').style.display = 'block';
                // document.getElementById('orderRaiseMob').style.display = 'none';

                // document.getElementById('catalogueMob').style.display = 'block';
                // document.getElementById('catalogueArrowSpan').style.display = 'none';
                // document.getElementById('catalogueRaiseMob').style.display = 'none';
                // document.getElementById('reportsMob').style.display = 'block';
                // document.getElementById('permissonsMob').style.display = 'block';

                // this.organisationList.forEach(function (item: any) {
                //     if (item.orgType === cusType) {
                //         // logic for permission set
                //         orgImgUrl = item.imgUrl;
                //         orgApiNameTemp = item.orgName;

                //         if ((item.permissions.menuPermission.orders).toUpperCase() == 'NONE') {
                //             //orders = true;
                //             document.getElementById('orderMob').style.display = 'none';
                //             // add disa class for orderRaiseMob
                //             document.getElementById('orderRaiseMob').style.display = 'none';
                //         } else {
                //             // write logic to show order
                //         }

                //         if ((item.permissions.menuPermission.raiseNewOrder).toUpperCase == 'NONE') {
                //             document.getElementById('orderArrowSpan').style.display = 'none';
                //             document.getElementById('orderRaiseMob').style.display = 'none';

                //         } else {
                //             // write logic to show raise new order
                //         }
                //         console.log(item.permissions.menuPermission.catalogueEnquiry);
                //         if ((item.permissions.menuPermission.catalogueEnquiry).toUpperCase() == 'NONE') {
                //             document.getElementById('catalogueMob').style.display = 'none';
                //             document.getElementById('catalogueRaiseMob').style.display = 'none';
                //         } else {
                //             // write logic to show add new product
                //         }

                //         if ((item.permissions.catalogAdminPermission.addNewProducts).toUpperCase() == 'NONE') {
                //             document.getElementById('catalogueArrowSpan').style.display = 'none';
                //             document.getElementById('catalogueRaiseMob').style.display = 'none';
                //         } else {
                //             // write logic to show add / edit new product
                //             if ((item.permissions.catalogAdminPermission.addNewProducts).toUpperCase() == 'R') {
                //                 //document.getElementById('editDiv').style.display = 'none';
                //                 writePermission = false;
                //             } else if ((item.permissions.catalogAdminPermission.addNewProducts).toUpperCase() == 'W') {
                //                 writePermission = true;
                //             }
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.menuPermission.reports).toUpperCase() == 'NONE') {
                //             document.getElementById('reportsMob').style.display = 'none';
                //         }

                //         if ((item.permissions.menuPermission.accessMgmt).toUpperCase() == 'NONE') {
                //             document.getElementById('permissonsMob').style.display = 'none';
                //         }
                //     }
                // });

                this.orgimgUrl = orgImgUrl;
                this.orgApiName = params.orgType.toLowerCase(); //(orgApiNameTemp == 'rontec') ? 'rontec' : orgApiNameTemp;
                //this.allowWrite = writePermission;
            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);

            // logic for resize pop up 
            window.onresize = () => {
                if (screen == "desktop") {
                    document.getElementById('prouctDetailTitle').style.display = 'block';
                    document.getElementById('productDetailHeader').style.display = 'block';
                    document.getElementById('productDetailImg').style.display = 'block';
                    document.getElementById('itemCodeMapping').style.display = 'block';
                    document.getElementById('itemPriceInfo').style.display = 'block';
                    document.getElementById('itemAttr').style.display = 'block';
                    document.getElementById('itemCodeMapping').classList.toggle("hidden-xs", true);
                    document.getElementById('itemPriceInfo').classList.toggle("hidden-xs", true);
                    document.getElementById('itemAttr').classList.toggle("hidden-xs", true);

                    if (window.innerWidth < this._globalFunc.stdWidth) {
                        // this.orderResult = false;
                    } else {
                        //  this.orderResult = true;
                    }
                } else {
                    if (window.innerWidth > this._globalFunc.stdWidth) {
                        //  this.orderResult = true;
                        //document.getElementById("resultModalClose").click();
                    } else {
                        //  this.orderResult = false;
                    }
                }

                screen = this._globalFunc.getDevice(window.innerWidth);
            };
            console.log(this.catalogueId);

            this.getCatalogueItemDetails();
        }
    }

    showTable(tableCode: string) {
        document.getElementById('prouctDetailTitle').style.display = 'none';
        document.getElementById('productDetailHeader').style.display = 'none';
        document.getElementById('productDetailImg').style.display = 'none';
        document.getElementById('itemCodeMapping').style.display = 'none';
        document.getElementById('itemPriceInfo').style.display = 'none';
        document.getElementById('itemAttr').style.display = 'none';
        document.getElementById('itemCodeMapping').classList.toggle("hidden-xs", true);
        document.getElementById('itemPriceInfo').classList.toggle("hidden-xs", true);
        document.getElementById('itemAttr').classList.toggle("hidden-xs", true);
        switch (tableCode) {
            case "itemCodeMapping":
                document.getElementById('itemCodeMapping').style.display = 'block';
                document.getElementById('itemCodeMapping').classList.toggle("hidden-xs", false);
                break;
            case "itemPriceInfo":
                document.getElementById('itemPriceInfo').style.display = 'block';
                document.getElementById('itemPriceInfo').classList.toggle("hidden-xs", false);
                break;
            case "itemAttr":
                document.getElementById('itemAttr').style.display = 'block';
                document.getElementById('itemAttr').classList.toggle("hidden-xs", false);
                break;
        }
    }

    // navigate function
    redirect(page: any, orgType: any) {
        this._globalFunc.logger("Redirecting to "+page);
        this._router.navigate([page, orgType]);
    }
    // open in new tab for edit 

    redirect2Edit(page: any, orgType: any, itemNumber: any) {
        this.getUserSession(orgType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
        
        if(this.terminateFlg == false ){
            this._globalFunc.logger("Redirecting to edit catalogue details.");
            sessionStorage.setItem("pageState","999");
            this._router.navigate([page, orgType, this.catalogueType, this.catalogueId, this.productAnyIdentifier, itemNumber]);
            /*let url = this.homeUrl + page + '/' + encodeURIComponent(orgType) + '/' + this.catalogueType + '/' + this.catalogueId + '/' + this.productAnyIdentifier + '/' + itemNumber;
            window.open(url);*/
        }
        
    }


    // download functionality
    exportData() {
        this._globalFunc.logger("Export to excel.");
        return this._cfunction.exportData('exportable',this.customerName+'orderDetails');
    }

    // print functionality
    printOrder() {
        this._globalFunc.logger("Print catalogue details.");
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        var printContentsNav = document.getElementById('printNav').innerHTML;
        var printContents = document.getElementById('exportable').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContentsNav + printContents + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContentsNav + printContents + '</body></html>');
            popup.document.close();
        }
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
        this._globalFunc.logger("Unloading Customer selection screen.");
        this._globalFunc.sentLoggerBatchToServer();
    }

    // key press event to disable special charater
    keyPress(event: any) {
        this._globalFunc.alphaNumeric(event);
    }

    getSingleProductDetail(value: string) {
        this.showLoader = true;
        this._globalFunc.logger("product detail service.");
        this._postsService.getProductDetailsUsingItemId(value).subscribe(data => {
            this.showLoader = false;
            if (data.itemNumber != null && data.itemNumber != undefined) {
                this.product = data;
            }
            if (data.imageUrl != null) {
                if (data.imageUrl[0] != undefined) { this.imageUrl = data.imageUrl[0].url; }
            }
        }
            , err => {
                this._globalFunc.logger("product detail service failed.");
                this.showLoader = false;
                //this._router.navigate(['/error404']);
            }
        );
    }

    // format date function
    formatDate(createdAtDate: string) {
        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
            if (createdAtDate.indexOf('/') >= 0) {
                return createdAtDate;
            } else {
                var d = new Date(createdAtDate);
                var yyyy = d.getFullYear();
                var mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1); // getMonth() is zero-based
                var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
                // var hh = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
                // var min = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
                // var sec = d.getSeconds() < 10 ? "0" + d.getSeconds() : d.getSeconds();
                // var ms = d.getMilliseconds() < 10 ? "0" + d.getMilliseconds() : d.getMilliseconds();

                return dd + "/" + mm + "/" + yyyy;
                // return dd + "/" + mm + "/" + yyyy + "  " + hh + ":" + min + ":" + sec + ":" + ms;
            }
        } else {
            return '-';
        }
    }
    showProductDetail() {
        document.getElementById('prouctDetailTitle').style.display = 'block';
        document.getElementById('productDetailHeader').style.display = 'block';
        document.getElementById('productDetailImg').style.display = 'block';
        document.getElementById('itemCodeMapping').classList.toggle("hidden-xs", true);
        document.getElementById('itemPriceInfo').classList.toggle("hidden-xs", true);
        document.getElementById('itemAttr').classList.toggle("hidden-xs", true);

    }

    sortFunction(a: any, b: any) {
        if (a.id === b.id) {
            return 0;
        } else {
            return (a.id < b.id) ? -1 : 1;
        }
    }

    getCatalogueItemDetails() {
        this.showLoader = true;
        this._globalFunc.logger("product items service.");
        this._postsService.getSingleCatalogueItemDetails(this.orgApiName, this.catalogueType, this.catalogueId).subscribe(data => {
            this.showLoader = false;
            this.productDetails = data;
            this.productMaps = data.maps;
            this.productPrices = data.prices;
            this.productAttributes = data.lookups;
            var temp = new Array;
            if (data.maps.length > 0) {
                for (let k = 0; k < data.maps.length; k++) {
                    var dataMapType = JSON.stringify(data.maps[k].type).toUpperCase();
                    if (dataMapType == '"EAN"') {
                        var tempObjean = { 'id': 0, 'jdata': data.maps[k].values[0].value };
                        temp.push(tempObjean);
                    } else {
                        if (dataMapType == '"MIN"') {
                            var tempObjmin = { 'id': 1, 'jdata': data.maps[k].values[0].value };
                            temp.push(tempObjmin);
                        } else {
                            if (dataMapType == '"PIN"') {
                                var tempObjpin = { 'id': 2, 'jdata': data.maps[k].values[0].value };
                                temp.push(tempObjpin);
                            }
                        }
                    }
                }
                // this.productAnyIdentifier = data.maps[0].values[0].value;
            }
            console.log(temp);
            if (temp.length > 0) {
                var sorted = temp.sort(this.sortFunction);
                console.log(sorted);
                this.productAnyIdentifier = sorted[0].jdata;
            }
            this.getSingleProductDetail(this.productAnyIdentifier);
        }
            , err => {
                this._globalFunc.logger("product items service failed");
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
        );
    }

    getCustomerImage(custName:string){
        return this._globalFunc.getCustomerImage(custName);
    }

    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }
     getUserSession(customerName:any, loggedInUserGroup:any, loggedInUserId:any){
        this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData:any) => {
            if(!userData.isSessionActive) { // false
                // User session has expired.
                if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                    console.log("Session terminated");
                    this.terminateFlg = true;
                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                } 
                else{
                    sessionStorage.clear();
                    this._router.navigate(['']);
                }
            } else {
                if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                    console.log("Session terminated");
                   /*  this.msgJSON = {
                        "width314": true,
                        "title": "Session terminated",
                        "message": userData.sessionMessage,
                        "okButton": true,
                        "okButtonLabel": "OK",
                        "cancelButton": false,
                        "cancelButtonLabel": "",
                        "showPopup": true
                    }; */
/* 
                    let dt = this._cfunction.getUserSession();
                    this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
                        console.log(userData);
                    }, (error: any) => {
                        console.log(error);
                    }); */
                    this.terminateFlg = true;

                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                } else{
                    this.terminateFlg = false;
                    this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                    let changeOrgAvailable: any = [];
                    let orgImgUrl = "";
                    let orgApiNameTemp = "";
                    this.organisationList.forEach(function (item: any) {
                        if (item.customerName !== customerName) {
                            changeOrgAvailable.push(item);
                        } else {
                            orgImgUrl = item.customerName+".png";
                            orgApiNameTemp = item.customerName;
                        }
                    });
                    
                    this.orgimgUrl = orgImgUrl;
                    this.orgApiName = orgApiNameTemp;
    
                    console.log("this.organisationList234");
                    console.log(this.organisationList);
                    let that = this;
                    let orderEnquiry = true;
                    let claimsReview = true;
                    let catalogueManagementMenuPermission = false;
                    let editCatalogueEntry = false;
                    let catalogueEnquiry = false;
                    this.organisationList.forEach(function (organisation: any) {
                        
                        if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                            console.log("organisation : "+that.customerName);
                            console.log(organisation);
                            organisation.permissionRoleList.forEach(function(permRoleList: any){
                                console.log("11");
                                console.log(permRoleList);
                                permRoleList.permissionDetailsList.forEach(function(permRoleDtlList: any){
                                    console.log("22");
                                    if(permRoleDtlList.permissionGroup == 'catalogueManagement'){
                                        permRoleDtlList.permissionNameList.forEach(function(permList: any){
                                            if(permList.permissionName == 'catalogueManagement'){// && permList.permissionStatus == 'active'){
                                                catalogueManagementMenuPermission = true;
                                                console.log("catalogueManagement permission");
                                            }
    
                                            switch(permList.permissionName){
                                                case "editCatalogueEntry":
                                                    console.log("edit catalogue permission");
                                                    editCatalogueEntry = true;
                                                    //alert(filfilment);
                                                    break;
                                                case "catalogueEnquiry":
                                                    console.log("catalogue enquiry permission");
                                                    catalogueEnquiry = true;
                                                    //alert(filfilment);
                                                    break;
                                            }
                                        });
                                    }
                                });
                            });
                        }
                    });
                    that = null;
                    if(!catalogueEnquiry){ // catalogue management permission not given
                        // redirect to order
                        this._router.navigate(["orders",customerName]);
                    }
    
                    this.allowWrite = editCatalogueEntry;

                }
            }
            //alert(this.disabledfilfilment);
        });
    }
}


   