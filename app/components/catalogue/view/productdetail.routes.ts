import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductDetailComponent } from './productdetail.component';

// route Configuration
const productdetailRoutes: Routes = [
  { path: '', component: ProductDetailComponent },
  { path: ':anyIdentifier/:itemNumber', loadChildren: './edit/editproduct.module#EditProductModule' }
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(productdetailRoutes)],
  exports: [RouterModule]
})
export class ProductDetailComponentRoutes { }
