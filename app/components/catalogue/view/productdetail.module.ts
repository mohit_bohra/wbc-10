import { NgModule } from '@angular/core';
import { ProductDetailComponentRoutes } from './productdetail.routes';
import { ProductDetailComponent } from './productdetail.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';

// import { DatepickerModule } from 'ng2-bootstrap';
import { DatepickerModule } from '../../datepicker/ng2-bootstrap';
@NgModule({
  imports: [
    ProductDetailComponentRoutes,
    CommonModule,
    FormsModule,
    DatepickerModule.forRoot()

  ],
  exports: [],
  declarations: [ProductDetailComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class ProductDetailModule { }
