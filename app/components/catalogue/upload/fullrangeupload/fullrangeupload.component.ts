import { Component } from '@angular/core';
// import { Http, Headers, Jsonp, BaseRequestOptions, XHRBackend } from '@angular/http';
// import { Observable } from 'rxjs/Rx';
import { Services } from '../../../../services/app.services';
import { GlobalComponent } from '../../../global/global.component';
import { Constant } from '../../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
//import { Ng2FileInputService, Ng2FileInputAction } from 'ng2-file-input';
import { Ng2FileInputService, Ng2FileInputAction } from '../../../ng2-file-input/dist/ng2-file-input';
import { commonfunction } from '../../../../services/commonfunction.services';


@Component({
    templateUrl: 'fullrangeupload.component.html',
    providers: [commonfunction]
})
export class FullRangeUploadComponent {
    file: File;
    s3SignedURL: string;
    public catalgoueDate: Date = new Date();
    objConstant = new Constant();
    whoImgUrl: any;
    catalogueCode: any;
    catalogueCodeSelected: any;
    catalogueList: any;
    catalogueListLength: any;
    catalogueUpdatedDate: any;
    catalogueno: string;
    showLoader: boolean;
    hideOrganisation: boolean = true;
    catalogueResult: boolean = true;
    printHeader: boolean = true;
    pageState: number = 19;
    customerName: string;
    customerDispayName: string;
    style: string;
    organisationList: any;
    imgName: string = "";
    organisationListChange: any;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disabledOrder: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    private _sub: any;

    //for catalogue Enquiry redevelopment
    catalogueTypes: any;
    catalogueType: string;
    cataloguemasterListLength: any;
    cataloguedetailListLength: any;
    catalogueDetailList: any;
    strInfoNotAvail: String;
    start: number;
    limit: number;
    count: number;
    hitNext: boolean;
    newBatchNo: number;
    fullRangeUploadMsgBox: boolean;
    MsgTitle: String;
    fullRangeUploadMsgBoxMsg: String;
    allowedMimeType: string[];
    uploadedFileName: String;
    fileuploadStatus: boolean = false;


    disablecatalogueBulkUploadView: boolean = true;
    disablecatalogueBulkUploadSubmit: boolean = true;


    msgJSON: any = {};
    terminateMsg: any;
    terminateFlg: boolean = false;
    btndisable:boolean = false;

    public todayDate: Date = new Date();

    homeUrl: any;
    public myFileInputIdentifier: string = "upload_catalogue";
    
    //bolt changes
    catalogTypes : string;
    selectedFileType : string;

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, private ng2FileInputService: Ng2FileInputService, public _cfunction: commonfunction) {
        // img urls
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
    }


    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    // function works on page load
    ngOnInit() {
        this._globalFunc.logger("Loading catalogue upload file screen.");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {
            this.uploadedFileName = "";
            this.allowedMimeType = ['text/comma-separated-values', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.ms-excel', 'application/vnd.msexcel'];
            this.showLoader = false;
            this.catalogueno = '';
            // user permission
            this.disabledCusChange = false;
            this.disabledPermission = false;
            this.disabledReports = false;
            this.disabledCatalogue = false;
            this.disabledOrder = false;
            this.disabledOrderRaise = false;
            this.disabledCatalogueRaise = false;
            this.disabledManageCatalogue = false;
            this.strInfoNotAvail = "-";
            this.catalogueType = "";
            this.newBatchNo = 0;
            this.fullRangeUploadMsgBox = false;
            this.MsgTitle = "Batch Number";
            this.fullRangeUploadMsgBoxMsg = "File uploaded successfully";
            this.file = null;

            // window scroll function
            this._globalFunc.autoscroll();

            // set header basket data
            this.pageState = 19;
            this.start = 0;
            this.limit = 20;// this.objConstant.SCROLL_RECORD_LIMIT;
            this.count = 0;
            this.hitNext = false;
            sessionStorage.setItem('pageState', '19');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }

                if(sessionStorage.getItem('orgType') === 'amazon' && sessionStorage.getItem('catalogueType')!= 'Core'){
                    this.selectedFileType = sessionStorage.getItem('catalogueType');
                    //console.log("here");
                }
                else{
                    this.selectedFileType = 'catalogue';
                    //console.log("there");
                }

                this.selectedFileType = this._globalFunc.titleCase(this.selectedFileType);

                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                let reports;
                let orders;
                let catalogues;
                let cataloguesRaise;
                let accessMgmt;
                let redirct;
                let changeOrgAvailable = new Array;
                let catManagement;



                this.imgName = this.getCustomerImage(params.orgType);
                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));


                // list of org assigned to user
                /*  this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));
  
                  this.organisationList.forEach(function (item: any) {
                      if (item.orgType !== cusType) {
                          changeOrgAvailable.push(item);
                      }
                  });
                  this.organisationListChange = changeOrgAvailable;
                  // this.disabledCusChange = false;
                  if (this.organisationList.length == 1) {
                      this.disabledCusChange = true;
                  }*/

                this.orgimgUrl = orgImgUrl;
                this.orgApiName = this.customerName;

                this.imgName = this.getCustomerImage(params.orgType);

                document.getElementById('orderArrowSpan').style.display = 'block';
                document.getElementById('orderMob').style.display = 'block';
                document.getElementById('orderRaiseMob').style.display = 'none';

                document.getElementById('catalogueMob').style.display = 'block';
                document.getElementById('catalogueArrowSpan').style.display = 'none';
                document.getElementById('catalogueRaiseMob').style.display = 'none';
                document.getElementById('reportsMob').style.display = 'block';
                document.getElementById('permissonsMob').style.display = 'block';

                if (document.getElementById('orderArrow').classList.contains('glyphicon-menu-up') === true) {
                    let sublinks = document.getElementById('orderArrow');
                    sublinks.classList.remove('glyphicon-menu-up');
                    sublinks.classList.add('glyphicon-menu-down');
                    document.getElementById('orderRaiseMob').style.display = 'none';
                }

                if (document.getElementById('catalogueArrow').classList.contains('glyphicon-menu-up') === true) {
                    let sublinks = document.getElementById('catalogueArrow');
                    sublinks.classList.remove('glyphicon-menu-up');
                    sublinks.classList.add('glyphicon-menu-down');
                    document.getElementById('catalogueRaiseMob').style.display = 'none';
                }


                /* this.organisationList.forEach(function (item: any) {
                     if (item.orgType === cusType) {
                         // logic for permission set
                         orgImgUrl = item.imgUrl;
                         orgApiNameTemp = item.orgName;
 
                         if ((item.permissions.menuPermission.orders).toLowerCase() == 'none') {
                             orders = true;
                             document.getElementById('orderMob').style.display = 'none';
                             document.getElementById('orderRaiseMob').style.display = 'none';
                         }
 
                         if ((item.permissions.menuPermission.raiseNewOrder).toLowerCase() == 'none') {
                             document.getElementById('orderArrowSpan').style.display = 'none';
                             document.getElementById('orderRaiseMob').style.display = 'none';
 
                         }
 
                         if ((item.permissions.menuPermission.catalogueManagement).toLowerCase() == 'none') {
                             catManagement = true;
                         } else {
                             catManagement = false;
                         }
 
 
                         if ((item.permissions.menuPermission.catalogueEnquiry).toLowerCase() == 'none') {
 
                             catalogues = true;
                             document.getElementById('catalogueMob').style.display = 'none';
                             document.getElementById('catalogueRaiseMob').style.display = 'none';
                             //redirct = 1;
                             if (sessionStorage.getItem('page_redirect') == '0') {
                                 sessionStorage.removeItem('page_redirect');
                             } else {
                                 sessionStorage.setItem('page_redirect', '1');
                             }
 
 
                         } else {
                             // write logic to show add new product
 
                             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                         }
 
                         if ((item.permissions.catalogAdminPermission.addNewProducts).toLowerCase() == 'none') {
                             cataloguesRaise = true;
                             document.getElementById('catalogueArrowSpan').style.display = 'none';
                             document.getElementById('catalogueRaiseMob').style.display = 'none';
 
 
                         } else {
                             // write logic to show add new product
 
                             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                         }
 
                         if ((item.permissions.menuPermission.reports).toLowerCase() == 'none') {
                             reports = true;
                             document.getElementById('reportsMob').style.display = 'none';
 
                         }
 
                         if ((item.permissions.menuPermission.accessMgmt).toLowerCase() == 'none') {
                             accessMgmt = true;
                             document.getElementById('permissonsMob').style.display = 'none';
                         }
 
                     }
                 });*/

                if (redirct == 1) {
                    redirct = 0;
                    this.redirect('/reports', this.customerName);
                }

                this.orgimgUrl = orgImgUrl;
                this.orgApiName = this.customerName;
                this.disabledReports = reports;
                this.disabledOrder = orders;
                this.disabledOrderRaise = orders;
                this.disabledCatalogue = catalogues;
                this.disabledCatalogueRaise = cataloguesRaise;
                this.disabledPermission = accessMgmt;
                this.disabledManageCatalogue = catManagement;
                //  this.getAllCatalogues();
                //this.getAllMasterData();
                //this.createS3SignedUrl();
            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);

            // logic for resize pop up 
            window.onresize = () => {
                if (screen == "desktop") {

                    if (window.innerWidth < this._globalFunc.stdWidth) {
                        // this.catalogueResult = false;

                    } else {
                        //  this.catalogueResult = true;
                    }
                } else {

                    if (window.innerWidth > this._globalFunc.stdWidth) {
                        //   this.catalogueResult = true;
                        document.getElementById("resultModalClose").click();
                    } else {
                        //  this.catalogueResult = false;

                    }
                }

                // get device to populate some part of ui
                screen = this._globalFunc.getDevice(window.innerWidth);

            };
            /*window.onscroll = () => {
                this.scrollFunction();
        };*/
        this.checkTime();
        }
    }

    /* public createS3SignedUrl() {

        this._postsService.getSignedURL(this.orgApiName).subscribe(data => {
            console.log(data);
            this.s3SignedURL = data.preSignedS3Url;
            this.showLoader = false;
        }, err => {
            this.showLoader = false;
            this._router.navigate(['/error404']);
        }
        );
    } */

    public onAdded(event: any) {
        this._globalFunc.logger("File added to placeholder and array.");
        console.log(event);
        console.log(this.s3SignedURL);
        let fileList: FileList = event.currentFiles;
        console.log(FileList);
        if (fileList.length > 0) {
            let file: File = fileList[0];
            this.file = file;
            let fileArr = this.file.name.split('.');
            console.log("Array Count : " + (fileArr.length - 1));
            if (fileArr[fileArr.length - 1].toLowerCase() != 'csv') {
                // Invalid file
                this.fullRangeUploadMsgBox = true;
                this.MsgTitle = "Error";
                this.fullRangeUploadMsgBoxMsg = "Please select CSV File only.";

                document.getElementById('fullRangeUploadBox').style.display = "block";
                console.log("Position : " + this.allowedMimeType.indexOf(this.file.type));
                this.ng2FileInputService.reset(this.myFileInputIdentifier);
                this.fileuploadStatus = false;
            } else {
                // Check File Size
                if (this.file.size > 200000000) {
                    // Invalid file
                    this.fullRangeUploadMsgBox = true;
                    this.MsgTitle = "Error";
                    this.fullRangeUploadMsgBoxMsg = "Please select file of size not more than 200mb.";
                    document.getElementById('fullRangeUploadBox').style.display = "block";
                    console.log("Position : " + this.allowedMimeType.indexOf(this.file.type));
                    this.ng2FileInputService.reset(this.myFileInputIdentifier);
                    this.fileuploadStatus = false;
                } else {
                    this.uploadedFileName = file.name;
                }

            }
        }
    }

    public onRemoved(event: any) {
        this._globalFunc.logger("File removed to placeholder and array.");
        console.log(event);
        console.log("this.uploadedFileName : " + this.uploadedFileName);
        this.uploadedFileName = "";
        delete (this.file);
    }
    public getFileNames(files: File[]): string {
        let names = files.map(file => file.name);
        return names ? names.join(", ") : "No files currently added.";
    }

    async sleep(ms: number) {
        await this._sleep(ms);
    }

    _sleep(ms: number) {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }

    upload(filenameui: any) {
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {            

            if(!this.disablecatalogueBulkUploadSubmit){           

                    if (res == 'sessionActive') {
                        let fileList = this.ng2FileInputService.getCurrentFiles(this.myFileInputIdentifier);
                        console.log(fileList);
                        console.log("this.uploadedFileName : " + this.uploadedFileName.length);
                        this.showLoader = true;
                        if(sessionStorage.getItem('orgType') === 'amazon' && sessionStorage.getItem('catalogueType')!= 'Core'){
                            this.catalogTypes = sessionStorage.getItem('catalogueType');
                            //console.log("here");
                        }
                        else{
                            this.catalogTypes = 'catalogue';
                            //console.log("there");
                        }

                        this.catalogTypes = this.catalogTypes.toLowerCase();

                        //if (this.uploadedFileName.length != 0) {
                        if (fileList.length != 0) {
                            this._globalFunc.logger("get presigned url service.");
                            this._postsService.getSignedURL(this.orgApiName, filenameui,this.catalogTypes).subscribe((data:any) => {
                                console.log(data.uploadURL);
                                console.log(data.type);
                                console.log(filenameui);
                                console.log(data.validUpto);
                                this.s3SignedURL = data.uploadURL;
                                //this.s3SignedURL = data.preSignedS3Url;
                                console.log("uploading...new");

                                let trackerStr: any;

                                trackerStr = '{"username": "' + sessionStorage.getItem('username') + '","customerId": "' + this.orgApiName + '","type": "'+ this.catalogTypes +'","orignalFileName": "' + filenameui + '","uploadedFileName": "' + data.fileName + '","status": "Validation Pending","uploadedFilePath": "wholesaleinboundcatalogue/processing/' + data.fileName + '","errorFilePath": "","eventCreatedBy": "' + sessionStorage.getItem('username') + '"}';

                                console.log(trackerStr);

                                

                                this._globalFunc.logger("Put file to S3.");
                                this._postsService.putFileToS3(this.s3SignedURL, this.file).subscribe((data:any) => {
                                    this._postsService.putdataincatalogueTracker(trackerStr, this.orgApiName).subscribe(data => { });
                                    console.log(data);
                                    this.showLoader = false;
                                    console.log('file uploaded');
                                    this.fullRangeUploadMsgBox = true;
                                    this.MsgTitle = "File uploaded";
                                    this.fullRangeUploadMsgBoxMsg = "File uploaded successfully.";
                                    document.getElementById('fullRangeUploadBox').style.display = "block";
                                    this.ng2FileInputService.reset(this.myFileInputIdentifier);
                                    this.uploadedFileName = "";
                                    this.fileuploadStatus = true;
                                    // this._router.navigate(['/catalogue', this.customerName, 'upload']);
                                }, (err:any) => {
                                    this._globalFunc.logger("Put file to S3 failed.");
                                    this.showLoader = false;
                                    //this._router.navigate(['/error404']);
                                    console.log("Error :");
                                    this.fullRangeUploadMsgBox = true;
                                    this.MsgTitle = "Error!";
                                    this.fullRangeUploadMsgBoxMsg = "Service temperory not available.";
                                    document.getElementById('fullRangeUploadBox').style.display = "block";
                                    this.fileuploadStatus = false;



                                    //console.log(err);
                                });
                                //this.showLoader = true;
                                /*var xhr = new XMLHttpRequest ();
                                xhr.open('PUT', this.s3SignedURL, true);
                                xhr.setRequestHeader('Content-Type','multipart/form-data');
                                xhr.send(this.file);
                                xhr.onload = () => {
                                    if (xhr.status == 200) {
                                        this.showLoader = false;
                                        console.log('file uploaded');
                                        this.fullRangeUploadMsgBox = true;
                                        this.MsgTitle = "File uploaded";
                                        this.fullRangeUploadMsgBoxMsg="File uploaded successfully.";
                                        document.getElementById('fullRangeUploadBox').style.display="block";
                                        this.ng2FileInputService.reset(this.myFileInputIdentifier);
                                        this.uploadedFileName = "";
                                    }
                        }*/
                                //this.showLoader = false;
                            }, (err:any) => {
                                this._globalFunc.logger("get presigned url service Failed.");
                                this.showLoader = false;
                                //this._router.navigate(['/error404']);
                                console.log("Error :");
                                this.fullRangeUploadMsgBox = true;
                                this.fileuploadStatus = false;
                                this.MsgTitle = "Error!";
                                this.fullRangeUploadMsgBoxMsg = "Service temperory not available.";
                                document.getElementById('fullRangeUploadBox').style.display = "block";
                                let trackerStr: any;

                            });
                        } else {
                            this.showLoader = false;
                            console.log("Error...");
                            this.fullRangeUploadMsgBox = true;
                            this.MsgTitle = "Error";
                            this.fullRangeUploadMsgBoxMsg = "Please select CSV File to upload.";
                            this.fileuploadStatus = false;

                            document.getElementById('fullRangeUploadBox').style.display = "block";
                            //console.log("Position : "+this.allowedMimeType.indexOf(this.file.type));
                        }
                    } else{
                        console.log(res);
                    }
            }
        }, reason =>{
            console.log(reason);
        });


    }

    onChange(event: any) {
        console.log("File Selected");
        console.log(event);
        var files = event.srcElement.files;
        this.file = files[0];
        console.log("File object created");
        console.log(this.file);
    }

    onScrolltop() {
        this._globalFunc.autoscroll();
    }
    checkTime(){
    var today = new Date().getHours(); 
    var min=new Date().getMinutes() ;
    var time:any
    time  = today+"."+min
      if (time >= 19.45 && time<=0.30) {
          this.btndisable = true;
    
      }
   /*  var currentTime = new Date();

     var currentOffset = currentTime.getTimezoneOffset();

     var ISTOffset = 330;   // IST offset UTC +5:30 

     var ISTTime = new Date(currentTime.getTime() + (ISTOffset + currentOffset)*60000);

     // ISTTime now represents the time in IST coordinates

    var hoursIST = ISTTime.getHours()
     var minutesIST = ISTTime.getMinutes()

     time= hoursIST + "." + minutesIST 
   
    
     if (time >= 16.45 && time<=17.00) {
         this.btndisable = true;
   }*/
     else{
     this.btndisable = false;
     }
    }
    /*scrollFunction() {
       if (document.body.scrollTop > 800 || document.documentElement.scrollTop > 800) {
            document.getElementById("topBtn").style.display = "block";
        } else {
            document.getElementById("topBtn").style.display = "none";
    }
    }*/

    resetOnClose(status: any) {
        this.fullRangeUploadMsgBox = false;
        document.getElementById('fullRangeUploadBox').style.display = "none";
        this.file = null;
        if (status == true) {
            this._router.navigate(['/catalogue', this.customerName, 'upload']);
        }

    }

    onScrollDown(catalogueCode: any, catalogueType: any) {

        this.start = this.start + this.limit;

        if (this.count == this.limit && this.hitNext == true) {

            this.showLoader = true;

            this._postsService.getCatalogueItem(this.orgApiName, catalogueCode, catalogueType, this.start, this.limit).subscribe(data => {

                this.showLoader = false;

                this.count = data.paginationMetaData.count;
                this.catalogueDetailList = this.catalogueDetailList.concat(data.items);


            }
                , err => {
                    this.catalogueno = '';
                    this.showLoader = false;
                    this.cataloguedetailListLength = 0;
                    this._router.navigate(['/error404']);
                }
            );

        }

    }
    redirect_toList(status: any) {

        if (status == true) {
            this._router.navigate(['/catalogue', this.customerName, 'upload']);
        } else {
            this.resetOnClose(status);
        }
    }
    // navigate function
    redirect(page: any, orgType: any) {
        this._globalFunc.logger("Redirect to " + page);
        if (page == 'addproduct') {
            let url = this.homeUrl + page + '/' + encodeURIComponent(orgType);
            window.open(url);
        } else {
            this._router.navigate([page, orgType]);
        }
    }
    // change organisation
    changeOrgType(page: any, orgType: any) {

        // reset field on change of organisation 
        this.catalogueno = '';
        this.catalogueList = [];
        this.catalogueListLength = 0;
        this.catalogueResult = true;
        this.catalogueType = '';
        this.catalogueCode = '';
        this.hitNext = false;
        this.start = 0;
        this.count = 0;
        this.toggleOrganisation();
        this._router.navigate([page, orgType]);


    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
        this._globalFunc.logger("Unloading Customer selection screen.");
        this._globalFunc.sentLoggerBatchToServer();
    }

    // change organisation div show and hide
    toggleOrganisation() {
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
            border.classList.remove('searchborder');
        }
        else {

            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
            border.classList.add('searchborder');

        }
    }

    // close operation of pop up
    closeOperation() {

        this.catalogueList = '';

    }

    // key press event to disable special charater
    keyPress(event: any) {
        this._globalFunc.alphaNumericWithSpace(event);
    }


    // get all catalogue detail
    getCatalogueItem(value: any) {
        let str = value.catalogueType;
        let searchTerm = '';
        if (str.search(':') != -1) {
            // console.log("adf");
            var strArr = str.split(':');
            searchTerm = strArr[1];
        } else {
            searchTerm = value.catalogueType;
        }



        this.showLoader = true;
        this.start = 0;
        this.count = 0;
        // console.log(value);
        this._postsService.getCatalogueItem(this.orgApiName, value.catalogueCode, searchTerm, this.start, this.limit).subscribe(data => {

            this.showLoader = false;
            this.catalogueCodeSelected = searchTerm;
            this.start = 0;
            this.count = 0;
            this.catalogueDetailList = [];
            if (data.items == undefined) {
                this.cataloguedetailListLength = 0;
            } else {
                this.hitNext = true;
                this.cataloguedetailListLength = data.items.length;
                this.count = data.paginationMetaData.count;
            }
            this.catalogueDetailList = data.items;

            if (window.innerWidth < this._globalFunc.stdWidth) {
                document.getElementById("openModalResult").click();
            } else {
                this.catalogueResult = false;
            }
        }
            , err => {
                this.catalogueno = '';
                this.showLoader = false;
                this.cataloguedetailListLength = 0;
                this._router.navigate(['/error404']);
            }
        );


    }


    // get relevant order detail
    getAllMasterData() {

        this.showLoader = true;
        this._postsService.getAllMasterData(this.orgApiName).subscribe(data => {
            this.showLoader = false;

            if (data.catalogueTypes.length == 0) {
                this.cataloguemasterListLength = 0;
            } else {
                for (var k = 0; k < data.catalogueTypes.length; k++) {
                    if (data.catalogueTypes[k].catalogueType.indexOf(':') > 0) {
                        data.catalogueTypes[k].catalogueType = data.catalogueTypes[k].catalogueType.split(':')[1];
                    }
                }
                this.cataloguemasterListLength = data.catalogueTypes.length;


            }
            this.catalogueTypes = data.catalogueTypes;

        }
            , err => {
                this.cataloguemasterListLength = 0;
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
        );
    }

    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {
            console.log("i m first");
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
             if (!userData.isSessionActive) { // false
                    // User session has expired.
                if(userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser){
                    console.log("Session terminated");
                    this.terminateFlg = true;
                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                }
                else{
                    sessionStorage.clear();
                    this._router.navigate(['']);
                    reject('sessionInactive');
                 }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        console.log("Session terminated");
                        this.terminateFlg = true;
                        reject('sessionInactive');

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {

                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
                        this.organisationListChange = changeOrgAvailable;
                        this.orgimgUrl = orgImgUrl;
                        this.orgApiName = orgApiNameTemp;
                        // this.disabledCusChange = false;
                        if (this.organisationList.length == 1) {
                            this.disabledCusChange = true;
                        }

                        console.log("this.organisationList234");
                        console.log(this.organisationList);

                        let catalogueBulkUploadView = true;
                        let catalogueBulkUploadSubmit = true;
                        let that = this;
                        let orderEnquiry = true;
                        let claimsReview = true;
                        let dashboardMenuPermission = false;
                        let userHavingPermission = false;
                        this.organisationList.forEach(function (organisation: any) {

                            if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                                console.log("organisation : " + that.customerName);
                                console.log(organisation);
                                organisation.permissionRoleList.forEach(function (permRoleList: any) {
                                    console.log("11");
                                    console.log(permRoleList);
                                    permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                        console.log("22");
                                        if (permRoleDtlList.permissionGroup == 'catalogueManagement') {
                                            userHavingPermission = true;
                                            permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                                if (permList.permissionName == 'catalogueManagement' && permList.permissionStatus == 'active') {
                                                    dashboardMenuPermission = true;
                                                }
                                                console.log(permList);

                                                switch (permList.permissionName) {
                                                    case "catalogueBulkUploadView":
                                                        //console.log("55");
                                                        catalogueBulkUploadView = false;
                                                        /* permArrayWholesaleCatalogues.push({ "catalogueBulkUploadView": true }); */
                                                        //alert(filfilment);
                                                        break;
                                                    case "catalogueBulkUploadSubmit":
                                                        //console.log("55");
                                                        catalogueBulkUploadSubmit = false;
                                                        //alert(filfilment);
                                                        break;

                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                        that = null;
                        if (!userHavingPermission) {
                            // redirect to order
                            // catalogue/mccolls/upload
                            this._router.navigate(["catalogue", customerName, 'upload']);
                        }

                        this.disablecatalogueBulkUploadView = catalogueBulkUploadView;
                        this.disablecatalogueBulkUploadSubmit = catalogueBulkUploadSubmit;
                        resolve('sessionActive');
                    }
                }
                //alert(this.disabledfilfilment);
            });
        });
    }

    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }
}

