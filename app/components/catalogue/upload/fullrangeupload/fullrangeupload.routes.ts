import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FullRangeUploadComponent } from './fullrangeupload.component';

// route Configuration
const fullrangeuploadRoutes: Routes = [
  { path: '', component: FullRangeUploadComponent },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(fullrangeuploadRoutes)],
  exports: [RouterModule]
})
export class FullRangeUploadComponentRoutes { }
