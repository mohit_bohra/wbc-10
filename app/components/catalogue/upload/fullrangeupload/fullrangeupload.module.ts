import { NgModule } from '@angular/core';
import { FullRangeUploadComponentRoutes } from './fullrangeupload.routes';
import { FullRangeUploadComponent } from './fullrangeupload.component';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../../global/global.component';
import { Services } from '../../../../services/app.services';
import { FormsModule } from '@angular/forms';

//import { Ng2FileInputModule } from 'ng2-file-input';
import { CatalogueManagePendingHeaderModule } from '../../../cataloguemanagement/cataloguemanagependingheader/cataloguemanagependingheader.module';
import { Ng2FileInputModule } from '../../../ng2-file-input/dist/ng2-file-input';


@NgModule({
  imports: [
    FullRangeUploadComponentRoutes,
    CommonModule,
    FormsModule,
    InfiniteScrollModule,
    Ng2FileInputModule.forRoot(),
    CatalogueManagePendingHeaderModule

  ],
  exports: [],
  declarations: [FullRangeUploadComponent],
  providers: [Services, GlobalComponent]
})
export class FullRangeUploadModule { }
