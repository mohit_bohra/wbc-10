import { NgModule } from '@angular/core';
import { CatalogueuploadComponentRoutes } from './catalogueupload.routes';
import { CatalogueuploadComponent } from './catalogueupload.component';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { FormsModule } from '@angular/forms';
import { CatalogueManagePendingHeaderModule } from '../../cataloguemanagement/cataloguemanagependingheader/cataloguemanagependingheader.module';

@NgModule({
  imports: [
    CatalogueuploadComponentRoutes,
    CommonModule,
    FormsModule,
    InfiniteScrollModule,
    CatalogueManagePendingHeaderModule
  ],
  exports: [],
  declarations: [CatalogueuploadComponent],
  providers: [Services, GlobalComponent]
})
export class CatalogueuploadModule { }
