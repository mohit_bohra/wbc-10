import { Component } from '@angular/core';
import { Services } from '../../../services/app.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { commonfunction } from '../../../services/commonfunction.services';

@Component({
    selector: 'catalogueupload',
    templateUrl: 'catalogueupload.component.html',
     providers: [commonfunction]
})
export class CatalogueuploadComponent {
    public catalgoueDate: Date = new Date();
    objConstant = new Constant();
    errorMsgString: string;
    showErrorMsg: boolean;
    whoImgUrl: any;
    catalogueCode: any;
    catalogueCodeSelected: any;
    catalogueList: any;
    catalogueListLength: any;
    catalogueUpdatedDate: any;
    catalogueno: string;
    showLoader: boolean;
    hideOrganisation: boolean = true;
    printHeader: boolean = true;
    pageState: number = 16;
    customerName: string;
    customerDispayName: string;
    style: string;
    organisationList: any;
    organisationListChange: any;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disabledOrder: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    private _sub: any;
    disabledExportCatalogue: boolean;
    disabledUploadCatalogue: boolean;
    disabledDashboard: boolean = false;
    succMSg: string;

    //for catalogue Enquiry redevelopment
    catalogueTypes: any;
    catalogueType: string;
    cataloguemasterListLength: any;
    catalogueFileListLength: any;
    catalogueFileList: any;
    strInfoNotAvail: string;
    start: number;
    limit: number;
    count: number;
    hitNext: boolean;
    catalogueFilename: string;
    custPermission: any[];
    serviceErrorMsg: string;
    s3path: any;

    public todayDate: Date = new Date();

    homeUrl: any;
    imgName: string = "";
    
    disablecatalogueBulkUploadView: boolean = true;
    disablecatalogueBulkUploadSubmit: boolean = true;

    
    
    msgJSON: any = {};
    terminateMsg: any;
    terminateFlg: boolean = false;

    //bolt changes
    isAmazonCatelogType: boolean = false;
    isAmazonCatelogData: boolean = false;

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, public _cfunction: commonfunction) {
        // img urls
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.s3path = this.objConstant.s3path;
    }

    // function works on page load
    ngOnInit() {
        this._globalFunc.logger("Loading Catalogue uploaded listing screen");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {
            this.showLoader = false;
            this.catalogueno = '';
            this.showErrorMsg = false;
            // user permission
            this.disabledCusChange = false;
            this.disabledPermission = false;
            this.disabledReports = false;
            this.disabledCatalogue = false;
            this.disabledOrder = false;
            this.disabledOrderRaise = false;
            this.disabledCatalogueRaise = false;
            this.disabledManageCatalogue = false;
            this.disabledExportCatalogue = false;
            this.disabledUploadCatalogue = false;
            this.strInfoNotAvail = "-";
            this.catalogueType = "";
            this.succMSg = this.strInfoNotAvail;

            // window scroll function
            this._globalFunc.autoscroll();

            // set header basket data
            this.pageState = 16;
            this.start = 0;
            this.limit = this.objConstant.SCROLL_RECORD_LIMIT;
            this.count = 0;
            this.hitNext = false;
            sessionStorage.setItem('pageState', '16');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }

                // bolt changes
                if (this.customerName === 'amazon') {                   
                    this.isAmazonCatelogType = true;
                    this.isAmazonCatelogData = false;
                }
                else{
                    this.isAmazonCatelogType = false;
                    this.isAmazonCatelogData = true;
                }

                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;

                this.imgName = this.getCustomerImage(params.orgType);
                let that = this;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = this.customerName;
                this.customerName = params.orgType;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                this.orgApiName = this.customerName;

                let custPerm: any = [];
                
                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
                if (res == 'sessionActive') {
                    this.imgName = this.getCustomerImage(params.orgType);
                    this.custPermission = custPerm;
                }
                let permissions = JSON.parse(sessionStorage.getItem('tempPermissionArray'));
                /* if(permissions[this.customerName]['catalogueManagement'].indexOf('storeBulkUploadSubmit')== -1){
                    this.disableStoreCatalogueFileSubmit = true;
                }else{
                    this.disableStoreCatalogueFileSubmit = false;
                } */
                }, reason => {
                    console.log(reason);
                });
                this.getlistofUploadedCatalogeFiles();

                //bolt changes
                this.getAllMasterData();

            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);

            // logic for resize pop up 
            window.onresize = () => {
                if (screen == "desktop") {
                    if (window.innerWidth < this._globalFunc.stdWidth) {
                        // this.catalogueResult = false;
                    } else {
                        //  this.catalogueResult = true;
                    }
                } else {
                    if (window.innerWidth > this._globalFunc.stdWidth) {
                        //   this.catalogueResult = true;
                        document.getElementById("resultModalClose").click();
                    } else {
                        //  this.catalogueResult = false;
                    }
                }

                // get device to populate some part of ui
                screen = this._globalFunc.getDevice(window.innerWidth);
            };
            window.onscroll = () => {
                //this.scrollFunction();
            };
        }
    }

    onScrolltop() {
        this._globalFunc.autoscroll();
    }

    scrollFunction() {
        if (document.body.scrollTop > 800 || document.documentElement.scrollTop > 800) {
            document.getElementById("topBtn").style.display = "block";
        } else {
            document.getElementById("topBtn").style.display = "none";
        }
    }

    // navigate function
    redirect(page: any, orgType: any) {
        this._globalFunc.logger("redirect to "+page);
        // console.log(page);
        // let url;
        if (page == 'addproduct') {
            //url = this.homeUrl + 'catalogue/' + encodeURIComponent(orgType) + '/add';
            this._router.navigate(['catalogue', encodeURIComponent(orgType), 'add']);
            //this._router.navigate(url);
        } else if (page == 'catalogueexport') {
            //url = this.homeUrl + 'catalogue/' + encodeURIComponent(orgType) + '/export';
            this._router.navigate(['catalogue', encodeURIComponent(orgType), 'export']);
        } else if (page == 'catalogueupload') {
            //url = this.homeUrl + 'catalogue/' + encodeURIComponent(orgType) + '/upload';
            this._router.navigate(['catalogue', encodeURIComponent(orgType), 'upload']);
        } else if (page == '/fullrangeupload') {
            //url = this.homeUrl + 'catalogue/' + encodeURIComponent(orgType) + '/upload';
            this._router.navigate(['catalogue', encodeURIComponent(orgType), 'upload', 'fullrangeupload']);
        } else {
            this._router.navigate([page, orgType]);
        }
    }

    // change organisation
    changeOrgType(page: any, orgType: any) {
        this._globalFunc.logger("Change customer to "+orgType);
        // reset field on change of organisation 
        this.hitNext = false;
        this.start = 0;
        this.count = 0;
        this.toggleOrganisation();
        this._router.navigate(['catalogue', orgType, 'upload']);
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
        this._globalFunc.logger("Unloading catalogue export screen.");
        this._globalFunc.sentLoggerBatchToServer();
    }

    // change organisation div show and hide
    toggleOrganisation() {
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
            border.classList.remove('searchborder');
        }
        else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
            border.classList.add('searchborder');

        }
    }

    // close operation of pop up
    closeOperation() {
        this.catalogueList = '';
    }

    fileDownload(filepath: any) {
        var downloadpath = this.s3path;
        window.open(downloadpath + filepath);
    }

    fileDownloadUsingPresignedURL(filename: string, filepath: string) {
        console.log(filename);
        console.log(filepath);
        this._globalFunc.logger("Download file using presigned url");
        this._postsService.fileReportDownloadUsingPresignedURL(this.orgApiName, filename, 'catalogue', 'download',"", filepath).subscribe(data => {
            console.log(data.preSignedS3Url);

            window.open(data.preSignedS3Url);
        }, error => {
            this._globalFunc.logger("Error while Downloading file using presigned url");
            this.succMSg = filename + " doesnot exist.";
            let sessionModal = document.getElementById('addConfirmationModal');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
        });
    }

    closePopup() {
        let sessionModal = document.getElementById('addConfirmationModal');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('addConfirmationModal').style.display = 'none';
    }

    onScrollDown() {
        console.log('++++++++++++++');
        this.start = this.start + this.limit;
        console.log(this.start + " " + this.count);        
        console.log('++++++++++++++');
        
       // if (this.count == this.limit && this.hitNext == true) {
          if(this.start < this.count) {
            this.showLoader = true;
            this._globalFunc.logger("Get list of uploaded catalogue service on scroll");
            this._postsService.getlistofUploadedCatalogeFiles(this.orgApiName, this.start, this.limit).subscribe(data => {
                this.showLoader = false;
                this.count = data.paginationMetaData.count;
                this.start = this.limit;
                this.limit = ((this.count-this.limit) > this.limit)  ? this.limit : (this.count-this.limit);
                this.catalogueFileList = this.catalogueFileList.concat(data.auditDetails);
            }
                , err => {
                    this._globalFunc.logger("Get list of uploaded catalogue service Failed");
                    this.showLoader = false;
                    this.catalogueFileListLength = 0;
                    this._router.navigate(['/error404']);
                }
            );
        }
    }

    getlistofUploadedCatalogeFiles() {
        if(this.catalogueType === '' && this.isAmazonCatelogType){            
            return false;
        }
        console.log(this.catalogueType);
        sessionStorage.setItem('catalogueType', this.catalogueType);
        this.isAmazonCatelogData = true;
        
        this.showLoader = true;
        this.start = 0;
        this.count = 0;
        // console.log(value);
        this._globalFunc.logger("Get list of uploaded catalogue service");
        this._postsService.getlistofUploadedCatalogeFiles(this.orgApiName, this.start, this.limit).subscribe(data => {

            this.showLoader = false;
            this.start = 0;
            this.count = 0;
            this.catalogueFileList = [];
        
            this.hitNext = true;
            this.catalogueFileListLength = data.auditDetails.length;
            this.count = data.paginationMetaData.count;
        
            this.catalogueFileList = data.auditDetails;
            console.log(this.catalogueFileList);
            console.log(this.catalogueFileListLength);

            this.errorMsgString = "";

            //for the scroll top
            var exptbl = document.getElementById('exportable');
            if (exptbl.scrollTop > 0) {
                document.getElementById('exportable').scrollTop = 0;
            }

        }
            , error => {
                this._globalFunc.logger("Get list of uploaded catalogue service error : "+error.errorMessage);
                if (error.errorCode == '404.33.402') {
                    this.errorMsgString = error.errorMessage;
                }
                //alert(error.errorCode);
                this.showLoader = false;
                this.catalogueFileListLength = 0;
                //this._router.navigate(['/error404']);
            }
        );


    }

    fileDownloadFromFileService(filename: any) {
        this._globalFunc.logger("Download file using file service");
        this._postsService.fileDownloadFromFileService(filename).subscribe(data => {
            this.showLoader = false;
            console.log(data);
            console.log(data.fileURL);
            this.fileDownload(data.fileURL);
        }
        );
    }


    // get all catalogue detail
    /*  getlistofUploadedCatalogeFiles() {

        this.showLoader = true;
        this.start = 0;
        this.count = 0;
        // console.log(value); 
        this._postsService.getlistofUploadedCatalogeFiles(this.orgApiName).subscribe(data => {

            this.showLoader = false;
            this.catalogueFileList = [];
            console.log(data);

            //  console.log(JSON.stringify(data.listOfCatalogFiles));
            this.catalogueFileList = (data.files);
            //  console.log(this.catalogueFileList);


        }
            , err => {
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
        );
    }  */

    // format date function
    formatDate(createdAtDate: string) {
        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
            if (createdAtDate.indexOf('/') >= 0) {
                return createdAtDate;
            } else {
                var d = new Date(createdAtDate);
                var yyyy = d.getFullYear();
                var mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1); // getMonth() is zero-based
                var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
                // var hh = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
                // var min = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
                return yyyy + "/" + mm + "/" + dd;
            }
        } else {
            return '-';
        }
    }

    getCustomerImage(custName:string){
        return this._globalFunc.getCustomerImage(custName);
    }

    getUserSession(customerName:any, loggedInUserGroup:any, loggedInUserId:any){
        return new Promise((resolve, reject) => {
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData:any) => {
                if(!userData.isSessionActive) { // false
                    // User session has expired.
                    if(userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser){
                            console.log("Session terminated");
                            reject('sessionInactive');
                            this.terminateFlg = true;
                            let sessionModal = document.getElementById('popup_SessionExpired');
                            sessionModal.classList.remove('in');
                            sessionModal.classList.add('out');
                            document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else{
                        reject('sessionInactive');
                        sessionStorage.clear();
                        this._router.navigate(['']);
                    }
                } else {
                    if(userData.sessionMessage == this.objConstant.userSessionTerminatedMsg){
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else{
                        resolve('sessionActive');
                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName+".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
                        this.organisationListChange = changeOrgAvailable;
                        this.orgimgUrl = orgImgUrl;
                        this.orgApiName = orgApiNameTemp;
                        // this.disabledCusChange = false;
                        if (this.organisationList.length == 1) {
                            this.disabledCusChange = true;
                        }
        
                        console.log("this.organisationList234");
                        console.log(this.organisationList);
                        
                        let catalogueBulkUploadView = true;
                        let catalogueBulkUploadSubmit = true;
                        let that = this;
                        let orderEnquiry = true;
                        let claimsReview = true;
                        let dashboardMenuPermission = false;
                        let userHavingPermission = false;
                        this.organisationList.forEach(function (organisation: any) {
                            
                            if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                                console.log("organisation : "+that.customerName);
                                console.log(organisation);
                                organisation.permissionRoleList.forEach(function(permRoleList: any){
                                    console.log("11");
                                    console.log(permRoleList);
                                    permRoleList.permissionDetailsList.forEach(function(permRoleDtlList: any){
                                        console.log("22");
                                        if(permRoleDtlList.permissionGroup == 'catalogueManagement'){
                                            userHavingPermission = true;
                                            permRoleDtlList.permissionNameList.forEach(function(permList: any){
                                                if(permList.permissionName == 'catalogueManagement' && permList.permissionStatus == 'active'){
                                                    dashboardMenuPermission = true;
                                                }
                                                console.log(permList);
                                                
                                                switch (permList.permissionName) { 
                                                    case "catalogueBulkUploadView":
                                                        //console.log("55");
                                                        catalogueBulkUploadView = false;
                                                        /* permArrayWholesaleCatalogues.push({ "catalogueBulkUploadView": true }); */
                                                        //alert(filfilment);
                                                        break;
                                                    case "catalogueBulkUploadSubmit":
                                                        //console.log("55");
                                                        catalogueBulkUploadSubmit = false;
                                                        //alert(filfilment);
                                                        break;
                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                        that = null;
                        if(!userHavingPermission){
                            // redirect to order
                            this._router.navigate(["cataloguemanagement",customerName]);
                        }
                        
                        this.disablecatalogueBulkUploadView = catalogueBulkUploadView;
                        this.disablecatalogueBulkUploadSubmit = catalogueBulkUploadSubmit;
                    }
                }
                //alert(this.disabledfilfilment);
            });
        });
    }
    
    closePopupp() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }

    getAllMasterData() {
        this.showLoader = true;
        this._globalFunc.logger("catalogue master data service");
        if(sessionStorage.getItem('orgType') === 'amazon'){
            this.cataloguemasterListLength = 2;
            this.catalogueTypes = this.objConstant.amazonCatalogueTypes;

        }
        else {
            this._postsService.getAllMasterData(this.orgApiName).subscribe(data => {
                this.showLoader = false;
                if (data.catalogueTypes.length == 0) {
                    this.cataloguemasterListLength = 0;
                } else {
                    for (var k = 0; k < data.catalogueTypes.length; k++) {
                        if (data.catalogueTypes[k].catalogueType.indexOf(':') > 0) {
                            data.catalogueTypes[k].catalogueType = data.catalogueTypes[k].catalogueType.split(':')[1];
                        }
                    }
                    this.cataloguemasterListLength = data.catalogueTypes.length;


                }
                this.catalogueTypes = data.catalogueTypes;
            
            }
                , err => {
                    this._globalFunc.logger("catalogue master data service failed");
                    this.cataloguemasterListLength = 0;
                    this.showLoader = false;
                    this._router.navigate(['/error404']);
                }
            );
        }
    }
}