import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogueuploadComponent } from './catalogueupload.component';

// route Configuration
const catalogueRoutes: Routes = [
  { path: '', component: CatalogueuploadComponent },
  { path: 'fullrangeupload', loadChildren: './fullrangeupload/fullrangeupload.module#FullRangeUploadModule' }
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(catalogueRoutes)],
  exports: [RouterModule]
})
export class CatalogueuploadComponentRoutes { }
