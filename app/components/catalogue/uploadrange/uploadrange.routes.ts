import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { UploadrangeComponent } from './uploadrange.component';

// route Configuration
const uploadrangeRoutes: Routes = [
  { path: '', component: UploadrangeComponent},
 { path: 'rangefileupload', loadChildren: './rangefileupload/rangefileupload.module#RangefileuploadModule' }
];


@NgModule({
  imports: [RouterModule.forChild(uploadrangeRoutes)],
  exports: [RouterModule]
})

export class UploadrangeRoutes{}