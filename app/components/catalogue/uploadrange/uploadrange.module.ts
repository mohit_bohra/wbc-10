import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadrangeComponent } from './uploadrange.component';
import { UploadrangeRoutes } from './uploadrange.routes';
import { CatalogueManagePendingHeaderModule } from '../../cataloguemanagement/cataloguemanagependingheader/cataloguemanagependingheader.module';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';

@NgModule({
  imports: [
    UploadrangeRoutes,
    CommonModule,
    CatalogueManagePendingHeaderModule,
    InfiniteScrollModule
  ],
  declarations: [UploadrangeComponent]
})
export class UploadrangeModule { }
