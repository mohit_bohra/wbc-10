import { Component } from '@angular/core';
import { Services } from '../../../../services/app.services';
import { GlobalComponent } from '../../../global/global.component';
import { Constant } from '../../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { Ng2FileInputService } from '../../../ng2-file-input/dist/ng2-file-input';
import { commonfunction } from '../../../../services/commonfunction.services';
import { FormBuilder } from '../../../../../node_modules/@angular/forms';
import { DialogService } from '../../../../services/dialog.service';

@Component({
  templateUrl: 'rangefileupload.component.html',
  providers: [commonfunction]
})
export class RangefileuploadComponent {
  
  //rbac
  homeUrl: any;
  objConstant = new Constant();
  whoImgUrl: any;
  showLoader: boolean;
  pageState: number;
  customerName: string;
  customerDispayName: string;
  style: string;
  organisationList: any;
  imgName: string = "";
  organisationListChange: any;
  orgimgUrl: string;
  orgApiName: string;
  custPermission: any[];
  disabledCusChange: boolean;
  private _sub: any;
  strInfoNotAvail: String;
  start: number;
  limit: number;
  count: number;
  hitNext: boolean;

  //file input
  file: File;
  newBatchNo: number;
  allowedMimeType: string[];
  uploadedFileName: String;
  fileuploadStatus: boolean = false;

  //popup message
  rangeFileUploadMsgBox: boolean;
  msgTitle: String;
  popupMessage: String;

  //session termination
  terminateMsg: any;
  terminateFlg: boolean = false;

  //maintenance
  moduleName: string;
  siteMaintanceFlg: boolean = false;
  siteMSg: any;
  errorMsg: string;
  deploymentDetailsMsg: string;
  featureDetailsMsg: string;
  businessMessageDetailsMsg: string;
  deploymentDetailsMsglength: any;
  featureDetailsMsglength: any;
  businessMessageDetailsMsglength: any;

  //upload range file required variables
  signedURL: string;
  disableUploadRangeFileView: boolean = true;
  disableUploadRangeFileSubmit: boolean = true;
  public todayDate: Date = new Date();
  public myFileInputIdentifier: string = "upload_rangefile";

  //unsaved changes
  unsavedChange: boolean = false;

  constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, private ng2FileInputService: Ng2FileInputService, public _cfunction: commonfunction, public dialogService: DialogService) {
    this.whoImgUrl = this.objConstant.WHOIMG_URL;
    this.homeUrl = this.objConstant.HOME_URL;
  }

  //get customer image
  getCustomerImage(custName: string) {
    return this._globalFunc.getCustomerImage(custName);
  }

  // function works on page load
  ngOnInit() {
    this._globalFunc.logger("Loading upload range file screen.");
    if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
      this._router.navigate(['']);
    } else {
      this.uploadedFileName = "";
      this.allowedMimeType = ['text/comma-separated-values', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.ms-excel', 'application/vnd.msexcel'];
      this.showLoader = false;
      // user permission
      this.disabledCusChange = false;
      this.strInfoNotAvail = "-";
      this.newBatchNo = 0;
      this.rangeFileUploadMsgBox = false;
      this.msgTitle = "Batch Number";
      this.popupMessage = "File uploaded successfully";
      this.file = null;

      // window scroll function
      this._globalFunc.autoscroll();

      // set header basket data
      this.pageState = 53;
      this.moduleName = "catalogueManagement";
      this.start = 0;
      this.limit = 20;// this.objConstant.SCROLL_RECORD_LIMIT;
      this.count = 0;
      this.hitNext = false;
      sessionStorage.setItem('pageState', '53');
      (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

      // subscribe to route params
      this._sub = this._route.params.subscribe((params: { orgType: string }) => {
        sessionStorage.setItem('orgType', params.orgType);
        this.customerName = params.orgType;
        this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
        if (this.customerName == 'mccolls') {
          this.style = 'margin-zero';
        } else {
          this.style = 'margin-zero product-title-top';
        }
        let cusType = params.orgType;
        let orgImgUrl;
        let orgApiNameTemp;

        this.imgName = this.getCustomerImage(params.orgType);
        let that = this;
        this.orgApiName = this.customerName;
        this.customerName = params.orgType;
        this.orgimgUrl = orgImgUrl;
        this.orgApiName = orgApiNameTemp;
        this.orgApiName = this.customerName;

        let custPerm: any = [];
        this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
          if (res == 'sessionActive') {
            this.imgName = this.getCustomerImage(params.orgType);
            this.custPermission = custPerm;
          }
        }, reason => {
          console.log(reason);
        });
      });
    }
  }

  public onAdded(event: any) {
    this._globalFunc.logger("File added to placeholder and array.");
    let fileList: FileList = event.currentFiles;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      this.file = file;
      let fileArr = this.file.name.split('.');
      if (fileArr[fileArr.length - 1].toLowerCase() != 'csv') {
        // Invalid file
        this.rangeFileUploadMsgBox = true;
        this.msgTitle = "Error";
        this.popupMessage = "Please select CSV File only.";

        document.getElementById('rangeFileUploadBox').style.display = "block";
        this.ng2FileInputService.reset(this.myFileInputIdentifier);
        this.fileuploadStatus = false;
      } else {
        // Check File Size
        if (this.file.size > 200000000) {
          // Invalid file
          this.rangeFileUploadMsgBox = true;
          this.msgTitle = "Error";
          this.popupMessage = "Please select file of size not more than 200mb.";
          document.getElementById('rangeFileUploadBox').style.display = "block";
          this.ng2FileInputService.reset(this.myFileInputIdentifier);
          this.fileuploadStatus = false;
        } else {
          this.uploadedFileName = file.name;
        }
      }
    }
  }

  public onRemoved(event: any) {
    this._globalFunc.logger("File removed to placeholder and array.");
    this.uploadedFileName = "";
    delete (this.file);
  }

  //get the file names
  public getFileNames(files: File[]): string {
    let names = files.map(file => file.name);
    return names ? names.join(", ") : "No files currently added.";
  }

  async sleep(ms: number) {
    await this._sleep(ms);
  }

  _sleep(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  //file upload functionality
  upload(filenameui: any) {
    this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
      if (res == 'sessionActive') {
        let fileList = this.ng2FileInputService.getCurrentFiles(this.myFileInputIdentifier);
        this.showLoader = true;
        if (fileList.length != 0) {
          this._globalFunc.logger("get presigned url service.");
          this._postsService.getSignedURL(this.orgApiName, filenameui, 'cloud').subscribe(data => {
            this.signedURL = data.uploadURL;
            let trackerStr: any;
            trackerStr = '{"username": "' + sessionStorage.getItem('username') + '","customerId": "' + this.orgApiName + '","type": "cloud","orignalFileName": "' + filenameui + '","uploadedFileName": "' + data.fileName + '","status": "Validation Pending","uploadedFilePath": "wholesaleexportcatalogue/amazon/cloud/processing/' + data.fileName + '","errorFilePath": "","eventCreatedBy": "' + sessionStorage.getItem('username') + '"}';
            this._globalFunc.logger("Put file to Cloud.");
            this._postsService.putFileToCloud(this.signedURL, this.file).subscribe(data => {
              this._postsService.putdataincatalogueTracker(trackerStr, this.orgApiName).subscribe(data => { });
              this.showLoader = false;
              this.rangeFileUploadMsgBox = true;
              this.msgTitle = "File upload status";
              this.popupMessage = 'Cloud file upload is in progress. Please check further status on "Status."';
              document.getElementById('rangeFileUploadBox').style.display = "block";
              this.ng2FileInputService.reset(this.myFileInputIdentifier);
              this.uploadedFileName = "";
              this.fileuploadStatus = true;
            }, err => {
              this._globalFunc.logger("Put file to Cloud failed.");
              this.showLoader = false;
              this.rangeFileUploadMsgBox = true;
              this.msgTitle = "Error!";
              this.popupMessage = "Service temporary not available.";
              document.getElementById('rangeFileUploadBox').style.display = "block";
              this.fileuploadStatus = false;
            });
          }, err => {
            this._globalFunc.logger("get presigned url service Failed.");
            this.showLoader = false;
            this.rangeFileUploadMsgBox = true;
            this.fileuploadStatus = false;
            this.msgTitle = "Error!";
            this.popupMessage = "Service temporary not available.";
            document.getElementById('rangeFileUploadBox').style.display = "block";
            let trackerStr: any;
          });
        } else {
          this.showLoader = false;
          this.rangeFileUploadMsgBox = true;
          this.msgTitle = "Error";
          this.popupMessage = "Please select CSV File to upload.";
          this.fileuploadStatus = false;
          document.getElementById('rangeFileUploadBox').style.display = "block";
        }
      } else {
        console.log(res);
      }
    }, reason => {
      console.log(reason);
    });
  }

  onScrolltop() {
    this._globalFunc.autoscroll();
  }

  //reset on close functionality
  resetOnClose(status: any) {
    this.rangeFileUploadMsgBox = false;
    document.getElementById('rangeFileUploadBox').style.display = "none";
    this.file = null;
    if (status == true) {
      this._router.navigate(['/catalogue', this.customerName, 'uploadrange']);
    }
  }

  //on successful upload redirect it to view uploaded range file
  redirect_toList(status: any) {
    if (status == true) {
      this._router.navigate(['/catalogue', this.customerName, 'uploadrange']);
    } else {
      this.resetOnClose(status);
    }
  }

  //rbac and session termination functionality
  getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
    return new Promise((resolve, reject) => {
      this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
        if (!userData.isSessionActive) {
          // User session has expired.
          if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
            this.terminateFlg = true;
            let sessionModal = document.getElementById('popup_SessionExpired');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('popup_SessionExpired').style.display = 'block';
          }
          else {
            sessionStorage.clear();
            this._router.navigate(['']);
            reject('sessionInactive');
          }
        } else {
          if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
            this.terminateFlg = true;
            reject('sessionInactive');
            let sessionModal = document.getElementById('popup_SessionExpired');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('popup_SessionExpired').style.display = 'block';
          } else {
            resolve('sessionActive');
          }
        }
      });
    });
  }

  //popup close functionality
  closePopup(id: string) {
    let sessionModal = document.getElementById(id);
    if (id == 'popup_SessionExpired') {//if session expired popup is displayed then on click of ok it should be redirected to login page
      sessionModal.classList.remove('out');
      sessionModal.classList.add('in');
      document.getElementById(id).style.display = 'none';
      let dt = this._cfunction.getUserSession();
      this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
      }, (error: any) => {
        console.log(error);
      });
      sessionStorage.clear();
      this._router.navigate(['']);
    } else {//if popup is displayed then on click of ok it should remain on same page
      sessionModal.classList.remove('out');
      sessionModal.classList.add('in');
      document.getElementById(id).style.display = 'none';
    }
  }

  //site maintenance functionaity
  getConfigService(pageName: any) {
    this._postsService.siteMaintanceCall('1', 'all').subscribe(userData => {
      if (userData.deploymentDetails != undefined && userData.deploymentDetails[0].status != 'NO') {
        this.deploymentDetailsMsg = userData.deploymentDetails[0].message + ' ' + userData.deploymentDetails[0].start;
        this.deploymentDetailsMsglength = this.deploymentDetailsMsg.length;
        if (sessionStorage.getItem('deploymentDetailsMsglength') == undefined || sessionStorage.getItem('deploymentDetailsMsglength') == null) {
          sessionStorage.setItem('deploymentDetailsMsglength', this.deploymentDetailsMsglength);
        } else {
          this.deploymentDetailsMsglength = sessionStorage.getItem('deploymentDetailsMsglength');
        }
      } else {
        this.deploymentDetailsMsg = '';
      }

      if (userData.featureDetails != undefined && userData.featureDetails[0].status != 'NO') {
        this.featureDetailsMsg = userData.featureDetails[0].message + ' ' + userData.featureDetails[0].start;
        this.featureDetailsMsglength = this.featureDetailsMsg.length;
        if (sessionStorage.getItem('featureDetailsMsglength') == undefined || sessionStorage.getItem('featureDetailsMsglength') == null) {
          sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
        }
        else {
          this.deploymentDetailsMsglength = sessionStorage.getItem('featureDetailsMsglength');
        }
      } else {
        this.featureDetailsMsg = '';
      }

      if (userData.businessMessageDetails != undefined && userData.businessMessageDetails[0].status != 'NO') {
        this.businessMessageDetailsMsg = userData.businessMessageDetails[0].message + ' ' + userData.businessMessageDetails[0].start;
        this.businessMessageDetailsMsglength = this.businessMessageDetailsMsg.length;
        if (sessionStorage.getItem('businessMessageDetailsMsglength') == undefined || sessionStorage.getItem('businessMessageDetailsMsglength') == null) {
          sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
          sessionStorage.setItem('businessMessageDetailsMsglength', this.businessMessageDetailsMsglength);
        }
        else {
          this.businessMessageDetailsMsglength = sessionStorage.getItem('businessMessageDetailsMsglength');
        }
      } else {
        this.businessMessageDetailsMsg = '';
      }

      (<HTMLLabelElement>document.getElementById('deploymentDetailsId')).textContent = this.deploymentDetailsMsg;
      (<HTMLLabelElement>document.getElementById('featureDetailsId')).textContent = this.featureDetailsMsg;
      (<HTMLLabelElement>document.getElementById('businessMessageDetailsId')).textContent = this.businessMessageDetailsMsg;
      if (userData.maintenanceDetails[0].moduleName == 'all') {
        if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
          this.siteMaintanceFlg = true;
          this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
        } else {
          this.siteMaintanceFlg = false;
          this._postsService.siteMaintanceCall(this.customerName, pageName).subscribe(userData => {
            if (userData.maintenanceDetails[0].moduleName == pageName) {
              if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                this.siteMaintanceFlg = true;
                this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
              } else {
                this.siteMaintanceFlg = false;
              }
            } else {
              this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
          }, error => {
            if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
              this.siteMaintanceFlg = true;
              this.siteMSg = "Site is under maintenance";
            } else if (error.errorCode == '404.26.408') {
              this.errorMsg = "User is Inactive";
            }
            else {
              this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
          });
        }
      } else {
        this.siteMaintanceFlg = false;
      }
      this.showLoader = false;
    }, error => {
      if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
        this.siteMaintanceFlg = true;
        this.siteMSg = "Site is under maintenance";
      } else if (error.errorCode == '404.26.408') {
        this.errorMsg = "User is Inactive";
      }
      else {
        this.siteMaintanceFlg = false;
      }
      this.showLoader = false;
    });
  }
}