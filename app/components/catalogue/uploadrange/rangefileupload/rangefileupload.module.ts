import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RangefileuploadComponent } from './rangefileupload.component';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { GlobalComponent } from '../../../global/global.component';
import { Services } from '../../../../services/app.services';
import { FormsModule } from '@angular/forms';
import { CatalogueManagePendingHeaderModule } from '../../../cataloguemanagement/cataloguemanagependingheader/cataloguemanagependingheader.module';
import { Ng2FileInputModule } from '../../../ng2-file-input/dist/ng2-file-input';
import { RangeFileComponentRoutes } from './rangefileupload.routes';

@NgModule({
  imports: [
    RangeFileComponentRoutes,
    CommonModule,
    FormsModule,
    InfiniteScrollModule,
    Ng2FileInputModule.forRoot(),
    CatalogueManagePendingHeaderModule
  ],
  exports: [],
  declarations: [RangefileuploadComponent],
  providers: [Services, GlobalComponent]
})
export class RangefileuploadModule {
  constructor(){
  }
 }
