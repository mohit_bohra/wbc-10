import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RangefileuploadComponent } from './rangefileupload.component';

// route Configuration
const rangefileuploadRoutes: Routes = [
  { path: '', component: RangefileuploadComponent }
];


@NgModule({
  imports: [RouterModule.forChild(rangefileuploadRoutes)],
  exports: [RouterModule]
})
export class RangeFileComponentRoutes { }
