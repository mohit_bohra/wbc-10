export class transitInformationForAmazon{
    virtualSellingLocation : string;
    supplyingDepotLocation : string;

    constructor(virtualSellingLocation:any, supplyingDepotLocation :any){
        this.virtualSellingLocation = virtualSellingLocation;
        this.supplyingDepotLocation = supplyingDepotLocation;
    }
}