
import { transitInformationForAmazon } from "./transitInformationForAmazon";

export class Merchants{
    merchantId : string;
    merchantName : string;
    merchantToken :string;
    marketplaceId : string;
    inventoryApplicable : boolean;
    priceUpdateApplicable : boolean;
    settlementReportApplicable : boolean;
    transitInformation : transitInformationForAmazon[];

    constructor( merchantId : string,merchantName : string,merchantToken :string,marketplaceId : string,
        inventoryApplicable : boolean,
        priceUpdateApplicable : boolean,settlementReportApplicable : boolean,
        transitInformation : any){
            this.merchantId = merchantId;
            this.merchantName = merchantName;
            this.merchantToken = merchantToken;
            this.marketplaceId = marketplaceId;
            this.inventoryApplicable = inventoryApplicable;
            this.priceUpdateApplicable = priceUpdateApplicable;
            this.settlementReportApplicable = settlementReportApplicable;
            this.transitInformation = transitInformation;         

    }
}