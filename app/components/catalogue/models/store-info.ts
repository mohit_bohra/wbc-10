import { Audit } from "./audit";
import{Category}from "./categories";
import { Merchants } from "./merchants";

export class StoreInfo {
    audit: Audit;
    status: string;
    storeIdPrefix:string;
    storeId: any;
    storeDescription:any;
    areaCode: string;
    createdBy: string;
    storeName: string;
    updatedBy: string;
    categories: Category[];
    contactFax: string;
    revisionId: string;
    regionCode: string;
    addressCity: string;
    addressName: string;
    areaManager: string;
    createdDate: any;
    tradingName: string;
    addressLine1: string;
    addressLine2: string;
    availability: string;
    contactEmail: string;
    contactPhone: string;
    port:string;
    customerName: string;
    addressCounty: string;
    minimumExpetedInventory : string;
    merchants : Merchants[];
   
    regionManager: string;
    addressCountry: string;
    addressPostcode: string;
    allocationPriority: string;
    supplyingDepotName: string;
  
    applyImmediate:boolean;
   // merchantId : any;
   

    constructor(audit: Audit,
        status: string,
        storeId: any,
        storeDescription :any,
        storeIdPrefix:string,
        areaCode: string,
        createdBy: string,
        storeName: string,
        updatedBy: string,
        categories:any ,
        contactFax: string,
        revisionId: string,
        regionCode: string,
        addressCity: string,
        addressName: string,
        areaManager: string,
        createdDate: any,
        tradingName: string,
        addressLine1: string,
        addressLine2: string,
        availability: string,
        contactEmail: string,
        contactPhone: string,
        port:string,
        customerName: string,
        addressCounty: string,
       
       
        regionManager: string,
        addressCountry: string,
        addressPostcode: string,
        allocationPriority: string,
        supplyingDepotName: string,
        applyImmediate:boolean,
        //merchantId:any,
       
        merchants : any,
        minimumExpetedInventory : any
       ) {
            this.status=status;
            this.storeId=storeId;
            this.storeDescription=storeDescription;
            this.storeIdPrefix = storeIdPrefix;
            this.areaCode=areaCode;
            this.createdBy=createdBy;
            this.storeName=storeName;
            this.updatedBy=updatedBy;
            this.categories=categories;
            this.contactFax=contactFax;
            this.revisionId=revisionId;
            this.regionCode=regionCode;
            this.addressCity=addressCity;
            this.addressName=addressName;
            this.areaManager=areaManager;
            this.createdDate=createdDate;
            this.tradingName=tradingName;
            this.addressLine1=addressLine1;
            this.addressLine2=addressLine2;
            this.availability=availability;
            this.contactEmail=contactEmail;
            this.contactPhone=contactPhone;
            this.port=port;
            this.customerName=customerName;
            this.addressCounty=addressCounty;
           
            this.regionManager=regionManager;
            this.addressCountry=addressCountry;
            this.addressPostcode=addressPostcode;
            this.allocationPriority=allocationPriority;
            this.supplyingDepotName=supplyingDepotName;
            this.applyImmediate=applyImmediate;
            //this.merchantId = merchantId;
           
            this.merchants = merchants;
            this.minimumExpetedInventory = minimumExpetedInventory;
        }

}