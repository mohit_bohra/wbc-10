export class TransitInformation {
    supplyingDepotLocation:string;
    virtualSellingLocation:string;
    supplyingShippingLocation:string;
   

    constructor(supplyingDepotLocation:string,virtualSellingLocation:string, supplyingShippingLocation:string) {
        this.supplyingDepotLocation=supplyingDepotLocation;
        this.virtualSellingLocation = virtualSellingLocation;
        this.supplyingShippingLocation = supplyingShippingLocation;
       
    }
}

