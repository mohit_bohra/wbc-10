import { TransitInformation } from "./transitInformation";

export class DeliveryOpportunity {
    when:string;
    value:string;
    effectiveDate:string;
    transitInformation:TransitInformation[]

    constructor(when:string,value:string, effectiveDate:string , transitInformation:any) {
        this.when=when;
        this.value = value;
        this.effectiveDate = effectiveDate;
        this.transitInformation = transitInformation;
    }
}

