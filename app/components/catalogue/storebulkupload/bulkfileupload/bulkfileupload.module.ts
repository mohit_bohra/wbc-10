import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BulkfileUploadComponent } from './bulkfileupload.component';
import { BulkFileUploadComponentRoutes } from './bulkfileupload.routes';
import { Services } from '../../../../services/app.services';
import { GlobalComponent } from '../../../global/global.component';
import { CatalogueManagePendingHeaderModule } from '../../../cataloguemanagement/cataloguemanagependingheader/cataloguemanagependingheader.module';
import { Ng2FileInputModule } from '../../../ng2-file-input/dist/ng2-file-input';
import { FormsModule } from '../../../../../node_modules/@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BulkFileUploadComponentRoutes,
    CatalogueManagePendingHeaderModule,
    Ng2FileInputModule.forRoot()
  ],
  declarations: [BulkfileUploadComponent],
  providers:[Services,GlobalComponent]
})
export class BulkfileUploadModule {
  constructor(){}
}
