import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BulkfileUploadComponent } from './bulkfileupload.component';

// route Configuration
const bulkfileuploadRoutes: Routes = [
  { path: '', component: BulkfileUploadComponent }
];


@NgModule({
  imports: [RouterModule.forChild(bulkfileuploadRoutes)],
  exports: [RouterModule]
})
export class BulkFileUploadComponentRoutes { }
