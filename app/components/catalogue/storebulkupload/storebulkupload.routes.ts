import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { StoreBulkUploadComponent } from './storebulkupload.component';

// route Configuration
const storebulkuploadRoutes: Routes = [
  { path: '', component: StoreBulkUploadComponent},
  { path: 'bulkfileupload', loadChildren: './bulkfileupload/bulkfileupload.module#BulkfileUploadModule' }
];


@NgModule({
  imports: [RouterModule.forChild(storebulkuploadRoutes)],
  exports: [RouterModule]
})

export class StorebulkUploadRoutes{}