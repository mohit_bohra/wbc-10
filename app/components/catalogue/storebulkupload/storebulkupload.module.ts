import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreBulkUploadComponent } from './storebulkupload.component';
import { StorebulkUploadRoutes } from './storebulkupload.routes';
import { CatalogueManagePendingHeaderModule } from '../../cataloguemanagement/cataloguemanagependingheader/cataloguemanagependingheader.module';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';

@NgModule({
  imports: [
    CommonModule,
    StorebulkUploadRoutes,
    CatalogueManagePendingHeaderModule,
    InfiniteScrollModule
  ],
  declarations: [StoreBulkUploadComponent]
})
export class StoreBulkUploadModule { }
