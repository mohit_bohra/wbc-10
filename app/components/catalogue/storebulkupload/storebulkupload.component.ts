import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { commonfunction } from '../../../services/commonfunction.services';
import { Constant } from '../../constants/constant';
import { Services } from '../../../services/app.services';
import { GlobalComponent } from '../../global/global.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  templateUrl: './storebulkupload.component.html',
  providers: [commonfunction]
})
export class StoreBulkUploadComponent implements OnInit {

  //rbac
  objConstant = new Constant();
  errorMsgString: string;
  showErrorMsg: boolean;
  whoImgUrl: any;
  showLoader: boolean;
  pageState: number;
  customerName: string;
  customerDispayName: string;
  style: string;
  organisationList: any;
  organisationListChange: any;
  orgimgUrl: string;
  orgApiName: string;
  disabledCusChange: boolean;
  disabledPermission: boolean;
  homeUrl: any;
  imgName: string = "";
  strInfoNotAvail: string;
  start: number;
  limit: number;
  count: number;
  hitNext: boolean;
  custPermission: any[];
  private _sub: any;

  //session termination
  terminateMsg: any;
  terminateFlg: boolean = false;

  //download popup message
  confirmsg: string = '';

  //permissions
  disableStoreCatalogueFileSubmit: boolean = false;

  //array list
  storeFileListLength: number;
  storeFileList: any;

  //document.getElementById replaced with ElementRef
  @ViewChild('confirmationModal') confirmationModal: ElementRef;
  @ViewChild('popup_SessionExpired') popup_SessionExpired: ElementRef;

  constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, public _cfunction: commonfunction) {
    this.whoImgUrl = this.objConstant.WHOIMG_URL;
    this.homeUrl = this.objConstant.HOME_URL;
    this.limit = 20;
    this.start = 0;
    this.count = 0;
  }

  ngOnInit() {
    this._globalFunc.logger("Loading Catalogue uploaded listing screen");
    if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
      this._router.navigate(['']);
    } else {
      this.showLoader = false;
      this.showErrorMsg = false;
      // user permission
      this.disabledCusChange = false;
      this.disabledPermission = false;
      this.strInfoNotAvail = "-";
      this.confirmsg = this.strInfoNotAvail;

      // window scroll function
      this._globalFunc.autoscroll();

      // set header basket data
      this.pageState = 54;
      this.hitNext = false;
      sessionStorage.setItem('pageState', '54');
      document.getElementById('login').style.display = 'inline';
      (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

      // subscribe to route params
      this._sub = this._route.params.subscribe((params: { orgType: string }) => {
        sessionStorage.setItem('orgType', params.orgType);
        this.customerName = params.orgType;
        this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
        if (this.customerName == 'mccolls') {
          this.style = 'margin-zero';
        } else {
          this.style = 'margin-zero product-title-top';
        }
        let cusType = params.orgType;
        let orgImgUrl;
        let orgApiNameTemp;

        this.imgName = this.getCustomerImage(params.orgType);
        let that = this;
        this.orgimgUrl = orgImgUrl;
        this.orgApiName = this.customerName;
        this.customerName = params.orgType;
        this.orgimgUrl = orgImgUrl;
        this.orgApiName = orgApiNameTemp;
        this.orgApiName = this.customerName;

        let custPerm: any = [];
        
        this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
          if (res == 'sessionActive') {
            this.imgName = this.getCustomerImage(params.orgType);
            this.custPermission = custPerm;
          }
          let permissions = JSON.parse(sessionStorage.getItem('tempPermissionArray'));
          if(permissions[this.customerName]['catalogueManagement'].indexOf('storeBulkUploadSubmit')== -1){
            this.disableStoreCatalogueFileSubmit = true;
          }else{
            this.disableStoreCatalogueFileSubmit = false;
          }
        }, reason => {
            console.log(reason);
        });
        this.getUploadedStoreFiles();
      });
    }
  }

  getCustomerImage(custName: string) {
    return this._globalFunc.getCustomerImage(custName);
  }

  getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
    return new Promise((resolve, reject) => {
      this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
        if (!userData.isSessionActive) { 
          // User session has expired.
          if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
            console.log("Session terminated");
            reject('sessionInactive');
            this.terminateFlg = true;
            let sessionModal = this.popup_SessionExpired;
            sessionModal.nativeElement.classList.remove('in');
            sessionModal.nativeElement.classList.add('out');
            sessionModal.nativeElement.style.display = 'block';
          }
          else {
            reject('sessionInactive');
            sessionStorage.clear();
            this._router.navigate(['']);
          }
        } else {
          if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
            console.log("Session terminated");
            reject('sessionInactive');
            this.terminateFlg = true;
            let sessionModal = this.popup_SessionExpired;
            sessionModal.nativeElement.classList.remove('in');
            sessionModal.nativeElement.classList.add('out');
            sessionModal.nativeElement.style.display = 'block';
          } else {
            resolve('sessionActive');
            this.terminateFlg = false;
            this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
            let changeOrgAvailable: any = [];
            let orgImgUrl = "";
            let orgApiNameTemp = "";
            this.organisationList.forEach(function (item: any) {
              if (item.customerName !== customerName) {
                changeOrgAvailable.push(item);
              } else {
                orgImgUrl = item.customerName + ".png";
                orgApiNameTemp = item.customerName;
              }
            });
            this.organisationListChange = changeOrgAvailable;
            this.orgimgUrl = orgImgUrl;
            this.orgApiName = orgApiNameTemp;

            let that = this;

            that = null;

          }
        }
      });
    });
  }

  //redirect to range file upload
  redirect(page: any, orgType: any) {
    this._globalFunc.logger("redirect to " + page);
    this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
      if(res = 'sessionActive'){
        if (page == '/bulkfileupload') {
          this._router.navigate(['catalogue', encodeURIComponent(orgType), 'storebulkupload', 'bulkfileupload']);
        } else {
          this._router.navigate([page, orgType]);
        }
      }
    }, reason =>{
    console.log(reason);
    });
  }

  //get uploaded store files
  getUploadedStoreFiles(){
    this.showLoader = true;
    this.start = 0;
    this.count = 0;
    this._globalFunc.logger("Get list of uploaded catalogue service");
    this._postsService.getlistofUploadedStoreFiles(this.orgApiName, this.start, this.limit).subscribe(data => {
      this.showLoader = false;
      this.start = 0;
      this.count = 0;
      this.storeFileList = [];
      this.hitNext = true;
      this.storeFileListLength = data.auditDetails.length;
      this.count = data.paginationMetaData.count;
      this.storeFileList = data.auditDetails;
      this.errorMsgString = "";
    }, error => {
        this._globalFunc.logger("Get list of uploaded catalogue service error : "+error.errorMessage);
        if (error.errorCode == '404.33.402') {
          this.errorMsgString = error.errorMessage;
        }
        this.showLoader = false;
        this.storeFileListLength = 0;
      }
    );
  }

  //download functionality
  fileDownloadUsingPresignedURL(filename: string, filepath: string) {
    this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
      if(res = 'sessionActive'){
        this._globalFunc.logger("Download file using presigned url");
        this._postsService.fileReportDownloadUsingPresignedURL(this.orgApiName, filename, 'catalogue', 'download',"", filepath).subscribe(data => {
          window.open(data.preSignedS3Url);
        }, error => {
          this._globalFunc.logger("Error while Downloading file using presigned url");
          this.confirmsg = filename + " doesnot exist.";
          let sessionModal = this.confirmationModal
          sessionModal.nativeElement.style.display="block";
          sessionModal.nativeElement.classList.add('in');
        });
      }
    }, reason =>{
        console.log(reason);
    });
  }

  //infinite scroll functionality
  onScrollDown() {
    this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
      this.start = this.start + this.limit;
      if (this.start< this.count && this.hitNext == true) {
        this.showLoader = true;
        this._globalFunc.logger("Get list of uploaded store catalogue on scroll");
        this._postsService.getlistofUploadedStoreFiles(this.orgApiName, this.start, this.limit).subscribe(data => {
          this.showLoader = false;
          this.count = data.paginationMetaData.count;
          this.storeFileList = this.storeFileList.concat(data.auditDetails);
        }, err => {
            this._globalFunc.logger("Get list of uploaded store catalogue on scroll failed");
            this.showLoader = false;
            this.storeFileListLength = 0;
            this._router.navigate(['/error404']);
          }
        );
      }
    });
  }

  //close popup functionality
  closePopup(id:string) {
    let sessionModal = document.getElementById(id);
    if(id=='popup_SessionExpired'){//if session expired popup is displayed then on click of ok it should be redirected to login page
      sessionModal.classList.remove('out');
      sessionModal.classList.add('in');
      document.getElementById(id).style.display = 'none';
      let dt = this._cfunction.getUserSession();
      this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
      }, (error: any) => {
          console.log(error);
      });
      sessionStorage.clear();
      this._router.navigate(['']);
    }else{//if popup is displayed then on click of ok it should remain on same page
      sessionModal.classList.remove('out');
      sessionModal.classList.add('in');
      document.getElementById(id).style.display = 'none';
    }
  }

}
