import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from "ngx-tooltip";
import { CalendarModule } from 'primeng/primeng';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { StoredetailsComponentRoutes } from './storedetails.routes';
import { StoredetailsComponent } from './storedetails.component';
import { commonfunction } from '../../../services/commonfunction.services';
import { CategoryFilter } from './categoryFilter.pipes';
@NgModule({
  imports: [
    
    StoredetailsComponentRoutes,
    CommonModule,
    FormsModule,
    CalendarModule,
    TooltipModule,
    InfiniteScrollModule
  ],
  exports: [],
  declarations: [StoredetailsComponent , CategoryFilter],
  
  providers: [Services, GlobalComponent,commonfunction]
})
export class StoredetailsModule { }
