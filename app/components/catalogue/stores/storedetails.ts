export class StoreDetail {
    "stores" : [
      {
        "audit": {
          "who": "",
          "when": ""
        },
        "status": "",
        "storeId": "",
        "areaCode": "",
        "createdBy": "",
        "storeName": "",
        "updatedBy": "",
        "categories": [
          {
            "name": "",
            "saleId": "",
            "depotId": "",
            "stockId": "",
            "deliveryOpportunities": [
              {
                "value": "",
                "effectiveDate": ""
              }
            ]
          }
        ],
        "contactFax": "",
        "regionCode": "",
        "addressCity": "",
        "addressName": "",
        "areaManager": "",
        "createdDate": "",
        "tradingName": "",
        "addressLine1": "",
        "addressLine2": "",
        "availability": "",
        "contactEmail": "",
        "contactPhone": "",
        "customerName": "",
        "addressCounty": "",
        "effectiveDate": "",
        "regionManager": "",
        "storeIdPrefix": "",
        "addressCountry": "",
        "addressPostcode": "",
        "allocationPriority": "",
        "supplyingDepotName": "",
        "revisionId": ""
      }
    ]
  }