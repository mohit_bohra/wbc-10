import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoredetailsComponent } from './storedetails.component';
import { storeDetailResolve } from './storedetails.resolve.service';


// route Configuration
const storedetailsRoutes: Routes = [
  { path: '', component: StoredetailsComponent },
  { path: 'edit', loadChildren: './edit/editstore.module#EditstoreModule',resolve: {storeDtl: storeDetailResolve} },
  
];


@NgModule({
  imports: [RouterModule.forChild(storedetailsRoutes)],
  exports: [RouterModule],
  providers: [storeDetailResolve]
})
export class StoredetailsComponentRoutes { }
