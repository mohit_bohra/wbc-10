import { Component } from '@angular/core';
import { Services } from '../../../../services/app.services';
import { GlobalComponent } from '../../../global/global.component';
import { Constant } from '../../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { commonfunction } from '../../../../services/commonfunction.services';

@Component({
    selector: 'stroeexport',
    templateUrl: 'storeexport.component.html',
})
export class StoreexportComponent {
    public catalgoueDate: Date = new Date();
    objConstant = new Constant();
    errorMsgString: string;
    showErrorMsg: boolean;
    whoImgUrl: any;
    exportCode: any;
    exportCodeSelected: any;
    exportList: any;
    exportListLength: any;
    exportUpdatedDate: any;
    catalogueno: string;
    showLoader: boolean;
    hideOrganisation: boolean = true;
    exportResult: boolean = true;
    printHeader: boolean = true;
    pageState: number = 56;
    customerName: string;
    customerDispayName: string;
    style: string;
    organisationList: any;
    organisationListChange: any;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disabledOrder: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    private _sub: any;
    disabledExportCatalogue: boolean;
    disabledUploadCatalogue: boolean;
    disabledDashboard: boolean = false;
    succMSg: string;
    //permission
    disableStoreStoreFileSubmit: boolean = false;
    //for catalogue Enquiry redevelopment
    exportTypes: any;
    exportType: string = 'stores';
    exportmasterListLength: any;
    exportdetailListLength: any;
    exportDetailList: any;
    strInfoNotAvail: string;
    start: number;
    limit: number;
    count: number;
    hitNext: boolean;
    exportFilename: string;
    custPermission: any[];

    public todayDate: Date = new Date();

    homeUrl: any;
    imgName: string = "";


   // public daterange: any = {};
    buttonHide: boolean = true;
    public singleDate: any;
    public singleDateFormate: any;

    public maxDate : any  = this.todayDate.getDate() + "/" + (this.todayDate.getMonth() + 1) + "/" + this.todayDate.getFullYear();

    public options: any = {
        locale: {"format": "DD/MM/YYYY"},
        singleDatePicker: true,
        startDate:Date.now(),
        endDate: Date.now(),
        maxDate: this.maxDate.toString(),
        autoApply: true,
        autoUpdateInput: true,
        opens: "right",
    };

    singleSelect(value: any) {
        console.log('singleSelect ++++++++++++++++++');
        this.singleDate = value.start.format('DD/MM/YYYY');
        this.singleDateFormate = value.start.format('YYYY-MM-DD');
        this.buttonHide = false;
    }
    // public selectedDate(value: any, datepicker?: any) {
    //     // this is the date the iser selected
    //     console.log(value);

    //     // any object can be passed to the selected event and it will be passed back here
    //     datepicker.start = value.start;
    //     datepicker.end = value.end;

    //     // or manupulat your own internal property
    //     this.daterange.start = value.start;
    //     this.daterange.end = value.end;
    //     this.daterange.label = value.label;
    // }


    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router,public _cfunction: commonfunction) {
        // img urls
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
      //  this.singleDate ='DD/MM/YYYY';
        this.buttonHide = true;
       
    }

    // function works on page load
    ngOnInit() {       
        this._globalFunc.logger("Loading Store export screen.");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
            this._globalFunc.logger("store export user session not found");
        } else {
           // this.showLoader = false;
            this.catalogueno = '';
            this.showErrorMsg = false;
            // user permission
            this.disabledCusChange = false;
            this.disabledPermission = false;
            this.disabledReports = false;
            this.disabledCatalogue = false;
            this.disabledOrder = false;
            this.disabledOrderRaise = false;
            this.disabledCatalogueRaise = false;
            this.disabledManageCatalogue = false;
            this.disabledExportCatalogue = false;
            this.disabledUploadCatalogue = false;
            this.strInfoNotAvail = "-";
            //this.exportType = "";
            this.succMSg = this.strInfoNotAvail;

            // window scroll function
            this._globalFunc.autoscroll();

            // set header basket data
            this.start = 0;
            this.limit = this.objConstant.SCROLL_RECORD_LIMIT;
            this.count = 0;
            this.hitNext = false;
            sessionStorage.setItem('pageState', '56');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                if(this.customerName != params.orgType && this.customerName != undefined){
                    this.customerName = params.orgType;    
                    window.location.reload();
                } else {
                    this.customerName = params.orgType;
                }
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;

                this.imgName = this.getCustomerImage(params.orgType);
                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
                
              

                let custPerm: any = [];
                let that = this;
                

                this.custPermission = custPerm;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = this.customerName;
               

                console.log('Load table +++++++++++++');
                this.buttonHide = true;
                this.singleDate ="";
               
                let val : any = {'catalogueType' : 'stores'};
                this.getlistofExportFiles(val);
                
            });

            //permission
            let permissions = JSON.parse(sessionStorage.getItem('tempPermissionArray'));
            if(permissions[this.customerName]['catalogueManagement'].indexOf('storeBulkExportSubmit')== -1){
                this.disableStoreStoreFileSubmit = true;
            }else{
                this.disableStoreStoreFileSubmit = false;
            }

            console.log('export Submit permission : ' + this.disableStoreStoreFileSubmit);
           
            //end permission

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);

            // logic for resize pop up 
            window.onresize = () => {
                if (screen == "desktop") {
                    if (window.innerWidth < this._globalFunc.stdWidth) {
                        // this.catalogueResult = false;
                    } else {
                        //  this.catalogueResult = true;
                    }
                } else {
                    if (window.innerWidth > this._globalFunc.stdWidth) {
                        //   this.catalogueResult = true;
                        document.getElementById("resultModalClose").click();
                    } else {
                        //  this.catalogueResult = false;
                    }
                }

                // get device to populate some part of ui
                screen = this._globalFunc.getDevice(window.innerWidth);
            };
            window.onscroll = () => {
                this.scrollFunction();
            };
        }
    }

    onScrolltop() {
        this._globalFunc.autoscroll();
    }

    scrollFunction() {
        if (document.body.scrollTop > 800 || document.documentElement.scrollTop > 300) {
            document.getElementById("topBtn").style.display = "block";
        } else {
            document.getElementById("topBtn").style.display = "none";
        }
    }

    resetOnClose() {
        this.hitNext = false;
    }

    /* onScrollDown(catalogueCode: any, catalogueType: any) {
 
         this.start = this.start + this.limit;
 
         if (this.count == this.limit && this.hitNext == true) {
 
             this.showLoader = true;
 
             this._postsService.getCatalogueItem(this.orgApiName, catalogueCode, catalogueType, this.start, this.limit).subscribe(data => {
 
                 this.showLoader = false;
 
                 this.count = data.paginationMetaData.count;
                 this.catalogueDetailList = this.catalogueDetailList.concat(data.listOfCatalogFiles);
 
 
             }
                 , err => {
                     this.catalogueno = '';
                     this.showLoader = false;
                     this.cataloguedetailListLength = 0;
                     this._router.navigate(['/error404']);
                 }
             );
 
         }
 
     }*/


    // navigate function
    redirect(page: any, orgType: any) {
        this._globalFunc.logger("redirect to "+page);
        // console.log(page);
        // let url;
        if (page == 'addproduct') {
            //url = this.homeUrl + 'catalogue/' + encodeURIComponent(orgType) + '/add';
            this._router.navigate(['catalogue', encodeURIComponent(orgType),'add']);
            //this._router.navigate(url);
        } else if (page == 'catalogueexport') {
            //url = this.homeUrl + 'catalogue/' + encodeURIComponent(orgType) + '/export';
            this._router.navigate(['catalogue', encodeURIComponent(orgType),'export']);
        } else if (page == 'catalogueupload') {
            //url = this.homeUrl + 'catalogue/' + encodeURIComponent(orgType) + '/upload';
            this._router.navigate(['catalogue', encodeURIComponent(orgType),'upload']);
        } else {
            this._router.navigate([page, orgType]);
        }
        //this._router.navigate([page, orgType]);

    }
    // change organisation
    changeOrgType(page: any, orgType: any, page1:any) {
        console.log("Page : "+page);
        console.log(orgType);
        console.log(page1);
        this._globalFunc.logger("Changing Client");
        // reset field on change of organisation 
        this.catalogueno = '';
        this.exportList = [];
        this.exportListLength = 0;
        this.exportResult = true;
        //this.exportType = '';
        this.exportCode = '';
        this.hitNext = false;
        this.start = 0;
        this.count = 0;
        this.toggleOrganisation();
        this._router.navigate(this._globalFunc.redirectToComponent('catalogueExport', orgType));
        //this._router.navigate([page, orgType,page1]);
    }

    // download functionality
    exportData() {
        this._globalFunc.logger("Export to excel.");
        return this._globalFunc.exportData('exportable',this.customerName+'catalogueExportData');
    }

    printCatalogue() {
        this._globalFunc.logger("Printing");
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        var printContentsNav = document.getElementById('printNavCatalogue').innerHTML;
        var printContents = document.getElementById('exportable').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContentsNav + printContents + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContentsNav + printContents + '</body></html>');
            popup.document.close();
        }
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
        this._globalFunc.logger("Unloading catalogue export screen.");
        this._globalFunc.sentLoggerBatchToServer();
    }

    // change organisation div show and hide
    toggleOrganisation() {
        console.log('---Toggle ----');
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
            border.classList.remove('searchborder');
        }
        else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
            border.classList.add('searchborder');
        }
    }

    // close operation of pop up
    closeOperation() {
        this.exportList = '';
    }

    /*// key press event to disable special charater
    keyPress(event: any) {
        this._globalFunc.alphaNumericWithSpace(event);
    }*/



    // fileDownload(filepath: any) {
    //     window.open(filepath);
    // }

    fileDownloadUsingPresignedURL(filename: string){
        this._globalFunc.logger("Downloading files using presigned url");
        this._postsService.fileReportDownloadUsingPresignedURL(this.orgApiName, filename,'catalogueExport', 'download',this.exportType).subscribe(data=>{
            console.log('url____ : ' + data.preSignedS3Url);
            
            window.open(data.preSignedS3Url);
        }, error => {
            this._globalFunc.logger("presigned url service error - "+filename+" not exist");
            this.succMSg = filename+" doesnot exist.";
            let sessionModal = document.getElementById('addConfirmationModal');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
        });
    }

    // get all catalogue detail
    getlistofExportFiles(value: any) {
        let str = value.catalogueType;
        let searchTerm = '';
        if (str.search(':') != -1) {
            // console.log("adf");
            var strArr = str.split(':');
            searchTerm = strArr[1];
        } else {
            searchTerm = value.catalogueType;
        }

        this.showLoader = true;
        this.start = 0;
        this.count = 0;
        // console.log(value);
        this._globalFunc.logger("Exported catalog service");
        this._postsService.getlistofCatalogeFiles(this.orgApiName, value.catalogueCode, searchTerm).subscribe(data => {
            this.showLoader = false;
            this.exportCodeSelected = searchTerm;
            this.exportDetailList = [];
            console.log(data.listOfCatalogFiles);
            // data.listOfCatalogFiles = ' [ { "filename": "amazon-default-20102017200003.csv", "fileDate": "20/10/2017 20:00:03", "downloadFilePath": "https://mdev.xxwmm.wholesaleweb.catalogue.eeeeeeeeee.s3-eu-west-1.amazonaws.com/wholesaleinboundcatalogue/pending/amazon-default-20102017200003.csv" },{ "filename": "amazon-default-20102017200003.csv", "fileDate": "20/10/2017 20:00:03", "downloadFilePath": "https://mdev.xxwmm.wholesaleweb.catalogue.eeeeeeeeee.s3-eu-west-1.amazonaws.com/wholesaleinboundcatalogue/pending/amazon-default-20102017200003.csv" },{ "filename": "amazon-default-20102017200003.csv", "fileDate": "20/10/2017 20:00:03", "downloadFilePath": "https://mdev.xxwmm.wholesaleweb.catalogue.eeeeeeeeee.s3-eu-west-1.amazonaws.com/wholesaleinboundcatalogue/pending/amazon-default-20102017200003.csv" },{ "filename": "amazon-default-20102017200003.csv", "fileDate": "20/10/2017 20:00:03", "downloadFilePath": "https://mdev.xxwmm.wholesaleweb.catalogue.eeeeeeeeee.s3-eu-west-1.amazonaws.com/wholesaleinboundcatalogue/pending/amazon-default-20102017200003.csv" },{ "filename": "amazon-default-20102017200003.csv", "fileDate": "20/10/2017 20:00:03", "downloadFilePath": "https://mdev.xxwmm.wholesaleweb.catalogue.eeeeeeeeee.s3-eu-west-1.amazonaws.com/wholesaleinboundcatalogue/pending/amazon-default-20102017200003.csv" } ] ';

            if (data.listOfCatalogFiles == undefined) {
                this.exportdetailListLength = 0;
            } else {
                this.hitNext = true;
                this.exportdetailListLength = data.listOfCatalogFiles.length;
            }
            console.log(JSON.stringify(data.listOfCatalogFiles));
            this.exportDetailList = (data.listOfCatalogFiles);
            console.log(this.exportDetailList);

            if (window.innerWidth < this._globalFunc.stdWidth) {
                document.getElementById("openModalResult").click();
            } else {
                this.exportResult = false;
            }

            //for the scroll top
            var exptbl = document.getElementById('exportable');
            if (exptbl.scrollTop > 0) {
                document.getElementById('exportable').scrollTop = 0;
            }
        }
            , err => {
                this._globalFunc.logger("Exported catalog service Failed");
                this.catalogueno = '';
                this.showLoader = false;
                this.exportdetailListLength = 0;
              //  this._router.navigate(['/error404']);
            }
        );
    }

    // use this function to generate catalgoue file api event
    generateExport(catalogueType: any) {

        this.showLoader = true;

        //checking process continue or not
        
        let val : any = {'catalogueType' : 'stores'};

        let str = val.catalogueType;
       
        let searchTerm = '';
        if (str.search(':') != -1) {
            // console.log("adf");
            var strArr = str.split(':');
            searchTerm = strArr[1];
        } else {
            searchTerm = val.catalogueType;
        }

        this._postsService.getlistofCatalogeFiles(this.orgApiName, val.catalogueCode, searchTerm).subscribe(data => {
                       let checkStatus : boolean = false;

            // console.log('data___ : ' + JSON.stringify(data));
            for (let d of data.listOfCatalogFiles){
               // console.log('data1___ : ' + JSON.stringify(d));
                //if(d.status == 'In Progress' && d.userName == sessionStorage.getItem('name')){
                if(d.status == 'In Progress'){                    
                    checkStatus = true;
                    break;
                }
            }

            if(checkStatus){
               this.showLoader = false; 
               this.succMSg = "Export is in progress, you cannot initiate another export" ;
               let sessionModal = document.getElementById('addConfirmationModal');
               sessionModal.classList.remove('in');
               sessionModal.classList.add('out');
               document.getElementById('addConfirmationModal').style.display = 'block';

            } else {

                if (catalogueType == '' || catalogueType == null) {
                    this.showLoader = false;
                    this.showErrorMsg = true;
                    this.errorMsgString = 'Please select catalogue type';
                    setTimeout(() => {
                        this.showErrorMsg = false;
                    }, 5000);
                    return false;
                } else {
                    
                    console.log(catalogueType);
                    this._globalFunc.logger("Generate catalogue service");
                    //this.singleDateFormate
                    this._postsService.generateCatalogue(this.orgApiName, catalogueType, this.singleDateFormate).subscribe(data => {
        
                        this.exportFilename = data.fileNameInProcess;
                        // data = '{"isFileExist": true,"isFileInProcess": true,"fileNameInProcess": "string"}';
        
                        // this.catalogueFilename = this.orgApiName + "-" + catalogueType + "-20171024121212.csv";
                        if (data.isFileExist == true || data.isFileExist == 'true') {
                            this.succMSg = 'Generation of Export Store is already in Progress or just completed. See file ' + this.exportFilename;
                        } else if (data.isFileInProcess == true) {
                            this.succMSg = 'Your request for initiating Store Export has been accepted. File will be generated as ' + this.exportFilename;
                        }
                        //alert(this.orgApiName + " " + catalogueType);
                        let sessionModal = document.getElementById('addConfirmationModal');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('addConfirmationModal').style.display = 'block';
        
                        setTimeout(()=>{
                            let val : any = {'catalogueType' : 'stores'};
                            this.getlistofExportFiles(val);
                            this.showLoader = false;
                        }, 4000);
                        
        
                    }, err => {
                        this._globalFunc.logger("Generate catalogue service Failed");
                        this.exportmasterListLength = 0;
                        this.showLoader = false;
                        this._router.navigate(['/error404']);
                    });
                }
                //some coding
            }
           // debugger;
        
        }, err => {
            this._globalFunc.logger("get catalogue service Failed");
            this.exportmasterListLength = 0;
            this.showLoader = false;
            
              this._postsService.generateCatalogue(this.orgApiName, catalogueType, this.singleDateFormate).subscribe(data => {
        
                        this.exportFilename = data.fileNameInProcess;
                        // data = '{"isFileExist": true,"isFileInProcess": true,"fileNameInProcess": "string"}';
        
                        // this.catalogueFilename = this.orgApiName + "-" + catalogueType + "-20171024121212.csv";
                        if (data.isFileExist == true || data.isFileExist == 'true') {
                            this.succMSg = 'Generation of Export Store is already in Progress or just completed. See file ' + this.exportFilename;
                        } else if (data.isFileInProcess == true) {
                            this.succMSg = 'Your request for initiating Store Export has been accepted. File will be generated as ' + this.exportFilename;
                        }
                        //alert(this.orgApiName + " " + catalogueType);
                        let sessionModal = document.getElementById('addConfirmationModal');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('addConfirmationModal').style.display = 'block';
        
                        setTimeout(()=>{
                            let val : any = {'catalogueType' : 'stores'};
                            this.getlistofExportFiles(val);
                            this.showLoader = false;
                        }, 4000);
                        }, err => {
                        this._globalFunc.logger("Generate catalogue service Failed");
                        this.exportmasterListLength = 0;
                        this.showLoader = false;
                        this._router.navigate(['/error404']);
        
                    });
         //   this._router.navigate(['/error404']);
        });

        //end checking process

        
    }

   
    // to show specific content in table 
    getMinValue(mapValues: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < mapValues.length; i++) {
            if ((mapValues[i].type).toUpperCase() == 'MIN') {
                dataAvail = true;
                return mapValues[i].values[0].value;
            }
        }

        if (!dataAvail)
            return this.strInfoNotAvail;
    }

    // to show specific CLIENTID value if avaliable
    getAsinValue(mapValues: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < mapValues.length; i++) {
            if ((mapValues[i].type).toUpperCase() == 'CLIENTID') {
                dataAvail = true;
                return mapValues[i].values[0].value;
            }
        }

        if (!dataAvail)
            return '-';
    }

    // to show specific content in table 
    getWholesalePriceValue(pricesValues: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < pricesValues.length; i++) {
            if ((pricesValues[i].type).toUpperCase() == 'WSP') {
                /* console.log(this.formatDate(pricesValues[i].values[0].start));
                console.log(this.formatDate(this.todayDate.toDateString()));

                 console.log(this.formatDate(pricesValues[i].values[0].end));
                console.log(this.formatDate(this.todayDate.toDateString())); */
                if ((pricesValues[i].values[0].currency).toUpperCase() == 'GBP' && (this.formatDate(pricesValues[i].values[0].start) <= this.formatDate(this.todayDate.toDateString())) && (this.formatDate(pricesValues[i].values[0].end) >= this.formatDate(this.todayDate.toDateString()))) {
                    {
                        dataAvail = true;
                        return pricesValues[i].values[0].value;
                    }
                }
            }
        }
        if (!dataAvail)
            return this.strInfoNotAvail;
    }
    // to show specific content in table 
    getWholesalePackSizeValue(pricesValues: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < pricesValues.length; i++) {
            if ((pricesValues[i].type).toUpperCase() == 'CS') {
                if (this.formatDate(pricesValues[i].values[0].start) <= this.formatDate(this.todayDate.toDateString()) && this.formatDate(pricesValues[i].values[0].end) >= this.formatDate(this.todayDate.toDateString())) {
                    {
                        dataAvail = true;
                        return pricesValues[i].values[0].value;
                    }
                }
            }
        }

        if (!dataAvail)
            return this.strInfoNotAvail;
    }
    // to show specific content in table 
    getWholesalePackTypeValue(pricesValues: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < pricesValues.length; i++) {
            if (pricesValues[i].type.toUpperCase() == 'CS' || pricesValues[i].type.toUpperCase() == 'EA') {
                dataAvail = true;
                // return pricesValues[i].values[0].value;
                return pricesValues[i].type.toUpperCase();
            }
        }

        if (!dataAvail)
            return this.strInfoNotAvail;
    }

    // format date function
    formatDate(createdAtDate: string) {
        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
            if (createdAtDate.indexOf('/') >= 0) {
                return createdAtDate;
            } else {
                var d = new Date(createdAtDate);
                var yyyy = d.getFullYear();
                var mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1); // getMonth() is zero-based
                var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
                // var hh = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
                // var min = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
                return yyyy + "/" + mm + "/" + dd;
            }
        } else {
            return '-';
        }
    }

    closePopup() {
        let sessionModal = document.getElementById('addConfirmationModal');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('addConfirmationModal').style.display = 'none';
    }

    getCustomerImage(custName:string){
        return this._globalFunc.getCustomerImage(custName);
    }

    getUserSession(customerName:any, loggedInUserGroup:any, loggedInUserId:any){
        this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData:any) => {
            if(!userData.isSessionActive) { // false
                // User session has expired.
                sessionStorage.clear();
                this._router.navigate(['']);
            } else {
                this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                let changeOrgAvailable: any = [];
                let orgImgUrl = "";
                let orgApiNameTemp = "";
                this.organisationList.forEach(function (item: any) {
                    if (item.customerName !== customerName) {
                        changeOrgAvailable.push(item);
                    } else {
                        orgImgUrl = item.customerName+".png";
                        orgApiNameTemp = item.customerName;
                    }
                });
                this.organisationListChange = changeOrgAvailable;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                // this.disabledCusChange = false;
                if (this.organisationList.length == 1) {
                    this.disabledCusChange = true;
                }

                console.log("this.organisationList234");
                console.log(this.organisationList);
                let that = this;
                let catalogueBulkExportView = true;
                let claimsReview = true;
                let dashboardMenuPermission = false;
                let userHavingPermission = false;
                this.organisationList.forEach(function (organisation: any) {
                    
                    if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                        console.log("organisation : "+that.customerName);
                        console.log(organisation);
                        organisation.permissionRoleList.forEach(function(permRoleList: any){
                            console.log("11");
                            console.log(permRoleList);
                            permRoleList.permissionDetailsList.forEach(function(permRoleDtlList: any){
                                console.log("22");
                                if(permRoleDtlList.permissionGroup == 'catalogueManagement'){
                                    userHavingPermission = true;
                                    permRoleDtlList.permissionNameList.forEach(function(permList: any){
                                        if(permList.permissionName == 'catalogueManagement' && permList.permissionStatus == 'active'){
                                            dashboardMenuPermission = true;
                                        }

                                        switch(permList.permissionName){
                                            case "catalogueBulkExportView":
                                                console.log("edit catalogue permission");
                                                catalogueBulkExportView = true;
                                                //alert(filfilment);
                                                break;
                                        }
                                    });
                                }
                            });
                        });
                    }
                });
                that = null;
                if(!catalogueBulkExportView){
                    // redirect to order
                    this._router.navigate(["cataloguemanagement",customerName]);
                }
            }
            //alert(this.disabledfilfilment);
        });
    }


    
}