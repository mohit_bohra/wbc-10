import { NgModule } from '@angular/core';
import { StoreexportComponentRoutes } from './storeexport.routes';
import { StoreexportComponent } from './storeexport.component';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../../global/global.component';
import { Services } from '../../../../services/app.services';
import { FormsModule } from '@angular/forms';
//import { NavigationModule } from '../../navigation/navigation.module';
import { NavigationModule } from '../../../navigationFulfilment/navigation.module';
import { commonfunction } from '../../../../services/commonfunction.services';
import { CatalogueManagePendingHeaderModule } from '../../../cataloguemanagement/cataloguemanagependingheader/cataloguemanagependingheader.module';


import { Daterangepicker } from 'ng2-daterangepicker';

@NgModule({
  imports: [
    StoreexportComponentRoutes,
    CommonModule,
    FormsModule,
    InfiniteScrollModule,
    NavigationModule,
    Daterangepicker,
    CatalogueManagePendingHeaderModule
  ],
  exports: [],
  declarations: [StoreexportComponent],
  providers: [Services, GlobalComponent,commonfunction]
})
export class StoreexportModule { }
