import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreexportComponent } from './storeexport.component';

// route Configuration
const storeExportRoutes: Routes = [
  { path: '', component: StoreexportComponent },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(storeExportRoutes)],
  exports: [RouterModule]
})
export class StoreexportComponentRoutes { }
