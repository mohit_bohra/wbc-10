import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../../../services/app.services';
import { Constant } from '../../../constants/constant';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { GlobalComponent } from '../../../global/global.component';
import { commonfunction } from '../../../../services/commonfunction.services';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DialogService } from '../../../../services/dialog.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Category } from "../../models/categories"
import { DeliveryOpportunity } from "../../models/deliveryOpportunities"
import { Merchants } from '../../models/merchants';
import { transitInformationForAmazon } from '../../models/transitInformationForAmazon';


declare let saveAs: any;

@Component({
    templateUrl: 'editstore.component.html',
    providers: [commonfunction]
})
export class EditstoreComponent {
    @ViewChild('editstore') editstore: any;

    category: Category;
    deliveryOpportunity: DeliveryOpportunity;
    errorMsgString: string

    storeDescription: string = "";
    strInfoNotAvail: string = "";
    storeIdPrefix: any;
    storeId: any;
    customerDispayName: string;
    storedetails: any;
    style: string;
    disabledCusChange: boolean;
    organisationListChange: any;
    hitNext: boolean;
    count: number;
    start: number;
    storedetailListLength: number;
    limit: any;

    storeid: any;
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;

    showLoader: boolean;
    private _sub: any;
    hideOrganisation: boolean = true;

    custPermission: any[];
    makeEffectiveToday: boolean = false;
    makeChangeToday: boolean = false;

    imgName: string = "";
    storeName: string = "";
    storeAvailability: string = "";
    tradingName: string = "";
    storeAreaCode: string = "";
    storeAreaManager: string = "";
    storeregionCode: string = "";
    storeregionManager: string = "";
    storeAllocationP: string = "";
    storeStatus: string = "";
    storeeffDate: string = "";
    userName: string = "";
    storeAddressName: string = "";
    storeAddressLine1: string = "";
    storeAddressLine2: string = "";
    storeCity: string = "";
    storeCounty: string = "";
    storeCountry: string = "";
    storePostCode: string = "";
    storeContactEmail: string = "";
    storeContactFAX: string = "";
    storeContactPhone: string = "";
    effectiveImmediately: boolean = false;
    allocationPriority: any = [];
    supplyingDepot: any;
    filterName: any;
    filterValue: any;
    supplyDeportCategory: any;
    supplyDeportCategoryLen: any = -1;
    availabilityMaster: any = [];
    statusMaster: any = [];
    portMaster: any = [];
    availabilityStatus: any = "ACTIVE";
    terminateFlg: boolean;
    supplyDepotType: any;
    merchantId : any = '';
    storeAlternateIds: any;
    
    storeDtl: any = this._route.snapshot.data['storeDtl'].stores[0];

    monY: number = 0;
    tueY: number = 0;
    wedY: number = 0;
    thuY: number = 0;
    friY: number = 0;
    satY: number = 0;
    sunY: number = 0;
    historicalCategory: any = "";
    storeCategoryIndex: number;
    calDefaultDate: any;
    DOCategoriesCurrentRow: any[];
    minDate: any = new Date();
    currentCalId: string;
    DOCategoriesTomorrowRow: any[];
    editEffectiveDate: boolean;
    popupMsg: string;
    supplyDepotCatData: any = [];
    deliveryOpportunityArray: any[];
    disableAddNewRow: boolean = false;
    confirmBoxResponse: boolean;
    tomorrowDate: string;
    todayDate: string;
    selectedCategoryIndex4Cal: number;
    errorMsgList: any = {};
    errorCount: number = 0;
    tooltipMsg: any;
    storeArray: any;
    disableSave: boolean = true;
    today: Date = new Date();
    tomorrow: Date = new Date();
    public options: any = {
        locale: { format: 'DD/MM/YYYY' },
        alwaysShowCalendars: false,
        singleDatePicker: true,
        "drops": "up"
    };
    fragment: string = 'maindetails';
    dummyArraDO: any = {};
    validationMsgDO_YN: string = 'Enter Capital Y/N only and 7 char long';
    validationMsgDO_YN_Required: string = 'Delivery Days is required.';
    LatestDOArray: any[] = [];
    editableDO: any[] = [];
    editableDOArray: any[] = [];
    addNewDOMode: boolean = false;
    curCategory: any;
    DOCategoriesWithoutCurrentRow: any = [];
    supplyingdepotchangeflag: boolean = false;
    makeEffectivePermission: boolean;
    fieldChange: boolean = false;
    storeData: any;
    mon: number = 0;
    tue: number = 0;
    wed: number = 0;
    thu: number = 0;
    fri: number = 0;
    sat: number = 0;
    sun: number = 0;
    supplyingDepoChange: boolean = false;
    clickableCategory: boolean = true;
    iseffectivetoday:boolean = false;
    checkoncedeletedflag:boolean = false;

    isAmazonCatelogType:boolean = false;
    amazonRegionCodesData:any;
    
    cataloguemasterListLength: any;
    
     //Dot co.uk Prime Changes
     merchants : Merchants[];
     merchantName : any[];
     merchantIDForPrimeNow : string ;
     merchantIDForCODOTUK : string;   
     rmsSellingLocationForPrimeNow : string;
     rmsSellingLocationForCODOTUK : string;
     inventoryApplicableForPrimeNow : boolean ;
     inventoryApplicableForCODOTUK : boolean ;
     priceApplicableForPrimeNow: boolean = false;
     priceApplicableForCODOTUK : boolean = false;
     settlementReportForPrimeNow: boolean = false;
     settlementReportForCODOTUK : boolean = false;
     isCheckedInventoryForPrimeNow : boolean = false;
     isCheckedInventoryForCODOTUK : boolean = false;
     isCheckedPriceForPrimeNow : boolean = false;
     isCheckedPriceForCODOTUK : boolean = false;
     isCheckedSettlementForPrimeNow : boolean = false;
     isCheckedSettlementForCODOTUK : boolean = false;    
     merchantforPrime : Merchants;
     merchantforCODOTUK : Merchants;
     transitInformationforPrime: transitInformationForAmazon;
     transitInformationforCODOTUK: transitInformationForAmazon;
     marketplaceforPrimeNow : any;
     marketplaceforCODOTUK : any;
     MerchantIdForHeader : any;
     merchantforStorepick : boolean = false;
     threshold : any;


    //Constructor is used to initialize variables during load time
    constructor(private _postsService: Services, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _globalFunc: GlobalComponent, public _cfunction: commonfunction, private formBuilder: FormBuilder, public dialogService: DialogService, private daterangepickerOptions: DaterangepickerConfig) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.merchantName = this.objConstant.merchantName;
        this.marketplaceforPrimeNow = this.objConstant.marketPlaceIdForPrimeNow;
        this.marketplaceforCODOTUK = this.objConstant.marketPlaceIdCODOTUK;        

        this.filterName = 'supplydepot';
        this.filterValue = '@all';
        this.editstore = this.formBuilder.group({});

        this.tomorrow = new Date();
        this.tomorrow.setDate(this.today.getDate() + 1);

        this.daterangepickerOptions.settings = {
            minDate: this.tomorrow,
        };

        if(this.storeDtl.tradingName == "AMAZON"){ 
            
            this.MerchantIdForHeader = this.storeDtl.merchants[0].merchantId;
            console.log("MerchantIDHeader"+this.MerchantIdForHeader);
        
       }        

        
       if(this.storeDtl.tradingName == "AMAZON STORE PICK"){
        this.merchantforStorepick = true;
        this.merchants = this.storeDtl.merchants;    
        this.threshold = this.storeDtl.minimumExpetedInventory;
        console.log(this.storeDtl.minimumExpetedInventory);
        for(let obj of this.storeDtl.merchants)
        {
           
            this.MerchantIdForHeader = obj.merchantId;
            console.log("MerchantIDD"+this.MerchantIdForHeader);
            if(obj.merchantName == "Prime Now")
            {
                this.merchantIDForPrimeNow = obj.marketplaceId;               
                this.rmsSellingLocationForPrimeNow = obj.transitInformation.virtualSellingLocation;
                console.log(obj.transitInformation.virtualSellingLocation);
                this.marketplaceforPrimeNow = obj.marketplaceId;               
                this.inventoryApplicableForPrimeNow = obj.inventoryApplicable;
                if(this.inventoryApplicableForPrimeNow)
                {
                    this.isCheckedInventoryForPrimeNow = true;
                }
                this.settlementReportForPrimeNow = obj.settlementReportApplicable;
                if(this.settlementReportForPrimeNow)
                {
                    this.isCheckedSettlementForPrimeNow = true;
                }
                this.priceApplicableForPrimeNow = obj.priceUpdateApplicable;
                if(this.priceApplicableForPrimeNow)
                {
                    this.isCheckedPriceForPrimeNow = true;
                }
                this.transitInformationforPrime = new transitInformationForAmazon(this.rmsSellingLocationForPrimeNow,this.storeDtl.storeId)
                this.merchantforPrime  = new Merchants(this.merchantIDForPrimeNow,"Prime Now","AAAAA",this.marketplaceforPrimeNow,this.inventoryApplicableForPrimeNow,this.priceApplicableForPrimeNow,this.settlementReportForPrimeNow,this.transitInformationforPrime);
            }
            if(obj.merchantName == ".CO.UK")
            {
                this.merchantIDForCODOTUK = obj.marketplaceId;             
                this.rmsSellingLocationForCODOTUK = obj.transitInformation.virtualSellingLocation;               
                this.marketplaceforCODOTUK = obj.marketplaceId;
                this.inventoryApplicableForCODOTUK = obj.inventoryApplicable;
                if(this.inventoryApplicableForCODOTUK)
                {
                    this.isCheckedInventoryForCODOTUK = true;
                }
                this.settlementReportForCODOTUK = obj.settlementReportApplicable;
                if(this.settlementReportForCODOTUK)
                {
                    this.isCheckedSettlementForCODOTUK = true;
                }
                this.priceApplicableForCODOTUK = obj.priceUpdateApplicable;
                if(this.priceApplicableForCODOTUK)
                {
                    this.isCheckedPriceForCODOTUK = true;
                }

                this.transitInformationforCODOTUK = new transitInformationForAmazon(this.rmsSellingLocationForCODOTUK,this.storeDtl.storeId)
                this.merchantforCODOTUK  = new Merchants(this.merchantIDForCODOTUK,".CO.UK","AAAAA",this.marketplaceforCODOTUK,this.inventoryApplicableForCODOTUK,this.priceApplicableForCODOTUK,this.settlementReportForCODOTUK,this.transitInformationforCODOTUK);      

            }          
           
        
       }
        }
     //  this.merchants.push(this.merchantforPrime,this.merchantforCODOTUK);
       console.log(this.merchantforCODOTUK);
       console.log(this.storeDtl);
    }

    checkInventoryApplicableforPrime(event:any)
    {
    if(event == "true")
        {           
        this.inventoryApplicableForPrimeNow = true;
        this.isCheckedInventoryForPrimeNow = true;
        }
    if(event == "false")
        {                   
        this.inventoryApplicableForPrimeNow = false;
        this.isCheckedInventoryForPrimeNow = false;
            
        }    
    }

    checkInventoryApplicableforCODOTUK(event:any)
    {
    if(event == "true")
        {           
        this.inventoryApplicableForCODOTUK = true;
        this.isCheckedInventoryForCODOTUK = true;
        }
    if(event == "false")
        {                   
        this.inventoryApplicableForCODOTUK = false;
        this.isCheckedInventoryForCODOTUK = false;
            
        }    
    }

    checkPriceApplicableforPrime(event:any)
    {
    if(event == "true")
    {      
        this.priceApplicableForPrimeNow = true;
        this.isCheckedPriceForPrimeNow = true;
    }
    if(event == "false")
    {              
        this.priceApplicableForPrimeNow = false;
        this.isCheckedPriceForPrimeNow = false;        
    }

    }

    checkPriceApplicableforCODOTUK(event:any)
    {
        if(event == "true")
        {      
            this.priceApplicableForCODOTUK = true;
            this.isCheckedPriceForCODOTUK = true;
        }
        if(event == "false")
        {              
            this.priceApplicableForCODOTUK = false;
            this.isCheckedPriceForCODOTUK = false;        
        }
    }

    checkSettlementReportforPrime(event:any)
    { 

    if(event == "true")
    {       
        this.settlementReportForPrimeNow = true;      
        this.isCheckedSettlementForPrimeNow = true;
    }
    if(event == "false")
    {
        this.settlementReportForPrimeNow = false;  
        this.isCheckedSettlementForPrimeNow = false;
    }
   }

   checkSettlementReportforCODOTUK(event:any){  

    if(event == "true")
    {   
        this.settlementReportForCODOTUK = true;
        this.isCheckedSettlementForCODOTUK = true;
    }
    if(event == "false")
    {   
        this.settlementReportForCODOTUK = false;
        this.isCheckedSettlementForCODOTUK = false;
         
    }


    }




    changeImmediately() {
        console.log('change ' + this.makeEffectiveToday);

        if (this.makeEffectiveToday == false) {

            document.getElementById('immediatelyModalPageLevel').style.display = 'block';


        } else {

            this.showLoader = true;
            var element = <HTMLInputElement>document.getElementById("addnewButton");

            if (element.disabled) {
                element.disabled = false;

            }

            console.log(' revert data ' + JSON.stringify(this.storeDtl.categories));

            document.getElementById('reverteffectModalPageLevel').style.display = 'block';
            this.showLoader = false;
            // window.location.reload();
            // let navigationExtras: NavigationExtras = {
            //     fragment: this.fragment
            // };

            //this._router.navigate(['/', 'store', this.customerName, 'storedetails', this.storeid, 'edit'], navigationExtras);

        }
        // this.makeEffectiveToday = !this.makeEffectiveToday;
    }

    noredirect() {
        var element = <HTMLInputElement>document.getElementById("effectiveImmediatelyId");

        element.checked = true;

        var elementbutton = <HTMLInputElement>document.getElementById("addnewButton");

        elementbutton.disabled = true;

        this.makeEffectiveToday = true;
        this.makeChangeToday = true;

        document.getElementById('reverteffectModalPageLevel').style.display = 'none';
    }

    yesredirect() {
        this.showLoader = true;
        document.getElementById('reverteffectModalPageLevel').style.display = 'none';
        window.location.reload();
    }

    yesEffect() {
        document.getElementById('immediatelyModalPageLevel').style.display = 'none';

        this.makeEffectiveToday = true;
        this.makeChangeToday = true;

        this.daterangepickerOptions.settings = {
            minDate: this.today,
        };

        //code for the loop of Historical and future data
        console.log('data ' + JSON.stringify(this.storeDtl.categories));
        var catName: string ='';
        for (let cat of this.storeDtl.categories) {
            catName = cat.name;
            for (let delivery of cat.deliveryOpportunities) {
                console.log(' Del   ' + JSON.stringify(delivery));
                delivery.calender = true;
               // debugger;
                if (delivery.isEditable != undefined && delivery.isEditable == 'Yes') {

                    if (delivery.when === undefined && delivery.value!="") {
                        if (this.makeEffectiveToday) {
                            delivery.effectiveDate = this.todayDate;
                            delivery.calender = false;
                        }
                        delivery.deleteicon = true;
                    } else if (delivery.when != undefined) {
                            console.log('when undefined');
                            console.log('edit Arr' + Object.keys(this.dummyArraDO).length + ' ' +  JSON.stringify(this.dummyArraDO));
                            if(Object.keys(this.dummyArraDO).length > 0){
                                console.log('delivery Arr ' + JSON.stringify(delivery));
                                console.log('cate Name : ' + catName);
                                console.log('object ' + JSON.stringify(Object.keys(this.dummyArraDO)));
                                //if(this.dummyArraDO[])
                                if (this.dummyArraDO[catName] != undefined && this.dummyArraDO[catName].length != 0) {
                                    //delivery.effectiveDate = this.todayDate;
                                    delivery.calender = false;
                                }
                            }

                    } else {

                    }

                } else {
                    delivery.deleteicon = false;

                }
            }
        }

        console.log('New data ' + JSON.stringify(this.storeDtl.categories));

    }

    noEffect() {

        document.getElementById('immediatelyModalPageLevel').style.display = 'none';
        // document.getElementById('effectiveImmediatelyId').removeAttribute('checked'); 
        var element = <HTMLInputElement>document.getElementById("effectiveImmediatelyId");

        element.checked = false;

        this.makeEffectiveToday = false;
        this.makeChangeToday = false;

    }

    ngOnInit() {
       
        this.showLoader = true;
        this.supplyingDepoChange = false;
        this.clickableCategory = true;
        sessionStorage.setItem('gridModelChange',' false');
       
       
        this._globalFunc.logger("Catalogue search screen loading.");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {
            sessionStorage.setItem("unsavedChanges", "false");
            sessionStorage.setItem("gridunsavedChanges", "false");            
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            this._route.fragment.subscribe(fragment => { this.fragment = fragment != null ? fragment : this.fragment; });
            
            this._globalFunc.autoscroll();
            this.limit = this.objConstant.SCROLL_RECORD_LIMIT;

            this.changeDateFormat();

            this._sub = this._route.params.subscribe((params: { orgType: string, storeid: any }) => {
                this.customerName = params.orgType;
                this.storeid = params.storeid;
                this.editableDOArray = [];
                console.log("this.fragment==> "+this.fragment);
                this.selectTab(this.fragment);
                this.imgName = this.getCustomerImage(params.orgType);
                if (this.customerName === 'amazon') {
                    this.isAmazonCatelogType = true;
                    // if(!this.storeDtl.storeAlternateIds){
                    //     this.storeDtl.storeAlternateIds = {'merchantId': this.merchantId};
                    // }
                    //this.merchantId = this.storeDtl.storeAlternateIds.merchantId;
                    this._postsService.getAllCatalogueTypes(this.customerName,'catalogueName','storepick').subscribe((data:any) => {
                        //console.log(data.storeUIEntryConfig);
                        this.showLoader = false;
                        if (data.storeUIEntryConfig.length == 0) {
                            this.cataloguemasterListLength = 0;
                            //this._globalFunc.logger("returned empty data.");
                        } else {
                            //this._globalFunc.logger("master service data found.");
                            for (var k = 0; k < data.storeUIEntryConfig.length; k++) {
                               // console.log(this._globalFunc.titleCase(data.storeUIEntryConfig[k].catalogueCode));
                                //this.amazonRegionCodesData[k] = data.storeUIEntryConfig[k].catalogueCode;
                                 data.storeUIEntryConfig[k].catalogueCode = this._globalFunc.titleCase(data.storeUIEntryConfig[k].catalogueCode);
                               
                               
                            }
                            this.cataloguemasterListLength = data.storeUIEntryConfig.length;
                        }
                        console.log(data.storeUIEntryConfig);
                        this.amazonRegionCodesData = data.storeUIEntryConfig;
                    }
                        , (err:any) => {
                            this._globalFunc.logger("catalogue config service failed.");
                            this.cataloguemasterListLength = 0;
                            this.showLoader = false;
                            this._router.navigate(['/error404']);
                        }
                    );
                }
                else{
                    this.isAmazonCatelogType = false;
                   // this.storeAlternateIds;
                }

                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                let cusType = params.orgType;

                this.storeIdPrefix = this.objConstant.storePrefix[cusType.toLowerCase()];

                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
                    if (res == 'sessionActive') {
                        this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                    }
                }, reason => {
                    //console.log(reason);
                });
                this.editableDO = [];
            });

            this.getAvailability();
            this.getStatus();
            this.getPort();
            this.supplyingDepot = this.getsupplyingDepot();
            this.getSupplyDepotCatData();
            this.getStoreDetailsForCurrentTable();
            this.filterStoreData().then(res => {

                this.showLoader = false;
            });
        }
    }


    getStoreDetailsForCurrentTable(): any {
        this.showLoader = true;
        console.log(this.storeid);
        let searchTerm = 'stores';
        this.start = 0;
        this.count = 0;
        // console.log(value);
        this._globalFunc.logger("Calling catalogue list service.");
        this._postsService.getStoreItem(this.customerName, this.storeid).subscribe((data: any) => {
            console.log(data);
            this.storeData = data;
            let that = this;
            this.userName = sessionStorage.getItem("username");
            this.storeData.stores[0].categories = this.storeData.stores[0].categories.sort((a: any, b: any) => {
                if (a.name > b.name) {
                    return 1;
                } else if (a.name < b.name) {
                    return -1;
                } else {
                    return 0;
                }
            });
            if(this.isAmazonCatelogType)
            {
                this.merchants = this.storeData.stores.merchants ;
                console.log("Merchants"+this.storeData.stores.merchants)
            }
           

            this.storeData.stores[0].categories.forEach(function (itm: any) {
                console.log(itm.deliveryOpportunities[0].value.slice(0, 1));
                if (itm.deliveryOpportunities[0].value.slice(0, 1) == "Y") {
                    that.mon = that.mon + 1;
                }
                if (itm.deliveryOpportunities[0].value.slice(1, 2) == "Y") {
                    that.tue = that.tue + 1;
                }
                if (itm.deliveryOpportunities[0].value.slice(2, 3) == "Y") {
                    that.wed = that.wed + 1;
                }
                if (itm.deliveryOpportunities[0].value.slice(3, 4) == "Y") {
                    that.thu++;
                }
                if (itm.deliveryOpportunities[0].value.slice(4, 5) == "Y") {
                    that.fri++;
                }
                if (itm.deliveryOpportunities[0].value.slice(5, 6) == "Y") {
                    that.sat++;
                }
                if (itm.deliveryOpportunities[0].value.slice(6, 7) == "Y") {
                    that.sun++;
                }
            });


        }
            , (err: any) => {
                this._globalFunc.logger("Store serch service failed.");
                this.storedetailListLength = 0;
                this._router.navigate(['/error404']);
            }
        );

        this.showLoader = false;
    }

    filterStoreData(selCat?: string) {
        return new Promise((resolve) => {
            let that = this;
            this.storeDtl.availability = this.storeDtl.availability.toUpperCase();
            this.storeDtl.categories.forEach(function (itm: any) {
                //if(itm.deliveryOpportunities.length>0){}    
                console.log(JSON.stringify(itm.deliveryOpportunities));
                console.log(itm.deliveryOpportunities[0].value);
                // debugger;      
                that.populateYN(itm.deliveryOpportunities[0].value);
                itm.deliveryOpportunities = that.sortDO(itm.deliveryOpportunities);
                that.LatestDOArray[itm.name] = itm.deliveryOpportunities[0].transitInformation;
            });
            this.supplyDeportCategoryLen = this.storeDtl.categories.length;

            let perms = JSON.parse(sessionStorage.getItem('tempPermissionArray'));
            if (perms[this.customerName]['catalogueManagement'].indexOf('editEffectiveDate') != -1) {
                this.editEffectiveDate = true;
            } else {
                this.editEffectiveDate = false;
            }

            if (perms[this.customerName]['catalogueManagement'].indexOf('makeEffectiveImmediatelyFlag') != -1) {
                this.makeEffectivePermission = true;
            } else {
                this.makeEffectivePermission = false;
            }
            // this.makeEffectivePermission = true; //tmp

            this.DOCategoriesTomorrowRow = [];
            this.checkTomorrowRowExist();
            console.log('name ' + this.storeDtl.categories[0].name);
            if (selCat != null || selCat != undefined) {
                this.populateHistoricalFutureDO(selCat,-1);
            } else {
                this.populateHistoricalFutureDO(this.storeDtl.categories[0].name);
            }

            this.DOCategoriesCurrentRow = [];
            this.getCurrentDOPerCategory();
            this.getAllocationPriorityDD();
            let isoDate = new Date()
            let tomorrowISODate = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1, 0, 0, 0));

            let todayISODate = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 0, 0, 0));

            this.tomorrowDate = this._cfunction.dateFormator(tomorrowISODate, 1);

            this.todayDate = this._cfunction.dateFormator(todayISODate, 1);

            this.findRowsEditable();
            console.log("this.editableDOArray");
            console.log(this.editableDOArray);
            return resolve(true);
        });
    }

    sortDO(array: any) {
        array.sort((a: any, b: any) => {
            let temp_a = a.effectiveDate.split("/");
            let temp_b = b.effectiveDate.split("/");
            let aDate_when = a.when;
            let bDate_when = b.when;

            let aDate = new Date(temp_a[2], temp_a[1] - 1, temp_a[0], 0, 0, 0);
            let bDate = new Date(temp_b[2], temp_b[1] - 1, temp_b[0], 0, 0, 0);
            //let aDate_when = new Date(temp_a_when[2], temp_a_when[1] - 1, temp_a_when[0], 0, 0, 0);
            //let bDate_when = new Date(temp_b_when[2], temp_b_when[1] - 1, temp_b_when[0], 0, 0, 0);

            if (aDate.getTime() == bDate.getTime()) {
                if (aDate_when != undefined && bDate_when != undefined) {
                    if (aDate_when.getTime() < bDate_when.getTime()) {
                        return 1;
                    } else if (aDate_when.getTime() > bDate_when.getTime()) {
                        return -1;
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                }
            } else if (aDate.getTime() < bDate.getTime()) {
                return 1;
            } else if (aDate.getTime() > bDate.getTime()) {
                return -1;
            } else {
                return 0;
            }
        });
        return array;
    }

    sortSupplyingDepot() {
        return new Promise((resolve) => {
            let tempArr = this.supplyingDepot.sort((a: any, b: any) => {
                if (a.supplyingDhlDepotName.toUpperCase() < b.supplyingDhlDepotName.toUpperCase()) {
                    return -1;
                } else if (a.supplyingDhlDepotName.toUpperCase() > b.supplyingDhlDepotName.toUpperCase()) {
                    return 1;
                } else {
                    return 0;
                }
            });
            this.supplyingDepot = tempArr;
            resolve(true);
        });
    }

    findRowsEditable() {
        let that = this;
        var respondedStatus: String = "Yes";
        
        this.storeDtl.categories.forEach(function (itm: any) {
            itm.deliveryOpportunities.forEach(function (row: any) {
                respondedStatus = that.showCalendarTextBox(row.effectiveDate, itm.name);
                if (respondedStatus == "Yes") {
                    row.isEditable = respondedStatus;
                }
            });
        });
    }

    canDeactivate(): Promise<boolean> | boolean {
        if (!this.editstore.dirty) {
            return true;
        }
        this.confirmBoxResponse = this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');

        return this.confirmBoxResponse;
    }

    getAvailability() {
        this.availabilityMaster = ["ACTIVE", "INACTIVE"];
    }

    getStatus() {
        this.statusMaster = ["rollout"]
    }
    getPort() {
        this.portMaster = ["St Helier", "St Peter Port"]
    }
    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    getSupplyDepotCatData(cat?: string) {
        if (cat === undefined) {
            this.supplyDepotCatData = [];
        }
        let that = this;

        // if(cat != undefined){
        //     that.supplyDepotCatData.categories.forEach(function(ele:any) {
        //        ele.deliveryOpportunities.forEach( function (ele1:any) {
        //             if(ele1.transitInformation.length == 0){
        //                 ele1.pop();
        //             }
        //        });
        //     });
        // }
        if (cat === undefined) {
            this.storeDtl.categories.forEach(function (itm: any) {
                console.log('@@@@@@@@@@@@@@@@@@@@@@@@@' + JSON.stringify(that.supplyDepotCatData));
                that.supplyDepotCatData.push({
                    'name': itm.name,
                    "deliveryOpportunities": [{
                        "transitInformation": {
                            "virtualSellingLocation": itm.virtualSellingLocation,
                            "supplyingDepotLocation": itm.supplyingDepotLocation,
                            "supplyingShippingLocation": itm.supplyingShippingLocation
                        }
                    }]
                });
            });
        }
    }

    getCurrentDOPerCategory() {
        let that = this;
        this.storeDtl.categories.forEach(function (itm: any, i: number) {
            itm.deliveryOpportunities.forEach(function (doRow: any, idx: number) {
                if (that.getCurrentTodayDateRecordDO(doRow.effectiveDate)) {
                    that.DOCategoriesCurrentRow[itm.name] = idx;
                }
            });
            if (typeof that.DOCategoriesCurrentRow[itm.name] === 'undefined') {
                that.DOCategoriesCurrentRow[itm.name] = that.getCurrentTodayDateNotExistRecordDO(itm.name, i);
                if (that.DOCategoriesCurrentRow[itm.name] == null) {
                    that.DOCategoriesWithoutCurrentRow.push(itm.name);
                }
            }
        });
        console.log("this.DOCategoriesWithoutCurrentRow");
        console.log(this.DOCategoriesWithoutCurrentRow);
    }

    getCurrentTodayDateNotExistRecordDO(cat: string, idx: number) {
        let isoDate = new Date();
        let todayISODateStart = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 0, 0, 0).getTime();
        let todayISODateEnd = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 23, 59, 59).getTime();
        let orgDate: number;
        let that = this;
        let dateDiff: number = null;
        let diff = 0;
        let rtnValue: number = null;
        this.storeDtl.categories[idx].deliveryOpportunities.forEach(function (doRow: any, idx1: number) {
            let temp = doRow.effectiveDate.split("/");
            //let orgDate = new Date(dt);
            orgDate = new Date(temp[2], temp[1] - 1, temp[0]).getTime();

            // Check for historical data
            if (todayISODateEnd >= orgDate) {
                //this is historical data
                if (dateDiff == null) {
                    dateDiff = (todayISODateStart - orgDate);
                    rtnValue = idx1;
                } else if (dateDiff > (todayISODateStart - orgDate)) {
                    dateDiff = (todayISODateStart - orgDate);
                    rtnValue = idx1;
                }
            } else {
                // this is future data
                // nothing to do with this
            }
        });
        return rtnValue;
    }

    getCurrentTodayDateRecordDO(dt: any) {
        let isoDate = new Date();
        let todayISODateEnd = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 23, 59, 59).getTime();
        let todayISODateStart = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 0, 0, 0).getTime();
        let temp = dt.split("/");
        //let orgDate = new Date(dt);
        let orgDate = new Date(temp[2], temp[1] + 1, temp[0]).getTime();
        if (orgDate >= todayISODateStart && orgDate <= todayISODateEnd) {
            return true;
        } else {
            return false;
        }
    }

    checkTomorrowRowExist() {
        return new Promise((resolve) => {
            let that = this;
            this.storeDtl.categories.forEach(function (itm: any) {
                itm.deliveryOpportunities.forEach(function (doRow: any, idx: number) {
                    if (that.getTomorrowDateRecordDO(doRow.effectiveDate)) {
                        that.DOCategoriesTomorrowRow[itm.name] = true;
                    }
                });
                if (typeof that.DOCategoriesTomorrowRow[itm.name] == 'undefined') {
                    that.DOCategoriesTomorrowRow[itm.name] = false;
                }
            });
        });
    }

    getTomorrowDateRecordDO(dt: any) {
        let isoDate = new Date();
        let todayISODateEnd = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1, 23, 59, 59);
        let todayISODateStart = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1, 0, 0, 0);
        let orgDate = new Date(dt);
        let temp = dt.split("/");
        orgDate = new Date(temp[2], temp[1] - 1, temp[0]);
        if (orgDate >= todayISODateStart && orgDate <= todayISODateEnd) {
            return true;
        } else {
            return false;
        }
    }

    DOCategoriesDate() {
        if (this.DOCategoriesTomorrowRow[this.historicalCategory])
            return true;
        else
            return false;
    }

    populateYN(itm: any) {
        if (itm.slice(0, 1).toUpperCase() == "Y") {
            this.monY = this.monY + 1;
        }
        if (itm.slice(1, 2).toUpperCase() == "Y") {
            this.tueY = this.tueY + 1;
        }
        if (itm.slice(2, 3).toUpperCase() == "Y") {
            this.wedY = this.wedY + 1;
        }
        if (itm.slice(3, 4).toUpperCase() == "Y") {
            this.thuY++;
        }
        if (itm.slice(4, 5).toUpperCase() == "Y") {
            this.friY++;
        }
        if (itm.slice(5, 6).toUpperCase() == "Y") {
            this.satY++;
        }
        if (itm.slice(6, 7).toUpperCase() == "Y") {
            this.sunY++;
        }
    }

    getSupplyDepotValuesOnChange(val: any) {
        this.supplyingdepotchangeflag = true;
        this.supplyingDepoChange = true;
        this._postsService.getsupplyingDepot(this.customerName, "suppDepotDetails", val).subscribe((supplyDepotDta: any) => {
            let that = this;
            that.supplyDepotCatData = [];
            supplyDepotDta.maintainSupplyDepot[0].productCategoryDetails.forEach(function (itm: any) {
                that.storeDtl.categories.forEach(function (itmStoreDtl: any) {
                    if (itmStoreDtl.name == itm.categoryName) {
                        itmStoreDtl.deliveryOpportunities[0].transitInformation.virtualSellingLocation = itm.rmsSellingStoreId;
                        itmStoreDtl.deliveryOpportunities[0].transitInformation.supplyingDepotLocation = itm.rmsWarehouseId;
                        itmStoreDtl.deliveryOpportunities[0].transitInformation.supplyingShippingLocation = itm.mainFrameStoreId;
                    }
                });
                that.supplyDepotCatData.push({
                    'name': itm.categoryName,
                    'deliveryOpportunities': [{
                        'transitInformation': {
                            "virtualSellingLocation": itm.rmsSellingStoreId,
                            "supplyingDepotLocation": itm.rmsWarehouseId,
                            "supplyingShippingLocation": itm.mainFrameStoreId
                        }
                    }]
                });
            });
        });
    }

    getsupplyingDepot() {
        this._postsService.getsupplyingDepot(this.customerName, "suppDepotDetails", this.filterValue).subscribe((supplyDepotDta: any) => {
            this.supplyingDepot = supplyDepotDta.maintainSupplyDepot;
            this.sortSupplyingDepot();
        }, (err: any) => {
            ////console.log(err);
        });
    }

    onChange(val: any) {
        if (val != '') {
            this.filterValue = val;
            this.getsupplyingDepot();
        } else {
            this.supplyDeportCategoryLen = -1;
            this.supplyDeportCategory = [];
        }
    }

    resetTodefault(val: any) {
        if (val != '') {
            this.filterValue = val;
            this.getsupplyingDepot();
        } else {
            this.supplyDeportCategoryLen = -1;
            this.supplyDeportCategory = [];
        }
    }

    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        // Session terminated;
                        reject('sessionInactive');
                        this.terminateFlg = true;
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else {
                        reject('sessionInactive')
                        sessionStorage.clear();
                        this._router.navigate(['']);
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        // Session terminated;
                        reject('sessionInactive');
                        this.terminateFlg = true;

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        resolve('sessionActive');
                        this.terminateFlg = false;
                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
                        this.organisationListChange = changeOrgAvailable;
                        this.orgimgUrl = orgImgUrl;
                        this.orgApiName = orgApiNameTemp;
                        // this.disabledCusChange = false;
                        if (this.organisationList.length == 1) {
                            this.disabledCusChange = true;
                        }
                    }
                }
            });
        });
    }

    getAllocationPriorityDD() {
        this._postsService.getAllocationPriority(this.customerName, 'dhlAllocationPriority', '@all').subscribe((data: any) => {
            this.allocationPriority = data.priorityList;
        });
    }
    filedchange() {
        this.fieldChange = true;
    }

    editStoreInfo(value: any) {
        this.supplyingDepoChange = false;
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res = 'sessionActive') {
                var errorList: any = [];
                if (Object.keys(this.errorMsgList).length == 0) {
                    errorList = this._cfunction.errorMsgList;
                } else if (Object.keys(this._cfunction.errorMsgList).length == 0) {
                    errorList = this.errorMsgList;
                } else {
                    errorList = Object.assign(this.errorMsgList, this._cfunction.errorMsgList); //this.errorMsgList.concat(this._cfunction.errorMsgList);
                }

                let msgList = Object.keys(errorList);
                this.showLoader = true;
                if (msgList.length == 0) {
                   
                    let that = this;
                    let that1 = this;
                    this.changeDateFormat2Org();
                    console.log('copy of $$$$$$$$$$$ ' + JSON.stringify(this.storeDtl))
                    let duplicateStoreDtl = JSON.parse(JSON.stringify(this.storeDtl));
                    console.log('before main : ' + JSON.stringify(duplicateStoreDtl));
                    duplicateStoreDtl.addressName= duplicateStoreDtl.storeName;
                    duplicateStoreDtl.categories.forEach(function (itm1: any) {
                        itm1.deliveryOpportunities = [];
                    });                   
                    duplicateStoreDtl.audit.who = sessionStorage.getItem('name');
                    duplicateStoreDtl.audit.when = new Date();
                    let dummyLocalArrayCategories: any = [];
                    let doArray: any = [];                    
                    var newRow: number = 0;                    
                    var StoreDtl = JSON.parse(JSON.stringify(this.storeDtl));
                    var newDummyArr: any = {};

                    StoreDtl.categories.forEach(function (delitm: any) {
                        console.log('^^^^^^^^^^^^^^^^^^' + JSON.stringify(delitm));
                        console.log(delitm.name);                        
                        var catname = delitm.name;
                        var delcnt: number = 0;
                        var tmpArr: any =[];
                        var val : number = 0;
                        delitm.deliveryOpportunities.forEach( function (delopp : any) {
                            var delWhen = JSON.stringify(delopp.when);
                            if (delWhen === undefined || delWhen == 'null' || delWhen == 'undefined') {
                                delcnt++;  
                                tmpArr.push(val);
                                val++;
                            }
                        });
                        console.log('newRow ' + delcnt + " " + catname); 
                        if(delcnt >0){
                            newDummyArr[catname]= tmpArr;
                        }
                       
                    });
                    console.log('this ' + JSON.stringify(newDummyArr));
                    
                    // if(this.isAmazonCatelogType){
                    //     StoreDtl.merchants = this.merchants;
                    //     console.log("New Store"+StoreDtl);
                    //     console.log("New merchants"+this.merchants);
                    // }
                    var sessionModel1 = sessionStorage.getItem('gridModelChange');
                    //debugger;
                    if(sessionModel1.trim() == 'false'){
                        this.dummyArraDO = newDummyArr;
                        that.dummyArraDO = newDummyArr;
                    }
                    console.log('this ' + JSON.stringify(this.dummyArraDO));
                    console.log('that ' + JSON.stringify(that.dummyArraDO));
                    if (Object.keys(this.dummyArraDO).length != 0) {
                        duplicateStoreDtl.categories.forEach(function (itmStoreDtl: any, idx: number) {
                            doArray = [];
                            if (that.dummyArraDO[itmStoreDtl.name] != undefined && that.dummyArraDO[itmStoreDtl.name].length != 0) {
                                that.dummyArraDO[itmStoreDtl.name].forEach(function (val1: any) {
                                    console.log(that.storeDtl.categories[idx].deliveryOpportunities[val1].value);
                                    console.log(JSON.stringify(that.storeDtl.categories[idx].deliveryOpportunities[val1].when));
                                    var checkWhen = JSON.stringify(that.storeDtl.categories[idx].deliveryOpportunities[val1].when);
                                    //debugger;
                                    
                                    console.log(checkWhen);
                                    if (checkWhen === undefined || checkWhen == 'null' || checkWhen == 'undefined') {
                                        console.log('$$$$');
                                        doArray.push({
                                            "value": that.storeDtl.categories[idx].deliveryOpportunities[val1].value,
                                            "effectiveDate": that.storeDtl.categories[idx].deliveryOpportunities[val1].effectiveDate,
                                            "transitInformation": that.storeDtl.categories[idx].deliveryOpportunities[val1].transitInformation
                                        });
                                    } else {
                                        //  debugger;
                                        var sessionModel = sessionStorage.getItem('gridModelChange');
                                        console.log(JSON.stringify(that.storeDtl.categories[idx].deliveryOpportunities[val1]));
                                        if(sessionModel.trim() == 'true'){
                                            //if(that.storeDtl.categories[idx].deliveryOpportunities[val1].when.getTime()>new Date().getTime()  || that.storeDtl.categories[idx].deliveryOpportunities[val1].when.getTime()==new Date().getTime() ){
                                            if(that.storeDtl.categories[idx].deliveryOpportunities[val1].isEditable == 'Yes'){ 
                                                doArray.push({
                                                    "value": that.storeDtl.categories[idx].deliveryOpportunities[val1].value,
                                                    "effectiveDate": that.storeDtl.categories[idx].deliveryOpportunities[val1].effectiveDate,
                                                    "transitInformation": that.storeDtl.categories[idx].deliveryOpportunities[val1].transitInformation
                                                });  
                                            }
                                        }
                                    }
                                })
                            }
                            
                            console.log('doArray : ' + JSON.stringify(doArray));
                            //debugger;
                            if (doArray != []) {
                                itmStoreDtl.deliveryOpportunities = doArray;
                            }
                        });
                    } else {
                        this.storeDtl.categories.forEach(function (itm: any) {
                            dummyLocalArrayCategories.push({
                                'name': itm.name,
                                "deliveryOpportunities": [{
                                    "transitInformation": {
                                        'virtualSellingLocation': itm.virtualSellingLocation,
                                        'supplyingDepotLocation': itm.supplyingDepotLocation,
                                        'supplyingShippingLocation': itm.supplyingShippingLocation
                                    }
                                }]
                            });
                        });
                    }

                    // Check for duplicate effective date in patch
                    var dummyArray: any = new Array;
                    var duplicateEffectiveDate: any = new Array;
                    var duplicateCategories: any = [];

                    var finaleCateArray: any = [];
                    var deliveryDaysRequired: boolean = false;
                    var deliveryDaysInvalid: boolean = false;
                    var DOPattern = /^[YN]{7}$/;
                    console.log('main : ' + JSON.stringify(duplicateStoreDtl));
                    duplicateStoreDtl.categories.forEach(function (itm: any, key: any) {
                        console.log('length : ' + itm.deliveryOpportunities.length);
                        if (itm.deliveryOpportunities.length != 0) {
                            dummyArray[itm.name] = [];
                            finaleCateArray.push(itm);
                            itm.deliveryOpportunities.forEach(function (doItm: any) {

                                // this is for checking duplicate effective date
                                console.log('dummyArrray' + JSON.stringify(dummyArray));
                                console.log('item Name ' + itm.name);
                                console.log('item value ' + doItm.value.trim());
                                console.log('date issue ' + dummyArray[itm.name].indexOf(doItm.effectiveDate));
                                if (dummyArray[itm.name].indexOf(doItm.effectiveDate) == -1) {
                                    dummyArray[itm.name].push(doItm.effectiveDate);
                                } else {
                                    // Duplicate in final patch
                                    if (duplicateCategories.indexOf(itm.name) == -1) {
                                        duplicateCategories.push(itm.name);
                                    }

                                    if (duplicateEffectiveDate[itm.name] == undefined) {
                                        duplicateEffectiveDate[itm.name] = [];
                                    }
                                    if (duplicateEffectiveDate[itm.name].indexOf(that._cfunction.dateFormator(doItm.effectiveDate, 1)) == -1) {
                                        duplicateEffectiveDate[itm.name].push(that._cfunction.dateFormator(doItm.effectiveDate, 1));
                                    }
                                }

                                // This is for checking valid delivery days
                                if (doItm.value.trim() == "") {
                                    deliveryDaysRequired = true;
                                } else {
                                    if (!DOPattern.test(doItm.value.trim())) {
                                        deliveryDaysInvalid = true;
                                    }
                                }
                            });
                        }
                    });
                    // Check for duplicate effective date in patch - END
                    if (this.supplyingdepotchangeflag == true) {
                        if (finaleCateArray.length != 0) {
                            let virtualselling = finaleCateArray[0].deliveryOpportunities[0].transitInformation.virtualSellingLocation
                            let supplydepotloaction = finaleCateArray[0].deliveryOpportunities[0].transitInformation.supplyingDepotLocation
                            let shippingId = finaleCateArray[0].deliveryOpportunities[0].transitInformation.supplyingShippingLocation
                            let dateforNewRow = finaleCateArray[0].deliveryOpportunities[0].effectiveDate;
                            let effectiveDateforSupplyingdepot: any;
                            let isoDate = new Date();
                            let dt = (isoDate.getUTCDate() + 1) < 10 ? "0" + (isoDate.getUTCDate() + 1) : (isoDate.getUTCDate() + 1);
                            let mm = (isoDate.getUTCMonth() + 1) < 10 ? "0" + (isoDate.getUTCMonth() + 1) : (isoDate.getUTCMonth() + 1);
                            let today = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 0, 0, 0));
                            let tomorrowISODate = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1, 0, 0, 0));
                            if (this.makeChangeToday) {
                                effectiveDateforSupplyingdepot = today
                            }
                            if (!this.makeChangeToday) {
                                effectiveDateforSupplyingdepot = tomorrowISODate
                            }
                            if (!this.fieldChange) {
                                this.supplyDepotCatData.forEach(function (itm: any) {
                                    finaleCateArray.push({
                                        "name": itm.name,
                                        "deliveryOpportunities": [{
                                            'value': "NA",
                                            'effectiveDate': effectiveDateforSupplyingdepot,
                                            "transitInformation": {
                                                "supplyingDepotLocation": itm.deliveryOpportunities[0].transitInformation.supplyingDepotLocation,
                                                "virtualSellingLocation": itm.deliveryOpportunities[0].transitInformation.virtualSellingLocation,
                                                "supplyingShippingLocation": itm.deliveryOpportunities[0].transitInformation.supplyingShippingLocation
                                            }
                                        }]
                                    });
                                });
                            }

                            else if (this.fieldChange) {
                                this.storeDtl.categories.forEach(function (itm1: any) {
                                    finaleCateArray.push({
                                        "name": itm1.name,
                                        "deliveryOpportunities": [{
                                            'value': "NA",
                                            'effectiveDate': effectiveDateforSupplyingdepot,
                                            "transitInformation": {
                                                "supplyingDepotLocation": itm1.deliveryOpportunities[0].transitInformation.supplyingDepotLocation,
                                                "virtualSellingLocation": itm1.deliveryOpportunities[0].transitInformation.virtualSellingLocation,
                                                "supplyingShippingLocation": itm1.deliveryOpportunities[0].transitInformation.supplyingShippingLocation
                                            }
                                        }]
                                    });
                                });
                            }
                        }
                        else {

                            let effectiveDateforSupplyingdepot: any;
                            let isoDate = new Date();
                            let dt = (isoDate.getUTCDate() + 1) < 10 ? "0" + (isoDate.getUTCDate() + 1) : (isoDate.getUTCDate() + 1);
                            let mm = (isoDate.getUTCMonth() + 1) < 10 ? "0" + (isoDate.getUTCMonth() + 1) : (isoDate.getUTCMonth() + 1);
                            let today = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 0, 0, 0));
                            let tomorrowISODate = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1, 0, 0, 0));
                            if (this.makeChangeToday) {
                                effectiveDateforSupplyingdepot = today
                            }
                            if (!this.makeChangeToday) {
                                effectiveDateforSupplyingdepot = tomorrowISODate
                            } if (!this.fieldChange) {
                                this.supplyDepotCatData.forEach(function (itm: any) {
                                    finaleCateArray.push({
                                        "name": itm.name,
                                        "deliveryOpportunities": [{
                                            'value': "NA",
                                            'effectiveDate': effectiveDateforSupplyingdepot,
                                            "transitInformation": {
                                                "supplyingDepotLocation": itm.deliveryOpportunities[0].transitInformation.supplyingDepotLocation,
                                                "virtualSellingLocation": itm.deliveryOpportunities[0].transitInformation.virtualSellingLocation,
                                                "supplyingShippingLocation": itm.deliveryOpportunities[0].transitInformation.supplyingShippingLocation
                                            }
                                        }]
                                    });
                                });
                            }
                            else if (this.fieldChange) {
                                this.storeDtl.categories.forEach(function (itm1: any) {
                                    finaleCateArray.push({
                                        "name": itm1.name,
                                        "deliveryOpportunities": [{
                                            'value': "NA",
                                            'effectiveDate': effectiveDateforSupplyingdepot,
                                            "transitInformation": {
                                                "supplyingDepotLocation": itm1.deliveryOpportunities[0].transitInformation.supplyingDepotLocation,
                                                "virtualSellingLocation": itm1.deliveryOpportunities[0].transitInformation.virtualSellingLocation,
                                                "supplyingShippingLocation": itm1.deliveryOpportunities[0].transitInformation.supplyingShippingLocation
                                            }
                                        }]
                                    });
                                });
                            }
                        }
                    }

                    if (this.fieldChange == true && this.supplyingdepotchangeflag == false) {
                        let effectiveDateforSupplyingdepot: any;

                        let isoDate = new Date();
                        let dt = (isoDate.getUTCDate() + 1) < 10 ? "0" + (isoDate.getUTCDate() + 1) : (isoDate.getUTCDate() + 1);
                        let mm = (isoDate.getUTCMonth() + 1) < 10 ? "0" + (isoDate.getUTCMonth() + 1) : (isoDate.getUTCMonth() + 1);
                        let today = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 0, 0, 0));

                        let tomorrowISODate = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1, 0, 0, 0));
                        if (this.makeChangeToday) {
                            effectiveDateforSupplyingdepot = today
                        }
                        if (!this.makeChangeToday) {
                            effectiveDateforSupplyingdepot = tomorrowISODate
                        }

                        this.storeDtl.categories.forEach(function (itm1: any) {
                            finaleCateArray.push({
                                "name": itm1.name,
                                "deliveryOpportunities": [{
                                    'value': "NA",
                                    'effectiveDate': effectiveDateforSupplyingdepot,
                                    "transitInformation": {
                                        "supplyingDepotLocation": itm1.deliveryOpportunities[0].transitInformation.supplyingDepotLocation,
                                        "virtualSellingLocation": itm1.deliveryOpportunities[0].transitInformation.virtualSellingLocation,
                                        "supplyingShippingLocation": itm1.deliveryOpportunities[0].transitInformation.supplyingShippingLocation
                                    }
                                }]
                            });
                        });
                    }
                    if (this.makeEffectivePermission) {
                        if (this.makeChangeToday) {
                            duplicateStoreDtl.activateImmediate = true;
                            duplicateStoreDtl.applyImmediate = true;
                        }
                        /* else if(!this.makeChangeToday){*/
                        else {
                            duplicateStoreDtl.activateImmediate = false;
                            duplicateStoreDtl.applyImmediate = false;
                        }
                    } else {
                        duplicateStoreDtl.activateImmediate = false;
                        duplicateStoreDtl.applyImmediate = false;
                    }
                    delete duplicateStoreDtl['effectiveDate'];
                    duplicateStoreDtl.categories = finaleCateArray;

                    if (duplicateStoreDtl.categories.length == 0) {
                        delete duplicateStoreDtl['categories'];
                    }
                    
                    let finalJSON: any = {
                        "stores": [duplicateStoreDtl]
                    }
                    if (this.customerName == "amazon") 
                    {
                        duplicateStoreDtl.categories == null;
                        duplicateStoreDtl.transitInformationforPrime == [];
                        duplicateStoreDtl.transitInformationforCODOTUK == [];
                        if(duplicateStoreDtl.tradingName == "AMAZON STORE PICK")
                        {
                            duplicateStoreDtl.minimumExpetedInventory = this.threshold;
                        }

                        this.transitInformationforPrime = new transitInformationForAmazon(this.rmsSellingLocationForPrimeNow,this.storeDtl.storeId)
                        this.transitInformationforCODOTUK = new transitInformationForAmazon(this.rmsSellingLocationForCODOTUK,this.storeDtl.storeId)
                        this.merchantforPrime  = new Merchants(this.MerchantIdForHeader,"Prime Now","AAAAA",this.marketplaceforPrimeNow,this.inventoryApplicableForPrimeNow,this.priceApplicableForPrimeNow,this.settlementReportForPrimeNow,this.transitInformationforPrime);
                        this.merchantforCODOTUK  = new Merchants(this.MerchantIdForHeader,".CO.UK","AAAAA",this.marketplaceforCODOTUK,this.inventoryApplicableForCODOTUK,this.priceApplicableForCODOTUK,this.settlementReportForCODOTUK,this.transitInformationforCODOTUK);      

                        duplicateStoreDtl.merchants = [this.merchantforPrime,this.merchantforCODOTUK];
                        console.log("Merchants==>"+ JSON.stringify(duplicateStoreDtl.merchants));
                       
                    }
                    var regMID = new RegExp(/^[a-zA-Z0-9_ ]*$/)
                    var regPN = new RegExp(/^[1-9]\d*$/);
                    var regUK = new RegExp(/^[1-9]\d*$/);
                    if (this.customerName != "amazon") {
                        delete  finalJSON.storeAlternateIds;
                    }
                    console.log('on Save ' + JSON.stringify(duplicateCategories));
                    console.log('final ' + JSON.stringify(finalJSON));
                    //debugger;                  
                    if (duplicateCategories.length == 0 && !deliveryDaysInvalid && !deliveryDaysRequired && !this.isAmazonCatelogType) {
                        this._postsService.saveStoreInfo(this.orgApiName, finalJSON, this.storeid).subscribe(data => {
                            this.supplyingdepotchangeflag = false;
                            this.fieldChange = false;
                            this.makeChangeToday = false;
                            this.popupMsg = "Store details updated successfully.";
                            let sessionModal = document.getElementById('messageModal');
                            sessionModal.classList.remove('in');
                            sessionModal.classList.add('out');
                            document.getElementById('messageModal').style.display = "block";
                            if (this.makeEffectivePermission == true) {
                                this.makeEffectiveToday = false;
                                var element = <HTMLInputElement>document.getElementById("effectiveImmediatelyId");
                                element.checked = false;
                            }
                            var element = <HTMLInputElement>document.getElementById("addnewButton");
                            element.disabled = false;
                             this.daterangepickerOptions.settings = {
                                    minDate: this.tomorrow,
                                };
                            this.getStoreDetails().then((res: any) => {
                                if (res) {
                                    this.ngOnInit();
                                    this.showLoader = false;
                                    this.editstore.control.markAsPristine();
                                }
                            });
                            this.dummyArraDO = {};                            
                            sessionStorage.setItem('gridModelChange', 'false');
                        }, error => {
                            var errorobject = JSON.parse(error._body)
                            this.errorMsgString = errorobject.errorMessage;
                            this.changeDateFormat();
                            let sessionModal = document.getElementById('errormodal');
                            sessionModal.classList.remove('in');
                            sessionModal.classList.add('out');
                            document.getElementById('errormodal').style.display = "block";
                            this.showLoader = false;

                        });
                    }
                    if(this.customerName == "amazon" && 
                    ((((regMID.test(this.merchantId)) && (regPN.test(this.threshold)) && (regPN.test(this.rmsSellingLocationForPrimeNow)) && (regUK.test(this.rmsSellingLocationForCODOTUK))) && this.storeDtl.tradingName == "AMAZON STORE PICK") ||
                    ( (!deliveryDaysInvalid) && (!deliveryDaysRequired) && this.storeDtl.tradingName == "AMAZON"))){                                   
                            

                            this._postsService.saveStoreInfo(this.orgApiName, finalJSON, this.storeid).subscribe(data => {
                            this.supplyingdepotchangeflag = false;
                            this.fieldChange = false;
                            this.makeChangeToday = false;
                            this.popupMsg = "Store details updated successfully.";
                            let sessionModal = document.getElementById('messageModal');
                            sessionModal.classList.remove('in');
                            sessionModal.classList.add('out');
                            document.getElementById('messageModal').style.display = "block";
                            if (this.makeEffectivePermission == true) {
                                this.makeEffectiveToday = false;
                                var element = <HTMLInputElement>document.getElementById("effectiveImmediatelyId");
                                element.checked = false;
                            }
                            var element = <HTMLInputElement>document.getElementById("addnewButton");
                            element.disabled = false;
                             this.daterangepickerOptions.settings = {
                                    minDate: this.tomorrow,
                                };
                            this.getStoreDetails().then((res: any) => {
                                if (res) {
                                    this.ngOnInit();
                                    this.showLoader = false;
                                    this.editstore.control.markAsPristine();
                                }
                            });
                            this.dummyArraDO = {};                            
                            sessionStorage.setItem('gridModelChange', 'false');
                        }, error => {
                            var errorobject = JSON.parse(error._body)
                            this.errorMsgString = errorobject.errorMessage;
                            this.changeDateFormat();
                            let sessionModal = document.getElementById('errormodal');
                            sessionModal.classList.remove('in');
                            sessionModal.classList.add('out');
                            document.getElementById('errormodal').style.display = "block";
                            this.showLoader = false;

                        });
                    }
                    

                    else {
                        this.popupMsg = "";
                        let dates: string = "";
                        let that = this;
                        this.changeDateFormat();
                        //this.dummyArraDO = {};
                        duplicateCategories.forEach(function (category: any) {
                            dates = duplicateEffectiveDate[category].join(", ");
                            that.popupMsg += "<div>Multiple delivery opportunity cannot be updated for Category " + category + " having effective date as " + dates + ".</div>";
                        });

                        if (deliveryDaysInvalid) {
                            that.popupMsg += "<div>Enter Capital Y/N only and 7 char long.</div>";
                        }
                        if (deliveryDaysRequired) {
                            that.popupMsg += "<div>Delivery Days is required.</div>";
                        }

                       if(!(regPN.test(this.threshold)) && this.threshold != "" && this.threshold != null && this.storeDtl.tradingName == "AMAZON STORE PICK")
                        {                    
                        this.popupMsg = this.popupMsg + "Header : Invalid Minimum Expected Inventory" + "<br/>";
                        }

                        
                       if(this.threshold == null || this.threshold == "" && this.storeDtl.tradingName == "AMAZON STORE PICK")
                       {
                       this.popupMsg = this.popupMsg + "Header : Minimum Expected Inventory" + "<br/>";
                       }

                        if(!(regPN.test(this.rmsSellingLocationForPrimeNow)) && this.rmsSellingLocationForPrimeNow != "" && this.storeDtl.tradingName == "AMAZON STORE PICK")
                        {
                        this.popupMsg = this.popupMsg + "MarketPlaces : Invalid RMS for Prime Now" + "<br/>";
                        }

                        if (this.rmsSellingLocationForPrimeNow == "" && this.storeDtl.tradingName == "AMAZON STORE PICK") {
    
                            this.popupMsg = this.popupMsg + "MarketPlaces : RMS for Prime Now" + "<br/>";
                        }                       

                        if(!(regUK.test(this.rmsSellingLocationForCODOTUK)) && this.rmsSellingLocationForCODOTUK != "" && this.storeDtl.tradingName == "AMAZON STORE PICK")
                        {
                            this.popupMsg = this.popupMsg + "MarketPlaces : Invalid RMS for .CO.UK" + "<br/>";
                        }
    
                        if (this.rmsSellingLocationForCODOTUK == "" && this.storeDtl.tradingName == "AMAZON STORE PICK") {
        
                            this.popupMsg = this.popupMsg + "MarketPlaces : RMS for .CO.UK" + "<br/>";
                        }
                        let sessionModal = document.getElementById('messageModal');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('messageModal').style.display = "block";
                        this.showLoader = false;
                    }
                    // No Error msg
                } else {
                    this.showLoader = false;
                    // There is some validation messages
                    this.popupMsg = "";
                    let that = this;
                    let DOErrorFlag = false;
                    let DOErrorFlagRequired = false;
                    msgList.forEach(function (key: any) {
                        if ((errorList[key] == that.validationMsgDO_YN && DOErrorFlag) || (errorList[key] == that.validationMsgDO_YN_Required && DOErrorFlagRequired)) {

                        } else {
                            that.popupMsg += errorList[key] + "<br/>";
                            if (errorList[key] == that.validationMsgDO_YN) {
                                DOErrorFlag = true;
                            }

                            if (errorList[key] == that.validationMsgDO_YN_Required) {
                                DOErrorFlagRequired = true;
                            }
                        }

                    });
                    let sessionModal = document.getElementById('messageModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('messageModal').style.display = "block";
                }
            }
        }, reason => {
            console.log(reason);
        });
    }

    closePopup(modalBoxId = "") {
        if (modalBoxId != "") {
            let sessionModal = document.getElementById(modalBoxId);
            sessionModal.classList.remove('out');
            sessionModal.classList.add('in');
            sessionModal.style.display = 'none';
        } else {
            let sessionModal = document.getElementById('popup_SessionExpired');
            sessionModal.classList.remove('out');
            sessionModal.classList.add('in');
            sessionModal.style.display = 'none';
            let dt = this._cfunction.getUserSession();
            this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
                ////console.log(userData);
            }, (error: any) => {
                ////console.log(error);
            });
            sessionStorage.clear();
            this._router.navigate(['']);
        }
    }

    back2Details() {

        let navigationExtras: NavigationExtras = {
            fragment: this.fragment
        };

        console.log(navigationExtras);

        if (this.canDeactivate()) {
            this._router.navigate(['/', 'store', this.customerName, 'storedetails', this.storeid], navigationExtras);
            sessionStorage.setItem('unsavedChanges', 'false');
        }

    }

    keepMeonSamePage() {
        let brandType = sessionStorage.getItem('previousBrandType');
        let sessionModal = document.getElementById('unsaveModalPageLevel');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('unsaveModalPageLevel').style.display = 'none';
    }

    navigatTopage() {
        let sessionModal = document.getElementById('unsaveModalPageLevel');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('unsaveModalPageLevel').style.display = 'none';
        this.showLoader = false;
    }

    populateHistoricalFutureDO(category: string, storeCategoryIndex: number = 0) {
        if(this.clickableCategory == false && storeCategoryIndex != -1){
            document.getElementById('disableCategory').style.display = 'block';
        } else{
            this.historicalCategory = category;
            this.storeCategoryIndex = storeCategoryIndex;
            let that = this;
            that.deliveryOpportunityArray = [];
            this.storeDtl.categories.forEach(function (itm: any) {
                if (itm.name == category) {
                    that.deliveryOpportunityArray = itm.deliveryOpportunities;
                }
            });
            //this.findRowsEditable();
            
           
        }
        if(this.makeChangeToday){
            //     var element = <HTMLInputElement>document.getElementById("addnewButton");

            // element.disabled = false;
            this.iseffectivetoday = true;
            //if(this.dummyArraDO[this.historicalCategory] != undefined  && !this.checkoncedeletedflag){
            if(this.dummyArraDO[this.historicalCategory] != undefined){
                var element = <HTMLInputElement>document.getElementById("addnewButton");
    
                element.disabled = true;
                
            } else {
                var element = <HTMLInputElement>document.getElementById("addnewButton");
    
                element.disabled = false;
              //  this.checkoncedeletedflag = false
                
            }
        }
       // debugger;
       // console.log(JSON.stringify(this.dummyArraDO[this.historicalCategory]));
        
    }

    deleteRow(e: any, i: number) {

        this.showLoader = true;

        console.log(JSON.stringify(this.deliveryOpportunityArray));

        //this. deliveryOpportunityArray = this. deliveryOpportunityArray.filter(item => item.id !== i);
        var y: number = 0;
        var tmp: any = [];

        this.deliveryOpportunityArray.forEach(function (itm: any) {
            // console.log(itm);
            if (y != i) {
                tmp.push(itm);
            }

            y++;
        });
       // console.log('~~~~~~~~ ' + JSON.stringify(tmp));        

        this.deliveryOpportunityArray = tmp;
        this.deliveryOpportunityArray.push();

        
        // console.log('Category Name :' + this.historicalCategory);
        var selCategory = this.historicalCategory;
        this.storeDtl.categories.forEach(function (sitm: any) {
            if (sitm.name == selCategory) {
                sitm.deliveryOpportunities = tmp;
            }
        });
        console.log('storeDtl ' + JSON.stringify(this.storeDtl));

        var whenmin: number = 0;
        var checkclick : number = 0;
        // this.deliveryOpportunityArray.forEach(function (itm1: any) {
        //     console.log('check ' + JSON.stringify(itm1.when));
        //     var check = JSON.stringify(itm1.when);
        //     if (check == 'null' || check === undefined) {
        //         console.log('out----------');
        //         console.log(JSON.stringify(itm1));
        //         console.log(whenmin + ' in-----------');
        //         whenmin = whenmin + 1;
        //         console.log('!!!!!!!!!!! ' + JSON.stringify(itm1));
        //         if(itm1.value.trim() ==''){
        //             checkclick++
        //         }
               
        //     }
        // });

        //new code

        var delStoreDtl = JSON.parse(JSON.stringify(this.storeDtl));      

        delStoreDtl.categories.forEach(function (delitm1: any) {
          
            delitm1.deliveryOpportunities.forEach(function (itm1: any) {
                console.log('check ' + JSON.stringify(itm1));
                console.log('check ' + JSON.stringify(itm1.when));
                var check = JSON.stringify(itm1.when);
                //debugger;  
                if (check == 'null' || check === undefined || check == 'undefined') {
                    console.log('out----------');
                    console.log(JSON.stringify(itm1));
                    console.log(whenmin + ' in-----------');
                    whenmin = whenmin + 1;
                    console.log('!!!!!!!!!!! ' + JSON.stringify(itm1));
                    if(itm1.value.trim() ==''){
                        checkclick++
                    }
                   
                }else if(check != undefined && itm1.isEditable == 'Yes') {
                    whenmin = whenmin + 1;
                }
            });
            
        });

        
        console.log(whenmin + ' -----------');
        console.log(checkclick + ' -----------');
        
        if(checkclick == 0){
            this.clickableCategory = true;
            sessionStorage.setItem('gridunsavedChanges', 'false');
        }

        console.log('*************** ' + sessionStorage.getItem('unsavedChanges'));
        var sessionSave = sessionStorage.getItem('unsavedChanges');
        if(whenmin == 0){
            sessionStorage.setItem('gridModelChange',' false');
        }
        if (whenmin == 0 && this.supplyingDepoChange == false && sessionSave == 'false') {
            this.editstore.control.markAsPristine();
        }
        console.log('final ' + JSON.stringify(this.deliveryOpportunityArray));



        //this.ngOnInit();
        this.supplyingDepot = this.getsupplyingDepot();
        this.getSupplyDepotCatData(selCategory);

        this.filterStoreData(selCategory).then(res => {

            //this.showLoader = false;

        });
        console.log('Updated Store Detail' + JSON.stringify(this.storeDtl));


        this.showLoader = false;
        if(this.makeChangeToday){
            var element = <HTMLInputElement>document.getElementById("addnewButton");
                element.disabled = false;
                this.checkoncedeletedflag = true;
         }
    }

    addNewRowDO() {
        let that = this;
        let isoDate: any;
        let tomorrowISODate: any = "";
        isoDate = new Date();
        let dt = (isoDate.getUTCDate() + 1) < 10 ? "0" + (isoDate.getUTCDate() + 1) : (isoDate.getUTCDate() + 1);
        let mm = (isoDate.getUTCMonth() + 1) < 10 ? "0" + (isoDate.getUTCMonth() + 1) : (isoDate.getUTCMonth() + 1);
        //tomorrowISODate = dt + "/" + mm + "/" + isoDate.getUTCFullYear();
        tomorrowISODate = this.tomorrowDate;//dt + "/" + mm + "/" + isoDate.getUTCFullYear();

        console.log("tomorrowISODate");
        console.log(tomorrowISODate);
        let addNewRowEffectiveDate
        if (this.addNewDOMode)
            this.addNewDOMode = false;
        else
            this.addNewDOMode = true;

        if (this.editEffectiveDate) {
            // if(this.DOCategoriesTomorrowRow[this.historicalCategory]){
            //     tomorrowISODate = (isoDate.getUTCDate()+1) + "/" + isoDate.getUTCMonth()+1 + "/" + isoDate.getUTCFullYear();
            // } else {
            //     tomorrowISODate = (isoDate.getUTCDate()+1) + "/" + isoDate.getUTCMonth()+1 + "/" + isoDate.getUTCFullYear();//this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate()+1));
            // }
        } else {
            // tomorrowISODate = (isoDate.getUTCDate()+1) + "/" + isoDate.getUTCMonth()+1 + "/" + isoDate.getUTCFullYear();//this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate()+1));
            //this.disableAddNewRow = true;
        }
        if(this.makeChangeToday){
            addNewRowEffectiveDate = this.todayDate
        }
        else if (!this.makeChangeToday){
            addNewRowEffectiveDate = tomorrowISODate

        }
        let tempObj = {
            "value": "",
            "effectiveDate": addNewRowEffectiveDate,
            "isEditable": 'Yes',
            "deleteicon": true,
            "transitInformation": this.LatestDOArray[this.historicalCategory]
        };

        let currentRowId = this.DOCategoriesCurrentRow[this.historicalCategory];
        //this.deliveryOpportunityArray.splice(currentRowId, 0, tempObj);
        this.deliveryOpportunityArray.unshift(tempObj);
        ++this.DOCategoriesCurrentRow[this.historicalCategory];
        
        this.clickableCategory = false;
        

        if (this.dummyArraDO[this.historicalCategory] != undefined) {
            //this.dummyArraDO[this.historicalCategory].map(function(val:number){return ++val});
            var karr: number[] = [];
            this.dummyArraDO[this.historicalCategory].forEach(function (val: number) {
                karr.push(val + 1);
            })
            this.dummyArraDO[this.historicalCategory] = karr;            
        }

        if(this.makeChangeToday){
         var element = <HTMLInputElement>document.getElementById("addnewButton");   
        element.disabled = true;
        }
    }

    showCalendarTextBox(dt: any, cat: any = null) {
        if (dt == "") {
            this.calDefaultDate = new Date();
            return "Yes";
        } else {
            let isoDate = new Date()
            let temp = dt.split("/");
            let todayISODate = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 0, 0, 0);
            let tomorrowISODate = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1);
            let orgDate = new Date(temp[2], temp[1] - 1, temp[0], 0, 0, 0);
            var curCategory: string = "";
            if (cat != null) {
                curCategory = cat;
            } else {
                curCategory = this.historicalCategory;
            }

            if (todayISODate.getTime() == orgDate.getTime()) {
                return "No";
            } else if (orgDate.getTime() > todayISODate.getTime()) {
                if (this.editableDOArray[curCategory] == undefined) {
                    this.editableDOArray[curCategory] = [];
                }
                if (this.editableDOArray[curCategory].indexOf(dt) != -1) {
                    // Already exist so DO is not editable
                    return "No";
                } else {
                    // not exist so DO is editable
                    this.editableDOArray[curCategory].push(dt);
                    return "Yes";
                }
                //return true;
            } else {
                return "No";
            }
        }
    }

    openCalendar(dt: any, id: string, idx: number = 0) {
        let dt1 = dt.split("/");
        this.calDefaultDate = new Date(dt1[2], dt1[1] - 1, dt1[0]);
        this.selectedCategoryIndex4Cal = idx;
        let calModal = document.getElementById('calenderModal');
        calModal.classList.remove('in');
        calModal.classList.add('out');
        document.getElementById('calenderModal').style.display = 'block';
        this.currentCalId = id;
    }

    closeDateModelBox() {
        document.getElementById('calenderModal').style.display = "none";
    }

    clearDateModelBox() {

    }

    CalOnDateSelection(value: any) {
        var dd1 = new Date(value).toString();
        var dd = this._cfunction.isodate(dd1);
        var dtStr = this._cfunction.dateFormator(dd, 1);
        this.deliveryOpportunityArray[this.selectedCategoryIndex4Cal].effectiveDate = dtStr;
        this.calDefaultDate = dtStr;
        document.getElementById('calenderModal').style.display = "none";
    }

    isTomorrowDate(dt: any = "") {
        if (dt == "") {
            return false;
        } else {
            // Check incoming date is for tomorrow
            let temp = dt.split("/");
            let isoDate = new Date();
            let tomorrowISODate = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1).getTime();
            let orgDate = new Date(temp[1] + "/" + temp[0] + "/" + temp[2]).getTime();
            if (tomorrowISODate == orgDate) {
                return true;
            } else {
                return false;
            }
        }
    }

    changeDateFormat() {
        let that = this;
        this.storeDtl.categories.forEach(function (itm1: any) {
            itm1.deliveryOpportunities.forEach(function (itm: any) {
                itm.effectiveDate = that._cfunction.dateFormator(itm.effectiveDate, 1);
                //itm.when = that._cfunction.dateFormator(itm.when, 3);
                itm.when = new Date(itm.when);
            });
        });
    }

    changeDateFormat2Org() {
        let that = this;
        this.storeDtl.categories.forEach(function (itm1: any) {
            itm1.deliveryOpportunities.forEach(function (itm: any) {
                itm.effectiveDate = that._cfunction.getISOfromDDMMYYYYFromDateOnly(itm.effectiveDate);
            });
        });
    }

    ChkFutureDateRecordsForEditEffectivePems(dt: any = '') {
        let isoDate = new Date();
        if (dt != '' || typeof dt != 'undefined') {
            let temp = dt.effectiveDate.split("/");
            let todayISODate = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 23, 59, 59).getTime();
            let tomorrowISODate = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1).getTime();
            let orgDate = new Date(temp[2], temp[1] + 1, temp[0]).getTime();

            if (todayISODate == orgDate) {
                return false;
            } else if (orgDate > todayISODate) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //dynamic tooltip text
    showInfoErrorMsg(fieldName: any) {
        let msg;
        msg = fieldName + ' is required.';
        return msg;
    }

    onKeydown(event: any): boolean {
        var charCode = (event.which) ? event.which : event.keyCode;
        console.log(charCode);
        ///let pattern = /^[YN]$/;
        if (charCode == 78 || charCode == 89 || charCode == 8) {                
            sessionStorage.setItem('gridModelChange',' true');
            return true;
        }
        return false;
    }
    //validate input text
    validateInputText(txt2Validate: any, validationId: any, errorId: string, fieldId: string, fieldName: string, isRequired: boolean = true) {
        if (txt2Validate.target.value != "") {
            let pattern;
            switch (validationId) {
                case 1:
                    pattern = /^[a-zA-Z0-9\/&().!:' ]+$/; //a-z A-Z 0-9 / & () . ! : '
                    break;
                case 2:
                    pattern = /^[a-zA-Z0-9]+[a-zA-Z0-9\/&().!: ]+$/; //a-z A-Z 0-9 / & () . ! :
                    break;
                case 3:
                    pattern = /^[a-zA-Z0-9]+[a-zA-Z0-9 ]+$/; //a-z A-Z 0-9
                    break;
                case 4:
                    pattern = /^[a-zA-Z]+[a-zA-Z()' ]+$/; //a-z A-Z '()
                    break;
                case 5:
                    pattern = /^[a-zA-Z]+$/; //a-z A-Z
                    break;
                case 6:
                    pattern = /^[a-zA-Z0-9]+[a-zA-Z 0-9]+$/; //a-z A-Z 0-9
                    break;
                case 7: // For email
                    pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; //For email
                    break;
                case 8: // For Numeric with space and dash
                    pattern = /^[0-9 -]+$/; //For Numeric only
                    break;
                case 9: // For Numeric
                    pattern = /^[0-9]*$/; //For Numeric only
                    break;
                case 10: //for allocation priority code
                    pattern = /^\b[P][0-9]{1,}\b$/;
                    break;
                case 11: // For Yand N only and  7char
                    pattern = /^[YN]{7}$/;
                    break;
                case 12:
                    break;               
                case 13:
                    pattern = /^(?!0$)/; 
                break;
            }
            if (validationId == 12) {
                if (!isRequired) {
                    document.getElementById(errorId).innerHTML = fieldName + " is optional.";
                } else {
                    document.getElementById(errorId).innerHTML = fieldName + " is required.";
                }
                document.getElementById(errorId).classList.remove('red');
                document.getElementById(fieldId).style.border = "0";
                delete this.errorMsgList[errorId];
                if (--this.errorCount < 0) {
                    this.errorCount = 0;
                }
            }
            else if (pattern.test(txt2Validate.target.value)) {

                // Valid
                if (!isRequired) {
                    document.getElementById(errorId).innerHTML = fieldName + " is optional.";
                } else {
                    document.getElementById(errorId).innerHTML = fieldName + " is required.";
                }
                document.getElementById(errorId).classList.remove('red');
                document.getElementById(fieldId).style.border = "0";
                if (validationId != 11) {
                    delete this.errorMsgList[errorId];
                }
                if (--this.errorCount < 0) {
                    this.errorCount = 0;
                }

            } else {

                // invalid
                if (validationId == 11) {
                    document.getElementById(fieldId).style.border = "1px solid red";
                    document.getElementById(errorId).innerHTML = this.validationMsgDO_YN;
                    //this.errorMsgList[errorId] = this.validationMsgDO_YN;
                } else {
                    document.getElementById(fieldId).style.border = "1px solid red";
                    document.getElementById(errorId).innerHTML = "Invalid " + fieldName;
                    this.errorMsgList[errorId] = "Invalid " + fieldName;
                }
                document.getElementById(errorId).classList.add('red');
                ++this.errorCount;
            }
        } else {
            // Empty;
            if (isRequired) {
                document.getElementById(fieldId).style.border = "1px solid red";
                document.getElementById(errorId).innerHTML = fieldName + " is required.";
                if (validationId != 11) {
                    this.errorMsgList[errorId] = fieldName + " is required.";
                }
                document.getElementById(errorId).classList.add('red');
            } else {
                document.getElementById(fieldId).style.border = "0";
                document.getElementById(errorId).innerHTML = fieldName + " is optional.";
                document.getElementById(errorId).classList.remove('red');
            }
        }
    }

    public selectedDate(value: any, idx: any) {
        this.deliveryOpportunityArray[idx].effectiveDate = value.start.format('DD/MM/YYYY');
        console.log(this.deliveryOpportunityArray[idx].effectiveDate);
        this.disableSave = false;
        this.editstore.control.markAsDirty();
        sessionStorage.setItem('gridModelChange','true');
    }

    onModelChange(category: string, index: number, model: any = "SD", mode: string) {       
        if (model == 'DO') {
            if (this.dummyArraDO[category] == undefined) {
                this.dummyArraDO[category] = [];
            }
            if (this.dummyArraDO[category].indexOf(index) == -1) {
                this.dummyArraDO[category].push(index);  
                              
            }     
            console.log('*****Model Change : ' + JSON.stringify(this.dummyArraDO));

            //check whether edit mode or new row is added.
            if (mode == 'addMode')
                this.addNewDOMode = true;
            else
                this.addNewDOMode = false;
        }

        console.log('delivery ' + JSON.stringify(this.deliveryOpportunityArray));
        var checkblank : number = 0;
        this.deliveryOpportunityArray.forEach(function (itm: any) {
            console.log(itm);
            if(itm.when === undefined){
                if(itm.value.trim() == ""){
                    checkblank++;
                }
            }
            
        });
        console.log('checkblank ' + checkblank );
        if(checkblank == 0){
            this.clickableCategory = true;
        }
               
        sessionStorage.setItem('gridunsavedChanges', 'true');
    }

    getStoreDetails(): any {
        return new Promise((resolve) => {
            this.showLoader = true;
            let searchTerm = 'stores';
            this.start = 0;
            this.count = 0;
            this._globalFunc.logger("Calling catalogue list service.");
            this._postsService.getStoreItemLatest(this.customerName, this.storeid).subscribe((data: any) => {
                this.storeDtl = data.stores[0];
                resolve(true);
            });
        });
    }

    onChange4UnsavedChanges(fieldName: string, idx: number = null, obj: any = null) {    
        if(fieldName.toLowerCase() != 'value'){            
            sessionStorage.setItem('unsavedChanges', 'true');
        }

        if(this.storeDtl.tradingName == "AMAZON STORE PICK" && this.isAmazonCatelogType)
        {
            this.merchantforStorepick = true;             
            console.log(this.merchantforStorepick);                
        }
        if(this.storeDtl.tradingName == "AMAZON" && this.isAmazonCatelogType)
        {           
            this.merchantforStorepick = false;              
            console.log(this.merchantforStorepick);    
            
        }

        if(document.getElementById("MarketPlaces")){
            document.getElementById("MarketPlaces").classList.remove('active');
            document.getElementById("maindetailshead").classList.add('active');
            document.getElementById("maindetails").classList.add('active'); 
        }
        if(document.getElementById("SupplyingDepot")){
            document.getElementById("SupplyingDepot").classList.remove('active');
            document.getElementById("maindetailshead").classList.add('active');
            document.getElementById("maindetails").classList.add('active'); 
        }
        if(document.getElementById("DELIVERYOPPORTUNITIES")){
            document.getElementById("DELIVERYOPPORTUNITIES").classList.remove('active');
            document.getElementById("maindetailshead").classList.add('active');
            document.getElementById("maindetails").classList.add('active'); 
        }
        if(document.getElementById("address")){
            document.getElementById("address").classList.remove('active');
            document.getElementById("addresshead").classList.remove('active');
            document.getElementById("maindetailshead").classList.add('active');
            document.getElementById("maindetails").classList.add('active'); 
        }
        
    }


    //auto scroll
    scrollTo() {
        window.scrollTo({
            top: 800,
            behavior: "smooth"
        });
    }

    selectTab(selectedTab: string = "maindetails") {
        this.fragment = selectedTab
    }

    editLatestDO(dt: any) {
        console.log("this.editableDOArray : " + dt);
        console.log(this.editableDOArray);
        if (this.editableDOArray.indexOf(dt) != -1) {
            // Already exist so DO is not editable
            return false;
        } else {
            // Already exist so DO is editable
            this.editableDOArray.push(dt);
            return true;
        }
    }
}