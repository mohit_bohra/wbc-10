import {Pipe, PipeTransform} from '@angular/core';
@Pipe({
    name: 'categoryfilter',
    pure: false
})
export class CategoryFilter implements PipeTransform {
    transform(items: any[], filter: string): any {
        //console.log("Pipe");
        //console.log(filter);
        if (!items || !filter) {
            return items;
        }
        // filter items array, items which match and return true will be
        // kept, false will be filtered out
        return items.filter(item => item.name == filter);
    }
}