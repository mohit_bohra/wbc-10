import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: "sort"
})
export class DOSortPipe {
    transform(array: Array<any>, args: string): Array<any> {
        array.sort((a: any, b: any) => {
            let aDate:any;
            let bDate:any;
            let temp_a = a.effectiveDate.split("/");
            aDate = new Date(temp_a[2], temp_a[1] - 1, temp_a[0], 0, 0, 0);

            let temp_b = b.effectiveDate.split("/");
            bDate = new Date(temp_b[2], temp_b[1] - 1, temp_b[0], 0, 0, 0);
            console.log(aDate + " : " + bDate);
            if (aDate.getTime() < bDate.getTime()) {
                return 1;
            } else if (aDate.getTime() > bDate.getTime()) {
                return -1;
            } else {
                return 0;
            }
        });
        return array;
        /*if (array.length > 15) {
            return array.slice(0, 15);
        } else {
            return array;
        }*/
    }
}