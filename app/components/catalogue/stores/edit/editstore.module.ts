import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../../global/global.component';
import { Services } from '../../../../services/app.services';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from "ngx-tooltip";
import { CalendarModule } from 'primeng/primeng';
import { EditstoreComponentRoutes } from './editstore.routes';
import { EditstoreComponent } from './editstore.component';
import { commonfunction } from '../../../../services/commonfunction.services';
import { CategoryFilter } from './categoryFilter.pipes';
import { DOSortPipe } from './sortDO.pipes';
import { DOSortCategoryPipe } from './sortDOCategories.pipes';
import { Daterangepicker } from 'ng2-daterangepicker';
import { UppercaseModule } from '../../../../directives/uppercase/uppercase.module';
@NgModule({
  imports: [
    UppercaseModule,
    EditstoreComponentRoutes,
    CommonModule,
    FormsModule,
    CalendarModule,
    TooltipModule,
    Daterangepicker
  ],
  exports: [],
  declarations: [EditstoreComponent, CategoryFilter, DOSortPipe, DOSortCategoryPipe],
  providers: [Services, GlobalComponent,commonfunction]
})
export class EditstoreModule { }
