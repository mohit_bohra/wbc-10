import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditstoreComponent } from './editstore.component';


// route Configuration
const editstoreRoutes: Routes = [
  { path: '', component: EditstoreComponent }
];


@NgModule({
  imports: [RouterModule.forChild(editstoreRoutes)],
  exports: [RouterModule]
})
export class EditstoreComponentRoutes { }
