import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../../services/app.services';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalComponent } from '../../global/global.component';
import { commonfunction } from '../../../services/commonfunction.services';
import { Merchants } from '../models/merchants';

declare let saveAs: any;

@Component({
    templateUrl: 'storedetails.component.html'

})
export class StoredetailsComponent {
    storeDescription: string = "-";
    strInfoNotAvail: string = "-";
    storeIdPrefix: any;
    storeId: any;
    customerDispayName: string;
    storedetails: any;
    style: string;
    disabledCusChange: boolean;
    organisationListChange: any;
    hitNext: boolean;
    count: number;
    start: number;
    storedetailListLength: number;
    limit: any;

    storeid: any;
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;

    showLoader: boolean;
    private _sub: any;
    hideOrganisation: boolean = true;

    custPermission: any[];

    imgName: string = "";
    storeName: string = "-";
    storeAvailability: string = "-";
    tradingName: string = "-";
    storeAreaCode: string = "-";
    storeAreaManager: string = "-";
    storeregionCode: string = "-";
    storeregionManager: string = "-";
    storeAllocationP: string = "-";
    storeStatus: string = "-";
    storeeffDate: string = "-";
    userName: string = "-";
    storeAddressName: string = "-";
    storeAddressLine1: string = "-";
    storeAddressLine2: string = "-";
    storeCity: string = "-";
    storeCounty: string = "-";
    storeCountry: string = "-";
    storePostCode: string = "-";
    storeContactEmail: string = "-";
    storeContactFAX: string = "-";
    storeContactPhone: string = "-";
    supplyingDepot: any;
    filterName: any;
    filterValue: any;
    supplyDeportCategory: any;
    supplyDeportCategoryLen: any = -1;
    supplyDepotType: any = "";
    allowEdit: boolean = false;
    supplyingDepotFlg: string;
    storeData: any;
    storeRevisionData: any;
    version: any;
    supplyingDepotName: any = "";
    storeDataList : any;   
    categories: any;
    mon: number = 0;
    tue: number = 0;
    wed: number = 0;
    thu: number = 0;
    fri: number = 0;
    sat: number = 0;
    sun: number = 0;
    historicalCategory: any = "";
    storeCategoryIndex: number;
    deliveryOpportunityArray: any[];
    selectedTab: any = 'maindetails';
    storeDataLatest: any;
    showColumn:boolean = false;
    isAmazonCatelogType: boolean = false;
    //for .co.uk changes
    merchantName : any[];
    merchantID : any;
    merchants : Merchants[];
    merchantforStorePick : boolean = false;
    threshold : any;
 
    //Constructor is used to initialize variables during load time
    constructor(private _postsService: Services, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _globalFunc: GlobalComponent, public _cfunction: commonfunction) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.filterName = 'supplydepot';
        this.filterValue = '@all';
        this.supplyingDepotFlg = '';
        this.merchantName = ['Prime Now','.CO.UK'];
    }

    ngOnInit() {
        this._globalFunc.logger("Catalogue search screen loading.");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {

            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            console.log('fragment : ' + JSON.stringify(this._route.fragment));
            console.log('fragment : ' + this._route.fragment['_value']);
            if(this._route.fragment['_value'] != undefined && this._route.fragment['_value'] != null ){
                this.selectedTab = this._route.fragment['_value'];
            }
            //alert(this.showLoader);
            this._globalFunc.autoscroll();
            this.limit = this.objConstant.SCROLL_RECORD_LIMIT;
            this._sub = this._route.params.subscribe((params: { orgType: string, storeid: any }) => {
                console.log(params);
                this.customerName = params.orgType;
                this.storeid = params.storeid;
                this.imgName = this.getCustomerImage(params.orgType); if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));

                //sessionStorage.setItem('orgType', params.orgType);
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;

                this.getStoreDetails(this.storeid);
                this.mon = 0;
                this.tue = 0;
                this.wed = 0;
                this.thu = 0;
                this.fri = 0;
                this.sat = 0;
                this.sun = 0;

                if(sessionStorage.getItem('orgType') === 'amazon'){
                    this.showColumn = true;
                    this.isAmazonCatelogType = true;
                }
                else{
                    this.showColumn = false;
                    this.isAmazonCatelogType = false;
                }
            });
        }
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    getAllRevisionId(storeid: any) {

        this.showLoader = true;
        console.log(storeid);

        this._postsService.getAllStoreRevisionId(this.customerName, storeid).subscribe((data: any) => {
            console.log(data);
            this.showLoader = false;
            this.storeRevisionData = data.revisions;

        }
            , (err: any) => {
                this._globalFunc.logger("Store revision service failed.");
                this.storedetailListLength = 0;
                this._router.navigate(['/error404']);
            }
        );

    }

    populateSpecificVersionStore(revisionId: any, storeId: any) {

        this._postsService.getRevisionByStoreId(this.customerName, storeId, revisionId).subscribe((data: any) => {
            console.log(data);
            this.storeData = data;
            
            let that = this;
            this.userName = sessionStorage.getItem("username");
            this.storeeffDate = this.formatDate(this.storeData.stores[0].effectiveDate);
            this.storeData.stores[0].categories = this.storeData.stores[0].categories.sort((a: any, b: any) => {
                if (a.name > b.name) {
                    return 1;
                } else if (a.name < b.name) {
                    return -1;
                } else {
                    return 0;
                }
            });

            this.storeData.stores[0].categories.forEach(function (itm: any) {
                console.log(itm.deliveryOpportunities[0].value.slice(0, 1));
                if (itm.deliveryOpportunities[0].value.slice(0, 1) == "Y") {
                    that.mon = that.mon + 1;
                }
                if (itm.deliveryOpportunities[0].value.slice(1, 2) == "Y") {
                    that.tue = that.tue + 1;
                }
                if (itm.deliveryOpportunities[0].value.slice(2, 3) == "Y") {
                    that.wed = that.wed + 1;
                }
                if (itm.deliveryOpportunities[0].value.slice(3, 4) == "Y") {
                    that.thu++;
                }
                if (itm.deliveryOpportunities[0].value.slice(4, 5) == "Y") {
                    that.fri++;
                }
                if (itm.deliveryOpportunities[0].value.slice(5, 6) == "Y") {
                    that.sat++;
                }
                if (itm.deliveryOpportunities[0].value.slice(6, 7) == "Y") {
                    that.sun++;
                }
            });

            this.supplyingDepotName = this.storeData.stores[0].supplyingDepotName != undefined ? this.storeData.stores[0].supplyingDepotName : '';
           

            this.populateHistoricalFutureDO(this.storeData.stores[0].categories[0].name);
            this.categories = this.storeData.stores[0].categories.length;
            

        }
            , (err: any) => {
                this._globalFunc.logger("Store serch service failed.");
                this.storedetailListLength = 0;
                this._router.navigate(['/error404']);
            }
        );


    }
    getStoreDetails(storeid: any): any {
        this.showLoader = true;
        console.log(storeid);
        let searchTerm = 'stores';
        this.start = 0;
        this.count = 0;
        // console.log(value);
        this._globalFunc.logger("Calling catalogue list service.");
        this._postsService.getStoreItem(this.customerName, storeid).subscribe((data: any) => {
           
            this.storeData = data;           
            this.storeDataList = [];
            this.storeDataList = data.stores;
            
            if(this.isAmazonCatelogType)
            {
                for(let obj of this.storeDataList[0].merchants)
            {
                this.merchantID = obj.merchantId;  
                console.log("this.merchantID"+this.merchantID);
            }
                this.merchants = this.storeDataList.merchants; 
                if(this.storeDataList[0].tradingName == "AMAZON STORE PICK") 
                {
                    this.threshold = this.storeDataList[0].minimumExpetedInventory;
                    this.merchantforStorePick = true;
                    console.log("Threshold"+this.storeDataList[0].minimumExpetedInventory);
                }
               
                console.log(this.merchantforStorePick);
                console.log(this.threshold);
            }
                    
            console.log("StoreData ==>"+JSON.stringify(this.storeDataList));
            console.log("StoreData ==>"+JSON.stringify(this.storeDataList[0].storeId));
            console.log("STorepick "+this.merchantforStorePick)
           
            
            let that = this;
            this.userName = sessionStorage.getItem("username");
            this.storeeffDate = this.formatDate(this.storeData.stores[0].effectiveDate);
            this.storeData.stores[0].categories = this.storeData.stores[0].categories.sort((a: any, b: any) => {
                if (a.name > b.name) {
                    return 1;
                } else if (a.name < b.name) {
                    return -1;
                } else {
                    return 0;
                }
            });
           
                
                
            
            this.storeData.stores[0].categories.forEach(function (itm: any) {
                console.log(itm.deliveryOpportunities[0].value.slice(0, 1));
                if (itm.deliveryOpportunities[0].value.slice(0, 1) == "Y") {
                    that.mon = that.mon + 1;
                }
                if (itm.deliveryOpportunities[0].value.slice(1, 2) == "Y") {
                    that.tue = that.tue + 1;
                }
                if (itm.deliveryOpportunities[0].value.slice(2, 3) == "Y") {
                    that.wed = that.wed + 1;
                }
                if (itm.deliveryOpportunities[0].value.slice(3, 4) == "Y") {
                    that.thu++;
                }
                if (itm.deliveryOpportunities[0].value.slice(4, 5) == "Y") {
                    that.fri++;
                }
                if (itm.deliveryOpportunities[0].value.slice(5, 6) == "Y") {
                    that.sat++;
                }
                if (itm.deliveryOpportunities[0].value.slice(6, 7) == "Y") {
                    that.sun++;
                }
            });

            this.supplyingDepotName = this.storeData.stores[0].supplyingDepotName != undefined ? this.storeData.stores[0].supplyingDepotName : '';

            this.populateHistoricalFutureDO(this.storeData.stores[0].categories[0].name);
            /*this.storeData.stores.forEach(function(stores:any){
                if(this.storeData.stores[0].supplyingDepotName!="" ||this.storeData.stores[0].supplyingDepotName!=undefined ){
                    this.supplyingDepotName = this.storeData.stores[0].supplyingDepotName;
                }
            })*/


            this.categories = this.storeData.stores[0].categories.length;
        }
            , (err: any) => {
                this._globalFunc.logger("Store serch service failed.");
                this.storedetailListLength = 0;
                this._router.navigate(['/error404']);
            }
        );
        console.log("DATA ==>>"+JSON.stringify(this.storeData));
        this.showLoader = false;
    }

    gotoBottom() {
        this._globalFunc.logger("Scroll to bottom");
        this._globalFunc.autoscrolldown();
    }

    getAttributeValue(data: any, lookupVal: any, serchVal: any, startDate: boolean = false) {
        this._globalFunc.logger("checking fo getStoreIdPrefix details");
        console.log(data);
        this._globalFunc.logger("Checking data Value.");
        var dataAvail: boolean = false;
        var result;
        data.forEach(function (item: any, i: any) {
            console.log(item);
            item[lookupVal].forEach(function (lookupdata: any, index: any) {
                if (lookupdata.type == serchVal) {
                    dataAvail = true;
                    console.log(lookupdata.values[0].value);
                    console.log(serchVal + " Value Found.");
                    if (startDate) {
                        result = lookupdata.values[0].start;
                    } else {
                        result = lookupdata.values[0].value;
                    }
                    return result;

                }
            })
        });

        if (!dataAvail) {
            result = "No data found";
            this._globalFunc.logger(serchVal + " not found");
            return this.strInfoNotAvail;
        } else {
            return result;
        }
    }
    populateHistoricalFutureDO(category: string) {
        this.historicalCategory = category;
        let that = this;
        that.deliveryOpportunityArray = [];
        let latestStoreData;
        this._postsService.getStoreItemLatest(this.customerName, this.storeid).subscribe((data: any) => {
            latestStoreData = data.stores[0];
            this.storeDataLatest = data;
            latestStoreData.categories.forEach(function (itm: any) {
                if (itm.name == category) {
                    that.deliveryOpportunityArray = itm.deliveryOpportunities;
                }
            });
            this.sortHistoricalData();
        }, (err: any) => {
            this._globalFunc.logger("Store serch service failed.");
            this.storedetailListLength = 0;
            this._router.navigate(['/error404']);
        }
        );
        this.gotoBottom();
    }

    sortHistoricalData() {
        return new Promise((resolve) => {
            let tempArr = this.deliveryOpportunityArray.sort((a: any, b: any) => {
                let aDate = new Date(a.effectiveDate);
                let bDate = new Date(b.effectiveDate);
                if (aDate < bDate) {
                    return 1;
                } else if (aDate > bDate) {
                    return -1;
                } else {
                    return 0;
                }
            });
            this.deliveryOpportunityArray = tempArr;
            resolve(true);
        });
    }

    getStoreAvailability(data: any, serchVal: any) {
        this._globalFunc.logger("checking fo getStoreIdPrefix details");
        console.log(data);
        this._globalFunc.logger("Checking data Value.");
        var dataAvail: boolean = false;
        var result;
        data.forEach(function (item: any, i: any) {
            console.log(item);
            result = item[serchVal];
        });
        return result;
    }

    changeOrgType(page: any, orgType: any) {

        this.storedetailListLength = 0;
        this.hitNext = false;
        this.start = 0;
        this.count = 0;
        this.toggleOrganisation();
        this._router.navigate([page, orgType]);
    }

    toggleOrganisation() {
        this._globalFunc.logger("Toggle customer list.");
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
            border.classList.remove('searchborder');
        }
        else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
            border.classList.add('searchborder');
        }
    }
    formatDate(createdAtDate: string, separator: string = "/") {
        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
            if (createdAtDate.indexOf('/') >= 0) {
                return createdAtDate;
            } else {
                var d = new Date(createdAtDate);
                var yyyy = d.getUTCFullYear();
                var mm = d.getUTCMonth() < 9 ? "0" + (d.getUTCMonth() + 1) : (d.getUTCMonth() + 1); // getMonth() is zero-based
                var dd = d.getUTCDate() < 10 ? "0" + d.getUTCDate() : d.getUTCDate();
                return dd + separator + mm + separator + yyyy;
            }
        } else {
            return '-';
        }

    }
    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
            if (!userData.isSessionActive) { // false
                // User session has expired.
                sessionStorage.clear();
                this._router.navigate(['']);
            } else {
                this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                let changeOrgAvailable: any = [];
                let orgImgUrl = "";
                let orgApiNameTemp = "";
                this.organisationList.forEach(function (item: any) {
                    if (item.customerName !== customerName) {
                        changeOrgAvailable.push(item);
                    } else {
                        orgImgUrl = item.customerName + ".png";
                        orgApiNameTemp = item.customerName;
                    }
                });
                this.organisationListChange = changeOrgAvailable;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                // this.disabledCusChange = false;
                if (this.organisationList.length == 1) {
                    this.disabledCusChange = true;
                }

                console.log("this.organisationList234");
                console.log(this.organisationList);
                let that = this;
                let catalogueEnquiry = true;
                let dashboardMenuPermission = false;
                let userHavingPermission = false;
                let editStoreDetails = false;
                this.organisationList.forEach(function (organisation: any) {

                    if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                        console.log("organisation : " + that.customerName);
                        console.log(organisation);
                        organisation.permissionRoleList.forEach(function (permRoleList: any) {
                            console.log("11");
                            console.log(permRoleList);
                            permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                console.log("22");
                                if (permRoleDtlList.permissionGroup == 'catalogueManagement') {
                                    userHavingPermission = true;
                                    permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                        if (permList.permissionName == 'catalogueManagement' && permList.permissionStatus == 'active') {
                                            dashboardMenuPermission = true;
                                        }

                                        switch (permList.permissionName) {
                                            case "catalogueEnquiry":
                                                console.log("55");
                                                catalogueEnquiry = false;
                                                //alert(filfilment);
                                                break;
                                            case "editStoreDetails":
                                                console.log("55");
                                                editStoreDetails = true;
                                                //alert(filfilment);
                                                break;
                                        }
                                    });
                                }
                            });
                        });
                    }
                });
                that = null;
                if (catalogueEnquiry) {
                    // redirect to order
                    this._router.navigate(["store", customerName]);
                }
                if (editStoreDetails) {
                    this.allowEdit = true;
                }
            }
            //alert(this.disabledfilfilment);
        });
    }

    selectTab(selectedTab: string = "maindetails") {
        this.selectedTab = selectedTab
    }

    //auto scroll
    scrollTo() {
        window.scrollTo({
            top: 800,
            behavior: "smooth"
        });
    }

    allowEditMethod(){
        this._router.navigate(['store', this.customerName, 'storedetails', this.storeid, 'edit'], {fragment: this.selectedTab});
        /*if(this.storeData.stores[0].categories == null) {
            this.showErrorPopupOnEdit();
        } else {
            if(this.storeDataLatest.stores[0].categories.length == this.storeData.stores[0].categories.length) {
                this._router.navigate(['store', this.customerName, 'storedetails', this.storeid, 'edit'], {fragment: this.selectedTab});
            } else {
                this.showErrorPopupOnEdit();
            }
        }*/
    }

    showErrorPopupOnEdit(){
        let sessionModal = document.getElementById('errormodal');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('errormodal').style.display = "block";
    }

    closePopup(modalBoxId = "") {
        if (modalBoxId != "") {
            let sessionModal = document.getElementById(modalBoxId);
            sessionModal.classList.remove('out');
            sessionModal.classList.add('in');
            sessionModal.style.display = 'none';
        } else {
            let sessionModal = document.getElementById('popup_SessionExpired');
            sessionModal.classList.remove('out');
            sessionModal.classList.add('in');
            sessionModal.style.display = 'none';
            let dt = this._cfunction.getUserSession();
            this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            }, (error: any) => {
            });
            sessionStorage.clear();
            this._router.navigate(['']);
        }

    }
}