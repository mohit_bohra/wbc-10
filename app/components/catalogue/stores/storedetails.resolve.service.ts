import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot,RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import { Services } from '../../../services/app.services';
import { StoreDetail } from './storedetails';

@Injectable()
export class storeDetailResolve implements Resolve<any> {

    constructor(private _getService: Services, private router: Router) { }
    
    resolve(route: ActivatedRouteSnapshot, state:RouterStateSnapshot): Observable<any> {
        let orgType = route.params['orgType'];
        let storeid = route.params['storeid'];
        return this._getService.getStoreItemLatest(orgType, storeid);
    }
}