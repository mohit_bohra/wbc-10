import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreComponent } from './store.component';

// route Configuration
const storeRoutes: Routes = [
  { path: '', component: StoreComponent },
  { path: 'storedetails/:storeid', loadChildren: './stores/storedetails.module#StoredetailsModule' },
  //{ path: 'upload', loadChildren: './upload/catalogueupload.module#CatalogueuploadModule' },
  //{ path: 'export', loadChildren: './export/catalogueexport.module#CatalogueexportModule' },
  //{ path: ':catalogueType/:catalogueId', loadChildren: './view/productdetail.module#ProductDetailModule' }
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
  { path: 'export', loadChildren: './stores/storeExport/storeexport.module#StoreexportModule'}
];


@NgModule({
  imports: [RouterModule.forChild(storeRoutes)],
  exports: [RouterModule]
})
export class StoreComponentRoutes { }
