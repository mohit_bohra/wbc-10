import { Component } from '@angular/core';
import { Services } from '../../services/app.services';
import { commonfunction } from '../../services/commonfunction.services';
import { GlobalComponent } from '../global/global.component';
import { Constant } from '../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'store-selector',
    templateUrl: 'store.component.html',
})
export class StoreComponent {
    public catalgoueDate: Date = new Date();
    objConstant = new Constant();
    whoImgUrl: any;
    catalogueCode: any;
    catalogueCodeSelected: any;
    catalogueList: any;
    catalogueListLength: any;
    catalogueUpdatedDate: any;
    catalogueno: string;
    showLoader: boolean;
    hideOrganisation: boolean = true;
    catalogueResult: boolean = true;
    printHeader: boolean = true;
    pageState: number = 3;
    customerName: string;
    customerDispayName: string;
    style: string;
    organisationList: any;
    organisationListChange: any;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disabledOrder: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    private _sub: any;
    disabledExportCatalogue: boolean;
    disabledUploadCatalogue: boolean;
    disabledDashboard: boolean = false;

    //for catalogue Enquiry redevelopment
    catalogueTypes: any;
    catalogueType: string;
    cataloguemasterListLength: any;
    cataloguedetailListLength: any;
    catalogueDetailList: any;
    merchantID : any;
    strInfoNotAvail: String;
    start: number;
    limit: number;
    count: number;
    hitNext: boolean;


    public todayDate: Date = new Date();

    homeUrl: any;
    custPermission: any[];

    imgName: string = "";
    terminateFlg: boolean;

    isAmazonCatelogType: boolean = false;

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, public _cfunction: commonfunction) {
        // img urls
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
    

    }

    // function works on page load
    ngOnInit() {
        this._globalFunc.logger("Catalogue search screen loading.");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {
            this.showLoader = false;
            this.catalogueno = '';
            // user permission
            this.disabledCusChange = false;
            this.disabledPermission = false;
            this.disabledReports = false;
            this.disabledCatalogue = false;
            this.disabledOrder = false;
            this.disabledOrderRaise = false;
            this.disabledCatalogueRaise = false;
            this.disabledManageCatalogue = false;
            this.disabledExportCatalogue = false;
            this.disabledUploadCatalogue = false;
            this.strInfoNotAvail = "-";
            this.catalogueType = "";

            // window scroll function
            this._globalFunc.autoscroll();

            // set header basket data
            this.pageState = 3;
            this.start = 0;
            this.limit = this.objConstant.SCROLL_RECORD_LIMIT;
            this.count = 0;
            this.hitNext = false;
            sessionStorage.setItem('pageState', '3');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                if (this.customerName === 'amazon') {                   
                    this.isAmazonCatelogType = true;
                }
                else{
                    this.isAmazonCatelogType = false;
                }

                this.imgName = this.getCustomerImage(params.orgType);
                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
                    if (res == 'sessionActive') {
                        // this.imgName = this.getCustomerImage(params.orgType);
                        // console.log("Module Name::", this.moduleName);
                        // this.getConfigService(this.moduleName);
                    }
                }, reason => {
                    console.log(reason);
                });

                // let reports;
                // let orders;
                // let catalogues;
                // let cataloguesRaise;
                // let uploadCatalogue;
                // let exportCatalogue;
                // let accessMgmt;
                // let redirct;
                // let changeOrgAvailable = new Array;
                // let catManagement;
                // let dashboardAccess;
                // // list of org assigned to user
                // this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));

                // this.organisationList.forEach(function (item: any) {
                //     if (item.orgType !== cusType) {
                //         changeOrgAvailable.push(item);
                //     }
                // });
                // this.organisationListChange = changeOrgAvailable;
                // // this.disabledCusChange = false;
                // if (this.organisationList.length == 1) {
                //     this.disabledCusChange = true;
                // }

                // document.getElementById('orderArrowSpan').style.display = 'block';
                // document.getElementById('orderMob').style.display = 'block';
                // document.getElementById('orderRaiseMob').style.display = 'none';

                // document.getElementById('catalogueMob').style.display = 'block';
                // document.getElementById('catalogueArrowSpan').style.display = 'none';
                // document.getElementById('catalogueRaiseMob').style.display = 'none';
                // document.getElementById('reportsMob').style.display = 'block';
                // document.getElementById('permissonsMob').style.display = 'block';

                // if (document.getElementById('orderArrow').classList.contains('glyphicon-menu-up') === true) {
                //     let sublinks = document.getElementById('orderArrow');
                //     sublinks.classList.remove('glyphicon-menu-up');
                //     sublinks.classList.add('glyphicon-menu-down');
                //     document.getElementById('orderRaiseMob').style.display = 'none';
                // }

                // if (document.getElementById('catalogueArrow').classList.contains('glyphicon-menu-up') === true) {
                //     let sublinks = document.getElementById('catalogueArrow');
                //     sublinks.classList.remove('glyphicon-menu-up');
                //     sublinks.classList.add('glyphicon-menu-down');
                //     document.getElementById('catalogueRaiseMob').style.display = 'none';
                // }

                let custPerm: any = [];

                // this.organisationList.forEach(function (item: any) {
                //     if (item.orgType === cusType) {
                //         // logic for permission set
                //         orgImgUrl = item.imgUrl;
                //         orgApiNameTemp = item.orgName;

                //         if ((item.permissions.menuPermission.orders).toLowerCase() == 'none') {
                //             orders = true;
                //             document.getElementById('orderMob').style.display = 'none';
                //             document.getElementById('orderRaiseMob').style.display = 'none';
                //         }

                //         if ((item.permissions.menuPermission.raiseNewOrder).toLowerCase() == 'none') {
                //             document.getElementById('orderArrowSpan').style.display = 'none';
                //             document.getElementById('orderRaiseMob').style.display = 'none';
                //         }

                //         if ((item.permissions.menuPermission.catalogueManagement).toLowerCase() == 'none') {
                //             catManagement = true;
                //         } else {
                //             catManagement = false;
                //         }


                //         if ((item.permissions.menuPermission.catalogueEnquiry).toLowerCase() == 'none') {
                //             catalogues = true;
                //             document.getElementById('catalogueMob').style.display = 'none';
                //             document.getElementById('catalogueRaiseMob').style.display = 'none';
                //             redirct = 1;
                //             if (sessionStorage.getItem('page_redirect') == '0') {
                //                 sessionStorage.removeItem('page_redirect');
                //             } else {
                //                 sessionStorage.setItem('page_redirect', '1');
                //             }
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.catalogAdminPermission.addNewProducts).toLowerCase() == 'none') {
                //             cataloguesRaise = true;
                //             document.getElementById('catalogueArrowSpan').style.display = 'none';
                //             document.getElementById('catalogueRaiseMob').style.display = 'none';
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }
                //         console.log(item.permissions.catalogAdminPermission.uploadCatalogue);
                //         if ((item.permissions.catalogAdminPermission.uploadCatalogue).toLowerCase() == 'none') {
                //             uploadCatalogue = true;
                //         } else {
                //             uploadCatalogue = false;
                //         }
                //         // item.permissions.catalogAdminPermission.exportCatalogue = 'W';
                //         if ((item.permissions.catalogAdminPermission.exportCatalogue).toLowerCase() == 'none') {
                //             exportCatalogue = true;
                //         } else {
                //             exportCatalogue = false;
                //         }

                //         if ((item.permissions.menuPermission.reports).toLowerCase() == 'none') {
                //             reports = true;
                //             document.getElementById('reportsMob').style.display = 'none';
                //         }

                //         if ((item.permissions.menuPermission.accessMgmt).toLowerCase() == 'none') {
                //             accessMgmt = true;
                //             document.getElementById('permissonsMob').style.display = 'none';
                //         }

                //         if ((item.permissions.menuPermission.dashboards).toLowerCase() == 'none') {
                //             dashboardAccess = true;
                //         //    document.getElementById('permissonsMob').style.display = 'none';
                //         }
                //     }

                //     /* Check atleast permission should exist for customer otherwise hide customer */
                //     if (item.permissions.menuPermission.catalogueEnquiry == 'none' &&
                //         item.permissions.menuPermission.catalogueManagement == 'none' &&
                //         item.permissions.menuPermission.orders == 'none' &&
                //         item.permissions.menuPermission.reports == 'none' &&
                //         item.permissions.menuPermission.accessMgmt == 'none' &&
                //         item.permissions.menuPermission.dashboards == 'none') {
                //         custPerm[item.orgType] = false;
                //     } else {
                //         custPerm[item.orgType] = true;
                //     }
                //     /* Check atleast permission should exist for customer otherwise hide customer - END */
                // });

                // if (redirct == 1) {
                //     redirct = 0;
                //     this.redirect('/cataloguemanagement', this.customerName);
                // }

                //this.custPermission = custPerm;
                this.orgimgUrl = orgImgUrl;
                // this.orgApiName = (orgApiNameTemp == 'rontec') ? 'rontec' : orgApiNameTemp;
                // this.disabledReports = reports;
                // this.disabledOrder = orders;
                // this.disabledOrderRaise = orders;
                // this.disabledCatalogue = catalogues;
                // this.disabledCatalogueRaise = cataloguesRaise;
                // this.disabledPermission = accessMgmt;
                // this.disabledManageCatalogue = catManagement;
                // this.disabledUploadCatalogue = uploadCatalogue;
                // this.disabledExportCatalogue = exportCatalogue;
                // this.disabledDashboard = dashboardAccess;
                this.orgApiName = this.customerName;
                //  this.getAllCatalogues();
                this.getAllMasterData();
                // this.getCatalogueItem("");
                this.getstoreList("");
            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            // let i = 0;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);

            // logic for resize pop up 
            window.onresize = () => {
                if (screen == "desktop") {
                    if (window.innerWidth < this._globalFunc.stdWidth) {
                        // this.catalogueResult = false;
                    } else {
                        //  this.catalogueResult = true;
                    }
                } else {
                    if (window.innerWidth > this._globalFunc.stdWidth) {
                        //   this.catalogueResult = true;
                        document.getElementById("resultModalClose").click();
                    } else {
                        //  this.catalogueResult = false;
                    }
                }

                // get device to populate some part of ui
                screen = this._globalFunc.getDevice(window.innerWidth);
            };
            window.onscroll = () => {
                this.scrollFunction();
            };
        }
    }

   
    onScrolltop() {
        this._globalFunc.autoscroll();
    }

    scrollFunction() {
        if (document.body.scrollTop > 800 || document.documentElement.scrollTop > 800) {
            // document.getElementById("topBtn").style.display = "block";
        } else {
            // document.getElementById("topBtn").style.display = "none";
        }
    }

    resetOnClose() {
        this.hitNext = false;
    }

    onScrollDown(catalogueCode: any, catalogueType: any) {
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                // alert(catalogueType);
                catalogueType = "stores";
                this.start = this.start + this.limit;
                if (this.count == this.limit && this.hitNext == true) {
                    this.showLoader = true;
                    this._postsService.getstoreList(this.orgApiName,this.start,this.limit ).subscribe((data: any) => {
                        this.showLoader = false;
                        this.count = data.paginationMetaData.count;
                        console.log("DATA"+data)
                       this.catalogueDetailList = this.catalogueDetailList.concat(data.stores);
                            
                      
                       
                    }
                        , (err: any) => {
                            this.catalogueno = '';
                            this.showLoader = false;
                            this.cataloguedetailListLength = 0;
                            this._router.navigate(['/error404']);
                        }
                    );
                }
            }
        }, reason => {
            console.log(reason);
        });

    }

    // navigate function
    redirect(page: any, orgType: any) {
        this._router.navigate(['catalogue', encodeURIComponent(orgType), 'export']);
    }
    // change organisation
    changeOrgType(page: any, orgType: any) {
        this.getUserSession(orgType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                // reset field on change of organisation 
                this.catalogueno = '';
                this.catalogueList = [];
                this.catalogueListLength = 0;
                this.catalogueResult = true;
                this.catalogueType = '';
                this.catalogueCode = '';
                this.hitNext = false;
                this.start = 0;
                this.count = 0;
                this.toggleOrganisation();
                this._router.navigate(['store', orgType]);
            }
        });
    }

    // download functionality
    exportData() {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                this._globalFunc.logger("Export to excel");
                return this._globalFunc.exportData('exportable', this.customerName + 'storeData');
            }
        });

    }

    printCatalogue() {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                this._globalFunc.logger("Print catalogue list");
                var printHeaderNav = document.getElementById('printHeader').innerHTML;
                var printContentsNav = document.getElementById('printNavCatalogue').innerHTML;
                var printContents = document.getElementById('exportable').innerHTML;
                if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
                    var popupWin = window.open('', '_blank');
                    popupWin.document.open();
                    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContentsNav + printContents + '</body></html>');
                    popupWin.document.close();
                    setTimeout(function () { popupWin.close(); }, 1000);
                } else {
                    var popup = window.open('', '_blank');
                    popup.document.open();
                    popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContentsNav + printContents + '</body></html>');
                    popup.document.close();
                }
            }
        });
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
        this._globalFunc.logger("Unloading catalogue search screen.");
        this._globalFunc.sentLoggerBatchToServer();
    }

    // change organisation div show and hide
    toggleOrganisation() {
        this._globalFunc.logger("Toggle customer list.");
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
            border.classList.remove('searchborder');
        }
        else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
            border.classList.add('searchborder');
        }
    }

    // close operation of pop up
    closeOperation() {
        this.catalogueList = '';
    }

    // key press event to disable special charater
    keyPress(event: any) {
        this._globalFunc.alphaNumericWithSpace(event);
    }


    // get all catalogue detail
    getstoreList(value: any) {
        this.showLoader = true;
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
        //let str = value.catalogueType;
        console.log(value);
        let searchTerm = 'stores';
        /* if (str.search(':') != -1) {
            // console.log("adf");
            var strArr = str.split(':');
            searchTerm = strArr[1];
        } else {
            searchTerm = value.catalogueType;
        } */

        this.showLoader = true;
        this.start = 0;
        this.count = 0;
        // console.log(value);
        this._globalFunc.logger("Calling catalogue list service.");
        this.showLoader = true;
        this._postsService.getstoreList(this.orgApiName,this.start,this.limit).subscribe((data: any) => {
            
            this.catalogueCodeSelected = searchTerm;
            this.start = 0;
            this.count = 0;
            this.catalogueDetailList = [];
            if (data.stores == undefined) {
                this.cataloguedetailListLength = 0;
                this._globalFunc.logger("catalogue service missing items node.");
            } else {
                this.hitNext = true;
                this.cataloguedetailListLength = data.stores.length;
                this.count = data.paginationMetaData.count;
                this.limit = data.paginationMetaData.limit;
                this._globalFunc.logger("catalogue service data found.");
            }
            this.catalogueDetailList = data.stores;  
            //if(this.catalogueDetailList.tradingName == "AMAZON STORE PICK"){
                //for(let obj of this.catalogueDetailList[0].merchants)
                //{
                  //this.merchantID =  this.catalogueDetailList[0].merchants[0].merchantId;  
                  //console.log("MerchantID"+obj.merchantId)             
               // }
           // }
           
           
                   
           // console.log(JSON.stringify(this.catalogueDetailList) + " JSON.stringify(this.catalogueDetailList)");
            //console.log(this.catalogueDetailList)
         
            if (window.innerWidth < this._globalFunc.stdWidth) {
                document.getElementById("openModalResult").click();
            } else {
                this.catalogueResult = false;
            }
            this.showLoader = false;
        }
            , (err: any) => {
                this._globalFunc.logger("catalogue service failed.");
                this.catalogueno = '';
                this.showLoader = false;
                this.cataloguedetailListLength = 0;
                this._router.navigate(['/error404']);
            }
        );
    }

   

    // get relevant order detail
    getAllMasterData() {
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
        this.showLoader = true;
        this._globalFunc.logger("calling catalogue master service.");
        this._postsService.getAllMasterData(this.orgApiName).subscribe(data => {
            this.showLoader = false;
            if (data.catalogueTypes.length == 0) {
                this.cataloguemasterListLength = 0;
                this._globalFunc.logger("returned empty data.");
            } else {
                this._globalFunc.logger("master service data found.");
                for (var k = 0; k < data.catalogueTypes.length; k++) {
                    if (data.catalogueTypes[k].catalogueType.indexOf(':') > 0) {
                        data.catalogueTypes[k].catalogueType = data.catalogueTypes[k].catalogueType.split(':')[1];
                    }
                }
                this.cataloguemasterListLength = data.catalogueTypes.length;
            }
            this.catalogueTypes = data.catalogueTypes;
        }
            , err => {
                this._globalFunc.logger("catalogue master service failed.");
                this.cataloguemasterListLength = 0;
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
        );
    }

    returnZero() { 
        return 0; 
      }
      
    // to show specific content in table 
    getMinValue(mapValues: any) {
        this._globalFunc.logger("Checking Min Value.");
        var dataAvail: boolean = false;
        for (var i = 0; i < mapValues.length; i++) {
            if ((mapValues[i].type).toUpperCase() == 'MIN') {
                dataAvail = true;
                this._globalFunc.logger("Min Value Found.");
                return mapValues[i].values[0].value;
            }
        }

        if (!dataAvail)
            return this.strInfoNotAvail;
    }

    // to show specific CLIENTID value if avaliable
    getAsinValue(mapValues: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < mapValues.length; i++) {
            if ((mapValues[i].type).toUpperCase() == 'CLIENTID') {
                dataAvail = true;
                this._globalFunc.logger("CLIENTID(ASIN) Value Found.");
                return mapValues[i].values[0].value;
            }
        }

        if (!dataAvail)
            return '-';
    }



    // to show specific CLIENTID value if avaliable
    getAttVal(mapValues: any, serchVal: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < mapValues.length; i++) {
            if ((mapValues[i].type).toUpperCase() == serchVal) {
                dataAvail = true;
                this._globalFunc.logger(serchVal + " Value Found.");
                console.log(mapValues[i].values[0].value);
                return mapValues[i].values[0].value;
            }
        }

        if (!dataAvail)
            return '-';
    }

    // to show specific content in table 
    getWholesalePriceValue(pricesValues: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < pricesValues.length; i++) {
            if ((pricesValues[i].type).toUpperCase() == 'WSP') {
                if ((pricesValues[i].values[0].currency).toUpperCase() == 'GBP' && (this.formatDate(pricesValues[i].values[0].start) <= this.formatDate(this.todayDate.toDateString())) && (this.formatDate(pricesValues[i].values[0].end) >= this.formatDate(this.todayDate.toDateString()))) {
                    {
                        this._globalFunc.logger("WSP Value Found.");
                        dataAvail = true;
                        return pricesValues[i].values[0].value;
                    }
                }
            }
        }
        if (!dataAvail)
            return this.strInfoNotAvail;
    }
    // to show specific content in table 
    getWholesalePackSizeValue(pricesValues: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < pricesValues.length; i++) {
            if ((pricesValues[i].type).toUpperCase() == 'CS') {
                if (this.formatDate(pricesValues[i].values[0].start) <= this.formatDate(this.todayDate.toDateString()) && this.formatDate(pricesValues[i].values[0].end) >= this.formatDate(this.todayDate.toDateString())) {
                    {
                        this._globalFunc.logger("CS Value Found.");
                        dataAvail = true;
                        return pricesValues[i].values[0].value;
                    }
                }
            }
        }

        if (!dataAvail)
            return this.strInfoNotAvail;
    }
    // to show specific content in table 
    getWholesalePackTypeValue(pricesValues: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < pricesValues.length; i++) {
            if (pricesValues[i].type.toUpperCase() == 'CS' || pricesValues[i].type.toUpperCase() == 'EA') {
                dataAvail = true;
                // return pricesValues[i].values[0].value;
                return pricesValues[i].type.toUpperCase();
            }
        }

        if (!dataAvail)
            return this.strInfoNotAvail;
    }

    // format date function
    formatDate(createdAtDate: string) {
        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
            if (createdAtDate.indexOf('/') >= 0) {
                return createdAtDate;
            } else {
                var d = new Date(createdAtDate);
                var yyyy = d.getFullYear();
                var mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1); // getMonth() is zero-based
                var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
                // var hh = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
                // var min = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
                return yyyy + "/" + mm + "/" + dd;
            }
        } else {
            return '-';
        }
    }


    // open in new tab for detail
    redirect_detail(orgType: any, storeId: any) {
        this.getUserSession(orgType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                this._globalFunc.logger("Redirect to catalogue details.");
                let storeid = storeId;
                let url = this.homeUrl + 'store' + '/' + encodeURIComponent(orgType) + '/storedetails/' + storeid;
                sessionStorage.setItem('pageState', '999');
                window.open(url);
                //this._router.navigate(['store', encodeURIComponent(orgType), 'storedetails', storeid]); 
            }
        });
    }

    getProductDetailByID(id: string) {
        var obj = this.catalogueDetailList.filter(function (node: any) {
            return node.id == id;
        });
        console.log(obj[0]);
        return obj[0];
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {
            this.showLoader = true;
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else {
                        reject('sessionInactive')
                        sessionStorage.clear();
                        this._router.navigate(['']);
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        resolve('sessionActive');
                        this.terminateFlg = false;
                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
                        this.organisationListChange = changeOrgAvailable;
                        this.orgimgUrl = orgImgUrl;
                        this.orgApiName = orgApiNameTemp;
                        // this.disabledCusChange = false;
                        if (this.organisationList.length == 1) {
                            this.disabledCusChange = true;
                        }

                        console.log("this.organisationList234");
                        console.log(this.organisationList);
                        let that = this;
                        let catalogueEnquiry = true;
                        let dashboardMenuPermission = false;
                        let userHavingPermission = false;
                        this.organisationList.forEach(function (organisation: any) {

                            if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                                console.log("organisation : " + that.customerName);
                                console.log(organisation);
                                organisation.permissionRoleList.forEach(function (permRoleList: any) {
                                    console.log("11");
                                    console.log(permRoleList);
                                    permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                        console.log("22");
                                        if (permRoleDtlList.permissionGroup == 'catalogueManagement') {
                                            userHavingPermission = true;
                                            permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                                if (permList.permissionName == 'catalogueManagement' && permList.permissionStatus == 'active') {
                                                    dashboardMenuPermission = true;
                                                }

                                                switch (permList.permissionName) {
                                                    case "catalogueEnquiry":
                                                        console.log("55");
                                                        catalogueEnquiry = false;
                                                        //alert(filfilment);
                                                        break;
                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                        that = null;
                        if (catalogueEnquiry) {
                            // redirect to order
                            this._router.navigate(["cataloguemanagement", customerName]);
                        }
                    }
                }
                this.showLoader = false;
                //alert(this.disabledfilfilment);
            });
        });
    }

    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }
}
