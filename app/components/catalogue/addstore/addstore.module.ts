import { NgModule } from '@angular/core';
import { StoreComponentRoutes } from './addstore.routes';
import { AddstoreComponent } from './addstore.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { DatepickerModule } from '../../datepicker/ng2-bootstrap';
import { Daterangepicker } from 'ng2-daterangepicker';

import { TooltipModule } from "ngx-tooltip";
import { DOSortCategoryPipe } from "./sortCategories.pipes";


@NgModule({
  imports: [
    StoreComponentRoutes,
    CommonModule,
    FormsModule,
    DatepickerModule.forRoot(),
    Daterangepicker,
   TooltipModule,
  

  ],
  exports: [],
  declarations: [AddstoreComponent ,DOSortCategoryPipe],
  providers: [Services, GlobalComponent, commonfunction]
})
export class AddstoreComponentModule { }
