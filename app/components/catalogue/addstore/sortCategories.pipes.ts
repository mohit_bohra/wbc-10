import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: "sortCategory"
})
export class DOSortCategoryPipe {
  transform(array: Array<any>, args: string): Array<any> {

    array.sort((a: any, b: any) => {
      if (a.name > b.name) {
        return 1;
      } else if (a.name < b.name) {
        return -1;
      } else {
        return 0;
      }
    });
    return array;
  }
}