import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddstoreComponent } from './addstore.component';

// route Configuration
const addstoreRoutes: Routes = [
  { path: '', component: AddstoreComponent },
 // { path: 'storedetails/:storeid', loadChildren: './stores/storedetails.module#StoredetailsModule' }
  //{ path: 'upload', loadChildren: './upload/catalogueupload.module#CatalogueuploadModule' },
  //{ path: 'export', loadChildren: './export/catalogueexport.module#CatalogueexportModule' },
  //{ path: ':catalogueType/:catalogueId', loadChildren: './view/productdetail.module#ProductDetailModule' }
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(addstoreRoutes)],
  exports: [RouterModule]
})
export class StoreComponentRoutes { }
