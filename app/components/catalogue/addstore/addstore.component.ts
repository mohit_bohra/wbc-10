import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../../services/app.services';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalComponent } from '../../global/global.component';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DialogService } from '../../../services/dialog.service';
import { StoreDetails } from "../models/store-details";
import { StoreInfo } from "../models/store-info";
import { DeliveryOpportunity } from "../models/deliveryOpportunities";
import { Category } from "../models/categories";
import { Audit } from "../models/audit";
import { document } from '../../datepicker/utils/facade/browser';
import { TransitInformation } from "../models/transitInformation";
import { DaterangepickerConfig } from "ng2-daterangepicker";
import { Container } from '../../../../node_modules/@angular/compiler/src/i18n/i18n_ast';
import { Merchants } from '../models/merchants';
import { transitInformationForAmazon } from '../models/transitInformationForAmazon';
declare let saveAs: any;

@Component({
    templateUrl: 'addstore.component.html',
    providers: [commonfunction]
})
export class AddstoreComponent implements OnInit {
    storeeffDate: any;
    availability: any;
    errorMsgString: string;
    userName: string;
    storeInfoList: StoreInfo[];
    storeDetails: StoreDetails;
    deliveryOpportunities: DeliveryOpportunity[];
    storeInfo: StoreInfo;
    tooltipMsg: string;
    storeArray: any;
    public today = new Date();
    public tomorrow = new Date(this.today.getTime() + 24 * 60 * 60 * 1000);

    public minDate: Date = this.tomorrow;
    public currentTime = new Date()
    public maxDate = new Date(this.currentTime.getFullYear(), this.currentTime.getMonth() + 1, +0);
    effectivedatedelivery: any;;
    public options: any = {
        locale: { format: 'DD/MM/YYYY' },
        alwaysShowCalendars: false,
        singleDatePicker: true,
        autoApply: true,

        autoUpdateInput: true,
        "drops": "up"

    };
    deliveryOpportunity: DeliveryOpportunity;
    transitInformation: TransitInformation;
    category: Category;
    Audit: Audit;
    @ViewChild('add') addstore: any;
    public showDatepicker: boolean = true;
    storeDescription: string = "";
    strInfoNotAvail: string = "";
    storeIdPrefix: any;
    errorMsgList: any = {};
    errorCount: number = 0;
    customerDispayName: string;
    storedetails: any;
    style: string;
    disabledCusChange: boolean;
    organisationListChange: string;
    hitNext: boolean;
    count: number;
    start: number;
    storedetailListLength: number;
    limit: number;
    storeName: string;
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;
    areaManager: any;
    showLoader: boolean;
    private _sub: any;
    regionCode: any;
    hideOrganisation: boolean = true;
    custPermission: any[];
    makeEffectiveToday: boolean = false;
    effectivedate: any;
    imgName: string = "";
    supplyingDepotName: any;
    storeID: any;
    selectedCategory: string = "";
    selectedCategorySupplyDepot: any = [];
    effectiveImmediately: boolean = false;
    allocationPriority: any = [];
    createdDate: Date;
    supplyingDepot: any;
    filterName: any;
    filterValue: any;
    supplyDeportCategory: any;
    supplyDeportCategoryLen: any = -1;
    availabilityMaster: any = [];
    statusmaster: any = [];
    portMaster: any = [];
    availabilityStatus: any = "ACTIVE";
    terminateFlg: boolean;
    supplyDepotType: any;
    historicalCategory: any = "";
    storeCategoryIndex: number;
    calDefaultDate: any;
    DOCategoriesCurrentRow: any[];
    // minDate: any = new Date();
    currentCalId: string;
    DOCategoriesTomorrowRow: any[];
    popupMsg: string = "";
    messsage: any;
    supplyDepotCatData: any = [];
    deliveryOpportunityArray: any[];
    disableAddNewRow: boolean = false;
    confirmBoxResponse: boolean;
    tomorrowDate: string
    todayDate: string;
    storeDtl: any;
    storePrefix: any[];
    storePrefixfornewStore: any;
    sypplyDeportTypeSelect: any;
    effectiveDate: any;
    monY: number = 0;
    tueY: number = 0;
    wedY: number = 0;
    thuY: number = 0;
    friY: number = 0;
    satY: number = 0;
    sunY: number = 0;
    savedisable: boolean
    fragment: string;
    addrName: any;
    msg: boolean = false;
    validationMsgDO_YN: any = 'Enter Capital Y/N only and 7 character long';
    validationMsgDO_YN_Required: any = 'Delivery Days is required';
    opsValue: any;
    effectiveDateAmazon: any;
    makeEffectivePermission: boolean;
    makeChangeToday: boolean = false;

    //bolt changes
    isAmazonCatelogType: boolean = false;
    amazonRegionCodesData: any[] ; 
    merchantId : any;
    cataloguemasterListLength : any;

    //Dot co.uk Prime Changes
    merchantName : any[];
    merchantIDForPrimeNow : string ;
    merchantIDForCODOTUK : string;   
    rmsSellingLocationForPrimeNow : any;
    rmsSellingLocationForCODOTUK : any;
    inventoryApplicableForPrimeNow : boolean = false;
    inventoryApplicableForCODOTUK : boolean = false;
    priceApplicableForPrimeNow: boolean = false;
    priceApplicableForCODOTUK : boolean = false;
    settlementReportForPrimeNow: boolean = false;
    settlementReportForCODOTUK : boolean = false;
    isCheckedInventoryForPrimeNow : boolean = false;
    isCheckedInventoryForCODOTUK : boolean = false;
    isCheckedPriceForPrimeNow : boolean = false;
    isCheckedPriceForCODOTUK : boolean = false;
    isCheckedSettlementForPrimeNow : boolean = false;
    isCheckedSettlementForCODOTUK : boolean = false;
    merchants:Merchants;
    merchantforPrime : Merchants;
    merchantforCODOTUK : Merchants;
    transitInformationforPrime: transitInformationForAmazon;
    transitInformationforCODOTUK: transitInformationForAmazon;
    marketplaceforPrimeNow : any;
    marketplaceforCODOTUK : any;
    tradingNameforPick : boolean = false;
    minimumExpetedInventory : any = "";

    //Form Validation
    submitted:boolean = false;
    errorFlag :boolean = false;
    popupMsgs: string = "";

    constructor(private _postsService: Services, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _globalFunc: GlobalComponent, public _cfunction: commonfunction, private formBuilder: FormBuilder, public dialogService: DialogService, private daterangepickerOptions: DaterangepickerConfig) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.minDate = this.tomorrow;
        this.merchantName = this.objConstant.merchantName;
        this.marketplaceforPrimeNow = this.objConstant.marketPlaceIdForPrimeNow;
        this.marketplaceforCODOTUK = this.objConstant.marketPlaceIdCODOTUK;
        console.log("CustomerName"+this.customerName);

        this.filterName = 'supplydepot';
        this.filterValue = '@all';
        this.addstore = this.formBuilder.group({});
        this.storePrefix = this.objConstant.storePrefix;
        let isoDate = new Date()
        let tomorrowISODate = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1, 0, 0, 0));

        let todayISODate = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 0, 0, 0));

        this.tomorrowDate = this._cfunction.dateFormator(tomorrowISODate, 1);
        this.todayDate = this._cfunction.dateFormator(todayISODate, 1);

        this.effectivedatedelivery = this.tomorrowDate;
        this.effectiveDate = this.tomorrowDate;

        this.tomorrow = new Date();
        this.tomorrow.setDate(this.today.getDate() + 1);

        this.daterangepickerOptions.settings = {
            minDate: this.tomorrow,
        };
    }
    changeImmediately() {
        console.log('change');
        if (this.makeEffectiveToday == false) {

            this.makeEffectiveToday = !this.makeEffectiveToday;
            document.getElementById('immediatelyModalPageLevel').style.display = 'block';
        }
        else {



            document.getElementById('reverteffectModalPageLevel').style.display = 'block';
            this.showLoader = false;
            // window.location.reload();
            // let navigationExtras: NavigationExtras = {
            //     fragment: this.fragment
            // };

            //this._router.navigate(['/', 'store', this.customerName, 'storedetails', this.storeid, 'edit'], navigationExtras);

        }
    }

    checkInventoryApplicableforPrime(event:any)
    {
    if(event == "true")
        {           
        this.inventoryApplicableForPrimeNow = true;
        this.isCheckedInventoryForPrimeNow = true;
        }
    if(event == "false")
        {                   
        this.inventoryApplicableForPrimeNow = false;
        this.isCheckedInventoryForPrimeNow = false;
            
        }    
    }

    checkInventoryApplicableforCODOTUK(event:any)
    {
    if(event == "true")
        {           
        this.inventoryApplicableForCODOTUK = true;
        this.isCheckedInventoryForCODOTUK = true;
        }
    if(event == "false")
        {                   
        this.inventoryApplicableForCODOTUK = false;
        this.isCheckedInventoryForCODOTUK = false;
            
        }    
    }

    checkPriceApplicableforPrime(event:any)
    {
    if(event == "true")
    {      
        this.priceApplicableForPrimeNow = true;
        this.isCheckedPriceForPrimeNow = true;
    }
    if(event == "false")
    {              
        this.priceApplicableForPrimeNow = false;
        this.isCheckedPriceForPrimeNow = false;        
    }

    }

    checkPriceApplicableforCODOTUK(event:any)
    {
        if(event == "true")
        {      
            this.priceApplicableForCODOTUK = true;
            this.isCheckedPriceForCODOTUK = true;
        }
        if(event == "false")
        {              
            this.priceApplicableForCODOTUK = false;
            this.isCheckedPriceForCODOTUK = false;        
        }
    }

    checkSettlementReportforPrime(event:any)
    { 

    if(event == "true")
    {       
        this.settlementReportForPrimeNow = true;      
        this.isCheckedSettlementForPrimeNow = true;
    }
    if(event == "false")
    {
        this.settlementReportForPrimeNow = false;  
        this.isCheckedSettlementForPrimeNow = false;
    }
   }

   checkSettlementReportforCODOTUK(event:any){  

    if(event == "true")
    {   
        this.settlementReportForCODOTUK = true;
        this.isCheckedSettlementForCODOTUK = true;
    }
    if(event == "false")
    {   
        this.settlementReportForCODOTUK = false;
        this.isCheckedSettlementForCODOTUK = false;   
    }


    }

    yesredirect() {
        this.showLoader = true;
        document.getElementById('reverteffectModalPageLevel').style.display = 'none';
        window.location.reload();
    }
    noredirect() {
        var element = <HTMLInputElement>document.getElementById("effectiveImmediatelyId");

        element.checked = true;



        this.makeEffectiveToday = true;
        this.makeChangeToday = true;

        document.getElementById('reverteffectModalPageLevel').style.display = 'none';
    }
    yesEffect() {
        document.getElementById('immediatelyModalPageLevel').style.display = 'none';

        this.makeEffectiveToday = true;
        this.makeChangeToday = true;

    }

    noEffect() {

        document.getElementById('immediatelyModalPageLevel').style.display = 'none';
        // document.getElementById('effectiveImmediatelyId').removeAttribute('checked'); 
        var element = <HTMLInputElement>document.getElementById("effectiveImmediatelyId");

        element.checked = false;

        this.makeEffectiveToday = false;
        this.makeChangeToday = false;


    }


    ngOnInit() {
        this.transitInformation = new TransitInformation('', '', '');
        this.deliveryOpportunity = new DeliveryOpportunity('', '', '', null);
        this.Audit = new Audit('', '');
        this.category = new Category('', [this.deliveryOpportunity]); 
        
        this.merchantforPrime  = new Merchants(this.merchantId,"","AAAAAA",this.merchantIDForPrimeNow,false,false,false,[]);
        this.merchantforCODOTUK  = new Merchants(this.merchantId,"","AAAAAA",this.merchantIDForCODOTUK,false,false,false,[]);
        
       
        this.storeInfo = new StoreInfo(this.Audit, '', null, '', '', '', '', '', '', this.category, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', false,[this.merchantforPrime,this.merchantforCODOTUK],this.minimumExpetedInventory);
        console.log("Threshold"+this.storeInfo.minimumExpetedInventory);
        console.log("StoreInfo Array2"+JSON.stringify(this.storeInfo));
        let storename = this.storeInfo.storeName;
        this.storeDetails = new StoreDetails([this.storeInfo]);
        var dateEffective = new Date(this.today.getTime() + 24 * 60 * 60 * 1000)
        let todayISODate = this._cfunction.isodate(new Date(this.today.getUTCFullYear(), this.today.getUTCMonth(), this.today.getUTCDate(), 0, 0, 0));
        this.createdDate = this._cfunction.dateFormator(todayISODate, 1);
        this.makeEffectivePermission = true;

        this._globalFunc.logger("Catalogue search screen loading.");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {
            sessionStorage.setItem("unsaveChagne", "false");
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            this._globalFunc.autoscroll();
            this.limit = this.objConstant.SCROLL_RECORD_LIMIT;
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                let that = this;
                this.isAmazonCatelogType = false;
                this.customerName = params.orgType;
                console.log("Parms Customer Name"+this.customerName);
                if (this.customerName == 'mccolls') {
                    this.storePrefixfornewStore = this.objConstant.storePrefixMccolls;
                }
                if (this.customerName == 'rontec' || this.customerName == 'rontec-b') {
                    this.storePrefixfornewStore = this.objConstant.storePrefixRontec;
                }
                if (this.customerName == 'amazon') {
                    this.storePrefixfornewStore = this.objConstant.storePrefixAmazon;
                    this.isAmazonCatelogType = true;
                    this._postsService.getAllCatalogueTypes(this.customerName,'catalogueName','storepick').subscribe((data:any) => {
                        //console.log(data.storeUIEntryConfig);
                        this.showLoader = false;
                        if (data.storeUIEntryConfig.length == 0) {
                            this.cataloguemasterListLength = 0;
                            //this._globalFunc.logger("returned empty data.");
                        } else {
                            //this._globalFunc.logger("master service data found.");
                            for (var k = 0; k < data.storeUIEntryConfig.length; k++) {
                               // console.log(this._globalFunc.titleCase(data.storeUIEntryConfig[k].catalogueCode));
                                //this.amazonRegionCodesData[k] = data.storeUIEntryConfig[k].catalogueCode;
                                 data.storeUIEntryConfig[k].catalogueCode = this._globalFunc.titleCase(data.storeUIEntryConfig[k].catalogueCode);
                               
                               
                            }
                            this.cataloguemasterListLength = data.storeUIEntryConfig.length;
                        }
                        console.log(data.storeUIEntryConfig);
                        this.amazonRegionCodesData = data.storeUIEntryConfig;
                    }
                        , (err:any) => {
                            this._globalFunc.logger("catalogue config service failed.");
                            this.cataloguemasterListLength = 0;
                            this.showLoader = false;
                            this._router.navigate(['/error404']);
                        }
                    );
                    //this.amazonRegionCodesData = this.objConstant.amazonRegionCodes;
                }
                if (this.customerName == 'sandpiper') {
                    this.storePrefixfornewStore = this.objConstant.storePrefixSandpiper;
                }
                if (this.customerName == 'mpk') {
                    this.storePrefixfornewStore = this.objConstant.storePrefixMPK;
                }
                if (this.customerName == 'harvest') {
                    this.storePrefixfornewStore = this.objConstant.storePrefixHarvest;
                }
                if (this.customerName == 'mcdly') {
                    this.storePrefixfornewStore = this.objConstant.storePrefixMCDLY;
                }
                this.imgName = this.getCustomerImage(params.orgType);

                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                if (this.customerName != 'amazon') {
                    this.getsupplyingDepot();
                    this.getSupplyDepotCatData();
                }
                if (this.customerName == 'amazon') {
                    this.getSupplyDepotCatDataAmazon();
                }
                this.DOCategoriesTomorrowRow = [];
                this.getAllocationPriorityDD();
                let isoDate = new Date()
                let tomorrowISODate = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1, 0, 0, 0));
                this.tomorrowDate = this._cfunction.dateFormator(tomorrowISODate, 1);

                let perms = JSON.parse(sessionStorage.getItem('tempPermissionArray'));
                if (perms[this.customerName]['catalogueManagement'].indexOf('makeEffectiveImmediatelyFlag') != -1) {
                    this.makeEffectivePermission = true;
                } else {
                    this.makeEffectivePermission = false;
                }
                //this.makeEffectivePermission = true; //tmp

                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
                    if (res == 'sessionActive') {
                        this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                        this.getAvailability();
                        this.getStatus();
                        this.getPort();
                    }
                }, reason => {
                });
            });
        }




    }

   
     
    canDeactivate(): Promise<boolean> | boolean {
        if (!this.addstore.dirty) {
            return true;
        }
        this.confirmBoxResponse = this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');

        return this.confirmBoxResponse;
    }
    onSelectionDone(e: any) {
        this.effectivedate = e;
        let selectedDate = this._cfunction.dateFormator(e.toDateString(), 6);
        sessionStorage.setItem('currDate', selectedDate);
        if (this.effectivedate == null || this.effectivedate == undefined) {
            this.showDatepicker = false;
        } else {
            this.showDatepicker = true;
        }
    }

    // 

    getAvailability() {
        this.availabilityMaster = ["ACTIVE", "INACTIVE"];
    }
    getStatus() {
        this.statusmaster = ["rollout"];
    }
    getPort() {
        this.portMaster = ["St Helier", "St Peter Port"]
    }
    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }
    public selectedDate(value: any, idx: any) {

        var start = new Date(this.today.getTime() + 24 * 60 * 60 * 1000);
        this.supplyDepotCatData[idx].effectiveDate = value.start.format('DD/MM/YYYY');
        this.supplyDepotCatData[idx].effectiveDateFormated = value.start.format('DD/MM/YYYY HH:mm:ss');
    }

    showInfoErrorMsg(fieldName: any) {
        let msg;
        msg = fieldName + ' is required. ';
        return msg;
    }
    showPopup() {
        if (this.showDatepicker == true) {
            this.showDatepicker = false;
        } else {
            this.showDatepicker = true;
        }
    }


    showSupplyingDepotTable(sd: string) {
        this.selectedCategory = sd;
        let that = this;
        this.selectedCategorySupplyDepot = [];
        this.supplyingDepot.forEach(function (itm: any) {
            if (itm.supplyingDhlDepotName == sd) {
                that.selectedCategorySupplyDepot = itm.productCategoryDetails;
            }
        });
    }

    showCalendarTextBox(dt: any) {
        if (dt == "") {
            this.calDefaultDate = new Date();
            return true;
        } else {
            let isoDate = new Date()
            let todayISODate = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 0, 0, 0);
            let tomorrowISODate = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1);
            let orgDate = new Date(isoDate[2], isoDate[1] - 1, isoDate[0], 0, 0, 0);
            if (todayISODate.getTime() == orgDate.getTime()) {
                return false;
            } else if (orgDate.getTime() > todayISODate.getTime()) {
                return true;
            } else {
                return false;
            }
        }
    }
    getsupplyingDepot() {
        this._postsService.getsupplyingDepot(this.customerName, "suppDepotDetails", '@all').subscribe((supplyDepotDta: any) => {
            this.supplyingDepot = supplyDepotDta.maintainSupplyDepot;
            this.showSupplyingDepotTable(this.supplyingDepot[0].supplyingDhlDepotName);
            console.log("Supply depot"+JSON.stringify(this.supplyingDepot));
        }, (err: any) => {
        });

    }



    onChange(val: any) {
        if (val != '') {
            this.filterValue = val;
            this.getsupplyingDepot();
        } else {
            this.supplyDeportCategoryLen = -1;
            this.supplyDeportCategory = [];
        }
    }
    getSupplyDepotValuesOnChange(val: any) {
        this._postsService.getsupplyingDepot(this.customerName, "suppDepotDetails", val).subscribe((supplyDepotDta: any) => {
            let that = this;
            this.supplyDeportCategoryLen = 1;
            that.supplyDepotCatData = [];
            let isoDate: any;
            let tomorrowISODate: any = "";
            isoDate = new Date();
            let dt = (isoDate.getUTCDate() + 1) < 10 ? "0" + (isoDate.getUTCDate() + 1) : (isoDate.getUTCDate() + 1);
            let mm = (isoDate.getUTCMonth() + 1) < 10 ? "0" + (isoDate.getUTCMonth() + 1) : (isoDate.getUTCMonth() + 1);
            tomorrowISODate = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1, 0, 0, 0));
            let tomorrowDate = this._cfunction.dateFormator(tomorrowISODate, 1);


            supplyDepotDta.maintainSupplyDepot[0].productCategoryDetails.forEach(function (itm: any) {
                that.supplyDepotCatData.push({
                    'name': itm.categoryName,
                    "saleId": itm.rmsSellingStoreId,
                    "depotId": itm.rmsWarehouseId,
                    "stockId": itm.mainFrameStoreId,
                    "effectiveDate": tomorrowDate,
                });
            });
        });
    }

    resetTodefault(val: any) {
        if (val != '') {
            this.filterValue = val;
            this.getsupplyingDepot();
        } else {
            this.supplyDeportCategoryLen = -1;
            this.supplyDeportCategory = [];
        }
    }

    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else {
                        reject('sessionInactive')
                        sessionStorage.clear();
                        this._router.navigate(['']);
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        resolve('sessionActive');
                        this.terminateFlg = false;
                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
                        this.organisationListChange = changeOrgAvailable;
                        this.orgimgUrl = orgImgUrl;
                        this.orgApiName = orgApiNameTemp;
                        if (this.organisationList.length == 1) {
                            this.disabledCusChange = true;
                        }
                        let that = this;
                        let editStoreDetails = true;
                        let dashboardMenuPermission = false;
                        let userHavingPermission = false;
                        this.organisationList.forEach(function (organisation: any) {

                            if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                                organisation.permissionRoleList.forEach(function (permRoleList: any) {
                                    permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                        if (permRoleDtlList.permissionGroup == 'catalogueManagement') {
                                            userHavingPermission = true;
                                            permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                                switch (permList.permissionName) {
                                                    case "editStoreDetails":
                                                        editStoreDetails = false;
                                                        break;
                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                        that = null;
                        if (editStoreDetails) {
                            this._router.navigate(["store", customerName]);
                        }
                    }
                }
            });
        });
    }

    getAllocationPriorityDD() {
        this._postsService.getAllocationPriority(this.customerName, 'dhlAllocationPriority', '@all').subscribe((data: any) => {
            this.allocationPriority = data.priorityList;
        });
    }
    getSupplyDepotCatData() {
        this.supplyDepotCatData = [];
        let that = this;
        this.storeInfo.categories = [this.selectedCategorySupplyDepot];
        this.storeInfo.categories.forEach(function (itm: any) {
            that.supplyDepotCatData.push({
                'name': itm.name,
                "saleId": itm.saleId,
                "depotId": itm.depotId,
                "stockId": itm.stockId
            });
        });
    }

    getSupplyDepotCatDataAmazon() {
        this.supplyDepotCatData = [];
        this.supplyDepotCatData = this.objConstant.supplyingDepotforAmazon;
        let that = this;
        this.storeInfo.categories = [this.selectedCategorySupplyDepot];
        let saleId = this.storeInfo.storeId
        let isoDate = new Date();
        let dt = (isoDate.getUTCDate() + 1) < 10 ? "0" + (isoDate.getUTCDate() + 1) : (isoDate.getUTCDate() + 1);
        let mm = (isoDate.getUTCMonth() + 1) < 10 ? "0" + (isoDate.getUTCMonth() + 1) : (isoDate.getUTCMonth() + 1);
        let tomorrowISODate = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1, 0, 0, 0));
        let tomorrowDate = this._cfunction.dateFormator(tomorrowISODate, 1);
        this.supplyDepotCatData[0].effectiveDate = tomorrowDate;

        if (this.makeEffectivePermission) {
                    if (this.makeChangeToday) {
                    let isoDate = new Date();
                        let dt = (isoDate.getUTCDate() + 1) < 10 ? "0" + (isoDate.getUTCDate() + 1) : (isoDate.getUTCDate() + 1);
                        let mm = (isoDate.getUTCMonth() + 1) < 10 ? "0" + (isoDate.getUTCMonth() + 1) : (isoDate.getUTCMonth() + 1);
                        let today = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 0, 0, 0));
                        this.supplyDepotCatData[0].effectiveDate = today;
                 }
                }

    }
    getCurrentDOPerCategory() {
        let that = this;
        this.storeDtl.categories.forEach(function (itm: any, i: number) {
            itm.deliveryOpportunities.forEach(function (doRow: any, idx: number) {
                if (that.getCurrentTodayDateRecordDO(doRow.effectiveDate)) {
                    that.DOCategoriesCurrentRow[itm.name] = idx;
                }
            });
            if (typeof that.DOCategoriesCurrentRow[itm.name] === 'undefined') {
                that.DOCategoriesCurrentRow[itm.name] = that.getCurrentTodayDateNotExistRecordDO(itm.name, i);
            }
        });
    }
    getCurrentTodayDateNotExistRecordDO(cat: string, idx: number) {
        let isoDate = new Date();
        let todayISODateStart = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 0, 0, 0).getTime();
        let todayISODateEnd = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 23, 59, 59).getTime();
        let orgDate: number;
        let that = this;
        let dateDiff: number = null;
        let diff = 0;
        let rtnValue: number = null;
        this.storeDtl.categories[idx].deliveryOpportunities.forEach(function (doRow: any, idx1: number) {
            let temp = doRow.effectiveDate.split("/");
            orgDate = new Date(temp[2], temp[1] - 1, temp[0]).getTime();
            if (todayISODateEnd >= orgDate) {
                if (dateDiff == null) {
                    dateDiff = (todayISODateStart - orgDate);
                    rtnValue = idx1;
                } else if (dateDiff > (todayISODateStart - orgDate)) {
                    dateDiff = (todayISODateStart - orgDate);
                    rtnValue = idx1;
                }
            } else {
            }
        });
        return rtnValue;
    }

    getCurrentTodayDateRecordDO(dt: any) {
        let isoDate = new Date();
        let todayISODateEnd = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 23, 59, 59).getTime();
        let todayISODateStart = new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 0, 0, 0).getTime();
        let temp = dt.split("/");
        let orgDate = new Date(temp[2], temp[1] + 1, temp[0]).getTime();
        if (orgDate >= todayISODateStart && orgDate <= todayISODateEnd) {
            return true;
        } else {
            return false;
        }
    }
    getvalue(value: any) {
        this.supplyDepotCatData.value = value;
        this.opsValue = this.supplyDepotCatData.value

    }
    getvaluefordate(value: any) {
        this.supplyDepotCatData.effectiveDate = value
        if (this.supplyDepotCatData.effectiveDate == undefined) {
            let isoDate = new Date();
            let dt = (isoDate.getUTCDate() + 1) < 10 ? "0" + (isoDate.getUTCDate() + 1) : (isoDate.getUTCDate() + 1);
            let mm = (isoDate.getUTCMonth() + 1) < 10 ? "0" + (isoDate.getUTCMonth() + 1) : (isoDate.getUTCMonth() + 1);
            let tomorrowISODate = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate() + 1, 0, 0, 0));
            let tomorrowDate = this._cfunction.dateFormator(tomorrowISODate, 1);
            this.supplyDepotCatData[0].effectiveDate = tomorrowDate;
        }
    }
    getStoreId(value: any) {
        if (this.customerName != "amazon") {
            if (this.storeInfo.storeId.length < 4) {
                while (this.storeInfo.storeId.length < 4)
                    this.storeInfo.storeId = "0" + this.storeInfo.storeId;
                return this.storeInfo.storeId;
            }
        }
        else {
            if (this.storeInfo.storeId.length < 3) {
                while (this.storeInfo.storeId.length < 3)
                    this.storeInfo.storeId = "0" + this.storeInfo.storeId;
                return this.storeInfo.storeId;
            }
        }
    }
    addStoreInfo(value: any) {
        
       
        console.log(value);
        console.log("After Save Control is here");
        this.storeInfoList = [];
        this.Audit.who = sessionStorage.getItem('name');
        this.Audit.when = this._cfunction.isodate(new Date());
        this.storeInfo.createdDate = this._cfunction.isodate(new Date());
        this.storeInfo.storeIdPrefix = this.storePrefixfornewStore;
        this.storeInfo.createdBy = sessionStorage.getItem('name');
        this.storeInfo.updatedBy = sessionStorage.getItem('name');
        this.storeInfo.audit = this.Audit;

        this.storeInfo.storeDescription = this.storeInfo.storeId + ":" + this.storeInfo.tradingName + " " + this.storeInfo.storeName
        let that = this;
        this.storeInfo.customerName = this.customerName;
        this.storeInfo.categories = []

        if (this.customerName == "amazon") {
             
        //FOR CODOTUK
        this.transitInformationforPrime = new transitInformationForAmazon(this.rmsSellingLocationForPrimeNow,this.storeInfo.storeId)
        this.transitInformationforCODOTUK = new transitInformationForAmazon(this.rmsSellingLocationForCODOTUK,this.storeInfo.storeId)
        this.merchantforPrime  = new Merchants(this.merchantId,"Prime Now","AAAAA",this.marketplaceforPrimeNow,this.inventoryApplicableForPrimeNow,this.priceApplicableForPrimeNow,this.settlementReportForPrimeNow,this.transitInformationforPrime);
        this.merchantforCODOTUK  = new Merchants(this.merchantId,".CO.UK","AAAAA",this.marketplaceforCODOTUK,this.inventoryApplicableForCODOTUK,this.priceApplicableForCODOTUK,this.settlementReportForCODOTUK,this.transitInformationforCODOTUK);      
        this.storeInfo.merchants = [this.merchantforPrime,this.merchantforCODOTUK]
        this.storeInfo.minimumExpetedInventory = this.minimumExpetedInventory;
           console.log("this.merchants ==>> "+ JSON.stringify(this.storeInfo.merchants));
   
           console.log(" minimumExpetedInventory"+this.minimumExpetedInventory);
            
            this.storeInfo.supplyingDepotName = "Amazon";
            this.storeInfo.allocationPriority = "0";
          //  this.storeInfo.storeAlternateIds = {'merchantId': this.merchantId};
           
          
            //this.storeInfo.storeAlternateIds = {'merchantId': ''};
            this.effectiveDateAmazon = this.supplyDepotCatData[0].effectiveDate
            this.opsValue = this.supplyDepotCatData[0].value
            this.supplyDepotCatData.forEach((selectCategory: any) => {
                this.deliveryOpportunities = [];
                this.transitInformation = new TransitInformation("0", this.storeInfo.storeId, "0")

                if (this.makeEffectivePermission) {
                    if (this.makeChangeToday) {
                        let isoDate = new Date();
                        let dt = (isoDate.getUTCDate() + 1) < 10 ? "0" + (isoDate.getUTCDate() + 1) : (isoDate.getUTCDate() + 1);
                        let mm = (isoDate.getUTCMonth() + 1) < 10 ? "0" + (isoDate.getUTCMonth() + 1) : (isoDate.getUTCMonth() + 1);
                        let today = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 0, 0, 0));
                        selectCategory.effectiveDate = today;
                        console.log("today ==>"+today);
                        this.effectiveDateAmazon = selectCategory.effectiveDate;
                    }
                }
                if(this.storeInfo.tradingName == "AMAZON")
                {
               this.deliveryOpportunity = new DeliveryOpportunity(this.Audit.when, selectCategory.value, this.effectiveDateAmazon, this.transitInformation);
               this.deliveryOpportunities.push(this.deliveryOpportunity);
               this.category.deliveryOpportunities.forEach
               this.category = new Category(selectCategory.name, this.deliveryOpportunities)
               this.storeInfo.categories.push(this.category);
               this.deliveryOpportunity.value = this.opsValue;
                }
                console.log(this.deliveryOpportunities);
                console.log("STOREINFO ==>> "+ JSON.stringify(this.storeInfo));
            })

        }
        if (this.customerName != "amazon") {
             delete  this.storeInfo.merchants;
            this.supplyDepotCatData.forEach((selectCategory: any) => {
                this.deliveryOpportunities = [];
                this.transitInformation = new TransitInformation(selectCategory.depotId, selectCategory.saleId, selectCategory.stockId, )
                if (this.makeEffectivePermission) {
                    if (this.makeChangeToday) {
                        let isoDate = new Date();
                        let dt = (isoDate.getUTCDate() + 1) < 10 ? "0" + (isoDate.getUTCDate() + 1) : (isoDate.getUTCDate() + 1);
                        let mm = (isoDate.getUTCMonth() + 1) < 10 ? "0" + (isoDate.getUTCMonth() + 1) : (isoDate.getUTCMonth() + 1);
                        let today = this._cfunction.isodate(new Date(isoDate.getUTCFullYear(), isoDate.getUTCMonth(), isoDate.getUTCDate(), 0, 0, 0));
                        console.log("todaynot ==>"+today);
                        selectCategory.effectiveDate = today;
                    }
                }
                this.deliveryOpportunity = new DeliveryOpportunity(this.Audit.when, selectCategory.value, selectCategory.effectiveDate, this.transitInformation);
                this.deliveryOpportunities.push(this.deliveryOpportunity);
                this.category.deliveryOpportunities.forEach
                this.category = new Category(selectCategory.name, this.deliveryOpportunities)
                this.storeInfo.categories.push(this.category);
            })
        }
        if (this.makeEffectivePermission) {
            if (this.makeChangeToday) {

                this.storeInfo.applyImmediate = true;


            }
            /* else if(!this.makeChangeToday){*/
            else {

                this.storeInfo.applyImmediate = false;
            }
        } else {

            this.storeInfo.applyImmediate = false;
        }
        this.changeDateFormat2Org();
        this.storeInfoList.push(this.storeInfo);

        console.log("Store info here");
        console.log(this.storeInfo);

        this.storeDetails.stores = this.storeInfoList;
        let msgList: any
        let msgListForcommon: any
        msgListForcommon = Object.keys(this._cfunction.errorMsgList);
        msgList = Object.keys(this.errorMsgList);
        var regMID = new RegExp(/^[a-zA-Z0-9_ ]*$/);
        var regPN = new RegExp(/^[1-9]\d*$/);
        var regUK = new RegExp(/^[1-9]\d*$/);
        if (msgList.length == 0 && msgListForcommon == 0) {
            console.log("in here");
            let that = this;

            // if ((this.storeInfo.storeName == "" || this.storeInfo.storeId == null || this.storeInfo.availability == "" || this.storeInfo.tradingName == "" || this.storeInfo.allocationPriority == "" ||
            //     this.storeInfo.status == "" || this.storeInfo.addressPostcode == "" || this.storeInfo.supplyingDepotName == "" || this.deliveryOpportunity.value == undefined || this.deliveryOpportunity.value == "") && this.customerName != "amazon"
            // ) {
                if(this.customerName != "amazon"){
                this.popupMsgs = '';
                // this.popupMsg = "List of required filed(s) are missing : " + "<br/>";
                if (this.storeInfo.storeName == "") {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Header : Store Name" + "<br/>";
                }

                if (this.storeInfo.storeId == null) {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Header : Store Id" + "<br/>";
                }
                if (isNaN(this.storeInfo.storeId)) {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Header : Invalid Store Id" + "<br/>";
                }

                if (this.storeInfo.tradingName == "") {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Header : Trading Name" + "<br/>";
                }
                if (this.storeInfo.availability == "") {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Header : Availabilty" + "<br/>";
                }
                

                if (this.storeInfo.allocationPriority == "") {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Main Details : AllocationPriority" + "<br/>";
                }

                if (this.storeInfo.status == "") {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Main Details : Status" + "<br/>";
                }

                if (this.storeInfo.addressPostcode == "") {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Address : Address Postcode" + "<br/>";
                }
                if (this.storeInfo.supplyingDepotName == "" || this.storeInfo.supplyingDepotName == undefined) {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Supplying Depot : Supplying depot name" + "<br/>";
                   
                }
                if (this.deliveryOpportunity.value == undefined) {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Delivery Opportunities : Delivery day" + "<br/>";
                }  
                if ((this.customerName == "rontec" || this.customerName == "rontec-b") && this.storeInfo.contactEmail == "") {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "contact Email is required";                    
                   
                }
                 if (this.customerName == "sandpiper" && this.storeInfo.port == "") {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Port is required";                    
                    
                }
                 if (isNaN(this.storeInfo.storeId)) {

                    this.popupMsg = " Invalid Store Id";
                    let sessionModal = document.getElementById('messageModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('messageModal').style.display = "block";
                   
                }

                if(this.popupMsgs.length == 0)
                {
                    this.errorFlag = false;
                }  
                    
                if(this.errorFlag == true){
                    this.popupMsg = "List of required field(s) are missing : " + "<br/>";
                    this.popupMsg = this.popupMsg + this.popupMsgs;
                    let sessionModal = document.getElementById('messageModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('messageModal').style.display = "block";
                    
                 }
               
                
            } 
            
            console.log("ErrorFlag before post method== > "+this.errorFlag);          
             if (this.customerName == "amazon" && this.storeInfo.tradingName.length==0)
            {
               console.log("Error is here==> "+this.storeInfo.tradingName);
               console.log(msgList.length +"  <==> "+ msgListForcommon +" <<flag>> "+ this.errorFlag);
               this.popupMsgs = '';
               // this.popupMsg = "List of required field(s) are missing : " + "<br/>";
              
               if (this.storeInfo.storeName == "") {
                   this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Header : Store Name" + "<br/>";
               }

               if (this.storeInfo.storeId == null) {
                    this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Header : Store Id" + "<br/>";
               }
               if (isNaN(this.storeInfo.storeId)) {
                   this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Header : Invalid Store Id" + "<br/>";
               }

               if (this.storeInfo.tradingName == null || this.storeInfo.tradingName == undefined || this.storeInfo.tradingName == "") {
                   this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Header : Trading Name" + "<br/>";
               }
               if (this.storeInfo.availability == "") {
                this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Header : Availability" + "<br/>";
               }
                if (this.merchantId == null || !(regMID.test(this.merchantId))) {
                    this.errorFlag = true;
                 this.popupMsgs = this.popupMsgs + "Header : Merchant Id" + "<br/>";
                }

               
               /*if (isNaN(this.merchantId) && this.merchantId != null) {

                   this.popupMsg = this.popupMsg + "Header : Invalid Merchant Id" + "<br/>";
               }*/
               if (this.storeInfo.status == "") {
                   this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Main Details : Status" + "<br/>";
               }

               if (this.storeInfo.regionCode === '' ) {
                   this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Main Details : Region Code" + "<br/>";
               }  
              
               if ((this.storeInfo.supplyingDepotName == "" || this.storeInfo.supplyingDepotName == undefined) 
               && this.storeInfo.tradingName == "AMAZON") {
                this.errorFlag = true;
                this.popupMsgs = this.popupMsgs + "Supplying Depot : Supplying depot name" + "<br/>";
               
                }
                if ((this.deliveryOpportunity.value == undefined || this.deliveryOpportunity.value == "") 
                && this.storeInfo.tradingName == "AMAZON") {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Delivery Opportunities : Delivery day" + "<br/>";
                }

                if(!(regPN.test(this.rmsSellingLocationForPrimeNow)) && this.rmsSellingLocationForPrimeNow != null 
                && this.storeInfo.tradingName == "AMAZON STORE PICK")
                {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "MarketPlaces : Invalid RMS for Prime Now" + "<br/>";
                }
                if (this.rmsSellingLocationForPrimeNow == null && this.storeInfo.tradingName == "AMAZON STORE PICK") {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "MarketPlaces : RMS for Prime Now" + "<br/>";
                }
            
                if(!(regUK.test(this.rmsSellingLocationForCODOTUK)) && this.rmsSellingLocationForCODOTUK != null && this.storeInfo.tradingName == "AMAZON STORE PICK")
                {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "MarketPlaces : Invalid RMS for .CO.UK" + "<br/>";
                }

                if (this.rmsSellingLocationForCODOTUK == null && this.storeInfo.tradingName == "AMAZON STORE PICK") 
                {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "MarketPlaces : RMS for .CO.UK" + "<br/>";
                }   

                 if(this.popupMsgs.length == 0)
                {
                    this.errorFlag = false;
                }  
            if(this.errorFlag == true){
                this.popupMsg = "List of required field(s) are missing : " + "<br/>";
                this.popupMsg = this.popupMsg + this.popupMsgs;
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
                
             }
             
           }               
            if (this.customerName == "amazon" &&  this.storeInfo.tradingName == 'AMAZON')
            {
               console.log("Error is here==> "+this.storeInfo.tradingName);
               console.log(msgList.length +"  <==> "+ msgListForcommon +" <<flag>> "+ this.errorFlag);
               this.popupMsgs = '';
               // this.popupMsg = "List of required field(s) are missing : " + "<br/>";
              
               if (this.storeInfo.storeName == "") {
                   this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Header : Store Name" + "<br/>";
               }

               if (this.storeInfo.storeId == null) {
                    this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Header : Store Id" + "<br/>";
               }
               if (isNaN(this.storeInfo.storeId)) {
                   this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Header : Invalid Store Id" + "<br/>";
               }
              
               if (this.storeInfo.availability == "") {
                this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Header : Availability" + "<br/>";
               }
                if (this.merchantId == null || !(regMID.test(this.merchantId))) {
                    this.errorFlag = true;
                 this.popupMsgs = this.popupMsgs + "Header : Merchant Id" + "<br/>";
                }
               /*if (isNaN(this.merchantId) && this.merchantId != null) {

                   this.popupMsg = this.popupMsg + "Header : Invalid Merchant Id" + "<br/>";
               }*/
               if (this.storeInfo.status == "") {
                   this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Main Details : Status" + "<br/>";
               }

               if (this.storeInfo.regionCode === '' ) {
                   this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Main Details : Region Code" + "<br/>";
               }  

              
            if (this.storeInfo.supplyingDepotName == "" || this.storeInfo.supplyingDepotName == undefined && this.storeInfo.tradingName == 'AMAZON') {
                this.errorFlag = true;
                this.popupMsgs = this.popupMsgs + "Supplying Depot : Supplying depot name" + "<br/>";
               
                }
                if ((this.deliveryOpportunity.value == undefined || this.deliveryOpportunity.value == "") && this.storeInfo.tradingName == 'AMAZON') {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Delivery Opportunities : Delivery day" + "<br/>";
                }
                if(this.popupMsgs.length == 0)
                {
                    this.errorFlag = false;
                }  
                
            if(this.errorFlag == true){
                this.popupMsg = "List of required field(s) are missing : " + "<br/>";
                this.popupMsg = this.popupMsg + this.popupMsgs;
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
                
             }
             
           }  
          
            if (this.customerName == "amazon" && this.storeInfo.tradingName == "AMAZON STORE PICK")
            {
               console.log("Error is here==> "+this.storeInfo.tradingName);
               console.log(msgList.length +"  <==> "+ msgListForcommon +" <<flag>> "+ this.errorFlag);
               this.popupMsgs = '';
               // this.popupMsg = "List of required field(s) are missing : " + "<br/>";
              
               if (this.storeInfo.storeName == "") {
                   this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Header : Store Name" + "<br/>";
               }

               if (this.storeInfo.storeId == null) {
                    this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Header : Store Id" + "<br/>";
               }
               if (isNaN(this.storeInfo.storeId)) {
                   this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Header : Invalid Store Id" + "<br/>";
               }
               
               if (this.storeInfo.availability == "") {
                this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Header : Availability" + "<br/>";
               }
                if (this.merchantId == null || !(regMID.test(this.merchantId))) {
                    this.errorFlag = true;
                 this.popupMsgs = this.popupMsgs + "Header : Merchant Id" + "<br/>";
                }

                if (this.minimumExpetedInventory == "") {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Header : Minimum Expeted Inventory" + "<br/>";
                }
                if (!(regUK.test(this.minimumExpetedInventory)) && this.minimumExpetedInventory != "") {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "Header : Invalid Minimum Expeted Inventory" + "<br/>";
                }
               /*if (isNaN(this.merchantId) && this.merchantId != null) {

                   this.popupMsg = this.popupMsg + "Header : Invalid Merchant Id" + "<br/>";
               }*/
               if (this.storeInfo.status == "") {
                   this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Main Details : Status" + "<br/>";
               }

               if (this.storeInfo.regionCode === '' ) {
                   this.errorFlag = true;
                   this.popupMsgs = this.popupMsgs + "Main Details : Region Code" + "<br/>";
               } 

                if(!(regPN.test(this.rmsSellingLocationForPrimeNow)) && this.rmsSellingLocationForPrimeNow != null && this.storeInfo.tradingName == "AMAZON STORE PICK")
                {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "MarketPlaces : Invalid RMS for Prime Now" + "<br/>";
                }
                if ((this.rmsSellingLocationForPrimeNow == null || this.rmsSellingLocationForPrimeNow == undefined) && this.storeInfo.tradingName == "AMAZON STORE PICK") {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "MarketPlaces : RMS for Prime Now" + "<br/>";
                }
            
                if(!(regUK.test(this.rmsSellingLocationForCODOTUK)) && this.rmsSellingLocationForCODOTUK != null && this.storeInfo.tradingName == "AMAZON STORE PICK")
                {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "MarketPlaces : Invalid RMS for .CO.UK" + "<br/>";
                }

                if ((this.rmsSellingLocationForCODOTUK == null || this.rmsSellingLocationForCODOTUK == undefined) && this.storeInfo.tradingName == "AMAZON STORE PICK") 
                {
                    this.errorFlag = true;
                    this.popupMsgs = this.popupMsgs + "MarketPlaces : RMS for .CO.UK" + "<br/>";
                }  
                if(this.popupMsgs.length == 0)
                {
                    this.errorFlag = false;
                }  
            if(this.errorFlag == true){
                this.popupMsg = "List of required field(s) are missing : " + "<br/>";
                this.popupMsg = this.popupMsg + this.popupMsgs;
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
               
             }
             
           }  
          
                // if ((this.customerName == "rontec" || this.customerName == "rontec-b") && this.storeInfo.contactEmail == "") {
                //     this.popupMsg = "";
                //     this.popupMsg = "contact Email is required";
                //     let sessionModal = document.getElementById('messageModal');
                //     sessionModal.classList.remove('in');
                //     sessionModal.classList.add('out');
                //     document.getElementById('messageModal').style.display = "block";
                   
                // }
                //  if (this.customerName == "sandpiper" && this.storeInfo.port == "") {
                //     this.popupMsg = "";
                //     this.popupMsg = "Port is required";
                //     let sessionModal = document.getElementById('messageModal');
                //     sessionModal.classList.remove('in');
                //     sessionModal.classList.add('out');
                //     document.getElementById('messageModal').style.display = "block";
                    
                // }
                //  if (isNaN(this.storeInfo.storeId)) {

                //     this.popupMsg = " Invalid Store Id";
                //     let sessionModal = document.getElementById('messageModal');
                //     sessionModal.classList.remove('in');
                //     sessionModal.classList.add('out');
                //     document.getElementById('messageModal').style.display = "block";
                   
                // }
               
                if(this.errorFlag == false) {
                    this.storeInfo.status = "rollout";
                    this.storeInfo.addressName = this.storeInfo.storeName;
                    console.log(this.storeDetails); 
                    
                    this._postsService.addStoreInfo(this.orgApiName, this.storeDetails, this.storeID).subscribe(data => {
                        this.popupMsg = "";
                        this.makeChangeToday = false;
                        this.popupMsg = "Store added successfully.";
                        this.savedisable = true;

                       
                        $('.store-details-btn').addClass('disable');

                        let sessionModal = document.getElementById('messageModal');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('messageModal').style.display = "block";
                    }, error => {
                        var errorobject = JSON.parse(error._body)
                        console.log("Error Object  " + errorobject);
                        if (errorobject.errorCode == '400.001') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.002') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.003') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.004') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.005') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.006') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.007') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.008') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.009') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.010') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.011') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.012') {
                            this.errorMsgString = errorobject.errorMessage
                        }

                        else if (errorobject.errorCode == '400.013' && this.customerName != "amazon") {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.014') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.015') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.016') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.017') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.018') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.019') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.020') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.021') {
                            this.errorMsgString = errorobject.errorMessage
                        }

                        else if (errorobject.errorCode == '400.035') {
                            this.errorMsgString = errorobject.errorMessage
                        }
                        else if (errorobject.errorCode == '400.036') {
                            this.errorMsgString = errorobject.errorMessage
                        }

                        let sessionModal = document.getElementById('errormodal');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('errormodal').style.display = "block";
                        this.showLoader = false;
                    }

                    );
                }
            }        
        else {
            this.popupMsg = ""
            let that = this;
            let DOErrorFlag = false;
            let DOErrorFlagRequired = false;

            msgList.forEach(function (key: any) {
                if ((that.errorMsgList[key] == that.validationMsgDO_YN && DOErrorFlag) || (that.errorMsgList[key] == that.validationMsgDO_YN_Required && DOErrorFlagRequired)) {

                } else {

                    that.popupMsg += that.errorMsgList[key] + "<br/>";
                    if (that.errorMsgList[key] == that.validationMsgDO_YN) {
                        DOErrorFlag = true;
                    }

                    if (that.errorMsgList[key] == that.validationMsgDO_YN_Required) {
                        DOErrorFlagRequired = true;

                    }
                }
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
            });

        }
    }
    changeDateFormat2Org() {
        let that = this;
        this.storeInfo.categories.forEach(function (itm1: any) {
            itm1.deliveryOpportunities.forEach(function (itm: any) {
                if (itm.effectiveDate != undefined) {
                    itm.effectiveDate = that._cfunction.isodate(itm.effectiveDate);
                }
            });
        });

    }
    closePopup(modalBoxId = "") {
        if (modalBoxId != "") {
            let sessionModal = document.getElementById(modalBoxId);
            sessionModal.classList.remove('out');
            sessionModal.classList.add('in');
            sessionModal.style.display = 'none';
        } else {
            let sessionModal = document.getElementById('popup_SessionExpired');
            sessionModal.classList.remove('out');
            sessionModal.classList.add('in');
            sessionModal.style.display = 'none';
            let dt = this._cfunction.getUserSession();
            this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            }, (error: any) => {
            });
            sessionStorage.clear();
            this._router.navigate(['']);
        }

    }

    keyPress(event: any) {
        this._globalFunc.alphaNumeric(event);
    }

    keyPressWithSpace(event: any) {
        this._globalFunc.alphaNumericWithSpace(event);
    }

    keyPressNumericWithDecimal(event: any) {
        this._globalFunc.numericWithDecimal(event);
    }

    keyPressNumericWithDecimalRestrict(event: any, val: any, elementId: any) {
        val = (<HTMLInputElement>document.getElementById(elementId)).value;
        if (parseFloat(val) == val) {

            var valArr = val.split('.');
            if (valArr.length > 2 || (typeof valArr[1] != 'undefined' && valArr[1].length > 2) || valArr[0] == "") {
                val = val.substr(0, val.length - 1);
                val = parseFloat(val).toFixed(2);
                (<HTMLInputElement>document.getElementById(elementId)).value = val;
            } else {
                if (parseFloat(val) != val) {
                    val = val.substr(0, val.length - 1);
                    (<HTMLInputElement>document.getElementById(elementId)).value = val;
                }
            }
        } else {
            (<HTMLInputElement>document.getElementById(elementId)).value = "";
        }
    }

    keepMeonSamePage() {
        let brandType = sessionStorage.getItem('previousBrandType');
        let sessionModal = document.getElementById('unsaveModalPageLevel');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('unsaveModalPageLevel').style.display = 'none';
    }

    navigatTopage() {
        let sessionModal = document.getElementById('unsaveModalPageLevel');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('unsaveModalPageLevel').style.display = 'none';
        this.showLoader = false;
    }
    validateInputText(txt2Validate: any, validationId: any, errorId: string, fieldId: string, fieldName: string, isRequired: boolean = true) {
        console.log("heretest");
        if (validationId != '0') {
            if (txt2Validate.target.value != "") {
                let pattern;
                switch (validationId) {
                    case 0:
                        pattern = /(.*?)(\s+)([0-9]+\.[0-9]{2})/; //a-z A-Z 0-9 / & () . ! : '
                        break;
                    case 1:
                        pattern = /^[a-zA-Z0-9]+[a-zA-Z0-9\/&().!:' ]+$/; //a-z A-Z 0-9 / & () . ! : '
                        break;
                    case 2:
                        pattern = /^[a-zA-Z0-9]+[a-zA-Z0-9\/&().!: ]+$/; //a-z A-Z 0-9 / & () . ! :
                        break;
                    case 3:
                        pattern = /^[a-zA-Z0-9]+[a-zA-Z0-9 ]+$/; //a-z A-Z 0-9
                        break;
                    case 4:
                        pattern = /^[a-zA-Z]+[a-zA-Z()' ]+$/; //a-z A-Z '()
                        break;
                    case 5:
                        pattern = /^[a-zA-Z]+$/; //a-z A-Z
                        break;
                    case 6:
                        pattern = /^[a-zA-Z0-9]+[a-zA-Z 0-9]+$/; //a-z A-Z 0-9
                        break;
                    case 7: // For email
                        pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; //For email
                        break;
                    case 8: // For Numeric with space and dash
                        pattern = /^[0-9 -]+$/; //For Numeric only
                        break;
                    case 9: // For Numeric
                        pattern = /^[0-9]*$/; //For Numeric only
                        break;
                    case 10: //for allocation priority code
                        pattern = /^\b[P][0-9]{1,}\b$/;
                        break;
                    case 11: // For Yand N only and  7char
                        pattern = /^[YN]{7}$/;
                        break;
                    case 12: // For Yand N only and  7char

                        break;
                    case 13:
                        pattern = /^[a-zA-Z\/]+$/; //a-z A-Z
                    break;
                }
                if (validationId == 12) {
                    if (!isRequired) {
                        document.getElementById(errorId).innerHTML = fieldName + " is optional.";
                    } else {
                        document.getElementById(errorId).innerHTML = fieldName + " is required.";
                    }
                    document.getElementById(errorId).classList.remove('red');
                    document.getElementById(fieldId).style.border = "0";
                    delete this.errorMsgList[errorId];
                    if (--this.errorCount < 0) {
                        this.errorCount = 0;
                    }
                }
                else if (pattern.test(txt2Validate.target.value)) {
                    if (!isRequired) {
                        document.getElementById(errorId).innerHTML = fieldName + " is optional.";
                    } else {
                        document.getElementById(errorId).innerHTML = fieldName + " is required.";
                    }
                    document.getElementById(errorId).classList.remove('red');
                    document.getElementById(fieldId).style.border = "0";
                    delete this.errorMsgList[errorId];
                    if (--this.errorCount < 0) {
                        this.errorCount = 0;
                    }
                } else {
                    if (validationId == 11) {
                        document.getElementById(fieldId).style.border = "1px solid red";
                        //document.getElementById(errorId).innerHTML = this.validationMsgDO_YN;
                        //this.errorMsgList[errorId] = this.validationMsgDO_YN;
                    } else {
                        document.getElementById(fieldId).style.border = "1px solid red";
                        document.getElementById(errorId).innerHTML = "Invalid " + fieldName;
                        this.errorMsgList[errorId] = "Invalid " + fieldName;
                    }
                    document.getElementById(errorId).classList.add('red');
                    ++this.errorCount;
                }
            }
            else {
                if (isRequired) {
                    document.getElementById(fieldId).style.border = "1px solid red";
                    document.getElementById(errorId).innerHTML = fieldName + " is required. ";
                  //  this.errorMsgList[errorId] = fieldName + " is required.  test 5";
                    document.getElementById(errorId).classList.add('red');
                } else {
                    document.getElementById(fieldId).style.border = "0";
                    document.getElementById(errorId).innerHTML = fieldName + " is optional.";
                    document.getElementById(errorId).classList.remove('red');
                    delete this.errorMsgList[errorId];
                }
            }
        } else {
            if (isRequired) {
                document.getElementById(fieldId).style.border = "1px solid red";
                document.getElementById(errorId).innerHTML = fieldName + " is required. ";
                this.errorMsgList[errorId] = fieldName + " is required. ";
                document.getElementById(errorId).classList.add('red');
            } else {
                document.getElementById(fieldId).style.border = "0";
                document.getElementById(errorId).innerHTML = fieldName + " is optional.";
                document.getElementById(errorId).classList.remove('red');
                delete this.errorMsgList[errorId];
            }
        }
    }
    onChange4UnsavedChanges(fieldName: string) {
        sessionStorage.setItem('unsavedChanges', 'true');
       
        
    }

    showHideMarketPlace(objs:any){       
        if(this.storeInfo.tradingName == "AMAZON STORE PICK" && this.isAmazonCatelogType)
        {
            this.tradingNameforPick = true;   
            console.log(this.tradingNameforPick);        
                        
        }
        if(this.storeInfo.tradingName != "AMAZON STORE PICK" && this.isAmazonCatelogType)
        {           
            this.tradingNameforPick = false;  
            console.log(this.tradingNameforPick);        
        }
        if(document.getElementById("Marketplaces")){
            document.getElementById("Marketplaces").classList.remove('active');
            document.getElementById("maindetailshead").classList.add('active');
            document.getElementById("maindetails").classList.add('active'); 
        }
        if(document.getElementById("SupplyingDepot")){
            document.getElementById("SupplyingDepot").classList.remove('active');
            document.getElementById("maindetailshead").classList.add('active');
            document.getElementById("maindetails").classList.add('active'); 
        }
        if(document.getElementById("DELIVERYOPPORTUNITIES")){
            document.getElementById("DELIVERYOPPORTUNITIES").classList.remove('active');
            document.getElementById("maindetailshead").classList.add('active');
            document.getElementById("maindetails").classList.add('active'); 
        }
        if(document.getElementById("ADDRESSDIV")){
            document.getElementById("ADDRESSDIV").classList.remove('active');
            document.getElementById("addresshead").classList.remove('active');
            document.getElementById("maindetailshead").classList.add('active');
            document.getElementById("maindetails").classList.add('active'); 
        }
    }
       
       
    


}