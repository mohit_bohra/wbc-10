import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealfileuploadComponent } from './deal.component';

// route Configuration
const dealfileuploadRoutes: Routes = [
  { path: '', component: DealfileuploadComponent }
];


@NgModule({
  imports: [RouterModule.forChild(dealfileuploadRoutes)],
  exports: [RouterModule]
})
export class DealFileComponentRoutes { }
