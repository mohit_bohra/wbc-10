import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DealfileuploadComponent } from './deal.component';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { FormsModule } from '@angular/forms';
import { CatalogueManagePendingHeaderModule } from '../../cataloguemanagement/cataloguemanagependingheader/cataloguemanagependingheader.module';
import { Ng2FileInputModule } from '../../ng2-file-input/dist/ng2-file-input';
import { DealFileComponentRoutes } from './deal.routes';

@NgModule({
  imports: [
    DealFileComponentRoutes,
    CommonModule,
    FormsModule,
    InfiniteScrollModule,
    Ng2FileInputModule.forRoot(),
    CatalogueManagePendingHeaderModule
  ],
  exports: [],
  declarations: [DealfileuploadComponent],
  providers: [Services, GlobalComponent]
})
export class DealfileuploadModule {
  constructor(){
  }
 }
