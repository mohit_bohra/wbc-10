import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogueComponent } from './catalogue.component';

// route Configuration
const catalogueRoutes: Routes = [
  { path: '', component: CatalogueComponent },
  { path: 'add', loadChildren: './add/addproduct.module#AddProductModule' },
  { path: 'upload', loadChildren: './upload/catalogueupload.module#CatalogueuploadModule' },
  { path: 'export', loadChildren: './export/catalogueexport.module#CatalogueexportModule' },
  { path: 'uploadrange', loadChildren: './uploadrange/uploadrange.module#UploadrangeModule' },
  { path: 'storebulkupload', loadChildren: './storebulkupload/storebulkupload.module#StoreBulkUploadModule' },
  { path: ':catalogueType/:catalogueId', loadChildren: './view/productdetail.module#ProductDetailModule' },
  { path: 'addstore', loadChildren: './addstore/addstore.module#AddstoreComponentModule'  } ,
  { path: 'uploadDeal', loadChildren: './uploadDeal/uploadDeal.module#UploadDealModule' },
  { path: 'deal', loadChildren: './deal/deal.module#DealfileuploadModule' },

];


@NgModule({
  imports: [RouterModule.forChild(catalogueRoutes)],
  exports: [RouterModule]
})
export class CatalogueComponentRoutes { }

