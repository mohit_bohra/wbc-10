import { NgModule } from '@angular/core';
import { CatalogueComponentRoutes } from './catalogue.routes';
import { CatalogueComponent } from './catalogue.component';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../global/global.component';
import { Services } from '../../services/app.services';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from "ngx-tooltip";
import { NavigationModule } from '../navigationFulfilment/navigation.module';
import { commonfunction } from '../../services/commonfunction.services';

@NgModule({
  imports: [
    CatalogueComponentRoutes,
    CommonModule,
    FormsModule,
    InfiniteScrollModule,
    TooltipModule,
    NavigationModule


  ],
  exports: [],
  declarations: [CatalogueComponent],
  providers: [Services, GlobalComponent,commonfunction]
})
export class CatalogueModule { }
