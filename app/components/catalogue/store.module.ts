import { NgModule } from '@angular/core';
import { StoreComponentRoutes } from './store.routes';
import { StoreComponent } from './store.component';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../global/global.component';
import { Services } from '../../services/app.services';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from "ngx-tooltip";
import { NavigationModule } from '../navigationFulfilment/navigation.module';
import { commonfunction } from '../../services/commonfunction.services';


@NgModule({
  imports: [
    StoreComponentRoutes,
    CommonModule,
    FormsModule,
    InfiniteScrollModule,
    TooltipModule,
    NavigationModule
  ],
  exports: [],
  declarations: [StoreComponent],
  providers: [Services, GlobalComponent,commonfunction]
})
export class StoreModule { }
