import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogueexportComponent } from './catalogueexport.component';

// route Configuration
const catalogueRoutes: Routes = [
  { path: '', component: CatalogueexportComponent },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(catalogueRoutes)],
  exports: [RouterModule]
})
export class CatalogueexportComponentRoutes { }
