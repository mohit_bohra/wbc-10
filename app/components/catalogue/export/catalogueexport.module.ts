import { NgModule } from '@angular/core';
import { CatalogueexportComponentRoutes } from './catalogueexport.routes';
import { CatalogueexportComponent } from './catalogueexport.component';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { FormsModule } from '@angular/forms';
//import { NavigationModule } from '../../navigation/navigation.module';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';
import { commonfunction } from '../../../services/commonfunction.services';

@NgModule({
  imports: [
    CatalogueexportComponentRoutes,
    CommonModule,
    FormsModule,
    InfiniteScrollModule,
    NavigationModule


  ],
  exports: [],
  declarations: [CatalogueexportComponent],
  providers: [Services, GlobalComponent,commonfunction]
})
export class CatalogueexportModule { }
