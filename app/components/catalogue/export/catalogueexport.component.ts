import { Component } from '@angular/core';
import { Services } from '../../../services/app.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { commonfunction } from '../../../services/commonfunction.services';

@Component({
    selector: 'catalogueexport',
    templateUrl: 'catalogueexport.component.html',
})
export class CatalogueexportComponent {
    public catalgoueDate: Date = new Date();
    objConstant = new Constant();
    errorMsgString: string;
    showErrorMsg: boolean;
    whoImgUrl: any;
    catalogueCode: any;
    catalogueCodeSelected: any;
    catalogueList: any;
    catalogueListLength: any;
    catalogueUpdatedDate: any;
    catalogueno: string;
    showLoader: boolean;
    hideOrganisation: boolean = true;
    catalogueResult: boolean = true;
    printHeader: boolean = true;
    pageState: number = 20;
    customerName: string;
    customerDispayName: string;
    style: string;
    organisationList: any;
    organisationListChange: any;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disabledOrder: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    private _sub: any;
    disabledExportCatalogue: boolean;
    disabledUploadCatalogue: boolean;
    disabledDashboard: boolean = false;
    succMSg: string;

    //for catalogue Enquiry redevelopment
    catalogueTypes: any;
    catalogueType: string;
    cataloguemasterListLength: any;
    cataloguedetailListLength: any;
    catalogueDetailList: any;
    strInfoNotAvail: string;
    start: number;
    limit: number;
    count: number;
    hitNext: boolean;
    catalogueFilename: string;
    custPermission: any[];

    public todayDate: Date = new Date();

    homeUrl: any;
    imgName: string = "";
    filterType:string='';
   constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, public _cfunction: commonfunction) {
        // img urls
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
    }

    // function works on page load
    ngOnInit() {
        this._globalFunc.logger("Loading catalogue export screen.");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
            this._globalFunc.logger("catalogue export user session not found");
        } else {
            this.showLoader = false;
            this.catalogueno = '';
            this.showErrorMsg = false;
            // user permission
            this.disabledCusChange = false;
            this.disabledPermission = false;
            this.disabledReports = false;
            this.disabledCatalogue = false;
            this.disabledOrder = false;
            this.disabledOrderRaise = false;
            this.disabledCatalogueRaise = false;
            this.disabledManageCatalogue = false;
            this.disabledExportCatalogue = false;
            this.disabledUploadCatalogue = false;
            this.strInfoNotAvail = "-";
            this.catalogueType = "";
            this.succMSg = this.strInfoNotAvail;

            // window scroll function
            this._globalFunc.autoscroll();

            // set header basket data
            this.pageState = 20;
            this.start = 0;
            this.limit = this.objConstant.SCROLL_RECORD_LIMIT;
            this.count = 0;
            this.hitNext = false;
            sessionStorage.setItem('pageState', '20');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;

                this.imgName = this.getCustomerImage(params.orgType);
                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
                
                // let reports;
                // let orders;
                // let catalogues;
                // let cataloguesRaise;
                // let uploadCatalogue;
                // let exportCatalogue;
                // let accessMgmt;
                // let redirct;
                // let changeOrgAvailable = new Array;
                // let catManagement;
                // let dashboardAccess;
                // // list of org assigned to user
                // this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));

                // this.organisationList.forEach(function (item: any) {
                //     if (item.orgType !== cusType) {
                //         changeOrgAvailable.push(item);
                //     }
                // });
                // this.organisationListChange = changeOrgAvailable;
                // // this.disabledCusChange = false;
                // if (this.organisationList.length == 1) {
                //     this.disabledCusChange = true;
                // }

                // document.getElementById('orderArrowSpan').style.display = 'block';
                // document.getElementById('orderMob').style.display = 'block';
                // document.getElementById('orderRaiseMob').style.display = 'none';

                // document.getElementById('catalogueMob').style.display = 'block';
                // document.getElementById('catalogueArrowSpan').style.display = 'none';
                // document.getElementById('catalogueRaiseMob').style.display = 'none';
                // document.getElementById('reportsMob').style.display = 'block';
                // document.getElementById('permissonsMob').style.display = 'block';

                // if (document.getElementById('orderArrow').classList.contains('glyphicon-menu-up') === true) {
                //     let sublinks = document.getElementById('orderArrow');
                //     sublinks.classList.remove('glyphicon-menu-up');
                //     sublinks.classList.add('glyphicon-menu-down');
                //     document.getElementById('orderRaiseMob').style.display = 'none';
                // }

                // if (document.getElementById('catalogueArrow').classList.contains('glyphicon-menu-up') === true) {
                //     let sublinks = document.getElementById('catalogueArrow');
                //     sublinks.classList.remove('glyphicon-menu-up');
                //     sublinks.classList.add('glyphicon-menu-down');
                //     document.getElementById('catalogueRaiseMob').style.display = 'none';
                // }

                let custPerm: any = [];
                let that = this;
                // this.organisationList.forEach(function (item: any) {
                //     if (item.orgType === cusType) {
                //         // logic for permission set
                //         orgImgUrl = item.imgUrl;
                //         orgApiNameTemp = item.orgName;

                //         if ((item.permissions.menuPermission.orders).toLowerCase() == 'none') {
                //             orders = true;
                //             document.getElementById('orderMob').style.display = 'none';
                //             document.getElementById('orderRaiseMob').style.display = 'none';
                //         }

                //         if ((item.permissions.menuPermission.raiseNewOrder).toLowerCase() == 'none') {
                //             document.getElementById('orderArrowSpan').style.display = 'none';
                //             document.getElementById('orderRaiseMob').style.display = 'none';
                //         }

                //         if ((item.permissions.menuPermission.catalogueManagement).toLowerCase() == 'none') {
                //             catManagement = true;
                //         } else {
                //             catManagement = false;
                //         }


                //         if ((item.permissions.menuPermission.catalogueEnquiry).toLowerCase() == 'none') {
                //             catalogues = true;
                //             document.getElementById('catalogueMob').style.display = 'none';
                //             document.getElementById('catalogueRaiseMob').style.display = 'none';
                //             redirct = 1;
                //             if (sessionStorage.getItem('page_redirect') == '0') {
                //                 sessionStorage.removeItem('page_redirect');
                //             } else {
                //                 sessionStorage.setItem('page_redirect', '1');
                //             }
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.catalogAdminPermission.addNewProducts).toLowerCase() == 'none') {
                //             cataloguesRaise = true;
                //             document.getElementById('catalogueArrowSpan').style.display = 'none';
                //             document.getElementById('catalogueRaiseMob').style.display = 'none';
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }
                //         console.log(item.permissions.catalogAdminPermission.uploadCatalogue);
                //         if ((item.permissions.catalogAdminPermission.uploadCatalogue).toLowerCase() == 'none') {
                //             uploadCatalogue = true;
                //         } else {
                //             uploadCatalogue = false;
                //         }

                //         // sitem.permissions.catalogAdminPermission.exportCatalogue = 'W';
                //         if ((item.permissions.catalogAdminPermission.exportCatalogue).toLowerCase() == 'none') {
                //             exportCatalogue = true;
                //         } else {
                //             exportCatalogue = false;
                //         }

                //         if ((item.permissions.menuPermission.reports).toLowerCase() == 'none') {
                //             reports = true;
                //             document.getElementById('reportsMob').style.display = 'none';
                //         }

                //         if ((item.permissions.menuPermission.accessMgmt).toLowerCase() == 'none') {
                //             accessMgmt = true;
                //             document.getElementById('permissonsMob').style.display = 'none';
                //         }

                //         if ((item.permissions.menuPermission.dashboards).toLowerCase() == 'none') {
                //             dashboardAccess = true;
                //         //    document.getElementById('permissonsMob').style.display = 'none';
                //         }
                //     }

                //     /* Check atleast permission should exist for customer otherwise hide customer */
                //     custPerm[item.orgType] = that._globalFunc.checkPermissionAvailableForCustomer(item.permissions.menuPermission);
                //     /* Check atleast permission should exist for customer otherwise hide customer - END */
                // });
                // that = null;
                // if (redirct == 1) {
                //     redirct = 0;
                //     this.redirect('/reports', this.customerName);
                // }

                this.custPermission = custPerm;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = this.customerName;
                // this.disabledReports = reports;
                // this.disabledOrder = orders;
                // this.disabledOrderRaise = orders;
                // this.disabledCatalogue = catalogues;
                // this.disabledCatalogueRaise = cataloguesRaise;
                // this.disabledPermission = accessMgmt;
                // this.disabledManageCatalogue = catManagement;
                // this.disabledUploadCatalogue = uploadCatalogue;
                // this.disabledExportCatalogue = exportCatalogue;
                // this.disabledDashboard = dashboardAccess;
                //  this.getAllCatalogues();
                this.getAllMasterData();
            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);

            // logic for resize pop up 
            window.onresize = () => {
                if (screen == "desktop") {
                    if (window.innerWidth < this._globalFunc.stdWidth) {
                        // this.catalogueResult = false;
                    } else {
                        //  this.catalogueResult = true;
                    }
                } else {
                    if (window.innerWidth > this._globalFunc.stdWidth) {
                        //   this.catalogueResult = true;
                        document.getElementById("resultModalClose").click();
                    } else {
                        //  this.catalogueResult = false;
                    }
                }

                // get device to populate some part of ui
                screen = this._globalFunc.getDevice(window.innerWidth);
            };
            window.onscroll = () => {
                this.scrollFunction();
            };
        }
    }

    onScrolltop() {
        this._globalFunc.autoscroll();
    }

    scrollFunction() {
        if (document.body.scrollTop > 800 || document.documentElement.scrollTop > 800) {
            document.getElementById("topBtn").style.display = "block";
        } else {
            document.getElementById("topBtn").style.display = "none";
        }
    }

    resetOnClose() {
        this.hitNext = false;
    }

    /* onScrollDown(catalogueCode: any, catalogueType: any) {
 
         this.start = this.start + this.limit;
 
         if (this.count == this.limit && this.hitNext == true) {
 
             this.showLoader = true;
 
             this._postsService.getCatalogueItem(this.orgApiName, catalogueCode, catalogueType, this.start, this.limit).subscribe(data => {
 
                 this.showLoader = false;
 
                 this.count = data.paginationMetaData.count;
                 this.catalogueDetailList = this.catalogueDetailList.concat(data.listOfCatalogFiles);
 
 
             }
                 , err => {
                     this.catalogueno = '';
                     this.showLoader = false;
                     this.cataloguedetailListLength = 0;
                     this._router.navigate(['/error404']);
                 }
             );
 
         }
 
     }*/


    // navigate function
    redirect(page: any, orgType: any) {
        this._globalFunc.logger("redirect to "+page);
        // console.log(page);
        // let url;
        if (page == 'addproduct') {
            //url = this.homeUrl + 'catalogue/' + encodeURIComponent(orgType) + '/add';
            this._router.navigate(['catalogue', encodeURIComponent(orgType),'add']);
            //this._router.navigate(url);
        } else if (page == 'catalogueexport') {
            //url = this.homeUrl + 'catalogue/' + encodeURIComponent(orgType) + '/export';
            this._router.navigate(['catalogue', encodeURIComponent(orgType),'export']);
        } else if (page == 'catalogueupload') {
            //url = this.homeUrl + 'catalogue/' + encodeURIComponent(orgType) + '/upload';
            this._router.navigate(['catalogue', encodeURIComponent(orgType),'upload']);
        } else {
            this._router.navigate([page, orgType]);
        }
        //this._router.navigate([page, orgType]);

    }
    // change organisation
    changeOrgType(page: any, orgType: any, page1:any) {
        console.log("Page : "+page);
        console.log(orgType);
        console.log(page1);
        this._globalFunc.logger("Changing Client");
        // reset field on change of organisation 
        this.catalogueno = '';
        this.catalogueList = [];
        this.catalogueListLength = 0;
        this.catalogueResult = true;
        this.catalogueType = '';
        this.catalogueCode = '';
        this.hitNext = false;
        this.start = 0;
        this.count = 0;
        this.toggleOrganisation();
        this._router.navigate(this._globalFunc.redirectToComponent('catalogueExport', orgType));
        //this._router.navigate([page, orgType,page1]);
    }

    // download functionality
    exportData() {
        this._globalFunc.logger("Export to excel.");
        return this._globalFunc.exportData('exportable',this.customerName+'catalogueExportData');
    }

    printCatalogue() {
        this._globalFunc.logger("Printing");
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        var printContentsNav = document.getElementById('printNavCatalogue').innerHTML;
        var printContents = document.getElementById('exportable').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContentsNav + printContents + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContentsNav + printContents + '</body></html>');
            popup.document.close();
        }
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
        this._globalFunc.logger("Unloading catalogue export screen.");
        this._globalFunc.sentLoggerBatchToServer();
    }

    // change organisation div show and hide
    toggleOrganisation() {
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
            border.classList.remove('searchborder');
        }
        else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
            border.classList.add('searchborder');
        }
    }

    // close operation of pop up
    closeOperation() {
        this.catalogueList = '';
    }

    /*// key press event to disable special charater
    keyPress(event: any) {
        this._globalFunc.alphaNumericWithSpace(event);
    }*/



    // fileDownload(filepath: any) {
    //     window.open(filepath);
    // }

    fileDownloadUsingPresignedURL(filename: string){
        this._globalFunc.logger("Downloading files using presigned url");
        if(sessionStorage.getItem('orgType') === 'amazon' && this.catalogueType === 'Core'){
            this.catalogueType = 'main';
        }

        this.catalogueType = this.catalogueType.toLowerCase();

        this._postsService.fileReportDownloadUsingPresignedURL(this.orgApiName, filename,'catalogueExport', 'download',this.catalogueType).subscribe(data=>{
            console.log(data.preSignedS3Url);
            
            window.open(data.preSignedS3Url);
        }, error => {
            this._globalFunc.logger("presigned url service error - "+filename+" not exist");
            this.succMSg = filename+" doesnot exist.";
            let sessionModal = document.getElementById('addConfirmationModal');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
        });
    }

    // get all catalogue detail
    getlistofCatalogeFiles(value: any) {
        let str = value.catalogueType;
        let searchTerm = '';
        if (str.search(':') != -1) {
            // console.log("adf");
            var strArr = str.split(':');
            searchTerm = strArr[1];
        } else {
            searchTerm = value.catalogueType;
        }

        if(sessionStorage.getItem('orgType') === 'amazon' && searchTerm === 'Core'){
            searchTerm = 'main';
        }

        searchTerm = searchTerm.toLowerCase();

        this.showLoader = true;
        this.start = 0;
        this.count = 0;
        // console.log(value);
        this._globalFunc.logger("Exported catalog service");
        this._postsService.getlistofCatalogeFiles(this.orgApiName, value.catalogueCode, searchTerm).subscribe(data => {
            this.showLoader = false;
            this.catalogueCodeSelected = searchTerm;
            this.catalogueDetailList = [];
            console.log(data.listOfCatalogFiles);
            // data.listOfCatalogFiles = ' [ { "filename": "amazon-default-20102017200003.csv", "fileDate": "20/10/2017 20:00:03", "downloadFilePath": "https://mdev.xxwmm.wholesaleweb.catalogue.eeeeeeeeee.s3-eu-west-1.amazonaws.com/wholesaleinboundcatalogue/pending/amazon-default-20102017200003.csv" },{ "filename": "amazon-default-20102017200003.csv", "fileDate": "20/10/2017 20:00:03", "downloadFilePath": "https://mdev.xxwmm.wholesaleweb.catalogue.eeeeeeeeee.s3-eu-west-1.amazonaws.com/wholesaleinboundcatalogue/pending/amazon-default-20102017200003.csv" },{ "filename": "amazon-default-20102017200003.csv", "fileDate": "20/10/2017 20:00:03", "downloadFilePath": "https://mdev.xxwmm.wholesaleweb.catalogue.eeeeeeeeee.s3-eu-west-1.amazonaws.com/wholesaleinboundcatalogue/pending/amazon-default-20102017200003.csv" },{ "filename": "amazon-default-20102017200003.csv", "fileDate": "20/10/2017 20:00:03", "downloadFilePath": "https://mdev.xxwmm.wholesaleweb.catalogue.eeeeeeeeee.s3-eu-west-1.amazonaws.com/wholesaleinboundcatalogue/pending/amazon-default-20102017200003.csv" },{ "filename": "amazon-default-20102017200003.csv", "fileDate": "20/10/2017 20:00:03", "downloadFilePath": "https://mdev.xxwmm.wholesaleweb.catalogue.eeeeeeeeee.s3-eu-west-1.amazonaws.com/wholesaleinboundcatalogue/pending/amazon-default-20102017200003.csv" } ] ';

            if (data.listOfCatalogFiles == undefined) {
                this.cataloguedetailListLength = 0;
            } else {
                this.hitNext = true;
                this.cataloguedetailListLength = data.listOfCatalogFiles.length;
            }
            console.log(JSON.stringify(data.listOfCatalogFiles));
            this.catalogueDetailList = (data.listOfCatalogFiles);
            console.log(this.catalogueDetailList);

            if (window.innerWidth < this._globalFunc.stdWidth) {
                document.getElementById("openModalResult").click();
            } else {
                this.catalogueResult = false;
            }
        }
            , err => {
                this._globalFunc.logger("Exported catalog service Failed");
                this.catalogueno = '';
                this.showLoader = false;
                this.cataloguedetailListLength = 0;
                this._router.navigate(['/error404']);
            }
        );
    }

    // use this function to generate catalgoue file api event
    generateCatalogue(catalogueType: any) {
        if (catalogueType == '' || catalogueType == null) {
            this.showErrorMsg = true;
            this.errorMsgString = 'Please select catalogue type';
            setTimeout(() => {
                this.showErrorMsg = false;
            }, 5000);
            return false;
        } else {
            this.showLoader = true;
            console.log(catalogueType);
            this._globalFunc.logger("Generate catalogue service");

            if(sessionStorage.getItem('orgType') === 'amazon' && catalogueType === 'Core'){
                catalogueType = 'main';
            }

            catalogueType = catalogueType.toLowerCase();

            this._postsService.generateCatalogue(this.orgApiName, catalogueType).subscribe(data => {

                this.catalogueFilename = data.fileNameInProcess;
                // data = '{"isFileExist": true,"isFileInProcess": true,"fileNameInProcess": "string"}';

                // this.catalogueFilename = this.orgApiName + "-" + catalogueType + "-20171024121212.csv";
                if (data.isFileExist == true || data.isFileExist == 'true') {
                    this.succMSg = 'Generation of Export Catalogue is already in Progress or just completed. See file ' + this.catalogueFilename;
                } else if (data.isFileInProcess == true) {
                   this.succMSg = 'Your request for initiating Catalogue Export has been accepted. File will be generated as ' + this.catalogueFilename;
                }
               //alert(this.orgApiName + " " + catalogueType);
                let sessionModal = document.getElementById('addConfirmationModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('addConfirmationModal').style.display = 'block';
                this.showLoader = false;
            }, err => {
                this._globalFunc.logger("Generate catalogue service Failed");
                this.cataloguemasterListLength = 0;
                this.showLoader = false;
                this._router.navigate(['/error404']);
            });
        }
        //some coding
    }

    // get relevant order detail
    getAllMasterData() {
        
        this.showLoader = true;
        this._globalFunc.logger("catalogue master data service");
           /* this._postsService.getAllMasterData(this.orgApiName).subscribe(data => {
                
                this.showLoader = false;
                if (data.catalogueTypes.length == 0) {
                    this.cataloguemasterListLength = 0;
                } else {
                    for (var k = 0; k < data.catalogueTypes.length; k++) {
                       
                        if (data.catalogueTypes[k].catalogueType.indexOf(':') > 0) {
                            
                           
                            data.catalogueTypes[k].catalogueType = data.catalogueTypes[k].catalogueType.split(':')[1];
                        }
                        else{
                           
                            data.catalogueTypes[k].catalogueType = this._globalFunc.titleCase(data.catalogueTypes[k].catalogueType);
                        }
                    }
                    this.cataloguemasterListLength = data.catalogueTypes.length;


                }
                this.catalogueTypes = data.catalogueTypes;
            
            }
                , err => {
                    this._globalFunc.logger("catalogue master data service failed");
                    this.cataloguemasterListLength = 0;
                    this.showLoader = false;
                    this._router.navigate(['/error404']);
                }
            );*/
            if(this.customerName === 'amazon'){
                this.filterType = 'storepick';
            }
            else{
                this.filterType = 'main';
            }
            this._postsService.getAllCatalogueTypes(this.customerName,'catalogueName',this.filterType).subscribe((data:any) => {
                console.log(data.storeUIEntryConfig);
                this.showLoader = false;
                if (data.storeUIEntryConfig.length == 0) {
                    this.cataloguemasterListLength = 0;
                    //this._globalFunc.logger("returned empty data.");
                } else {
                    //this._globalFunc.logger("master service data found.");
                    for (var k = 0; k < data.storeUIEntryConfig.length; k++) {
                       // console.log(this._globalFunc.titleCase(data.storeUIEntryConfig[k].catalogueCode));
                        //this.amazonRegionCodesData[k] = data.storeUIEntryConfig[k].catalogueCode;
                         data.storeUIEntryConfig[k].catalogueType = this._globalFunc.titleCase(data.storeUIEntryConfig[k].catalogueType);
                       
                       
                    }
                    this.cataloguemasterListLength = data.storeUIEntryConfig.length;
                }
                console.log(data.storeUIEntryConfig);
                this.catalogueTypes = data.storeUIEntryConfig;
            }
                , (err:any) => {
                    this._globalFunc.logger("catalogue config service failed.");
                    this.cataloguemasterListLength = 0;
                    this.showLoader = false;
                    this._router.navigate(['/error404']);
                }
            );
        
    }

    // to show specific content in table 
    getMinValue(mapValues: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < mapValues.length; i++) {
            if ((mapValues[i].type).toUpperCase() == 'MIN') {
                dataAvail = true;
                return mapValues[i].values[0].value;
            }
        }

        if (!dataAvail)
            return this.strInfoNotAvail;
    }
     getAsinValue(mapValues: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < mapValues.length; i++) {
            if ((mapValues[i].type).toUpperCase() == 'CLIENTID') {
                dataAvail = true;
                return mapValues[i].values[0].value;
            }
        }

        if (!dataAvail)
            return '-';
    }

    // to show specific content in table 
    getWholesalePriceValue(pricesValues: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < pricesValues.length; i++) {
            if ((pricesValues[i].type).toUpperCase() == 'WSP') {
                /* console.log(this.formatDate(pricesValues[i].values[0].start));
                console.log(this.formatDate(this.todayDate.toDateString()));

                 console.log(this.formatDate(pricesValues[i].values[0].end));
                console.log(this.formatDate(this.todayDate.toDateString())); */
                if ((pricesValues[i].values[0].currency).toUpperCase() == 'GBP' && (this.formatDate(pricesValues[i].values[0].start) <= this.formatDate(this.todayDate.toDateString())) && (this.formatDate(pricesValues[i].values[0].end) >= this.formatDate(this.todayDate.toDateString()))) {
                    {
                        dataAvail = true;
                        return pricesValues[i].values[0].value;
                    }
                }
            }
        }
        if (!dataAvail)
            return this.strInfoNotAvail;
    }
    // to show specific content in table 
    getWholesalePackSizeValue(pricesValues: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < pricesValues.length; i++) {
            if ((pricesValues[i].type).toUpperCase() == 'CS') {
                if (this.formatDate(pricesValues[i].values[0].start) <= this.formatDate(this.todayDate.toDateString()) && this.formatDate(pricesValues[i].values[0].end) >= this.formatDate(this.todayDate.toDateString())) {
                    {
                        dataAvail = true;
                        return pricesValues[i].values[0].value;
                    }
                }
            }
        }

        if (!dataAvail)
            return this.strInfoNotAvail;
    }
    // to show specific content in table 
    getWholesalePackTypeValue(pricesValues: any) {
        var dataAvail: boolean = false;
        for (var i = 0; i < pricesValues.length; i++) {
            if (pricesValues[i].type.toUpperCase() == 'CS' || pricesValues[i].type.toUpperCase() == 'EA') {
                dataAvail = true;
                // return pricesValues[i].values[0].value;
                return pricesValues[i].type.toUpperCase();
            }
        }

        if (!dataAvail)
            return this.strInfoNotAvail;
    }

    // format date function
    formatDate(createdAtDate: string) {
        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
            if (createdAtDate.indexOf('/') >= 0) {
                return createdAtDate;
            } else {
                var d = new Date(createdAtDate);
                var yyyy = d.getFullYear();
                var mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1); // getMonth() is zero-based
                var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
                // var hh = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
                // var min = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
                return yyyy + "/" + mm + "/" + dd;
            }
        } else {
            return '-';
        }
    }

    closePopup() {
        let sessionModal = document.getElementById('addConfirmationModal');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('addConfirmationModal').style.display = 'none';
    }

    getCustomerImage(custName:string){
        return this._globalFunc.getCustomerImage(custName);
    }
   getUserSession(customerName:any, loggedInUserGroup:any, loggedInUserId:any){
        this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData:any) => {
            if(!userData.isSessionActive) { // false
                // User session has expired.
                sessionStorage.clear();
                this._router.navigate(['']);
            } else {
                this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                let changeOrgAvailable: any = [];
                let orgImgUrl = "";
                let orgApiNameTemp = "";
                this.organisationList.forEach(function (item: any) {
                    if (item.customerName !== customerName) {
                        changeOrgAvailable.push(item);
                    } else {
                        orgImgUrl = item.customerName+".png";
                        orgApiNameTemp = item.customerName;
                    }
                });
                this.organisationListChange = changeOrgAvailable;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                // this.disabledCusChange = false;
                if (this.organisationList.length == 1) {
                    this.disabledCusChange = true;
                }

                console.log("this.organisationList234");
                console.log(this.organisationList);
                let that = this;
                let catalogueBulkExportView = true;
                let claimsReview = true;
                let dashboardMenuPermission = false;
                let userHavingPermission = false;
                this.organisationList.forEach(function (organisation: any) {
                    
                    if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                        console.log("organisation : "+that.customerName);
                        console.log(organisation);
                        organisation.permissionRoleList.forEach(function(permRoleList: any){
                            console.log("11");
                            console.log(permRoleList);
                            permRoleList.permissionDetailsList.forEach(function(permRoleDtlList: any){
                                console.log("22");
                                if(permRoleDtlList.permissionGroup == 'catalogueManagement'){
                                    userHavingPermission = true;
                                    permRoleDtlList.permissionNameList.forEach(function(permList: any){
                                        if(permList.permissionName == 'catalogueManagement' && permList.permissionStatus == 'active'){
                                            dashboardMenuPermission = true;
                                        }

                                        switch(permList.permissionName){
                                            case "catalogueBulkExportView":
                                                console.log("edit catalogue permission");
                                                catalogueBulkExportView = true;
                                                //alert(filfilment);
                                                break;
                                        }
                                    });
                                }
                            });
                        });
                    }
                });
                that = null;
                if(!catalogueBulkExportView){
                    // redirect to order
                    this._router.navigate(["cataloguemanagement",customerName]);
                }
            }
            //alert(this.disabledfilfilment);
        });
    }
}