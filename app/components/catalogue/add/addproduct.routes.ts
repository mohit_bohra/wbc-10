import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddProductComponent } from './addproduct.component';

const addProductRoutes: Routes = [
  { path: '', component: AddProductComponent },
];


@NgModule({
  imports: [RouterModule.forChild(addProductRoutes)],
  exports: [RouterModule]
})
export class AddProductComponentRoutes { }
