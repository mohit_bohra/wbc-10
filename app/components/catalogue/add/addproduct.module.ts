import { NgModule } from '@angular/core';
import { AddProductComponentRoutes } from './addproduct.routes';
import { AddProductComponent } from './addproduct.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/primeng';

import { DatepickerModule } from '../../datepicker/ng2-bootstrap';
@NgModule({
  imports: [
    AddProductComponentRoutes,
    CommonModule,
    FormsModule,
    DatepickerModule.forRoot(),
    CalendarModule

  ],
  exports: [],
  declarations: [AddProductComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class AddProductModule { }
