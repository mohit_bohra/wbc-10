import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    templateUrl: 'addproduct.component.html',
    providers: [commonfunction]
})
export class AddProductComponent {
    objConstant = new Constant();
    whoImgUrl: any;
    productid: string;
    errorMsg: boolean;
    errorMsgString: string;
    showLoader: boolean;
    printHeader: boolean;
    organisationList: any;
    pageState: number = 7;
    customerName: string;
    customerDispayName: string;
    style: string;
    orgimgUrl: string;
    orgApiName: string;
    productDetails: any;

    productName: any;
    mapLen: any;
    description: any;
    homeUrl: any;
    private _sub: any;
    availabilityMaster: any;
    WorkflowMaster: any;
    catalogueTypeMaster: any;
    availabilityStatus: string;
    workflowStatusName: string;
    mapTypesMaster: any;
    mapTypesMasterDisplay: any;
    lookupTypesMaster: any;
    lookupTypesMasterDisplay: any;
    financialTypesMaster: any;
    financialTypesMasterDisplay: any;
    financialCurrencies: any;
    catalogueType: string;
    imageUrl: string;
    productMaps: any;
    productPrices: any;
    productmin: string;
    maptypeValue: string;
    isProductIdEntered: boolean;

    showSuccessModel: boolean;
    minDate: any;
    addedProdId: any;
    addedMin: any;
    identifierRow: number = 1;
    pricingRow: number = 1;
    attributeRow: number = 1;

    prodIdentifierRowHTML: any;

    todayDateStr: string;
    tempCalId: any;
    calDefaultDate: Date;

    allowWrite: boolean;
    imgName: string = "";
    allowAddCatalogueAttribute:boolean = false;

    
    msgJSON: any = {};
    terminateMsg: any;
    terminateMsgForInactiveUser :any;
    terminateFlg: boolean = false;
    filterType : string ='';

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.terminateMsg = this.objConstant.userSessionTerminatedMsg;
        this.terminateMsgForInactiveUser = this.objConstant.userSessionTerminatedMsgForInactiveUser;
        this.errorMsgString = '';
        this.errorMsg = false;
    }
     ngOnInit() {
        this._globalFunc.logger("Loading add product on UI");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {
            this.printHeader = true;
            this.availabilityStatus = '';
            this.workflowStatusName = '';
            this.catalogueType = '';
            this.productid = '';
            this.productName = '';
            this.productmin = '';
            this.description = '';
            this.imageUrl = this.whoImgUrl + "add_product.jpg";
            this.isProductIdEntered = false;
            this.allowWrite = false;
            this.tempCalId = "";
            this._globalFunc.autoscroll();
            this.maptypeValue = '';
            this.minDate = new Date();
            this.todayDateStr = this._cfunction.todayDateStrFunction();
            this.pageState = 7;
            this.identifierRow = 1;
            this.pricingRow = 1;
            this.attributeRow = 1;
            sessionStorage.setItem('pageState', '999');
            document.getElementById('diableLogo').style.cursor = "not-allowed";
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                this.showLoader = true;
                this.allowWrite = false;
                document.getElementById('addConfirmationModal').style.display = 'none';
                this.customerName = params.orgType;
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                let cusType = params.orgType;

                let orgImgUrl;
                let orgApiNameTemp;
                let writePermission;

                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
                this.imgName = this.getCustomerImage(params.orgType);
                this.allowWrite = writePermission;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = params.orgType.toLowerCase();
            });
            let wid: any;
            wid = window.innerWidth;
            let screen: string;
             screen = this._globalFunc.getDevice(wid);
             window.onresize = () => {
                if (screen == "desktop") {
                    document.getElementById('prouctDetailTitle').style.display = 'block';
                    document.getElementById('productDetailHeader').style.display = 'block';
                    document.getElementById('productDetailImg').style.display = 'block';
                }

                screen = this._globalFunc.getDevice(window.innerWidth);
            };

            this.getAllMasterData();
            this.getCatalogueTypeConfigData();
        }
    }

     redirect(page: any, orgType: any) {
        this._globalFunc.logger("Redirect to "+page);
        this._router.navigate([page, orgType]);
    }

    refresh() {
        this._globalFunc.logger("Reload the screen");
        this.ngOnInit();
    }

    ngOnDestroy() {
        this._globalFunc.logger("Unloading add product screen.");
        this._globalFunc.sentLoggerBatchToServer();
    }

    keyPress(event: any) {
        this._globalFunc.alphaNumeric(event);
    }

    keyPressWithSpace(event: any) {
        this._globalFunc.alphaNumericWithSpace(event);
    }

    keyPressNumericWithDecimal(event: any) {
        this._globalFunc.numericWithDecimal(event);
    }

    keyPressNumericWithDecimalRestrict(event: any, val: any, elementId: any) {
        console.log(elementId + val);
        val = (<HTMLInputElement>document.getElementById(elementId)).value;
        console.log(val);
        if (parseFloat(val) == val) {

            var valArr = val.split('.');
            if (valArr.length > 2 || (typeof valArr[1] != 'undefined' && valArr[1].length > 2) || valArr[0] == "") {
                val = val.substr(0, val.length - 1);
                val = parseFloat(val).toFixed(2);
                (<HTMLInputElement>document.getElementById(elementId)).value = val;
            } else {
                if (parseFloat(val) != val) {
                    val = val.substr(0, val.length - 1);
                    (<HTMLInputElement>document.getElementById(elementId)).value = val;
                }
            }
        } else {
            (<HTMLInputElement>document.getElementById(elementId)).value = "";
        }
    }


    eventHandler(keyCode: any, field: any) {
        if (keyCode != '' && field == 1) {
            document.getElementById("search-img").classList.remove('disable');
        } else {
            document.getElementById("search-img").classList.add('disable');
        }
    }

    getSingleProductDetail(value: any) {
        document.getElementById('addProdErrMsg').style.display = 'none';
        this.showLoader = true;
        this._globalFunc.logger("Product detail service");
        this._postsService.getProductDetailsUsingItemId(value).subscribe(data => {
            if (data != null && data.itemDescription != null && data != undefined && data.itemDescription != undefined) {
                this.showLoader = false;
                this.productName = data.itemDescription;
                if (data.imageUrl != null) {
                    if (data.imageUrl[0] != undefined) { this.imageUrl = data.imageUrl[0].url; }
                }
                this.isProductIdEntered = true;
            }
        }
            , err => {
                this._globalFunc.logger("Product detail service failed - Invalid Product id");
                this.showLoader = false;
                this.errorMsgString = "Invalid Product id";
                document.getElementById('addProdErrMsg').style.display = 'block';
                this._globalFunc.autoscroll();
                setTimeout(() => {
                    document.getElementById('addProdErrMsg').style.display = 'none';
                }, 5000);
                this.productid = "";
            }
        );
    }

    getAllMasterData() {
        this._globalFunc.logger("Catalogue master data service");
        this._postsService.getAllMasterData(this.orgApiName).subscribe(data => {
            this.showLoader = false;
            if (data != null && data != undefined) {
                if (data.availabilityStatusList != undefined) {
                    this.availabilityMaster = data.availabilityStatusList;
                }
                if (data.workflowStatuses != undefined) {
                    this.WorkflowMaster = data.workflowStatuses;
                }
                /*if (data.catalogueTypes != undefined) {
                    for (var k = 0; k < data.catalogueTypes.length; k++) {
                       
                        if (data.catalogueTypes[k].catalogueType.indexOf(':') > 0) {
                            
                           
                            data.catalogueTypes[k].catalogueType = data.catalogueTypes[k].catalogueType.split(':')[1];
                        }
                        else{
                           
                            data.catalogueTypes[k].catalogueType = this._globalFunc.titleCase(data.catalogueTypes[k].catalogueType);
                        }
                    }
                    this.catalogueTypeMaster = data.catalogueTypes;
                    
                }*/
                if (data.financialTypes != undefined) {
                    this.financialTypesMaster = data.financialTypes;
                    this.financialTypesMasterDisplay = this.financialTypesMaster;

                    this.financialTypesMasterDisplay.forEach(function (financialTypesItem: any) {
                        financialTypesItem['id'] = 'pricing_' + financialTypesItem.financialType;
                        financialTypesItem['value'] = '';
                        financialTypesItem['currency_id'] = "currency_" + financialTypesItem.financialType;
                        financialTypesItem['currency_value'] = '';
                        financialTypesItem['date_id'] = "date_pricing_" + financialTypesItem.financialType;
                        financialTypesItem['date_value'] = '';
                        financialTypesItem['financialType1'] = "";
                    });
                }

                if (data.financialCurrencies != undefined) {
                    this.financialCurrencies = data.financialCurrencies;
                }

                if (data.mapTypes != undefined) {
                    this.mapTypesMaster = data.mapTypes;
                    this.mapTypesMasterDisplay = this.mapTypesMaster;
                    this.mapTypesMasterDisplay.forEach(function (mapTypeItem: any) {
                        mapTypeItem['id'] = 'identifier_' + mapTypeItem.mapName;
                        mapTypeItem['value'] = '';
                        mapTypeItem['date_id'] = 'date_identifier_' + mapTypeItem.mapName;
                        mapTypeItem['date_value'] = '';
                        mapTypeItem['mapName1'] = "";
                    });
                }

                if (data.lookupTypes != undefined) {
                    this.lookupTypesMaster = data.lookupTypes;
                    this.lookupTypesMasterDisplay = this.lookupTypesMaster;

                    for (let j = 0; j < this.lookupTypesMasterDisplay.length; j++) {
                        let lookupTypeItem = this.lookupTypesMasterDisplay[j];
                        lookupTypeItem['id'] = 'lookup_' + lookupTypeItem.lookupName;
                        lookupTypeItem['value'] = '';
                        lookupTypeItem['date_id'] = 'date_lookup_' + lookupTypeItem.lookupName;
                        lookupTypeItem['date_value'] = '';
                        lookupTypeItem['lookupName1'] = '';
                    }
                }
            }
        }
            , err => {
                this._globalFunc.logger("Catalogue master data service failed");
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
        );
    }

    isodate(dateStr: any) {
        if (dateStr.length < 20) {
            var formatd = dateStr.split(" ");
            var formatedDateArr = formatd[0].split("/");
            return formatedDateArr[2] + '-' + formatedDateArr[1] + '-' + formatedDateArr[0] + 'T' + formatd[1] + ':00Z';
        } else {
            var dd = new Date(dateStr).toString();
            var formatdd = dd.split(" ");
           var months = [" ", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
           var mmm = months.indexOf(formatdd[1]) < 10 ? "0" + (months.indexOf(formatdd[1])) : (months.indexOf(formatdd[1])); 
            return formatdd[3] + '-' + mmm + '-' + formatdd[2] + 'T' + formatdd[4] + 'Z';
        }
    }
    async sleep(ms: number){
        await this._sleep(ms);
    }

    _sleep(ms: number){
        return new Promise((resolve) => setTimeout(resolve, ms));
    }

    addNewProduct(value: any, event: Event) {

        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));

        console.time('start');
        this.sleep(10000).then((msg) =>{
            console.timeEnd('start');
        });
        
        event.preventDefault();
        var dateISOMap: any;

        var todayDateSringLocal = this._cfunction.todayDateStrFunction();
        if(sessionStorage.getItem('orgType') === 'amazon' && value.catalogueType === 'Core'){
            value.catalogueType = 'main';
        }
        
        value.catalogueType = value.catalogueType.toLowerCase();

        var that = this;
        var newProductString = '{"items":[';
        var productValue = '{';
         productValue += '"availability":"' + value.availabilityStatus + '"';
        productValue += ',"workflow": "' + value.workflowStatusName + '"';
        productValue += ',"description":"' + value.description + '"';
        productValue += ',"customer":"' + this.orgApiName + '"';
        productValue += ',"catalogue":"' + value.catalogueType + '"';
        productValue += ',"weight":false';
        productValue += ',"maps":[';

        this.mapTypesMasterDisplay.forEach(function (mapTypeItem: any) {
            if ((<HTMLInputElement>document.getElementById('identifier_' + mapTypeItem.mapName)).value != "" && mapTypeItem.mapName != "") {
                if ((<HTMLInputElement>document.getElementById('date_identifier_' + mapTypeItem.mapName)).value != '') {
                    dateISOMap = that._cfunction.isodate((<HTMLInputElement>document.getElementById('date_identifier_' + mapTypeItem.mapName)).value);
                } else {
                    dateISOMap = todayDateSringLocal;
                }

                productValue += '{';
                productValue += '"type":"' + mapTypeItem.mapName + '"';
                productValue += ',"values":[{';
                productValue += '"value": "' + (<HTMLInputElement>document.getElementById('identifier_' + mapTypeItem.mapName)).value + '"';
                productValue += ',"start": "' + dateISOMap + '"';
                productValue += ',"end":"' + "2999-12-31T00:00:00Z" + '"';
                productValue += '}]},';
            }
        });

       for (var n = 1; n < this.identifierRow; n++) {
            if ((<HTMLInputElement>document.getElementById('identifierKey' + n)) && (<HTMLInputElement>document.getElementById('identifierValue' + n))) {
                if ((<HTMLInputElement>document.getElementById('identifierKey' + n)).value != "" && (<HTMLInputElement>document.getElementById('identifierValue' + n)).value != "") {
                    if ((<HTMLInputElement>document.getElementById('identifierDateKey' + n)).value != '') {
                        dateISOMap = that._cfunction.isodate((<HTMLInputElement>document.getElementById('identifierDateKey' + n)).value);
                    } else {
                        dateISOMap = todayDateSringLocal;
                    }
                    productValue += '{';
                    productValue += '"type":"' + (<HTMLInputElement>document.getElementById('identifierKey' + n)).value + '"';
                    productValue += ',"values":[{';
                    productValue += '"value": "' + (<HTMLInputElement>document.getElementById('identifierValue' + n)).value + '"';
                    productValue += ',"start": "' + dateISOMap + '"';
                    productValue += ',"end":"' + "2999-12-31T00:00:00Z" + '"';
                    productValue += '}]},';
                }
            }
        }

        if (productValue[productValue.length - 1] == ',') {
            productValue = productValue.substr(0, productValue.length - 1);
        }

        productValue += ']';
        productValue += ',"prices":[';

        var dateISOPrice;
        var currencyVal;
        this.financialTypesMasterDisplay.forEach(function (financialTypesMasterItem: any) {
            if ((<HTMLInputElement>document.getElementById('pricing_' + financialTypesMasterItem.financialType)).value != "" && financialTypesMasterItem.financialType != "") {
                if ((<HTMLInputElement>document.getElementById('date_pricing_' + financialTypesMasterItem.financialType)).value != '') {
                    dateISOPrice = that._cfunction.isodate((<HTMLInputElement>document.getElementById('date_pricing_' + financialTypesMasterItem.financialType)).value);
                } else {
                    console.log("22");
                    dateISOPrice = todayDateSringLocal;
                }

                if ((<HTMLInputElement>document.getElementById('currency_' + financialTypesMasterItem.financialType)).value != "") {
                    currencyVal = (<HTMLInputElement>document.getElementById('currency_' + financialTypesMasterItem.financialType)).value;
                } else {
                    if (financialTypesMasterItem.financialType == 'cs') {
                        currencyVal = 'none';
                    } else {
                        currencyVal = 'gbp';
                    }
                }

                productValue += '{';
                productValue += '"type":"' + financialTypesMasterItem.financialType + '"';
                productValue += ',"values":[{';
                productValue += '"currency": "' + currencyVal + '"';
                productValue += ',"value": ' + (<HTMLInputElement>document.getElementById('pricing_' + financialTypesMasterItem.financialType)).value;
                productValue += ',"start": "' + dateISOPrice + '"';
                productValue += ',"end":"' + "2999-12-31T00:00:00Z" + '"';
                productValue += '}]},';
            }
        });

       for (var n = 1; n < this.pricingRow; n++) {
            if ((<HTMLInputElement>document.getElementById('pricingKey' + n)) && (<HTMLInputElement>document.getElementById('pricingValue' + n))) {
                if ((<HTMLInputElement>document.getElementById('pricingKey' + n)).value != "" && (<HTMLInputElement>document.getElementById('pricingValue' + n)).value != "") {
                    if ((<HTMLInputElement>document.getElementById('pricingDateKey' + n)).value != '') {
                        dateISOPrice = that._cfunction.isodate((<HTMLInputElement>document.getElementById('pricingDateKey' + n)).value);
                    } else {
                        console.log("22");
                        dateISOPrice = todayDateSringLocal;
                    }

                    productValue += '{';
                    productValue += '"type":"' + (<HTMLInputElement>document.getElementById('pricingKey' + n)).value + '"';
                    productValue += ',"values":[{';
                    productValue += '"currency": "' + (<HTMLInputElement>document.getElementById('pricingCurrencyKey' + n)).value + '"';
                    productValue += ',"value": ' + (<HTMLInputElement>document.getElementById('pricingValue' + n)).value;
                    productValue += ',"start": "' + dateISOPrice + '"';
                    productValue += ',"end":"' + "2999-12-31T00:00:00Z" + '"';
                    productValue += '}]},';
                }
            }
        }

        if (productValue[productValue.length - 1] == ',') {
            productValue = productValue.substr(0, productValue.length - 1);
        }

        productValue += ']';
        productValue += ',"lookups":[';

        var dateISOLookup;
        this.lookupTypesMasterDisplay.forEach(function (lookupTypeItem: any, index: number) {
            if ((<HTMLInputElement>document.getElementById('lookup_' + lookupTypeItem.lookupName)).value != "" && lookupTypeItem.lookupName != "") {
                if ((<HTMLInputElement>document.getElementById('date_lookup_' + lookupTypeItem.lookupName)).value != '') {
                    dateISOLookup = that._cfunction.isodate((<HTMLInputElement>document.getElementById('date_lookup_' + lookupTypeItem.lookupName)).value);
                } else {
                    console.log("33");
                    dateISOLookup = todayDateSringLocal;
                }

                productValue += '{';
                productValue += '"type":"' + lookupTypeItem.lookupName + '"';
                productValue += ',"values":[{';
                productValue += '"value": "' + (<HTMLInputElement>document.getElementById('lookup_' + lookupTypeItem.lookupName)).value + '"';
                productValue += ',"start": "' + dateISOLookup + '"';
                productValue += ',"end":"' + "2999-12-31T00:00:00Z" + '"';
                productValue += '}]},';
            }
        });

         for (var n = 1; n < this.attributeRow; n++) {
            if ((<HTMLInputElement>document.getElementById('attributeKey' + n)) && (<HTMLInputElement>document.getElementById('attributeValue' + n))) {
                if ((<HTMLInputElement>document.getElementById('attributeKey' + n)).value != "" && (<HTMLInputElement>document.getElementById('attributeValue' + n)).value != "") {
                    if ((<HTMLInputElement>document.getElementById('attributeDateKey' + n)).value != '') {
                        dateISOLookup = that._cfunction.isodate((<HTMLInputElement>document.getElementById('attributeDateKey' + n)).value);
                    } else {
                        dateISOLookup = todayDateSringLocal;
                    }
                    productValue += '{';
                    productValue += '"type":"' + (<HTMLInputElement>document.getElementById('attributeKey' + n)).value + '"';
                    productValue += ',"values":[{';
                    productValue += '"value": "' + (<HTMLInputElement>document.getElementById('attributeValue' + n)).value + '"';
                    productValue += ',"start": "' + dateISOLookup + '"';
                    productValue += ',"end":"' + "2999-12-31T00:00:00Z" + '"';
                    productValue += '}]},';
                }
            }
        }

        if (productValue[productValue.length - 1] == ',') {
            productValue = productValue.substr(0, productValue.length - 1);
        }

        productValue += ']';
        productValue += '}';

        newProductString += productValue + ']}';
        console.log("Final String");
        console.log(newProductString);
        var checkStr = JSON.parse(newProductString);
        console.log(checkStr);

        var mapsRequiredExist = false;
         checkStr.items[0].maps.forEach(function (mapItem: any) {
           if ((mapItem.type).toUpperCase() == 'MIN' || (mapItem.type).toUpperCase() == 'PIN' || (mapItem.type).toUpperCase() == 'EAN') {
                mapsRequiredExist = true;
            }
        });

        if (mapsRequiredExist) {
            this.showLoader = true;
            this._globalFunc.logger("add product Post service");
            this._postsService.addNewProductToCatalogue(this.orgApiName, value.catalogueType, newProductString).subscribe(data => {
                this.showLoader = false;
                this.showSuccessModel = true;

                let sessionModal = document.getElementById('addConfirmationModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('addConfirmationModal').style.display = 'block';
            }
                , err => {
                    this._globalFunc.logger("add product Post service Failed");
                    this.showLoader = false;
                    this._router.navigate(['/error404']);
                }
            );
        } else {
            document.getElementById('addProdErrMsg').style.display = 'block';
            this.errorMsgString = "Please enter at least one product identifier from min, pin and ean";
            this._globalFunc.autoscroll();
            setTimeout(() => {
                document.getElementById('addProdErrMsg').style.display = 'none';
            }, 10000);
        }
    }

    openCalender(id: any) {
        console.log(id);
        console.log("Previous Date : " + (<HTMLInputElement>document.getElementById(id)).value);
        if ((<HTMLInputElement>document.getElementById(id)).value != "") {
            var dtStr = (<HTMLInputElement>document.getElementById(id)).value.split("/");
            this.calDefaultDate = new Date(dtStr[2] + "-" + dtStr[1] + "-" + dtStr[0]);
            console.log(this.calDefaultDate);
        } else {
            this.calDefaultDate = new Date();
        }

        this.tempCalId = id;
        let calModal = document.getElementById('calenderModal');
        calModal.classList.remove('in');
        calModal.classList.add('out');
        document.getElementById('calenderModal').style.display = 'block';
    }

    CalOnDateSelection(value: any) {
        console.log(value);
        var dd = new Date(value).toString();
        var formatdd = dd.split(" ");
        var months = [" ", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var mmm = months.indexOf(formatdd[1]) < 10 ? "0" + (months.indexOf(formatdd[1])) : (months.indexOf(formatdd[1]));
        var dtStr = formatdd[2] + "/" + mmm + "/" + formatdd[3];
        (<HTMLInputElement>document.getElementById(this.tempCalId)).value = dtStr;
        document.getElementById('calenderModal').style.display = "none";
    }

    closeDateModelBox() {
        document.getElementById('calenderModal').style.display = "none";
    }
    clearDateModelBox() {
        (<HTMLInputElement>document.getElementById(this.tempCalId)).value = null;
        document.getElementById('calenderModal').style.display = "none";
    }

   addProdMapNewRow() {
        this._globalFunc.logger("add product identifier row");
        var z = document.createElement('tr');
        z.innerHTML = "<td><input required class='ht33 orders-heading avail-text textbox-wid-bor' id='identifierKey" + this.identifierRow + "' name='identifierKey" + this.identifierRow + "' maxlength='15'></td>"
            + "<td><input required class='ht33 orders-heading avail-text textbox-wid-bor' id='identifierValue" + this.identifierRow + "' name='identifierValue" + this.identifierRow + "' maxlength='15' oncopy='return false' oncut='return false' onpaste='return false' ></td>"
            + "<td id='identifierDateTDKey" + this.identifierRow + "'><div class='input-group calendar-input-grp avail-text textbox-wid-bor'><input placeholder='DD/MM/YYYY' value='' id='identifierDateKey" + this.identifierRow + "' name='identifierDateKey" + this.identifierRow + "' readonly class='form-control selectdate bg-transparent' required><span class='input-group-addon calendar-addon bg-transparent' ><i class='glyphicon glyphicon-calendar cust-calender'></i></span></div></td>"
            + "<td><div class='dis-inlineblc padleft-5' id='identifierDeleteKey" + this.identifierRow + "' name='identifierDeleteKey" + this.identifierRow + "'><span class='glyphicon glyphicon-trash text-green padd-top-11 font-size16px' ></span></div>"
            + "</td>";

        document.getElementById('identifierBody').appendChild(z);
        var rowNumber = this.identifierRow;
        var contest = this;
        document.getElementById('identifierKey' + rowNumber).onkeypress = function ($event) {
            contest.keyPress($event);
        }
        document.getElementById('identifierValue' + rowNumber).onkeypress = function ($event) {
            contest.keyPress($event);
        }

        document.getElementById('identifierDateTDKey' + rowNumber).onclick = function () {
            contest.openCalender('identifierDateKey' + rowNumber);
        }
        document.getElementById('identifierDeleteKey' + rowNumber).onclick = function () {
            contest.deleteProdMapNewRow('identifierDeleteKey' + rowNumber);

        }
        this.identifierRow = this.identifierRow + 1;
    }

    addProdPriceNewRow() {
        this._globalFunc.logger("add product pricing row");
        var selCurriencies = '<div class="col-xs-12 col-md-12 col-lg-12 pad-left-right"><span class="up_arrow glyphicon glyphicon-menu-down top fontsize-25 top3 rt5px"></span><select required class="bg-none ht33 orders-heading avail-text textbox-wid-bor" id="pricingCurrencyKey' + this.pricingRow + '" name="pricingCurrencyKey' + this.pricingRow + '"><option value="" selected>Please Select</option>';
        this.financialCurrencies.forEach(function (fin: any) {
            selCurriencies += '<option   [value]="' + fin.financialCurrency + '" >' + fin.financialCurrency + '</option> ';
        });
        selCurriencies += "</select></div>";
        var z = document.createElement('tr');
        z.innerHTML = "<td><input required class='ht33 orders-heading avail-text textbox-wid-bor' id='pricingKey" + this.pricingRow + "' name='pricingKey" + this.pricingRow + "' maxlength='15'></td>"
            + "<td>" + selCurriencies + "</td>"
            + "<td><input required class='ht33 orders-heading avail-text textbox-wid-bor' id='pricingValue" + this.pricingRow + "' name='pricingValue" + this.pricingRow + "' maxlength='15' oncopy='return false' oncut='return false' onpaste='return false' ></td>"
            + "<td id='pricingDateTDKey" + this.pricingRow + "'><div class='input-group calendar-input-grp avail-text textbox-wid-bor'><input placeholder='DD/MM/YYYY' value='' id='pricingDateKey" + this.pricingRow + "' name='pricingDateKey" + this.pricingRow + "' readonly class='form-control selectdate bg-transparent' required><span class='input-group-addon calendar-addon bg-transparent' ><i class='glyphicon glyphicon-calendar cust-calender'></i></div></td>"
            + "<td><div class='dis-inlineblc padleft-5' id='pricingDeleteKey" + this.pricingRow + "' name='pricingDeleteKey" + this.pricingRow + "'><span class='glyphicon glyphicon-trash text-green padd-top-11 font-size16px' ></span></div>"
            + "</td>";
        document.getElementById('pricingBody').appendChild(z);
        var rowNumber = this.pricingRow;
        var contest = this;
        document.getElementById('pricingKey' + rowNumber).onkeypress = function ($event) {
            contest.keyPress($event);
        }
        document.getElementById('pricingValue' + rowNumber).onkeyup = function ($event) {
            contest.keyPressNumericWithDecimalRestrict($event, (<HTMLInputElement>document.getElementById("pricingValue" + rowNumber)).value, "pricingValue" + rowNumber);
        }

        document.getElementById('pricingDateTDKey' + rowNumber).onclick = function () {
            contest.openCalender('pricingDateKey' + rowNumber);
        }
        document.getElementById('pricingDeleteKey' + rowNumber).onclick = function () {
            contest.deleteProdMapNewRow('pricingDeleteKey' + rowNumber);
        }
        this.pricingRow = this.pricingRow + 1;
    }
   
    addProdLookupNewRow() {
        this._globalFunc.logger("add product lookup row");
        var z = document.createElement('tr');
        z.innerHTML = "<td><input required class='ht33 orders-heading avail-text textbox-wid-bor' id='attributeKey" + this.attributeRow + "' name='attributeKey" + this.attributeRow + "' maxlength='15'></td>"
            + "<td><input required class='ht33 orders-heading avail-text textbox-wid-bor' id='attributeValue" + this.attributeRow + "' name='attributeValue" + this.attributeRow + "' maxlength='15' oncopy='return false' oncut='return false' onpaste='return false' ></td>"
            + "<td id='attributeDateTDKey" + this.attributeRow + "'><div class='input-group calendar-input-grp avail-text textbox-wid-bor'><input placeholder='DD/MM/YYYY' value='' id='attributeDateKey" + this.attributeRow + "' name='attributeDateKey" + this.attributeRow + "' readonly class='form-control selectdate bg-transparent' required><span class='input-group-addon calendar-addon bg-transparent' ><i class='glyphicon glyphicon-calendar cust-calender'></i></div></td>"
            + "<td><div class='dis-inlineblc padleft-5' id='attributeDeleteKey" + this.attributeRow + "' name='attributeDeleteKey" + this.attributeRow + "'><span class='glyphicon glyphicon-trash text-green padd-top-11 font-size16px' ></span></div>"
            + "</td>";
        document.getElementById('attributeBody').appendChild(z);
        var rowNumber = this.attributeRow;
        var contest = this;
        document.getElementById('attributeKey' + rowNumber).onkeypress = function ($event) {
            contest.keyPress($event);
        }
        document.getElementById('attributeValue' + rowNumber).onkeypress = function ($event) {
            contest.keyPressWithSpace($event);
        }
        document.getElementById('attributeDateTDKey' + rowNumber).onclick = function () {
            contest.openCalender('attributeDateKey' + rowNumber);
        }
        document.getElementById('attributeDeleteKey' + rowNumber).onclick = function () {
            contest.deleteProdMapNewRow('attributeDeleteKey' + rowNumber);
        }
        this.attributeRow = this.attributeRow + 1;
    }

    deleteProdMapNewRow(value: any) {
        console.log(value);
        let list = document.getElementById(value).parentNode.parentNode;
        console.log(list);
        list.parentNode.removeChild(list);
        
    }

    deleteProdPriceNewRow(value: any) {
        this._globalFunc.logger("delete product pricing row");
        var newPriceArray: any = [];
        this.financialTypesMasterDisplay.forEach(function (financialTypesMasterItem: any, indexOf: number) {
            if (indexOf == value) {
            } else {
                newPriceArray.push(financialTypesMasterItem);
            }
        });
        this.financialTypesMasterDisplay = newPriceArray;

    }

    deleteProdLookupNewRow(value: any) {
        this._globalFunc.logger("delete product lookup row");
        var newLookupArray: any = [];
        this.lookupTypesMasterDisplay.forEach(function (lookupTypeItem: any, indexOf: number) {
            if (indexOf == value) {
            } else {
                newLookupArray.push(lookupTypeItem);
            }
        });
        this.lookupTypesMasterDisplay = newLookupArray;
    }

    deleteProdNewRow(value: any) {
        this._globalFunc.logger("delete product identifier row");
        var list = document.getElementById(value);
        list.parentElement.parentElement.remove();
    }

    closeTab() {
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));

        window.top.close();
    }

    getCustomerImage(custName:string){
        return this._globalFunc.getCustomerImage(custName);
    }

    getUserSession(customerName:any, loggedInUserGroup:any, loggedInUserId:any){
        this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData:any) => {
            if(!userData.isSessionActive) { 
                if(userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser){
                    console.log("Session terminated");
                    this.terminateFlg = true;

                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                }
                else{
                sessionStorage.clear();
                this._router.navigate(['']);
                }
            } else {
                if(userData.sessionMessage == this.objConstant.userSessionTerminatedMsg){
                    console.log("Session terminated");
                    this.terminateFlg = true;

                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                }else{                    
                    this.terminateFlg = false;
                    this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                    let changeOrgAvailable: any = [];
                    let orgImgUrl = "";
                    let orgApiNameTemp = "";
                    this.organisationList.forEach(function (item: any) {
                        if (item.customerName !== customerName) {
                            changeOrgAvailable.push(item);
                        } else {
                            orgImgUrl = item.customerName+".png";
                            orgApiNameTemp = item.customerName;
                        }
                    });
                    
                    this.orgimgUrl = orgImgUrl;
                    this.orgApiName = orgApiNameTemp;
    
                    console.log("this.organisationList234");
                    console.log(this.organisationList);
                    let that = this;
                    let orderEnquiry = true;
                    let claimsReview = true;
                    let catalogueManagementMenuPermission = false;
                    let addCatalogueEntry = false;
                    let addCatalogueAttribute = false;
                    this.organisationList.forEach(function (organisation: any) {
                        
                        if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                            console.log("organisation : "+that.customerName);
                            console.log(organisation);
                            organisation.permissionRoleList.forEach(function(permRoleList: any){
                                console.log("11");
                                console.log(permRoleList);
                                permRoleList.permissionDetailsList.forEach(function(permRoleDtlList: any){
                                    console.log("22");
                                    if(permRoleDtlList.permissionGroup == 'catalogueManagement'){
                                        permRoleDtlList.permissionNameList.forEach(function(permList: any){
                                            if(permList.permissionName == 'catalogueManagement'){
                                                catalogueManagementMenuPermission = true;
                                                console.log("catalogueManagement permission");
                                            }
    
                                            switch(permList.permissionName){
                                                case "addCatalogueEntry":
                                                    console.log("edit catalogue permission");
                                                    addCatalogueEntry = true;
                                                    
                                                    break;
                                                case "addCatalogueAttribute":
                                                    console.log("add catalogue attribute permission");
                                                    addCatalogueAttribute = true;
                                                   
                                                    break;
                                            }
                                        });
                                    }
                                });
                            });
                        }
                    });
                    that = null;
                    if(!addCatalogueEntry){ 
                        this._router.navigate(["cataloguemanagement",customerName]);
                    }
    
                    this.allowWrite = addCatalogueEntry;
                    this.allowAddCatalogueAttribute = addCatalogueAttribute;
                }
            }
         });
    }

    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }

    getCatalogueTypeConfigData(){
        if(this.customerName === 'amazon'){
            this.filterType = 'storepick';
        }
        else{
            this.filterType = 'main';
        }
        this._postsService.getAllCatalogueTypes(this.customerName,'catalogueName',this.filterType).subscribe((data:any) => {
            console.log(data.storeUIEntryConfig);
            //this.showLoader = false;
            if (data.storeUIEntryConfig.length == 0) {
                //this.cataloguemasterListLength = 0;
                //this._globalFunc.logger("returned empty data.");
            } else {
                //this._globalFunc.logger("master service data found.");
                for (var k = 0; k < data.storeUIEntryConfig.length; k++) {
                   // console.log(this._globalFunc.titleCase(data.storeUIEntryConfig[k].catalogueCode));
                    //this.amazonRegionCodesData[k] = data.storeUIEntryConfig[k].catalogueCode;
                     data.storeUIEntryConfig[k].catalogueType = this._globalFunc.titleCase(data.storeUIEntryConfig[k].catalogueType);
                   
                   
                }
                //this.cataloguemasterListLength = data.storeUIEntryConfig.length;
            }
            console.log(data.storeUIEntryConfig);
            this.catalogueTypeMaster = data.storeUIEntryConfig;
        }
            , (err:any) => {
                this._globalFunc.logger("catalogue config service failed.");
                //this.cataloguemasterListLength = 0;
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
        );
    }
}