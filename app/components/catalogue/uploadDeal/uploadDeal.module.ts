import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadrangeComponent } from './uploadDeal.component';
import { UploadDealRoutes } from './uploadDeal.routes';
import { CatalogueManagePendingHeaderModule } from '../../cataloguemanagement/cataloguemanagependingheader/cataloguemanagependingheader.module';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';

import { InfiniteScrollModule } from 'angular2-infinite-scroll';

@NgModule({
  imports: [
    UploadDealRoutes,
    CommonModule,
    CatalogueManagePendingHeaderModule,
    InfiniteScrollModule,
    NavigationModule
  ],
  declarations: [UploadrangeComponent]
})
export class UploadDealModule { }
