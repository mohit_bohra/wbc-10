import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { UploadrangeComponent } from './uploadDeal.component';

// route Configuration
const uploadDealRoutes: Routes = [
  { path: '', component: UploadrangeComponent},

];


@NgModule({
  imports: [RouterModule.forChild(uploadDealRoutes)],
  exports: [RouterModule]
})

export class UploadDealRoutes{}