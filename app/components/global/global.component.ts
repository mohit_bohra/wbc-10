import { Services } from '../../services/app.services';
// import { Router, ActivatedRoute } from '@angular/router';
import { Constant } from '../constants/constant';

declare let saveAs: any;
export class GlobalComponent {

    // global variables

    public stdWidth: number = 768;
    public elementId: string;
    public customerName4Catalogue: string;
    public loggerArray: any = [];

    objConstant = new Constant();
    private _postsService: Services;

    //constructor(private _postsService: Services) { }

    // get device from width
    public getDevice(str: number) {
        var screen: string;
        if (str < this.stdWidth) {
            screen = "mobile";
            document.getElementById('nav-toggle').style.display = 'block';
        } else {
            screen = "desktop";
            document.getElementById('nav-toggle').style.display = 'none';
        }
        return screen;
    }

    formatSelection(val: any) {
        var length = 30;
        // shipToLocationId(GLN999999), billToId(12)
        var trimmedString;
       // console.log(val);
       // console.log(val.length);
        if (val.length > length) {
            let appendStr = '...';
            trimmedString = val.substring(0, length) + appendStr;
        }
        else {
            trimmedString = val.substring(0, length);
        }
        return trimmedString;
    }


    // export data
    exportData(exportable: any, filename: any) {      
        var blob = new Blob([document.getElementById(exportable).innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, filename + ".xls");
    }



    // price formating function
    public getPriceformat(str: string) {
        var parts = new Array;
        var output = new Array;
        var i = 1;

        if (str.indexOf('.') > 0) {
            parts = str.split('.');
            str = parts[0];
        } else {
            str = str + '.00';
            parts = str.split('.');
            str = parts[0];

        }
        str = str.split('').reverse().toString();
        for (var j = 0, len = str.length; j < len; j++) {
            if (str[j] !== ',') {
                output.push(str[j]);
                if (i % 3 === 0 && j < (len - 1)) {
                    output.push(',');
                }
                i++;
            }
        }

        if (parts[1].length === 1) {
            parts[1] = parts[1] + '0';
        }
        var formatted = output.reverse().join('');
        var formated_output = (formatted + ((parts) ? '.' + parts[1].substr(0, 2) : '.00'));
        /* if (parts[0] < 10) {
                 formated_output  = '0' + formated_output;
              } */
        return formated_output;

    }

    // to set on top of page
    autoscroll() {
        var scrollToTop = window.setInterval(function () {
            var pos = window.pageYOffset;
            if (pos > 0) {
                window.scrollTo(0, pos - 200000000); // how far to scroll on each step
            } else {
                window.clearInterval(scrollToTop);
            }
        }, 1); // how fast to scroll (this equals roughly 60 fps)
    }

    autoscrolldown() {
        var pos = window.pageYOffset;
        if (pos > 0) {
            var h = window.innerHeight;
            window.scrollTo(h, pos + 200000000); // how far to scroll on each step
        }
    }

    // accept only alphanumeric
    alphaNumeric(event: any) {


        const pattern = /^[a-zA-Z0-9-]+|[\b]+$/;
        let inputChar = String.fromCharCode(event.charCode);
        console.log(event.charCode);
        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            if (event.charCode != 0)
                event.preventDefault();
        }
    }

    // accept only alphanumeric with space
    alphaNumericWithSpace(event: any) {


        const pattern = /^[a-zA-Z0-9\s._]+|[\b]+$/;
        let inputChar = String.fromCharCode(event.charCode);
        console.log(event.charCode);
        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            if (event.charCode != 0)
                event.preventDefault();
        }
    }

    numericWithDecimal(event: any) {
        const pattern = /^\d+(\.\d{0,2})?$/;
        let inputChar = String.fromCharCode(event.charCode);
        console.log(event.charCode);
        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            if (event.charCode != 46)
                if (event.charCode != 0)
                    event.preventDefault();
        }
    }

    // Accept amount with 2 digit after decimal. Characters rejected.
    validateAmount(event: any, val: any, elementId: string) {
        console.log(elementId + val);
        val = (<HTMLInputElement>document.getElementById(elementId)).value;
        console.log(val);
        if (parseFloat(val) == val) {
            //float value
            var valArr = val.split('.');
            if (valArr.length > 2 || (typeof valArr[1] != 'undefined' && valArr[1].length > 2) || valArr[0] == "") {
                val = val.substr(0, val.length - 1);
                val = parseFloat(val).toFixed(2);
                (<HTMLInputElement>document.getElementById(elementId)).value = val;
            } else {
                if (parseFloat(val) != val) {
                    val = val.substr(0, val.length - 1);
                    (<HTMLInputElement>document.getElementById(elementId)).value = val;
                }
            }
        } else {
            val = val.substr(0, val.length - 1);
            (<HTMLInputElement>document.getElementById(elementId)).value = val;
        }
    }

    // Accept percentage with 2 digit after decimal. Max 100 accepted.
    validatePercentage(event: any, val: any, elementId: string, ) {
        console.log(elementId + val);
        val = (<HTMLInputElement>document.getElementById(elementId)).value;
        var valuetxt;
        console.log(val);
        if (val == '') {
            console.log("11");
            valuetxt = true;
        } else if (parseFloat(val) == val) {
            console.log("22");
            valuetxt = false;
        } else {
            console.log("33");
            valuetxt = true;
        }
        if (parseFloat(val) == val) {
            //float value
            if (val > 100) {
                val = val.substr(0, val.length - 1);
                (<HTMLInputElement>document.getElementById(elementId)).value = val;
            } else {
                var valArr = val.split('.');
                if (valArr.length > 2 || (typeof valArr[1] != 'undefined' && valArr[1].length > 2)) {
                    val = val.substr(0, val.length - 1);
                    val = parseFloat(val).toFixed(2);
                    (<HTMLInputElement>document.getElementById(elementId)).value = val;
                }
            }
        } else {
            val = val.substr(0, val.length - 1);
            (<HTMLInputElement>document.getElementById(elementId)).value = val;
        }
        //return valuetxt;
    }

    checkPermissionAvailableForCustomer(permObj: any){
        if (permObj.catalogueEnquiry == 'none' && 
            permObj.catalogueManagement == 'none' &&
            permObj.orders == 'none' &&
            permObj.reports == 'none' &&
            permObj.accessMgmt == 'none' &&
            permObj.dashboards == 'none') {
            return false;
        } else {
            return true;
        }
    }

    logger(msg:string){
        switch(this.objConstant.sentLogger){
            case "Console":
                console.log(msg);
                break;
            case "Server":
                this.loggerArray.push({"m": msg, "t": new Date().getTime()});
                break;
            case "Both":
                console.log(msg);
                this.loggerArray.push({"m": msg, "t": new Date().getTime()});
                break;
            default:
                break;
        }
    }

    sentLoggerBatchToServer(){
        console.log(this.loggerArray);
        
        /*this._postsService.logger2Server(this.loggerArray).subscribe(data => {

        });*/
        //this.loggerArray = [];
    }

    getCustomerImage(custName:string){
        let img = "";
        // This is for images only
        //console.log(custName);

        let dummArr = JSON.parse(sessionStorage.getItem('assignedIconList'));

        dummArr.forEach(function (orgItem: any) {
            if(orgItem.customerName == custName){
                img = orgItem.image;
            }
            // switch(orgItem.customerName.toLowerCase()){
            //     case "AMAZON":
            //         img = "amazon.png";
            //         break;
            //     case "RONTEC":
            //         img = "rontec.png";
            //         break;
            //     case "SANDPIPER":
            //         img = "sandpiper.png";
            //         break;
            //     case "MCCOLLS":
            //         img = "mccolls.png";
            //         break;
            //     default:
            //         img = custName.toLowerCase()+".png";
            //         break;
            // }
        });
        
        return img;
    }

    redirectToComponent(page:string, orgType:string):any{
        let perms = JSON.parse(sessionStorage.getItem('tempPermissionArray'));
        let returnArray:any = [];
        console.log("red : "+page);
        switch(page){
            case "orders":
                if (typeof perms[orgType]['orders'] !== 'undefined' && perms[orgType]['orders'].indexOf('orders') != -1) {
                    returnArray = ['orders', orgType];
                } else if (typeof perms[orgType]['reports'] !== 'undefined' && perms[orgType]['reports'].indexOf('reports') != -1) {
                    returnArray = ['reports', orgType];
                } else if (typeof perms[orgType]['dashboards'] !== 'undefined' && (perms[orgType]['dashboards'].indexOf('dashboards') != -1)) {
                    returnArray = ['dashboard', orgType];
                } else if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueManagement') != -1) {
                    returnArray = ['cataloguemanagement', orgType];
                } else {
                    returnArray = ['customerselection'];
                }
                break;
            case "orderEnquiry":
                if (typeof perms[orgType]['orders'] !== 'undefined' && perms[orgType]['orders'].indexOf('orders') != -1 && perms[orgType]['orders'].indexOf('orderEnquiry') != -1) {
                    console.log("red 1");
                    returnArray = ['orders', orgType, 'enquiry'];
                } else if (typeof perms[orgType]['orders'] !== 'undefined' && perms[orgType]['orders'].indexOf('orders') != -1) {
                    console.log("red 2");
                    returnArray = ['orders', orgType];
                } else if (typeof perms[orgType]['reports'] !== 'undefined' && perms[orgType]['reports'].indexOf('reports') != -1) {
                    console.log("red 3");
                    returnArray = ['reports', orgType];
                } else if (typeof perms[orgType]['dashboards'] !== 'undefined' && (perms[orgType]['dashboards'].indexOf('dashboards') != -1)) {
                    console.log("red 4");
                    returnArray = ['dashboard', orgType];
                } else if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueManagement') != -1) {
                    console.log("red 5");
                    returnArray = ['cataloguemanagement', orgType];
                } else {
                    console.log("red 6");
                    returnArray = ['customerselection'];
                }
                break;
            case "reports":
                if (typeof perms[orgType]['reports'] !== 'undefined' && perms[orgType]['reports'].indexOf('reports') != -1) {
                    returnArray = ['reports', orgType];
                } else if (typeof perms[orgType]['dashboards'] !== 'undefined' && (perms[orgType]['dashboards'].indexOf('dashboards') != -1)) {
                    returnArray = ['dashboard', orgType];
                } else if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueManagement') != -1) {
                    returnArray = ['cataloguemanagement', orgType];
                } else if (typeof perms[orgType]['orders'] !== 'undefined' && perms[orgType]['orders'].indexOf('orders') != -1) {
                    returnArray = ['orders', orgType];
                } else {
                    returnArray = ['customerselection'];
                }
                break;
            case "dashboard":
                if (typeof perms[orgType]['dashboards'] !== 'undefined' && (perms[orgType]['dashboards'].indexOf('dashboards') != -1)) {
                    returnArray = ['dashboard', orgType];
                } else if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueManagement') != -1) {
                    returnArray = ['cataloguemanagement', orgType];
                } else if (typeof perms[orgType]['orders'] !== 'undefined' && perms[orgType]['orders'].indexOf('orders') != -1) {
                    returnArray = ['orders', orgType];
                } else if (typeof perms[orgType]['reports'] !== 'undefined' && perms[orgType]['reports'].indexOf('reports') != -1) {
                    returnArray = ['reports', orgType];
                } else {
                    returnArray = ['customerselection'];
                }
                break;
            case "fulfilment":
                if (typeof perms[orgType]['dashboards'] !== 'undefined' && (perms[orgType]['dashboards'].indexOf('dashboards') != -1)) {
                    if(perms[orgType]['dashboards'].indexOf('ordersFulfilmentDashboard')){
                        returnArray = ['dashboard', orgType,'fulfilment'];
                    } else {
                        returnArray = ['dashboard', orgType];
                    }
                } else if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueManagement') != -1) {
                    returnArray = ['cataloguemanagement', orgType];
                } else if (typeof perms[orgType]['orders'] !== 'undefined' && perms[orgType]['orders'].indexOf('orders') != -1) {
                    returnArray = ['orders', orgType];
                } else if (typeof perms[orgType]['reports'] !== 'undefined' && perms[orgType]['reports'].indexOf('reports') != -1) {
                    returnArray = ['reports', orgType];
                } else {
                    returnArray = ['customerselection'];
                }
                break;
            case "cataloguemanagement":
                if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueManagement') != -1) {
                    returnArray = ['cataloguemanagement', orgType];
                } else if (typeof perms[orgType]['orders'] !== 'undefined' && perms[orgType]['orders'].indexOf('orders') != -1) {
                    returnArray = ['orders', orgType];
                } else if (typeof perms[orgType]['reports'] !== 'undefined' && perms[orgType]['reports'].indexOf('reports') != -1) {
                    returnArray = ['reports', orgType];
                } else if (typeof perms[orgType]['dashboards'] !== 'undefined' && (perms[orgType]['dashboards'].indexOf('dashboards') != -1)) {
                    returnArray = ['dashboard', orgType];
                } else {
                    returnArray = ['customerselection'];
                }
                break;
            case "catalogueEnquiry":
                if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueEnquiry') != -1) {
                        returnArray = ['catalogue', orgType];
                } else if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueManagement') != -1) {
                    returnArray = ['cataloguemanagement', orgType];
                } else if (typeof perms[orgType]['orders'] !== 'undefined' && perms[orgType]['orders'].indexOf('orders') != -1) {
                    returnArray = ['orders', orgType];
                } else if (typeof perms[orgType]['reports'] !== 'undefined' && perms[orgType]['reports'].indexOf('reports') != -1) {
                    returnArray = ['reports', orgType];
                } else if (typeof perms[orgType]['dashboards'] !== 'undefined' && (perms[orgType]['dashboards'].indexOf('dashboards') != -1)) {
                    returnArray = ['dashboard', orgType];
                } else {
                    returnArray = ['customerselection'];
                }
                break;
            case "maintainSupplyDepot":
                if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('maintainSupplyingDepots') != -1) {
                        returnArray = ['cataloguemanagement', orgType, 'maintainsupplydepot'];
                } else if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueManagement') != -1) {
                    returnArray = ['cataloguemanagement', orgType];
                } else if (typeof perms[orgType]['orders'] !== 'undefined' && perms[orgType]['orders'].indexOf('orders') != -1) {
                    returnArray = ['orders', orgType];
                } else if (typeof perms[orgType]['reports'] !== 'undefined' && perms[orgType]['reports'].indexOf('reports') != -1) {
                    returnArray = ['reports', orgType];
                } else if (typeof perms[orgType]['dashboards'] !== 'undefined' && (perms[orgType]['dashboards'].indexOf('dashboards') != -1)) {
                    returnArray = ['dashboard', orgType];
                } else {
                    returnArray = ['customerselection'];
                }
                break;
            
            case "catalogueupload":
                if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueBulkUploadView') != -1) {
                    returnArray = ['catalogue', orgType, 'upload'];
                } else if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueManagement') != -1) {
                    returnArray = ['cataloguemanagement', orgType];
                } else if (typeof perms[orgType]['orders'] !== 'undefined' && perms[orgType]['orders'].indexOf('orders') != -1) {
                    returnArray = ['orders', orgType];
                } else if (typeof perms[orgType]['reports'] !== 'undefined' && perms[orgType]['reports'].indexOf('reports') != -1) {
                    returnArray = ['reports', orgType];
                } else if (typeof perms[orgType]['dashboards'] !== 'undefined' && (perms[orgType]['dashboards'].indexOf('dashboards') != -1)) {
                    returnArray = ['dashboard', orgType];
                } else {
                    returnArray = ['customerselection'];
                }
                break;

            case "catalogueExport":
                if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueBulkExportView') != -1) {
                        returnArray = ['catalogue', orgType, 'export'];
                } else if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueManagement') != -1) {
                    returnArray = ['cataloguemanagement', orgType];
                } else if (typeof perms[orgType]['orders'] !== 'undefined' && perms[orgType]['orders'].indexOf('orders') != -1) {
                    returnArray = ['orders', orgType];
                } else if (typeof perms[orgType]['reports'] !== 'undefined' && perms[orgType]['reports'].indexOf('reports') != -1) {
                    returnArray = ['reports', orgType];
                } else if (typeof perms[orgType]['dashboards'] !== 'undefined' && (perms[orgType]['dashboards'].indexOf('dashboards') != -1)) {
                    returnArray = ['dashboard', orgType];
                } else {
                    returnArray = ['customerselection'];
                }
                break;
                
            case "storebulkupload":
                if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('storeBulkUploadView') != -1) {
                        returnArray = ['catalogue', orgType, 'storebulkupload'];
                } else if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueManagement') != -1) {
                    returnArray = ['cataloguemanagement', orgType];
                } else if (typeof perms[orgType]['orders'] !== 'undefined' && perms[orgType]['orders'].indexOf('orders') != -1) {
                    returnArray = ['orders', orgType];
                } else if (typeof perms[orgType]['reports'] !== 'undefined' && perms[orgType]['reports'].indexOf('reports') != -1) {
                    returnArray = ['reports', orgType];
                } else if (typeof perms[orgType]['dashboards'] !== 'undefined' && (perms[orgType]['dashboards'].indexOf('dashboards') != -1)) {
                    returnArray = ['dashboard', orgType];
                } else {
                    returnArray = ['customerselection'];
                }
                break;
            case "storeExport":
                if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('storeBulkExportView') != -1) {
                        returnArray = ['store', orgType, 'export'];
                } else if (typeof perms[orgType]['catalogueManagement'] !== 'undefined' && perms[orgType]['catalogueManagement'].indexOf('catalogueManagement') != -1) {
                    returnArray = ['cataloguemanagement', orgType];
                } else if (typeof perms[orgType]['orders'] !== 'undefined' && perms[orgType]['orders'].indexOf('orders') != -1) {
                    returnArray = ['orders', orgType];
                } else if (typeof perms[orgType]['reports'] !== 'undefined' && perms[orgType]['reports'].indexOf('reports') != -1) {
                    returnArray = ['reports', orgType];
                } else if (typeof perms[orgType]['dashboards'] !== 'undefined' && (perms[orgType]['dashboards'].indexOf('dashboards') != -1)) {
                    returnArray = ['dashboard', orgType];
                } else {
                    returnArray = ['customerselection'];
                }
                break;
        }
        return returnArray;
    }

    titleCase(input:string){
        
        return input.replace(/([^\W_]+[^\s-]*) */g, (txt => txt[0].toUpperCase() + txt.substr(1).toLowerCase() ));
    }
    
}
