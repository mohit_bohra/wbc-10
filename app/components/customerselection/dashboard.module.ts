import { NgModule } from '@angular/core';
import { DashboardComponentRoutes } from './dashboard.routes';
import { DashboardComponent } from './dashboard.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../global/global.component';
import { Services } from '../../services/app.services';
import { NavigationModule } from '../navigationFulfilment/navigation.module';
import { CustomerOrderBy } from './customer.orderby.pipe';


@NgModule({
  imports: [
    DashboardComponentRoutes,
    CommonModule,
    NavigationModule
  ],
  exports: [],
  declarations: [DashboardComponent,CustomerOrderBy],
  providers: [Services, GlobalComponent]
})
export class DashboardModule { }
