import { Component, ElementRef, Renderer } from '@angular/core';
// import { Services } from '../../services/app.services';
import { GlobalComponent } from '../global/global.component';
import { Constant } from '../constants/constant';
import { Services } from '../../services/app.services';
import { Router } from '@angular/router';
import { commonfunction } from '../../services/commonfunction.services';

@Component({
    selector: 'dashboard',
    templateUrl: 'dashboard.component.html',
    providers: [commonfunction]
})
export class DashboardComponent {
    // globalListenFunc will hold the function returned by "renderer.listenGlobal"
    globalListenFunc: Function;

    objConstant = new Constant();
    whoImgUrl: any;
    organisationList: any = [];
    showLoader: boolean = false;
    pageState: number = 1;
    allCustPermFalse: boolean = true;
    msgJSON: any;
    iconList: any = [];
    custPermission: any[];

    assignedIconList: any = [];
    curCustomer: string = "";
    tempPermissionArray: any = [];

    siteMaintanceFlg1: boolean;
    siteMaintanceFlg: boolean = false;
    siteMSg: string;

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _router: Router, elementRef: ElementRef, renderer: Renderer, private _commFuc: commonfunction) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        if (sessionStorage.getItem('name') !== null && sessionStorage.getItem('name') !== 'undefined') {
            let timeoutId = setTimeout(() => {
                if (sessionStorage.getItem('pageState') !== '0') {
                    sessionStorage.setItem('timeoutState', '1');
                    let sessionModal = document.getElementById('timeoutModal');
                    console.log("a1");
                    sessionModal.classList.remove('out');
                    sessionModal.classList.add('in');
                    document.getElementById('timeoutModal').style.display = 'block';
                    setTimeout(() => {
                        this.timeOutNoReaction();
                    }, this.objConstant.WARNING_TIMEOUT);
                }
            }, this.objConstant.SESSION_TIMEOUT);
            // We cache the function "listenGlobal" returns
            this.globalListenFunc = renderer.listenGlobal('document', 'click', (event: any) => {
                // Do something with 'event'
                clearTimeout(timeoutId);
                timeoutId = setTimeout(() => {
                    if (sessionStorage.getItem('pageState') !== '0') {
                        sessionStorage.setItem('timeoutState', '1');
                        let sessionModal = document.getElementById('timeoutModal');
                        console.log("a2");
                        sessionModal.classList.remove('out');
                        sessionModal.classList.add('in');
                        document.getElementById('timeoutModal').style.display = 'block';
                        setTimeout(() => {
                            this.timeOutNoReaction();
                        }, this.objConstant.WARNING_TIMEOUT);
                    }
                }, this.objConstant.SESSION_TIMEOUT);
            });

            this.globalListenFunc = renderer.listenGlobal('document', 'keypress', (event: any) => {
                // Do something with 'event'
                clearTimeout(timeoutId);
                timeoutId = setTimeout(() => {
                    if (sessionStorage.getItem('pageState') !== '0') {
                        sessionStorage.setItem('timeoutState', '1');
                        let sessionModal = document.getElementById('timeoutModal');
                        console.log("a3");
                        sessionModal.classList.remove('out');
                        sessionModal.classList.add('in');
                        document.getElementById('timeoutModal').style.display = 'block';
                        setTimeout(() => {
                            this.timeOutNoReaction();
                        }, this.objConstant.WARNING_TIMEOUT);

                    }

                }, this.objConstant.SESSION_TIMEOUT);
            });
        }
    }

    getConfigCutomerImgs() {

        return new Promise((resolve, reject) => {
            this._postsService.getConfigCutomerImgs('all').subscribe((data: any) => {
                console.log(data);
                this.iconList = data.customerConfigDetails;
                resolve('validIcons');
            }, (error: any) => {
                console.log(error);
                resolve('inValid');
            });
        });
    }

    // function works on page load
    ngOnInit() {
        this._globalFunc.logger("Loading Customer Selection on UI");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._globalFunc.logger("User Session Not Found");
            this._router.navigate(['']);
        } else {
            this.showLoader = true;
            
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            this.getUserSession('default', sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
                if (res == 'sessionActive') {
                    this.getConfigCutomerImgs().then(res => {
                        if (res == 'validIcons') {
                            console.log("this.organisationList123");
                            console.log(this.organisationList);
                            console.log(this.iconList);

                            var dummArr: any = [];
                            let that = this;

                            this.organisationList.forEach(function (orgItem: any) {
                                console.log(orgItem.customerName);
                                that.iconList.forEach(function (iconItem: any) {
                                    console.log(iconItem.customerName);
                                    if (orgItem.customerName == iconItem.customerName) {
                                        //dummArr[orgItem.customerName] = iconItem.customerIconList[0].iconPath;
                                        if (iconItem.customerIconList != undefined) {
                                            dummArr.push({ 'customerName': orgItem.customerName, 'image': iconItem.customerIconList[0].iconPath })
                                        } else {
                                            dummArr.push({ 'customerName': orgItem.customerName, 'image': '' })
                                        }
                                    }
                                });
                            });
                            console.log("dummArr");
                            console.log(dummArr);
                            this.assignedIconList = dummArr;
                            sessionStorage.setItem('assignedIconList', JSON.stringify(dummArr));

                            if (this.assignedIconList.length < 1) {
                                this.showLoader = false;
                            } else {
                                let path: string = window.location.hash;
                                if (path && path.length > 0) {
                                    this._router.navigate([path.substr(2)]);
                                }

                                // window scroll function
                                this._globalFunc.autoscroll();
                                this.pageState = 1;
                                sessionStorage.setItem('pageState', '1');
                                document.getElementById('login').style.display = 'inline';
                                // initial page width
                                let wid: any;
                                wid = window.innerWidth;
                                let screen: string;

                                // get device to populate some part of ui
                                screen = this._globalFunc.getDevice(wid);

                                window.onresize = () => {
                                    // get device to populate some part of ui
                                    screen = this._globalFunc.getDevice(window.innerWidth);


                                    //this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));
                                    let custPerm: any = [];
                                    let allFalse = true;
                                    document.getElementById('orderMob').style.display = 'none';
                                    document.getElementById('orderRaiseMob').style.display = 'none';
                                    document.getElementById('catalogueMob').style.display = 'none';
                                    document.getElementById('catalogueRaiseMob').style.display = 'none';
                                    document.getElementById('reportsMob').style.display = 'none';
                                    document.getElementById('permissonsMob').style.display = 'none';
                                    // alert(this.organisationList.length);
                                    /*if (this.organisationList.length == 1 || this.organisationList.length == '1') {
                                        //If there is only on customer
                                        this.redirect('/orders', this.organisationList[0].customerName);
                                    }*/

                                    if (this.organisationList.length == 1){
                                        this.redirectToComponent(this.curCustomer);
                                    }
                                }
                            }
                        }


                    }, reason => {
                        console.log(reason);
                    });
                    this.showLoader = false;
                }
            }, reason => {
                console.log(reason);
            });
        }

        //this.siteMaintanceFlg1 = sessionStorage.getItem('site');
        // if(sessionStorage.getItem('site') == "true"){
            
        //     this.siteMaintanceFlg1 = false;
        // }else { 
        //     this.siteMSg = "Site is under maintenance";
        //     this.siteMaintanceFlg1 = true;
        // }
    }

    timeOutNoReaction() {
        if (sessionStorage.getItem('timeoutState') == '1') {
            sessionStorage.setItem('timeoutState', '0');
            let sessionModal = document.getElementById('timeoutModal');
            console.log("a4");
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('timeoutModal').style.display = 'none';
            sessionStorage.clear();
            document.getElementById('login').style.display = 'none';
            this._router.navigate(['']);
        }
    }

    // navigate function
    redirect(page: any, orgType: any) {
        try {
            this._globalFunc.logger("Redirecting to " + page + " - " + orgType);
            this._router.navigate([page, orgType]);
        } catch (ex) {
            this._globalFunc.logger("Exception occured on redirection - " + ex.name + " : " + ex.message);
            throw (ex);
        }
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
        this._globalFunc.logger("Unloading Customer selection screen.");
        this._globalFunc.sentLoggerBatchToServer();
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {

            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        reject("sessionInactive");
                        console.log("Session terminated");
                        this.msgJSON = {
                            "width314": true,
                            "title": "Session terminated",
                            "message": userData.sessionMessage,
                            "okButton": true,
                            "okButtonLabel": "OK",
                            "cancelButton": false,
                            "cancelButtonLabel": "",
                            "showPopup": true
                        };

                        let dt = this._commFuc.getUserSession();
                        this._postsService.authLoginTracker(customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
                            console.log(userData);
                        }, (error: any) => {
                            console.log(error);
                        });

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        console.log("a5");
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else {
                        reject("sessionInactive");
                        sessionStorage.clear();
                        this._router.navigate(['']);
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        reject("sessionInactive");
                        console.log("Session terminated");
                        this.msgJSON = {
                            "width314": true,
                            "title": "Session terminated",
                            "message": userData.sessionMessage,
                            "okButton": true,
                            "okButtonLabel": "OK",
                            "cancelButton": false,
                            "cancelButtonLabel": "",
                            "showPopup": true
                        };

                        let dt = this._commFuc.getUserSession();
                        this._postsService.authLoginTracker(customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
                            console.log(userData);
                        }, (error: any) => {
                            console.log(error);
                        });

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        console.log("a6");
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        resolve("sessionActive");
                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;

                        if (this.organisationList == undefined) {
                            this.organisationList = [];
                        }

                        this.simplifyPermissionJSON(this.organisationList, customerName);

                        console.log(this.organisationList.length);
                        sessionStorage.setItem('groupId4Users', userData.userPermissionJson.groupId);
                        sessionStorage.setItem('groupname4User', userData.userPermissionJson.groupName);
                        sessionStorage.setItem('loggedInUserGroupId', userData.userPermissionJson.groupId);
                        sessionStorage.setItem('loggedInUserGroupName', userData.userPermissionJson.groupName);
                        sessionStorage.setItem('loggedInUserAdminFlag', userData.userPermissionJson.credentials.adminFlag);

                        if (this.organisationList.length == 1) {
                            //If there is only on customer
                            // this.redirect('/orders', this.organisationList[0].customerName);
                        } else if (this.organisationList.length == 0) {
                            console.log(userData.userPermissionJson.groupName + "==" + this.objConstant.SUPERADMIN_GROUP);
                            console.log(userData.userPermissionJson.credentials.adminFlag);
                            if ((userData.userPermissionJson.groupName == this.objConstant.SUPERADMIN_GROUP) || userData.userPermissionJson.credentials.adminFlag) {
                               this.redirect('/groups', 'mccolls');
                            } else {
                                this._globalFunc.logger("Permission is not given for any customer to user.");
                                // User doesnot have access to any customer
                            }
                        }
                    }
                }
            });
        });
    }

    getUserLandingPage(customerName: string, loggedInUserGroup: string, loggedInUserId: any, currentScreen: string = "", nextScreen: string = "") {
        return new Promise((resolve, reject) => {
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.
                    reject("sessionInactive");
                    sessionStorage.clear();
                    this._router.navigate(['']);
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        reject("sessionTerminated");
                    } else {
                        let organisationList = userData.userPermissionJson.userRoleDetails.organizations;

                        if (organisationList == undefined) {
                            reject("NoCustomerAccess");
                        } else {
                            this.simplifyPermissionJSON(organisationList, customerName);

                            resolve("PermissionsAvailable");
                        }
                    }
                }
            });
        });
    }

    simplifyPermissionJSON(organisationList: any, customerName: string) {
        let dummyPermissionArray: any = {};
        let permRoleDtlList1: any = {};
        let permissionDetailsList: any = {};
        let permGroup: string = "";
        let tempArr: any = {};
        let tempCustName: string = "";

        this.curCustomer = customerName;
        if (this.curCustomer == 'default') {
            if (organisationList.length == 1) {
                this.curCustomer = organisationList[0].customerName;
            }
        }

        organisationList.forEach(function (organisation: any) {
            if (organisation.customerName == customerName || customerName == 'default') {
                tempCustName = organisation.customerName;
                dummyPermissionArray[tempCustName] = {};
                organisation.permissionRoleList.forEach(function (permRoleList: any) {
                    permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                        permGroup = permRoleDtlList.permissionGroup;
                        if (typeof dummyPermissionArray[tempCustName][permGroup] === 'undefined') {
                            dummyPermissionArray[tempCustName][permGroup] = [];
                        }
                        permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                            dummyPermissionArray[tempCustName][permGroup].push(permList.permissionName);
                        });
                    });
                });
            }
        });
        console.log("dummyPermissionArray");
        dummyPermissionArray as Array<any>
        dummyPermissionArray = JSON.parse(JSON.stringify(dummyPermissionArray));
        console.log(dummyPermissionArray);
        console.log(JSON.stringify(dummyPermissionArray));
        console.log(dummyPermissionArray.length);
        sessionStorage.setItem('tempPermissionArray', JSON.stringify(dummyPermissionArray));
        this.tempPermissionArray = dummyPermissionArray;
    }

    redirectToComponent(customerName:string){
        let dummyPermissionArray = this.tempPermissionArray;
        if (typeof dummyPermissionArray[customerName]['orders'] !== 'undefined' && dummyPermissionArray[customerName]['orders'].indexOf('orders')) {
            this.redirect('/orders', customerName);
        } else if (typeof dummyPermissionArray[customerName]['reports'] !== 'undefined' && dummyPermissionArray[customerName]['reports'].indexOf('reports')) {
            this.redirect('/reports', customerName);
        } else if (typeof dummyPermissionArray[customerName]['dashboards'] !== 'undefined' && dummyPermissionArray[customerName]['dashboards'].indexOf('dashboards')) {
            this.redirect('/dashboard', customerName);
        } else if (typeof dummyPermissionArray[customerName]['catalogueManagement'] !== 'undefined' && dummyPermissionArray[customerName]['catalogueManagement'].indexOf('catalogueManagement')) {
            this.redirect('/cataloguemanagement', customerName);
        }
    }

    closePopup(){
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._commFuc.getUserSession();
        this._postsService.authLoginTracker(this.curCustomer, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }
}

