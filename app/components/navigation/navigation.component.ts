import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Router, ActivatedRoute } from '@angular/router';
import { Constant } from '../constants/constant';
import { Services } from '../../services/app.services';

@Component({
    selector: 'main-navigation-menu',
    templateUrl: 'navigation.component.html',
})
export class NavigationComponent {

    customerFlag: boolean;
    objConstant = new Constant();
    whoImgUrl: string;
    orgApiName: string;
    _sub: any;
    customerName: any;
    homeUrl: any;
    loggedInUserId:any;
    loggedInUserGroupId:any;

    organisationList: any;
    organisationListChange: any;
    orgimgUrl: string;

    showPendingReq: boolean;
    donotshow: boolean = true;

    currentComponent: string = "";
    currentScreen: string = "";
    currentCustomerPermission: any;

    /********************** Properties to disable Navigation Icons - START **************************/
    enableOrder: boolean = false;
    enablecatalogueEnquiry: boolean = false;
    enablecatalogueManagement: boolean = false;
    enablereports: boolean = false;
    enabledashboards: boolean = false;
    enableaccessMgmt: boolean = false;
    enableorderMgmtClaims: boolean = false;

    /** Order Specific NAvigation */
    enableraiseNewOrder: boolean = false;

    /* Catalogue specific navigation */
    enablecatalogueAddNewProducts: boolean = false;
    enablecatalogueUploadCatalogue: boolean = false;
    enablecatalogueExportCatalogue: boolean = false;

    /* Dashboard specific navigation */
    enablefilfilment: boolean = false;

    /********************** Properties to disable Navigation Icons - END ****************************/

    loggedInUserGroup:string = "";
    loggedInUserAdminFlag:string = "";
    superAdminGroup:string = "";

    constructor(private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, private _postsService: Services) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.superAdminGroup = this.objConstant.SUPERADMIN_GROUP;
    }

    // function works on page load
    ngOnInit() {
        console.log("navi");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {
            // subscribe to route params
            //this._sub = this._route.params.subscribe((params: { orgType: string }) => {
            //sessionStorage.getItem('orgType', params.orgType);

            this.customerName = sessionStorage.getItem('orgType');
            
            this.loggedInUserGroup = sessionStorage.getItem('loggedInUserGroupName');
            this.loggedInUserAdminFlag = sessionStorage.getItem('loggedInUserAdminFlag');
            this.loggedInUserId = sessionStorage.getItem('userId');
            this.loggedInUserGroupId = sessionStorage.getItem('loggedInUserGroupId');

            let cusType = sessionStorage.getItem('orgType');
            let orgImgUrl;
            let orgApiNameTemp;
            let tempPermissionArr: any = [];

            // Read current URL
            let currentUrl = this._router.url.split("/");
            this.currentComponent = currentUrl[1];
            this.currentScreen = currentUrl[currentUrl.length - 1];

            this.getUserSession(this.customerName, this.loggedInUserGroupId, this.loggedInUserId);


            // list of org assigned to user
            this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));

            this.organisationList.forEach(function (item: any) {
                if (item.orgType === cusType) {
                    tempPermissionArr = item;
                }
            });
            this.currentCustomerPermission = tempPermissionArr;

            tempPermissionArr = null;

            /**************************** This are the primary navigation - Start *****************************/
            if ((this.currentCustomerPermission.permissions.menuPermission.orders).toLowerCase() != 'none') {
                this.enableOrder = true;
                /* document.getElementById('orderMob').style.display = 'none';
                     document.getElementById('orderRaiseMob').style.display = 'none';*/
            } else {
                this.enableOrder = false;
            }

            if ((this.currentCustomerPermission.permissions.menuPermission.catalogueEnquiry).toLowerCase() != 'none') {
                this.enablecatalogueEnquiry = true;
                /* document.getElementById('orderMob').style.display = 'none';
                     document.getElementById('orderRaiseMob').style.display = 'none';*/
            } else {
                this.enablecatalogueEnquiry = false;
            }

            if ((this.currentCustomerPermission.permissions.menuPermission.catalogueManagement).toLowerCase() != 'none') {
                this.enablecatalogueManagement = true;
                /* document.getElementById('orderMob').style.display = 'none';
                     document.getElementById('orderRaiseMob').style.display = 'none';*/
            } else {
                this.enablecatalogueManagement = false;
            }

            if ((this.currentCustomerPermission.permissions.menuPermission.reports).toLowerCase() != 'none') {
                this.enablereports = true;
                /* document.getElementById('orderMob').style.display = 'none';
                     document.getElementById('orderRaiseMob').style.display = 'none';*/
            } else {
                this.enablereports = false;
            }

            if ((this.currentCustomerPermission.permissions.menuPermission.dashboards).toLowerCase() != 'none') {
                this.enabledashboards = true;
                /* document.getElementById('orderMob').style.display = 'none';
                     document.getElementById('orderRaiseMob').style.display = 'none';*/
            } else {
                this.enabledashboards = false;
            }

            if (this.currentCustomerPermission.permissions.menuPermission.orderMgmtClaims && (this.currentCustomerPermission.permissions.menuPermission.orderMgmtClaims).toLowerCase() != 'none') {
                this.enableorderMgmtClaims = true;
                /* document.getElementById('orderMob').style.display = 'none';
                     document.getElementById('orderRaiseMob').style.display = 'none';*/
            } else {
                this.enableorderMgmtClaims = false;
            }
            console.log(this.currentCustomerPermission.permissions.menuPermission.accessMgmt);
            if ((this.currentCustomerPermission.permissions.menuPermission.accessMgmt).toLowerCase() != 'none') {
                if(this.loggedInUserGroup == this.superAdminGroup) {
                  
                    this.enableaccessMgmt = true;
                } else if(this.loggedInUserGroup != this.superAdminGroup) {
                    if(this.loggedInUserAdminFlag == 'true') {
                        this.enableaccessMgmt = true;
                    } else {
                        this.enableaccessMgmt = false;
                    }
                } else {
                    this.enableaccessMgmt = false;
                }
                /* document.getElementById('orderMob').style.display = 'none';
                     document.getElementById('orderRaiseMob').style.display = 'none';*/
            } else {
                this.enableaccessMgmt = false;
            }
            /**************************** This are the primary navigation - END *******************************/
            this.showScreenDependentNavigation();
            //});
        }
    }

    showScreenDependentNavigation() {
        switch (this.currentComponent) {
            case "orders":
                this.enableOrderDependentNavigation();
                break;
            case "catalogue":
                this.enableCatalogueDependentNavigation();
                break;
            case "cataloguemanagement":
                this.enableCatalogueMAnagementDependentNavigation();
                break;
            case "fulrangeexport":
                break;
            case "fullrangeexport":
                break;
            case "reports":
                this.enablereports = false;
                break;
            case "dashboard":
                this.enableDashboardDependentNavigation();
                break;
            case "groups":
                this.enableaccessMgmt = false;
                break;
        }
    }

    enableDashboardDependentNavigation() {
        this.enabledashboards = false;
        if ((this.currentCustomerPermission.permissions.dashboardsPermission.fulfilment).toLowerCase() != 'none') {
            this.enablefilfilment = true;
        } else {
            this.enablefilfilment = false;
        }

        // Disable current navigation icon
        let currentUrl = this._router.url.split("/");

        if (currentUrl[4] == 'levelone' || currentUrl[4] == 'leveltwo') {
            this.enabledashboards = true;
        } else {
            switch (this.currentScreen) {
                case "fulfilment":
                case "levelone":
                    this.enabledashboards = true;
                    break;

            }
        }

    }

    enableCatalogueMAnagementDependentNavigation() {
        console.log("Length : " + this._router.url.split("/").length);
        if (this._router.url.split("/").length > 3) {
            this.enablecatalogueManagement = true;
        } else {
            this.enablecatalogueManagement = false;
        }
    }

    enableOrderDependentNavigation() {
        this.enableOrder = false;
        if ((this.currentCustomerPermission.permissions.menuPermission.raiseNewOrder).toLowerCase() != 'none') {
            this.enableraiseNewOrder = true;
            /* document.getElementById('orderArrowSpan').style.display = 'none';
            document.getElementById('orderRaiseMob').style.display = 'none'; */
        } else {
            this.enableraiseNewOrder = false;
        }

        switch (this.currentScreen) {
            case "claims":
                this.enableorderMgmtClaims = false;
                this.enableOrder = true;
                break;
        }
    }

    enableCatalogueDependentNavigation() {
        this.enablecatalogueEnquiry = false;
        // Enable all functionality specific navigation icon.
        if ((this.currentCustomerPermission.permissions.catalogAdminPermission.addNewProducts).toLowerCase() != 'none') {
            this.enablecatalogueAddNewProducts = true;
            /* document.getElementById('orderArrowSpan').style.display = 'none';
            document.getElementById('orderRaiseMob').style.display = 'none'; */
        } else {
            this.enablecatalogueAddNewProducts = false;
        }
        if ((this.currentCustomerPermission.permissions.catalogAdminPermission.uploadCatalogue).toLowerCase() != 'none') {
            this.enablecatalogueUploadCatalogue = true;
            /* document.getElementById('orderArrowSpan').style.display = 'none';
            document.getElementById('orderRaiseMob').style.display = 'none'; */
        } else {
            this.enablecatalogueUploadCatalogue = false;
        }
        if ((this.currentCustomerPermission.permissions.catalogAdminPermission.exportCatalogue).toLowerCase() != 'none') {
            this.enablecatalogueExportCatalogue = true;
            /* document.getElementById('orderArrowSpan').style.display = 'none';
            document.getElementById('orderRaiseMob').style.display = 'none'; */
        } else {
            this.enablecatalogueExportCatalogue = false;
        }

        // Disable current navigation icon
        
        switch (this.currentScreen) {
            case "add":
                this.enablecatalogueAddNewProducts = false;
                this.enablecatalogueEnquiry = true;
                break;
            case "upload":
                this.enablecatalogueUploadCatalogue = false;
                this.enablecatalogueEnquiry = true;
                this.enablecatalogueAddNewProducts = false;
                break;
            case "export":
                this.enablecatalogueExportCatalogue = false;
                this.enablecatalogueEnquiry = true;
                this.enablecatalogueAddNewProducts = false;
                break;
            case "fullrangeupload":
                this.enablecatalogueEnquiry = true;
                this.enablecatalogueAddNewProducts = false;
        }
    }

    // navigate function
    redirect(urlArr: any[], newTab: boolean = false) {
        let urlAr = [urlArr, newTab] ;
        sessionStorage.setItem("urlArr",JSON.stringify(urlAr));
        if (sessionStorage.getItem("unsaveChagne") == 'true') {
            let sessionModal = document.getElementById('unsaveModal');
            sessionModal.classList.remove('out');
            sessionModal.classList.add('in');
            document.getElementById('unsaveModal').style.display = 'block';

            setTimeout(() => {               
            }, this.objConstant.WARNING_TIMEOUT);
        } else {
            let currentCustomer = sessionStorage.getItem('orgType');
            urlArr[1] = currentCustomer;
            if (newTab) {
                console.log(urlArr.join("/"));
                let url = this.homeUrl + urlArr.join("/");
                sessionStorage.setItem("pageState","999");
                window.open(url);
            } else {
                this._router.navigate(urlArr);
            }
        }
       
    }

    getUserSession(customerName:any, loggedInUserGroup:any, loggedInUserId:any){
        this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData:any) => {
            if(!userData.isSessionActive) { // false
                // User session has expired.
                sessionStorage.clear();
                this._router.navigate(['']);
            }
        });
    }

    reloadNavigation(orgType:string){
        console.log("orgType : " + orgType);
        sessionStorage.setItem('orgType', orgType);
        this.ngOnInit();
    }
}

