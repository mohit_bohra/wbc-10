import { NgModule, ModuleWithProviders } from '@angular/core';

import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../global/global.component';
import { Services } from '../../services/app.services';
import { FormsModule } from '@angular/forms';
import { NavigationComponent } from './navigation.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [NavigationComponent],
  declarations: [NavigationComponent],
  providers: [Services, GlobalComponent]
})
export class NavigationModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NavigationModule
    }
  }
}
