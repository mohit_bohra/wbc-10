import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResetpasswordComponent } from './resetpassword.component';

// route Configuration
const resetpasswordRoutes: Routes = [
  { path: '', component: ResetpasswordComponent },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(resetpasswordRoutes)],
  exports: [RouterModule]
})
export class ResetpasswordComponentRoutes { }
