import { Component, ElementRef, Renderer } from '@angular/core';
// import { Services } from '../../services/app.services';
import { GlobalComponent } from '../global/global.component';
import { Constant } from '../constants/constant';
import { Services } from '../../services/app.services';
import { Router } from '@angular/router';
import { commonfunction } from '../../services/commonfunction.services';

@Component({
    selector: 'resetpassword',
    templateUrl: 'resetpassword.component.html',
    providers: [commonfunction]
})
export class ResetpasswordComponent {
    userName: any;
    resetPasswordMsg: string;
    showcancel: boolean = false;
    resetPwdFlag: boolean = false;
    // globalListenFunc will hold the function returned by "renderer.listenGlobal"
    globalListenFunc: Function;

    objConstant = new Constant();
    whoImgUrl: any;
    organisationList: any = [];
    showLoader: boolean = false;
    pageState: number = -1;
    allCustPermFalse: boolean = true;
    msgJSON: any;
    closePopup: any;

    custPermission: any[];
    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _router: Router, elementRef: ElementRef, renderer: Renderer, private _commFuc: commonfunction) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
    }

    // function works on page load
    ngOnInit() {
        this._globalFunc.logger("Loading Customer Reset password screen");
        document.getElementById('login').style.display = 'none';
    }

    closerestPopup(popupName: string = 'messageModal') {
        this.resetPasswordMsg = "";
        let sessionModal = document.getElementById(popupName);
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById(popupName).style.display = "none";
    }



    resetPasswordFunction(selectedUser: any, pwdFlag: any) {
        console.log(pwdFlag);
        if (pwdFlag == false) {
            this._postsService.resetPassword(selectedUser).subscribe(userData => {
                this.showcancel = false;

                this.resetPasswordMsg = "Password has been reset and email notification sent to " + selectedUser;

                this.resetPwdFlag = true;
            }, error => {
                this.showcancel = false;
                this.resetPasswordMsg = "Either enter Email Id is Invalid or Password could not be reset at this time " + selectedUser + ". Please retry.";
                this.resetPwdFlag = false;
            });
        } else {
            this.closerestPopup('resetPassword');
        }
        this.resetPwdFlag = false;

    }

    resetpassword(form: any) {

        this.showLoader = true;
        this.userName = form.username;
        this._globalFunc.logger("Calling User management API");
        this._postsService.resetPassword(form.username).subscribe(userData => {
            this.resetPasswordMsg = "Password has been reset and email notification sent to " + form.username;
            this.resetPwdFlag = true;
        }, (error: JSON) => {
            console.log("JSON.stringify(error)");
            console.log(JSON.stringify(error));
            this.resetPasswordMsg = "Either enter Email Id is Invalid or Password could not be reset at this time for " + form.username + ". Please retry.";
            this.resetPwdFlag = true;
        });

        setTimeout(function () {
            this.showLoader = false;
            let sessionModal = document.getElementById('resetPassword');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('resetPassword').style.display = "block";
        }, 4000);
        this.showLoader = false;
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
        this._globalFunc.logger("Unloading REset pass word screen.");
        this._globalFunc.sentLoggerBatchToServer();
    }

}

