import { NgModule } from '@angular/core';
import { ResetpasswordComponentRoutes } from './resetpassword.routes';
import { ResetpasswordComponent } from './resetpassword.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../global/global.component';
import { Services } from '../../services/app.services';
import { NavigationModule } from '../navigationFulfilment/navigation.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ResetpasswordComponentRoutes,
    CommonModule,
    NavigationModule,
    FormsModule
  ],
  exports: [],
  declarations: [ResetpasswordComponent],
  providers: [Services, GlobalComponent]
})
export class ResetpasswordModule { }
