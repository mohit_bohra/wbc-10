export class Constant {

    rangeDateVal: number;
    //claimUrl: string;
    rbacCustomerName: boolean;
    orderUrl: string;
    orderapiKeyType: string;
    orderApiKey: string;
    orderAuthorization: string;
    productUrl: string;
    productapiKeytype: string;
    productApiKey: string;
    productAuthorization: string;
    catalogueurl: string;
    catalogueType: string;
    catalogueapiKey: string;
    catalogueauthorization: string;
    cataloguemanagementurl: string;
    cataloguemanagementType: string;
    cataloguemanagementapiKey: string;
    cataloguemanagementauthorization: string;
    fileserviceurl: string;
    fileserviceType: string;
    fileserviceapiKey: string;
    fileserviceauthorization: string;
    priceManagementUrl: string;
    priceManagementapiKeyType: string;
    priceManagementapiKey: string;
    priceManagementauthorization: string;
    reportmanagementurl: string;
    reportmanagementType: string;
    reportmanagementapiKey: string;
    reportmanagementauthorization: string;
    configManagementurl: string;
    configManagementType: string;
    configManagementapiKey: string;
    configManagementauthorization: string;
    fulfilmenturl: string;
    fulfilmentType: string;
    fulfilmentapiKey: string;
    fulfilmentauthorization: string;
    cataloguetrackerurl: string;
    cataloguetrackerType: string;
    cataloguetrackerapiKey: string;
    cataloguetrackerauthorization: string;
    salesconfigrurl: string;
    salesconfigType: string;
    salesconfigapiKey: string;
    salesconfigauthorization: string;
    authorizationUrl: string;
    authorizationType: string;
    authorizationApiKey: string;
    authauthorization: string;
    public tactocal: boolean;
    public claimTactical: boolean = false;



    public configTactical: boolean = true;
    storePrefix: any = {
        'amazon': '',
        'rontec': 'RTC',
        'sandpiper': 'SPR',
        'mccolls': '',
        'kens': '',
        'mpk': 'MPK',
        'mcdly': 'MCD',
        'harvest': 'HAR',
        'rontec-b': 'RTB'
    }
    storePrefixMccolls = '-';
    storePrefixSandpiper = 'SPR';
    storePrefixRontec = 'RTC';
    storePrefixAmazon = 'AMZ';
    storePrefixMPK = 'MPK';
    storePrefixMCDLY = 'MCD';
    storePrefixHarvest = 'HAR';
    storePrefixRontecB = 'RTB';

    channelList: any = [
        { 'name': 'All', 'value': 'All' },
        { 'name': 'Core', 'value': 'Non-OF' },
        { 'name': 'Store Pick', 'value': 'Storepick' },
        { 'name': 'Storepick-Old', 'value': 'OF' }
    ];

    channelSubTypeList: any = [
        {'name':'Core','value':'MP8QW'},
        {'name':'Fresh','value':'PRXKD'},
        {'name':'Prime Now','value':'THSV2'},
        {'name':'Pantry','value':'THSV3'}
    ];
   supplyingDepotforAmazon: any = [
        { productCategoryId: 0, name: "ALL" },
        { productCategoryId: 1, name: "AM" },
        { productCategoryId: 2, name: "CL" },
        { productCategoryId: 3, name: "CS" },
        { productCategoryId: 4, name: "FR" },
        { productCategoryId: 5, name: "PH" },
        ];
    /* constant values */
    public NOTATION: string;
    // in milli sec
    public SESSION_TIMEOUT: number;
    public WARNING_TIMEOUT: number;
    public SCROLL_RECORD_LIMIT: number;
    /* configuration api */
    public ENVIRNMNT: string;
    public WHOIMG_URL: any;

    /*site urls          */

    public HOME_URL: string;
    public CONTACT_US: string;

    /* data apis */
    public LOGINAPI: string;
    public s3path: string;
    public s3pathBucket: string;

    /*------- Constant for Logger -----------*/
    public enableLogger: boolean = true;
    public sentLogger: string = "Both"; // Possible values Console, Server, Both
    /*------- Constant for Logger - END -----------*/
    public rbacktactical: boolean = false;
    public customerName4Service: string = 'default';

    public SUPERADMIN_GROUP: string = 'MWEE-Admin';
    public activeUserSessionMessage = "Session is Active";
    public userSessionTerminatedMsg = "Session has been terminated - please log in again.";
    public userSessionTerminatedMsgForInactiveUser = "Session is InActive";

    public storemanagementurl: string = "https://api.morrisons.com/wholesale/v1/";
    public storemanagementapiKey: string = "LTFGeKGoEsGnFAPPuG5Yak8drKwn6IFI";
    public storemanagementauthorization: string = "Basic TFRGR2VLR29Fc0duRkFQUHVHNVlhazhkckt3bjZJRkk6S0ZxTkh6c0wxQXZGTDRuQQ==";

    public claimApikey: string = "AXsPEbJwbcqx0QdIhGXmxEYKdoj51k0D";
    public claimUrl: string = "https://api.morrisons.com/wholesale/v1/";
    public claimAuth: string = "Basic QVhzUEViSndiY3F4MFFkSWhHWG14RVlLZG9qNTFrMEQ6VmpwUlNZaWQ4R0RlaEtTZA==";
    
    public amazonCatalogueTypes : any = [
        {catalogueTypeId: 13045761, catalogueType: 'Core'},
        {catalogueTypeId: 13045762, catalogueType: 'Storepick'}
    ];
    constructor() {

        // configuration constant values
        this.NOTATION = '';
        this.SESSION_TIMEOUT = 7200000;
        this.WARNING_TIMEOUT = 60000;
        this.SCROLL_RECORD_LIMIT = 100;
        // configuration api
        this.ENVIRNMNT = 'PROD';
        this.WHOIMG_URL = 'app/images/';
        this.rbacCustomerName = true;
        this.rangeDateVal = 5;

        // site urls
       // this.HOME_URL = 'https://wholesale.morrisons.com/#/';
        this.HOME_URL = 'https://wholesale.apps.mymorri.com';
        this.CONTACT_US = 'https://your.morrisons.com/Contact-Us/?clkInTab=ContactUs';

        // apis
        this.LOGINAPI = 'https://wholesaleuser.morconnect.com/wholesaleweb/prod/users/v1/';
        this.s3path = 'https://mprod.xxwmm.wholesaleweb.catalogue.zzzzzzzzzz.s3-eu-west-1.amazonaws.com/wholesaleinboundcatalogue/archive/';
        this.s3pathBucket = 'https://mprod.xxwmm.wholesaleweb.catalogue.zzzzzzzzzz.s3-eu-west-1.amazonaws.com/';

        // this.claimUrl = " https://wholesale-core.dev.np.morconnect.com/wholesale-claims/wholesale/v1/";

        // login url
        //order details
        this.orderUrl = "aHR0cHM6Ly9hcGkubW9ycmlzb25zLmNvbS93aG9sZXNhbGVvcmRlci92MS8=";
        this.orderapiKeyType = "Order";
        this.orderApiKey = "LTFGeKGoEsGnFAPPuG5Yak8drKwn6IFI";
        this.orderAuthorization = "Basic TFRGR2VLR29Fc0duRkFQUHVHNVlhazhkckt3bjZJRkk6S0ZxTkh6c0wxQXZGTDRuQQ==";

        // product details
        this.productUrl = "aHR0cHM6Ly9hcGkubW9ycmlzb25zLmNvbS9wcm9kdWN0L3YxLw==";
        this.productapiKeytype = "Product";
        this.productApiKey = "QG54H3ZouwUaEGje1GZBzAWI0ASAC1KP";
        this.productAuthorization = "Basic UUc1NEgzWm91d1VhRUdqZTFHWkJ6QVdJMEFTQUMxS1A6MEF4TXNoSmhmeGxZQmpVMg==";

        // catalogue type details
        this.catalogueurl = "aHR0cHM6Ly9hcGkubW9ycmlzb25zLmNvbS93aG9sZXNhbGVvcmRlci92MS8=";
        this.catalogueType = "Catalogue";
        this.catalogueapiKey = "LTFGeKGoEsGnFAPPuG5Yak8drKwn6IFI";
        this.catalogueauthorization = "Basic TFRGR2VLR29Fc0duRkFQUHVHNVlhazhkckt3bjZJRkk6S0ZxTkh6c0wxQXZGTDRuQQ==";

        //catalgoue management detail
        this.cataloguemanagementurl = "aHR0cHM6Ly9hcGkubW9ycmlzb25zLmNvbS93aG9sZXNhbGUvdjEv";
        this.cataloguemanagementType = "CatalogueManagement";
        this.cataloguemanagementapiKey = "LTFGeKGoEsGnFAPPuG5Yak8drKwn6IFI";
        this.cataloguemanagementauthorization = "Basic TFRGR2VLR29Fc0duRkFQUHVHNVlhazhkckt3bjZJRkk6S0ZxTkh6c0wxQXZGTDRuQQ==";

        //file service details
        this.fileserviceurl = "aHR0cHM6Ly9hcGkubW9ycmlzb25zLmNvbS9maWxlL3Yx";
        this.fileserviceType = "File";
        this.fileserviceapiKey = "LTFGeKGoEsGnFAPPuG5Yak8drKwn6IFI";
        this.fileserviceauthorization = "Basic TFRGR2VLR29Fc0duRkFQUHVHNVlhazhkckt3bjZJRkk6S0ZxTkh6c0wxQXZGTDRuQQ==";

        // price management
        this.priceManagementUrl = "aHR0cHM6Ly9hcGkubW9ycmlzb25zLmNvbS93aG9sZXNhbGUvdjEv";
        this.priceManagementapiKeyType = "PriceManagement";
        this.priceManagementapiKey = "LTFGeKGoEsGnFAPPuG5Yak8drKwn6IFI";
        this.priceManagementauthorization = "Basic TFRGR2VLR29Fc0duRkFQUHVHNVlhazhkckt3bjZJRkk6S0ZxTkh6c0wxQXZGTDRuQQ==";

        // report management
        this.reportmanagementurl = "aHR0cHM6Ly9hcGkubW9ycmlzb25zLmNvbS93aG9sZXNhbGUvdjEv";
        this.reportmanagementType = "ReportManagement";
        this.reportmanagementapiKey = "LTFGeKGoEsGnFAPPuG5Yak8drKwn6IFI";
        this.reportmanagementauthorization = "Basic TFRGR2VLR29Fc0duRkFQUHVHNVlhazhkckt3bjZJRkk6S0ZxTkh6c0wxQXZGTDRuQQ==";

        //config managemnet 
        this.configManagementurl = "aHR0cHM6Ly9hcGkubW9ycmlzb25zLmNvbS93aG9sZXNhbGUvdjEv";
        this.configManagementType = "ConfigManagement";
        this.configManagementapiKey = "LTFGeKGoEsGnFAPPuG5Yak8drKwn6IFI";
        this.configManagementauthorization = "Basic TFRGR2VLR29Fc0duRkFQUHVHNVlhazhkckt3bjZJRkk6S0ZxTkh6c0wxQXZGTDRuQQ==";

        // fulfilment dtls
        this.fulfilmenturl = "aHR0cHM6Ly9hcGkubW9ycmlzb25zLmNvbS93aG9sZXNhbGUvdjEv";
        this.fulfilmentType = "Fulfilment";
        this.fulfilmentapiKey = "LTFGeKGoEsGnFAPPuG5Yak8drKwn6IFI";
        this.fulfilmentauthorization = "Basic TFRGR2VLR29Fc0duRkFQUHVHNVlhazhkckt3bjZJRkk6S0ZxTkh6c0wxQXZGTDRuQQ==";

        //catalogue upload tracker
        this.cataloguetrackerurl = "aHR0cHM6Ly9hcGkubW9ycmlzb25zLmNvbS93aG9sZXNhbGUvdjEv";
        this.cataloguetrackerType = "CatalogueUploadTracker";
        this.cataloguetrackerapiKey = "LTFGeKGoEsGnFAPPuG5Yak8drKwn6IFI";
        this.cataloguetrackerauthorization = "Basic TFRGR2VLR29Fc0duRkFQUHVHNVlhazhkckt3bjZJRkk6S0ZxTkh6c0wxQXZGTDRuQQ==";

        //sales consolidation 
        this.salesconfigrurl = "aHR0cHM6Ly9hcGkubW9ycmlzb25zLmNvbS93aG9sZXNhbGUvdjEv";
        this.salesconfigType = "SaleConsolidation";
        this.salesconfigapiKey = "LTFGeKGoEsGnFAPPuG5Yak8drKwn6IFI";
        this.salesconfigauthorization = "Basic TFRGR2VLR29Fc0duRkFQUHVHNVlhazhkckt3bjZJRkk6S0ZxTkh6c0wxQXZGTDRuQQ==";

        // authorization 
        this.authorizationUrl = "aHR0cHM6Ly9hcGkubW9ycmlzb25zLmNvbS93aG9sZXNhbGUvdjEv";
        this.authorizationType = "Authorization";
        this.authorizationApiKey = "LTFGeKGoEsGnFAPPuG5Yak8drKwn6IFI";
        this.authauthorization = "Basic TFRGR2VLR29Fc0duRkFQUHVHNVlhazhkckt3bjZJRkk6S0ZxTkh6c0wxQXZGTDRuQQ==";

    }
}
