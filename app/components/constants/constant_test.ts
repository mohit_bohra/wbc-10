export class Constant {

    rangeDateVal: number;
    claimUrl: string;
    rbacCustomerName: boolean;
    /* constant values */
    public NOTATION: string;
    // in milli sec
    public SESSION_TIMEOUT: number;
    public WARNING_TIMEOUT: number;
    public SCROLL_RECORD_LIMIT: number;
    /* configuration api */
    public ENVIRNMNT: string;
    public WHOIMG_URL: any;

    /*site urls          */

    public HOME_URL: string;
    public CONTACT_US: string;

    /* data apis */
    public LOGINAPI: string;
    public tactocal: boolean = false;
  
    /*------- Constant for Logger -----------*/
    public enableLogger: boolean = true;
    public sentLogger: string = "Both"; // Possible values Console, Server, Both
    /*------- Constant for Logger - END -----------*/
    public rbacktactical: boolean = true;
    public customerName4Service: string = 'default';

    public SUPERADMIN_GROUP: string = 'MWEE-Admin';
    
    constructor() {

        // configuration constant values
        this.NOTATION = '';
        this.SESSION_TIMEOUT = 1800000;
        this.WARNING_TIMEOUT = 60000;
        this.SCROLL_RECORD_LIMIT = 100;
        // configuration api
        this.ENVIRNMNT = 'TEST';
        this.WHOIMG_URL = 'app/images/';
        this.rbacCustomerName = true;
        this.rangeDateVal = 5;

        // site urls
        this.HOME_URL = 'http://mdev.xxwmm.wholesaleweb.gggggggggg.s3-website-eu-west-1.amazonaws.com/#/';

        this.CONTACT_US = 'https://your.morrisons.com/Contact-Us/?clkInTab=ContactUs';

        // apis
        this.LOGINAPI = 'https://34.205.135.131:8443/wholesaleweb/dev/users/v1/';
        this.claimUrl = " https://wholesale-core.dev.np.morconnect.com/wholesale-claims/wholesale/v1/";
     
      

    }
}
