export class Constant {

    rangeDateVal: number;
    rbacCustomerName: boolean;
    orderUrl: string;
    orderapiKeyType: string;
    orderApiKey: string;
    orderAuthorization: string;
    productUrl: string;
    productapiKeytype: string;
    productApiKey: string;
    productAuthorization: string;
    catalogueurl: string;
    catalogueType: string;
    catalogueapiKey: string;
    catalogueauthorization: string;
    cataloguemanagementurl: string;
    cataloguemanagementType: string;
    cataloguemanagementapiKey: string;
    cataloguemanagementauthorization: string;
    fileserviceurl: string;
    fileserviceType: string;
    fileserviceapiKey: string;
    fileserviceauthorization: string;
    priceManagementUrl: string;
    priceManagementapiKeyType: string;
    priceManagementapiKey: string;
    priceManagementauthorization: string;
    reportmanagementurl: string;
    reportmanagementType: string;
    reportmanagementapiKey: string;
    reportmanagementauthorization: string;
    configManagementurl: string;
    configManagementType: string;
    configManagementapiKey: string;
    configManagementauthorization: string;
    fulfilmenturl: string;
    fulfilmentType: string;
    fulfilmentapiKey: string;
    fulfilmentauthorization: string;
    cataloguetrackerurl: string;
    cataloguetrackerType: string;
    cataloguetrackerapiKey: string;
    cataloguetrackerauthorization: string;
    salesconfigrurl: string;
    salesconfigType: string;
    salesconfigapiKey: string;
    salesconfigauthorization: string;
    authorizationUrl: string;
    authorizationType: string;
    authorizationApiKey: string;
    authauthorization: string;
    /* constant values */
    public NOTATION: string;
    // in milli sec
    public SESSION_TIMEOUT: number;
    public WARNING_TIMEOUT: number;
    public SCROLL_RECORD_LIMIT: number;
    /* configuration api */
    public ENVIRNMNT: string;
    public WHOIMG_URL: any;

    /*site urls          */

    public HOME_URL: string;
    public CONTACT_US: string;

    /* data apis */
    public LOGINAPI: string;
    public s3path: string;
    public s3pathBucket: string;
    public tactocal: boolean = false;

    public configTactical: boolean = true;
    storePrefix: any = {
        'amazon': '',
        'rontec': 'RTC',
        'sandpiper': 'SPR',
        'mccolls': '',
        'kens': '',
        'mpk': 'MPK',
        'mcdly': 'MCD',
        'harvest': 'HAR',
        'rontec-b': 'RTB'
    }
    storePrefixMccolls = '-';
    storePrefixSandpiper = 'SPR';
    storePrefixRontec = 'RTC';
    storePrefixAmazon = 'AMZ';
    storePrefixMPK = 'MPK';
    storePrefixMCDLY = 'MCD';
    storePrefixHarvest = 'HAR';
    storePrefixRontecB = 'RTB';
    

    channelList: any = [
        { 'name': 'All', 'value': 'All' },
        { 'name': 'Core', 'value': 'Non-OF' },
        { 'name': 'Store Pick', 'value': 'Storepick' },
        { 'name': 'Storepick-Old', 'value': 'OF' }
    ];

    channelSubTypeList: any = [
        {'name':'Core','value':'MP8QW'},
        {'name':'Fresh','value':'PRXKD'},
        {'name':'Prime Now','value':'THSV2'},
        {'name':'Pantry','value':'THSV3'}
    ];
    supplyingDepotforAmazon: any = [
        { productCategoryId: 0, name: "ALL" },
        { productCategoryId: 1, name: "AM" },
        { productCategoryId: 2, name: "CL" },
        { productCategoryId: 3, name: "CS" },
        { productCategoryId: 4, name: "FR" }
        ];

   

    
    /*------- Constant for Logger -----------*/
    public enableLogger: boolean = true;
    public sentLogger: string = "Both"; // Possible values Console, Server, Both
    /*------- Constant for Logger - END -----------*/
    public rbacktactical: boolean = false;
    public customerName4Service: string = 'default';
      public claimTactical: boolean = false;

    public SUPERADMIN_GROUP: string = 'MWEE-Admin';
    public activeUserSessionMessage = "Session is Active";
    public userSessionTerminatedMsg = "Session has been terminated - please log in again.";
     public userSessionTerminatedMsgForInactiveUser = "Session is InActive";

    public claimApikey: string = "OCz10LUivxT4ZixZVuR2MXwMX5JFQCAQ";
    public claimUrl: string = "https://uat-api.morrisons.com/wholesale/v1/";
    public claimAuth: string = "Basic T0N6MTBMVWl2eFQ0Wml4WlZ1UjJNWHdNWDVKRlFDQVE6T2J0bmtOYXE0RXh3WkdSMw==";

    public storemanagementurl: string = "https://uat-api.morrisons.com/wholesale/v1/";
    public storemanagementapiKey: string = "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz";
    public storemanagementauthorization: string = "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==";

    public amazonCatalogueTypes : any = [
        {catalogueTypeId: 13045761, catalogueType: 'Core'},
        {catalogueTypeId: 13045762, catalogueType: 'Storepick'}
    ];

    public channelType: any = {
        'amazon': 'CORE',
        'rontec': 'DAILY',
        'sandpiper': 'DAILY',
        'mccolls': 'CNV',        
        'mpk': 'DAILY',
        'mcdly': 'DAILY',
        'harvest': 'DAILY',
        'rontec-b': 'DAILY'
    }

    public marketPlaceIdForPrimeNow = "AM7DNVYQULIQ5";
    public marketPlaceIdCODOTUK = "A1F83G8C2ARO7P";        
    
    public merchantName : any = ["Prime Now",".CO.UK"];
    
    constructor() {

        // configuration constant values
        this.NOTATION = '';
        this.SESSION_TIMEOUT = 720000;
        this.WARNING_TIMEOUT = 60000;
        this.SCROLL_RECORD_LIMIT = 100;
        // configuration api
        this.ENVIRNMNT = 'UAT';
        this.tactocal = false;
        this.WHOIMG_URL = 'app/images/';
        this.rbacCustomerName = true;
        this.rangeDateVal = 5;

        // site urls
        //this.HOME_URL = 'https://wholesale.uat.np.morconnect.com/#/';
        this.HOME_URL = 'https://wholesale.uat.testapps.mymorri.com/#/';

        this.CONTACT_US = 'https://your.morrisons.com/Contact-Us/?clkInTab=ContactUs';

        // apis
        this.LOGINAPI = 'https://wholesaleuser.uat.np.morconnect.com/wholesaleweb/uat/users/v1/';
        this.s3path  = 'https://muat.xxwmm.wholesaleweb.catalogue.eeeeeeeeee.s3-eu-west-1.amazonaws.com/wholesaleinboundcatalogue/archive/';
        this.s3pathBucket  = 'https://muat.xxwmm.wholesaleweb.catalogue.eeeeeeeeee.s3-eu-west-1.amazonaws.com/';
       
        // login url
        //order details
        this.orderUrl = "aHR0cHM6Ly91YXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlb3JkZXIvdjEv";
        this.orderapiKeyType = "Order";
        this.orderApiKey = "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz";
        this.orderAuthorization = "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==";

        // product details
        this.productUrl = "aHR0cHM6Ly91YXQtYXBpLm1vcnJpc29ucy5jb20vcHJvZHVjdC92MS8=";
        this.productapiKeytype = "Product";
        this.productApiKey = "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz";
        this.productAuthorization = "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==";

        // catalogue type details
        this.catalogueurl = "aHR0cHM6Ly91YXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlb3JkZXIvdjEv";
        this.catalogueType = "Catalogue";
        this.catalogueapiKey = "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz";
        this.catalogueauthorization = "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==";

        //catalgoue management detail
        this.cataloguemanagementurl = "aHR0cHM6Ly91YXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlL3YxLw==";
        this.cataloguemanagementType = "CatalogueManagement";
        this.cataloguemanagementapiKey = "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz";
        this.cataloguemanagementauthorization = "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==";

        //file service details
        this.fileserviceurl = "aHR0cHM6Ly91YXQtYXBpLm1vcnJpc29ucy5jb20vZmlsZS92MQ==";
        this.fileserviceType = "File";
        this.fileserviceapiKey = "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz";
        this.fileserviceauthorization = "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==";

        // price management
        this.priceManagementUrl = "aHR0cHM6Ly91YXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlL3YxLw==";
        this.priceManagementapiKeyType = "PriceManagement";
        this.priceManagementapiKey = "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz";
        this.priceManagementauthorization = "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==";

        // report management
        this.reportmanagementurl = "aHR0cHM6Ly91YXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlL3YxLw==";
        this.reportmanagementType = "ReportManagement";
        this.reportmanagementapiKey = "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz";
        this.reportmanagementauthorization = "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==";

        //config managemnet 
        this.configManagementurl = "aHR0cHM6Ly91YXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlL3YxLw==";
        this.configManagementType = "ConfigManagement";
        this.configManagementapiKey = "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz";
        this.configManagementauthorization = "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==";

        // fulfilment dtls
        this.fulfilmenturl = "aHR0cHM6Ly91YXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlL3YxLw==";
        this.fulfilmentType = "Fulfilment";
        this.fulfilmentapiKey = "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz";
        this.fulfilmentauthorization = "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==";

        //catalogue upload tracker
        this.cataloguetrackerurl = "aHR0cHM6Ly91YXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlL3YxLw==";
        this.cataloguetrackerType = "CatalogueUploadTracker";
        this.cataloguetrackerapiKey = "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz";
        this.cataloguetrackerauthorization = "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==";

        //sales consolidation 
        this.salesconfigrurl = "aHR0cHM6Ly91YXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlL3YxLw==";
        this.salesconfigType = "SaleConsolidation";
        this.salesconfigapiKey = "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz";
        this.salesconfigauthorization = "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==";

        // authorization 
        this.authorizationUrl = "aHR0cHM6Ly91YXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlL3YxLw==";
        this.authorizationType = "Authorization";
        this.authorizationApiKey = "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz";
        this.authauthorization = "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==";
    }
}
