import { Component } from '@angular/core';
import { Services } from '../../services/app.services';
import { Router, ActivatedRoute } from '@angular/router';
import { Constant } from '../constants/constant';

@Component({
    selector: 'catalogue-management-pending-request-header',
    templateUrl: 'cataloguemanagependingheader.component.html',
})
export class CatalogueManagePendingHeaderComponent {

    objConstant = new Constant();
    whoImgUrl: string;
    showLoader: boolean;
    orgApiName: string;
    dataTeamPendingRequests: number = 0;
    commercialPendingRequests: number = 0;
    supplyChainPendingRequests: number = 0;
    totalPendingRequests: number = 0;
    _sub: any;
    customerName: any;
    organisationList: any;
    channels: string;
    requesttypes: string;
    teamname: string;
    requestid: string;
    organisationListChange: any;
    orgimgUrl: string;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disableduploadCatalogue: boolean;
    disabledOrder: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    showPendingReq: boolean;


    constructor(private _postsService: Services, private _route: ActivatedRoute, private _router: Router) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.showLoader = false;
        this.channels = "@all";
        this.requesttypes = "@all";
        this.teamname = "@all";
        this.requestid = "@all";
        this.showPendingReq = true;
    }

    // function works on page load
    ngOnInit() {
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {

            this.disabledCusChange = false;
            this.disabledPermission = false;
            this.disabledReports = false;
            this.disabledCatalogue = false;
            this.disabledOrder = false;
            this.disabledOrderRaise = false;
            this.disabledCatalogueRaise = false;
            this.disabledManageCatalogue = false;

            // subscribe to route params
            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                let reports;
                let orders;
                let catalogues;
                let uploadCatalogue;
                let cataloguesRaise;
                let accessMgmt;
                let redirct;
                let changeOrgAvailable = new Array;
                let catManagement;
                // list of org assigned to user
                this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));

                this.organisationList.forEach(function (item: any) {
                    if (item.orgType !== cusType) {
                        changeOrgAvailable.push(item);
                    }
                });
                this.organisationListChange = changeOrgAvailable;
                // this.disabledCusChange = false;
                if (this.organisationList.length == 1) {
                    this.disabledCusChange = true;
                }

                document.getElementById('orderArrowSpan').style.display = 'block';
                document.getElementById('orderMob').style.display = 'block';
                document.getElementById('orderRaiseMob').style.display = 'none';

                document.getElementById('catalogueMob').style.display = 'block';
                document.getElementById('catalogueArrowSpan').style.display = 'none';
                document.getElementById('catalogueRaiseMob').style.display = 'none';
                document.getElementById('reportsMob').style.display = 'block';
                document.getElementById('permissonsMob').style.display = 'block';

                if (document.getElementById('orderArrow').classList.contains('glyphicon-menu-up') === true) {
                    let sublinks = document.getElementById('orderArrow');
                    sublinks.classList.remove('glyphicon-menu-up');
                    sublinks.classList.add('glyphicon-menu-down');
                    document.getElementById('orderRaiseMob').style.display = 'none';
                }

                if (document.getElementById('catalogueArrow').classList.contains('glyphicon-menu-up') === true) {
                    let sublinks = document.getElementById('catalogueArrow');
                    sublinks.classList.remove('glyphicon-menu-up');
                    sublinks.classList.add('glyphicon-menu-down');
                    document.getElementById('catalogueRaiseMob').style.display = 'none';
                }


                this.organisationList.forEach(function (item: any) {
                    if (item.orgType === cusType) {
                        // logic for permission set
                        orgImgUrl = item.imgUrl;
                        orgApiNameTemp = item.orgName;

                        if ((item.permissions.menuPermission.orders).toLowerCase() == 'none') {
                            orders = true;
                            document.getElementById('orderMob').style.display = 'none';
                            document.getElementById('orderRaiseMob').style.display = 'none';
                        }

                        if ((item.permissions.catalogAdminPermission.uploadCatalogue).toLowerCase() == 'none') {
                            uploadCatalogue = true;
                        } else {
                            uploadCatalogue = false;
                        }

                        if ((item.permissions.menuPermission.raiseNewOrder).toLowerCase() == 'none') {
                            document.getElementById('orderArrowSpan').style.display = 'none';
                            document.getElementById('orderRaiseMob').style.display = 'none';

                        }

                        if ((item.permissions.menuPermission.catalogueManagement).toLowerCase() == 'none') {
                            catManagement = true;
                        } else {
                            catManagement = false;
                        }


                        if ((item.permissions.menuPermission.catalogueEnquiry).toLowerCase() == 'none') {

                            catalogues = true;
                            document.getElementById('catalogueMob').style.display = 'none';
                            document.getElementById('catalogueRaiseMob').style.display = 'none';
                            redirct = 1;
                            if (sessionStorage.getItem('page_redirect') == '0') {
                                sessionStorage.removeItem('page_redirect');
                            } else {
                                sessionStorage.setItem('page_redirect', '1');
                            }


                        } else {
                            // write logic to show add new product

                            //document.getElementById('catalogueRaiseMob').style.display = 'block';
                        }

                        if ((item.permissions.catalogAdminPermission.addNewProducts).toLowerCase() == 'none') {
                            cataloguesRaise = true;
                            document.getElementById('catalogueArrowSpan').style.display = 'none';
                            document.getElementById('catalogueRaiseMob').style.display = 'none';


                        } else {
                            // write logic to show add new product

                            //document.getElementById('catalogueRaiseMob').style.display = 'block';
                        }

                        if ((item.permissions.menuPermission.reports).toLowerCase() == 'none') {
                            reports = true;
                            document.getElementById('reportsMob').style.display = 'none';

                        }

                        if ((item.permissions.menuPermission.accessMgmt).toLowerCase() == 'none') {
                            accessMgmt = true;
                            document.getElementById('permissonsMob').style.display = 'none';
                        }

                    }
                });

                /*if (redirct == 1) {
                    redirct = 0;
                    this.redirect('/reports', this.customerName);
            }*/

                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                this.disabledReports = reports;
                this.disabledOrder = orders;
                this.disabledOrderRaise = orders;
                this.disableduploadCatalogue = uploadCatalogue;
                this.disabledCatalogue = catalogues;
                this.disabledCatalogueRaise = cataloguesRaise;
                this.disabledPermission = accessMgmt;
                this.disabledManageCatalogue = catManagement;
                this.getPendingRequests();

            });
        }
    }

    // get relevant order detail
    getPendingRequests() {
        this.showLoader = true;

        this._postsService.getCataloguePendingRequests(this.orgApiName, this.channels, this.requesttypes, this.teamname, this.requestid).subscribe(data => {
            this.showLoader = false;
            if (sessionStorage.getItem('pageState') == '9') {
                this.showPendingReq = false;
                let borderClass = document.getElementById('setBorder');
                borderClass.classList.remove('border-right-yellow');

            } else {
                this.showPendingReq = true;
                let borderClass = document.getElementById('setBorder');
                borderClass.classList.add('border-right-yellow');

            }
            this.dataTeamPendingRequests = data.pendingRequestSummary.pendingDataTeamReviewCount;
            this.commercialPendingRequests = data.pendingRequestSummary.pendingCommercialReviewCount;
            this.supplyChainPendingRequests = data.pendingRequestSummary.pendingSupplychainReviewCount;
            this.totalPendingRequests = this.supplyChainPendingRequests + this.commercialPendingRequests + this.dataTeamPendingRequests;

        }, err => {
            this.showLoader = false;
            this._router.navigate(['/error404']);
        });
    }

    // navigate function
    redirect(page: any, orgType: any) {
        this._router.navigate([page, orgType]);
    }

    redirect_review(page: any, orgType: any) {
        if(page == '/datareview')
            this._router.navigate(['cataloguemanagement', orgType, 'review','data']);
        else if(page == '/commercialreview')
            this._router.navigate(['cataloguemanagement', orgType, 'review','commercial']);
        else if(page == '/supplychainreview')
            this._router.navigate(['cataloguemanagement', orgType, 'review','supplychain']);
        else 
            this._router.navigate([page, orgType]);
    }
}

