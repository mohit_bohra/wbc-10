import { Component } from '@angular/core';
// import { DomSanitizer } from '@angular/platform-browser';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../services/app.services';
import { commonfunction } from '../../services/commonfunction.services';
import { GlobalComponent } from '../global/global.component';
import { Constant } from '../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'fullrangeexport.component.html',
    providers: [commonfunction]
})
export class FullRangeExportComponent {

    objConstant = new Constant();
    whoImgUrl: any;

    productid: string;
    errorMsg: boolean;
    errorMsgString: string;
    showLoader: boolean;
    printHeader: boolean;
    organisationList: any;

    pageState: number = 15;
    customerName: string;
    orgimgUrl: string;
    orgApiName: string;
    productDetails: any;
    selectOption: string;
    productName: any;
    mapLen: any;
    description: any;
    homeUrl: any;
    private _sub: any;
    availabilityMaster: any;
    WorkflowMaster: any;
    catalogueTypeMaster: any;
    availabilityStatus: string;
    workflowStatusName: string;
    mapTypesMaster: any;
    mapTypesMasterDisplay: any;
    lookupTypesMaster: any;
    lookupTypesMasterDisplay: any;
    financialTypesMaster: any;
    financialTypesMasterDisplay: any;
    financialCurrencies: any;
    catalogueType: string;
    imageUrl: string;
    productMaps: any;
    productPrices: any;
    productmin: string;
    maptypeValue: string;
    isProductIdEntered: boolean;

    showSuccessModel: boolean;
    minDate: any;
    addedProdId: any;
    addedMin: any;
    identifierRow: number = 1;
    pricingRow: number = 1;
    attributeRow: number = 1;

    prodIdentifierRowHTML: any;

    todayDateStr: string;

    hideOrganisation: boolean = true;
    tempCalId: any;
    calDefaultDate: Date;

    allowWrite: boolean;

    requestType: any;
    disabledOrder: boolean = false;
    disabledCatalogue: boolean = false;

    imgName: string = "";

    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;

    channels: string;
    requesttypes: string;
    teamname: string;
    requestid: string;
    detailid: string;
    allLinesExortData: any;
    allLinesExortDatacount: any;
    errorSummary: any;
    noDataFound: boolean;
    viewListExports: any;
    viewListExportPrint: any = [];
    s3path: any;
    succMSg: any;

    /*rbac session related declared variables start*/    
    assignedPermissions: any = [];
    noPermissionMsg: string = "";
    hasAccess: boolean = false;
    disablecatalogueEnquiry: boolean = true;
    disablestoreEnquiry: boolean = true;
    disableaddStoreNewEntry: boolean = true;
    disableeditCatalogueEntry: boolean = true;
    disableaddCatalogueEntry: boolean = true;
    disableaddCatalogueAttribute: boolean = true;
    disablecatalogueBulkUploadView: boolean = true;
    disablecatalogueBulkUploadSubmit: boolean = true;
    disablecatalogueBulkExportView: boolean = true;
    disablestoreBulkExportView: boolean = true;
    disablecatalogueBulkExportSubmit: boolean = true;
    disablestoreBulkExportSubmit: boolean = true;
    disablemaintainAllocationPriorities:boolean = true;
    disablestoreBulkUploadView:boolean = true;
    disablemaintainSupplyingDepots:boolean = true;
    disableViewFullRangeExports: boolean = true;
    disablefullRangeExportsSupplyChainReview: boolean = true;
    disableaddRequests: boolean = true;
    disablemanageRequests: boolean = true;
    disablerequestsErrorCorrection: boolean = true;
    disablerequestsDataTeamReview: boolean = true;
    disablerequestsSupplyChainReview: boolean = true;
    disablerequestsCommercialReview: boolean = true;
    disablerepricingConfiguration: boolean = true;
    disablerepricingSupplierRequestsReview: boolean = true;
    disablerepricingPeriodicReview: boolean = true;
    disablerepricingProductsIdentification: boolean = true;
    organisationListChange: any;
    permissionArray: any = [];

    /*rbac session related declared variables end*/


    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction) {


        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.noDataFound = false;

        // set default date null to calender
        this.errorMsgString = '';
        this.errorMsg = false;
        this.s3path = this.objConstant.s3path;
    }

    // function works on page load

    ngOnInit() {
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {

            this.showLoader = true;
            // document.getElementById('viewExportContainer').style.display = 'none';
            this.printHeader = true;

            this.availabilityStatus = '';
            this.workflowStatusName = '';
            this.catalogueType = '';
            this.productid = '';
            this.productName = '';
            this.productmin = '';
            this.description = '';
            this.imageUrl = this.whoImgUrl;
            this.isProductIdEntered = false;
            this.allowWrite = false;
            this.requestType = 'Select';
            this.selectOption = '';
            this.tempCalId = '';
            //this.getViewListOfExportsDownload();
            //this.calDefaultDate = "30/07/2017";
            /* configurations to load data */
            this.channels = "default";
            this.requesttypes = "@all";
            this.teamname = "@all";
            this.requestid = "@all";
            this.detailid = "@all";
            this.allLinesExortData = '';
            this.errorSummary = '';

            // window scroll function
            this._globalFunc.autoscroll();
            this.maptypeValue = '';

            this.minDate = new Date();
            /*this.minDate.setMonth(prevMonth);
            this.minDate.setFullYear(prevYear);*/
            this.todayDateStr = this._cfunction.todayDateStrFunction();


            // set page state variable
            this.pageState = 15;
            this.identifierRow = 1;
            this.pricingRow = 1;
            this.attributeRow = 1;
            sessionStorage.setItem('pageState', '15');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            // subscribe to route params
            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                let reports;
                let orders;
                let catalogues;
                let cataloguesRaise;
                let accessMgmt;
                let redirct;
                let changeOrgAvailable = new Array;
                let catManagement;

                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
                this.imgName = this.getCustomerImage(params.orgType);

                this.getFullRangeExport();
                //  this.getAllCatalogues();

            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);


            // logic for resize pop up 
            window.onresize = () => {

                if (screen == "desktop") {
                    /*   document.getElementById('prouctDetailTitle').style.display = 'block';
                       document.getElementById('productDetailHeader').style.display = 'block';
                       document.getElementById('productDetailImg').style.display = 'block';*/
                }

                screen = this._globalFunc.getDevice(window.innerWidth);

            };

        }
    }

    // navigate function
    redirect(page: any, orgType: any) {
        this._router.navigate([page, orgType]);
    }

    refresh() {
        //window.location.reload();
        this.ngOnInit();
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
    }

    toggleOrganisation() {
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            //  document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
            border.classList.remove('searchborder');
        } else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            // document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
            border.classList.add('searchborder');

        }
    }

    // get error lines from npr request request
    getFullRangeExport() {
        this._postsService.getFullRangeExport(this.customerName, this.channels, this.requesttypes, this.teamname, this.requestid, this.detailid).subscribe(data => {
            this.showLoader = false;
            // document.getElementById('viewExportContainer').style.display = 'block';
            this.allLinesExortData = data.requestHeaders;
            console.log(this.allLinesExortData);
            this.allLinesExortDatacount = data.requestHeaders.length;
            this.errorSummary = data.errorSummary;
            this.noDataFound = false;

        }, error => {
            this.showLoader = false;
            if (error.errorCode === '404.33.402' || error.httpResponseCode === 404) {
                //alert('nodata');
                this.noDataFound = true;
            } else {
                // alert("404");
                this._router.navigate(['/error404']);
            }
        });
        //setTimeout(function () {}, 4000);
    }


    getViewListOfExportsDownload() {
        this._postsService.getViewListOfExportsDownload(this.orgApiName, this.channels).subscribe(data => {
            this.viewListExportPrint = data;
        });
    }

    // print functionality

    printExportLines() {

        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        var printContents = document.getElementById('exportable').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContents + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContents + '</body></html>');
            popup.document.close();

        }
    }

    fileDownloadFromFileService(filename: any) {
        //this.showLoader = true; 
        /*this._postsService.fileDownloadFromFileServicelist(filename).subscribe(data => {
            //this.showLoader = false;            
            console.log(data);
            console.log(data.fileURL);
            this.fileDownload(data.fileURL);
        }
        );*/


        this._postsService.fileReportDownloadUsingPresignedURL(this.orgApiName, filename, 'fullrange', 'download').subscribe(data => {
            console.log(data.preSignedS3Url);
            window.open(data.preSignedS3Url);
        }, error => {
            this.succMSg = filename + " doesnot exist.";
            let sessionModal = document.getElementById('addConfirmationModal');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
        });
    }

    closePopup() {
        let sessionModal = document.getElementById('addConfirmationModal');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('addConfirmationModal').style.display = 'none';
    }

    // fileDownload(filepath: any) {
    //     window.open(filepath);
    // }

    fileDownload(filepath: any) {
        var downloadpath = this.s3path;
        window.open(downloadpath + filepath);
    }

    // download functionality
    exportData() {
        return this._cfunction.exportData('exportable',this.customerName+'FullRangeExport');
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    /*rbac implementation - get user session function call start*/
    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject)=>{
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.
                    sessionStorage.clear();
                    this._router.navigate(['']);
                } else {
                    this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                    let changeOrgAvailable: any = [];
                    let orgImgUrl = "";
                    let orgApiNameTemp = "";
                    this.organisationList.forEach(function (item: any) {
                        if (item.customerName !== customerName) {
                            changeOrgAvailable.push(item);
                        } else {
                            orgImgUrl = item.customerName + ".png";
                            orgApiNameTemp = item.customerName;
                        }
                    });
                    this.organisationListChange = changeOrgAvailable;
                    this.orgimgUrl = orgImgUrl;
                    this.orgApiName = orgApiNameTemp;
                    // this.disabledCusChange = false;
                    if (this.organisationList.length == 1) {
                        this.disabledCusChange = true;
                    }

                    console.log("this.organisationList234");
                    console.log(this.organisationList);
                    let that = this;
                    let catalogueEnquiry = true;
                    let editCatalogueEntry = true;
                    let addCatalogueEntry = true;
                    let addCatalogueAttribute = true;
                    let catalogueBulkUploadView = true;
                    let catalogueBulkUploadSubmit = true;
                    let catalogueBulkExportView = true;
                    let storeBulkExportView = true;
                    let catalogueBulkExportSubmit = true;
                    let storeBulkExportSubmit = true;
                    let maintainAllocationPriorities = true;
                    let storeBulkUploadView = true;
                    let maintainSupplyingDepots = true;
                    let storeEnquiry = true;
                    let addStoreNewEntry = true;
                    let ViewFullRangeExports = true;
                    let fullRangeExportsSupplyChainReview = true;
                    let addRequests = true;
                    let manageRequests = true;
                    let requestsErrorCorrection = true;
                    let requestsDataTeamReview = true;
                    let requestsSupplyChainReview = true;
                    let requestsCommercialReview = true;
                    let repricingConfiguration = true;
                    let repricingSupplierRequestsReview = true;
                    let repricingPeriodicReview = true;
                    let repricingProductsIdentification = true;
                    let catalogueManagementMenuPermission = false;
                    let permArray = [];
                    let permArrayMainEstateRange: any = [];

                    this.organisationList.forEach(function (organisation: any) {
                        if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                            console.log("organisation : " + that.customerName);
                            console.log(organisation);
                            organisation.permissionRoleList.forEach(function (permRoleList: any) {
                                //console.log("11");
                                //console.log(permRoleList);
                                permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                    //console.log("22");
                                    that.setPermissionsArray(permRoleDtlList.permissionGroup);
                                    if (permRoleDtlList.permissionGroup == 'catalogueManagement' || permRoleDtlList.permissionGroup == 'morrisonsPermissions') {
                                        permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                            if (permList.permissionName == 'catalogueManagement' && permList.permissionStatus == 'active') {
                                                catalogueManagementMenuPermission = true;
                                            }
                                            console.log(permList);
                                            switch (permList.permissionName) {
                                                case "viewFullRangeExports":
                                                    //console.log("55");
                                                    ViewFullRangeExports = false;
                                                    permArrayMainEstateRange.push({ "ViewFullRangeExports": true });
                                                    //alert(filfilment);
                                                    break;
                                                case "fullRangeExportsSupplyChainReview":
                                                    //console.log("55");
                                                    fullRangeExportsSupplyChainReview = false;
                                                    permArrayMainEstateRange.push({ "fullRangeExportsSupplyChainReview": true });
                                                    //alert(filfilment);
                                                    break;
                                            }
                                        });
                                    }
                                });
                            });
                        }
                    });
                    that = null;
                    if (!catalogueManagementMenuPermission) {
                        let loggedInUserGroup = sessionStorage.getItem('loggedInUserGroupName');
                        let loggedInUserAdminFlag: string = sessionStorage.getItem('loggedInUserAdminFlag');
                        let superAdminGroup = this.objConstant.SUPERADMIN_GROUP;
                        console.log(loggedInUserGroup + " : " + superAdminGroup + " : " + loggedInUserAdminFlag);
                        console.log((loggedInUserGroup == superAdminGroup));
                        console.log((loggedInUserGroup != superAdminGroup && loggedInUserAdminFlag));
                        console.log((loggedInUserGroup == superAdminGroup) || (loggedInUserGroup != superAdminGroup && loggedInUserAdminFlag));
                        if ((loggedInUserGroup == superAdminGroup)) {
                            console.log("aa");
                            this._router.navigate(["groups", customerName]);
                        } else if (loggedInUserGroup != superAdminGroup && loggedInUserAdminFlag == "true") {
                            console.log("ff");
                            this._router.navigate(["groups", customerName]);
                        } else {
                            if (this.assignedPermissions.length) {
                                console.log("cc");
                                this._router.navigate(["orders", customerName]);
                            } else {
                                console.log("dd");
                                this.noPermissionMsg = "you do not have any access permissions defined for " + customerName;
                                this.hasAccess = true;
                            }
                        }
                        // redirect to order
                    }
                    this.disablecatalogueEnquiry = catalogueEnquiry;
                    console.log("disablecatalogueEnquiry : " + this.disablecatalogueEnquiry + " : " + catalogueEnquiry);
                    this.disablestoreEnquiry = storeEnquiry;
                    console.log("disablestoreEnquiry : " + this.disablestoreEnquiry + " : " + storeEnquiry);
                    this.disableaddStoreNewEntry = addStoreNewEntry;
                    this.disableeditCatalogueEntry = editCatalogueEntry;
                    this.disableaddCatalogueEntry = addCatalogueEntry;
                    this.disableaddCatalogueAttribute = addCatalogueAttribute;
                    this.disablecatalogueBulkUploadView = catalogueBulkUploadView;
                    this.disablecatalogueBulkUploadSubmit = catalogueBulkUploadSubmit;
                    this.disablecatalogueBulkExportView = catalogueBulkExportView;
                    this.disablestoreBulkExportView = storeBulkExportView;
                    console.log("disablestoreBulkExportView : " + this.disablestoreBulkExportView + " : " + storeBulkExportView);
                    this.disablecatalogueBulkExportSubmit = catalogueBulkExportSubmit;
                    this.disablestoreBulkExportSubmit = storeBulkExportSubmit;
                    this.disablemaintainAllocationPriorities = maintainAllocationPriorities;
                    this.disablestoreBulkUploadView = storeBulkUploadView;
                    this.disablemaintainSupplyingDepots = maintainSupplyingDepots;
                    this.disableViewFullRangeExports = ViewFullRangeExports;
                    this.disablefullRangeExportsSupplyChainReview = fullRangeExportsSupplyChainReview;
                    this.disableaddRequests = addRequests;
                    this.disablemanageRequests = manageRequests;
                    this.disablerequestsErrorCorrection = requestsErrorCorrection;
                    this.disablerequestsDataTeamReview = requestsDataTeamReview;
                    this.disablerequestsSupplyChainReview = requestsSupplyChainReview;
                    this.disablerequestsCommercialReview = requestsCommercialReview;
                    this.disablerepricingConfiguration = repricingConfiguration;
                    this.disablerepricingSupplierRequestsReview = repricingSupplierRequestsReview;
                    this.disablerepricingPeriodicReview = repricingPeriodicReview;
                    this.disablerepricingProductsIdentification = repricingProductsIdentification;
                    this.hasAccess = false;
                    
                    if (permArrayMainEstateRange.length > 0) {
                        this.permissionArray.permArrayMainEstateRange = true;
                        this.hasAccess = true;
                    } else {
                        this.permissionArray.permArrayMainEstateRange = false;
                    }
                }
                console.log("this.permissionArray");
                console.log(this.permissionArray);
                console.log(this.permissionArray.length);
                //alert(this.disabledfilfilment);
            }, (error: any) =>{
                if(error.errorCode =='503.65.001'){
                    this._router.navigate(['/error404']);
                }
            });
        });
    }
    
    setPermissionsArray(permissionGroup: string) {
        this.assignedPermissions.push(permissionGroup);
    }
    /*rbac functionality - get user session function end*/

}