import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FullRangeExportComponent } from './fullrangeexport.component';

// route Configuration
const fullrangeexportRoutes: Routes = [
    { path: '', component: FullRangeExportComponent },
    //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
    imports: [RouterModule.forChild(fullrangeexportRoutes)],
    exports: [RouterModule]
})
export class FullRangeExportRoutes { }
