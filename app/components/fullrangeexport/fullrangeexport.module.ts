import { NgModule } from '@angular/core';
import { FullRangeExportRoutes } from './fullrangeexport.routes';
import { FullRangeExportComponent } from './fullrangeexport.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../global/global.component';
import { Services } from '../../services/app.services';
import { commonfunction } from '../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/primeng';
import { TooltipModule } from "ngx-tooltip";
import { CatalogueManagePendingHeaderModule } from '../cataloguemanagement/cataloguemanagependingheader/cataloguemanagependingheader.module';


// import { DatepickerModule } from 'ng2-bootstrap';
import { DatepickerModule } from '../datepicker/ng2-bootstrap';
@NgModule({
    imports: [
        FullRangeExportRoutes,
        CommonModule,
        FormsModule,
          TooltipModule,
        DatepickerModule.forRoot(),
        CalendarModule,
        CatalogueManagePendingHeaderModule
    ],
    exports: [],
    declarations: [FullRangeExportComponent],
    providers: [Services, GlobalComponent, commonfunction]
})
export class FullRangeExportModule { }
