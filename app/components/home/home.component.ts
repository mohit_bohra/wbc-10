// backp up

import { Component } from '@angular/core';
import { GlobalComponent } from '../global/global.component';
import { Router } from '@angular/router';
import { Constant } from '../constants/constant';
// import { RouterModule, Routes } from '@angular/router';
import { Services } from '../../services/app.services';
import { commonfunction } from '../../services/commonfunction.services';

/*  For Logger 
import { JL } from 'jsnlog';
import { Inject } from '@angular/core';
/*  For Logger - END */

@Component({
    selector: 'home',
    templateUrl: 'home.component.html',
    providers: [Services, GlobalComponent, commonfunction]
})

export class HomeComponent {

    orderUrl: string;
    orderapiKeyType: string;
    orderAuthorization: string;
    productUrl: string;
    productapiKeytype: string;
    productApiKey: string;
    productAuthorization: string;
    catalogueurl: string;
    catalogueType: string;
    catalogueapiKey: string;
    catalogueauthorization: string;
    cataloguemanagementurl: string;
    cataloguemanagementType: string;
    cataloguemanagementapiKey: string;
    cataloguemanagementauthorization: string;
    fileserviceurl: string;
    fileserviceType: string;
    fileserviceapiKey: string;
    fileserviceauthorization: string;
    priceManagementUrl: string;
    priceManagementapiKeyType: string;
    priceManagementapiKey: string;
    priceManagementauthorization: string;
    reportmanagementurl: string;
    reportmanagementType: string;
    reportmanagementapiKey: string;
    reportmanagementauthorization: string;
    // configManagementurl: string;
    configManagementType: string;
    // configManagementapiKey: string;
    // configManagementauthorization: string;
    fulfilmenturl: string;
    fulfilmentType: string;
    fulfilmentapiKey: string;
    fulfilmentauthorization: string;
    cataloguetrackerurl: string;
    cataloguetrackerType: string;
    cataloguetrackerapiKey: string;
    cataloguetrackerauthorization: string;
    salesconfigrurl: string;
    salesconfigType: string;
    salesconfigapiKey: string;
    salesconfigauthorization: string;

    siteMSg: any = "Site is under maintenance";
    siteMaintanceFlg: boolean;
    siteMaintanceFlg1: boolean;
    groupId: any;
    //JL: JL.JSNLog; // For Logger

    objConstant = new Constant();
    pageState: number = 0;
    showLoader: boolean;
    whoImgUrl: any;
    logout: boolean;
    loginalert: boolean;
    errorMsg: string;
    username: string;
    password: string;
    loginError: boolean; //@Inject('JSNLOG') JL: JL.JSNLog, 
    orgType: any;
    userId: any;
    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _router: Router, private _commonFuntion: commonfunction) {
        // Img url path
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.logout = false;
        //this.JL = JL;
        // this.getAllurls();
    }

    getAllurls() {

        //
        this.productUrl = this.objConstant.productUrl;
        this.productapiKeytype = this.objConstant.productapiKeytype;
        this.productApiKey = this.objConstant.productApiKey;
        this.productAuthorization = this.objConstant.productAuthorization;

        //
        this.catalogueurl = this.objConstant.catalogueurl;
        this.catalogueType = this.objConstant.catalogueType;
        this.catalogueapiKey = this.objConstant.catalogueapiKey;
        this.catalogueauthorization = this.objConstant.catalogueauthorization;

        //catalgoue
        this.cataloguemanagementurl = this.objConstant.cataloguemanagementurl;
        this.cataloguemanagementType = this.objConstant.cataloguemanagementType;
        this.cataloguemanagementapiKey = this.objConstant.cataloguemanagementapiKey;
        this.cataloguemanagementauthorization = this.objConstant.cataloguemanagementauthorization;

        //file
        this.fileserviceurl = this.objConstant.fileserviceurl;
        this.fileserviceType = this.objConstant.fileserviceType;
        this.fileserviceapiKey = this.objConstant.fileserviceapiKey;
        this.fileserviceauthorization = this.objConstant.fileserviceauthorization;

        //
        this.priceManagementUrl = this.objConstant.priceManagementUrl;
        this.priceManagementapiKeyType = this.objConstant.priceManagementapiKeyType;
        this.priceManagementapiKey = this.objConstant.priceManagementapiKey;
        this.priceManagementauthorization = this.objConstant.priceManagementauthorization;

        //
        this.reportmanagementurl = this.objConstant.reportmanagementurl;
        this.reportmanagementType = this.objConstant.reportmanagementType;
        this.reportmanagementapiKey = this.objConstant.reportmanagementapiKey;
        this.reportmanagementauthorization = this.objConstant.reportmanagementauthorization;

        //config
        // this.configManagementurl = this.objConstant.configManagementurl;
        this.configManagementType = this.objConstant.configManagementType;
        // this.configManagementapiKey = this.objConstant.configManagementapiKey;
        // this.configManagementauthorization = this.objConstant.configManagementauthorization;

        //
        this.fulfilmenturl = this.objConstant.fulfilmenturl;
        this.fulfilmentType = this.objConstant.fulfilmentType;
        this.fulfilmentapiKey = this.objConstant.fulfilmentapiKey;
        this.fulfilmentauthorization = this.objConstant.fulfilmentauthorization;

        //catalogue
        this.cataloguetrackerurl = this.objConstant.cataloguetrackerurl;
        this.cataloguetrackerType = this.objConstant.cataloguetrackerType;
        this.cataloguetrackerapiKey = this.objConstant.cataloguetrackerapiKey;
        this.cataloguetrackerauthorization = this.objConstant.cataloguetrackerauthorization;

        //sales
        this.salesconfigrurl = this.objConstant.salesconfigrurl;
        this.salesconfigType = this.objConstant.salesconfigType;
        this.salesconfigapiKey = this.objConstant.salesconfigapiKey;
        this.salesconfigauthorization = this.objConstant.salesconfigauthorization;

    }

    // function execute on page load
    ngOnInit() {
        this._globalFunc.logger("Loading Login screen on UI");
        // set page state variable
        this.pageState = 0;
        sessionStorage.setItem('pageState', '0');
        this.showLoader = false;
        this.loginalert = false;
        this.errorMsg = '';
        this.username = '';
        this.password = '';
        this.loginError = true;
        document.getElementById('login').style.display = 'none';

        // page load default back to top function
        this._globalFunc.autoscroll();

        // following condition is for local development at offshore as login is not working here.
        if (this.objConstant.ENVIRNMNT == 'LOCAL') {
            /* this._globalFunc.logger("Setting user session for Local environment");
            sessionStorage.setItem("organisation", '[{"orgType":"Amazon","orgName":"amazon","imgUrl":"Amazon.png","permissions":{"menuPermission":{"catalogueEnquiry":"W","catalogueManagement":"W","orders":"W","reports":"W","accessMgmt":"W","raiseNewOrder":"W","dashboards":"W","orderMgmtClaims": "W"},"catalogManagementPermission":{"newProductRequestPermission":{"createSingleRequest":"W","manageRequest":"W","viewNISFeederExport":"W","viewAddToChannelStatusReport":"W"},"fullRangeExportPermission":{"viewFullRangeExports":"W","generateNewFullRangeExport":"W"},"priceIncreaseReviewPermission":{"productsIdentification":"W","pricingConfiguration":"W","productsRepricing":"W"},"changeSummaryReportPermission":{"viewChangeSummaryReports":"W","generateNewChangeSummaryReport":"W","manageDelistDates":"W"}},"catalogAdminPermission":{"addNewProducts":"W","addMasterData":"W","uploadCatalogue":"W","exportCatalogue":"W"},"dashboardsPermission":{"fulfilment":"W"}}},{"orgType":"Rontec","orgName":"rontec","imgUrl":"Rontec.png","permissions":{"menuPermission":{"catalogueEnquiry":"W","catalogueManagement":"W","orders":"W","reports":"W","accessMgmt":"W","raiseNewOrder":"W","dashboards":"W"},"catalogManagementPermission":{"newProductRequestPermission":{"createSingleRequest":"W","manageRequest":"W","viewNISFeederExport":"W","viewAddToChannelStatusReport":"W"},"fullRangeExportPermission":{"viewFullRangeExports":"W","generateNewFullRangeExport":"W"},"priceIncreaseReviewPermission":{"productsIdentification":"W","pricingConfiguration":"W","productsRepricing":"W"},"changeSummaryReportPermission":{"viewChangeSummaryReports":"W","generateNewChangeSummaryReport":"W","manageDelistDates":"W"}},"catalogAdminPermission":{"addNewProducts":"W","addMasterData":"W","uploadCatalogue":"W","exportCatalogue":"W"},"dashboardsPermission":{"fulfilment":"W"}}},{"orgType":"mccolls","orgName":"mccolls","imgUrl":"mccolls.png","permissions":{"menuPermission":{"catalogueEnquiry":"W","catalogueManagement":"W","orders":"W","reports":"W","accessMgmt":"W","raiseNewOrder":"W","dashboards":"W"},"catalogManagementPermission":{"newProductRequestPermission":{"createSingleRequest":"W","manageRequest":"W","viewNISFeederExport":"W","viewAddToChannelStatusReport":"W"},"fullRangeExportPermission":{"viewFullRangeExports":"W","generateNewFullRangeExport":"W"},"priceIncreaseReviewPermission":{"productsIdentification":"W","pricingConfiguration":"W","productsRepricing":"W"},"changeSummaryReportPermission":{"viewChangeSummaryReports":"W","generateNewChangeSummaryReport":"W","manageDelistDates":"W"}},"catalogAdminPermission":{"addNewProducts":"W","addMasterData":"W","uploadCatalogue":"W","exportCatalogue":"W"},"dashboardsPermission":{"fulfilment":"W"}}},{"orgType":"sandpiper","orgName":"sandpiper","imgUrl":"Sandpiper.png","permissions":{"menuPermission":{"catalogueEnquiry":"W","catalogueManagement":"W","orders":"W","reports":"W","accessMgmt":"W","raiseNewOrder":"W","dashboards":"W"},"catalogManagementPermission":{"newProductRequestPermission":{"createSingleRequest":"W","manageRequest":"W","viewNISFeederExport":"W","viewAddToChannelStatusReport":"W"},"fullRangeExportPermission":{"viewFullRangeExports":"W","generateNewFullRangeExport":"W"},"priceIncreaseReviewPermission":{"productsIdentification":"W","pricingConfiguration":"W","productsRepricing":"W"},"changeSummaryReportPermission":{"viewChangeSummaryReports":"W","generateNewChangeSummaryReport":"W","manageDelistDates":"W"}},"catalogAdminPermission":{"addNewProducts":"W","addMasterData":"W","uploadCatalogue":"W","exportCatalogue":"W"},"dashboardsPermission":{"fulfilment":"W"}}}]');
            sessionStorage.setItem("pageState", "1");
            sessionStorage.setItem('userId', "1");
            sessionStorage.setItem("catalogueurl", "aHR0cHM6Ly9zaXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlb3JkZXIvdjEv")
            sessionStorage.setItem('fulfilmenturl', btoa('https://wholesalecataloguemgmt.sit.np.morconnect.com/wholesale/v1/fulfilment/'));
            sessionStorage.setItem('loggedInUserGroupName', "MWEE-Admin");
            sessionStorage.setItem('loggedInUserAdminFlag', "true");
            sessionStorage.setItem('loggedInUserGroupId', "1");
            // sessionStorage.setItem("ConfigManagementapiKey", "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz");
            sessionStorage.setItem("catalogueType", "Catalogue");
            sessionStorage.setItem("catalogueapiKey", "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz");
            sessionStorage.setItem("catalogueauthorization", "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==");
            sessionStorage.setItem("cataloguemanagementType", "CatalogueManagement");
            sessionStorage.setItem("cataloguemanagementapiKey", "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz");
            sessionStorage.setItem("cataloguemanagementauthorization", "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==");
            sessionStorage.setItem("cataloguemanagementurl", "aHR0cHM6Ly9zaXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlL3YxLw==");
             */// sessionStorage.setItem("catalogueurl", "aHR0cHM6Ly9zaXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlb3JkZXIvdjEv");
            // sessionStorage.setItem("configManagementurl", "aHR0cHM6Ly9zaXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlL3YxLw==");
            /* sessionStorage.setItem("email", "mastek2@wholesale.com");
            sessionStorage.setItem("fileserviceType", "File");
            sessionStorage.setItem("fileserviceapiKey", "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz");
            sessionStorage.setItem("fileserviceauthorization", "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==");
            sessionStorage.setItem("fileserviceurl", "aHR0cHM6Ly9zaXQtYXBpLm1vcnJpc29ucy5jb20vZmlsZS92MQ==");
            sessionStorage.setItem("fulfilmentType", "Fulfilment");
            sessionStorage.setItem("fulfilmentapiKey", "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz");
            sessionStorage.setItem("fulfilmentauthorization", "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==");
            sessionStorage.setItem("name", "mastek2@wholesale.com");
            sessionStorage.setItem("orderApiKey", "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz");
            sessionStorage.setItem("orderAuthorization", "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==");
            sessionStorage.setItem("orderUrl", "aHR0cHM6Ly9zaXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlb3JkZXIvdjEv");
            sessionStorage.setItem("orderapiKeyType", "Order");
            sessionStorage.setItem("orgType", "mccolls");
            sessionStorage.setItem("productApiKey", "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz");
            sessionStorage.setItem("productAuthorization", "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==");
            sessionStorage.setItem("productUrl", "aHR0cHM6Ly91YXQtYXBpLm1vcnJpc29ucy5jb20vcHJvZHVjdC92MS8=");
            sessionStorage.setItem("productpricingType", "PriceManagement");
            sessionStorage.setItem("productpricingapiKey", "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz");
            sessionStorage.setItem("productpricingauthorization", "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==");
            sessionStorage.setItem("productpricingurl", "aHR0cHM6Ly93aG9sZXNhbGVjYXRhbG9ndWVtZ210LnNpdC5ucC5tb3Jjb25uZWN0LmNvbS93aG9sZXNhbGUvdjEv");
            sessionStorage.setItem("reportmanagementType", "ReportManagement");
            sessionStorage.setItem("reportmanagementapiKey", "LHn5X2zrjSlrdFKfhnzruE0gVLPkJCiz");
            sessionStorage.setItem("reportmanagementauthorization", "Basic TEhuNVgyenJqU2xyZEZLZmhuenJ1RTBnVkxQa0pDaXo6eXgyYXBBNjQzY1drcktBWQ==");
            sessionStorage.setItem("reportmanagementurl", "aHR0cHM6Ly9zaXQtYXBpLm1vcnJpc29ucy5jb20vd2hvbGVzYWxlL3YxLw==");
            sessionStorage.setItem("username", "mastek2@wholesale.com");
            sessionStorage.setItem("usrjsn", "eyJ1c2VybmFtZSI6Im1hc3RlazJAd2hvbGVzYWxlLmNvbSIsInBhc3N3b3JkIjoidGVzdHVzZXIyQFRodTIifQ=="); */

            /* login tracker MWEE-2799*/
            this.orgType = this.objConstant.customerName4Service;
            this.groupId = '@all';
            this.userId = sessionStorage.getItem("userId");
            //console.log(this._commonFuntion.dateFormator((dateForlogin).toDateString(), 7));
            let dt = this._commonFuntion.getUserSession();
            this._postsService.authLoginTracker(this.orgType, this.groupId, this.userId, dt).subscribe((userData: any) => {
                this._globalFunc.logger(userData);
                sessionStorage.setItem("usdi", userData.sessionId);
                this._router.navigate(['customerselection']);
            }, (error: any) => {
                this._globalFunc.logger(error);
            });
        }
        // navigation if already logged in
        if (sessionStorage.getItem('name') !== null && sessionStorage.getItem('name') !== 'undefined') {
            this._globalFunc.logger("Redirect user to customer selection if user is already registered");
            this._router.navigate(['customerselection']);
        }



        document.getElementById('nav-toggle').style.display = 'none';
        // logic for resize pop up 
        window.onresize = () => {
            document.getElementById('nav-toggle').style.display = 'none';
        };
        // sessionStorage.setItem('configManagementapiKey', this.objConstant.configManagementapiKey);
        // sessionStorage.setItem('configManagementType', this.objConstant.configManagementType);
        // sessionStorage.setItem('configManagementauthorization', this.objConstant.configManagementauthorization);
        // sessionStorage.setItem('configManagementurl', this.objConstant.configManagementurl);

        this.showLoader = true;
        this._postsService.siteMaintanceCall('1', 'all').subscribe(userData => {
            console.log(userData);
            console.log(userData.maintenanceDetails[0].message);
            console.log(userData.maintenanceDetails[0].start);
            console.log(userData.maintenanceDetails[0].status);
            if (userData.maintenanceDetails[0].moduleName == 'all') {
                if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                    this.siteMaintanceFlg = true;
                    this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                } else {
                    this.siteMaintanceFlg = false;
                }
            } else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        } , error => {
            console.log(error);
            if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                this.siteMaintanceFlg = true;
                this.siteMSg = "Site is under maintenance";
            } else if (error.errorCode == '404.26.408') {
                this.errorMsg = "User is Inactive";
            }
            else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        });


        //this.siteMaintanceFlg1 = sessionStorage.getItem('site');
        // if(sessionStorage.getItem('site') == "true"){
            
        //     this.siteMaintanceFlg1 = false;
        // }else { 
        //     this.siteMSg = "Site is under maintenance";
        //     this.siteMaintanceFlg1 = true;
        // }

    }

    // login function
    authLogin(form: any): void {
        this.showLoader = true;
        this._globalFunc.logger("Calling User management API");
        this._postsService.authLogin(form.username, form.password).subscribe(userData => {
            this._globalFunc.logger("Login Successful");
            console.log((userData));
            // user details
            sessionStorage.setItem('username', userData.credentials.userName);
            sessionStorage.setItem('email', userData.credentials.userName);
            sessionStorage.setItem('userId', userData.credentials.userId);

            this.showLoader = false;
            sessionStorage.setItem('name', userData.credentials.userName);
            sessionStorage.setItem('organisation', JSON.stringify(userData.organizations));
            sessionStorage.setItem('loggedInUserGroupName', userData.groupName);
            sessionStorage.setItem('loggedInUserGroupId', userData.groupId);
            sessionStorage.setItem('loggedInUserAdminFlag', userData.credentials.adminFlag);

            /* // product api details
            sessionStorage.setItem('productApiKey', this.productApiKey);
            sessionStorage.setItem('productAuthorization', this.productAuthorization);
            sessionStorage.setItem('productUrl', this.productUrl);

            // catalogue api details
            sessionStorage.setItem('catalogueapiKey', this.catalogueapiKey);
            sessionStorage.setItem('catalogueType', this.catalogueType);
            sessionStorage.setItem('catalogueauthorization', this.catalogueauthorization);
            sessionStorage.setItem('catalogueurl', this.catalogueurl);


            // catalogue management api details
            sessionStorage.setItem('cataloguemanagementapiKey', this.cataloguemanagementapiKey);
            sessionStorage.setItem('cataloguemanagementType', this.cataloguemanagementType);
            sessionStorage.setItem('cataloguemanagementauthorization', this.cataloguemanagementauthorization);
            sessionStorage.setItem('cataloguemanagementurl', this.cataloguemanagementurl);


            // file service api
            sessionStorage.setItem('fileserviceapiKey', this.fileserviceapiKey);
            sessionStorage.setItem('fileserviceType', this.fileserviceType);
            sessionStorage.setItem('fileserviceauthorization', this.fileserviceauthorization);
            sessionStorage.setItem('fileserviceurl', this.fileserviceurl);


            // product pricing api details
            sessionStorage.setItem('productpricingapiKey', this.priceManagementapiKey);
            sessionStorage.setItem('productpricingType', this.priceManagementapiKeyType);
            sessionStorage.setItem('productpricingauthorization', this.priceManagementauthorization);
            sessionStorage.setItem('productpricingurl', this.priceManagementUrl);

            //report management api details
            sessionStorage.setItem('reportmanagementapiKey', this.reportmanagementapiKey);
            sessionStorage.setItem('reportmanagementType', this.reportmanagementType);
            sessionStorage.setItem('reportmanagementauthorization', this.reportmanagementauthorization);
            sessionStorage.setItem('reportmanagementurl', this.reportmanagementurl);


            // ConfigManagement api details
            // sessionStorage.setItem('configManagementapiKey', this.configManagementapiKey);
            //sessionStorage.setItem('configManagementType', this.configManagementType);
            // sessionStorage.setItem('configManagementauthorization', this.configManagementauthorization); 
            // sessionStorage.setItem('configManagementurl', this.configManagementurl);

            // fulfilment api details
            sessionStorage.setItem('fulfilmentapiKey', this.fulfilmentapiKey);
            sessionStorage.setItem('fulfilmentType', this.fulfilmentType);
            sessionStorage.setItem('fulfilmentauthorization', this.fulfilmentauthorization);
            sessionStorage.setItem('fulfilmenturl', this.fulfilmenturl);

            //cataloguetracker
            sessionStorage.setItem('cataloguetrackerapiKey', this.cataloguetrackerapiKey);
            sessionStorage.setItem('cataloguetrackerType', this.cataloguetrackerType);
            sessionStorage.setItem('cataloguetrackerauthorization', this.cataloguetrackerauthorization);
            sessionStorage.setItem('cataloguetrackerurl', this.cataloguetrackerurl);

            //salesconfig
            sessionStorage.setItem('salesconfigapiKey', this.salesconfigapiKey);
            sessionStorage.setItem('salesconfigType', this.salesconfigType);
            sessionStorage.setItem('salesconfigauthorization', this.salesconfigauthorization);
            sessionStorage.setItem('salesconfigrurl', this.salesconfigrurl); */
            /*}

            /* login tracker MWEE-2799*/
            this.orgType = this.objConstant.customerName4Service;
            this.groupId = '@all';
            this.userId = sessionStorage.getItem("userId");
            //console.log(this._commonFuntion.dateFormator((dateForlogin).toDateString(), 7));
            let dt = this._commonFuntion.getUserSession();
            this._postsService.authLoginTracker(this.orgType, this.groupId, this.userId, dt).subscribe((userData: any) => {
                this._globalFunc.logger(userData);
                sessionStorage.setItem("usdi", userData.sessionId);
                this._router.navigate(['customerselection']);
            }, (error: any) => {
                console.log(error);
                if(error.header.statusText == 'Not found' || error.header.status == '404'){
                    this._router.navigate(['error404']);
                }
                this._globalFunc.logger(error);
            });
        }
            , (err) => {
                console.log(err);
                if (err.statusText == '' || err.httpResponseCode == '503' || err.httpResponseCode == '502' || err.httpResponseCode == '555') {
                    this.siteMaintanceFlg = true;
                    this.siteMSg = "Site is under maintenance";
                } else if (err.httpResponseCode == '404') {
                    this.loginError = false;
                    this.errorMsg = "Invalid login credentials";
                    this._globalFunc.logger("Invalid Login credentials");
                } else if (err.errorCode == '404.26.408') {
                    this.errorMsg = "User is Inactive";
                }

                else {
                    this.siteMaintanceFlg = false;
                }
                this.showLoader = false;
                this.loginError = false;
                this.errorMsg = "Invalid login credentials";
                this._globalFunc.logger("Invalid Login credentials");
                setTimeout(() => {
                    this.username = '';
                    this.password = '';
                    this.errorMsg = '';
                    this.loginError = true;
                }, 5000);
            }
        );
    }

    // logout user clear session
    logoutUser() {
        sessionStorage.clear();
        this._globalFunc.logger("User Logout");
        this._router.navigate(['']);
    }

    // navigate function
    redirect(page: any, catName: any, catId: any) {
        this._globalFunc.logger("Redirecting to " + page);
        this._router.navigate([page, catName, catId]);
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
        this._globalFunc.logger("Unloading login screen.");
        this._globalFunc.sentLoggerBatchToServer();
    }
}
