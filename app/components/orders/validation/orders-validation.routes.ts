import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrdersValidationComponent } from './orders-validation.component';
import { CanDeactivateGuard } from '../../../services/can-deactivate-guard.service';

const orderValidationRoutes: Routes = [
  { path: '', component: OrdersValidationComponent,canDeactivate: [CanDeactivateGuard]  }
];
@NgModule({
  imports: [RouterModule.forChild(orderValidationRoutes)],
  exports: [RouterModule]
})
export class OrdersValidationComponentRoutes { }
