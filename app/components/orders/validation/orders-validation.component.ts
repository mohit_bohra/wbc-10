import { Component, ViewEncapsulation } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../../services/app.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { commonfunction } from '../../../services/commonfunction.services';
import { PagerService } from '../../../services/pager.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {FormControl} from '@angular/forms';



@Component({
	templateUrl: 'orders-validation.component.html',
	styles: [`

        .page{
            width: 375px; 
            position: absolute; 
            top: 0; 
            right: 0; 
            text-align: right; 
            margin-right: 30px; 
            font-size: 12px;
        }
        
        .page  .pagination { margin: 5px 0 !important;}
        
        .page .pagination>.active>a, .page .pagination>.active>a:focus, .page .pagination>.active>a:hover, .page .pagination>.active>span, .page .pagination>.active>span:focus, .page .pagination>.active>span:hover { background-color : #004e36 !important;  } 
        
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
            border-color: #004e36 !important;
		}
		
		.custom-select.is-invalid,
		.form-control.is-invalid,
		.was-validated .custom-select:invalid,
		.was-validated .form-control:invalid {
			border-color: #dc3545
		}

    `],

    encapsulation: ViewEncapsulation.Emulated,
    
    providers: [commonfunction]
})
export class OrdersValidationComponent {
    objConstant = new Constant();
    terminateFlg: boolean;
    organisationListChange: any;
    organisationList: any;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    hasDuplicateAP: boolean = false;
    unsavedChange: boolean = false;
	allocationPriorities: any;
	imgName: string = "";
    popupMsg: string;
	disableSave: boolean = true;
    disableAddBtn: boolean = false;
    newRow: boolean = false;
    customerName: string;
    originalData: any = [];
	custPermission: any[];
	board: Object;
	showLoader: boolean;
	//variables for pagination
	pager: any = {};
	start: number;
    limit: number;
    count: number;
	hitNext: boolean;
	pagedItems: any[];
	OrderData: any;
	finalPageDisplay: boolean = true;

	//variables for adding a new row
	addRow: boolean = false;
	newOrder: boolean = false;
	
	//Form Validation
	validateForm: FormGroup;
	submitted:boolean = false;

	//for date calender
	public today: Date = new Date();
	public singleDate: any;
	public singleDateFormate: any;	
    public tomorrow = new Date(this.today.getTime() + 24 * 60 * 60 * 1000);

    public minDate: Date = this.tomorrow;
	//this.tomrrowDate.setDate(this.maxDate.getDate() + 7);
	public options: any = {
        locale: { format: 'YYYY-MM-DD' },
        alwaysShowCalendars: false,
        singleDatePicker: true,
        autoApply: false,
        minDate: this.minDate,
        autoUpdateInput: false,
        "drops": "up"

	};
	isAmazonCustomer:boolean = false;
	private _sub: any;
	whoImgUrl: any;
	homeUrl: any;
	nodatamsg:any;
	siteMaintanceFlg: boolean;
	siteMSg: any;
	errorMsgString: string;
    deploymentDetailsMsg: string;
    featureDetailsMsg: string;
    businessMessageDetailsMsg: string;
    deploymentDetailsMsglength:any;
    featureDetailsMsglength:any;
	businessMessageDetailsMsglength:any;
	recordsIfExit: any = 0;

	
    constructor(private fb:FormBuilder,private pagerService: PagerService,private _postsService: Services,private _route: ActivatedRoute,private _router: Router,private _cfunction: commonfunction, public _globalFunc: GlobalComponent) {
		this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
	}
    ngOnInit() {
		this._globalFunc = this._globalFunc;
		(<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
		(<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
		
		this._sub = this._route.params.subscribe((params: { orgType: string }) => {
			this.customerName = params.orgType;
			this.orgApiName = this.customerName;
			if (this.customerName == 'amazon') {
				this.isAmazonCustomer = true;
			}
			else{
				this.isAmazonCustomer = false;
			}	
			this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
				if(res == 'sessionActive')
				{ 
					this.setPage(1);
				}
			}, reason => {
				console.log(reason);
			});
		})
		
			this.validateForm=this.fb.group({
				maxQuantity:['',[Validators.required, Validators.pattern(/^[1-9]\d*$/)]], ///^\d{0,9}(\.\d{1,9})?$/
				startDate:[null, Validators.required],
				description:[null, [Validators.required,Validators.pattern(/^[a-zA-Z0-9_ ]*$/)]],
				createdBy:[null],
				customerId:[null],
				channel:[null],
				//changed_By:[null],
			});

	}
	
	
	
	//selection of date
	singleSelect(value: any,idx: any) {
		console.log("value");
		console.log(value);
		this.validateForm.patchValue({
			startDate :  value.start.format('YYYY-MM-DD')
		});
         //this.singleDate = value.start.format('DD/MM/YYYY');
         //this.singleDateFormate = value.start.format('YYYY-MM-DD');
    }

	
	 // convenience getter for easy access to form fields
	 get f() { return this.validateForm.controls; }


	 onSubmit() {
		this.submitted = true;

        // stop here if form is invalid
        if (this.validateForm.invalid) {			
            return;
		}
		this.showLoader = true;
		
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
					this.validateForm.patchValue({
						createdBy: sessionStorage.getItem('email'),
						customerId:this.customerName,
						channel:this.objConstant.channelType[this.customerName.toLowerCase()],

						//changed_By:Date.now()
					})
					console.log(JSON.stringify(this.validateForm.value));
					this._postsService.addMaxOrderInfo(this.customerName,this.validateForm.value).subscribe(data => {
						//console.log(data);
						this.popupMsg = "";
						//this.makeChangeToday = false;
						this.setPage(1);
						this.popupMsg = "Maximum order quantity is added successfully.";
						let sessionModal = document.getElementById('messageModal');
						sessionModal.classList.remove('in');
						sessionModal.classList.add('out');
						document.getElementById('messageModal').style.display = "block";
						this.cancelMaxOrder();	
						this.showLoader = false;
						
					}, error => {	
						
						//console.log(error);
						this.popupMsg = "";
						//this.makeChangeToday = false;
						this.popupMsg = "Problem in saving data.";
						let sessionModal = document.getElementById('messageModal');
						sessionModal.classList.remove('in');
						sessionModal.classList.add('out');
						document.getElementById('messageModal').style.display = "block";
						this.cancelMaxOrder();	
						this.showLoader = false;
					});	
							
				} else {
					this.showLoader = false;
				}
			}, reason => {
				console.log(reason);
			});
	}

    //add new row functionality
	addNewRow() {
		this.submitted = false;
		this.addRow=true;
		this.disableSave = false;
		this.newOrder=true;
	}
	
	setPage(page: number) {

        var recordPerPage =10;
        var totalPage =0;

        if (page < 1 || page > this.pager.totalPages) {
            return;
        }


        //debugger;
        
        this.start = ((page-1) * this.limit);
        this.showLoader = true;
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
				this._postsService.getAllMaxOrderValidationRS(this.customerName).subscribe(data => {
						
						
						this.OrderData = data.maxOrderValidation;                       
				

					
					// get pager object from service
					totalPage = this.OrderData.length;
					this.recordsIfExit = this.OrderData.length
					this.pager = this.pagerService.getPager(totalPage, page, recordPerPage);

					// get current page of items
					this.pagedItems = this.OrderData.slice(this.pager.startIndex, this.pager.endIndex + 1);
					
					this.OrderData =this.pagedItems;
					}
					, error => {
						console.log(error);
						if (error.errorCode == '404.33.410' || error.httpResponseCode == '404') {
							this.OrderData = [];
							this.nodatamsg = "Sorry, No data found at this moment.";
							this.showLoader = false;
						} else {
							this._router.navigate(['/error404']);
						}
			
					}
				);
				this.showLoader = false;
			}
			else {
				this.showLoader = false;
			}
		}, reason => {
            console.log(reason);
        });

        
	}
	
	//cancel button
	cancelMaxOrder(){
	 this.validateForm.reset()
	 this.addRow=false;
	 this.newOrder=false;

	}


	getCustomerImage(custName: string) {
		return this._globalFunc.getCustomerImage(custName);
	}

    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
		return new Promise((resolve, reject) => {
			this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
				if (!userData.isSessionActive) { // false
					// User session has expired.
					if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
						console.log("Session terminated");
						reject('sessionInactive');
						this.terminateFlg = true;
						let sessionModal = document.getElementById('popup_SessionExpired');
						sessionModal.classList.remove('in');
						sessionModal.classList.add('out');
						document.getElementById('popup_SessionExpired').style.display = 'block';
					} else {
						reject('sessionInactive');
						sessionStorage.clear();
						this._router.navigate(['']);
					}
				} else {
					if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
						console.log("Session terminated");
						reject('sessionInactive');
						this.terminateFlg = true;
						let sessionModal = document.getElementById('popup_SessionExpired');
						sessionModal.classList.remove('in');
						sessionModal.classList.add('out');
						document.getElementById('popup_SessionExpired').style.display = 'block';
					} else {
						resolve('sessionActive');
						this.terminateFlg = false;
						this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
						let changeOrgAvailable: any = [];
						let orgImgUrl = "";
						let orgApiNameTemp = "";
						this.organisationList.forEach(function (item: any) {
							if (item.customerName !== customerName) {
								changeOrgAvailable.push(item);
							} else {
								orgImgUrl = item.customerName + ".png";
								orgApiNameTemp = item.customerName;
							}
						});
						this.organisationListChange = changeOrgAvailable;
						this.orgimgUrl = orgImgUrl;
						this.orgApiName = orgApiNameTemp;
						// this.disabledCusChange = false;
						if (this.organisationList.length == 1) {
							this.disabledCusChange = true;
						}
					}

				}
			});
		});
	}  
	
	closePopup(modalBoxId = "") {
        if (modalBoxId != "") {
            let sessionModal = document.getElementById(modalBoxId);
            sessionModal.classList.remove('out');
            sessionModal.classList.add('in');
            sessionModal.style.display = 'none';
        } else {
            let sessionModal = document.getElementById('popup_SessionExpired');
            sessionModal.classList.remove('out');
            sessionModal.classList.add('in');
            sessionModal.style.display = 'none';
            let dt = this._cfunction.getUserSession();
            this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            }, (error: any) => {
            });
            sessionStorage.clear();
            this._router.navigate(['']);
        }

	}
	
	ngOnDestroy() {
        this._globalFunc.logger("Max order validation screen unloading");
        this._globalFunc.sentLoggerBatchToServer();
    }
   
	getConfigService(pageName: any) {
        this._postsService.siteMaintanceCall('1', 'all').subscribe(userData => {
            console.log(userData);
            console.log(userData.maintenanceDetails[0].message);
            console.log(userData.maintenanceDetails[0].start);
            console.log(userData.maintenanceDetails[0].status);
            console.log(userData.deploymentDetails);
            if (userData.deploymentDetails != undefined && userData.deploymentDetails[0].status != 'NO') {
                this.deploymentDetailsMsg = userData.deploymentDetails[0].message + ' ' + userData.deploymentDetails[0].start;
                 this.deploymentDetailsMsglength= this.deploymentDetailsMsg.length;
                   if (sessionStorage.getItem('deploymentDetailsMsglength') == undefined || sessionStorage.getItem('deploymentDetailsMsglength') == null) {
                    sessionStorage.setItem('deploymentDetailsMsglength', this.deploymentDetailsMsglength);
                    console.log(this.deploymentDetailsMsglength +"this.deploymentDetailsMsglength");
                } else {
                    this.deploymentDetailsMsglength = sessionStorage.getItem('deploymentDetailsMsglength');
                    console.log(this.deploymentDetailsMsglength +"this.deploymentDetailsMsglength");
                }

            } else {
                this.deploymentDetailsMsg = '';
            }


            console.log(userData.featureDetails);
            if (userData.featureDetails != undefined && userData.featureDetails[0].status != 'NO') {
                this.featureDetailsMsg = userData.featureDetails[0].message + ' ' + userData.featureDetails[0].start;
                this.featureDetailsMsglength = this.featureDetailsMsg.length;
                if (sessionStorage.getItem('featureDetailsMsglength') == undefined || sessionStorage.getItem('featureDetailsMsglength') == null) { 
                     sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
                }
                else {
                    this.deploymentDetailsMsglength = sessionStorage.getItem('featureDetailsMsglength');
                }
            } else {
                this.featureDetailsMsg = '';
            }

            console.log(userData.businessMessageDetails);
            if (userData.businessMessageDetails != undefined && userData.businessMessageDetails[0].status != 'NO') {
                this.businessMessageDetailsMsg = userData.businessMessageDetails[0].message + ' ' + userData.businessMessageDetails[0].start;
                this.businessMessageDetailsMsglength = this.businessMessageDetailsMsg.length;
                if (sessionStorage.getItem('businessMessageDetailsMsglength') == undefined || sessionStorage.getItem('businessMessageDetailsMsglength') == null) {                    sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
                    sessionStorage.setItem('businessMessageDetailsMsglength', this.businessMessageDetailsMsglength);
                }
                else {
                   this.businessMessageDetailsMsglength = sessionStorage.getItem('businessMessageDetailsMsglength');
                }
            } else {
                this.businessMessageDetailsMsg = '';
            }

             

            (<HTMLLabelElement>document.getElementById('deploymentDetailsId')).textContent = this.deploymentDetailsMsg;
            (<HTMLLabelElement>document.getElementById('featureDetailsId')).textContent = this.featureDetailsMsg;
            (<HTMLLabelElement>document.getElementById('businessMessageDetailsId')).textContent = this.businessMessageDetailsMsg;
            if (userData.maintenanceDetails[0].moduleName == 'all') {
                if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                    this.siteMaintanceFlg = true;
                    this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                } else {
                    this.siteMaintanceFlg = false;
                    this._postsService.siteMaintanceCall(this.customerName, pageName).subscribe(userData => {
                        console.log(userData);
                        console.log(userData.maintenanceDetails[0].message);
                        console.log(userData.maintenanceDetails[0].start);
                        console.log(userData.maintenanceDetails[0].status);
                        if (userData.maintenanceDetails[0].moduleName == pageName) {
                            if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                                this.siteMaintanceFlg = true;
                                this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                            } else {
                                this.siteMaintanceFlg = false;
                            }
                        } else {
                            this.siteMaintanceFlg = false;
                        }
                        this.showLoader = false;
                    }, error => {
                        console.log(error);
                        if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                            this.siteMaintanceFlg = true;
                            this.siteMSg = "Site is under maintenance";
                        } else if (error.errorCode == '404.26.408') {
                            this.errorMsgString = "User is Inactive";
                        }
                        else {
                            this.siteMaintanceFlg = false;
                        }
                        this.showLoader = false;
                    });
                }
            } else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        }, error => {
            console.log(error);
            if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                this.siteMaintanceFlg = true;
                this.siteMSg = "Site is under maintenance";
            } else if (error.errorCode == '404.26.408') {
                this.errorMsgString = "User is Inactive";
            }
            else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        });
    }
	


   


}