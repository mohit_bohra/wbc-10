import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';
import { commonfunction } from '../../../services/commonfunction.services';
import { DatepickerModule } from '../../datepicker/ng2-bootstrap';
import { OrdersValidationComponentRoutes } from './orders-validation.routes';
import { OrdersValidationComponent } from './orders-validation.component';
import { lineBreakStr } from '../../pipe/line-break-str.pipe';

import { Daterangepicker } from 'ng2-daterangepicker';

@NgModule({
  imports: [
    OrdersValidationComponentRoutes,
    CommonModule,
    FormsModule,
    DatepickerModule.forRoot(),
    NavigationModule,
    ReactiveFormsModule,
   // FormsModule,
    Daterangepicker
  ],
  exports: [],
  declarations: [OrdersValidationComponent, lineBreakStr] ,
  providers: [Services, GlobalComponent, commonfunction]
})
export class OrdersValidationModule { }
