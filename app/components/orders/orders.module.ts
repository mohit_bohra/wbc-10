import { NgModule } from '@angular/core';
import { OrdersComponentRoutes } from './orders.routes';
import { OrdersComponent } from './orders.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../global/global.component';
import { Services } from '../../services/app.services';
import { FormsModule } from '@angular/forms';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { TooltipModule } from "ngx-tooltip";
import { NavigationModule } from '../navigationFulfilment/navigation.module';
import { DatepickerModule } from '../datepicker/ng2-bootstrap';
@NgModule({
  imports: [
    OrdersComponentRoutes,
    CommonModule,
    FormsModule,
    DatepickerModule.forRoot(),
    Ng2OrderModule,
    NavigationModule
  ],
  exports: [],
  declarations: [OrdersComponent],
  providers: [Services, GlobalComponent]
})
export class OrdersModule { }
