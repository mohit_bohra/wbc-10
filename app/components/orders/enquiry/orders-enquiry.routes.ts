import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrdersEnquiryComponent } from './orders-enquiry.component';

const ordersEnquiryRoutes: Routes = [
  { path: '', component: OrdersEnquiryComponent }
];
@NgModule({
  imports: [RouterModule.forChild(ordersEnquiryRoutes)],
  exports: [RouterModule]
})
export class OrdersEnquiryComponentRoutes { }
