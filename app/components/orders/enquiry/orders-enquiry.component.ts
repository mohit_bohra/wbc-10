import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../../services/app.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { commonfunction } from '../../../services/commonfunction.services';

@Component({
    templateUrl: 'orders-enquiry.component.html',
})
export class OrdersEnquiryComponent {
    terminateFlg: boolean;
    public showDatepicker: boolean = true;
    objConstant = new Constant();
    whoImgUrl: any;
    orderList: any;
    orderListLength: any;
    orderno: string;
    productcode: string;
    errorMsg: boolean;
    showLoader: boolean;
    printHeader: boolean;
    hideOrganisation: boolean = true;
    advSearch: boolean = true;
    orderResult: boolean = true;
    pageState: number = 2;
    customerName: string;
    style: string;
    customerDispayName: string;
    organisationList: any;
    organisationListChange: any;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledDashboard: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disabledOrderRaise: boolean;
    selectStatus: any;
    selectChannels: any;
    disabledManageCatalogue: boolean;
    public maxDate: Date = new Date();
    public myDate = new Date();
    channel: any;
    status: any;
    shipto: any;
    public deliverydate: Date = new Date();
    invoiceId: any;
    channelsshow: boolean;
    statusshow: boolean;
    shiptoshow: boolean;
    deliverydateshow: boolean;
    invoiceIdshow: boolean;
    public orderdate: Date = new Date();
    public invoicedate: Date = new Date();
    shipfrom: any;
    orderdateshow: boolean;
    invoicedateshow: boolean;
    shipfromshow: boolean;
    fieldSelected = new Array;
    fieldSelectedCount: number = 0;
    isInvoicePresent: boolean;
    homeUrl: any;
    private _sub: any;
    custPermission: any[];
    sortByColumn: string = '';
    reverseSorting: boolean = false;
    invoiceFileLocation: any;
    imgName: string = "";
    channelList: any = {};
    messageType: any = "All";
    channelSubType: any = "All";
    channelSubTypeList: any = {};
    iscustomerAmazon : boolean = false;

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.channelList = this.objConstant.channelList;
        this.channelSubTypeList = this.objConstant.channelSubTypeList;
        location.onPopState(() => {
            document.getElementById("resultModalClose").click();
            document.getElementById("advSearchClose").click();
        });
        this.orderdate = void 0;
        this.deliverydate = void 0;
        this.invoicedate = void 0;
        this.maxDate = new Date();
        this.myDate.setDate(this.maxDate.getDate() + 7);
        this.errorMsg = false;
    }
    ngOnInit() {
        this._globalFunc.logger("Order screen loading");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._globalFunc.logger("User session not found");
            this._router.navigate(['']);
        } else {
            this.shipfromshow = true;
            this.printHeader = true;
            this.showLoader = false;
            this.orderno = '';
            this.productcode = '';
            this.shipfrom = '';
            this.shipto = '';
            this.status = '';
            this.channel = '';
            this.invoiceId = '';
            this.shiptoshow = false;
            this.statusshow = false;
            this.channelsshow = true;
            this.invoiceIdshow = false;
            this.disabledManageCatalogue = false;
            this.disabledCusChange = false;
            this.disabledPermission = false;
            this.disabledDashboard = false;
            this.disabledReports = false;
            this.disabledCatalogue = false;
            this.disabledOrderRaise = false;
            this.selectStatus = [{ status: 'Please select', value: '' },
            { status: 'Raised', value: 'raised' },
            { status: 'Confirmed', value: 'confirmed' },
            { status: 'Confirmed Partial', value: 'confirmed-partial' },
            { status: 'Picked', value: 'picked' },
            { status: 'Picked Partial', value: 'picked-partial' },
            { status: 'Shipped', value: 'shipped' },
            { status: 'Shipped Partial', value: 'shipped-partial' },
            { status: 'Receipted', value: 'receipted' },
            { status: 'Receipted Partial', value: 'receipted-partial' },
            { status: 'Accounted', value: 'accounted' },
            { status: 'Accounted Partial', value: 'accounted-partial' },
            { status: 'Invoiced', value: 'invoiced' },
            { status: 'Invoiced Partial', value: 'invoiced-partial' },
            { status: 'Complete', value: 'complete' },
            { status: 'Errored', value: 'errored' },
            { status: 'Errored Price', value: 'errored-price' },
            { status: 'Deferred', value: 'deferred' },
            { status: 'Error Reported', value: 'error-reported' },
            { status: 'Order On Hold', value: 'order-on-hold' },
            { status: 'Partial-Claimed', value:'partial-claimed'},
            { status: 'Claimed', value:'claimed'},
            { status: 'Cancelled', value:'cancelled'},
            { status: 'Store-Not-Found', value:'store-not-found'}          
           

            ];
            this._globalFunc.autoscroll();
            this.pageState = 2;
            sessionStorage.setItem('pageState', '2');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                console.log("Customer Name" + this.customerName);
                
                if(this.customerName == "amazon")
                {
                    this.iscustomerAmazon = true;
                    console.log("Customer Name"+this.iscustomerAmazon);
                }
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;

                this.imgName = this.getCustomerImage(cusType);
                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
                let custPerm: any = [];
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = this.customerName;
            });
            let wid: any;
            wid = window.innerWidth;
            let screen: string;
            screen = this._globalFunc.getDevice(wid);
            window.onresize = () => {
                if (screen == "desktop") {
                    if (window.innerWidth < this._globalFunc.stdWidth) {

                    } else {

                    }
                } else {
                    if (window.innerWidth > this._globalFunc.stdWidth) {

                        document.getElementById("resultModalClose").click();
                    } else {

                    }
                }
                screen = this._globalFunc.getDevice(window.innerWidth);
            };
            window.onscroll = () => {
                this.scrollFunction();
            };
        }
    }
    public clear(calId: number): void {
        if (calId == 1) {
            this.orderdate = void 0;
        } else if (calId == 2) {
            this.deliverydate = void 0;
        } else if (calId == 3) {
            this.invoicedate = void 0;
        }
        this.toggaleCalender(calId);
    }
    showPopup() {
        this.showDatepicker = true;
    }

    toggaleCalender(calId: number) {
        if (calId == 1) {
            if (document.getElementById("orderDatePicker").style.display == "none") {
                document.getElementById("orderDatePicker").style.display = "block";
                document.getElementById("deliveryDatePicker").style.display = "none";
                document.getElementById("invoiceDatePicker").style.display = "none";
            } else {
                document.getElementById("orderDatePicker").style.display = "none";
                document.getElementById("deliveryDatePicker").style.display = "none";
                document.getElementById("invoiceDatePicker").style.display = "none";
            }
            if (document.getElementById("orderDatePickerMob").style.display == "none") {
                document.getElementById("orderDatePickerMob").style.display = "block";
                document.getElementById("deliveryDatePickerMob").style.display = "none";
                document.getElementById("invoiceDatePickerMob").style.display = "none";
            } else {
                document.getElementById("orderDatePickerMob").style.display = "none";
                document.getElementById("deliveryDatePickerMob").style.display = "none";
                document.getElementById("invoiceDatePickerMob").style.display = "none";
            }

        } else if (calId == 2) {
            if (document.getElementById("deliveryDatePicker").style.display == "none") {
                document.getElementById("deliveryDatePicker").style.display = "block";
                document.getElementById("orderDatePicker").style.display = "none";
                document.getElementById("invoiceDatePicker").style.display = "none";
            } else {
                console.log("In");
                document.getElementById("orderDatePicker").style.display = "none";
                document.getElementById("deliveryDatePicker").style.display = "none";
                document.getElementById("invoiceDatePicker").style.display = "none";
            }

            if (document.getElementById("deliveryDatePickerMob").style.display == "none") {
                document.getElementById("deliveryDatePickerMob").style.display = "block";
                document.getElementById("orderDatePickerMob").style.display = "none";
                document.getElementById("invoiceDatePickerMob").style.display = "none";
            } else {
                document.getElementById("orderDatePickerMob").style.display = "none";
                document.getElementById("deliveryDatePickerMob").style.display = "none";
                document.getElementById("invoiceDatePickerMob").style.display = "none";
            }

        } else if (calId == 3) {
            if (document.getElementById("invoiceDatePicker").style.display == "none") {
                document.getElementById("invoiceDatePicker").style.display = "block";
                document.getElementById("orderDatePicker").style.display = "none";
                document.getElementById("deliveryDatePicker").style.display = "none";
            } else {
                document.getElementById("orderDatePicker").style.display = "none";
                document.getElementById("deliveryDatePicker").style.display = "none";
                document.getElementById("invoiceDatePicker").style.display = "none";
            }

            if (document.getElementById("invoiceDatePickerMob").style.display == "none") {
                document.getElementById("invoiceDatePickerMob").style.display = "block";
                document.getElementById("orderDatePickerMob").style.display = "none";
                document.getElementById("deliveryDatePickerMob").style.display = "none";
            } else {
                document.getElementById("orderDatePickerMob").style.display = "none";
                document.getElementById("deliveryDatePickerMob").style.display = "none";
                document.getElementById("invoiceDatePickerMob").style.display = "none";
            }
        }
    }

    redirect(page: any, orgType: any) {
        this._globalFunc.logger("Redirecting to page." + page + " for customer " + orgType);
        this._router.navigate([page, orgType]);
    }

    redirect_detail(page: any, orgType: any, orderno: any) {
        this.getUserSession(orgType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {

                this._globalFunc.logger("Redirecting to Order drilldown screen.");
                let url = this.homeUrl + page + '/' + encodeURIComponent(orgType) + '/' + orderno;
                console.log(url);
                sessionStorage.setItem("pageState", "999");
                window.open(url);

            } else {
                sessionStorage.clear();
                this._router.navigate(['']);
            }
        }, reason => {
            console.log(reason);
        });
    }

    exportData() {
        this._globalFunc.logger("Export to excel.");
        return this._globalFunc.exportData('exportable', this.customerName + 'orders');
    }

    printOrder() {
        this._globalFunc.logger("Printing drilldown.");
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        var printContentsNav = document.getElementById('printNav').innerHTML;
        var printContents = document.getElementById('exportable').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContentsNav + printContents + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 5000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContentsNav + printContents + '</body></html>');
            popup.document.close();

        }


    }
    changeOrgType(page: any, orgType: any) {
        this._globalFunc.logger("Changing customer to " + orgType);
        this.orderno = '';
        this.productcode = '';
        this.shipto = '';
        this.shipfrom = '';
        this.status = '';
        this.channel = '';
        this.invoiceId = '';
        this.orderdate = void 0;
        this.deliverydate = void 0;
        this.invoicedate = void 0;
        this.fieldSelected = new Array;
        this.fieldSelectedCount = 0;

        this.shipfromshow = false;
        this.shiptoshow = false;
        this.statusshow = false;
        this.channelsshow = true;

        document.getElementById("orderIdSearch").classList.remove('disable');
        document.getElementById("orderIdSearchMob").classList.remove('disable');
        document.getElementById("productcodeSearch").classList.remove('disable');
        document.getElementById("productcodeSearchMob").classList.remove('disable');

        this.orderList = [];
        this.orderResult = true;
        if (this.advSearch == false) {
            this.advSearch = true;
            let advSearchIcon = document.getElementById('advSearchIcon');
            advSearchIcon.classList.remove('glyphicon-menu-up');
            advSearchIcon.classList.add('glyphicon-menu-down');
            let getOrderDiv = document.getElementById('orderIdSearch');
            getOrderDiv.classList.remove('disable');
            let getProductDiv = document.getElementById('productcodeSearch');
            getProductDiv.classList.remove('disable');
        }
        this.messageType = 'All';
        this.channelSubType = 'All';
        this.toggleOrganisation();
        sessionStorage.setItem('orgType', orgType);
        //this._router.navigate([page, orgType, "enquiry"]);
        this._router.navigate(this._globalFunc.redirectToComponent('orderEnquiry', orgType));

    }
    getInvoiceDownload(orderId: any) {

        var orderList = this.orderList;
        orderList.forEach(function (orderList: any) {
            if (orderList.orderId == orderId) {
                orderList.statusCycle.forEach(function (statusCycle: any) {
                    if (statusCycle.cycle == "invoice") {
                        if (statusCycle.params.invoiceFileLocation != undefined || statusCycle.params.invoiceFileLocation != null) {
                            window.open(statusCycle.params.invoiceFileLocation);
                        }
                    }
                });
            }
        });

    }
    ngOnDestroy() {
        this._globalFunc.logger("Order screen unloading");
        this._globalFunc.sentLoggerBatchToServer();
    }
    toggleOrganisation() {
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
            border.classList.remove('searchborder');
        } else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
            border.classList.add('searchborder');
        }
    }
    mobAdvSearch() {
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
        document.getElementById("productcodeSearchMob").classList.remove('disable');
        document.getElementById("orderIdSearchMob").classList.remove('disable');
        this.orderno = '';
        this.productcode = '';
        this.shipto = '';
        this.shipfrom = '';
        this.status = '';
        this.channel = '';
        this.invoiceId = '';
        this.orderdate = void 0;
        this.deliverydate = void 0;
        this.invoicedate = void 0;
        this.fieldSelected = new Array;
        this.fieldSelectedCount = 0;
        this.shiptoshow = false;
        this.statusshow = false;
        this.invoiceIdshow = false;
        document.getElementById("deliverydateshow").classList.remove('disable');
        document.getElementById("deliverydateshowmob").classList.remove('disable');
        document.getElementById("orderDatePickerMob").style.display = "none";
        document.getElementById("deliveryDatePickerMob").style.display = "none";
        document.getElementById("invoiceDatePickerMob").style.display = "none";
        this.shipfromshow = true;
        document.getElementById("orderdateshowmob").classList.remove('disable');
        document.getElementById("invoicedateshowmob").classList.add('disable');
    }
    toggleAdvSearch() {
        if (this.advSearch) {
            this.orderno = '';
            this.productcode = '';
            this.shipto = '';
            this.shipfrom = '';
            this.status = '';
            this.channel = '';
            this.invoiceId = '';
            this.orderdate = void 0;
            this.deliverydate = void 0;
            this.invoicedate = void 0;
            this.fieldSelected = new Array;
            this.fieldSelectedCount = 0;
            this.shiptoshow = false;
            this.statusshow = false;
            this.channelsshow = true;
            this.invoiceIdshow = false;
            document.getElementById("deliverydateshow").classList.remove('disable');
            document.getElementById("deliverydateshowmob").classList.remove('disable');
            document.getElementById("orderDatePicker").style.display = "none";
            document.getElementById("deliveryDatePicker").style.display = "none";
            document.getElementById("invoiceDatePicker").style.display = "none";
            document.getElementById("orderDatePickerMob").style.display = "none";
            document.getElementById("deliveryDatePickerMob").style.display = "none";
            document.getElementById("invoiceDatePickerMob").style.display = "none";
            this.shipfromshow = true;
            document.getElementById("orderdateshow").classList.remove('disable');
            document.getElementById("invoicedateshow").classList.add('disable');
            document.getElementById("orderdateshowmob").classList.add('disable');
            document.getElementById("invoicedateshowmob").classList.add('disable');
            this.advSearch = false;
            let advSearchIcon = document.getElementById('advSearchIcon');
            advSearchIcon.classList.remove('glyphicon-menu-down');
            advSearchIcon.classList.add('glyphicon-menu-up');
            let getOrderDiv = document.getElementById('orderIdSearch');
            getOrderDiv.classList.add('disable');
            let getProductDiv = document.getElementById('productcodeSearch');
            getProductDiv.classList.add('disable');
        } else {
            this.advSearch = true;
            let advSearchIcon = document.getElementById('advSearchIcon');
            advSearchIcon.classList.remove('glyphicon-menu-up');
            advSearchIcon.classList.add('glyphicon-menu-down');
            let getOrderDiv = document.getElementById('orderIdSearch');
            getOrderDiv.classList.remove('disable');
            let getProductDiv = document.getElementById('productcodeSearch');
            getProductDiv.classList.remove('disable');
        }
    }
    getSingleOrder(value: any) {
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                if (value.orderno == "") {
                } else {
                    this.showLoader = true;
                    this.sortByColumn = '';
                    this.reverseSorting = false;
                    this._postsService.getSingleOrder(this.orgApiName, value.orderno).subscribe(data => {
                        console.log(data);
                        this.showLoader = false;
                        this.orderList = data.orders;
                        if (data.orders.length == 0) {
                            this.orderListLength = 0;
                            document.getElementById('searchSingleText').style.display = 'none';
                            document.getElementById('searchAdvText').style.display = 'none';
                        } else {
                            this.orderListLength = data.orders.length;
                            document.getElementById('searchSingleText').style.display = 'inline';
                            document.getElementById('searchAdvText').style.display = 'none';
                            setTimeout(() => {
                                var elmnt = document.getElementById("exportable1");
                                elmnt.scrollLeft = 0;
                                elmnt.scrollTop = 0;
                            }, 1500);
                        }
                        var isinvoice: boolean;
                        this.orderList.forEach(function (orders: any) {
                            orders.statusCycle.forEach(function (statusCycle: any) {
                                if (statusCycle.cycle == "invoice") {
                                    console.log(statusCycle.params.invoiceFileLocation);
                                    if (statusCycle.params.invoiceFileLocation != undefined) {
                                        console.log("i m inside");
                                        isinvoice = true;
                                        console.log(isinvoice + "isinvoice");
                                        orders['isInvoice'] = isinvoice;
                                    } else {
                                        console.log("false");
                                        isinvoice = false;
                                        console.log(isinvoice + "isinvoice");
                                        orders['isInvoice'] = isinvoice;
                                    }
                                }
                                isinvoice = false;
                            });

                        });
                        console.log(this.orderList);
                        this.getInvoiceforRelevantOrder();
                        if (window.innerWidth < this._globalFunc.stdWidth) {
                            document.getElementById("openModalResult").click();
                        } else {
                            this.orderResult = false;
                            window.scrollBy(0, 2000);
                        }
                    }, err => {
                        this.showLoader = false;
                        this.orderListLength = 0;
                        this._router.navigate(['/error404']);
                    }
                    );
                }
            } else {
                this.showLoader = false;
            }
        }, reason => {
            console.log(reason);
        });
    }

    getInvoiceforRelevantOrder() {
        var orderList = this.orderList;
        var isinvoice: boolean;
        orderList.forEach(function (orders: any) {
            orders.statusCycle.forEach(function (statusCycle: any) {
                if (statusCycle.cycle == "invoice") {
                    if (statusCycle.params.invoiceFileLocation != undefined || statusCycle.params.invoiceFileLocation != null) {
                        isinvoice = true;
                    }
                }
            });
        });
        if (isinvoice == true) {
            this.isInvoicePresent = false;
        }
        else {
            this.isInvoicePresent = true;
        }
    }
    getReleventOrders(value: any) {
        this._globalFunc.logger("Order advance search form submit.");
        console.log(value);
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
        let indexkey1 = '', indexvalue1 = '', indexkey2 = '', indexvalue2 = '', filterkey, filtervalue;
        if (value.orderdatefrom != undefined && value.orderdatefrom != '') {
            let orderdatefrom = value.orderdatefrom.split('/');
            let createdAt = orderdatefrom[2] + '-' + orderdatefrom[1] + '-' + orderdatefrom[0];
            if (indexkey1 == undefined || indexkey1 == '') {
                indexkey1 = 'orderRaisedDate';
                indexvalue1 = createdAt;
            } else {
                indexkey2 = 'orderRaisedDate';
                indexvalue2 = createdAt;
            }
        }
        if (value.status != undefined && value.status != '') {
            if (indexkey1 == undefined || indexkey1 == '') {
                indexkey1 = 'statusCurrent';
                indexvalue1 = value.status;
            } else {
                indexkey2 = 'statusCurrent';
                indexvalue2 = value.status;
            }
        }
        if (value.shipto != undefined && value.shipto != '') {
            if (indexkey1 == undefined || indexkey1 == '') {
                indexkey1 = 'shipToLocationId';
                indexvalue1 = value.shipto;
            } else {
                indexkey2 = 'shipToLocationId';
                indexvalue2 = value.shipto;
            }
        }
        if (value.deliverydatefrom != undefined && value.deliverydatefrom != '') {
            let deliverydatefrom = value.deliverydatefrom.split('/');
            let shipToDeliverAt = deliverydatefrom[2] + '-' + deliverydatefrom[1] + '-' + deliverydatefrom[0];

            if (indexkey1 == undefined || indexkey1 == '') {
                indexkey1 = 'shipToDeliverAt';
                indexvalue1 = shipToDeliverAt;
            } else {
                indexkey2 = 'shipToDeliverAt';
                indexvalue2 = shipToDeliverAt;
            }
        }
        if (value.invoiceId != undefined && value.invoiceId != '') {
            if (indexkey1 == undefined || indexkey1 == '') {
                indexkey1 = 'billToId';
                indexvalue1 = value.invoiceId;
            } else {
                indexkey2 = 'billToId';
                indexvalue2 = value.invoiceId;
            }
        }
        if (value.invoicedatefrom != undefined && value.invoicedatefrom != '') {
            let invoicedatefrom = value.invoicedatefrom.split('/');
            let billToAt = invoicedatefrom[2] + '-' + invoicedatefrom[1] + '-' + invoicedatefrom[0];
            filterkey = 'billToDate';
            filtervalue = billToAt;
        } else if (value.shipfrom != undefined && value.shipfrom != '') {
            filterkey = 'shipFromLocationId';
            filtervalue = value.shipfrom;
        } else {
            filterkey = '';
            filtervalue = '';
        }
        if (indexkey1 == '') {
            this.errorMsg = true;
            setTimeout(() => {
                this.errorMsg = false;
            }, 5000);
        } else {
            document.getElementById('advSearchClose').click();
            this.showLoader = true;

            this.sortByColumn = '';
            this.reverseSorting = false;
            this._globalFunc.logger("Indexkey1 : " + indexkey1 + ", Indexvalue1 : " + indexvalue1 + ", Indexkey2 : " + indexkey2 + ", Indexvalue2 : " + indexvalue2 + ", filterkey : " + filterkey + ", filtervalue : " + filtervalue);
            this._postsService.getReleventOrders(this.orgApiName, indexkey1, indexvalue1, indexkey2, indexvalue2, filterkey, filtervalue).subscribe(data => {
                if (data.orders == undefined) {
                    this._globalFunc.logger("Undefined response");
                    this.orderListLength = 0;
                } else {
                    if (data.orders.length == 0) {
                        this._globalFunc.logger("Response having no orders");
                        this.orderListLength = 0;
                    } else {
                        this._globalFunc.logger("Orders found");
                        this.orderListLength = data.orders.length;

                        this.orderList = data.orders;
                        console.log(this.orderList);
                        var orderList = this.orderList;
                        var isinvoice: boolean;
                        orderList.forEach(function (orders: any) {
                            orders.statusCycle.forEach(function (statusCycle: any) {
                                if (statusCycle.cycle == "invoice") {
                                    console.log(statusCycle.params.invoiceFileLocation);
                                    if (statusCycle.params.invoiceFileLocation != undefined) {
                                        console.log("i m inside");
                                        isinvoice = true;
                                        console.log(isinvoice + "isinvoice");
                                        orders['isInvoice'] = isinvoice;
                                    } else {
                                        console.log("false");
                                        isinvoice = false;
                                        console.log(isinvoice + "isinvoice");
                                        orders['isInvoice'] = isinvoice;
                                    }
                                }
                                isinvoice = false;
                            });

                        });
                        console.log(this.orderList);
                    }
                }

                this.showLoader = false;
                if (this.orderListLength == 0) {
                    document.getElementById('searchSingleText').style.display = 'none';
                    document.getElementById('searchAdvText').style.display = 'none';
                } else {
                    document.getElementById('searchSingleText').style.display = 'none';
                    document.getElementById('searchAdvText').style.display = 'inline';
                    setTimeout(() => {
                        var elmnt = document.getElementById("exportable1");
                        elmnt.scrollLeft = 0;
                        elmnt.scrollTop = 0;
                    }, 1500);
                }

                if (window.innerWidth < this._globalFunc.stdWidth) {
                    document.getElementById("openModalResult").click();
                } else {
                    this.orderResult = false;
                    var element = document.getElementById("deliverydateshow");
                    element.scrollIntoView();
                }
            }
                , err => {
                    this._globalFunc.logger("Error in advance search order service");
                    this.showLoader = false;
                    this._router.navigate(['/error404']);
                }
            );
        }
    }

    getProductSearchResult(value: any) {
        this.showLoader = true;
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                if (value.productcode != undefined && value.productcode != '') {
                    this.sortByColumn = '';
                    this.reverseSorting = false;
                    this._globalFunc.logger("Order Search using product code itemId(IndexKey) : " + value.productcode + "(IndexValue)");
                    this._postsService.getProductOrders(this.orgApiName, "itemId", value.productcode).subscribe(data => {
                        this.showLoader = false;
                        this.orderList = data.items;
                        if (data.items == undefined || data.items.length == 0) {
                            this._globalFunc.logger("No orders found");
                            this.orderListLength = 0;
                            document.getElementById('searchSingleText').style.display = 'none';
                            document.getElementById('searchAdvText').style.display = 'none';
                        } else {
                            this._globalFunc.logger("Orders Found");
                            this.orderListLength = data.items.length;
                            document.getElementById('searchSingleText').style.display = 'none';
                            document.getElementById('searchAdvText').style.display = 'inline';
                            setTimeout(() => {
                                var elmnt = document.getElementById("exportable1");
                                elmnt.scrollLeft = 0;
                                elmnt.scrollTop = 0;
                            }, 1500);
                        }

                        if (window.innerWidth < this._globalFunc.stdWidth) {
                            document.getElementById("openModalResult").click();
                        } else {
                            this.orderResult = false;
                        }
                    }
                        , err => {
                            this._globalFunc.logger("Error in single search order service");
                            this.showLoader = false;
                            this._router.navigate(['/error404']);
                        }
                    );
                }
            } else {
                this.showLoader = false;
            }
        }, reason => {
            console.log(reason);
        });
    }

    closeOperation() {
        this.orderList = '';
    }
    eventHandler(keyCode: any, field: any) {
        if (keyCode != '' && field == 1) {
            document.getElementById("productcodeSearch").classList.add('disable');
            document.getElementById("productcodeSearchMob").classList.add('disable');
        } else {
            document.getElementById("productcodeSearch").classList.remove('disable');
            document.getElementById("productcodeSearchMob").classList.remove('disable');
        }
        if (keyCode != '' && field == 2) {
            document.getElementById("orderIdSearch").classList.add('disable');
            document.getElementById("orderIdSearchMob").classList.add('disable');
        } else {
            document.getElementById("orderIdSearch").classList.remove('disable');
            document.getElementById("orderIdSearchMob").classList.remove('disable');
        }
    }

    onKeypress(myval: any, filed: any) {
        if (window.innerWidth > this._globalFunc.stdWidth && (filed == 2)) {
            document.getElementById('adv-search-desk').focus();
        }
        switch (filed) {
            case 1:
                this.switchAction(myval, 1);
                break;
            case 2:
                this.switchAction(myval, 2);
                break;
            case 3:
                this.switchAction(myval, 3);
                break;
            case 4:
                this.switchAction(myval, 4);
                break;
            case 5:
                this.switchAction(myval, 5);
                break;
            case 6:
                this.switchAction(myval, 6);
                break;
            case 7:
                this.switchAction(myval, 7);
                break;
            case 8:
                this.switchAction(myval, 8);
                break;
        }
        if (this.fieldSelectedCount == 0) {
            this.shipfrom = '';
            this.orderdate = void 0;
            this.invoicedate = void 0;
            this.shipfromshow = true;
            document.getElementById("orderdateshow").classList.remove('disable');
            document.getElementById("invoicedateshow").classList.add('disable');
            document.getElementById("orderdateshowmob").classList.add('disable');
            document.getElementById("invoicedateshowmob").classList.add('disable');
        } else if (this.fieldSelectedCount == 1) {
            this.statusshow = false;
            this.shiptoshow = false;
            this.invoiceIdshow = false;
            document.getElementById("deliverydateshow").classList.remove('disable');
            document.getElementById("deliverydateshowmob").classList.remove('disable');
            this.channelsshow = false;
            this.shipfromshow = false;
            document.getElementById("orderdateshow").classList.remove('disable');
            document.getElementById("invoicedateshow").classList.remove('disable');
            document.getElementById("orderdateshowmob").classList.remove('disable');
            document.getElementById("invoicedateshowmob").classList.remove('disable');
        } else if (this.fieldSelectedCount == 2) {
            this.statusshow = true;
            this.shiptoshow = true;
            this.invoiceIdshow = true;
            document.getElementById("deliverydateshow").classList.add('disable');
            document.getElementById("orderdateshow").classList.add('disable');
            document.getElementById("invoicedateshow").classList.add('disable');
            document.getElementById("deliverydateshowmob").classList.add('disable');
            document.getElementById("orderdateshowmob").classList.add('disable');
            document.getElementById("invoicedateshowmob").classList.add('disable');
            this.shipfromshow = true;
            for (var i = 0; i < this.fieldSelected.length; i++) {
                for (var j = 1; j <= 8; j++) {
                    if (this.fieldSelected[i] == j) {
                        switch (j) {
                            case 1:
                                this.channelsshow = false;
                                break;
                            case 2:
                                this.statusshow = false;
                                break;
                            case 3:
                                this.shiptoshow = false;
                                break;
                            case 4:
                                document.getElementById("deliverydateshow").classList.remove('disable');
                                document.getElementById("deliverydateshowmob").classList.remove('disable');
                                break;
                            case 5:
                                this.invoiceIdshow = false;
                                break;
                            case 6:
                                document.getElementById("orderdateshow").classList.remove('disable');
                                document.getElementById("orderdateshowmob").classList.remove('disable');
                                break;
                            case 7:
                                document.getElementById("invoicedateshow").classList.remove('disable');
                                document.getElementById("invoicedateshowmob").classList.remove('disable');
                                break;
                            case 8:
                                this.shipfromshow = false;
                                break;
                        }
                    }
                }
            }
        }
    }

    switchAction(myval: any, id: number) {
        if (myval != '') {
            if (this.fieldSelected.indexOf(id) == -1) {
                this.fieldSelected.push(id);
                this.fieldSelectedCount = this.fieldSelectedCount + 1;
            }
        } else {
            if (this.fieldSelected.indexOf(id) != -1) {
                this.fieldSelectedCount = this.fieldSelectedCount - 1;
                this.fieldSelected.splice((this.fieldSelected.indexOf(id)), 1);
            }
        }
    }

    keyPress(event: any) {
        this._globalFunc.alphaNumeric(event);
    }

    formatDate(createdAtDate: string) {
        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
            if (createdAtDate.indexOf('/') >= 0) {
                return createdAtDate;
            } else {
                var d = new Date(createdAtDate);
                var yyyy = d.getUTCFullYear();
                var mm = d.getUTCMonth() < 9 ? "0" + (d.getUTCMonth() + 1) : (d.getUTCMonth() + 1);
                var dd = d.getUTCDate() < 10 ? "0" + d.getUTCDate() : d.getUTCDate();
                return dd + "/" + mm + "/" + yyyy;
            }
        } else {
            return '-';
        }
    }

    formatTime(createdAtDate: string) {

        var momenttm = require('moment-timezone');

        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
            /*var d = new Date(createdAtDate);
            var hh = (d.getUTCHours()+1) < 10 ? "0" + (d.getUTCHours()+1) : (d.getUTCHours()+1);
            var min = d.getUTCMinutes() < 10 ? "0" + d.getUTCMinutes() : d.getUTCMinutes();*/
            var d = momenttm.tz(createdAtDate,"Europe/London").format('HH:mm:ss');

            var opieces = d.split(':');

            var hh = opieces[0];
            var min = opieces[1];
            
            return hh + ":" + min;
        } else {
            return '-';
        }
    }

    onScrolltop() {
        this._globalFunc.logger("Scroll to top");
        this._globalFunc.autoscroll();
    }

    gotoBottom() {
        this._globalFunc.logger("Scroll to bottom");
        this._globalFunc.autoscrolldown();
    }

    scrollFunction() {
        var element = document.getElementById('topBtn');
        if (typeof (element) != 'undefined' && element != null) {
            if (document.body.scrollTop > 800 || document.documentElement.scrollTop > 800) {
                document.getElementById("topBtn").style.display = "block";
            } else {
                document.getElementById("topBtn").style.display = "none";
            }
        }
    }

    changeSorting(columnName: string) {
        this._globalFunc.logger("Order sort by " + columnName);
        if (this.sortByColumn == columnName) {
            this.reverseSorting = !this.reverseSorting;
        } else {
            this.sortByColumn = columnName;
            this.reverseSorting = false;
        }
        console.log(this.sortByColumn + " : " + this.reverseSorting);
    }

    selectedClass(columnName: string) {
        if (columnName == 'createdAt')
            return this.sortByColumn == columnName ? 'min-wdt190 align-middle sort-' + this.reverseSorting : 'min-wdt190 align-middle sortit';
        else
            return this.sortByColumn == columnName ? 'min-wdt80 align-middle sort-' + this.reverseSorting : 'min-wdt80 align-middle sortit';
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        console.log("Session terminated");
                        this.terminateFlg = true;
                        sessionStorage.setItem('terminateFlg', 'true');
                        reject('sessionInactive');

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else {
                        reject('sessionInactive');
                        sessionStorage.clear();
                        this._router.navigate(['']);
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        console.log("Session terminated");
                        this.terminateFlg = true;
                        sessionStorage.setItem('terminateFlg', 'true');
                        reject('sessionInactive');

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        resolve('sessionActive');
                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
                        this.organisationListChange = changeOrgAvailable;
                        this.orgimgUrl = orgImgUrl;
                        this.orgApiName = orgApiNameTemp;
                        if (this.organisationList.length == 1) {
                            this.disabledCusChange = true;
                        }
                        console.log("this.organisationList234");
                        console.log(this.organisationList);
                        let that = this;
                        let orderEnquiry = true;
                        let dashboardMenuPermission = false;
                        let userHavingPermission = false;
                        this.organisationList.forEach(function (organisation: any) {
                            if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                                console.log("organisation : " + that.customerName);
                                console.log(organisation);
                                organisation.permissionRoleList.forEach(function (permRoleList: any) {
                                    console.log("11");
                                    console.log(permRoleList);
                                    permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                        console.log("22");
                                        if (permRoleDtlList.permissionGroup == 'orders') {
                                            userHavingPermission = true;
                                            permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                                if (permList.permissionName == 'orders' && permList.permissionStatus == 'active') {
                                                    dashboardMenuPermission = true;
                                                }

                                                switch (permList.permissionName) {
                                                    case "orderEnquiry":
                                                        console.log("55");
                                                        orderEnquiry = false;
                                                        break;
                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                        that = null;
                        if (orderEnquiry) {
                            this._router.navigate(["orders", customerName]);
                        }
                    }
                }
            });
        });
    }
    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }

    onChange(e:any){
        if(e.target.value !='Non-OF'){
            this.channelSubType='All';
        }
        if(e.target.value == 'Storepick'){
            sessionStorage.setItem('type','Storepick');
        }
    }

}