import { NgModule } from '@angular/core';
import { OrdersEnquiryComponentRoutes } from './orders-enquiry.routes';
import { OrdersEnquiryComponent } from './orders-enquiry.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { FormsModule } from '@angular/forms';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { TooltipModule } from "ngx-tooltip";
import { NavigationModule } from '../../navigationFulfilment/navigation.module';
import { commonfunction } from '../../../services/commonfunction.services';
import { DatepickerModule } from '../../datepicker/ng2-bootstrap';
import { OrderFilter } from '../orderFilter.pipes';
import { OrderBy } from '../order.orderby.pipe';
@NgModule({
  imports: [
    OrdersEnquiryComponentRoutes,
    CommonModule,
    FormsModule,
    DatepickerModule.forRoot(),
    Ng2OrderModule,
    NavigationModule
  ],
  exports: [],
  declarations: [OrdersEnquiryComponent, OrderFilter, OrderBy] ,
  providers: [Services, GlobalComponent, commonfunction]
})
export class OrdersEnquiryModule { }
