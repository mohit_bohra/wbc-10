import {Pipe, PipeTransform} from '@angular/core';
@Pipe({
    name: 'orderfilter',
    pure: true
})
export class OrderFilter implements PipeTransform {
    transform(items: any[], filter: string, channelFilter: string): any {
        console.log("Pipe");
        console.log(items);
        console.log(filter);
        console.log('channel filter',channelFilter);
       
        if (!items || !filter) {
            return items;
        }
        // filter items array, items which match and return true will be
        // kept, false will be filtered out
        /*if(filter == 'All'){
            return items;
        }else if(filter == 'Non-OF' && channelFilter == 'All'){
            console.log("here1");
            return items.filter(item => item.messageType != 'OF');
            
        }else if(filter == 'Non-OF' && channelFilter != 'All'){
            console.log("here2");
            //return items.filter(item => item.messageType != 'OF');
            return items.filter(item => item.messageType == channelFilter);
        }else{
            console.log("here3");
            return items.filter(item => item.messageType == filter);
        }*/
        if(filter == 'All'){
            return items;
        }else if(filter == 'Storepick' && channelFilter == 'All'){
            return items.filter(item => item.messageType == 'STOREPICK');
            
        }else if(filter == 'OF' && channelFilter == 'All'){
            return items.filter(item => item.messageType == 'OF');
            
        }else if(filter == 'Non-OF' && channelFilter == 'All'){
            //items.filter(item => item.messageType != 'OF');
            return items.filter(this.nonOfValues);
            
        }else if(filter == 'Non-OF' && channelFilter != 'All'){            
            //return items.filter(item => item.messageType != 'OF');
            return items.filter(item => item.messageType == channelFilter);
        }else{
            console.log("here3");
            return items.filter(item => item.messageType == filter);
        }

        
    }
    nonOfValues(value:any){
        return value.messageType!="OF" && value.messageType!="STOREPICK"
    }
    
}