import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrdersComponent } from './orders.component';

const ordersRoutes: Routes = [
  { path: '', component: OrdersComponent },
  { path: 'enquiry', loadChildren: './enquiry/orders-enquiry.module#OrdersEnquiryModule' },
  { path: 'claims', loadChildren: './claims/orderclaims.module#OrderClaimsModule' },
  { path: 'orders-validation', loadChildren: './validation/orders-validation.module#OrdersValidationModule' },
  { path: ':orderNo', loadChildren: './details/ordersdetail.module#OrdersdetailModule' }
 
];


@NgModule({
  imports: [RouterModule.forChild(ordersRoutes)],
  exports: [RouterModule]
})
export class OrdersComponentRoutes { }
