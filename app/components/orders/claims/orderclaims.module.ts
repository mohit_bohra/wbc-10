import { NgModule } from '@angular/core';
import { OrderClaimsComponentRoutes } from './orderclaims.routes';
import { OrderClaimsComponent } from './orderclaims.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TooltipModule } from "ngx-tooltip";
import { NavigationModule } from '../../navigationFulfilment/navigation.module';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';

@NgModule({
  imports: [
    OrderClaimsComponentRoutes,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TooltipModule,
    NavigationModule,
    InfiniteScrollModule
  ],
  exports: [],
  declarations: [OrderClaimsComponent],
  providers: [Services, GlobalComponent]
})
export class OrderClaimsModule { }
