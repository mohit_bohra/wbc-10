import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderClaimsComponent } from './orderclaims.component';
import { CanDeactivateGuard } from '../../../services/can-deactivate-guard.service';

// route Configuration
const orderClaimRoutes: Routes = [
  { path: '', component: OrderClaimsComponent, canDeactivate: [CanDeactivateGuard] },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(orderClaimRoutes)],
  exports: [RouterModule]
})
export class OrderClaimsComponentRoutes { }
