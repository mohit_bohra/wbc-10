import { Component, ViewChild } from '@angular/core';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DialogService } from '../../../services/dialog.service';
import { debug } from 'util';

declare let saveAs: any;
@Component({
    templateUrl: 'orderclaims.component.html',
    providers: [commonfunction]
})
export class OrderClaimsComponent {
    //@ViewChild('searchOrdClaims') searchOrdClaimsForm: any;
    @ViewChild('orderClaim') orderClaim: any;
    successMsg: string;
    objConstant = new Constant();
    whoImgUrl: any;
    orderDetail: any;
    orderno: string;
    showLoader: boolean;
    printHeader: boolean;
    pageState: number = 49;
    customerName: string;
    organisationList: any;
    orgimgUrl: string;
    orgApiName: string;
    _sub: any;
    hideOrganisation: boolean = true;
    homeUrl: any;
    organisationListChange: any;

    popupMsg: string = "";
    noDataFound: boolean = false;
    errTxt: string = "Data is getting Loaded...";

    autoDecision: string = 'INVESTIGATE';
    manualDecision: string = 'NEW'

    offset: number;
    start: number;
    limit: number;
    count: number;
    hitNext: boolean;
    customerDispayName: string = "";
    style: string;
    disabledCusChange: boolean;
    custPermission: any[];
    claimData: any = [];
    updateArr: any = [];
    claimDataLength: any;
    claimRules: any;
    imgName: string = "";
    event: string = "pageload";
    statusAll: any;
    status: string = 'NEW';
    isManualAcceptDisable: boolean = false;
    isManualRejectDisable: boolean = false;
    remarkdropdownOption: any = [];
    autoStatusArr: any = [];
    manStatusArr: any = [];
    statusArr: any = [];
    claimRef: any;
    itemNumber: any
    reason: any;
    comments: any;
    remarkdropdownOption1: any = [];
    commentsEnable: boolean = false;
    isRemarkAcceptDisable: boolean = false;
    reasonSel: string = "";
    reasonComment: string = "";
    unsavedChange: boolean = false;
    prevOrgSelected: any;
    searchString: string;
    gridChangeForSelect: boolean;
    claimsEditable : boolean;




    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, public _cfunction: commonfunction, private formBuilder: FormBuilder, public dialogService: DialogService) {
        // img url
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        //this.searchOrdClaimsForm = this.formBuilder.group({});
        this.orderClaim = this.formBuilder.group({});

    }

    canDeactivate(): Promise<boolean> | boolean {
        console.log('unsaved : ' + sessionStorage.getItem('unsavedChanges'));
        if (sessionStorage.getItem('unsavedChanges') !== 'true') {
            return true;
        }
        return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
    }

    // function works on page load
    ngOnInit() {

        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {
            this.autoStatusArr = [{ status: 'INVESTIGATE', value: 'INVESTIGATE' },
            { status: 'NEW', value: 'NEW' },
            { status: 'AUTO ACCEPT', value: 'AUTO ACCEPT' },
            { status: 'AUTO REJECT', value: 'AUTO REJECT' },
            { status: 'ALL', value: '' }

            ];


            this.manStatusArr = [{ status: 'NEW', value: 'NEW' },
            { status: 'APPROVED', value: 'APPROVED' },
            { status: 'REJECTED', value: 'REJECTED' },
            { status: 'ALL', value: '' }
            ];

            this.statusArr = [{ status: 'NEW', value: 'NEW' },
            { status: 'SENT TO EBS', value: 'SENT TO EBS' },
            { status: 'ADJUSTED', value: 'ADJUSTED' },
            { status: 'CLAIMED', value: 'CLAIMED' },
            { status: 'ALL', value: '' },
            ];


            // window scroll function
            this._globalFunc.autoscroll();

            // set header basket data
            this.start = 0;
            this.limit = this.objConstant.SCROLL_RECORD_LIMIT;
            this.count = 0;
            this.hitNext = false;
            this.printHeader = true;
            sessionStorage.setItem('pageState', '27');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string, orderNo: string }) => {
                console.log("params.orgType : " + params.orgType);
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                this.orderno = params.orderNo;
                console.log(this.orderno + "this.ordernothis.ordernothis.orderno")

                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                let reports;

                this.imgName = this.getCustomerImage(cusType);
                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));

                let redirct;
                let changeOrgAvailable = new Array;
                let catManagement;
                let dashboards;
                let custPerm: any = [];

                console.log(this.custPermission)
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = this.customerName;
            });

            this.showLoader = true;
            this.claimData = [];
            this.claimDataLength = 0;
            this.noDataFound = true;
            this.event = 'pageload';
            this.searchOrderClaims();

        }
    }

    resetclaim() {
        this._router.navigate(['orders', this.customerName]);
    }
    searchOrderClaims() {
        if(this.gridChangeForSelect == true) {
            if(this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.') === true){

                this.gridChangeForSelect = false;
                this.showLoader = true;
                this.unsavedChange = false;
                sessionStorage.setItem('unsavedChanges','false');
                console.log("serach start");
                console.log(this.autoDecision);
                console.log(this.manualDecision);
                if (this.autoDecision != '' || this.manualDecision != "" || this.status != '') {
                    this.event = "";
                }
                if (this.searchString != undefined && this.searchString != "") {
                    this.autoDecision = '';
                    this.manualDecision = '';
                    this.status = '';
                    this.event = "";


                }
                this.offset = 0;
                this._postsService.getOrderClaims(this.customerName, this.autoDecision, this.manualDecision, this.status, this.event, this.offset, this.searchString).subscribe((data: any) => {
                    console.log(data);
                    this.showLoader = false;
                    console.log("serach end");
                    this.claimData = data.claimList;
                    console.log(this.claimData);
                    this.claimDataLength = data.claimList.length;
                    this.noDataFound = false;
                    this.count = data.paginationMetaData.totalCount;
                    this.claimData.forEach(function (element: any, index: any) {
                        element['manOrgStatus'] = element['manStatus'];
                        element['requiredField'] = null;
                        if (element.autoStatus == "INVESTIGATE" || element.autoStatus == "AUTO REJECT" ) {
                            var ruleFlag = false;
                            var rules = element.additionalAttributes.rules
                            rules.forEach(function (item: any, index: any) {

                                if(item.status == "INVESTIGATE" || item.status == "AUTO REJECT"){
                                if (ruleFlag == false) { 
                                    element.autoStatus = item.ruleId + "-" + element.autoStatus;
                                    console.log(element.autoStatus + "element.autoStatus")
                                    ruleFlag = true;
                                }
                                }

                            });
                        }
                    });
                }, (err: any) => {
                    if (err.errorCode = '404.33.401') {
                        this.noDataFound = true;
                        this.errTxt = err.errorMessage;

                        this.showLoader = false;

                    } else {

                        this._router.navigate(['/error404']);
                    }
                });
                this._postsService.getConfigRemarkMenu(this.customerName).subscribe((data: any) => {
                    console.log(data);
                    var remarkdropdownOptionarray
                    this.remarkdropdownOption = data.customerConfigDetails;
                    this.remarkdropdownOption.forEach(function (element: any, index: any) {
                        if (element['customerName'] == 'mccolls') {
                            let remarkdropdownOption = element['claimRemarks'];
                            // remarkdropdownOption = "claim1, claim2 claim3, claim4, claim5, claim6, claim7, other"
                            remarkdropdownOptionarray = remarkdropdownOption.split(',');


                        }
                    });
                    this.remarkdropdownOption1 = remarkdropdownOptionarray

                }, (err: any) => {
                    if (err.errorCode = '404.33.401') {
                        this.noDataFound = true;
                        this.errTxt = err.errorMessage;

                        this.showLoader = false;

                    } else {

                        this._router.navigate(['/error404']);
                    }
                });

            }
        } else {
                this.showLoader = true;
                console.log("serach start");
                console.log(this.autoDecision);
                console.log(this.manualDecision);
                if (this.autoDecision != '' || this.manualDecision != "" || this.status != '') {
                    this.event = "";
                }
                if (this.searchString != undefined && this.searchString != "") {
                    this.autoDecision = '';
                    this.manualDecision = '';
                    this.status = '';
                    this.event = "";


                }
                this.offset = 0;
                this._postsService.getOrderClaims(this.customerName, this.autoDecision, this.manualDecision, this.status, this.event, this.offset, this.searchString).subscribe((data: any) => {
                    console.log(data);
                    this.showLoader = false;
                    console.log("serach end");
                    this.claimData = data.claimList;
                    console.log(this.claimData);
                    this.claimDataLength = data.claimList.length;
                    this.noDataFound = false;
                    this.count = data.paginationMetaData.totalCount;
                    this.claimData.forEach(function (element: any, index: any) {
                        element['manOrgStatus'] = element['manStatus'];
                        element['requiredField'] = null;
                        if (element.autoStatus == "INVESTIGATE" || element.autoStatus == "AUTO REJECT" ) {
                            var ruleFlag = false;
                            var rules = element.additionalAttributes.rules
                            rules.forEach(function (item: any, index: any) {

                                if(item.status == "INVESTIGATE" || item.status == "AUTO REJECT"){
                                if (ruleFlag == false) { 
                                    element.autoStatus = item.ruleId + "-" + element.autoStatus;
                                    console.log(element.autoStatus + "element.autoStatus")
                                    ruleFlag = true;
                                }
                                }

                            });
                        }
                    });
                }, (err: any) => {
                    if (err.errorCode = '404.33.401') {
                        this.noDataFound = true;
                        this.errTxt = err.errorMessage;

                        this.showLoader = false;

                    } else {

                        this._router.navigate(['/error404']);
                    }
                });
                this._postsService.getConfigRemarkMenu(this.customerName).subscribe((data: any) => {
                    console.log(data);
                    var remarkdropdownOptionarray
                    this.remarkdropdownOption = data.customerConfigDetails;
                    this.remarkdropdownOption.forEach(function (element: any, index: any) {
                        if (element['customerName'] == 'mccolls') {
                            let remarkdropdownOption = element['claimRemarks'];
                            // remarkdropdownOption = "claim1, claim2 claim3, claim4, claim5, claim6, claim7, other"
                            remarkdropdownOptionarray = remarkdropdownOption.split(',');


                        }
                    });
                    this.remarkdropdownOption1 = remarkdropdownOptionarray

                }, (err: any) => {
                    if (err.errorCode = '404.33.401') {
                        this.noDataFound = true;
                        this.errTxt = err.errorMessage;

                        this.showLoader = false;

                    } else {

                        this._router.navigate(['/error404']);
                    }
                });
        }
        
    }


    onScrollDown() {
        console.log("scroll" + this.count);
        this.showLoader= true;
        if (this.autoDecision != '' || this.manualDecision != "") {
            this.event = "";
        }

        this.offset = this.offset + this.limit;
        if (this.offset > this.count){
             this.showLoader = false;
        }
        if (this.offset < this.count) {
            this._postsService.getOrderClaims(this.customerName, this.autoDecision, this.manualDecision, this.status, this.event, this.offset, this.searchString).subscribe((data: any) => {
                this.showLoader = false;
                this.claimData = this.claimData.concat(data.claimList);
                //this.productPriceData = data.periodicRepricing;
                // this.claimData = this.claimData.length;
                //this.count = data.paginationMetaData.count;
                console.log(data);
                console.log(this.claimData + "claimData");
                this.claimData.forEach(function (element: any, index: any) {
                    element['manOrgStatus'] = element['manStatus'];
                    element['requiredField'] = null;
                    if (element.autoStatus == "INVESTIGATE" || element.autoStatus == "AUTO REJECT" ) {
                    var ruleFlag = false;
                    var rules = element.additionalAttributes.rules
                    rules.forEach(function (item: any, index: any) {

                         if(item.status == "INVESTIGATE" || item.status == "AUTO REJECT"){
                          if (ruleFlag == false) { 
                            element.autoStatus = item.ruleId + "-" + element.autoStatus;
                            console.log(element.autoStatus + "element.autoStatus")
                            ruleFlag = true;
                        }
                         }

                    });
                }

                });


            }, (error: any) => {
                console.log(error);
                if (error.errorCode == '404.33.410') {
                    this.claimData.length = 0;
                    this.claimData = 0;
                } else {
                    this._router.navigate(['/error404']);
                }

            });


        }



    }


    enableComments() {
        this.unsavedChange = true;
        sessionStorage.setItem('unsavedChanges', 'true');
        this.commentsEnable = true;
    }
    updateOrderClaims1(val: any, claimRef: any, itemNum: any) {
        console.log(this.claimData + "this.claimData")
        val.reason = this.reasonSel + "." + this.reasonComment;
        console.log("val : " + "#remarkreson_" + claimRef + itemNum);
        console.log(val)
        this.claimData.reason = val;
        let pushVal;
        let claimReference = claimRef;
        let ItemNumber = itemNum;
        this.commentsEnable = false;
        //this.searchOrdClaimsForm.form.markAsPristine();
        $("#remarkreson_" + claimRef + ItemNumber).val(val);
        this.gridChangeForSelect = true;
        this.searchString = null;
        // this.updateOrderClaims(val, claimRef);
        this.claimData.forEach(function (element: any, index: any) {
            if (element.claimReference == claimReference) {
                if (element.itemNumber == ItemNumber) {
                    element.reason = val.reason;
                }
            }



        });

    }

    updateOrderClaims(val: any) {
        this.unsavedChange = false;
        this.gridChangeForSelect = false;
        sessionStorage.setItem('unsavedChanges', 'false');
        console.log(this.claimData);
        let that = this;
        this.updateArr = [];
        var pushVal;
        let reason = val.reason;

        this.searchString = null;
        this.claimData.forEach(function (element: any, index: any) {

            if (((element.manStatus != element.manOrgStatus) && element.manStatus != null) || element.reason != null) {
                console.log(element);
                // element.reason = null;
                console.log("val  ==> " + val);
                pushVal = {
                    "queryReference": element.claimReference,
                    "claimList": [
                        {
                            "itemNumber": element.itemNumber,
                            "reasonCode": element.reasonCode,
                            "manStatus": element.manStatus,
                            "updatedBy": sessionStorage.getItem("email"),
                            "orderId": element.orderId,
                            "reason": (element.reason == null ? reason : element.reason),
                            "comments": element.comments
                        }
                    ]
                }
                that.updateArr.push(pushVal);
            }
        });
        console.log(this.updateArr);

        this._postsService.updateOrderClaim(this.customerName, this.updateArr).subscribe((data: any) => {
            console.log(data);
            this.successMsg = "Claim Review saved successfully.";

            let sessionModal = document.getElementById('updatePatch');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('updatePatch').style.display = 'block';

            this.event = 'pageload';
            this.searchOrderClaims();
        });
    }

    onChange(event: any) {
        console.log('++++++++++++++++++++++++++++++++++++++++');
        console.log(event);
        console.log(event.target.nodeName);
        console.log('++++++++++++++++++++++++++++++++++++++++');
        this.unsavedChange = true;
        sessionStorage.setItem('unsavedChanges', 'true');
        console.log(this.claimData);

        // if(event.target.nodeName == 'SELECT' && this.gridChangeForSelect == true) {
        //    //console.log('REsponse ---------------:  ' + this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.'));
        //    if(this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.') === false){
        //         var id = event.target.id;
        //         console.log(id);
               
        //         event.stopPropagation();
        //         event.preventDefault();
             
        //         return false;
        //     }
        // }
    }

    closeRepricingUpdateSuccess() {
        this.showLoader = false;
        let sessionModal = document.getElementById('updatePatch');
        sessionModal.classList.add('in');
        sessionModal.classList.remove('out');
        document.getElementById('updatePatch').style.display = 'none';

    }

    getActivateCheckbox(index: any, value: any, eleName: string, autoStatus: any, status: any, reasonCode: any, claimRef: any, itemNum: any) {
        this._cfunction.getActivateCheckbox(index, value, eleName);
        this.unsavedChange = true;
        sessionStorage.setItem('unsavedChanges', 'true');
        console.log('radioClaim' + index);
        console.log(autoStatus);
        this.claimData.forEach(function (element: any, i: any) {
            if (index == i) {
                if (autoStatus == 'INVESTIGATE') {
                    element['requiredField'] = true;
                } else {
                    element['requiredField'] = false;
                }
            }
        });
        
        this.gridChangeForSelect = true;

    }
    getCheckboxDisableValue(index: any, value: any, eleName: string, autoStatus: any, status: any, reasonCode: any, claimRef: any, itemNum: any) {
        this.claimRef = claimRef;
        this.itemNumber = itemNum;
        this.isManualAcceptDisable;
        if (autoStatus == 'NEW' || status != 'NEW') {
            this.isManualAcceptDisable = true;
        }
       
        else {
            this.isManualAcceptDisable = false;
        }

    }
    enableRemark(index: any, value: any, eleName: string, autoStatus: any, status: any, manstatus: any, reasonCode: any) {
        let manStatus = manstatus;
       
         if (status != 'NEW') {
            this.isRemarkAcceptDisable = true;
        }
        
        else {
            this.isRemarkAcceptDisable = false;
        }

    }
    openRemarkModal(value: any, itemNum: any, claimreason: any) {

        this.claimRef = value;
        this.itemNumber = itemNum;

        let sessionModal = document.getElementById('remarkModal');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('remarkModal').style.display = "block";
        // $("#comment").val(claimreason);
        // $("#remarkDrop").val(claimreason);
        if (claimreason != null) {
            let dummyArr = claimreason.split(".");
            let splitStr = claimreason.substring(dummyArr[0].length + 1);
            $("#comment").val(splitStr);
            $("#remarkDrop").val(dummyArr[0]);
            this.reasonSel = dummyArr[0];
            this.reasonComment = splitStr;
        } else {
            this.reasonSel = "";
            this.reasonComment = "";
        }

    }
    toggleOrganisation() {
        //alert(this.hideOrganisation);
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
        } else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
        }
    }

    changeOrgType(customerName: any) {
        if (this.unsavedChange == true) {
            this.prevOrgSelected = sessionStorage.getItem('orgType');
            sessionStorage.setItem('prevOrgSelected', this.prevOrgSelected);
            sessionStorage.setItem('customername', customerName)
            sessionStorage.setItem('orders', 'true');
            this.canDeactivate();
        } else {
            this._router.navigate(this._globalFunc.redirectToComponent('orders', customerName));
            // reset field on change of organisation      
            this.toggleOrganisation();

            this.customerName = customerName;
            sessionStorage.setItem('orgType', this.customerName);
            this.errTxt = "Data is getting loaded... !"
            this.showLoader = true;
            this.claimData = [];
            this.claimDataLength = 0;
            this.noDataFound = true;
            this.event = 'pageload';
            this.searchOrderClaims();
        }
    }


    closePopup(popupName: string = 'messageModal') {
        this.popupMsg = "";
        this.commentsEnable = false;
        if (popupName == 'autoValidationPopupModal') {
            this.claimRules = [];
        }
        if (popupName == 'remarkModal') {
            $("#remarkDrop").val("");

        }
        let sessionModal = document.getElementById(popupName);
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById(popupName).style.display = "none";
    }

    showValidation(idx: number) {
        if (this.claimData[idx].autoStatus != "NEW") {
            if (this.claimData[idx].additionalAttributes.rules) {
                this.claimRules = this.claimData[idx].additionalAttributes.rules;
            } else {
                this.claimRules = [];
            }

            let sessionModal = document.getElementById("autoValidationPopupModal");
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById("autoValidationPopupModal").style.display = "block";

            var elmnt = document.getElementById("rulesTable");
            elmnt.scrollLeft = 0;
            elmnt.scrollTop = 0;
        }

    }

    printOrderClaims() {
        // This method is activated when we click on print icon
        var printHeaderNav = document.getElementById('printheader').innerHTML;
        var tableData = document.getElementById('exportable').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + tableData + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + tableData + '</body></html>');
            popup.document.close();
        }

    }

    exportOrderClaims() {
        var blob = new Blob([document.getElementById('exportable1').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, this.customerName + "OrderClaims.xls");
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        console.log("customerName : " + customerName);
        this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
            if (!userData.isSessionActive) { // false
                // User session has expired.
                sessionStorage.clear();
                this._router.navigate(['']);
            } else {
                this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                let changeOrgAvailable: any = [];
                let orgImgUrl = "";
                let orgApiNameTemp = "";
                this.organisationList.forEach(function (item: any) {
                    if (item.customerName !== customerName) {
                        changeOrgAvailable.push(item);
                    } else {
                        orgImgUrl = item.customerName + ".png";
                        orgApiNameTemp = item.customerName;
                    }
                });
                this.organisationListChange = changeOrgAvailable;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                // this.disabledCusChange = false;
                if (this.organisationList.length == 1) {
                    this.disabledCusChange = true;
                }

                console.log("this.organisationList234");
                console.log(this.organisationList);
                let that = this;
                let claimsReview = true;
                let claimsEdit = true;
                let dashboardMenuPermission = false;
                let userHavingPermission = false;
                this.organisationList.forEach(function (organisation: any) {
                    if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                        console.log("organisation : " + that.customerName);
                        console.log(organisation);
                        organisation.permissionRoleList.forEach(function (permRoleList: any) {
                            console.log("11");
                            console.log(permRoleList);
                            permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                console.log("22");
                                if (permRoleDtlList.permissionGroup == 'orders') {
                                    userHavingPermission = true;
                                    permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                        if (permList.permissionName == 'orders' && permList.permissionStatus == 'active') {
                                            dashboardMenuPermission = true;
                                        }

                                        switch (permList.permissionName) {
                                            case "claimsReviewView":
                                                console.log("55");
                                                claimsReview = false;
                                                //alert(filfilment);
                                                break;
                                            case "claimsReviewEdit":
                                                console.log("55");
                                                claimsEdit = false;
                                                //alert(filfilment);
                                                break;
                                        }
                                    });
                                }
                            });
                        });
                    }
                });
                that = null;
                console.log("claimsEdit==> "+ claimsEdit);
                this.claimsEditable = claimsEdit;
                if (claimsReview) {
                    console.log("REdirecting from here : " + customerName);
                    // redirect to order
                    this._router.navigate(["orders", customerName]);
                }
            }
            //alert(this.disabledfilfilment);
        });
    }

}

