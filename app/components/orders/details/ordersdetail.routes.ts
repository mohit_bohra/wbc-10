import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrdersdetailComponent } from './ordersdetail.component';

// route Configuration
const ordersdetailRoutes: Routes = [
  { path: '', component: OrdersdetailComponent },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(ordersdetailRoutes)],
  exports: [RouterModule]
})
export class OrdersdetailComponentRoutes { }
