import { NgModule } from '@angular/core';
import { OrdersdetailComponentRoutes } from './ordersdetail.routes';
import { OrdersdetailComponent } from './ordersdetail.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from "ngx-tooltip";
import { PositivePipe } from './positive.pipes';

@NgModule({
  imports: [
    OrdersdetailComponentRoutes,
    CommonModule,
    FormsModule,
    TooltipModule,

  ],
  exports: [],
  declarations: [OrdersdetailComponent,PositivePipe],
  providers: [Services, GlobalComponent]
})
export class OrdersdetailModule { }
