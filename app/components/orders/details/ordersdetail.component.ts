import { Component } from '@angular/core';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';

import { error } from 'util';


@Component({
    selector: 'orders',
    templateUrl: 'ordersdetail.component.html',
    providers: [commonfunction]
})
export class OrdersdetailComponent {
    objConstant = new Constant();
    whoImgUrl: any;
    orderDetail: any;
    orderno: string;
    showLoader: boolean;
    printHeader: boolean;
    pageState: number = 4;
    customerName: string;
    organisationList: any;
    terminateFlg: boolean = false;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disabledOrderRaise: boolean;
    amazonType: boolean= false;

    // order detail data params
    customerId: string;
    orderId: string;
    shipToLocationId: string;
    shipSaleLocationId: string;
    shipToDeliverAt: string;
    shipToDespatchAt: string;
    orderBuyerPartyId: string;
    orderSellerPartyId: string;
    orderReferenceCode: string;
    messageId: string;
    messageType: string;
    messageCreatedAt: string;
    statusCurrent: string;
    shipToPartyId: string;
    shipToAddressName: string;
    shipToAddressLine1: string;
    shipToAddressLine2: string;
    shipToAddressCity: string;
    shipToAddressCountryCode: string;
    shipToAddressPostCode: string;
    billToPartyId: string;
    billToAddressName: string;
    billToAddressLine1: string;
    billToAddressLine2: string;
    billToAddressCity: string;
    billToAddressPostCode: string;
    billToAddressCountryCode: string;
    itemsConfirmedCountTotal: string;
    itemsConfirmedSumTotal: string;
    itemsOrderedCountTotal: string;
    itemsOrderedSumTotal: string;
    statuscycletimestamp: any;;
    billToTotalNetAmount: string;
    billToTotalTaxAmount: string;
    billToTotalAmount: string;

    itemsPickedSumTotal: string;
    itemsShippedCountTotal: string;
    itemsPickedCountTotal: string;
    itemsShippedSumTotal: string;
    billToId: string;
    createdAt: string;
    isinvoice: boolean;
    // order item list
    orderItemList: any;
    organisationListChange: any;

    //  orderValidationList :any = [];
    attributeList: string;

    orderValidationmsgsList: any;
    statusOrderTracker: boolean;
    statusOrderDetail: boolean;

    private _sub: any;
    identifierHide: boolean;
    identifierHeaderSize: number;
    identifierquantitySize: number;
    identifierpriceSize: number;
    identifierstatusSize: number;
    quantityHide: boolean;
    priceHide: boolean;
    statusHide: boolean;
    timeStampConfirmed: any;
    timeStampRaised: any;
    timeStampShip: any;
    timeStampReceipt: any;
    timeStampAccounted: any;
    timeStampInvoiced: any;
    timeStampPicked: any;
    net_Total: string;
    total_Total: string;
    orderTrackerSelected: boolean;
    orderDetailSelected: boolean;
    isAmazon: boolean = false;
    orderTrackerList: any;
    DHLtime: any;
    sms2Time: any;
    ISINVOICEpresent: boolean;
    imgName: string = "";
    iscustomerAmazon : boolean = false;
    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, public _cfunction: commonfunction) {
        // img url
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.printHeader = true;
        this.showLoader = false;
        this.statusOrderDetail = true;
        this.orderDetailSelected = true;

    }

    // function works on page load
    ngOnInit() {
        this._globalFunc.logger("Loading Order Drilldown screen on UI");
        // session check on load
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._globalFunc.logger("Order Drilldown screen - User session not found");
            this._router.navigate(['']);
        } else {
            if(sessionStorage.getItem('type')== 'Storepick'){
                this.amazonType = true;
            }
            this.identifierHide = true;
            this.identifierHeaderSize = 3;
            this.identifierquantitySize = 2;
            this.identifierpriceSize = 2;
            this.identifierstatusSize = 1;
            this.quantityHide = true;
            this.priceHide = true;
            this.statusHide = true;
            // user permission
            this.disabledCusChange = false;
            this.disabledPermission = false;
            this.disabledReports = false;
            this.disabledCatalogue = false;
            this.disabledOrderRaise = false;
            this.net_Total = '0';
            this.total_Total = '0';


            // window scroll function
            this._globalFunc.autoscroll();
            // set header basket data
            this.pageState = 4;
            sessionStorage.setItem('pageState', '999');
            document.getElementById('diableLogo').style.cursor = "not-allowed";
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string, orderNo: string }) => {
                //sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                if(this.customerName == "amazon")
                {
                    this.iscustomerAmazon = true;
                    console.log("Customer Name"+this.iscustomerAmazon);
                }
                console.log(this.customerName + " this.customerName");

                if (this.customerName == 'amazon') {
                    this.isAmazon = true;
                }
                this.orderno = params.orderNo;
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                let reports;
                let orders;
                let catalogues;
                let accessMgmt;
                // list of org assigned to user

                document.getElementById('orderArrowSpan').style.display = 'block';
                document.getElementById('orderMob').style.display = 'block';
                document.getElementById('orderRaiseMob').style.display = 'none';

                document.getElementById('catalogueMob').style.display = 'block';
                document.getElementById('catalogueArrowSpan').style.display = 'none';
                document.getElementById('catalogueRaiseMob').style.display = 'none';
                document.getElementById('reportsMob').style.display = 'block';
                document.getElementById('permissonsMob').style.display = 'block';
                let that = this;
                this.imgName = this.getCustomerImage(params.orgType);
                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
                    if(res == 'sessionActive')
                    {                        
                        this.getValdiationMsgs(this.customerName);
                        this.getSingleOrderDetail(this.orderno);
                        this.getOrderTrackerInfo(this.orderno);
                    }
                }, reason => {
                    console.log(reason);
                });
            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            let screen: string;
            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);
            // logic for resize pop up 
            window.onresize = () => {
                screen = this._globalFunc.getDevice(window.innerWidth);
            };

            window.onscroll = () => {
                this.scrollFunction();
            };


        }
    }

    onScrolltop() {
        // When the user clicks on the button, scroll to the top of the document
        // function topFunction() {
        /* document.body.scrollTop = 0;
         document.documentElement.scrollTop = 0;*/
        this._globalFunc.autoscroll();
        // }
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    scrollFunction() {
        if (document.body.scrollTop > 800 || document.documentElement.scrollTop > 800) {
            document.getElementById("topBtn").style.display = "block";
        } else {
            document.getElementById("topBtn").style.display = "none";
        }
    }


    // download functionality
    exportData() {
        this._globalFunc.logger("Export to excel");
        return this._cfunction.exportData('export_xls', this.customerName + 'orderDetails');
    }

    // print functionality
    printOrderDetail() {
        this._globalFunc.logger("Print order Drilldown");
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        var printContentsHighLevel = document.getElementById('orderDetailTable').innerHTML;
        // var printContentsHeader = document.getElementById('order-head-details-table').innerHTML;
        var printContentsTable = document.getElementById('orderItemTable').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContentsHighLevel + printContentsTable + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContentsHighLevel + printContentsTable + '</body></html>');
            popup.document.close();
        }
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
        this._globalFunc.logger("Unloading Order drilldown screen.");
        this._globalFunc.sentLoggerBatchToServer();
    }

    getValdiationMsgs(orgName: any) {
        this._globalFunc.logger("Calling validation message service.");
        this._postsService.getOrderValidationMessage(this.customerName, "").subscribe(data => {
            console.log(data);
            this.orderValidationmsgsList = data.validationList;
            console.log(this.orderValidationmsgsList + "order validation message");
        }, error => {
            console.log(error);
        });
    }
    getInvoiceDownload() {
        this.orderDetail.statusCycle.forEach(function (statusCycle: any) {
            if (statusCycle.cycle == "invoice") {
                if (statusCycle.params.invoiceFileLocation != undefined || statusCycle.params.invoiceFileLocation != null) {
                    window.open(statusCycle.params.invoiceFileLocation);
                }
            }
        });
    }
    getOrderTrackerInfo(value: any) {
        var Dhltimestamp: any
        var sms2Timestamp: any;
        this._postsService.getOrderTracker(this.customerName, value).subscribe((data: any) => {
            console.log(data + "ordertracker");
            this.orderTrackerList = data.orderTrackerList;
            this.orderTrackerList.forEach(function (orderTrackerList: any) {
                Dhltimestamp = orderTrackerList.notificationStatus.dhlDate;
                sms2Timestamp = orderTrackerList.notificationStatus.orderConsolidationDate;
            });
            this.DHLtime = Dhltimestamp;
            this.sms2Time = sms2Timestamp;
        }, (error: any) => {
            console.log(error);
        });
    }

    // get single order detail
    getSingleOrderDetail(value: any) {
        var orderValidationList: any;
        var validationCode: string;
        var rangeFlag: boolean;
        var rangeerrorflag: boolean;
        var validationMessage: string;
        this.showLoader = true;
        this._globalFunc.logger("Order detail service.");
        this._postsService.getSingleOrder(this.customerName, value).subscribe(data => {
            console.log("SINGLE ORDER"+data);
            this.orderDetail = data.orders[0];

            this.customerId = this.orderDetail.customerId;
            this.orderId = this.orderDetail.orderId;
            this.shipToLocationId = this.orderDetail.shipToLocationId;
            this.shipSaleLocationId = this.orderDetail.shipSaleLocationId;
            this.shipToDeliverAt = this.orderDetail.shipToDeliverAt;
            this.shipToDespatchAt = this.orderDetail.shipToDespatchAt;
            this.orderBuyerPartyId = this.orderDetail.orderBuyerPartyId;
            this.orderSellerPartyId = this.orderDetail.orderSellerPartyId;
            this.orderReferenceCode = this.orderDetail.orderReferenceCode;
            this.messageId = this.orderDetail.messageId;
            this.messageType = this.orderDetail.messageType;
            this.messageCreatedAt = this.orderDetail.messageCreatedAt;
            this.statusCurrent = this.orderDetail.statusCurrent;
            this.shipToPartyId = this.orderDetail.shipToPartyId;
            this.shipToAddressName = this.orderDetail.shipToAddressName;
            this.shipToAddressLine1 = this.orderDetail.shipToAddressLine1;
            this.shipToAddressLine2 = this.orderDetail.shipToAddressLine2;
            this.shipToAddressCity = this.orderDetail.shipToAddressCity;
            this.shipToAddressCountryCode = this.orderDetail.shipToAddressCountryCode;
            this.shipToAddressPostCode = this.orderDetail.shipToAddressPostCode;
            this.billToPartyId = this.orderDetail.billToPartyId;
            this.billToAddressName = this.orderDetail.billToAddressName;
            this.billToAddressLine1 = this.orderDetail.billToAddressLine1;
            this.billToAddressLine2 = this.orderDetail.billToAddressLine2;
            this.billToAddressCity = this.orderDetail.billToAddressCity;
            this.billToAddressPostCode = this.orderDetail.billToAddressPostCode;
            this.billToAddressCountryCode = this.orderDetail.billToAddressCountryCode;
            this.itemsConfirmedCountTotal = this.orderDetail.itemsConfirmedCountTotal;
            this.itemsConfirmedSumTotal = this.orderDetail.itemsConfirmedSumTotal;
            this.itemsOrderedCountTotal = this.orderDetail.itemsOrderedCountTotal;
            this.itemsOrderedSumTotal = this.orderDetail.itemsOrderedSumTotal;
            this.orderDetail.statusCycle.forEach(function (statusCycle: any) {
                if (statusCycle.cycle == 'confirm' || statusCycle.cycle == 'CONFIRMED') {
                    statuscycletimestampConfirmed = statusCycle.time;
                }
                if (statusCycle.cycle == 'pick' || statusCycle.cycle == 'PICK') {
                    statuscycletimeStampPicked = statusCycle.time;
                }

                if (statusCycle.cycle == 'ship' || statusCycle.cycle == 'SHIP') {
                    statuscycletimestampShip = statusCycle.time;
                }
                if (statusCycle.cycle == 'receipt' || statusCycle.cycle == 'RECEIPT') {
                    statuscycletimestampReicipt = statusCycle.time;
                }
                if (statusCycle.cycle == 'account' || statusCycle.cycle == 'ACCOUNT') {
                    statuscycletimestampAccounted = statusCycle.time;
                }
                if (statusCycle.cycle == 'invoice' || statusCycle.cycle == 'INVOICE') {
                    statuscycletimestampInvoiced = statusCycle.time;
                }
            });

            statuscycletimestampRaised = this.orderDetail.orderRaisedDate;


            this.timeStampConfirmed = statuscycletimestampConfirmed;
            this.timeStampRaised = statuscycletimestampRaised;
            this.timeStampShip = statuscycletimestampShip;
            this.timeStampReceipt = statuscycletimestampReicipt;
            this.timeStampAccounted = statuscycletimestampAccounted;
            this.timeStampInvoiced = statuscycletimestampInvoiced;
            this.timeStampPicked = statuscycletimeStampPicked;
            var isinvoice: boolean;
            var ISINVOICEpresent: boolean;
            this.orderDetail.statusCycle.forEach(function (statusCycle: any) {
                if (statusCycle.cycle == "invoice") {
                    console.log(statusCycle.params.invoiceFileLocation);
                    if (statusCycle.params.invoiceFileLocation != undefined) {

                        isinvoice = true;
                        console.log(isinvoice + "isinvoice");
                        ISINVOICEpresent = isinvoice;
                        console.log(ISINVOICEpresent + "this.ISINVOICEpresent");
                    } else {
                        console.log("false");
                        isinvoice = false;
                        console.log(isinvoice + "isinvoice");
                        ISINVOICEpresent = isinvoice;
                        console.log(ISINVOICEpresent + "this.ISINVOICEpresent");
                    }
                }
                isinvoice = false;
            });
            this.ISINVOICEpresent = ISINVOICEpresent;

            if (this.orderDetail.billToTotalNetAmount != undefined) {
                this.billToTotalNetAmount = this._globalFunc.getPriceformat(this.orderDetail.billToTotalNetAmount.toString())
            } else {
                this.billToTotalNetAmount = this.orderDetail.billToTotalNetAmount;
            }
            if (this.orderDetail.billToTotalTaxAmount != undefined) {
                this.billToTotalTaxAmount = this._globalFunc.getPriceformat(this.orderDetail.billToTotalTaxAmount.toString())
            } else {
                this.billToTotalTaxAmount = this.orderDetail.billToTotalTaxAmount;
            }
            if (this.orderDetail.billToTotalNetAmount != undefined && this.orderDetail.billToTotalTaxAmount != undefined) {
                let totalAmt = this.orderDetail.billToTotalNetAmount + this.orderDetail.billToTotalTaxAmount;
                this.billToTotalAmount = this._globalFunc.getPriceformat(totalAmt.toString());
            } else {
                this.billToTotalAmount = "-";
            }

            this.itemsPickedSumTotal = this.orderDetail.itemsPickedSumTotal;
            this.itemsShippedCountTotal = this.orderDetail.itemsShippedCountTotal;
            this.itemsPickedCountTotal = this.orderDetail.itemsPickedCountTotal;
            this.itemsShippedSumTotal = this.orderDetail.itemsShippedSumTotal;
            this.billToId = this.orderDetail.billToId;

            this.createdAt = this.orderDetail.createdAt;
            this._globalFunc.logger("Order items service.");
            var statuscycletimestampConfirmed: any;
            var statuscycletimestampRaised: any;
            var statuscycletimeStampPicked: any;
            var statuscycletimestampShip: any;
            var statuscycletimestampReicipt: any;
            var statuscycletimestampAccounted: any;
            var statuscycletimestampInvoiced: any;
            this._postsService.getOrderItems(this.orgApiName, value).subscribe(data => {
                this.showLoader = false;
                this.orderItemList = data.items;
                 //console.log("DATA==>"+JSON.stringify(data.items));
                var netTotal = 0;
                var totalTotal = 0;
                
                let that = this;
                //  var data1=orderValidationList;
                orderValidationList = this.orderValidationmsgsList;
                this.orderItemList.forEach(function (item: any) {
                    // For statusValidation
                    if (item.statusValidation || item.statusValidation !== undefined) {
                        let attributeList = item.statusValidation.split(";");
                        var statValColor = "";
                        var statValText = "";
                        //  var validationCodeMsg:string;
                        attributeList.forEach(function (attribute: any) {
                            let attributeInfo = attribute.split(":");
                            rangeerrorflag = true;
                            if (attributeInfo[1] == 0) {
                                if (attributeInfo[0] == "range" && attributeInfo[1] == 0) {
                                    rangeFlag = true;
                                }
                                if (attributeInfo[0] == "active" && rangeFlag == true) {
                                    rangeerrorflag = false;
                                }
                                if (attributeInfo[0] == "case" && rangeFlag == true) {
                                    rangeerrorflag = false;
                                }
                                if (attributeInfo[0] == "price" && rangeFlag == true) {
                                    rangeerrorflag = false;
                                }
                                if (attributeInfo[0] == "orderable" && rangeFlag == true) {
                                    rangeerrorflag = false;
                                }

                                if (attributeInfo[1] == 0) {
                                    let validationMessageExist = false;
                                    orderValidationList.forEach(function (validationList: any) {
                                        validationCode = validationList.validationCode;
                                        validationMessage = validationList.validationMessage;
                                        if (validationMessage == "" || validationMessage == undefined) {
                                            validationMessage = "Message is not configured";
                                        }

                                        if (attributeInfo[0] == validationCode) {
                                            validationMessageExist = true;
                                        }

                                        if (attributeInfo[0] == validationCode && rangeerrorflag == true) {
                                            statValText = statValText + attributeInfo[0] + "  :  " + validationMessage + ";" + "\n";
                                            statValColor = 'bg-red txtclr-white';
                                        }
                                    });

                                    if (!validationMessageExist) {
                                        statValText = statValText + attributeInfo[0] + " : Business friendly message has not been configured;" + "\n";
                                        statValColor = 'bg-red txtclr-white';
                                    }
                                }
                            }
                        });

                        if (statValText == "") {
                            statValColor = 'bg-green txtclr-white';
                            statValText = 'OK';
                        }
                    } else {
                        statValText = "-";
                        statValColor = 'bg-grey';
                    }
                    item.statValColor = statValColor;
                    item.statValText = statValText;

                   
                    // tempArr.push(item);
                    if (typeof item.billToNetAmount !== undefined && item.billToNetAmount > 0) {
                        if(item.messageType.toLowerCase() == "storepick"){                           
                            netTotal = netTotal + item.priceConfirmedAmount * (that.getQutyData(item.statusCurrent, item))
                            / (1 + (item.priceConfirmedTaxRate /100));           
                           
                        }
                        else{                           
                            netTotal = netTotal + item.billToNetAmount;
                        }
                    } else {                        
                        if(item.messageType.toLowerCase() == "storepick"){                         
                            netTotal = netTotal + item.priceConfirmedAmount * (that.getQutyData(item.statusCurrent, item))
                            / (1 + (item.priceConfirmedTaxRate /100)); 
                        }
                        else{                           
                            netTotal = netTotal + (item.priceConfirmedAmount * (that.getQutyData(item.statusCurrent, item)));
                        }
                    }
                    if (typeof item.billToAmount !== undefined && item.billToAmount > 0) {
                       
                        if(item.messageType.toLowerCase() == "storepick"){  
                            
                            totalTotal = totalTotal + (item.priceConfirmedAmount * that.getQutyData(item.statusCurrent, item));
                        }
                        else{                           
                            totalTotal = totalTotal + item.billToAmount;
                        }
                    } else {
                        if(item.messageType.toLowerCase() == "storepick"){                             
                            totalTotal = totalTotal + (item.priceConfirmedAmount * that.getQutyData(item.statusCurrent, item));
                        }
                        else{                            
                            totalTotal = totalTotal + ((item.priceConfirmedAmount * (that.getQutyData(item.statusCurrent, item))) * (item.priceConfirmedTaxRate / 100) + (item.priceConfirmedAmount * (that.getQutyData(item.statusCurrent, item))));
                        }
                    }
                   
                });
               
                this.net_Total = this._globalFunc.getPriceformat(netTotal.toString());
                this.total_Total = this._globalFunc.getPriceformat(totalTotal.toString());
                console.log("this.total_Total==> "+ this.total_Total);
            }, err => {
                this._globalFunc.logger("Order item service failed.");
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
            );
            //this.organisationList = data.body;
        }
            , err => {
                this._globalFunc.logger("Order detail service failed.");
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
        );
    }
    //tooltip for validation messageCreatedAt
    formatSelection(val: any) {
        console.log(val);
        if (val != null) {
            return this._globalFunc.formatSelection(val);
        } else {
            return '-';
        }
    }

    formatDate(createdAtDate: string) {
        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
            var d = new Date(createdAtDate);
            var yyyy = d.getFullYear();
            var mm = d.getMonth() < 10 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1); // getMonth() is zero-based
            var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
            // var hh = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
            // var min = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();

            return dd + "/" + mm + "/" + yyyy;
        } else {
            return '-';
        }
    }

    formatTime(createdAtDate: string) {

        var momenttm = require('moment-timezone');    

        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
            /*var d = new Date(createdAtDate);
            //var hh = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
            var hh = (d.getUTCHours()+1) < 10 ? "0" + (d.getUTCHours()+1) : (d.getUTCHours()+1);
            var min = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();*/

            var d = momenttm.tz(createdAtDate,"Europe/London").format('HH:mm:ss');

            var opieces = d.split(':');

            var hh = opieces[0];
            var min = opieces[1];
            
            return hh + ":" + min;
        } else {
            return '-';
        }
    }

    toggleArrow(divId: string) {
        document.getElementById(divId).classList.toggle("glyphicon-chevron-right");
        document.getElementById(divId).classList.toggle("glyphicon-chevron-left");
    }

    toggleTableColumns(tableId: string) {
        this._globalFunc.logger("Toggle drilldown table column for " + tableId);
        switch (tableId) {
            case "Product":
                if (this.identifierHide == true) {
                    this.identifierHeaderSize = 5;
                    this.identifierHide = false;
                } else {
                    this.identifierHeaderSize = 3;
                    this.identifierHide = true;
                }
                this.toggleArrow('orderItemTable_product');
                break;
            case "Quantity":
                if (this.quantityHide == true) {
                    this.identifierquantitySize = 5;
                    this.quantityHide = false;
                } else {
                    this.identifierquantitySize = 2;
                    this.quantityHide = true;
                }
                this.toggleArrow('orderItemTable_quantity');
                break;
            case "Price":
                if (this.priceHide == true) {
                    this.identifierpriceSize = 4;
                    this.priceHide = false;
                } else {
                    this.identifierpriceSize = 2;
                    this.priceHide = true;
                }
                this.toggleArrow('orderItemTable_price');
                break;
            case "Status":
                if (this.statusHide == true) {
                    this.identifierstatusSize = 5;
                    this.statusHide = false;
                } else {
                    this.identifierstatusSize = 1;
                    this.statusHide = true;
                }
                this.toggleArrow('orderItemTable_status');
                break;
        }
    }

    getQutyData(status: any, item: any) {
        if (status == 'raised' || status == 'RAISED') {
            return item.quantityOrdered;
        } else if (status == 'confirmed' || status == 'CONFIRMED') {
            return item.quantityConfirmed;
        } else if (status == 'picked' || status == 'PICKED') {
            return item.quantityPicked;
        } else if (status == 'SHIPPED' || status == 'shipped') {
            return item.quantityShipped;
        } else if (status == 'ACCOUNTED' || status == 'accounted') {
            return item.quantityShipped;
        } else if (status == 'INVOICED' || status == 'invoiced') {
            return item.quantityShipped;
        } else {
            return 1;
        }
    }

    showdetail(detailInfo: string) {
        //document.getElementById('OrderQuantityInfo').style.display='none';
        /*document.getElementById('OrderQuantityInfo').classList.toggle("hidden-md", true);
        document.getElementById('OrderQuantityInfo').classList.toggle("hidden-lg", true);

        document.getElementById('TransmissionInfo').style.display='none';
        document.getElementById('TransmissionInfo').classList.toggle("hidden-md", true);
        document.getElementById('TransmissionInfo').classList.toggle("hidden-lg", true);

        document.getElementById('ShippingAdd').style.display='none';
        document.getElementById('ShippingAdd').classList.toggle("hidden-md", true);
        document.getElementById('ShippingAdd').classList.toggle("hidden-lg", true);

        document.getElementById('BillingAdd').style.display='none';
        document.getElementById('BillingAdd').classList.toggle("hidden-md", true);
        document.getElementById('BillingAdd').classList.toggle("hidden-lg", true);

        document.getElementById('InvoiceInfo').style.display='none';
        document.getElementById('InvoiceInfo').classList.toggle("hidden-md", true);
        document.getElementById('InvoiceInfo').classList.toggle("hidden-lg", true);*/
        this._globalFunc.logger("Toggle drilldown header - " + detailInfo);
        switch (detailInfo) {
            case "OrderQuantityInfo":
                document.getElementById('OrderQuantityInfo').classList.toggle("hidden-md");
                document.getElementById('OrderQuantityInfo').classList.toggle("hidden-lg");
                document.getElementById('OrderQuantityInfo').classList.toggle("hidden-sm");
                document.getElementById('OrderQuantityInfo').classList.toggle("hidden-xs");
                document.getElementById('order_Qnt_Info').classList.toggle("glyphicon-menu-down");
                document.getElementById('order_Qnt_Info').classList.toggle("glyphicon-menu-up");
                break;

            case "TransmissionInfo":
                //document.getElementById('TransmissionInfo').style.display='block';
                document.getElementById('TransmissionInfo').classList.toggle("hidden-md");
                document.getElementById('TransmissionInfo').classList.toggle("hidden-lg");
                document.getElementById('TransmissionInfo').classList.toggle("hidden-sm");
                document.getElementById('TransmissionInfo').classList.toggle("hidden-xs");
                document.getElementById('trans_info').classList.toggle("glyphicon-menu-down");
                document.getElementById('trans_info').classList.toggle("glyphicon-menu-up");
                break;

            case "ShippingAdd":
                // document.getElementById('ShippingAdd').style.display='block';
                document.getElementById('ShippingAdd').classList.toggle("hidden-md");
                document.getElementById('ShippingAdd').classList.toggle("hidden-lg");
                document.getElementById('ShippingAdd').classList.toggle("hidden-sm");
                document.getElementById('ShippingAdd').classList.toggle("hidden-xs");
                document.getElementById('ship_add').classList.toggle("glyphicon-menu-down");
                document.getElementById('ship_add').classList.toggle("glyphicon-menu-up");
                break;

            case "BillingAdd":
                //document.getElementById('BillingAdd').style.display='block';
                document.getElementById('BillingAdd').classList.toggle("hidden-md");
                document.getElementById('BillingAdd').classList.toggle("hidden-lg");
                document.getElementById('BillingAdd').classList.toggle("hidden-sm");
                document.getElementById('BillingAdd').classList.toggle("hidden-xs");
                document.getElementById('bill_add').classList.toggle("glyphicon-menu-down");
                document.getElementById('bill_add').classList.toggle("glyphicon-menu-up");
                break;

            case "InvoiceInfo":
                //document.getElementById('InvoiceInfo').style.display='block';
                document.getElementById('InvoiceInfo').classList.toggle("hidden-md");
                document.getElementById('InvoiceInfo').classList.toggle("hidden-lg");
                document.getElementById('InvoiceInfo').classList.toggle("hidden-sm");
                document.getElementById('InvoiceInfo').classList.toggle("hidden-xs");
                document.getElementById('invoice_info').classList.toggle("glyphicon-menu-down");
                document.getElementById('invoice_info').classList.toggle("glyphicon-menu-up");
                break;
        }
    }
    showOrderTracker() {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if(res == 'sessionActive'){
                this.statusOrderTracker = true;
                this.statusOrderDetail = false;
                this.orderTrackerSelected = true;
                this.orderDetailSelected = false;
            }  else {
                let sessionModal = document.getElementById('popup_SessionExpired');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('popup_SessionExpired').style.display = 'block';
            }
        }, reason => {
            console.log(reason);
        });        
    }
    showOrderDetail() {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if(res=='sessionActive'){
                this.orderTrackerSelected = false;
                this.orderDetailSelected = true;
                this.statusOrderTracker = false;
                this.statusOrderDetail = true;
            } else {
                let sessionModal = document.getElementById('popup_SessionExpired');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('popup_SessionExpired').style.display = 'block';
            }
        }, reason => {
            console.log("I am here")
            console.log(reason);
        });
    }

    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.
                    if ( userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } 
                    else{
                    reject('sessionInactive');
                    sessionStorage.clear();
                    this._router.navigate(['']);
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        console.log("Session terminated");
                        reject('sessionInactive');

                        

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        resolve('sessionActive');
                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
                        this.organisationListChange = changeOrgAvailable;
                        this.orgimgUrl = orgImgUrl;
                        this.orgApiName = orgApiNameTemp;
                        // this.disabledCusChange = false;
                        if (this.organisationList.length == 1) {
                            this.disabledCusChange = true;
                        }

                        console.log("this.organisationList234");
                        console.log(this.organisationList);
                        let that = this;
                        let orderEnquiry = true;
                        let dashboardMenuPermission = false;
                        let userHavingPermission = false;
                        this.organisationList.forEach(function (organisation: any) {
                            if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                                console.log("organisation : " + that.customerName);
                                console.log(organisation);
                                organisation.permissionRoleList.forEach(function (permRoleList: any) {
                                    console.log("11");
                                    console.log(permRoleList);
                                    permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                        console.log("22");
                                        if (permRoleDtlList.permissionGroup == 'orders') {
                                            userHavingPermission = true;
                                            permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                                if (permList.permissionName == 'orders' && permList.permissionStatus == 'active') {
                                                    dashboardMenuPermission = true;
                                                }

                                                switch (permList.permissionName) {
                                                    case "orderEnquiry":
                                                        console.log("55");
                                                        orderEnquiry = false;
                                                        //alert(filfilment);
                                                        break;
                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                        that = null;
                        if (orderEnquiry) {
                            // redirect to order
                            this._router.navigate(["orders", customerName]);
                        }
                    }

                }
                //alert(this.disabledfilfilment);
            });
        });
    }

    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }
}

