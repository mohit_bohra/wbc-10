import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../services/app.services';
import { GlobalComponent } from '../global/global.component';
import { Constant } from '../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { commonfunction } from '../../services/commonfunction.services';

@Component({
    selector: 'orders',
    templateUrl: 'orders.component.html',
    providers: [commonfunction]
})
export class OrdersComponent {
    public showDatepicker: boolean = true;
    objConstant = new Constant();
    whoImgUrl: any;
    errorMsg: boolean;
    showLoader: boolean;
    hideOrganisation: boolean = true;
    pageState: number = 2;
    customerName: string;
    style: string;
    customerDispayName: string;
    organisationList: any;
    organisationListChange: any;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    homeUrl: any;
    private _sub: any;
    imgName: string = "";
    disabledorderEnquiry: boolean;
    disabledclaimsReview: boolean;
    disabledorderValidation: boolean;


    moduleName: string = "order";

    terminateFlg: boolean = false;
    siteMaintanceFlg: boolean;
    siteMaintanceFlg1: boolean;
    siteMSg: any;
    errorMsgString: string;
    deploymentDetailsMsg: string;
    featureDetailsMsg: string;
    businessMessageDetailsMsg: string;
    deploymentDetailsMsglength:any;
    featureDetailsMsglength:any;
    businessMessageDetailsMsglength:any;

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.errorMsg = false;
    }

    ngOnInit() {
        this._globalFunc.logger("Order screen loading");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._globalFunc.logger("User session not found");
            this._router.navigate(['']);
        } else {
            this.disabledorderEnquiry = false;
            this.disabledclaimsReview = false;
            this.disabledCusChange = false;
            this.showLoader = false;
            this._globalFunc.autoscroll();
            this.pageState = 2;
            sessionStorage.setItem('pageState', '2');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                this.imgName = this.getCustomerImage(params.orgType);
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                let changeOrgAvailable = new Array;

                let custPerm: any = [];
                let that = this;
                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
                    if (res == 'sessionActive') {
                        this.imgName = this.getCustomerImage(params.orgType);
                        this.orgApiName = this.customerName;
                        this.getConfigService(this.moduleName);


                        let wid: any;
                        wid = window.innerWidth;
                        let screen: string;

                        screen = this._globalFunc.getDevice(wid);
                        window.onresize = () => {

                            if (screen == "desktop") {
                                if (window.innerWidth < this._globalFunc.stdWidth) {
                                } else {
                                }
                            } else {
                                if (window.innerWidth > this._globalFunc.stdWidth) {
                                    document.getElementById("resultModalClose").click();
                                } else {
                                }
                            }

                            screen = this._globalFunc.getDevice(window.innerWidth);
                        };
                    }
                }, reason => {
                    console.log(reason);
                });
                that = null;

            });
        }

        
        //this.siteMaintanceFlg1 = sessionStorage.getItem('site');
        // if(sessionStorage.getItem('site') == "true"){           
        //     this.siteMaintanceFlg1 = false;
        // }else { 
        //     this.siteMSg = "Site is under maintenance";
        //     this.siteMaintanceFlg1 = true;
        // }
    }

    redirect(page: any, orgType: any) {
        this._globalFunc.logger("Redirecting to page." + page + " for customer " + orgType);
        this._router.navigate([page, orgType]);
    }

    changeOrgType(page: any, orgType: any) {
        this._globalFunc.logger("Changing customer to " + orgType);
        this.toggleOrganisation();
        sessionStorage.setItem('orgType', orgType);
        this._router.navigate(this._globalFunc.redirectToComponent('orders', orgType));
    }

    ngOnDestroy() {
        this._globalFunc.logger("Order screen unloading");
        this._globalFunc.sentLoggerBatchToServer();
    }

    toggleOrganisation() {
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
        } else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
        }
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else {
                        reject('sessionInactive');
                        sessionStorage.clear();
                        this._router.navigate(['']);
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        resolve('sessionActive');
                        console.log("get user session");
                        this.terminateFlg = false;
                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
                        this.organisationListChange = changeOrgAvailable;
                        this.orgimgUrl = orgImgUrl;
                        this.orgApiName = orgApiNameTemp;
                        if (this.organisationList.length == 1) {
                            this.disabledCusChange = true;
                        }

                        console.log("this.organisationList234");
                        console.log(this.organisationList);
                        let that = this;
                        let orderEnquiry = true;
                        let claimsReview = true;
                        let orderValidation = true;
                        let dashboardMenuPermission = false;
                        let userHavingPermission = false;
                        this.organisationList.forEach(function (organisation: any) {

                            if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                                console.log("organisation : " + that.customerName);
                                console.log(organisation);
                                organisation.permissionRoleList.forEach(function (permRoleList: any) {
                                    console.log("11");
                                    console.log(permRoleList);
                                    permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                        console.log("22");
                                        if (permRoleDtlList.permissionGroup == 'orders') {
                                            userHavingPermission = true;
                                            permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                                if (permList.permissionName == 'orders' && permList.permissionStatus == 'active') {
                                                    dashboardMenuPermission = true;
                                                }

                                                switch (permList.permissionName) {
                                                    case "orderEnquiry":
                                                        console.log("55");
                                                        orderEnquiry = false;
                                                        break;
                                                    case "claimsReviewView":
                                                        console.log("55");
                                                        claimsReview = false; 
                                                        break;
                                                    case "orderValidation":
                                                        console.log("56");
                                                        orderValidation = false; 
                                                    break;
                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                        that = null;
                        /*if (!userHavingPermission) {
                            this._router.navigate(["reports", customerName]);
                        }*/
                        this.disabledorderEnquiry = orderEnquiry;
                        this.disabledclaimsReview = claimsReview;
                        this.disabledorderValidation = orderValidation;
                    }
                }
            });
        });
    }

    redirect_page(redirectArray: any) {
        this._router.navigate(redirectArray);
    }

    getConfigService(pageName: any) {
        this._postsService.siteMaintanceCall('1', 'all').subscribe(userData => {
            console.log(userData);
            console.log(userData.maintenanceDetails[0].message);
            console.log(userData.maintenanceDetails[0].start);
            console.log(userData.maintenanceDetails[0].status);
            console.log(userData.deploymentDetails);
            if (userData.deploymentDetails != undefined && userData.deploymentDetails[0].status != 'NO') {
                this.deploymentDetailsMsg = userData.deploymentDetails[0].message + ' ' + userData.deploymentDetails[0].start;
                 this.deploymentDetailsMsglength= this.deploymentDetailsMsg.length;
                   if (sessionStorage.getItem('deploymentDetailsMsglength') == undefined || sessionStorage.getItem('deploymentDetailsMsglength') == null) {
                    sessionStorage.setItem('deploymentDetailsMsglength', this.deploymentDetailsMsglength);
                    console.log(this.deploymentDetailsMsglength +"this.deploymentDetailsMsglength");
                } else {
                    this.deploymentDetailsMsglength = sessionStorage.getItem('deploymentDetailsMsglength');
                    console.log(this.deploymentDetailsMsglength +"this.deploymentDetailsMsglength");
                }

            } else {
                this.deploymentDetailsMsg = '';
            }


            console.log(userData.featureDetails);
            if (userData.featureDetails != undefined && userData.featureDetails[0].status != 'NO') {
                this.featureDetailsMsg = userData.featureDetails[0].message + ' ' + userData.featureDetails[0].start;
                this.featureDetailsMsglength = this.featureDetailsMsg.length;
                if (sessionStorage.getItem('featureDetailsMsglength') == undefined || sessionStorage.getItem('featureDetailsMsglength') == null) { 
                     sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
                }
                else {
                    this.deploymentDetailsMsglength = sessionStorage.getItem('featureDetailsMsglength');
                }
            } else {
                this.featureDetailsMsg = '';
            }

            console.log(userData.businessMessageDetails);
            if (userData.businessMessageDetails != undefined && userData.businessMessageDetails[0].status != 'NO') {
                this.businessMessageDetailsMsg = userData.businessMessageDetails[0].message + ' ' + userData.businessMessageDetails[0].start;
                this.businessMessageDetailsMsglength = this.businessMessageDetailsMsg.length;
                if (sessionStorage.getItem('businessMessageDetailsMsglength') == undefined || sessionStorage.getItem('businessMessageDetailsMsglength') == null) {                    sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
                    sessionStorage.setItem('businessMessageDetailsMsglength', this.businessMessageDetailsMsglength);
                }
                else {
                   this.businessMessageDetailsMsglength = sessionStorage.getItem('businessMessageDetailsMsglength');
                }
            } else {
                this.businessMessageDetailsMsg = '';
            }

             

            (<HTMLLabelElement>document.getElementById('deploymentDetailsId')).textContent = this.deploymentDetailsMsg;
            (<HTMLLabelElement>document.getElementById('featureDetailsId')).textContent = this.featureDetailsMsg;
            (<HTMLLabelElement>document.getElementById('businessMessageDetailsId')).textContent = this.businessMessageDetailsMsg;
            if (userData.maintenanceDetails[0].moduleName == 'all') {
                if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                    this.siteMaintanceFlg = true;
                    this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                } else {
                    this.siteMaintanceFlg = false;
                    this._postsService.siteMaintanceCall(this.customerName, pageName).subscribe(userData => {
                        console.log(userData);
                        console.log(userData.maintenanceDetails[0].message);
                        console.log(userData.maintenanceDetails[0].start);
                        console.log(userData.maintenanceDetails[0].status);
                        if (userData.maintenanceDetails[0].moduleName == pageName) {
                            if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                                this.siteMaintanceFlg = true;
                                this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                            } else {
                                this.siteMaintanceFlg = false;
                            }
                        } else {
                            this.siteMaintanceFlg = false;
                        }
                        this.showLoader = false;
                    }, error => {
                        console.log(error);
                        if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                            this.siteMaintanceFlg = true;
                            this.siteMSg = "Site is under maintenance";
                        } else if (error.errorCode == '404.26.408') {
                            this.errorMsgString = "User is Inactive";
                        }
                        else {
                            this.siteMaintanceFlg = false;
                        }
                        this.showLoader = false;
                    });
                }
            } else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        }, error => {
            console.log(error);
            if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                this.siteMaintanceFlg = true;
                this.siteMSg = "Site is under maintenance";
            } else if (error.errorCode == '404.26.408') {
                this.errorMsgString = "User is Inactive";
            }
            else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        });
    }

    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }
}


