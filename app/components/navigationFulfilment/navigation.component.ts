import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Router, ActivatedRoute } from '@angular/router';
import { Constant } from '../constants/constant';
import { Services } from '../../services/app.services';
import { commonfunction } from '../../services/commonfunction.services';

@Component({
    selector: 'main-navigation-menu',
    templateUrl: 'navigation.component.html',
})
export class NavigationComponent {

    customerFlag: boolean;
    objConstant = new Constant();
    whoImgUrl: string;
    orgApiName: string;
    _sub: any;
    customerName: any;
    homeUrl: any;
    loggedInUserId:any;
    loggedInUserGroupId:any;

    organisationList: any;
    organisationListChange: any;
    orgimgUrl: string;

    showPendingReq: boolean;
    donotshow: boolean = true;

    currentComponent: string = "";
    currentScreen: string = "";
    currentCustomerPermission: any;

    /********************** Properties to disable Navigation Icons - START **************************/
    enableOrder: boolean = false;
    enablecatalogueEnquiry: boolean = false;
    enablecatalogueManagement: boolean = false;
    enablereports: boolean = false;
    enabledashboards: boolean = false;
    enableaccessMgmt: boolean = false;
    

    /** Order Specific Navigation */
    enableraiseNewOrder: boolean = false;
    enableOrderEnquiry: boolean = false;
    enableorderMgmtClaims: boolean = false;

    /* Catalogue specific navigation */
    enablecatalogueAddNewProducts: boolean = false;
    enablecatalogueUploadCatalogue: boolean = false;
    enablecatalogueExportCatalogue: boolean = false;

    /* Dashboard specific navigation */
    enablefilfilment: boolean = false;

    /********************** Properties to disable Navigation Icons - END ****************************/

    orderPermsArray:any = [];
    catalogueEnquiryPermsArray:any = [];
    catalogueManagementPermsArray:any = [];
    reportsPermsArray:any = [];
    dashboardPermsArray:any = [];
    accessManagementPermsArray:any = [];

    loggedInUserGroup:string = "";
    loggedInUserAdminFlag:string = "";
    superAdminGroup:string = "";

    msgJSON:any = {};

    constructor(private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, private _postsService: Services, public _cfunction: commonfunction) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.superAdminGroup = this.objConstant.SUPERADMIN_GROUP;
    }

    // function works on page load
    ngOnInit() {
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {
            // subscribe to route params
            //this._sub = this._route.params.subscribe((params: { orgType: string }) => {
            //sessionStorage.getItem('orgType', params.orgType);

            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                this.customerName = params.orgType;
            });

            //this.customerName = sessionStorage.getItem('orgType');
            
            this.loggedInUserGroup = sessionStorage.getItem('loggedInUserGroupName');
            this.loggedInUserAdminFlag = sessionStorage.getItem('loggedInUserAdminFlag');
            this.loggedInUserId = sessionStorage.getItem('userId');
            this.loggedInUserGroupId = sessionStorage.getItem('loggedInUserGroupId');

            let cusType = this.customerName;
            let orgImgUrl;
            let orgApiNameTemp;
            let tempPermissionArr: any = [];

            // Read current URL
            let currentUrl = this._router.url.split("/");
            this.currentComponent = currentUrl[1];
            this.currentScreen = currentUrl[currentUrl.length - 1];
            this.getUserSession(this.customerName, this.loggedInUserGroupId, this.loggedInUserId);
            this.currentCustomerPermission = tempPermissionArr;
            tempPermissionArr = null;
        }
    }

    showScreenDependentNavigation() {
        switch (this.currentComponent) {
            case "orders":
                this.enableOrderDependentNavigation();
                break;
            case "catalogue":
                this.enableCatalogueDependentNavigation();
                break;
            case "cataloguemanagement":
                this.enableCatalogueMAnagementDependentNavigation();
                break;
            case "fulrangeexport":
                break;
            case "fullrangeexport":
                break;
            case "reports":
                this.enablereports = false;
                break;
            case "dashboard":
                this.enableDashboardDependentNavigation();
                break;
            case "groups":
                this.enableaccessMgmt = false;
                break;
        }
    }

    enableDashboardDependentNavigation() {
        this.enabledashboards = false;

        let that = this;
        this.dashboardPermsArray.forEach(function(itm:any){
            if(itm.permissionName == 'fulfilment'){
                that.enablefilfilment = true;
            }
        });

        // Disable current navigation icon
        let currentUrl = this._router.url.split("/");

        if (currentUrl[4] == 'levelone' || currentUrl[4] == 'leveltwo') {
            this.enabledashboards = true;
        } else {
            switch (this.currentScreen) {
                case "fulfilment":
                case "levelone":
                    this.enabledashboards = true;
                    break;
            }
        }

    }

    enableCatalogueMAnagementDependentNavigation() {
        if (this._router.url.split("/").length > 3) {
            this.enablecatalogueManagement = true;
        } else {
            this.enablecatalogueManagement = false;
        }
    }

    enableOrderDependentNavigation() {
        this.enableOrder = false;
        let that = this;
        this.orderPermsArray.forEach(function(itm:any){
            if(itm.permissionName == 'raiseNewOrder'){
                that.enableraiseNewOrder = true;
            }

            if(itm.permissionName == 'orderEnquiry'){
                that.enableOrderEnquiry = true;
            }
            
            if(itm.permissionName == 'orderClaims'){
                that.enableorderMgmtClaims = true;
            }
        });

        switch (this.currentScreen) {
            case "claims":
                this.enableorderMgmtClaims = false;
                this.enableOrder = true;
                break;
            case "enquiry":
                this.enableorderMgmtClaims = false;
                this.enableOrder = true;
                break;
            case "newOrder":
                this.enableraiseNewOrder = false;
                this.enableOrder = true;
                break;
        }
    }

    enableCatalogueDependentNavigation() {
        this.enablecatalogueEnquiry = false;

        let that = this;
        this.catalogueEnquiryPermsArray.forEach(function(itm:any){
            if(itm.permissionName == 'addNewProducts'){
                that.enablecatalogueAddNewProducts = true;
            }
            if(itm.permissionName == 'uploadCatalogue'){
                that.enablecatalogueUploadCatalogue = true;
            }
            if(itm.permissionName == 'exportCatalogue'){
                that.enablecatalogueExportCatalogue = true;
            }
        });

        // Disable current navigation icon
        switch (this.currentScreen) {
            case "add":
                this.enablecatalogueAddNewProducts = false;
                this.enablecatalogueEnquiry = true;
                break;
            case "upload":
                this.enablecatalogueUploadCatalogue = false;
                this.enablecatalogueEnquiry = true;
                this.enablecatalogueAddNewProducts = false;
                break;
            case "export":
                this.enablecatalogueExportCatalogue = false;
                this.enablecatalogueEnquiry = true;
                this.enablecatalogueAddNewProducts = false;
                break;
        }
    }

    // navigate function
    redirect(urlArr: any[], newTab: boolean = false) {
        let currentCustomer = sessionStorage.getItem('orgType');
        urlArr[1] = currentCustomer;
        console.log('URL',urlArr);
        if (sessionStorage.getItem("unsaveChagne") == 'true') {
            this.changeModal(urlArr[0]);
        }else{
            if (newTab) {
                let url = this.homeUrl + urlArr.join("/");
                window.open(url);
            } else {
                this._router.navigate(urlArr);
            }
        }
    }

    setPermissionsArray(permissions:any, permsGroup:any){
        let that = this;
        switch(permsGroup){
            case "orders":
                permissions.forEach(function(perms:any){
                    that.orderPermsArray.push(perms);
                });
                break;
            case "catalogueManagement":
                permissions.forEach(function(perms:any){
                    that.catalogueManagementPermsArray.push(perms);
                });
                break;
            case "reports":
                permissions.forEach(function(perms:any){
                    that.reportsPermsArray.push(perms);
                });
                break;
            case "dashboard":
                permissions.forEach(function(perms:any){
                    that.dashboardPermsArray.push(perms);
                });
                break;
            case "crossCustomer":
                permissions.forEach(function(perms:any){
                    that.accessManagementPermsArray.push(perms);
                });
                break;
            case "catalogueEnquiry":
                permissions.forEach(function(perms:any){
                    that.catalogueEnquiryPermsArray.push(perms);
                });
                break;
        }
    }

    getUserSession(customerName:any, loggedInUserGroup:any, loggedInUserId:any){
        console.log("session checking");
        this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData:any) => {
            console.log(userData.isSessionActive);
            if(!userData.isSessionActive) { // false
                // User session has expired.
                if(userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser){
                    console.log("Session terminated");
                    this.msgJSON = {
                        "width314":true,
                        "title":"Session terminated",
                        "message":userData.sessionMessage,
                        "okButton":true,
                        "okButtonLabel":"OK",
                        "cancelButton":false,
                        "cancelButtonLabel":"",
                        "showPopup":true
                    };

                    let dt = this._cfunction.getUserSession();
                    this._postsService.authLoginTracker(this.customerName, this.loggedInUserGroupId, this.loggedInUserId, '' , dt).subscribe((userData: any) => {
                        console.log(userData);
                    }, (error: any) =>{
                        console.log(error);
                    });

                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                }
                else{
                sessionStorage.clear();
                this._router.navigate(['']);
                }
            } else {
                console.log(userData.sessionMessage);
                if(userData.sessionMessage == this.objConstant.userSessionTerminatedMsg){
                    console.log("Session terminated");
                    this.msgJSON = {
                        "width314":true,
                        "title":"Session terminated",
                        "message":userData.sessionMessage,
                        "okButton":true,
                        "okButtonLabel":"OK",
                        "cancelButton":false,
                        "cancelButtonLabel":"",
                        "showPopup":true
                    };

                    let dt = this._cfunction.getUserSession();
                    this._postsService.authLoginTracker(this.customerName, this.loggedInUserGroupId, this.loggedInUserId, '' , dt).subscribe((userData: any) => {
                        console.log(userData);
                    }, (error: any) =>{
                        console.log(error);
                    });

                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                }
                this.enablecatalogueEnquiry = false;
                this.enableOrder = false;
                this.enableaccessMgmt = false;
                this.enablereports = false;
                this.enablecatalogueManagement = false;
                this.enabledashboards = false;

                if(userData.userPermissionJson.groupName == this.superAdminGroup) {
                  
                    this.enableaccessMgmt = true;
                } else {
                    if(typeof userData.userPermissionJson.credentials.adminFlag !== 'undefined'){
                        if(userData.userPermissionJson.credentials.adminFlag == true) {
                            this.enableaccessMgmt = true;
                        } else {
                            this.enableaccessMgmt = false;
                        }
                    } else {
                        this.enableaccessMgmt = false;
                    }
                }
                
                this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                let that = this;
                this.organisationList.forEach(function (organisation: any) {
                    if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                        organisation.permissionRoleList.forEach(function(permRoleList: any){
                            permRoleList.permissionDetailsList.forEach(function(permRoleDtlList: any){
                                that.setPermissionsArray(permRoleDtlList.permissionNameList,permRoleDtlList.permissionGroup);
                                switch(permRoleDtlList.permissionGroup){
                                    case "catalogueEnquiry":
                                        that.enablecatalogueEnquiry = true;
                                        break;
                                    case "orders":
                                        that.enableOrder = true;
                                        break;
                                    case "accessMgmt":
                                        that.enableaccessMgmt = true;
                                        break;
                                    case "reports":
                                        that.enablereports = true;
                                        break;
                                    case "catalogueManagement":
                                        that.enablecatalogueManagement = true;
                                        break;
                                    case "dashboards":
                                        that.enabledashboards = true;
                                        break;
                                }
                            });
                        });
                    }
                });
                that = null;
                this.showScreenDependentNavigation();
            }
        });
    }

    reloadNavigation(orgType:string){
        if(sessionStorage.getItem('changeCustomer')!=='false' && sessionStorage.getItem('changeCustomer') != undefined){
            sessionStorage.setItem('orgType', orgType);
        }
        this.ngOnInit();
    }

    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        sessionStorage.clear();
        this._router.navigate(['']);
    }

    /*change modal changes starts here*/
    changeModal(pageName:any){
        sessionStorage.setItem('pageName',pageName);
        console.log('PageName:',sessionStorage.getItem('pageName'));
        let sessionModal = document.getElementById('unsaveModal');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('unsaveModal').style.display = 'block';

        setTimeout(() => {               
        }, this.objConstant.WARNING_TIMEOUT);
    }

    keepMeonSamePage(){
        let sessionModal = document.getElementById('unsaveModal');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('unsaveModal').style.display = 'none';
    }

    navigatTopage(){
        this.keepMeonSamePage();
        sessionStorage.setItem("unsaveChagne","false");
        let urlAr = JSON.parse(sessionStorage.getItem("urlArr"));
        let pageName = sessionStorage.getItem('pageName');
        let currentCustomer = sessionStorage.getItem('orgType');
        console.log('Page Name',pageName);
        console.log('Customer Name',currentCustomer);
        if(pageName == ''){
            this._router.navigate(['/']);
        }else if(pageName!='customerselection'){
            this._router.navigate([pageName,currentCustomer]);
        }else{
            this._router.navigate([pageName]);
        }
    }
    /*change modal changes ends here*/
}