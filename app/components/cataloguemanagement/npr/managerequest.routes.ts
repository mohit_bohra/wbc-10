import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageRequestComponent } from './managerequest.component';

// route Configuration
const managerequestRoutes: Routes = [
  { path: '', component: ManageRequestComponent },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(managerequestRoutes)],
  exports: [RouterModule]
})
export class ManageRequestRoutes { }
