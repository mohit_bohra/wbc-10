import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MaintainSupplyDepotComponent } from './maintainsupplydepot.component';
import { CanDeactivateGuard } from '../../../services/can-deactivate-guard.service';

// route Configuration
const maintainsupplydepotRoutes: Routes = [
  { path: '', component: MaintainSupplyDepotComponent, canDeactivate: [CanDeactivateGuard] }
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(maintainsupplydepotRoutes)],
  exports: [RouterModule]
})
export class MaintainSupplyDepotComponentRoutes { }
