import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: "orderBy"
})
export class SDCategorySortPipe implements PipeTransform {
    transform(array: Array<any>, args: string='categoryName'): Array<any> {
        array.sort((a: any, b: any) => {
            if (a[args].toUpperCase() < b[args].toUpperCase()) {
                return -1;
            } else if (a[args].toUpperCase() > b[args].toUpperCase()) {
                return 1;
            } else {
                return 0;
            }
        });
        return array;
    }
}