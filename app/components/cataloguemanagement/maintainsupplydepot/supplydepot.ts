export class SupplyDepot {
	"customerId": string;
	"maintainSupplyDepot": [{
		"supplyDepotId": number;
		"supplyingDhlDepotName": string;
		"productCategoryDetails": [{
			"productCategoryId": number;
			"categoryName": string;
			"mainFrameStoreId": number;
			"rmsSellingStoreId": number;
			"rmsWarehouseId": number;
		}]
	}]
}