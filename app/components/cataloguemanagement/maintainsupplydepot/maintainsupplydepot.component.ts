import { Component, OnInit, ViewChild, Renderer } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DialogService } from '../../../services/dialog.service';
import { isNull } from 'util';
import { CategoryFilter } from '../../catalogue/stores/edit/categoryFilter.pipes';

@Component({
    templateUrl: 'maintainsupplydepot.component.html',
    providers: [commonfunction]
})
export class MaintainSupplyDepotComponent {
    deploymentDetailsMsglength: any;
    featureDetailsMsglength: any;
    @ViewChild('maintainSuppDepot') maintainSuppDepot:any;
    @ViewChild('newSupplyDepotName') newSupplyDepotName: any;
    objConstant = new Constant();
    whoImgUrl: any;
    showLoader: boolean;
    printHeader: boolean;
    pageState: number = 51;
    customerName: string;
    homeUrl: any;
    moduleName: string;
    supplyingDepot: any;
    selectedCategory: string = "";
    selectedCategorySupplyDepot:any = [];
    chkDuplicateDO:boolean[] = [];
    hasDuplicateSD:boolean = false;
    terminateFlg: boolean;
    popupMsg: string;
    confirmBoxResponse: any;
    unsavedChange: boolean;
    customerDispayName: string;
    hideOrganisation: any = true;
    organisationList: any;
    organisationListChange: any;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    imgName: string;
    storeIdPrefix: any;
    newBoxDisplay: boolean = false;
    dropdownBox: boolean = true;
    addNewButton: boolean = true;
    addSaveButton: boolean = false;
    viewTable : boolean = true;
    editTable : boolean = false;
    dropdownList: any = [];
    supplyingDepotCategoryDD: any;
    supplyingDhlDepotName: any;
    prevSelectedCategory: string;
    supplyingDhlDepotNameNew: string = "";
    newSD:boolean = false; // Flag for new Supply depot
    disableSupplyDepotDD: boolean = false;
    supplyingDepotClone:any = [];
    siteMaintanceFlg: boolean;
    siteMSg: string;
    errorMsg: string;
    businessMessageDetailsMsg: string;
    deploymentDetailsMsg: string;
    featureDetailsMsg: any;
    businessMessageDetailsMsglength: any;
    addSupplyingDepotPermission: boolean = true;
    editSupplyingDepotPermission: boolean = true;

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, private formBuilder: FormBuilder, public dialogService: DialogService, private renderer: Renderer) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;

        this.maintainSuppDepot = this.formBuilder.group({});
    }

    // function works on page load
    ngOnInit() {
       this.onFirst();
    }

    onFirst(){
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {
            // window scroll function
            this._globalFunc.autoscroll();

            // set header basket data
            this.pageState = 51;
            this.moduleName = "catalogueManagement";
            sessionStorage.setItem('pageState', '51');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            

            // subscribe to route params
            this._route.params.subscribe((params: { orgType: string }) => {
                this.getUserSession(params.orgType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res=>{
                    if (res == 'sessionActive') {
                        this.getConfigService(this.moduleName);
                        this.supplyingDepot = this._route.snapshot.data['supplyDepot'].maintainSupplyDepot;
                        this.supplyingDepotCategoryDD = this._route.snapshot.data['supplyDepotCategoryDD'].productCategoryDropDownList;
                        this.hasDuplicateSD=false;
                        this._cfunction.errorMsgList = [];
                        sessionStorage.setItem('orgType', params.orgType);
                        if(this.customerName != params.orgType){
                            this.supplyingDhlDepotName = undefined;
                        }
                        this.customerName = params.orgType;
                        this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                        this.storeIdPrefix = this.objConstant.storePrefix[params.orgType.toLowerCase()];
                        
                        let tempArr:any = [];
                        if(this.supplyingDepotCategoryDD != undefined) {
                            this.supplyingDepotCategoryDD.forEach(function(arr:any){
                                tempArr[arr.productCategoryName] = arr;
                            });
                        }
                        this.supplyingDepotCategoryDD = tempArr;
                       
                        console.log('supplying depot : ' + this.supplyingDhlDepotName); //debugger;
                        if(this.supplyingDepot != undefined){
                            this.imgName = this.getCustomerImage(params.orgType);
                            this.sortSupplyingDepot().then(res=>{
                                this.supplyingDepotClone = JSON.stringify(this.supplyingDepot);
                                let sdlength = this.supplyingDepot.length;
                               
                                if(this.supplyingDhlDepotName == undefined){
                                   this.supplyingDhlDepotName = this.supplyingDepot[0].supplyingDhlDepotName;
                                }
                               
                                for(let i=0; i<sdlength; i++){
                                    this.chkDuplicateDO.push(false);
                                }
                                sessionStorage.setItem("unsavedChanges", 'false');
                                this.addSaveButton = false;
                                this.addNewButton = true;
                                this.newBoxDisplay = false;
                                this.disableSupplyDepotDD = false;
                                
                                this.showSupplyingDepotTable(this.supplyingDhlDepotName);
                               
                            });
                        } else {
                            //this.supplyingDepot = [];
                        }

                        let permissions = JSON.parse(sessionStorage.getItem('tempPermissionArray'));
                        if(permissions[this.customerName.toLowerCase()]['catalogueManagement'].indexOf('addSupplyingDepot')== -1){
                            this.addSupplyingDepotPermission = false;
                        }else{
                            this.addSupplyingDepotPermission = true;
                        }

                        if(permissions[this.customerName.toLowerCase()]['catalogueManagement'].indexOf('editSupplyingDepot')== -1){
                            this.editSupplyingDepotPermission = false;
                        }else{
                            this.editSupplyingDepotPermission = true;
                        }
                    }
                }, reason=>{
                    //console.log(reason);
                });
                
            });
        }
        this.showLoader = false;
    }
    //site maintenance
	getConfigService(pageName: any) {
		this._postsService.siteMaintanceCall('1', 'all').subscribe(userData => {
			console.log(userData);
			console.log(userData.maintenanceDetails[0].message);
			console.log(userData.maintenanceDetails[0].start);
			console.log(userData.maintenanceDetails[0].status);
			console.log(userData.deploymentDetails);
			if (userData.deploymentDetails != undefined && userData.deploymentDetails[0].status != 'NO') {
				this.deploymentDetailsMsg = userData.deploymentDetails[0].message + ' ' + userData.deploymentDetails[0].start;
				this.deploymentDetailsMsglength = this.deploymentDetailsMsg.length;
				if (sessionStorage.getItem('deploymentDetailsMsglength') == undefined || sessionStorage.getItem('deploymentDetailsMsglength') == null) {
					sessionStorage.setItem('deploymentDetailsMsglength', this.deploymentDetailsMsglength);
					console.log(this.deploymentDetailsMsglength + "this.deploymentDetailsMsglength");
				} else {
					this.deploymentDetailsMsglength = sessionStorage.getItem('deploymentDetailsMsglength');
					console.log(this.deploymentDetailsMsglength + "this.deploymentDetailsMsglength");
				}

			} else {
				this.deploymentDetailsMsg = '';
			}


			console.log(userData.featureDetails);
			if (userData.featureDetails != undefined && userData.featureDetails[0].status != 'NO') {
				this.featureDetailsMsg = userData.featureDetails[0].message + ' ' + userData.featureDetails[0].start;
				this.featureDetailsMsglength = this.featureDetailsMsg.length;
				if (sessionStorage.getItem('featureDetailsMsglength') == undefined || sessionStorage.getItem('featureDetailsMsglength') == null) {
					sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
				}
				else {
					this.deploymentDetailsMsglength = sessionStorage.getItem('featureDetailsMsglength');
				}
			} else {
				this.featureDetailsMsg = '';
			}

			console.log(userData.businessMessageDetails);
			if (userData.businessMessageDetails != undefined && userData.businessMessageDetails[0].status != 'NO') {
				this.businessMessageDetailsMsg = userData.businessMessageDetails[0].message + ' ' + userData.businessMessageDetails[0].start;
				this.businessMessageDetailsMsglength = this.businessMessageDetailsMsg.length;
				if (sessionStorage.getItem('businessMessageDetailsMsglength') == undefined || sessionStorage.getItem('businessMessageDetailsMsglength') == null) {
					sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
					sessionStorage.setItem('businessMessageDetailsMsglength', this.businessMessageDetailsMsglength);
				}
				else {
					this.businessMessageDetailsMsglength = sessionStorage.getItem('businessMessageDetailsMsglength');
				}
			} else {
				this.businessMessageDetailsMsg = '';
			}



			(<HTMLLabelElement>document.getElementById('deploymentDetailsId')).textContent = this.deploymentDetailsMsg;
			(<HTMLLabelElement>document.getElementById('featureDetailsId')).textContent = this.featureDetailsMsg;
			(<HTMLLabelElement>document.getElementById('businessMessageDetailsId')).textContent = this.businessMessageDetailsMsg;
			if (userData.maintenanceDetails[0].moduleName == 'all') {
				if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
					this.siteMaintanceFlg = true;
					this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
				} else {
					this.siteMaintanceFlg = false;
					this._postsService.siteMaintanceCall(this.customerName, pageName).subscribe(userData => {
						console.log(userData);
						console.log(userData.maintenanceDetails[0].message);
						console.log(userData.maintenanceDetails[0].start);
						console.log(userData.maintenanceDetails[0].status);
						if (userData.maintenanceDetails[0].moduleName == pageName) {
							if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
								this.siteMaintanceFlg = true;
								this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
							} else {
								this.siteMaintanceFlg = false;
							}
						} else {
							this.siteMaintanceFlg = false;
						}
						this.showLoader = false;
					}, error => {
						console.log(error);
						if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
							this.siteMaintanceFlg = true;
							this.siteMSg = "Site is under maintenance";
						} else if (error.errorCode == '404.26.408') {
							this.errorMsg = "User is Inactive";
						}
						else {
							this.siteMaintanceFlg = false;
						}
						this.showLoader = false;
					});
				}
			} else {
				this.siteMaintanceFlg = false;
			}
			this.showLoader = false;
		}, error => {
			console.log(error);
			if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
				this.siteMaintanceFlg = true;
				this.siteMSg = "Site is under maintenance";
			} else if (error.errorCode == '404.26.408') {
				this.errorMsg = "User is Inactive";
			}
			else {
				this.siteMaintanceFlg = false;
			}
			this.showLoader = false;
		});
	}

    showSupplyingDepotTable(sd:string){
        console.log('function : ' + sd);  //debugger;
        return new Promise((resolve) => {
            this.selectedCategory = sd;
            let that = this;
            this.selectedCategorySupplyDepot = [];
            this.supplyingDepot.forEach(function(itm:any){
                if(itm.supplyingDhlDepotName == sd){
                    that.selectedCategorySupplyDepot = itm.productCategoryDetails;
                }
            });
            console.log('function :' + JSON.stringify(this.supplyingDepot));
            resolve(true);
        });
    }

    addNewRowSD(){
        let tempSDRow = JSON.parse(JSON.stringify(this.supplyingDepot[0]));
        tempSDRow.supplyDepotId = 0;
        tempSDRow.supplyingDhlDepotName = '';
        
        this.selectedCategorySupplyDepot.forEach(function(itm:any, idx:number){
            tempSDRow.productCategoryDetails[idx].mainFrameStoreId = "";
            tempSDRow.productCategoryDetails[idx].rmsSellingStoreId = "";
            tempSDRow.productCategoryDetails[idx].rmsWarehouseId = "";
        });
        this.supplyingDepot.push(tempSDRow);
        console.log("this.supplyingDepot");
        console.log(this.supplyingDepot);
        this.chkDuplicateDO.push(false);
    }
    buttonDisplay(){
        this.addSaveButton = true;
        this.unsavedChange = true;
        sessionStorage.setItem('unsavedChanges','true');
    }
    addNewSD() {
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if(res = 'sessionActive'){
                this.supplyingDepot = JSON.parse(this.supplyingDepotClone);
                document.getElementById('err_newSupplyDepotNameDuplicate').innerHTML = "";
                document.getElementById('err_newSupplyDepotName').classList.remove('red');
                document.getElementById('div_newSupplyDepotName').classList.remove('error-border-red');
                document.getElementById('supplyDepotTable').classList.remove('supplyDepotTableReq');
                this.newBoxDisplay = true;        
                this.dropdownBox = false;
                this.addNewButton = false;
                this.prevSelectedCategory = this.selectedCategory;
                this.selectedCategory = "";
                this.disableSupplyDepotDD = true;
                this.newSD = true;
                document.getElementById('err_newSupplyDepotName').innerHTML="";
                document.getElementById('err_newSupplyDepotName').classList.remove('red');
                document.getElementById('div_newSupplyDepotName').classList.remove('error-border-red');
                sessionStorage.setItem('unsavedChanges','false');
                this.supplyingDhlDepotNameNew = "";
                this.addNewRowSD();
                this.showSupplyingDepotTable('');
                setTimeout(function(){ 
                    document.getElementById('newSupplyDepotName').focus();
                }, 1000);
            }
        }, reason =>{
            //console.log(reason);
        });
    }

    canNewSD(){
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if(res = 'sessionActive'){
                if(this.canDeactivate()){
                    this.supplyingDepot = JSON.parse(this.supplyingDepotClone);
                    this.newBoxDisplay = false;        
                    this.dropdownBox = true;
                    this.addNewButton = true;
                    this.viewTable = true;
                    this.editTable = false;
                    this.addSaveButton = false;
                    this.newSD = false;
                    this.unsavedChange = false;
                    this.disableSupplyDepotDD = false;
                    document.getElementById('supplyDepotTable').classList.remove('supplyDepotTableReq');
                    delete this._cfunction.errorMsgList['err_newSupplyDepotName'];
                    --this._cfunction.errorCount;
                    document.getElementById('err_newSupplyDepotName').innerHTML="";
                    document.getElementById('err_newSupplyDepotName').classList.remove('red');
                    document.getElementById('div_newSupplyDepotName').classList.remove('error-border-red');
                    sessionStorage.setItem('unsavedChanges','false');
                    this.supplyingDhlDepotNameNew = "";
                    this.showSupplyingDepotTable(this.prevSelectedCategory).then(res=>{
                        if(res){
                            
                        }
                    });
                }
            }
        }, reason =>{
            //console.log(reason);
        });
    }

    checkDuplicateSD(sd:string, id:number, errId:string, elementId:string = null){
        if(sd != ""){
            let that = this;
            let isDuplicate:boolean = false;
            this.supplyingDepot.forEach(function(itm:any, idx:number) {
                if(itm.supplyingDhlDepotName.toUpperCase() == sd.toUpperCase() && idx != id) {
                    isDuplicate = true;
                    that.hasDuplicateSD = true;
                }
            });
            this.chkDuplicateDO[id] = isDuplicate;
            this.hasDuplicateSD = this.checkAllForDuplicate();
            let divElement = document.getElementById(errId);
            let existingMsg = divElement.innerHTML;
            if(this.chkDuplicateDO[id]){
                divElement.innerHTML = "This Supplying Depot already exist.";
                document.getElementById('err_newSupplyDepotNameDuplicate').innerHTML = "This Supplying Depot already exist.";
                if(elementId != null){
                    document.getElementById(elementId).classList.add('error-border-red');
                }
            } else {
                divElement.innerHTML = "";
                if(elementId != null){
                    document.getElementById(elementId).classList.remove('error-border-red');
                }
                
                document.getElementById('err_newSupplyDepotNameDuplicate').innerHTML = "";
            }
            
        }
    }

    checkAllForDuplicate(){
        if(this.chkDuplicateDO.indexOf(true) != -1) {
            return true;
        } else {
            return false;
        }
    }

    saveSupplyDepot(){
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if(res = 'sessionActive'){
                document.getElementById('supplyDepotTable').classList.add('supplyDepotTableReq');
                let msgList = Object.keys(this._cfunction.errorMsgList);
                
                if(msgList.length == 0 && !this.hasDuplicateSD) {
                    // All clear
                    let that = this;
                    this.supplyingDepot.forEach(function(itm:any){
                        if(itm.supplyDepotId == 0){
                            itm.supplyingDhlDepotName = that.supplyingDhlDepotNameNew;
                        }
                    });

                    let finalJSON = {
                        "customerId" : this.customerName,
                        "maintainSupplyDepot" : this.supplyingDepot
                    }
                    //console.log(finalJSON);
                    this._postsService.saveSupplyingDepot(this.customerName, 'suppDepotDetails', '@all', finalJSON).subscribe(data => {
                        this.supplyingDepot = data.maintainSupplyDepot;
                        this.getSupplyingDepot();
                        
                        this.sortSupplyingDepot().then(res=>{
                            this.supplyingDepotClone = JSON.stringify(this.supplyingDepot);
                            let sdlength = this.supplyingDepot.length;
                            this.supplyingDhlDepotName = this.selectedCategory;
                            for(let i=0; i<sdlength; i++){
                                this.chkDuplicateDO.push(false);
                            }
                            sessionStorage.setItem("unsavedChanges", 'false');
                        });
                       
                        if(this.newBoxDisplay){
                            this.popupMsg = "Supply Depot added successfully.";
                            this.showSupplyingDepotTable(this.supplyingDepot[0].supplyingDhlDepotName);
                        } else {
                            this.popupMsg = "Supply Depot updated successfully.";
                        }
                        
                        this.newBoxDisplay = false;        
                        this.dropdownBox = true;
                        this.addNewButton = true;
                        this.viewTable = true;
                        this.editTable = false;
                        this.addSaveButton = false;
                        this.newSD = false;
                        this.disableSupplyDepotDD = false;

                        finalJSON.maintainSupplyDepot = [];
                        
                        let sessionModal = document.getElementById('messageModal');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('messageModal').style.display = "block";
                        this.unsavedChange = false;
                        sessionStorage.setItem('unsavedChanges','false');
                        this.onFirst(); //check with sruthi
                         // this.getSupplyingDepot();
                    });
                    
                } else {
                    // Error or duplicate
                    // There is some validation messages
                    this.popupMsg = "";
                    let that = this;
                    msgList.forEach(function(key: any){
                        that.popupMsg += that._cfunction.errorMsgList[key]+". ";
                    });
                    if(this.hasDuplicateSD){
                        this.popupMsg += " There is some duplicate values entered";
                    }
                    //this.popupMsg = "test";//this._cfunction.errorMsgList.join(", ");
                    let sessionModal = document.getElementById('messageModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('messageModal').style.display = "block";
                }
            }
        }, reason =>{
            //console.log(reason);
        });
    }

    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        reject('sessionInactive');
                        this.terminateFlg = true;
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        reject('sessionInactive')
                         sessionStorage.clear();
                        this._router.navigate(['']);
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        reject('sessionInactive');
                        this.terminateFlg = true;
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        resolve('sessionActive');
                        this.terminateFlg = false;
                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
                        this.organisationListChange = changeOrgAvailable;
                        this.orgimgUrl = orgImgUrl;
                        this.orgApiName = orgApiNameTemp;
                        // this.disabledCusChange = false;
                        if (this.organisationList.length == 1) {
                            this.disabledCusChange = true;
                        }
                    }
                }
                //alert(this.disabledfilfilment);
            });
        });
    }

    closePopup(modalBoxId="") {
        if(modalBoxId == 'popup_SessionExpired'){
            let sessionModal = document.getElementById('popup_SessionExpired');
            sessionModal.classList.remove('out');
            sessionModal.classList.add('in');
            sessionModal.style.display = 'none';
            let dt = this._cfunction.getUserSession();
            this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
                //console.log(userData);
            }, (error: any) => {
                //console.log(error);
            });
            sessionStorage.clear();
            this._router.navigate(['']);
        } else {
            let sessionModal = document.getElementById(modalBoxId);
            sessionModal.classList.remove('out');
            sessionModal.classList.add('in');
            sessionModal.style.display = 'none';
        }
    }
    
    getSupplyingDepot(){
        this._postsService.getsupplyingDepot(this.customerName, "suppDepotDetails", '@all').subscribe((supplyDepotDta: any) => {
            this.supplyingDepot = supplyDepotDta.maintainSupplyDepot;
        }, (err: any) => {
            //console.log(err);
        });
    }

    sortSupplyingDepot(){
        return new Promise((resolve) => {
            let tempArr = this.supplyingDepot.sort((a: any, b: any) => {
                if (a.supplyingDhlDepotName.toUpperCase() < b.supplyingDhlDepotName.toUpperCase()) {
                    return -1;
                } else if (a.supplyingDhlDepotName.toUpperCase() > b.supplyingDhlDepotName.toUpperCase()) {
                    return 1;
                } else {
                    return 0;
                }
            });
            this.supplyingDepot = tempArr;
            resolve(true);
        });
    }

    canDeactivate(): Promise<boolean> | boolean {
        if (!this.unsavedChange || sessionStorage.getItem('unsavedChanges')!=='true') {
            return true;
        }
        return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
    }

    validate(){
        document.getElementById('supplyDepotTable').classList.add('supplyDepotTableReq');
}

    exitScreen(){
        console.log(this.addNewButton);
        if(!this.addNewButton){
            this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
                if(res = 'sessionActive'){
                    if(this.canDeactivate()){
                        this.supplyingDepot = JSON.parse(this.supplyingDepotClone);
                        this.supplyingDhlDepotName = this.supplyingDepot[0].supplyingDhlDepotName;
                        for(let i=0; i<this.supplyingDepot.length; i++){
                            this.chkDuplicateDO.push(false);
                        }
                        this.showSupplyingDepotTable(this.supplyingDepot[0].supplyingDhlDepotName);
                        this._cfunction.errorMsgList = [];
                        this.hasDuplicateSD= false;
                        this.unsavedChange = false;
                         sessionStorage.setItem("unsavedChanges", 'false');
                        document.getElementById('supplyDepotTable').classList.remove('supplyDepotTableReq');
                        this.unsavedChange = false;
                        this.newBoxDisplay = false;        
                        this.dropdownBox = true;
                        this.addNewButton = true;
                        this.viewTable = true;
                        this.editTable = false;
                        this.addSaveButton = false;
                        this.newSD = false;
                        this.disableSupplyDepotDD = false;
                    }
                }
            }, reason =>{
                //console.log(reason);
            });
        } else {
            this._router.navigate(['/','cataloguemanagement',this.customerName]);
        }
        
    }

    // change organisation div show and hide
    toggleOrganisation() {
        this._globalFunc.logger("Toggle customer list.");
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
            border.classList.remove('searchborder');
        }
        else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
            border.classList.add('searchborder');
        }
    }

    changeOrgType(page: any, orgType: any) {
        this.getUserSession(orgType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res=>{
            if (res == 'sessionActive') {
                this.toggleOrganisation();
                this._router.navigate(['cataloguemanagement', orgType, 'maintainsupplydepot']);
            }
        });
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    printIt() {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res=>{
            if (res == 'sessionActive') {
                var printHeaderNav = document.getElementById('printHeader').innerHTML;
                var printContents = document.getElementById('exportable').innerHTML;
                if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
                    var popupWin = window.open('', '_blank');
                    popupWin.document.open();
                    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav +  printContents + '</body></html>');
                    popupWin.document.close();
                    setTimeout(function () { popupWin.close(); }, 1000);
                } else {
                    var popup = window.open('', '_blank');
                    popup.document.open();
                    popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContents + '</body></html>');
                    popup.document.close();
                }
            }
        }); 
    }

    exportIt() {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res=>{
            if (res == 'sessionActive') {
                this._globalFunc.logger("Export to excel");
                return this._globalFunc.exportData('exportable', this.customerName + 'maintainSupplyDepot');
            }
        });
        
    }

    modelChange(){
        this.addSaveButton = true;
        this.addNewButton = false;
        this.unsavedChange = true;
        sessionStorage.setItem('unsavedChanges','true');
    }
}