import { NgModule } from '@angular/core';
import { MaintainSupplyDepotComponentRoutes } from './maintainsupplydepot.routes';
import { MaintainSupplyDepotComponent } from './maintainsupplydepot.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';
import { SDCategorySortPipe } from './sortSD.pipes';
import { CatalogueManagePendingHeaderModule } from '../cataloguemanagependingheader/cataloguemanagependingheader.module';
import { SortNumericPipe } from './sortnumber.pipes';

@NgModule({
  imports: [
    MaintainSupplyDepotComponentRoutes,
    CommonModule,
    FormsModule,
    NavigationModule,
    CatalogueManagePendingHeaderModule
  ],
  exports: [],
  declarations: [MaintainSupplyDepotComponent, SDCategorySortPipe, SortNumericPipe],
  providers: [Services, GlobalComponent, commonfunction]
})
export class MaintainSupplyDepotModule { }
