import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot,RouterStateSnapshot, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

import { Services } from '../../../services/app.services';

@Injectable()
export class SupplyDepotCategoryDDResolve implements Resolve<any> {

    constructor(private _getService: Services, private router: Router) { }
    
    resolve(route: ActivatedRouteSnapshot, state:RouterStateSnapshot): Observable<any> {
        let orgType = route.params['orgType'];
        console.log("Resolve supply depot Category");
        return this._getService.getsupplyingDepot(orgType, "categoryDropDown", '@all')
        .map((data:any) => {
            if (data) {
                return data;
            }
        })
        .catch((error:any) => {
            console.log(error);
            //if(error.errorCode == '404.33.1')
            //this.router.navigate(['/products']);
            return Observable.of(error.errorMessage);
        });
    }
}