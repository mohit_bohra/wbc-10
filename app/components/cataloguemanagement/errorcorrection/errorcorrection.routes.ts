import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorCorrectionComponent } from './errorcorrection.component';

// route Configuration
const errorCorrectionRoutes: Routes = [
  { path: '', component: ErrorCorrectionComponent },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(errorCorrectionRoutes)],
  exports: [RouterModule]
})
export class ErrorCorrectionRoutes { }
