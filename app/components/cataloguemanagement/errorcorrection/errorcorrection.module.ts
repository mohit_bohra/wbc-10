import { NgModule } from '@angular/core';
import { ErrorCorrectionRoutes } from './errorcorrection.routes';
import { ErrorCorrectionComponent } from './errorcorrection.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/primeng';
import { TooltipModule } from "ngx-tooltip";
import { CatalogueManagePendingHeaderModule } from '../cataloguemanagependingheader/cataloguemanagependingheader.module';

// import { DatepickerModule } from 'ng2-bootstrap';
import { DatepickerModule } from '../../datepicker/ng2-bootstrap';
@NgModule({
  imports: [
    ErrorCorrectionRoutes,
    CommonModule,
    FormsModule,
    DatepickerModule.forRoot(),
    CalendarModule,
    TooltipModule,
    CatalogueManagePendingHeaderModule

  ],
  exports: [],
  declarations: [ErrorCorrectionComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class ErrorCorrectionModule { }
