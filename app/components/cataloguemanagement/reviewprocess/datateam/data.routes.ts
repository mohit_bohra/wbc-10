import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataComponent } from './data.component';

// route Configuration
const dataRoutes: Routes = [
  { path: '', component: DataComponent },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(dataRoutes)],
  exports: [RouterModule]
})
export class DataComponentRoutes { }
