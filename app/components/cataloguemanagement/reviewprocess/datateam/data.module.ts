import { NgModule } from '@angular/core';
import { DataComponentRoutes } from './data.routes';
import { DataComponent } from './data.component';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../../global/global.component';
import { Services } from '../../../../services/app.services';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from "ngx-tooltip";
import { CatalogueManagePendingHeaderModule } from '../../cataloguemanagependingheader/cataloguemanagependingheader.module';


@NgModule({
  imports: [
    DataComponentRoutes,
    CommonModule,
    FormsModule,
    InfiniteScrollModule,
    TooltipModule,
    CatalogueManagePendingHeaderModule


  ],
  exports: [],
  declarations: [DataComponent],
  providers: [Services, GlobalComponent]
})
export class DataModule { }
