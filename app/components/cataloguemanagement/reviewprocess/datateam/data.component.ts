import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { GlobalComponent } from '../../../global/global.component';
import { Constant } from '../../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'data.component.html',
    providers: [commonfunction]
})
export class DataComponent {

    objConstant = new Constant();
    whoImgUrl: any;

    productid: string;
    errorMsg: boolean;
    errorMsgString: string;
    showLoader: boolean;
    printHeader: boolean;
    organisationList: any;

    pageState: number = 10;
    customerName: string;
    orgimgUrl: string;
    orgApiName: string;
    productDetails: any;
    selectOption: string;
    productName: any;
    mapLen: any;
    description: any;
    homeUrl: any;
    private _sub: any;
    availabilityMaster: any;
    WorkflowMaster: any;
    catalogueTypeMaster: any;
    availabilityStatus: string;
    workflowStatusName: string;
    mapTypesMaster: any;
    mapTypesMasterDisplay: any;
    lookupTypesMaster: any;
    lookupTypesMasterDisplay: any;
    financialTypesMaster: any;
    financialTypesMasterDisplay: any;
    financialCurrencies: any;
    catalogueType: string;
    imageUrl: string;
    productMaps: any;
    productPrices: any;
    productmin: string;
    maptypeValue: string;
    isProductIdEntered: boolean;

    showSuccessModel: boolean;
    minDate: any;
    addedProdId: any;
    addedMin: any;
    identifierRow: number = 1;
    pricingRow: number = 1;
    attributeRow: number = 1;

    prodIdentifierRowHTML: any;

    todayDateStr: string;

    hideOrganisation: boolean = true;
    tempCalId: any;
    calDefaultDate: Date;

    allowWrite: boolean;

    requestType: any;
    disabledOrder: boolean = false;
    disabledCatalogue: boolean = false;

    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    channels: string;
    requesttypes: string;
    teamname: string;
    requestid: string;
    detailid: string;
    datateamData: any;
    selectedPendingRequest: any;
    selectedIndex: number;
    errorRowData: number[];
    errorSummary: any;
    errorRowStatus: any;
    datateamData4PrintNExport: any[];
    show4Single: boolean;

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction) {


        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;

        // set default date null to calender
        this.errorMsgString = '';
        this.errorMsg = false;

        location.onPopState(() => {

            console.log('pressed back!');
            document.getElementById('close-popup').click();

        });
    }

    // function works on page load

    ngOnInit() {

        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {

            this.showLoader = true;
            document.getElementById('dataContainer').style.display = 'none';
            this.printHeader = true;
            this.show4Single = true;

            this.availabilityStatus = '';
            this.workflowStatusName = '';
            this.catalogueType = '';
            this.productid = '';
            this.productName = '';
            this.productmin = '';
            this.description = '';
            this.imageUrl = this.whoImgUrl;
            this.isProductIdEntered = false;
            this.allowWrite = false;
            this.requestType = 'Select';
            this.selectOption = '';
            this.tempCalId = '';
            this.channels = "default";
            this.requesttypes = "@all";
            this.teamname = "datateam";
            this.requestid = "@all";
            this.detailid = "@all";
            this.selectedIndex = 0;
            this.selectedPendingRequest = "";
            this.datateamData = "";
            this.errorSummary = "";
            //this.calDefaultDate = "30/07/2017";

            // window scroll function
            this._globalFunc.autoscroll();
            this.maptypeValue = '';

            this.minDate = new Date();
            /*this.minDate.setMonth(prevMonth);
            this.minDate.setFullYear(prevYear);*/
            this.todayDateStr = this._cfunction.todayDateStrFunction();


            // set page state variable
            this.pageState = 10;
            this.identifierRow = 1;
            this.pricingRow = 1;
            this.attributeRow = 1;
            sessionStorage.setItem('pageState', '10');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            // subscribe to route params
            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                let reports;
                let orders;
                let catalogues;
                let cataloguesRaise;
                let accessMgmt;
                let redirct;
                let changeOrgAvailable = new Array;
                let catManagement;
                // list of org assigned to user
                this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));

                this.organisationList.forEach(function (item: any) {
                    if (item.orgType !== cusType) {
                        changeOrgAvailable.push(item);
                    }
                });

                document.getElementById('orderArrowSpan').style.display = 'block';
                document.getElementById('orderMob').style.display = 'block';
                document.getElementById('catalogueMob').style.display = 'block';
                document.getElementById('catalogueArrowSpan').style.display = 'none';
                document.getElementById('catalogueRaiseMob').style.display = 'none';
                document.getElementById('reportsMob').style.display = 'block';
                document.getElementById('permissonsMob').style.display = 'block';

                if (document.getElementById('orderArrow').classList.contains('glyphicon-menu-up') === true) {
                    let sublinks = document.getElementById('orderArrow');
                    sublinks.classList.remove('glyphicon-menu-up');
                    sublinks.classList.add('glyphicon-menu-down');
                    document.getElementById('orderRaiseMob').style.display = 'none';
                }

                if (document.getElementById('catalogueArrow').classList.contains('glyphicon-menu-up') === true) {
                    let sublinks = document.getElementById('catalogueArrow');
                    sublinks.classList.remove('glyphicon-menu-up');
                    sublinks.classList.add('glyphicon-menu-down');
                    document.getElementById('catalogueRaiseMob').style.display = 'none';
                }


                this.organisationList.forEach(function (item: any) {
                    if (item.orgType === cusType) {
                        // logic for permission set
                        orgImgUrl = item.imgUrl;
                        orgApiNameTemp = item.orgName;

                        if ((item.permissions.menuPermission.orders).toLowerCase() == 'none') {
                            orders = true;
                            document.getElementById('orderMob').style.display = 'none';
                            document.getElementById('orderRaiseMob').style.display = 'none';
                        }



                        if ((item.permissions.menuPermission.catalogueManagement).toLowerCase() == 'none') {
                            catManagement = true;
                        } else {
                            catManagement = false;
                        }


                        if ((item.permissions.menuPermission.catalogueEnquiry).toLowerCase() == 'none') {

                            catalogues = true;
                            document.getElementById('catalogueMob').style.display = 'none';
                            document.getElementById('catalogueRaiseMob').style.display = 'none';
                            //redirct = 1;
                            if (sessionStorage.getItem('page_redirect') == '0') {
                                sessionStorage.removeItem('page_redirect');
                            } else {
                                sessionStorage.setItem('page_redirect', '1');
                            }


                        } else {
                            // write logic to show add new product

                            //document.getElementById('catalogueRaiseMob').style.display = 'block';
                        }

                        if ((item.permissions.catalogAdminPermission.addNewProducts).toLowerCase() == 'none') {
                            cataloguesRaise = true;
                            document.getElementById('catalogueArrowSpan').style.display = 'none';
                            document.getElementById('catalogueRaiseMob').style.display = 'none';


                        } else {
                            // write logic to show add new product

                            //document.getElementById('catalogueRaiseMob').style.display = 'block';
                        }

                        if ((item.permissions.menuPermission.reports).toLowerCase() == 'none') {
                            reports = true;
                            document.getElementById('reportsMob').style.display = 'none';

                        }

                        if ((item.permissions.menuPermission.accessMgmt).toLowerCase() == 'none') {
                            accessMgmt = true;
                            document.getElementById('permissonsMob').style.display = 'none';
                        }

                    }
                });

                if (redirct == 1) {
                    redirct = 0;
                    this.redirect('/reports', this.customerName);
                }

                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                this.disabledReports = reports;
                this.disabledOrder = orders;
                this.disabledCatalogue = catalogues;
                this.disabledCatalogueRaise = cataloguesRaise;
                this.disabledPermission = accessMgmt;
                this.disabledManageCatalogue = catManagement;
                this.getTeamwisePendingRequest();
                /*setTimeout(function () {
                    
                }, 4000);*/
                //console.log("sel 1");
                //console.log(this.datateamData[this.selectedIndex]);
            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);


            // logic for resize pop up 
            window.onresize = () => {

                if (screen == "desktop") {
                    /*   document.getElementById('prouctDetailTitle').style.display = 'block';
                       document.getElementById('productDetailHeader').style.display = 'block';
                       document.getElementById('productDetailImg').style.display = 'block';*/
                }

                screen = this._globalFunc.getDevice(window.innerWidth);

            };

        }
    }

    // get teamwise pending request
    getTeamwisePendingRequest() {
/* 
        let errorSummary1: any[];
        let errorRowStatus1: any[]; */
        this._postsService.getTeamwisePendingRequest(this.orgApiName, this.channels, this.requesttypes, this.teamname, this.requestid, this.detailid).subscribe(data => {

            console.log(data);
            //console.log(data.requestHeader);
            document.getElementById('dataContainer').style.display = 'block';
            this.datateamData = data.requestHeaders[0].requestDetails;
            this.datateamData4PrintNExport = this.datateamData;
            //this.datateamData = data.requestHeader.requestDetails[0].dataTeam;
            //this.selectedPendingRequest = this.datateamData[0];
            this.errorSummary = data.errorSummary;
            this.showLoader = false;
            //console.log(errorRowStatus1);
        }, err => {
            this.showLoader = false;
            this._router.navigate(['/error404']);
        });
        //setTimeout(function () {}, 4000);
    }

    dataTeamPopup(idx: number) {
        console.log("idx = " + idx);
        this.selectedIndex = idx;
        this.datateamData4PrintNExport = [];
        this.datateamData4PrintNExport[0] = this.datateamData[idx];
        console.log(this.datateamData4PrintNExport);
        console.log("length = " + this.datateamData4PrintNExport.length);
        //this.selectedPendingRequest = this.datateamData[idx];
        //console.log(this.datateamData[this.selectedIndex]);
    }

    // navigate function
    redirect(page: any, orgType: any) {
        this._router.navigate([page, orgType]);
    }

    refresh() {
        //window.location.reload();
        this.ngOnInit();
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak

    }

    // key press event to disable special charater
    keyPress(event: any) {
        this._globalFunc.alphaNumeric(event);
    }
    keyPressWithSpace(event: any) {
        this._globalFunc.alphaNumericWithSpace(event);
    }

    keyPressNumericWithDecimal(event: any) {
        this._globalFunc.numericWithDecimal(event);
    }

    // print functionality
    printOrder() {
        console.log(this.datateamData);
        //this.datateamData4PrintNExport = this.datateamData;
        this.show4Single = true;
        /*setTimeout(() => {
                    
        }, 2000); */
        //this.printHeader = false;
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        var printHeaderNav1 = document.getElementById('printHeader1').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav1 + printHeaderNav + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav1 + printHeaderNav + '</body></html>');
            popup.document.close();
        }
        //this.printHeader = true;
    }

    // download functionality
    exportData() {
        return this._cfunction.exportData('exportable',this.customerName+'datateamreview');
    }

    // print functionality
    printSingle() {
        //this.datateamData4PrintNExport = this.datateamData;
        /*setTimeout(() => {
                    
        }, 2000); */
        //this.printHeader = false;
        this.show4Single = false;
        var printHeaderNav = document.getElementById('printHeaderSingle').innerHTML;
        var printHeaderNav1 = document.getElementById('printHeader1').innerHTML;
        var printHeaderNav2 = document.getElementById('printHeader2').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav1 + printHeaderNav2 + printHeaderNav + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav1 + printHeaderNav2 + printHeaderNav + '</body></html>');
            popup.document.close();
        }
        //this.printHeader = true;
    }

    // download functionality
    exportSingle() {
        return this._cfunction.exportData('exportableSingle',this.customerName+'dataTeamReview');
    }

    getActivateCheckbox(index: any, value: any, eleName: string) {
        this._cfunction.getActivateCheckbox(index, value, eleName);
    }
}