import { Component, ViewChild } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { GlobalComponent } from '../../../global/global.component';
import { Constant } from '../../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DialogService } from '../../../../services/dialog.service';

// not used old code
@Component({
    templateUrl: 'fullrangereview.component.html',
    providers: [commonfunction]
})


export class fullrangereviewComponent {
    @ViewChild('supplychain') supplychain:any
    objConstant = new Constant();
    whoImgUrl: any;
    chkboxclick: boolean;

    productid: string;
    errorMsg: boolean;
    errorMsgString: string;
    showLoader: boolean;
    noDataFound: boolean;
    printHeader: boolean;
    organisationList: any;

    pageState: number = 23;
    customerName: string;
    orgimgUrl: string;
    orgApiName: string;
    productDetails: any;
    selectOption: string;
    productName: any;
    mapLen: any;
    description: any;
    homeUrl: any;
    private _sub: any;
    availabilityMaster: any;
    WorkflowMaster: any;
    catalogueTypeMaster: any;
    availabilityStatus: string;
    workflowStatusName: string;
    mapTypesMaster: any;
    mapTypesMasterDisplay: any;
    lookupTypesMaster: any;
    lookupTypesMasterDisplay: any;
    financialTypesMaster: any;
    financialTypesMasterDisplay: any;
    financialCurrencies: any;
    catalogueType: string;
    imageUrl: string;
    productMaps: any;
    productPrices: any;
    productmin: string;
    maptypeValue: string;
    isProductIdEntered: boolean;

    showSuccessModel: boolean;
    minDate: any;
    addedProdId: any;
    addedMin: any;
    identifierRow: number = 1;
    pricingRow: number = 1;
    attributeRow: number = 1;

    prodIdentifierRowHTML: any;

    todayDateStr: string;

    hideOrganisation: boolean = true;
    tempCalId: any;
    calDefaultDate: Date;

    allowWrite: boolean;

    requestType: any;
    disabledOrder: boolean = false;
    disabledCatalogue: boolean = false;

    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    channels: string;
    requesttypes: string;
    teamname: string;
    requestid: string;
    detailid: string;
    supplychainData: any;
    errorSummary: any;
    selectStatus: any;
    start: number;
    limit: number;
    count: number;
    hitNext: boolean;
    supplychainDatatLength: any;
    supplyChainDataPrint: any;

    supplychainDataAll: any[];
    errorSummaryAll: any[];
    selectStatusPNNodes: any;
    selectedAll: boolean;
    selectedAllArr: boolean[];

    /*rbac session related declared variables start*/
    assignedPermissions: any = [];
    noPermissionMsg: string = "";
    hasAccess: boolean = false;
    disableViewFullRangeExports: boolean = true;
    disablefullRangeExportsSupplyChainReview: boolean = true;
    organisationListChange: any;
    permissionArray: any = [];
    imgName: string = "";
    unsavedChange: boolean = false;

    /*rbac session related declared variables end*/

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, private formBuilder: FormBuilder, public dialogService: DialogService) {


        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;

        // set default date null to calender
        this.errorMsgString = '';
        this.errorMsg = false;
        this.chkboxclick = true;
        this.limit = 15;
        this.start = 0;
        this.selectStatusPNNodes = [
            { status: 'Yes', value: 'available' },
            { status: 'No', value: 'not available' },
            { status: 'Some', value: 'available to some nodes' }];
        this.selectStatus = [
            { status: 'Yes', value: 'available' },
            { status: 'No', value: 'not available' }];
        this.noDataFound = false;
        this.selectedAll = false;
        this.selectedAllArr = [];
        this.supplychain = this.formBuilder.group({});
    }

    canDeactivate(): Promise<boolean> | boolean {
        if (!this.unsavedChange) {
            return true;
        }
        return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
    }

    // function works on page load

    ngOnInit() {

        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {

            this.showLoader = false;
            this.printHeader = true;
            this.availabilityStatus = '';
            this.workflowStatusName = '';
            this.catalogueType = '';
            this.productid = '';
            this.productName = '';
            this.productmin = '';
            this.description = '';
            this.imageUrl = this.whoImgUrl;
            this.isProductIdEntered = false;
            this.allowWrite = false;
            this.requestType = 'Select';
            this.selectOption = '';
            this.tempCalId = '';

            this.channels = "@all";
            this.requesttypes = "@all";
            this.teamname = "@all";
            this.requestid = "@all";
            this.detailid = "@all";
            this.errorSummary = "";
            //this.calDefaultDate = "30/07/2017";

            // window scroll function
            this._globalFunc.autoscroll();
            this.maptypeValue = '';
            this.hitNext = false;

            this.minDate = new Date();
            /*this.minDate.setMonth(prevMonth);
            this.minDate.setFullYear(prevYear);*/
            this.todayDateStr = this._cfunction.todayDateStrFunction();

            this.supplychainDataAll = [];
            this.errorSummaryAll = [];


            // set page state variable
            this.pageState = 23;
            this.identifierRow = 1;
            this.pricingRow = 1;
            this.attributeRow = 1;
            sessionStorage.setItem('pageState', '23');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            // subscribe to route params
            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                let reports;
                let orders;
                let catalogues;
                let cataloguesRaise;
                let accessMgmt;
                let redirct;
                let changeOrgAvailable = new Array;
                let catManagement;


                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
                this.imgName = this.getCustomerImage(params.orgType);


                // this.showLoader = true;
                this.getsupplychainreivewdata();
                // this.getSupplyChainReviewDataPrint();

                // this.showLoader = false;
                //  this.getAllCatalogues();

            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);


            // logic for resize pop up 
            window.onresize = () => {

                if (screen == "desktop") {
                    /*   document.getElementById('prouctDetailTitle').style.display = 'block';
                       document.getElementById('productDetailHeader').style.display = 'block';
                       document.getElementById('productDetailImg').style.display = 'block';*/
                }

                screen = this._globalFunc.getDevice(window.innerWidth);

            };

        }
    }

    getValue(val: any) {
        var result = "Yes";
        if (val == false || val == 'N') {
            result = "No";
        }
        return result;
    }

    getActivateCheckbox(index: any, value: any, eleName: string) {
        this._cfunction.getActivateCheckbox(index, value, eleName);
        if (this.supplychainData[index]['status'] && this.supplychainData[index]['status'] == "accept") {
            this.supplychainData[index]['status'] = "notaccept";
        } else {
            this.supplychainData[index]['status'] = "accept";
        }
        console.log(this.supplychainData[index]);
        console.log(value);
    }

    onScrollDown() {
        this.start = this.start + this.limit;
        /* if(this.start >= this.count && this.count > 0){
             this.start = this.count-1;
         }*/
        if (this.count > this.start) {
            // this.showLoader = true;
            this._postsService.getsupplychainreivewdata(this.customerName, this.channels, this.requesttypes, this.requestid, this.detailid, this.start).subscribe(data => {
                //// this.showLoader = false;
                if (Object.keys(data).length === 0) {
                    // alert('adf');
                    this.hitNext = false;
                } else {
                    this.count = data.paginationMetaData.count;
                    console.log(data.requestHeaders[0].requestDetails);

                    let tempLength = this.supplychainData.length; // For select all functionality

                    this.supplychainData = this.supplychainData.concat(data.requestHeaders[0].requestDetails);

                    // For select all functionality
                    for (let i = tempLength; i < this.supplychainData.length; i++) {
                        this.selectedAllArr[i] = this.selectedAll;
                    }
                    // For select all functionality - END

                    if (data.paginationMetaData.count - data.paginationMetaData.offset == 1) {
                        this.hitNext = false;
                        this.count = -1;
                    }
                }
            }, error => {
                this.showLoader = false;
                if (error.errorCode === '404.33.402' || error.httpResponseCode === 404) {
                    //alert('nodata');
                    this.hitNext = false;
                } else {
                    // alert("404");
                    this._router.navigate(['/error404']);
                }
            });
        }
    }


    // get teamwise pending request
    getsupplychainreivewdata() {
        console.log("123");
        this.showLoader = true;
        this._postsService.getsupplychainreivewdata(this.customerName, this.channels, this.requesttypes, this.requestid, this.detailid, this.start).subscribe(data => {
            //this.start = 0;
            this.count = data.paginationMetaData.count;
            if (data == undefined || data == null) {
                this.supplychainDatatLength = 0;
                this.supplychainData = [];
                this.hitNext = false;
            } else {
                this.hitNext = true;
                this.supplychainDatatLength = data.requestHeaders[0].requestDetails.length;
                //console.log(data.requestHeaders[0].requestDetails.length);
                //console.log("22 : this.supplychainDatatLength : " + this.supplychainDatatLength);
                this.supplychainData = data.requestHeaders[0].requestDetails;
                //console.log(data.requestHeaders);

                //this.count = data.paginationMetaData.count;

                // For select all functionality
                for (let i = 0; i < this.supplychainData.length; i++) {
                    this.selectedAllArr[i] = this.selectedAll;
                }
                // For select all functionality - END
            }
            this.errorSummary = data.errorSummary;
            this.showLoader = false;
        }, error => {
            this.showLoader = false;
            if (error.errorCode === '404.33.402' || error.httpResponseCode === 404) {
                //alert('nodata');
                this.noDataFound = true;
            } else {
                //alert("404");
                this._router.navigate(['/error404']);
            }
        });
    }

    // This method fetches the supplyviewdata from services for print purpose only ( in this case, all the records are returned)
    getSupplyChainReviewDataPrint() {
        console.log('printdata');
        this._postsService.getSupplyChainReviewDataPrint(this.customerName, this.channels, this.requesttypes, this.requestid, this.detailid, this.start).subscribe(data => {

            if (data == undefined) {
                //this.supplychainDatatLength = 0;
                this.supplyChainDataPrint = [];
            } else {
                this.supplyChainDataPrint = data.requestHeaders[0].requestDetails;
            }
        }, error => {
            console.log(error.errorCode);
            this.showLoader = false;
            if (error.errorCode === '404.33.402' || error.httpResponseCode === 404) {
                //alert('nodata');
                this.noDataFound = true;
            } else {
                // alert("404");
                this._router.navigate(['/error404']);
            }
        });
    }

    updateSupplychain(value: any) {
        this.unsavedChange = false;
        sessionStorage.setItem('unsavedChanges','false');
        var tempArr: any = [];
        this.supplychainData.forEach(function (item: any) {
            if (item.status && item.status == 'accept')
                //item.pop('supplyChainTeam');
                tempArr.push(item);
        });

        if (tempArr.length > 0) {
            console.log("TEmp Arr");
            console.log(tempArr);
            var prodString: string = "";
            prodString = '{"requestHeaders": [{ "customer": "amazon", "requestDetails": ' + JSON.stringify(tempArr) + '} ] }';
            console.log("Final String : " + prodString);
            this.showLoader = true;
            this.start = 0;
            this.selectedAll = false;
            this._postsService.updateSupplyChainReviewData(this.customerName, prodString).subscribe(data => {
                this.showLoader = false;
                //alert("Update Done");
                // this.showLoader = true;
                this.getsupplychainreivewdata();
                // this.getSupplyChainReviewDataPrint();
                // this.showLoader = false;
                let sessionModal = document.getElementById('fullrangeUpdateSuccess');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('fullrangeUpdateSuccess').style.display = 'block';
            }
                , err => {
                    // this.showLoader = false;
                    this._router.navigate(['/error404']);
                }
            );
        } else {
            let sessionModal = document.getElementById('fullrangeError');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('fullrangeError').style.display = 'block';
        }

    }

    // navigate function
    redirect(page: any, orgType: any) {
        this._router.navigate([page, orgType]);
    }

    refresh() {
        //window.location.reload();
        this.ngOnInit();
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
    }

    toggleOrganisation() {
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            //  document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
            border.classList.remove('searchborder');
        } else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            // document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
            border.classList.add('searchborder');

        }
    }
    // key press event to disable special charater
    keyPress(event: any) {
        this._globalFunc.alphaNumeric(event);
    }
    keyPressWithSpace(event: any) {
        this._globalFunc.alphaNumericWithSpace(event);
    }

    keyPressNumericWithDecimal(event: any) {
        this._globalFunc.numericWithDecimal(event);
    }

    // print functionality
    printOrder() {
        //this.printHeader = false;
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        var tableData = document.getElementById('exportable').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + tableData + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + tableData + '</body></html>');
            popup.document.close();
        }
        //this.printHeader = true;
    }

    // download functionality
    exportData() {
        return this._cfunction.exportData('exportable', this.customerName + 'SupplyTReview');
    }

    closefullrangeUpdateSuccess() {
        // this.showLoader = false;
        let sessionModal = document.getElementById('fullrangeUpdateSuccess');
        sessionModal.classList.add('in');
        sessionModal.classList.remove('out');
        document.getElementById('fullrangeUpdateSuccess').style.display = 'none';
    }

    closefullrangeUpdateError() {
        // this.showLoader = false;
        let sessionModal = document.getElementById('fullrangeError');
        sessionModal.classList.add('in');
        sessionModal.classList.remove('out');
        document.getElementById('fullrangeError').style.display = 'none';
    }

    selectAll() {
        // For select all functionality
        for (let i = 0; i < this.selectedAllArr.length; i++) {
            this.selectedAllArr[i] = true;
            this.supplychainData[i]['status'] = "accept";
        }
        this.selectedAll = true;
        this.unsavedChange = true;
        sessionStorage.setItem('unsavedChanges','true'); 
        // For select all functionality - END
    }

    UnselectAll() {
        // For select all functionality
        for (let i = 0; i < this.selectedAllArr.length; i++) {
            this.selectedAllArr[i] = false;
            this.supplychainData[i]['status'] = "notaccept";
        }
        this.selectedAll = false;
        this.unsavedChange = true;
        sessionStorage.setItem('unsavedChanges','true');
        // For select all functionality - END
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    /*rbac implementation - get user session function call start*/
    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
            if (!userData.isSessionActive) { // false
                // User session has expired.
                sessionStorage.clear();
                this._router.navigate(['']);
            } else {
                this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                let changeOrgAvailable: any = [];
                let orgImgUrl = "";
                let orgApiNameTemp = "";
                this.organisationList.forEach(function (item: any) {
                    if (item.customerName !== customerName) {
                        changeOrgAvailable.push(item);
                    } else {
                        orgImgUrl = item.customerName + ".png";
                        orgApiNameTemp = item.customerName;
                    }
                });
                this.organisationListChange = changeOrgAvailable;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                // this.disabledCusChange = false;
                if (this.organisationList.length == 1) {
                    this.disabledCusChange = true;
                }

                console.log("this.organisationList234");
                console.log(this.organisationList);
                let that = this;
                let catalogueEnquiry = true;
                let editCatalogueEntry = true;
                let addCatalogueEntry = true;
                let addCatalogueAttribute = true;
                let catalogueBulkUploadView = true;
                let catalogueBulkUploadSubmit = true;
                let catalogueBulkExportView = true;
                let storeBulkExportView = true;
                let catalogueBulkExportSubmit = true;
                let storeBulkExportSubmit = true;
                let maintainAllocationPriorities = true;
                let storeBulkUploadView = true;
                let maintainSupplyingDepots = true;
                let storeEnquiry = true;
                let addStoreNewEntry = true;
                let ViewFullRangeExports = true;
                let fullRangeExportsSupplyChainReview = true;
                let addRequests = true;
                let manageRequests = true;
                let requestsErrorCorrection = true;
                let requestsDataTeamReview = true;
                let requestsSupplyChainReview = true;
                let requestsCommercialReview = true;
                let repricingConfiguration = true;
                let repricingSupplierRequestsReview = true;
                let repricingPeriodicReview = true;
                let repricingProductsIdentification = true;
                let catalogueManagementMenuPermission = false;
                let permArray = [];
                let permArrayMainEstateRange: any = [];

                this.organisationList.forEach(function (organisation: any) {
                    if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                        console.log("organisation : " + that.customerName);
                        console.log(organisation);
                        organisation.permissionRoleList.forEach(function (permRoleList: any) {
                            //console.log("11");
                            //console.log(permRoleList);
                            permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                //console.log("22");
                                that.setPermissionsArray(permRoleDtlList.permissionGroup);
                                if (permRoleDtlList.permissionGroup == 'catalogueManagement' || permRoleDtlList.permissionGroup == 'morrisonsPermissions') {
                                    permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                        if (permList.permissionName == 'catalogueManagement' && permList.permissionStatus == 'active') {
                                            catalogueManagementMenuPermission = true;
                                        }
                                        console.log(permList);
                                        switch (permList.permissionName) {
                                            case "viewFullRangeExports":
                                                //console.log("55");
                                                ViewFullRangeExports = false;
                                                permArrayMainEstateRange.push({ "ViewFullRangeExports": true });
                                                //alert(filfilment);
                                                break;
                                            case "fullRangeExportsSupplyChainReview":
                                                //console.log("55");
                                                fullRangeExportsSupplyChainReview = false;
                                                permArrayMainEstateRange.push({ "fullRangeExportsSupplyChainReview": true });
                                                //alert(filfilment);
                                                break;
                                        }
                                    });
                                }
                            });
                        });
                    }
                });
                that = null;
                if (!catalogueManagementMenuPermission) {
                    let loggedInUserGroup = sessionStorage.getItem('loggedInUserGroupName');
                    let loggedInUserAdminFlag: string = sessionStorage.getItem('loggedInUserAdminFlag');
                    let superAdminGroup = this.objConstant.SUPERADMIN_GROUP;
                    console.log(loggedInUserGroup + " : " + superAdminGroup + " : " + loggedInUserAdminFlag);
                    console.log((loggedInUserGroup == superAdminGroup));
                    console.log((loggedInUserGroup != superAdminGroup && loggedInUserAdminFlag));
                    console.log((loggedInUserGroup == superAdminGroup) || (loggedInUserGroup != superAdminGroup && loggedInUserAdminFlag));
                    if ((loggedInUserGroup == superAdminGroup)) {
                        console.log("aa");
                        this._router.navigate(["groups", customerName]);
                    } else if (loggedInUserGroup != superAdminGroup && loggedInUserAdminFlag == "true") {
                        console.log("ff");
                        this._router.navigate(["groups", customerName]);
                    } else {
                        if (this.assignedPermissions.length) {
                            console.log("cc");
                            this._router.navigate(["orders", customerName]);
                        } else {
                            console.log("dd");
                            this.noPermissionMsg = "you do not have any access permissions defined for " + customerName;
                            this.hasAccess = true;
                        }
                    }
                    // redirect to order
                }

                this.disableViewFullRangeExports = ViewFullRangeExports;
                this.disablefullRangeExportsSupplyChainReview = fullRangeExportsSupplyChainReview;

                this.hasAccess = false;

                if (permArrayMainEstateRange.length > 0) {
                    this.permissionArray.permArrayMainEstateRange = true;
                    this.hasAccess = true;
                } else {
                    this.permissionArray.permArrayMainEstateRange = false;
                }
            }
            console.log("this.permissionArray");
            console.log(this.permissionArray);
            console.log(this.permissionArray.length);
            //alert(this.disabledfilfilment);
        }, (error: any) => {
            if (error.errorCode == '503.65.001') {
                this._router.navigate(['/error404']);
            }
        });
    }


    setPermissionsArray(permissionGroup: string) {
        this.assignedPermissions.push(permissionGroup);
    }

    onChange(){
        this.unsavedChange = true;
        sessionStorage.setItem('unsavedChanges','true');
    }
}