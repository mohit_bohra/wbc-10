import { NgModule } from '@angular/core';
import { fullrangereviewComponentRoutes } from './fullrangereview.routes';
import { fullrangereviewComponent } from './fullrangereview.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../../global/global.component';
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/primeng';
import { TooltipModule } from "ngx-tooltip";
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { CatalogueManagePendingHeaderModule } from '../../cataloguemanagependingheader/cataloguemanagependingheader.module';


// import { DatepickerModule } from 'ng2-bootstrap';
import { DatepickerModule } from '../../../datepicker/ng2-bootstrap';
@NgModule({
  imports: [
    fullrangereviewComponentRoutes,
    CommonModule,
    FormsModule,
    DatepickerModule.forRoot(),
    CalendarModule,
    TooltipModule,
    CatalogueManagePendingHeaderModule,
    InfiniteScrollModule

  ],
  exports: [],
  declarations: [fullrangereviewComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class fullrangereviewModule { }
