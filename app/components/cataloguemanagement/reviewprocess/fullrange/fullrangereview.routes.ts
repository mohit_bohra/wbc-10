import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { fullrangereviewComponent } from './fullrangereview.component';
import { CanDeactivateGuard } from '../../../../services/can-deactivate-guard.service';

// route Configuration
const fullrangereviewRoutes: Routes = [
  { path: '', component: fullrangereviewComponent, canDeactivate: [CanDeactivateGuard] },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(fullrangereviewRoutes)],
  exports: [RouterModule]
})
export class fullrangereviewComponentRoutes { }
