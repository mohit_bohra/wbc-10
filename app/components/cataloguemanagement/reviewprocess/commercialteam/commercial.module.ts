import { NgModule } from '@angular/core';
import { CommercialComponentRoutes } from './commercial.routes';
import { CommercialComponent } from './commercial.component';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../../global/global.component';
import { Services } from '../../../../services/app.services';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from "ngx-tooltip";
import { CatalogueManagePendingHeaderModule } from '../../cataloguemanagependingheader/cataloguemanagependingheader.module';

@NgModule({
  imports: [
    CommercialComponentRoutes,
    CommonModule,
    FormsModule,
    InfiniteScrollModule,
    TooltipModule,
    CatalogueManagePendingHeaderModule

  ],
  exports: [],
  declarations: [CommercialComponent],
  providers: [Services, GlobalComponent]
})
export class CommercialModule { }
