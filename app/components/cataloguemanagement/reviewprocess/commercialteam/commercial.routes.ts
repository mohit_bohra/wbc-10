import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommercialComponent } from './commercial.component';

// route Configuration
const commercialRoutes: Routes = [
  { path: '', component: CommercialComponent },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(commercialRoutes)],
  exports: [RouterModule]
})
export class CommercialComponentRoutes { }
