import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SupplychainComponent }  from './supplychain.component';

// route Configuration
 const supplychainRoutes: Routes = [
  { path: '', component: SupplychainComponent  },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(supplychainRoutes)],
  exports: [RouterModule]
})
export class SupplychainComponentRoutes { }
