import { NgModule } from '@angular/core';
import { SupplychainComponentRoutes } from './supplychain.routes';
import { SupplychainComponent } from './supplychain.component';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../../global/global.component';
import { Services } from '../../../../services/app.services';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from "ngx-tooltip";
import { CatalogueManagePendingHeaderModule } from '../../cataloguemanagependingheader/cataloguemanagependingheader.module';

@NgModule({
  imports: [
    SupplychainComponentRoutes,
    CommonModule,
    FormsModule,
    InfiniteScrollModule,
    TooltipModule,
    CatalogueManagePendingHeaderModule


  ],
  exports: [],
  declarations: [SupplychainComponent],
  providers: [Services, GlobalComponent]
})
export class SupplychainModule { }
