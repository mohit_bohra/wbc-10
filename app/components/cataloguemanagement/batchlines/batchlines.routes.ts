import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BatchLinesComponent } from './batchlines.component';

// route Configuration
const batchLinesRoutes: Routes = [
  { path: '', component: BatchLinesComponent },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(batchLinesRoutes)],
  exports: [RouterModule]
})
export class BatchLinesRoutes { }
