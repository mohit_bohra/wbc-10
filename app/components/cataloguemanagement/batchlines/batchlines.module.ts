import { NgModule } from '@angular/core';
import { BatchLinesRoutes } from './batchlines.routes';
import { BatchLinesComponent } from './batchlines.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/primeng';
import { TooltipModule } from "ngx-tooltip";
import { CatalogueManagePendingHeaderModule } from '../cataloguemanagependingheader/cataloguemanagependingheader.module';

// import { DatepickerModule } from 'ng2-bootstrap';
import { DatepickerModule } from '../../datepicker/ng2-bootstrap';
@NgModule({
  imports: [
    BatchLinesRoutes,
    CommonModule,
    FormsModule,
    DatepickerModule.forRoot(),
    CalendarModule,
    TooltipModule,
    CatalogueManagePendingHeaderModule

  ],
  exports: [],
  declarations: [BatchLinesComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class BatchLinesModule { }
