import { NgModule } from '@angular/core';
import { fulRangeExportComponentRoutes } from './fulrangeexport.routes';
import { fulRangeExportComponent } from './fulrangeexport.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../global/global.component';
import { Services } from '../../services/app.services';
import { commonfunction } from '../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/primeng';
import { CatalogueManagePendingHeaderModule } from '../cataloguemanagependingheader/cataloguemanagependingheader.module';


// import { DatepickerModule } from 'ng2-bootstrap';
import { DatepickerModule } from '../datepicker/ng2-bootstrap';
@NgModule({
  imports: [
    fulRangeExportComponentRoutes,
    CommonModule,
    FormsModule,
    DatepickerModule.forRoot(),
    CalendarModule,
    CatalogueManagePendingHeaderModule

  ],
  exports: [],
  declarations: [fulRangeExportComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class fulrangeExportModule { }
