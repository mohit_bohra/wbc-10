import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../services/app.services';
import { commonfunction } from '../../services/commonfunction.services';
import { GlobalComponent } from '../global/global.component';
import { Constant } from '../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'cataloguemanagement.component.html',
    providers: [commonfunction]
})
export class CatalogueManagementComponent {

    objConstant = new Constant();
    whoImgUrl: any;

    productid: string;
    errorMsg: boolean;
    errorMsgString: string;
    showLoader: boolean;
    printHeader: boolean;
    organisationList: any;

    pageState: number = 9;
    customerName: string;
    orgimgUrl: string;
    orgApiName: string;
    productDetails: any;

    // repricingStatus: boolean;
    // configurationstatus: boolean;

    productName: any;
    mapLen: any;
    description: any;
    homeUrl: any;
    availabilityMaster: any;
    WorkflowMaster: any;
    catalogueTypeMaster: any;
    availabilityStatus: string;
    workflowStatusName: string;
    mapTypesMaster: any;
    mapTypesMasterDisplay: any;
    lookupTypesMaster: any;
    lookupTypesMasterDisplay: any;
    financialTypesMaster: any;
    financialTypesMasterDisplay: any;
    financialCurrencies: any;
    catalogueType: string;
    imageUrl: string;
    productMaps: any;
    productPrices: any;
    productmin: string;
    maptypeValue: string;
    isProductIdEntered: boolean;
    organisationListChange: any;
    showSuccessModel: boolean;
    minDate: any;
    addedProdId: any;
    addedMin: any;
    identifierRow: number = 1;
    pricingRow: number = 1;
    attributeRow: number = 1;

    prodIdentifierRowHTML: any;

    todayDateStr: string;

    hideOrganisation: boolean = true;
    tempCalId: any;
    calDefaultDate: Date;

    allowWrite: boolean;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disabledOrder: boolean;
    disabledOrderRaise: boolean;
    disableduploadCatalogue: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    headerHtml: any;
    disablePriceConfig: boolean = false;
    disableRePricing: boolean = false;
    disableprodRePricing: boolean = false;

    imgName: string = "";
    disablecatalogueEnquiry: boolean = true;
    disableeditCatalogueEntry: boolean = true;
    disableaddCatalogueEntry: boolean = true;
    disableaddCatalogueAttribute: boolean = true;
    disablecatalogueBulkUploadView: boolean = true;
    disablecatalogueBulkUploadSubmit: boolean = true;
    disablecatalogueBulkExportView: boolean = true;
    disablestoreBulkExportView: boolean = true;
    disablecatalogueBulkExportSubmit: boolean = true;
    disablestoreBulkExportSubmit: boolean = true;
    disablemaintainAllocationPriorities: boolean = true;
    disablestoreBulkUploadView: boolean = true;
    disablemaintainSupplyingDepots: boolean = true;
    disablestoreEnquiry: boolean = true;
    disableaddStoreNewEntry: boolean = true;
    disableViewFullRangeExports: boolean = true;
    disablefullRangeExportsSupplyChainReview: boolean = true;
    disableaddRequests: boolean = true;
    disablemanageRequests: boolean = true;
    disablerequestsErrorCorrection: boolean = true;
    disablerequestsDataTeamReview: boolean = true;
    disablerequestsSupplyChainReview: boolean = true;
    disablerequestsCommercialReview: boolean = true;
    disablerepricingConfiguration: boolean = true;
    disablerepricingSupplierRequestsReview: boolean = true;
    disablerepricingPeriodicReview: boolean = true;
    disablerepricingProductsIdentification: boolean = true;
    disablemanageDelistDates: boolean = true;
    disableUploadRangeFileView: boolean = true;
    disableUploadRangeFileSubmit: boolean = true;
    disableUploadPromotionsView:boolean = true;
    disableUploadPromotionFileSubmit :boolean = true;
    disableRAFs: boolean = true;
    permissionArray: any = [];
    hasAccess: boolean = true;
    assignedPermissions: any = [];
    noPermissionMsg: string = "";

    moduleName: string;
    terminateFlg: boolean = false;

    siteMaintanceFlg: boolean;
    siteMaintanceFlg1: boolean;
    siteMSg: any;
    businessMessageDetailsMsg: string;
    featureDetailsMsg: string;
    deploymentDetailsMsg: string;
     deploymentDetailsMsglength:any;
    featureDetailsMsglength:any;
    businessMessageDetailsMsglength:any;


    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction) {


        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.noPermissionMsg = '';
        // set default date null to calender
        this.errorMsgString = '';
        this.errorMsg = false;
        this.hasAccess = false;
    }

    // function works on page load

    ngOnInit() {

        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {

            // user permission

            // this.disabledPermission = false;
            // this.disabledReports = false;
            // this.disabledCatalogue = false;
            // this.disabledOrder = false;

            // this.disabledCatalogueRaise = false;
            // this.disabledManageCatalogue = false;

            // this.repricingStatus = false;
            // this.configurationstatus = false;



            // window scroll function
            this._globalFunc.autoscroll();

            // set header basket data
            this.pageState = 9;
            this.moduleName = "catalogueManagement";
            console.log("Module Name", this.moduleName);
            sessionStorage.setItem('pageState', '9');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            // subscribe to route params
            this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                let reports;
                let orders;
                let uploadCatalogue;
                let catalogues;
                let cataloguesRaise;
                let accessMgmt;
                let redirct;
                let changeOrgAvailable = new Array;
                let catManagement;
                let npr1: boolean = true;
                let npr2: boolean = true;
                let npr3: boolean = true;
                let npr4: boolean = true;

                let ppc1: boolean = true;
                let ppc2: boolean = true;
                let ppc3: boolean = true;
                let ppc4: boolean = true;

                let csreport1: boolean = true;
                let csreport2: boolean = true;
                let csreport3: boolean = true;

                let mfre1: boolean = true;
                let mfre2: boolean = true;

                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
                    if (res == 'sessionActive') {
                        this.imgName = this.getCustomerImage(params.orgType);
                        console.log("Module Name::", this.moduleName);
                        this.getConfigService(this.moduleName);
                    }
                }, reason => {
                    console.log(reason);
                });

                sessionStorage.setItem("unsaveChagne", 'false');

                // list of org assigned to user
                // this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));

                // this.organisationList.forEach(function (item: any) {
                //     if (item.orgType !== cusType) {
                //         changeOrgAvailable.push(item);
                //     }
                // });
                // this.organisationListChange = changeOrgAvailable;
                // // this.disabledCusChange = false;
                // if (this.organisationList.length == 1) {
                //     this.disabledCusChange = true;
                // }

                // document.getElementById('orderArrowSpan').style.display = 'block';
                // document.getElementById('orderMob').style.display = 'block';
                // document.getElementById('orderRaiseMob').style.display = 'none';

                // document.getElementById('catalogueMob').style.display = 'block';
                // document.getElementById('catalogueArrowSpan').style.display = 'none';
                // document.getElementById('catalogueRaiseMob').style.display = 'none';
                // document.getElementById('reportsMob').style.display = 'block';
                // document.getElementById('permissonsMob').style.display = 'block';
                // document.getElementById('noNprAccess').style.display = 'none';

                // if (document.getElementById('orderArrow').classList.contains('glyphicon-menu-up') === true) {
                //     let sublinks = document.getElementById('orderArrow');
                //     sublinks.classList.remove('glyphicon-menu-up');
                //     sublinks.classList.add('glyphicon-menu-down');
                //     document.getElementById('orderRaiseMob').style.display = 'none';
                // }

                // if (document.getElementById('catalogueArrow').classList.contains('glyphicon-menu-up') === true) {
                //     let sublinks = document.getElementById('catalogueArrow');
                //     sublinks.classList.remove('glyphicon-menu-up');
                //     sublinks.classList.add('glyphicon-menu-down');
                //     document.getElementById('catalogueRaiseMob').style.display = 'none';
                // }

                redirct = 0;
                let that = this;
                // this.organisationList.forEach(function (item: any) {
                //     if (item.orgType === cusType) {

                //         // logic for permission set
                //         orgImgUrl = item.imgUrl;
                //         orgApiNameTemp = item.orgName;
                //         if ((item.permissions.menuPermission.orders).toLowerCase() == 'none') {
                //             orders = true;
                //             document.getElementById('orderMob').style.display = 'none';
                //             document.getElementById('orderRaiseMob').style.display = 'none';
                //         }

                //         if ((item.permissions.catalogAdminPermission.uploadCatalogue).toLowerCase() == 'none') {
                //             uploadCatalogue = true;
                //         } else {
                //             uploadCatalogue = false;
                //         }

                //         if ((item.permissions.menuPermission.raiseNewOrder).toLowerCase() == 'none') {
                //             document.getElementById('orderArrowSpan').style.display = 'none';
                //             document.getElementById('orderRaiseMob').style.display = 'none';

                //         }

                //         if (item.permissions.menuPermission.catalogueManagement == 'W') {
                //             catManagement = true;
                //         } else {
                //             catManagement = false;
                //             redirct = 1;
                //         }


                //         if ((item.permissions.menuPermission.catalogueEnquiry).toLowerCase() == 'none') {

                //             catalogues = true;
                //             document.getElementById('catalogueMob').style.display = 'none';
                //             document.getElementById('catalogueRaiseMob').style.display = 'none';
                //             //redirct = 1;
                //             /* if (sessionStorage.getItem('page_redirect') == '0') {
                //                  sessionStorage.removeItem('page_redirect');
                //              } else {
                //                  sessionStorage.setItem('page_redirect', '1');
                //              }*/


                //         } else {
                //             // write logic to show add new product

                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.catalogAdminPermission.addNewProducts).toLowerCase() == 'none') {
                //             cataloguesRaise = true;
                //             document.getElementById('catalogueArrowSpan').style.display = 'none';
                //             document.getElementById('catalogueRaiseMob').style.display = 'none';


                //         } else {
                //             // write logic to show add new product

                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.menuPermission.reports).toLowerCase() == 'none') {
                //             reports = true;
                //             document.getElementById('reportsMob').style.display = 'none';

                //         }

                //         if ((item.permissions.menuPermission.accessMgmt).toLowerCase() == 'none') {
                //             accessMgmt = true;
                //             document.getElementById('permissonsMob').style.display = 'none';
                //         }
                //         // ground level permission
                //         if ((item.permissions.catalogManagementPermission.newProductRequestPermission.createSingleRequest).toLowerCase() == 'none') {
                //             document.getElementById('csr').style.display = 'none';
                //             npr1 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.catalogManagementPermission.newProductRequestPermission.manageRequest).toLowerCase() == 'none') {
                //             document.getElementById('mr').style.display = 'none';
                //             npr2 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.catalogManagementPermission.newProductRequestPermission.viewNISFeederExport).toLowerCase() == 'none') {
                //             document.getElementById('nsfe').style.display = 'none';
                //             npr3 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }
                //         if ((item.permissions.catalogManagementPermission.newProductRequestPermission.viewAddToChannelStatusReport).toLowerCase() == 'none') {
                //             document.getElementById('vasr').style.display = 'none';
                //             npr4 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }
                //         if (!npr1 && !npr2 && !npr3 && !npr4) {
                //             document.getElementById('npr').style.display = 'none';
                //         }
                //         // product price check section
                //         if ((item.permissions.catalogManagementPermission.priceIncreaseReviewPermission.productsIdentification).toLowerCase() == 'none') {
                //             document.getElementById('vlor').style.display = 'none';
                //             ppc1 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.catalogManagementPermission.priceIncreaseReviewPermission.pricingConfiguration).toLowerCase() == 'none') {
                //             document.getElementById('priceconfig').style.display = 'none';
                //             ppc2 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //             //console.log("prici Config");
                //             that.getPricingStatus();
                //         }

                //         if ((item.permissions.catalogManagementPermission.priceIncreaseReviewPermission.productsRepricing).toLowerCase() == 'none') {
                //             document.getElementById('reprice').style.display = 'none';
                //             ppc3 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.catalogManagementPermission.priceIncreaseReviewPermission.productsRepricing).toLowerCase() == 'none') {
                //             document.getElementById('prodicreprice').style.display = 'none';
                //             ppc4 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         /* if ((item.permissions.catalogManagementPermission.priceIncreaseReviewPermission.generateNewPeriodPriceCheckReport).toLowerCase() == 'none') {
                //             document.getElementById('gnr').style.display = 'none';
                //             ppc2 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         } */

                //         /* if ((item.permissions.catalogManagementPermission.periodicPriceCheckPermission.manageExceptions).toLowerCase() == 'none') {
                //             document.getElementById('me').style.display = 'none';
                //             ppc3 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('me').style.display = 'block';
                //         } */

                //         if (!ppc1 && !ppc2 && !ppc3 && !ppc4) {
                //             document.getElementById('ppc').style.display = 'none';
                //         }

                //         // changes summary report section
                //         if ((item.permissions.catalogManagementPermission.changeSummaryReportPermission.viewChangeSummaryReports).toLowerCase() == 'none') {
                //             document.getElementById('vloreports').style.display = 'none';
                //             csreport1 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('vloreports').style.display = 'block';
                //         }

                //         if ((item.permissions.catalogManagementPermission.changeSummaryReportPermission.generateNewChangeSummaryReport).toLowerCase() == 'none') {
                //             document.getElementById('gnreports').style.display = 'none';
                //             csreport2 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('gnreports').style.display = 'block';
                //         }

                //         if ((item.permissions.catalogManagementPermission.changeSummaryReportPermission.manageDelistDates).toLowerCase() == 'none') {
                //             document.getElementById('mdd').style.display = 'none';
                //             csreport3 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('mdd').style.display = 'block';
                //         }


                //         if (!csreport1 && !csreport2 && !csreport3) {
                //             document.getElementById('csreport').style.display = 'none';
                //         }

                //         // Morrisons Full Range - Exports
                //         if ((item.permissions.catalogManagementPermission.fullRangeExportPermission.viewFullRangeExports).toLowerCase() == 'none') {
                //             document.getElementById('vloe').style.display = 'none';
                //             mfre1 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.catalogManagementPermission.fullRangeExportPermission.generateNewFullRangeExport).toLowerCase() == 'none') {
                //             document.getElementById('gne').style.display = 'none';
                //             mfre2 = false;
                //         } else {
                //             // write logic to show add new product
                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }
                //         if (!mfre1 && !mfre2) {
                //             document.getElementById('mfre').style.display = 'none';
                //         }

                //         if (!mfre1 && !mfre2 && !csreport1 && !csreport2 && !csreport3 && !ppc1 && !ppc2 && !ppc3 && !ppc4 && !npr1 && !npr2 && !npr3 && !npr4) {
                //             document.getElementById('noNprAccess').style.display = 'block';
                //         }


                //     }
                // });

                // if (redirct == 1) {
                //     redirct = 0;
                //     //console.log("inside condition");
                //     this.redirect('/reports', this.customerName);
                // }

                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                // this.disabledReports = reports;
                // this.disabledOrder = orders;
                // this.disabledOrderRaise = orders;
                // this.disableduploadCatalogue = uploadCatalogue;
                // this.disabledCatalogue = catalogues;
                // this.disabledCatalogueRaise = cataloguesRaise;
                // this.disabledPermission = accessMgmt;
                // this.disabledManageCatalogue = catManagement;
                //  this.getAllCatalogues();
                //this.getAllMasterData();
                // if(this.orgApiName == "amazon"){
                //     this.repricingStatus = false;
                //     this.configurationstatus = false;
                // }else if(this.orgApiName == "mccolls"){
                //    // this.getPricingStatus();
                // }

            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);

            // logic for resize pop up 
            window.onresize = () => {
                if (screen == "desktop") {

                    if (window.innerWidth < this._globalFunc.stdWidth) {
                        // this.catalogueResult = false;

                    } else {
                        //  this.catalogueResult = true;
                    }
                } else {

                    if (window.innerWidth > this._globalFunc.stdWidth) {
                        //   this.catalogueResult = true;
                        // document.getElementById("resultModalClose").click();
                    } else {
                        //  this.catalogueResult = false;

                    }
                }

                // get device to populate some part of ui
                screen = this._globalFunc.getDevice(window.innerWidth);

            };
            // window.onscroll = () => {
            //     this.scrollFunction();
            // };

        }


        setTimeout(() => {
            this.showLoader = false;
        }, 50000);

        //console.log("inside nginit function");

        //this.siteMaintanceFlg1 = sessionStorage.getItem('site');
        // if(sessionStorage.getItem('site') == "true"){
            
        //     this.siteMaintanceFlg1 = false;
        // }else { 
        //     this.siteMSg = "Site is under maintenance";
        //     this.siteMaintanceFlg1 = true;
        // }

    }


    // onScrolltop() {
    //     this._globalFunc.autoscroll();
    // }

    getPricingStatus() {
        //console.log("Inside getPricingStatus Functi");
        //console.log("outsite ");

        // Check for Configuration link
        this._postsService.getPricingStatus(this.customerName, "").subscribe(data => {
            //console.log("first ");
            // this.repricingStatus = data.productRepricingStatus;
            // this.configurationstatus = data.pricingConfigStatus;
            //console.log(Object.getOwnPropertyNames(data).length);
            if (Object.getOwnPropertyNames(data).length == 0) {
                this.disablePriceConfig = true;
                this.disableRePricing = true;
                this.disableprodRePricing = true;
            } else if (data.count == 0) {
                this.disablePriceConfig = false;
                // Check for Repricing link
                this._postsService.getPricingStatus(this.customerName, "Y").subscribe(data => {
                    //console.log("2nd");
                    if (Object.getOwnPropertyNames(data).length == 0) {
                        this.disableRePricing = true;
                    } else if (data.count == 0) {
                        this.disableRePricing = false;
                    } else {
                        this.disableRePricing = true;
                        //     this.disableprodRePricing = true;
                    }
                });
                // Check for Periodic Repricing link
                this._postsService.getPricingStatus(this.customerName, "Y", "", "periodic_repricing_status").subscribe(data => {
                    //console.log("3rd");
                    if (Object.getOwnPropertyNames(data).length == 0) {
                        this.disableprodRePricing = true;
                    } else if (data.count == 0) {
                        this.disableprodRePricing = false;
                    } else {
                        this.disableprodRePricing = true;
                    }
                });
            } else {
                this.disablePriceConfig = true;
                this.disableRePricing = true;
                this.disableprodRePricing = true;
            }
        });
    }

    //
    ////console.log(document.body.scrollTop);
    ////console.log(document.documentElement.scrollTop);
    /* if (document.body.scrollTop > 800 || document.documentElement.scrollTop > 800) {
         document.getElementById("topBtn").style.display = "block";
     } else {
         document.getElementById("topBtn").style.display = "none";
     } */
    //}

    redirectWithStatus(page: any, orgType: any, status: boolean) {
        if (!status) {
            this.redirect(page, orgType);
        }
    }

    // navigate function
    redirect(page: any, orgType: any) {
        //console.log("inside redirect function");
        if (page == '/fulrangeexport') {
            this._postsService.generateFulRangeExport().subscribe(data => {
                this._router.navigate([page, orgType]);
            });
        } else if (page == '/managerequest') {
            this._router.navigate(['cataloguemanagement', orgType, 'npr', 'manage']);
        } else if (page == '/singleproductrequest') {
            this._router.navigate(['cataloguemanagement', orgType, 'npr', 'singleproductrequest']);
        } else if (page == '/nisfeederexport') {
            this._router.navigate(['cataloguemanagement', orgType, 'nisfeederexport']);
        } else if (page == '/fullrangereview') {
            this._router.navigate(['cataloguemanagement', orgType, 'fullrangereview']);
        } else if (page == '/productpricing') {
            this._router.navigate(['cataloguemanagement', orgType, 'productpricing']);
        } else if (page == '/repricing') {
            this._router.navigate(['cataloguemanagement', orgType, 'productpricing', 'repricing']);
        } else if (page == '/prodicrepricing') {
            this._router.navigate(['cataloguemanagement', orgType, 'productpricing', 'prodicrepricing']);
        } else if (page == '/configuration') {
            this._router.navigate(['cataloguemanagement', orgType, 'productpricing', 'configuration']);
        } else if (page == '/maintainsupplydepot') {
            this._router.navigate(['cataloguemanagement', orgType, 'maintainsupplydepot']);
        } else if (page == '/maintainallocationpriority') {
            this._router.navigate(['cataloguemanagement', orgType, 'maintainallocationpriority']);
        }
        else if (page == '/addstore') {
            this._router.navigate(['cataloguemanagement', orgType, 'addstore']);
            
        }
        else {
            //console.log("inside redirect function else");
            this._router.navigate([page, orgType]);
        }
    }

    redirectTo(redirectArray: any, newTab: boolean = false) {
        if (newTab) {
            let url = this.homeUrl + redirectArray.join("/"); //page + '/' + encodeURIComponent(orgType) + '/' + orderno;
            sessionStorage.setItem("pageState", "999");
           window.open(url);
        } else {
            this._router.navigate(redirectArray);
        }
    }

    refresh() {
        //window.location.reload();
        this.ngOnInit();
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
    }

    toggleOrganisation() {
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            //  document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
            border.classList.remove('searchborder');
        } else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            // document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
            border.classList.add('searchborder');

        }
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.
                   if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else{
                        reject('sessionInactive')
                         sessionStorage.clear();
                        this._router.navigate(['']);
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        resolve('sessionActive');
                        this.terminateFlg = false;
                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
                        this.organisationListChange = changeOrgAvailable;
                        this.orgimgUrl = orgImgUrl;
                        this.orgApiName = orgApiNameTemp;
                        // this.disabledCusChange = false;
                        if (this.organisationList.length == 1) {
                            this.disabledCusChange = true;
                        }

                        console.log("this.organisationList234");
                        console.log(this.organisationList);
                        let that = this;
                        let catalogueEnquiry = true;
                        let editCatalogueEntry = true;
                        let addCatalogueEntry = true;
                        let addCatalogueAttribute = true;
                        let catalogueBulkUploadView = true;
                        let catalogueBulkUploadSubmit = true;
                        let catalogueBulkExportView = true;
                        let storeBulkExportView = true;
                        let catalogueBulkExportSubmit = true;
                        let storeBulkExportSubmit = true;
                        let maintainAllocationPriorities = true;
                        let storeBulkUploadView = true;
                        let maintainSupplyingDepots = true;
                        let storeEnquiry = true;
                        let addStoreNewEntry = true;
                        let ViewFullRangeExports = true;
                        let fullRangeExportsSupplyChainReview = true;
                        let addRequests = true;
                        let manageRequests = true;
                        let requestsErrorCorrection = true;
                        let requestsDataTeamReview = true;
                        let requestsSupplyChainReview = true;
                        let requestsCommercialReview = true;
                        let repricingConfiguration = true;
                        let repricingSupplierRequestsReview = true;
                        let repricingPeriodicReview = true;
                        let repricingProductsIdentification = true;
                        let catalogueManagementMenuPermission = false;
                        let permArray = [];
                        let permArrayWholesaleCatalogues: any = [];
                        let preArrayWholesaleStores: any = []
                        let permArrayNewRequests: any = [];
                        let permArrayPricing: any = [];
                        let permArrayMainEstateRange: any = [];
                        let uploadRangeFileView = true;
                        let uploadRangeFileSubmit = true;
                        let uploadDealPromotions = true;
                        let uploadPromotionFileSubmit = true;

                        this.organisationList.forEach(function (organisation: any) {
                            if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                                console.log("organisation : " + that.customerName);
                                console.log(organisation);
                                organisation.permissionRoleList.forEach(function (permRoleList: any) {
                                    //console.log("11");
                                    //console.log(permRoleList);
                                    permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                        //console.log("22");
                                        that.setPermissionsArray(permRoleDtlList.permissionGroup);
                                        if (permRoleDtlList.permissionGroup == 'catalogueManagement' || permRoleDtlList.permissionGroup == 'morrisonsPermissions') {
                                            permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                                if (permList.permissionName == 'catalogueManagement' && permList.permissionStatus == 'active') {
                                                    catalogueManagementMenuPermission = true;
                                                }
                                                console.log('Permission List',permList);
                                                switch (permList.permissionName) {
                                                    case "catalogueEnquiry":
                                                        //console.log("55");
                                                        catalogueEnquiry = false;
                                                        console.log("catalogueEnquiry : " + catalogueEnquiry);
                                                        permArrayWholesaleCatalogues.push({ "catalogueEnquiry": true });
                                                        //alert(filfilment);
                                                        break;
                                                    case "editCatalogueEntry":
                                                        //console.log("55");
                                                        editCatalogueEntry = false;
                                                        //alert(filfilment);
                                                        break;
                                                    case "addCatalogueEntry":
                                                        //console.log("55");
                                                        addCatalogueEntry = false;
                                                        permArrayWholesaleCatalogues.push({ "addCatalogueEntry": true });
                                                        //alert(filfilment);
                                                        break;
                                                    case "addCatalogueAttribute":
                                                        //console.log("55");
                                                        addCatalogueAttribute = false;
                                                        //alert(filfilment);
                                                        break;
                                                    case "catalogueBulkUploadView":
                                                        //console.log("55");
                                                        catalogueBulkUploadView = false;
                                                        permArrayWholesaleCatalogues.push({ "catalogueBulkUploadView": true });
                                                        //alert(filfilment);
                                                        break;
                                                    case "catalogueBulkUploadSubmit":
                                                        //console.log("55");
                                                        catalogueBulkUploadSubmit = false;
                                                        //alert(filfilment);
                                                        break;
                                                    case "catalogueBulkExportView":
                                                        //console.log("55");
                                                        catalogueBulkExportView = false;
                                                        permArrayWholesaleCatalogues.push({ "catalogueBulkExportView": true });
                                                        //alert(filfilment);
                                                        break;
                                                    case "storeBulkExportView":
                                                        storeBulkExportView = false;
                                                        preArrayWholesaleStores.push({ "storeBulkExportView": true });
                                                        break;
                                                    case "storeEnquiry":
                                                        storeEnquiry = false;
                                                        console.log("storeEnquiry : " + storeEnquiry);
                                                        preArrayWholesaleStores.push({ "storeEnquiry": true });
                                                        break;
                                                    case "addStoreNewEntry":
                                                        addStoreNewEntry = false;
                                                        console.log("addStoreNewEntry:" + addStoreNewEntry);
                                                        preArrayWholesaleStores.push({ "addStoreNewEntry": true });
                                                        break;
                                                    case "catalogueBulkExportSubmit":
                                                        //console.log("55");
                                                        catalogueBulkExportSubmit = false;
                                                        //alert(filfilment);
                                                        break;
                                                    case "viewFullRangeExports":
                                                        //console.log("55");
                                                        ViewFullRangeExports = false;
                                                        permArrayMainEstateRange.push({ "ViewFullRangeExports": true });
                                                        //alert(filfilment);
                                                        break;
                                                    case "fullRangeExportsSupplyChainReview":
                                                        //console.log("55");
                                                        fullRangeExportsSupplyChainReview = false;
                                                        permArrayMainEstateRange.push({ "fullRangeExportsSupplyChainReview": true });
                                                        //alert(filfilment);
                                                        break;
                                                    case "addRequests":
                                                        //console.log("55");
                                                        addRequests = false;
                                                        permArrayNewRequests.push({ "addRequests": true });
                                                        //alert(filfilment);
                                                        break;
                                                    case "manageRequests":
                                                        //console.log("55");
                                                        manageRequests = false;
                                                        permArrayNewRequests.push({ "manageRequests": true });
                                                        //alert(filfilment);
                                                        break;
                                                    case "requestsErrorCorrection":
                                                        //console.log("55");
                                                        requestsErrorCorrection = false;
                                                        //alert(filfilment);
                                                        break;
                                                    case "requestsDataTeamReview":
                                                        //console.log("55");
                                                        requestsDataTeamReview = false;
                                                        //alert(filfilment);
                                                        break;
                                                    case "requestsSupplyChainReview":
                                                        //console.log("55");
                                                        requestsSupplyChainReview = false;
                                                        //alert(filfilment);
                                                        break;
                                                    case "requestsCommercialReview":
                                                        //console.log("55");
                                                        requestsCommercialReview = false;
                                                        //alert(filfilment);
                                                        break;
                                                    case "pricingConfiguration":
                                                        //console.log("55");
                                                        repricingConfiguration = false;
                                                        permArrayPricing.push({ "repricingConfiguration": true });
                                                        //alert(filfilment);
                                                        that.getPricingStatus();
                                                        break;
                                                    case "repricingSupplierRequestsReview":
                                                        //console.log("55");
                                                        repricingSupplierRequestsReview = false;
                                                        permArrayPricing.push({ "repricingSupplierRequestsReview": true });
                                                        //alert(filfilment);
                                                        break;
                                                    case "repricingPeriodicReview":
                                                        //console.log("55");
                                                        repricingPeriodicReview = false;
                                                        permArrayPricing.push({ "repricingPeriodicReview": true });
                                                        //alert(filfilment);
                                                        break;
                                                    case "productIdentification":
                                                        //console.log("55");
                                                        repricingProductsIdentification = false;
                                                        permArrayPricing.push({ "repricingProductsIdentification": true });
                                                        //alert(filfilment);
                                                        break;
                                                    case "maintainAllocationPriorities":
                                                        maintainAllocationPriorities = false;
                                                        preArrayWholesaleStores.push({ "maintainAllocationPriorities": true });
                                                        break;
                                                    case "storeBulkUploadView":
                                                        storeBulkUploadView = false;
                                                        preArrayWholesaleStores.push({ "storeBulkUploadView": true });
                                                        break;
                                                    case "maintainSupplyingDepots":
                                                        maintainSupplyingDepots = false;
                                                        preArrayWholesaleStores.push({ "maintainSupplyingDepots": true });
                                                        break;
                                                    case "storeBulkExportSubmit":
                                                        //console.log("55");
                                                        storeBulkExportSubmit = false;
                                                        //alert(filfilment);
                                                        break;
                                                    case "uploadRangeFileView":
                                                        //console.log("55");
                                                        uploadRangeFileView = false;
                                                        permArrayWholesaleCatalogues.push({ "uploadRangeFileView": true });
                                                        //alert(filfilment);
                                                        break;
                                                    case "uploadRangeFileSubmit":
                                                        //console.log("55");
                                                        uploadRangeFileSubmit = false;
                                                        //alert(filfilment);
                                                        break;
                                                    case "uploadDealPromotions" :
                                                          uploadDealPromotions = false; 
                                                         permArrayWholesaleCatalogues.push({ "uploadDealPromotions": true });

                                                          break; 
                                                     case "uploadPromotionFileSubmit":
                                                        //console.log("55");
                                                        uploadPromotionFileSubmit = false;
                                                        //alert(filfilment);
                                                        break;
                                                          
                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                        that = null;
                        if (!catalogueManagementMenuPermission) {
                            let loggedInUserGroup = sessionStorage.getItem('loggedInUserGroupName');
                            let loggedInUserAdminFlag: string = sessionStorage.getItem('loggedInUserAdminFlag');
                            let superAdminGroup = this.objConstant.SUPERADMIN_GROUP;
                            console.log(loggedInUserGroup + " : " + superAdminGroup + " : " + loggedInUserAdminFlag);
                            console.log((loggedInUserGroup == superAdminGroup));
                            console.log((loggedInUserGroup != superAdminGroup && loggedInUserAdminFlag));
                            console.log((loggedInUserGroup == superAdminGroup) || (loggedInUserGroup != superAdminGroup && loggedInUserAdminFlag));
                            if ((loggedInUserGroup == superAdminGroup)) {
                                console.log("aa");
                                this._router.navigate(["groups", customerName]);
                            } else if (loggedInUserGroup != superAdminGroup && loggedInUserAdminFlag == "true") {
                                console.log("ff");
                                this._router.navigate(["groups", customerName]);
                            } else {
                                if (this.assignedPermissions.length) {
                                    console.log("cc");
                                    this._router.navigate(["orders", customerName]);
                                } else {
                                    console.log("dd");
                                    this.noPermissionMsg = "You do not have any access permissions defined for " + customerName;
                                    this.hasAccess = true;
                                }
                            }
                            // redirect to order
                        }
                        this.disablecatalogueEnquiry = catalogueEnquiry;
                        console.log("disablecatalogueEnquiry : " + this.disablecatalogueEnquiry + " : " + catalogueEnquiry);
                        this.disablestoreEnquiry = storeEnquiry;
                        console.log("disablestoreEnquiry : " + this.disablestoreEnquiry + " : " + storeEnquiry);
                        this.disableaddStoreNewEntry = addStoreNewEntry;
                        this.disableeditCatalogueEntry = editCatalogueEntry;
                        this.disableaddCatalogueEntry = addCatalogueEntry;
                        this.disableaddCatalogueAttribute = addCatalogueAttribute;
                        this.disablecatalogueBulkUploadView = catalogueBulkUploadView;
                        this.disablecatalogueBulkUploadSubmit = catalogueBulkUploadSubmit;
                        this.disablecatalogueBulkExportView = catalogueBulkExportView;
                        this.disablestoreBulkExportView = storeBulkExportView;
                        console.log("disablestoreBulkExportView : " + this.disablestoreBulkExportView + " : " + storeBulkExportView);
                        this.disablecatalogueBulkExportSubmit = catalogueBulkExportSubmit;
                        this.disablestoreBulkExportSubmit = storeBulkExportSubmit;
                        this.disablemaintainAllocationPriorities = maintainAllocationPriorities;
                        this.disablestoreBulkUploadView = storeBulkUploadView;
                        this.disablemaintainSupplyingDepots = maintainSupplyingDepots;
                        this.disableViewFullRangeExports = ViewFullRangeExports;
                        this.disablefullRangeExportsSupplyChainReview = fullRangeExportsSupplyChainReview;
                        this.disableaddRequests = addRequests;
                        this.disablemanageRequests = manageRequests;
                        this.disablerequestsErrorCorrection = requestsErrorCorrection;
                        this.disablerequestsDataTeamReview = requestsDataTeamReview;
                        this.disablerequestsSupplyChainReview = requestsSupplyChainReview;
                        this.disablerequestsCommercialReview = requestsCommercialReview;
                        this.disablerepricingConfiguration = repricingConfiguration;
                        this.disablerepricingSupplierRequestsReview = repricingSupplierRequestsReview;
                        this.disablerepricingPeriodicReview = repricingPeriodicReview;
                        this.disablerepricingProductsIdentification = repricingProductsIdentification;
                        this.disableUploadRangeFileView = uploadRangeFileView;
                        this.disableUploadPromotionsView = uploadDealPromotions;
                        this.disableUploadRangeFileSubmit = uploadRangeFileSubmit;
                        this.disableUploadPromotionFileSubmit = uploadPromotionFileSubmit;
                        this.hasAccess = false;

                        if (permArrayWholesaleCatalogues.length > 0) {
                            this.permissionArray.permArrayWholesaleCatalogues = true;
                            this.hasAccess = true;
                        } else {
                            this.permissionArray.permArrayWholesaleCatalogues = false;
                        }
                        if (preArrayWholesaleStores.length > 0) {
                            this.permissionArray.preArrayWholesaleStores = true;
                        } else {
                            this.permissionArray.preArrayWholesaleStores = false;
                        }
                        if (permArrayNewRequests.length > 0) {
                            this.permissionArray.permArrayNewRequests = true;
                            this.hasAccess = true;
                        } else {
                            this.permissionArray.permArrayNewRequests = false;
                        }
                        if (permArrayMainEstateRange.length > 0) {
                            this.permissionArray.permArrayMainEstateRange = true;
                            this.hasAccess = true;
                        } else {
                            this.permissionArray.permArrayMainEstateRange = false;
                        }
                        if (permArrayPricing.length > 0) {
                            this.permissionArray.permArrayPricing = true;
                            this.hasAccess = true;
                        } else {
                            this.permissionArray.permArrayPricing = false;
                        }
                    }
                }
                console.log("this.permissionArray");
                console.log(this.permissionArray);
                console.log(this.permissionArray.length);
                //alert(this.disabledfilfilment);
            }, (error: any) => {
                if (error.errorCode == '503.65.001') {
                    this._router.navigate(['/error404']);
                }
            });
        });
    }

    setPermissionsArray(permissionGroup: string) {
        this.assignedPermissions.push(permissionGroup);
    }

    getConfigService(pageName: any) {
        this._postsService.siteMaintanceCall('1', 'all').subscribe(userData => {
            console.log(userData);
            console.log(userData.maintenanceDetails[0].message);
            console.log(userData.maintenanceDetails[0].start);
            console.log(userData.maintenanceDetails[0].status);
            console.log(userData.deploymentDetails);
            if (userData.deploymentDetails != undefined || userData.deploymentDetails.status!='NO' )  {
                this.deploymentDetailsMsg = userData.deploymentDetails[0].message;
                this.deploymentDetailsMsglength= this.deploymentDetailsMsg.length;
                  sessionStorage.setItem('deploymentDetailsMsglength', this.deploymentDetailsMsglength);
            } else {
                this.deploymentDetailsMsg = '';
            }


            console.log(userData.featureDetails);
            if (userData.featureDetails != undefined||  userData.featureDetails.status != 'NO') {
                this.featureDetailsMsg = userData.featureDetails[0].message + ' ' + userData.featureDetails[0].start;
                     this.featureDetailsMsglength = this.featureDetailsMsg.length;
                sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
            } else {
                this.featureDetailsMsg = '';
            }

            console.log(userData.businessMessageDetails);
            if (userData.businessMessageDetails != undefined || userData.businessMessageDetails != 'NO') {
                this.businessMessageDetailsMsg = userData.businessMessageDetails[0].message + ' ' + userData.businessMessageDetails[0].start;
                 this.businessMessageDetailsMsglength = this.businessMessageDetailsMsg.length;
                sessionStorage.setItem('businessMessageDetailsMsglength', this.businessMessageDetailsMsglength);;

            } else {
                this.businessMessageDetailsMsg = '';
            }

           (<HTMLLabelElement>document.getElementById('deploymentDetailsId')).textContent = this.deploymentDetailsMsg;
            (<HTMLLabelElement>document.getElementById('featureDetailsId')).textContent = this.featureDetailsMsg;
            (<HTMLLabelElement>document.getElementById('businessMessageDetailsId')).textContent = this.businessMessageDetailsMsg;
            if (userData.maintenanceDetails[0].moduleName == 'all') {
                if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                    this.siteMaintanceFlg = true;
                    this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                } else {
                    this.siteMaintanceFlg = false;
                    this._postsService.siteMaintanceCall(this.customerName, pageName).subscribe(userData => {
                        console.log(userData);
                        console.log(userData.maintenanceDetails[0].message);
                        console.log(userData.maintenanceDetails[0].start);
                        console.log(userData.maintenanceDetails[0].status);
                        if (userData.maintenanceDetails[0].moduleName == pageName) {
                            if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                                this.siteMaintanceFlg = true;
                                this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                            } else {
                                this.siteMaintanceFlg = false;
                            }
                        } else {
                            this.siteMaintanceFlg = false;
                        }
                        this.showLoader = false;
                    }
                        , error => {
                            console.log(error);
                            if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                                this.siteMaintanceFlg = true;
                                this.siteMSg = "Site is under maintenance";
                            } else if (error.errorCode == '404.26.408') {
                                this.errorMsgString = "User is Inactive";
                            }
                            else {
                                this.siteMaintanceFlg = false;
                            }
                            this.showLoader = false;
                        });
                }
            } else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        }, error => {
            console.log(error);
            if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                this.siteMaintanceFlg = true;
                this.siteMSg = "Site is under maintenance";
            } else if (error.errorCode == '404.26.408') {
                this.errorMsgString = "User is Inactive";
            }
            else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        });
    }
    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }
}