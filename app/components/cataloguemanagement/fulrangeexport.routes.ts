import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { fulRangeExportComponent } from './fulrangeexport.component';

// route Configuration
const fulRangeExportRoutes: Routes = [
  { path: '', component: fulRangeExportComponent },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(fulRangeExportRoutes)],
  exports: [RouterModule]
})
export class fulRangeExportComponentRoutes { }
