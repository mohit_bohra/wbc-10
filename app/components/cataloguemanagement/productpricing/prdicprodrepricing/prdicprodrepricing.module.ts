import { NgModule } from '@angular/core';
import { PrdicprodrepricingRoutes } from './prdicprodrepricing.routes';
import { PrdicprodrepricingComponent } from './prdicprodrepricing.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../../global/global.component';
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from "ngx-tooltip";
import { CalendarModule } from 'primeng/primeng';
import { CatalogueManagePendingHeaderModule } from '../../cataloguemanagependingheader/cataloguemanagependingheader.module';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { MaxNumberModule } from '../../../../directives/maxnumber/maxnumber.module';
import { MaxNumberWithInfiiteDecimalValueModule } from '../../../../directives/maxnumberwithinfiitedecimalvalue/maxnumberwithinfiitedecimalvalue.module';
import { OnlyNumberModule } from '../../../../directives/onlynumber/onlynumber.module';



// import { DatepickerModule } from 'ng2-bootstrap';
import { DatepickerModule } from '../../../datepicker/ng2-bootstrap';
import { SearchFilterPipe } from './serchfilter.component';
@NgModule({
  imports: [
    PrdicprodrepricingRoutes,
    CommonModule,
    FormsModule,
    DatepickerModule.forRoot(),
    CalendarModule,
    CatalogueManagePendingHeaderModule,
    TooltipModule,
    InfiniteScrollModule,
    MaxNumberModule,
    
    OnlyNumberModule
  ],
  exports: [],
  declarations: [PrdicprodrepricingComponent, SearchFilterPipe],
  providers: [Services, GlobalComponent, commonfunction]
})
export class PrdicprodrepricingModule { }
