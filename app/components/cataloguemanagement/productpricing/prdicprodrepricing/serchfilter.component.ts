import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'searchfilter'
})

@Injectable()
export class SearchFilterPipe implements PipeTransform {
    transform(items: any[], field: string, value: string): any[] {
        console.log(field);
        let temp: any[];
        console.log(value);
        if (!items) return [];
        if (field == '' || field == 'undefined' || typeof (field) == 'undefined') {
            sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => (it.approvalStatus == '' || it.approvalStatus == undefined))));
            return items.filter(it => (it.approvalStatus == '' || it.approvalStatus == undefined));
        }/* 
        if (field == 'cost.amount') {
            console.log(field);
            console.log(value);
            if (value == 'unaction') {
                sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => it.cost.amount > value)));
                return items.filter(it => (it.cost.amount > value) && (it.approvalStatus == '' || it.approvalStatus == undefined));
            } else {
                sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => it.cost.amount > value)));
                return items.filter(it => (it.cost.amount > value));
            }

        } else if (field == 'cost.percIncrease') {
            console.log(field);
            console.log(value);
            if (value == 'unaction') {
                sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => it.cost.percIncrease > value)));
                return items.filter(it => (it.cost.percIncrease > value) && (it.approvalStatus == '' || it.approvalStatus == undefined));
            } else {
                sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => it.cost.percIncrease > value)));
                return items.filter(it => it.cost.percIncrease > value);
            }

        } else */ if (field == 'both') {
            console.log(field);
            console.log(value[0]);
            console.log(value[1]);
            console.log(value[2]);
            console.log(items);
            sessionStorage.setItem('actualData', JSON.stringify(items.filter(it => (it.cost.amount))));
            sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]))));


            if (value[0] == '' && value[1] == '' && value[2] == 'true') {
                console.log('landing page');

                sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => (it.approvalStatus == '' || it.approvalStatus == undefined))));
                // return items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]));
                return items.filter(it => (it.approvalStatus == '' || it.approvalStatus == undefined));
            } else if (value[0] != '' && value[1] == '' && value[2] == 'true') {

                console.log("amount change");
                sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => (it.cost.amount > value[0]) && (it.approvalStatus == '' || it.approvalStatus == undefined))));
                return items.filter(it => (it.cost.amount > value[0]) && (it.approvalStatus == '' || it.approvalStatus == undefined));
                // return items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]));
            } else if (value[0] == '' && value[1] != '' && value[2] == 'true') {

                console.log("percentage change");
                sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => (it.cost.percIncrease > value[1]) && (it.approvalStatus == '' || it.approvalStatus == undefined))));
                return items.filter(it => (it.cost.percIncrease > value[1]) && (it.approvalStatus == '' || it.approvalStatus == undefined));
                // return items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]));
            } else if (value[0] != '' && value[1] != '' && value[2] == 'true') {

                console.log("amount change & percentage change");
                sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]) && (it.approvalStatus == '' || it.approvalStatus == undefined))));
                return items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]) && (it.approvalStatus == '' || it.approvalStatus == undefined));
                // return items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]));
            } else if (value[0] == '' && value[1] == '' && value[2] == 'false') {
                console.log('landing page false');

                sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => (it.cost.amount))));
                // return items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]));
                return items.filter(it => it.cost.amount);
            } else if (value[0] != '' && value[1] == '' && value[2] == 'false') {

                console.log("amount change");
                sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => (it.cost.amount > value[0]))));
                return items.filter(it => (it.cost.amount > value[0]));
                // return items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]));
            } else if (value[0] == '' && value[1] != '' && value[2] == 'false') {

                console.log("percentage change");
                sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => (it.cost.percIncrease > value[1]))));
                return items.filter(it => (it.cost.percIncrease > value[1]));
                // return items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]));
            } else if (value[0] != '' && value[1] != '' && value[2] == 'false') {
                console.log("amount change & percentage change");
                sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]) )));
                return items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]));
                // return items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]));
            }
            //temp = items.filter(it => (it.cost.amount > value[1] && it.cost.percIncrease > value[0]));
            //return items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]));
        } /* else if (field == 'approvalStatus') {
            if (value == 'unaction') {
                sessionStorage.setItem('ppfilterdata', JSON.stringify(items.filter(it => (it.approvalStatus == '' || it.approvalStatus == undefined))));
                return items.filter(it => (it.approvalStatus == '' || it.approvalStatus == undefined));
            } else {
                sessionStorage.setItem('ppolddata', JSON.stringify(items.filter(it => (it))));
                return items.filter(it => (it));
            }
        } else {
            sessionStorage.setItem('ppolddata', JSON.stringify(items.filter(it => (it))));
            return items.filter(it => it.cost.amount);
        } */
    }
}