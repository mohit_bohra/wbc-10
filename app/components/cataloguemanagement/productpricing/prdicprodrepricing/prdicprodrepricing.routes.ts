import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrdicprodrepricingComponent } from './prdicprodrepricing.component';
import { CanDeactivateGuard } from '../../../../services/can-deactivate-guard.service';

// route Configuration
const prdicprodrepricingRoutes: Routes = [
  { path: '', component: PrdicprodrepricingComponent, canDeactivate: [CanDeactivateGuard] },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(prdicprodrepricingRoutes)],
  exports: [RouterModule]
})
export class PrdicprodrepricingRoutes { }
