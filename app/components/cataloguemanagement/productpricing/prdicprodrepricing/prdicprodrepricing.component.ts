import { Component, ViewChild } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { Constant } from '../../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { document } from '../../../datepicker/utils/facade/browser';
import { GlobalComponent } from '../../../global/global.component';
import { FormBuilder } from '@angular/forms';
import { DialogService } from '../../../../services/dialog.service';

declare let saveAs: any;

@Component({
    templateUrl: 'prdicprodrepricing.component.html',
    providers: [commonfunction]
})
export class PrdicprodrepricingComponent {
    @ViewChild('repricingSearch') repricingSearch: any;
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;

    showLoader: boolean;
    start: any;
    limit: number;
    count: number;
    noDataFound: boolean;
    errorMsgPopup: any;

    printHeader: boolean;
    productRepricingData: any = [];
    productRepricingDatalen: any;
    tempproductRepricingData: any = [];
    productRepricingData1: any = [];
    productRepricingAllData: any = [];
    oldproductRepricingData: any = [];

    pageState: number = 33;
    catalogueTypes: any;
    catalogueType: string = "";
    cataloguemasterListLength: any;
    cataloguedetailListLength: any;
    catalogueDetailList: any;
    brandType: string = 'branded';
    productRepricingSalesData: any = [];
    productRepricingSalesDatalen: any;
    familyMemberData:any=[];
    familyMemberDatalen :any;
    successMsg: string = "";

    disablesub: boolean = false;
    disableDraftsub: boolean = false;
    disabletxtonSub: boolean = false;
    disableApprove: boolean = false;
    disableReject: boolean = false;
    disableSave: boolean = false;
    nodatamsg: string = '';
    filterValue: string = '';
    searchParam: string = '';
    searchVal: any;
    amountStr: any;
    increasePStr: any;
    counter: number = 0;
    unsavedChanges: boolean = false;
    reasonCodeFlag: any = [];

    organisationListChange: any;
    disabledCusChange: boolean;
    btn_repricingSupplierRequestsReviewSubmit: boolean = false;
    btn_repricingPeriodicReviewSubmit: boolean = false;
    repricingPeriodicReview: any;
    imgName: string = "";
    msgJSON: any = {};
    terminateMsg: any;
    terminateFlg: boolean = false;
    onlyshowUnactionFlag: boolean = false;
    changedBrand: any;
    previousBrand: any = 'branded';
    actionButton: any = [];

    //Constructor is used to initialize variables during load time
    constructor(private _postsService: Services, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, public _globalFunc: GlobalComponent, private formBuilder: FormBuilder, public dialogService: DialogService) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        this.limit = 15;
        this.start = 0;
        this.count = 30;
        this.searchVal = '';
        this.amountStr = '';
        this.increasePStr = '';
        this.onlyshowUnactionFlag = true;
        this.repricingSearch = this.formBuilder.group({})
    }

    canDeactivate(): Promise<boolean> | boolean {
        if (!this.unsavedChanges || sessionStorage.getItem('unsavedChanges')!=='true') {
            return true;
        }
        return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
    }

    getFieldVal(val: any) {
        console.log(this.onlyshowUnactionFlag);
        if (this.onlyshowUnactionFlag == true) {
            this.searchVal = [this.amountStr, this.increasePStr, 'true'];
        } else {
            this.searchVal = [this.amountStr, this.increasePStr, 'false'];
        }

        this.filterValue = 'both';
        /* if (this.amountStr != '' && this.increasePStr == '') {
            val = 'amountStr';
        } else if (this.increasePStr != '' && this.amountStr == '') {
            val = 'increasePStr';
        } else if (this.amountStr != '' && this.increasePStr != '') {
            val = 'both';
        }
        if (val == 'amountStr') {
            this.filterValue = 'cost.amount';
            this.searchParam = 'txtFname';
            if (this.amountStr == 'undefined') {
                this.amountStr = 0;
            }
            this.searchVal = this.amountStr;
        } else if (val == 'increasePStr') {
            this.filterValue = 'cost.percIncrease';
            this.searchParam = 'percenTxtname';
            if (this.increasePStr == 'undefined') {
                this.increasePStr = 0;
            }
            this.searchVal = this.increasePStr;
        } else if (val == 'both') {
            this.searchVal = [this.amountStr, this.increasePStr, this.onlyshowUnactionFlag];
            this.filterValue = 'both';
        }  else if (val == 'unaction') {
            this.filterValue = 'approvalStatus';
            this.searchVal = 'unaction';
        } else {
            this.searchVal = 0;
        } */
    }

    // This method is called on page load.
    // Hence all the basic cosmetic related elements are initialized here
    ngOnInit() {
        this.imageUrl = this.whoImgUrl;
        this.printHeader = true;
        this.unsavedChanges=false;
        sessionStorage.setItem("unsavedChanges", "false");
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

        sessionStorage.setItem('pageState', '33');

        this.sub = this._route.params.subscribe((params: { orgType: string }) => {
            let orgImgUrl: any;
            let cusType = params.orgType;
            this.customerName = params.orgType;
            let orgApiNameTemp;
            /* this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));
 
             this.organisationList.forEach(function (item: any) {
                 if (item.orgType === cusType) {
                     // logic for permission set
                     orgImgUrl = item.imgUrl;
                     orgApiNameTemp = item.orgName;
                 }
             });*/
            this.orgimgUrl = orgImgUrl;
            this.orgApiName = orgApiNameTemp;
            this.orgApiName = this.customerName;
            //this.getprodicProductReprising();
            this.imgName = this.getCustomerImage(params.orgType);
            this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
            this.getAllMasterData();
            this.productRepricingDatalen = -1;
        });

    }

    // All the line item data on HTML comes from here ( for all the tabs )
    // In this method we call a method from service class (app.service)
    // We store the data in a variable and then the same variable is used in HTML. In this case, the variable is 'productRepricingData'
    getprodicProductReprising(value?: any) {
        this.showLoader = true;
        /* console.log(value);
        this.catalogueType = value.catalogueType;
        var filterValue = value.ritem;
        this.brandType =  value.ritem; */
        //this.showLoader = true;
        this.previousBrand = this.brandType;

        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                sessionStorage.setItem('unsavedChanges','false');

                this.start = 0;
                this._postsService.getprodicProductReprising(this.customerName, this.catalogueType, this.brandType, this.start).subscribe(data => {
                    console.log(data);
                    if (Object.keys(data).length === 0) {
                        this.showLoader = false;

                        console.log('empty');
                        this.nodatamsg = 'No Records found for selected Filter!';
                        this.productRepricingData.length = 0;
                        this.productRepricingData1.length = 0;
                        this.productRepricingDatalen = 0;
                    } else {
                        this.showLoader = false;
                        /* if (data.pendingStatusCount == 0) {
                            this.onlyshowUnactionFlag = false;
                        } */
                        console.log('123--->' + JSON.stringify(data));
                        this.showHideButton(data);
                        
                        this.productRepricingData1 = data;
                        this.productRepricingData = data.periodicRepricing;
                        this.count = data.paginationMetaData.count;
                        this.productRepricingDatalen = this.productRepricingData.length;
                        this.oldproductRepricingData = this.productRepricingData;

                        data.periodicRepricing.forEach(function (itm: any, idx: number) {
                            itm.reasonFlag = false;
                            itm.isModified = false;
                        });

                        // data.periodicRepricing1.periodicRepricing.forEach(function (itm: any, idx: number) {
                        //     itm.isModified = false;
                        // });
                        
                        //console.log(this.productRepricingData);
                        sessionStorage.setItem('olddata', JSON.stringify(data.periodicRepricing));
                        console.log("old data")
                        console.log(sessionStorage.getItem('olddata'));
                        
                        
                    }
                    /* // document.getElementById('greenCheckboxForUnactionedInput').checked = true;
                    if (document.getElementById('greenCheckboxForUnactionedInput').checked) {
                        this.getFieldVal('unaction');
                    } else {
                        this.getFieldVal('');
                    } */
                    /* if(sessionStorage.getItem("ppfilterdata") != undefined){
                        this.productRepricingData = JSON.parse(sessionStorage.getItem("ppfilterdata"));
                    }  */

                }, error => {
                    console.log(error);
                    if (error.errorCode == '404.33.410') {
                        this.productRepricingData.length = 0;
                        this.productRepricingDatalen = 0;
                        this.nodatamsg = error.errorMessage;
                        this.showLoader = false;
                    } else {
                        this._router.navigate(['/error404']);
                    }

                });
            }
        }, reason => {
            console.log(reason);
        });

    }

    onScrollDown() {
        console.log("scroll" + this.count);
        this.start = this.start + this.limit;
        if (this.start < this.count - 1) {
            this.showLoader = true;
            this._postsService.getprodicProductReprising(this.customerName, this.catalogueType, this.brandType, this.start).subscribe(data => {
                this.showLoader = false;
                this.errorMsgPopup = "";

                this.productRepricingData = this.productRepricingData.concat(data.periodicRepricing);
                //this.productPriceData = data.periodicRepricing;
                this.productRepricingDatalen = this.productRepricingData.length;
                //this.count = data.paginationMetaData.count;
                console.log(data);
                console.log(this.productRepricingData);

                console.log("Data");
                console.log(data);
                this.showLoader = false;

            }, error => {
                console.log(error);
                if (error.errorCode == '404.33.410') {
                    this.showLoader = false;
                } else {
                    this._router.navigate(['/error404']);
                }

            });
        }
    }

    showHideButton(data: any) {
        console.log(data.status);
        
        if (data.status == 'PeriodicRepricingSubmissionCompleted' || data.status == 'lock' || data.status == 'notYetFinished') {
            
            this.disablesub = true;
            this.disableSave = true;
            this.disableApprove = true;
            this.disableDraftsub = true;
            this.disableReject = true;
            this.disabletxtonSub = true;
        } else if (data.status == 'draft') {
            if (typeof (data.pendingStatusCount) !== 'undefined' && (data.pendingStatusCount == 0 || data.pendingStatusCount == '0')) {
                this.disableSave = true;
                this.disableApprove = true;
                this.disablesub = false;// enable
                this.disableDraftsub = false;// enable
                this.disableReject = true;
            } else if (typeof (data.pendingStatusCount) !== 'undefined' && (data.pendingStatusCount != 0 || data.pendingStatusCount != '0')) {
                this.disableSave = false;// enable
                this.disableApprove = false;// enable
                this.disablesub = true;
                this.disableDraftsub = true;
                this.disableReject = false;// enable
            }
        } else if (data.status == 'PeriodicRepricingDraftCompleted') {
            this.disablesub = false;// enable
            this.disableSave = false;// enable
            this.disableApprove = false;// enable
            this.disableDraftsub = false;// enable
            this.disableReject = false;// enable
        } else {
            this.disablesub = false;
            this.disableSave = true;// enable
            this.disableApprove = true;// enable
            this.disableDraftsub = false;
            this.disableReject = true;// enable
        }

    }

    // calcualte Cutomer Impact
    /* calcualteCutomerImpact(anualSales: any, uom: any, customerVal: any, i: number, caseSizeNew: any, amount: any){
        let customerImpactVal: any;
        console.log(uom);
        if(uom == 'CS' || uom == 'cs'){
            customerImpactVal = (parseInt(anualSales) * caseSizeNew * amount).toFixed(2);
        } else {
            customerImpactVal = (parseInt(anualSales) * amount).toFixed(2);
        }
        this.productRepricingData[i].calculatedVal = customerImpactVal;
        console.log(customerImpactVal);
        return customerImpactVal;
    }  */

    getFinalMargin(calMargin: any, index: any,arrItem: any) {
        console.log(JSON.parse(sessionStorage.getItem('ppfilterdata')));
        let tempval = JSON.parse(sessionStorage.getItem('ppfilterdata'));
        this.productRepricingData.forEach(function (item: any, i: any) {
             item.isModified = false;
            if(!isNaN(calMargin)) {
                if (typeof (calMargin) !== 'undefined' && item.margin.overwrittenMarginPercent === calMargin && item == arrItem) {
                        if (calMargin == '') {
                            item.margin.finalMarginPercent = tempval[i].margin.calculatedMarginPercent;
                            item.nonPromoWsp.nonPromoWsp = (tempval[i].margin.totalCost / (1 - (tempval[i].margin.finalMarginPercent / 100)));
                            item.nonPromoWsp.preChallengeWsp = (tempval[i].margin.totalCost / (1 - (tempval[i].margin.finalMarginPercent / 100))).toFixed(4);
                            item.nonPromoWsp.nonPromoFinalWsp = ((tempval[i].nonPromoWsp.preChallengeWsp) * (1 - (tempval[i].nonPromoWsp.nonPromoChallenge / 100))).toFixed(4);
                            item.cost.wspNew = tempval[i].cost.wspNew;
                            item.cost.amount = tempval[i].cost.amount;
                            console.log(tempval[i].productInfo.customerImpact);
                            item.productInfo.customerImpact = tempval[i].productInfo.customerImpact;
                            var valueResultnewtemp = tempval[i].nonPromoWsp.nonPromoFinalWsp * tempval[i].cost.caseSize;
                            item.nonPromoWsp.nonPromoWspCase = valueResultnewtemp.toFixed(2);
                            // item.productInfo.customerImpact = tempval[i].productInfo.customerImpact;
                            item.cost.percIncrease = tempval[i].cost.percIncrease;
                            item.isModified = false;
                        } else {
                        item.margin.finalMarginPercent = item.margin.overwrittenMarginPercent;
                        item.nonPromoWsp.nonPromoWsp = (item.margin.totalCost / (1 - (item.margin.finalMarginPercent / 100)));
                        item.nonPromoWsp.preChallengeWsp = (item.margin.totalCost / (1 - (item.margin.finalMarginPercent / 100))).toFixed(4);
                        item.nonPromoWsp.nonPromoFinalWsp = ((item.nonPromoWsp.preChallengeWsp) * (1 - (item.nonPromoWsp.nonPromoChallenge / 100))).toFixed(4);
                        item.cost.wspNew = item.nonPromoWsp.nonPromoFinalWsp;
                        item.cost.amount = (item.cost.wspNew - item.cost.wsp).toFixed(2);
                        item.cost.percIncrease = ((item.cost.amount / item.cost.wsp) * 100).toFixed(2);
                        var valueResult1 = item.nonPromoWsp.nonPromoFinalWsp * item.cost.caseSize;
                        item.nonPromoWsp.nonPromoWspCase = valueResult1.toFixed(2);
                        console.log(item.cost.uomType);
                        item.isModified = true;
                        console.log(item.productInfo.itemAnnualSaleQty);
                        if (item.productInfo.itemAnnualSaleQty == undefined || item.productInfo.itemAnnualSaleQty == "") {
                            item.productInfo.customerImpact = "";
                        } else {
                            if (item.cost.uomType == 'EA' || item.cost.uomType == 'ea') {
                                console.log("nonsence");
                                console.log(item.cost.amount);
                                console.log(parseInt(item.productInfo.itemAnnualSaleQty));
                                item.productInfo.customerImpact = (item.cost.amount * parseInt(item.productInfo.itemAnnualSaleQty)).toFixed(2);
                            } else {
                                item.productInfo.customerImpact = (item.cost.caseSizeNew * item.cost.amount * parseInt(item.productInfo.itemAnnualSaleQty)).toFixed(2);
                            }
                        }

                    }

                }
            }
        });
        this.disableDraftsub = true; // disable 
        this.disablesub = true; // disable 
        this.disableSave = false; // enable 
        this.disableApprove = false; // enable 
        this.disableReject = false; // enable


        //new code for the unsaved msg
            let val = calMargin;
        	if (this.oldproductRepricingData[index].approvalStatus != val) {
            this.unsavedChanges=true;
            sessionStorage.setItem("unsavedChanges", "true");
            this.productRepricingData[index].isModified = true;
            this.counter += 1;
        } else {
            if (this.counter > 1) {
                this.unsavedChanges=true;
                sessionStorage.setItem("unsavedChanges", "true");
                this.productRepricingData[index].isModified = true;
                this.counter -= 1;
            } else {
                this.unsavedChanges=false;
                sessionStorage.setItem("unsavedChanges", "false");
                this.productRepricingData[index].isModified = false;
                this.counter -= 1;
            }
        }

    }

    getcalOMp(nonPromoOverwrittenWsp: any, index: any, arrITem: any) {
        console.log('1212121212');
        console.log(nonPromoOverwrittenWsp);
        console.log(index);
        console.log(arrITem);
        console.log(JSON.parse(sessionStorage.getItem('ppfilterdata')));
        console.log(this.productRepricingData);
        console.log('2121212121');
        let val = nonPromoOverwrittenWsp;
        if (this.oldproductRepricingData[index].approvalStatus != val) {
            this.unsavedChanges=true;
            sessionStorage.setItem("unsavedChanges", "true");
            this.productRepricingData[index].isModified = true;
            this.counter += 1;
        } else {
            if (this.counter > 1) {
                this.unsavedChanges=true;
                sessionStorage.setItem("unsavedChanges", "true");
                this.productRepricingData[index].isModified = true;
                this.counter -= 1;
            } else {
                this.unsavedChanges=false;
                sessionStorage.setItem("unsavedChanges", "false");
                this.productRepricingData[index].isModified = false;
                this.counter -= 1;
            }
        }

        let tempval = JSON.parse(sessionStorage.getItem('ppfilterdata'));
        if(!isNaN(nonPromoOverwrittenWsp)) {
            this.productRepricingData.forEach(function (item: any, i: any) {
                console.log(i);
                item.isModified = false;
                var nonPromoOverwrittenWsp = item.nonPromoWsp.nonPromoOverwrittenWsp;
                if (typeof (nonPromoOverwrittenWsp) !== 'undefined' && item.nonPromoWsp.nonPromoOverwrittenWsp === nonPromoOverwrittenWsp && item == arrITem) {
                    if (nonPromoOverwrittenWsp == '') {
                        console.log(tempval[i].margin.finalMarginPercent);
                        item.margin.finalMarginPercent = tempval[i].margin.calculatedMarginPercent;
                        item.margin.calculatedMarginPercent = tempval[i].margin.calculatedMarginPercent;
                        item.margin.overwrittenMarginPercent = tempval[i].margin.overwrittenMarginPercent;
                        /*  item.nonPromoWsp.nonPromoWsp = tempval[i].nonPromoWsp.nonPromoWsp;
                        item.nonPromoWsp.preChallengeWsp = tempval[i].nonPromoWsp.preChallengeWsp
                        item.nonPromoWsp.nonPromoFinalWsp = tempval[i].nonPromoWsp.nonPromoFinalWsp;
                        item.nonPromoWsp.nonPromoWspCase = tempval[i].nonPromoWsp.nonPromoWspCase; */

                        // recalculated based on MWEE- defect 
                        item.nonPromoWsp.nonPromoWsp = (tempval[i].margin.totalCost / (1 - (tempval[i].margin.finalMarginPercent / 100)));
                        item.nonPromoWsp.preChallengeWsp = (tempval[i].margin.totalCost / (1 - (tempval[i].margin.finalMarginPercent / 100))).toFixed(4);
                        item.nonPromoWsp.nonPromoFinalWsp = ((tempval[i].nonPromoWsp.preChallengeWsp) * (1 - (tempval[i].nonPromoWsp.nonPromoChallenge / 100))).toFixed(4);
                        item.cost.wspNew = tempval[i].cost.wspNew;
                        item.cost.amount = tempval[i].cost.amount;
                        console.log(tempval[i].cost.customerImpact);
                        item.productInfo.customerImpact = tempval[i].productInfo.customerImpact;
                        var valueResultnew = tempval[i].nonPromoWsp.nonPromoFinalWsp * tempval[i].cost.caseSize;
                        item.nonPromoWsp.nonPromoWspCase = valueResultnew.toFixed(2);
                        item.cost.percIncrease = tempval[i].cost.percIncrease;
                        item.isModified = false;
                    } else {

                        var valueResult = (1 - ((1 - (item.nonPromoWsp.nonPromoChallenge / 100)) * (item.margin.totalCost) / (nonPromoOverwrittenWsp))) * 100;
                        if (nonPromoOverwrittenWsp == 0) {
                            var valueResult = 0
                        }
                        item.margin.overwrittenMarginPercent = valueResult.toFixed(2);
                        item.margin.finalMarginPercent = item.margin.overwrittenMarginPercent;
                        item.nonPromoWsp.nonPromoWsp = (item.margin.totalCost / (1 - (item.margin.finalMarginPercent / 100)));
                        item.nonPromoWsp.preChallengeWsp = (item.margin.totalCost / (1 - (item.margin.finalMarginPercent / 100))).toFixed(4);
                        item.nonPromoWsp.nonPromoFinalWsp = ((item.nonPromoWsp.preChallengeWsp) * (1 - (item.nonPromoWsp.nonPromoChallenge / 100))).toFixed(4);
                        item.cost.wspNew = item.nonPromoWsp.nonPromoFinalWsp;
                        item.cost.amount = (item.cost.wspNew - item.cost.wsp).toFixed(2);
                        item.cost.percIncrease = ((item.cost.amount / item.cost.wsp) * 100).toFixed(2);
                        console.log(item.productInfo.itemAnnualSaleQty);
                        item.isModified = true;
                        if (item.productInfo.itemAnnualSaleQty == undefined || item.productInfo.itemAnnualSaleQty == "") {
                            item.productInfo.customerImpact = "";
                        } else {
                            if (item.cost.uomType == 'EA' || item.cost.uomType == 'ea') {
                                console.log("nileshafdaf");
                                console.log(item.cost.amount);
                                console.log(parseInt(item.productInfo.itemAnnualSaleQty));
                                item.productInfo.customerImpact = (item.cost.amount * parseInt(item.productInfo.itemAnnualSaleQty)).toFixed(2);
                            } else {
                                item.productInfo.customerImpact = (item.cost.caseSizeNew * item.cost.amount * parseInt(item.productInfo.itemAnnualSaleQty)).toFixed(2);
                            }
                        }

                        var valueResult1 = item.nonPromoWsp.nonPromoFinalWsp * item.cost.caseSize;
                        item.nonPromoWsp.nonPromoWspCase = valueResult1.toFixed(2);
                    }
                }
            });
        }
        this.disableDraftsub = true; // disable 
        this.disablesub = true; // disable 
        this.disableSave = false; // enable 
        this.disableApprove = false; // enable 
        this.disableReject = false; // enable
    }

    calPromoWSPCase(promowspcase: any, index: any, arrItem: any) {
        console.log(JSON.parse(sessionStorage.getItem('ppfilterdata')));

        let val = promowspcase;
        if (this.oldproductRepricingData[index].approvalStatus != val) {
            this.unsavedChanges=true;
            sessionStorage.setItem("unsavedChanges", "true");
            this.productRepricingData[index].isModified = true;
            this.counter += 1;
        } else {
            if (this.counter > 1) {
                this.unsavedChanges=true;
                sessionStorage.setItem("unsavedChanges", "true");
                this.productRepricingData[index].isModified = true;
                this.counter -= 1;
            } else {
                this.unsavedChanges=false;
                sessionStorage.setItem("unsavedChanges", "false");
                this.productRepricingData[index].isModified = false;
                this.counter -= 1;
            }
        }

        let tempval = JSON.parse(sessionStorage.getItem('ppfilterdata'));
        this.productRepricingData.forEach(function (item: any, i: any) {
             item.isModified = false;
            if(!isNaN(promowspcase)){
                if (typeof (promowspcase) !== 'undefined' && item.promoWsp.promoFinalWsp === promowspcase && item == arrItem) {
                    if (promowspcase == '') {
                        if (promowspcase == "" || promowspcase == 0) {
                            item.promoWsp.promoWspCase = '';
                        }
                        else {
                            var valueResult = tempval[i].promoWsp.promoFinalWsp * tempval[i].cost.caseSize;
                            item.promoWsp.promoWspCase = valueResult.toFixed(2);
                        }
                    } else {
                        console.log(item.cost.caseSize);
                        var valueResult = promowspcase * item.cost.caseSize;
                        item.promoWsp.promoWspCase = valueResult.toFixed(2);
                    }
                    item.isModified = true;
                }
            }
        });
        this.disableDraftsub = true; // disable 
        this.disablesub = true; // disable 
        this.disableSave = false; // enable 
        this.disableApprove = false; // enable 
        this.disableReject = false; // enable
    }

    /*   calnonpromowspcase(nonPromoFinalWsp: any, index: any) {
          this.productRepricingData.forEach(function (item: any, i: any) {
              if (typeof (nonPromoFinalWsp) !== 'undefined' && item.nonPromoWsp.nonPromoFinalWsp === nonPromoFinalWsp && i == index) {
                  console.log(valueResult);
                  console.log(nonPromoFinalWsp);
                  console.log(item.cost.caseSize);
                  var valueResult = nonPromoFinalWsp * item.cost.caseSize;
                  item.nonPromoWsp.nonPromoWspCase = valueResult.toFixed(2);
              }
          });
      } */

    cancelandExit() {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                this._router.navigate(['cataloguemanagement', this.customerName]);
            }
        }, reason => {
            console.log(reason);
        });
    }


    //rejectallpending
    rejectallpending(productRepricingDataLen: any) {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                if (sessionStorage.getItem('unsavedChanges') == 'false') {
                    sessionStorage.setItem('unsavedChanges', 'true');
                }
                this.getprodicProductReprisingAllRecords('rejected');
                /*this.getprodicProductReprising("");
                console.log(this.productRepricingData1);
                for (var j = 0; j < this.productRepricingData.length; j++) {
                    if (this.productRepricingData[j]['approvalStatus'] == "" || this.productRepricingData[j]['approvalStatus'] == 'undefined' || this.productRepricingData[j]['approvalStatus'] != 'approved') {
                        this.productRepricingData[j]['approvalStatus'] = 'rejected';
                    }
                }
                console.log(this.productRepricingData1);
                console.log(this.productRepricingData);
                this.updateRepricing(this.productRepricingData1);*/
            }
        }, reason => {
            console.log(reason);
        });
    }

    getprodicProductReprisingAllRecords(status: string) {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                this.showLoader = true;
                this._postsService.getprodicProductReprising(this.customerName, this.catalogueType, this.brandType, '@all').subscribe(data => {
                    console.log(data);
                    if (Object.keys(data).length === 0) {

                    } else {
                        this.productRepricingAllData = data;
                       // console.log('Data Pricing :' + JSON.stringify(data));
                        for (var j = 0; j < this.productRepricingAllData.periodicRepricing.length; j++) {
                           
                            if (this.productRepricingAllData.periodicRepricing[j]['approvalStatus'] == "" || this.productRepricingAllData.periodicRepricing[j]['approvalStatus'] == 'undefined' || this.productRepricingAllData.periodicRepricing[j]['approvalStatus'] != 'approved') {
                                this.productRepricingAllData.periodicRepricing[j]['approvalStatus'] = status;
                                this.productRepricingAllData.periodicRepricing[j]['isModified'] = true;
                               
                            }
                        }                        
                        this.updatePeriodicRepricingAllRecord();
                    }
                    this.showLoader = false;
                }, error => {
                    this.showLoader = false;
                });
            }
        }, reason => {
            console.log(reason);
        });


    }

    updatePeriodicRepricingAllRecord() {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                this.showLoader = true;                
                this._postsService.updateperiodicRepricing(this.customerName, this.productRepricingAllData, this.catalogueType).subscribe(data => {
                    this.showLoader = false;
                    if (status == 'notYetFinished' || status == 'draft_lock') {
                        this.successMsg = "Your request for initiating Report has been accepted.Report generation is in progress";
                    }

                    this.successMsg = "Periodic Repricing saved successfully.";
                    this.start = 0;
                    this.getprodicProductReprising("");


                    let sessionModal = document.getElementById('RepricingSave');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('RepricingSave').style.display = 'block';
                  this.showHideButton(data);
                }
                    , error => {
                        console.info("Error is here");
                        console.log(error);
                        this.showLoader = false;
                        //this._router.navigate(['/error404']);
                    }
                );
            }
        }, reason => {
            console.log(reason);
        });


    }

    approveAll(arrLen: any) {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                this.showLoader = true;
                console.info(this.searchVal);
                console.info(this.filterValue);
                console.log('approveAll');
                console.log(arrLen);
                console.log(this.productRepricingData);
                console.log(this.productRepricingData.length);

                if (sessionStorage.getItem('unsavedChanges') == 'false') {
                    this.unsavedChanges = true;
                    sessionStorage.setItem('unsavedChanges', 'true');
                }

                if (this.searchVal[0] == "" && this.searchVal[1] == "") {
                    console.log("Without Filter");
                    for (var j = 0; j < this.productRepricingData.length; j++) {
                        this.productRepricingData[j]['approvalStatus'] = 'approved';
                       this.productRepricingData[j].isModified = true;

                    }
                } else {
                    for (var j = 0; j < this.productRepricingData.length; j++) {
                        if(this.searchVal[0] != "" && this.searchVal[1] != ""){
                            if(this.productRepricingData[j]['cost']['percIncrease'] > this.searchVal[1] && this.productRepricingData[j]['cost']['amount'] > this.searchVal[0]){
                                this.productRepricingData[j]['approvalStatus'] = 'approved';
                                this.productRepricingData[j].isModified = true;

                            }
                        } else if(this.searchVal[0] != "" && this.productRepricingData[j]['cost']['amount'] > this.searchVal[0]){
                            this.productRepricingData[j]['approvalStatus'] = 'approved';
                           this.productRepricingData[j].isModified = true;

                        } else if(this.searchVal[1] != "" && this.productRepricingData[j]['cost']['percIncrease'] > this.searchVal[1]){
                            this.productRepricingData[j]['approvalStatus'] = 'approved';
                            this.productRepricingData[j].isModified = true;

                        }
                    }
                }
                console.log(this.productRepricingData);
                console.log("Abov is final json");
                // this.updateRepricing(this.productRepricingData1);
                // this.disableApprove = true;
                this.showLoader = false;
            }
        }, reason => {
            console.log(reason);
        });


    }

    generateSub() {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                /* this.readonlyText = true;*/
                this.productRepricingData1.status = 'lock';
                status = this.productRepricingData1.status;
                this.disablesub = true;
                this.disabletxtonSub = true;
                this.disableDraftsub = true;
                this.disableSave = true;
                this.disableApprove = true;
                this.disableReject = true;
                console.log(this.productRepricingData1);
                this.updateRepricing(this.productRepricingData1);
            }
        }, reason => {
            console.log(reason);
        });



    }

    generateDraftSub() {

        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                this.getprodicProductReprising("");
                if (this.productRepricingData.status == 'notYetFinished') {
                    this.successMsg = "Periodic Repricing Draft is in Progress. Please try after some time";
                    let sessionModal = document.getElementById('RepricingSave');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('RepricingSave').style.display = 'block';
                } else {
                    /* this.readonlyText = true;*/
                    this.productRepricingData1.status = 'draft_lock';
                    status = this.productRepricingData1.status;
                    console.log(this.productRepricingData1);
                    this.updateRepricing(this.productRepricingData1);
                }
            }
        }, reason => {
            console.log(reason);
        });

    }

    updateRepricing(value: any) {

        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {

                console.log(value);
                let tempJSON;
                if (value == 'form') {
                    tempJSON = this.productRepricingData1;
                    this.productRepricingData1.status = 'draft';
                } else {
                    console.log('generate');
                    tempJSON = value;
                }


                this.productRepricingData1 = tempJSON;
                this.productRepricingData1.userName = sessionStorage.getItem('name');
                console.log(this.productRepricingData1);

                this.productRepricingData1.periodicRepricing = this.productRepricingData;
                console.log(this.productRepricingData);
                console.log(this.productRepricingData1);
                this._postsService.updateperiodicRepricing(this.customerName, this.productRepricingData1, this.catalogueType).subscribe(data => {
                    this.showLoader = false;
                    console.log(status + "status");
                    if (status == 'notYetFinished' || status == 'draft_lock') {
                        this.successMsg = "Your request for initiating Report has been accepted.Report generation is in progress"
                    }

                    else if (status == this.productRepricingData1.status) {
                        this.successMsg = "Periodic Repricing locked successfully.";
                    } else {
                        this.successMsg = "Periodic Repricing saved successfully.";
                    }

                    this.start = 0;
                   //  this.getprodicProductReprising("");
                   //  this.showHideButton(data);
                   this.getProdicProductReprisingsingle();
                    // data.pendingStatusCount = "0";
                    let sessionModal = document.getElementById('RepricingSave');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('RepricingSave').style.display = 'block';

                   
                }
                    , error => {
                        console.info("Error is here");
                        console.log(error);
                        //this._router.navigate(['/error404']);
                    }
                );

            }
        }, reason => {
            console.log(reason);
        });
    }

    getProdicProductReprisingsingle(){
        console.log('New Function');
        this._postsService.getprodicProductReprising(this.orgApiName, this.catalogueType, this.brandType, this.start).subscribe(data => {
            console.log('123-->' + data.pendingStatusCount);
            console.log('Action Button : ' + JSON.stringify(this.actionButton));
            
            //code added on 6/9/2018 against 5892 isse
            this.productRepricingAllData = data;
           // console.log('Data Pricing :' + JSON.stringify(data));
            for (let j = 0; j < this.productRepricingAllData.periodicRepricing.length; j++) {
                
                // if (this.productRepricingAllData.periodicRepricing[j]['approvalStatus'] == "" || this.productRepricingAllData.periodicRepricing[j]['approvalStatus'] == 'undefined' || this.productRepricingAllData.periodicRepricing[j]['approvalStatus'] != 'approved') {
                //     this.productRepricingAllData.periodicRepricing[j]['approvalStatus'] = status;
                //     this.productRepricingAllData.periodicRepricing[j]['isModified'] = true;
                // }
                
                for (let k = 0; k < this.actionButton.length; k++) {
                    if (this.productRepricingAllData.periodicRepricing[j]['legacyCode'] == this.actionButton[k].legacyCode) {
                        this.productRepricingAllData.periodicRepricing[j]['approvalStatus'] = this.actionButton[k].status;
                        this.productRepricingAllData.periodicRepricing[j]['isModified'] = true;
                        break;
                    }
                }
                
                
                
            }
            // for(let j = 0; j < this.actionButton.length; j++){
            //     this.productRepricingAllData.periodicRepricing[this.actionButton[j].index]['approvalStatus']= this.actionButton[j].status;
            //     this.productRepricingAllData.periodicRepricing[this.actionButton[j].index]['isModified'] = true;
            // }
            console.log('update data : ' + JSON.stringify(this.productRepricingAllData));
            
            this.updatePeriodicRepricingAllRecord();

            // this.showHideButton(data);
        });
        
    }

    closeRepricingUpdateSuccess() {
        // this.showLoader = false;
        let sessionModal = document.getElementById('RepricingSave');
        sessionModal.classList.add('in');
        sessionModal.classList.remove('out');
        document.getElementById('RepricingSave').style.display = 'none';
    }


    // This method is activated when one clicks on the check or uncheck button in HTML
    // This method calls another method, which is situated in common.services class
    getActivateCheckbox(val: any, index: any, value: any, eleName: string, codeLegacy?: any) {
        console.log('-------' + this.disableSave);
       // if(this.disableSave === true){
        if(this.disableSave && !this.disablesub){
            this.reasoncodeclick();
        }
        this._cfunction.getActivateCheckbox(index, value, eleName);
        var button: any = {};
        var isFlag = false;
        button.index = index;
        button.status = val;
        button.legacyCode = codeLegacy;
        console.log('Length --->' + this.actionButton.length);
        if(this.actionButton === null) {
            this.actionButton.push(button);
        } else {
            for (let i =0; i< this.actionButton.length; i++){
                if(this.actionButton[i].index == button.index){
                    this.actionButton[i].status = button.status;
                    this.actionButton[i].legacyCode = button.legacyCode;
                    isFlag = true;
                    break;
                } 
            }

            if (!isFlag){
                this.actionButton.push(button);
            }
        }       
        console.log('Click ' + JSON.stringify(this.actionButton));
        this.oldproductRepricingData = JSON.parse(sessionStorage.getItem('ppfilterdata'));

        if (this.oldproductRepricingData[index].approvalStatus != val) {
            this.unsavedChanges=true;
            sessionStorage.setItem("unsavedChanges", "true");
            this.productRepricingData[index].isModified = true;
            this.counter += 1;
        } else {
            if (this.counter > 1) {
                this.unsavedChanges=true;
                sessionStorage.setItem("unsavedChanges", "true");
                this.productRepricingData[index].isModified = true;
                this.counter -= 1;
            } else {
                this.unsavedChanges=false;
                sessionStorage.setItem("unsavedChanges", "false");
                this.productRepricingData[index].isModified = false;
                this.counter -= 1;
            }
        }

    }

    checkForCharacter(value: any) {
        var pattern = /^(\d{1,2}|\d{0,2}\.\d{1,2})$/;
        //let inputChar = String.fromCharCode(value.charCode);
        var iValue = value.target.value;
        if (pattern.test(iValue)) {

        } else {
            //invalid character, prevent input
            value.preventDefault();

        }
    }

    // On HTML, there is a dropdown, all the data of this dropdown comes from this method
    // In this method we call a method from service class (app.service)
    // We store the data in a variable and then the same variable is used in HTML. In this case, the variable is 'catalogueTypes'
    getAllMasterData() {
        this.showLoader = true;
        this._postsService.getAllMasterData(this.customerName).subscribe(data => {
            this.showLoader = false;
            if (data.catalogueTypes.length == 0) {
                this.cataloguemasterListLength = 0;
            } else {
                for (var k = 0; k < data.catalogueTypes.length; k++) {
                    if (data.catalogueTypes[k].catalogueType.indexOf(':') > 0) {
                        data.catalogueTypes[k].catalogueType = data.catalogueTypes[k].catalogueType.split(':')[1];
                    }
                }
                this.cataloguemasterListLength = data.catalogueTypes.length;
            }
            this.catalogueTypes = data.catalogueTypes;
            this.showHideButton(data);
        }
            , err => {
                this.cataloguemasterListLength = 0;
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
        );
    }

    // This method is activated when we click on print icon
    printOrder() {
        //this.printHeader = false;
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        var tableData = document.getElementById('exportable').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + tableData + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + tableData + '</body></html>');
            popup.document.close();
        }
    }

    //  // This method is activated when we click on download icon
    exportData() {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Product Repricing.xls");
    }

    getChangeValue(val: any, index: any, event: any) {
        //  alert(val)
        console.log(event);
        this.oldproductRepricingData = JSON.parse(sessionStorage.getItem('ppfilterdata'));
        var key = event.keyCode || event.charCode;
        // if(key == 8 || key == 46){

        // // }
        // if(this.oldproductRepricingData[index].productInfo.reasonCode != val){
        //     sessionStorage.setItem("unsaveChagne", "true");
        //     this.unsavedChanges = true;
        //     this.counter += 1;
        // }else{

        // if( this.counter > 1){
        //     sessionStorage.setItem("unsaveChagne", "true");
        //     this.unsavedChanges = true;
        //     this.counter -= 1;
        // }else{
        //     sessionStorage.setItem("unsaveChagne", "false");
        //     this.unsavedChanges = false;
        //     this.counter -= 1;
        // }
        // }
        
        if ( key == 46 || (key>47 && key <57)) {            
            console.log(this.oldproductRepricingData[index].productInfo.reasonCode +  ' != ' + val);
            if (this.oldproductRepricingData[index].productInfo.reasonCode != val) {
                console.log('1');
                this.unsavedChanges=true;
                sessionStorage.setItem("unsavedChanges", "true");
                this.productRepricingData[index].isModified = true;
                this.counter += 1;
            } else {
                if (this.counter > 1 || event.target.value != null) {
                    console.log('2');
                    this.unsavedChanges=true;
                    sessionStorage.setItem("unsavedChanges", "true");
                    this.productRepricingData[index].isModified = true;
                    this.counter -= 1;
                } else {
                    console.log('3');
                    this.unsavedChanges=false;
                    sessionStorage.setItem("unsavedChanges", "false");
                    this.productRepricingData[index].isModified = false;
                    this.counter -= 1;
                    if (val == "") {
                        this.unsavedChanges=false;
                        sessionStorage.setItem("unsavedChanges", "false");
                        this.productRepricingData[index].isModified = false;
                        this.counter = 0;
                    }
                }
            }
        } else {
            if (this.oldproductRepricingData[index].productInfo.reasonCode != val) {            
                console.log('4');
                this.unsavedChanges=true;
                sessionStorage.setItem("unsavedChanges", "true");
                this.productRepricingData[index].isModified = true;
                this.counter += 1;
            } else {
                if (this.counter > 1 || event.target.value != null) {
                    console.log('5');
                    this.unsavedChanges=true;
                    sessionStorage.setItem("unsavedChanges", "true");
                    this.productRepricingData[index].isModified = true;
                    this.counter -= 1;
                } else {
                    console.log('6');
                    this.unsavedChanges=false;
                    sessionStorage.setItem("unsavedChanges", "false");
                    this.productRepricingData[index].isModified = false;
                    this.counter -= 1;
                }
            }
        }
        console.log("this.productRepricingData : "+index);
        console.log(this.productRepricingData);

        //let tempval = JSON.parse(sessionStorage.getItem('olddata'));

        // if(val == ''){
        //     this.productRepricingData[index].productInfo.reasonCode = tempval[index].productInfo.reasonCode;
        //     sessionStorage.setItem("unsaveChagne", "false");
        //     this.unsavedChanges = false;
        // }

        // if(tempval[index].productInfo.reasonCode != val){
        //     sessionStorage.setItem("unsaveChagne", "true");
        // }

        // this.productRepricingData.forEach(function (item: any, i: any) {
        //     console.log(item.productInfo.reasonCode);
        //     console.log(i);
        //     console.log(val);
        //     if(val == ''){
        //         //that.productRepricingData[index].productInfo.reasonCode = tempval[i].productInfo.reasonCode;
        //        //item.productInfo.reasonCode = tempval[i].productInfo.reasonCode;
        //         sessionStorage.setItem("unsaveChagne", "false");
        //         this.unsavedChanges = false;
        //     }else{
        //         if (index == i) {
        //             if (item.productInfo.reasonCode != val) {
        //                 sessionStorage.setItem("unsaveChagne", "false");
        //                 this.unsavedChanges = false;
        //             } else {
        //                 sessionStorage.setItem("unsaveChagne", "true");
        //                 this.unsavedChanges = true;
        //             }
        //         } else {
        //             sessionStorage.setItem("unsaveChagne", "true");
        //             this.unsavedChanges = true;
        //         }
        //     }       
        // });
        console.log(sessionStorage.getItem("unsavedChanges"));
    }

    reasoncodeclick() {
        this.disableDraftsub = true; // disable 
        this.disablesub = true; // disable 
        this.disableSave = false; // enable 
        this.disableApprove = false; // enable 
        this.disableReject = false; // enable               

    }

    salesPopup(idx: number, legacy: any, pin: any) {
        console.log("idx = " + idx);
        this._postsService.getProductRepricingSalesData(this.customerName, legacy, pin).subscribe(data => {
            this.productRepricingSalesData = data.salesDetails;
            this.productRepricingSalesDatalen = 1;
            console.log(this.productRepricingData);
        }, error => {
            console.log(error);
            if (error.errorCode == '404.33.410' || error.httpResponseCode == '404') {
                this.productRepricingSalesDatalen = 0;
                this.showLoader = false;
            } else {
                this._router.navigate(['/error404']);
            }

        });
    }

    familyMemberpopup(idx: number, leadPrd: any) {
        let leadProduct = leadPrd;
        let filterKey = "getFamliyMember";
        console.log("idx = " + idx);
        this._postsService.getFamilyMemberData(this.customerName, this.catalogueType,this.brandType, '@all', filterKey, leadProduct).subscribe(data => {
            this.familyMemberData = data.leadProductDetails;
            this.familyMemberDatalen = 1;
            console.log(this.productRepricingData);
        }, error => {
            console.log(error);
            if (error.errorCode == '404.33.410' || error.httpResponseCode == '404') {
                this.familyMemberDatalen = 0;
                this.showLoader = false;
            } else {
                this._router.navigate(['/error404']);
            }

        });
    }

    /* changeModal(val: any) {
        if (sessionStorage.getItem("unsavedChanges") == 'true') {
            if (val == "CatalogueManagement") {
                sessionStorage.setItem('pageName', 'cataloguemanagement');
            } else if (val == "Home") {
                sessionStorage.setItem('pageName', 'customerselection');
            }
            let sessionModal = document.getElementById('unsaveModal');
            sessionModal.classList.remove('out');
            sessionModal.classList.add('in');
            document.getElementById('unsaveModal').style.display = 'block';
            setTimeout(() => {
            }, this.objConstant.WARNING_TIMEOUT);
        } else {
            if (val == "CatalogueManagement") {
                this._router.navigate(['cataloguemanagement', this.customerName]);
            } else if (val == "Home") {
                this._router.navigate(['customerselection']);
            }

        }
    } */

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } 
                    else{
                    reject('sessionInactive');
                     sessionStorage.clear();
                    this._router.navigate(['']);
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        resolve('sessionActive');
                        this.terminateFlg = false;
                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
                        this.organisationListChange = changeOrgAvailable;
                        this.orgimgUrl = orgImgUrl;
                        this.orgApiName = orgApiNameTemp;
                        // this.disabledCusChange = false;
                        if (this.organisationList.length == 1) {
                            this.disabledCusChange = true;
                        }

                        console.log("this.organisationList234");
                        console.log(this.organisationList);
                        let that = this;
                        let btn_repricingSupplierRequestsReviewSubmit = false;
                        let btn_repricingPeriodicReviewSubmit = false;

                        let permArray = [];
                        let permArrayWholesaleCatalogues: any = [];
                        let permArrayNewRequests: any = [];
                        let permArrayPricing: any = [];
                        let permArrayMainEstateRange: any = [];
                        let repricingPeriodicReview: any

                        this.organisationList.forEach(function (organisation: any) {
                            if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                                console.log("organisation : " + that.customerName);
                                console.log(organisation);
                                organisation.permissionRoleList.forEach(function (permRoleList: any) {
                                    //console.log("11");
                                    //console.log(permRoleList);
                                    permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                        //console.log("22");
                                        if (permRoleDtlList.permissionGroup == 'catalogueManagement' || permRoleDtlList.permissionGroup == 'morrisonsPermissions') {
                                            permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                                /*if(permList.permissionName == 'catalogueManagement' && permList.permissionStatus == 'active'){
                                                    dashboardMenuPermission = true;
                                                }*/
                                                console.log(permList);
                                                switch (permList.permissionName) {
                                                    case "repricingPeriodicReview":
                                                        repricingPeriodicReview = true;
                                                        break;
                                                    case "btn_repricingSupplierRequestsReviewSubmit":
                                                        btn_repricingSupplierRequestsReviewSubmit = true;
                                                        break;
                                                    case "btn_repricingPeriodicReviewSubmit":
                                                        btn_repricingPeriodicReviewSubmit = true;
                                                        break;
                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                        that = null;
                        // if(!dashboardMenuPermission){
                        //     // redirect to order
                        //     this._router.navigate(["orders",customerName]);
                        // }
                        this.btn_repricingSupplierRequestsReviewSubmit = btn_repricingSupplierRequestsReviewSubmit;
                        this.btn_repricingPeriodicReviewSubmit = btn_repricingPeriodicReviewSubmit;
                        this.repricingPeriodicReview = repricingPeriodicReview
                    }

                }
            });
        });
    }

    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }

    /*only show unactioned when checkbox is clicked starts here*/
    getActivateCheckboxForUnactioned(event: any) {
        console.log('event : ' + JSON.stringify(event));
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                if (event.target.checked) {
                    console.log('tick');
                    this.onlyshowUnactionFlag = true;
                    this.searchVal = [this.amountStr, this.increasePStr, 'true'];
                    this.filterValue = 'both';
                } else {
                    console.log('un-tick');
                    this.onlyshowUnactionFlag = false;
                    this.searchVal = [this.amountStr, this.increasePStr, 'false'];
                    this.filterValue = 'both';
                }
            }
        }, reason => {
            console.log(reason);
        });


    }
    /*only show unactioned when checkbox is clicked ends here*/

    onChangeBrand(event: any) {
        this.changedBrand = event.target.value;
        if (sessionStorage.getItem("unsavedChanges") == 'true') {
            this.changeModalPageLevel();
            sessionStorage.setItem("pageLevel",'true');
            sessionStorage.setItem('changedBrandType',this.changedBrand);
            sessionStorage.setItem('previousBrandType',this.previousBrand);
        }else{
            return false;
        }
    }

    changeModalPageLevel() {
        let sessionModal = document.getElementById('unsaveModalPageLevel');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('unsaveModalPageLevel').style.display = 'block';
        setTimeout(() => {
        }, this.objConstant.WARNING_TIMEOUT);
    }

    keepMeonSamePage() {
        let brandType = sessionStorage.getItem('previousBrandType');
        let sessionModal = document.getElementById('unsaveModalPageLevel');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('unsaveModalPageLevel').style.display = 'none';
        document.getElementById(brandType).checked = 'true';
    }

    navigatTopage() {
        let sessionModal = document.getElementById('unsaveModalPageLevel');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('unsaveModalPageLevel').style.display = 'none';
        this.showLoader = false;
        this.previousBrand = this.changedBrand;
    }
}