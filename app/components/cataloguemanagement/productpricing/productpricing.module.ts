import { NgModule } from '@angular/core';
import { ProductPricingRoutes } from './productpricing.routes';
import { ProductPricingComponent } from './productpricing.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from "ngx-tooltip";
import { CalendarModule } from 'primeng/primeng';
import { CatalogueManagePendingHeaderModule } from '../cataloguemanagependingheader/cataloguemanagependingheader.module';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { Zerofilter } from './zerofilter.pipes';

// import { DatepickerModule } from 'ng2-bootstrap';
import { DatepickerModule } from '../../datepicker/ng2-bootstrap';
import { SearchFilterPipe } from './serchfilter.component';
import { FiltermultiPipe } from './filtermulti.pipe';
@NgModule({
  imports: [
    ProductPricingRoutes,
    CommonModule,
    FormsModule,
    DatepickerModule.forRoot(),
    CalendarModule,
    CatalogueManagePendingHeaderModule,
    TooltipModule,
    InfiniteScrollModule
  ],
  exports: [],
  declarations: [ProductPricingComponent, Zerofilter , SearchFilterPipe, FiltermultiPipe],
  providers: [Services, GlobalComponent, commonfunction]
})
export class ProductPricingModule { }
