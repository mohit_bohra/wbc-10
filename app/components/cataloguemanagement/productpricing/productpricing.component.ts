import { Component, ViewChild } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { document } from '../../datepicker/utils/facade/browser';
import { GlobalComponent } from '../../global/global.component';
import { FormBuilder } from '@angular/forms';
import { DialogService } from '../../../services/dialog.service';

@Component({
    templateUrl: 'productpricing.component.html',
    providers: [commonfunction]
})
export class ProductPricingComponent {
    @ViewChild('searchPricing') searchPricing: any;
    objConstant = new Constant();

    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    errorMsg: boolean;
    errorMsgString: string;
    imageUrl: string;
    sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;
    productPriceId: any;
    pricingData: any;
    productPriceData: any;
    productPriceDatalen: any;
    printHeader: boolean;
    productPricepopupData: any;
    productPriceDatapoplen: any;
    productPatchData: any[];
    productPriceRowIndex: number;
    updatedProductPricingRows: number[];
    errorMsgPopup: any;
    updatedArr: any[];
    upPriceId: any;
    finalArray4Patch: any[];
    showResult: any;
    hideRepeat: any;
    productPricePrint: any = [];
    searchValArr: any = [{'pricingRepeat':'NO'},{'affectedCount':''},{'procuredPricingStatus':'unaction'}]
    msgJSON: any = {};
    terminateMsg: any;
    terminateFlg: boolean = false;
    filterValue: string = '';
    searchVal: string = 'unaction';

    showLoader: boolean;
    start: number;
    limit: number;
    count: number;
    noDataFound: boolean;

    popupDtlData: any[];

    searchResultData: any[];
    searchResultDataLength: number = 0;

    hideOnLanding: boolean = true;
    pageState: number = 22;
    saveStatus: boolean = false;
    disablerepricingProductsIdentification: boolean = true;
    assignedPermissions: any = [];

    permissionArray: any = [];
    hasAccess: boolean = false;
    noPermissionMsg: string = "";
    imgName: string = "";
    oldproductRepricingData: any = [];
    counter: number = 0;
    supplier:any;
    category:any;
    unsavedChange: boolean = false;

    constructor(private _postsService: Services, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, private _globalFunc: GlobalComponent, private formBuilder: FormBuilder, public dialogService: DialogService) {
        console.log("inside init");
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        sessionStorage.setItem('terminateFlg', 'false');
        // set default date null to calender
        this.errorMsgString = '';
        this.errorMsg = false;
        this.productPriceData = [{ 'supplier': '', 'category': '' }];
        this.productPriceDatalen = 0;
        this.productPriceRowIndex = 0;
        this.updatedProductPricingRows = [];
        this.updatedArr = [];
        this.upPriceId = 0;
        this.finalArray4Patch = [];
        this.showResult = 'All';
        this.hideRepeat = 'NO';
        this.showLoader = false;
        this.limit = 15;
        this.start = 0;
        this.count = 30;
        this.popupDtlData = [];
        this.searchResultData = [];
        this.productPricepopupData = [];
        this.noDataFound = false;
        this.searchPricing = this.formBuilder.group({})
    }

    canDeactivate(): Promise<boolean> | boolean {
        if (!this.unsavedChange || sessionStorage.getItem('unsavedChanges')!=='true') {
            return true;
        }
        return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
    }

    ngOnInit() {
        //this.pricingData = pricingData;
        this.imageUrl = this.whoImgUrl;
        this.printHeader = true;
        this.errorMsgPopup = "";
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

        sessionStorage.setItem('pageState', '22');
        this.sub = this._route.params.subscribe((params: { orgType: string }) => {

            let orgImgUrl: any;
            let cusType = params.orgType;
            this.customerName = params.orgType;
            let orgApiNameTemp;
            //this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));


            /* this.organisationList.forEach(function (item: any) {
                 if (item.orgType === cusType) {
                     // logic for permission set
                     orgImgUrl = item.imgUrl;
                     orgApiNameTemp = item.orgName;
                 }
             });*/
            this.orgimgUrl = orgImgUrl;
            this.orgApiName = orgApiNameTemp;
            this.orgApiName = this.customerName;


            this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
            this.imgName = this.getCustomerImage(params.orgType);

            this.getProductPrice();
            this.getProductPricePrintDownload();
        });
    }

    getActivateCheckboxForUnactioned(event: any) {
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then((res) => {
            if (res == 'sessionActive') {

                event = event || window.event;
                this.filterValue = 'procuredPricingStatus';
                if (event.target.checked) {
                    this.searchVal = 'unaction';
                } else {
                    this.searchVal = 'ALL';
                }
                this.searchValArr = [{'pricingRepeat':this.hideRepeat},{'affectedCount':this.showResult},{'procuredPricingStatus':this.searchVal}];
            }
        }, reason => {
            console.log(reason);
        });
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    getActivateCheckbox(indexvalue: any, value: any, eleName: string) {
        //this._cfunction.getActivateCheckbox(index, value, eleName);
        console.log(this.productPriceRowIndex);
        console.info(indexvalue);
        if (value == 'remove') {
            // remove row
            this.productPricepopupData[indexvalue].itemStatus = 'N';
            --this.productPriceData[this.productPriceRowIndex].affectedCount;
            this.updatedArr[this.upPriceId] = this.productPricepopupData;

            //this.productPricepopupData.splice(indexvalue, 1);
            console.log(this.productPriceData);
            console.info("remove row");

        } else if (value == 'add') {
            // remove row
            //this.productPricepopupData[indexvalue].itemStatus = 'Y';
            var tempVar = false;
            var that = this;
            var tempAllreadyExistWithNoStatusId = -1;
            this.productPricepopupData.forEach(function (row: any, idx: number) {
                if (row.vendorSKU == that.searchResultData[indexvalue].lpm && row.itemStatus == 'Y') {
                    tempVar = true;
                } else if (row.vendorSKU == that.searchResultData[indexvalue].lpm && row.itemStatus == 'N') {
                    tempAllreadyExistWithNoStatusId = idx;
                }
            })
            that = null;

            if (tempAllreadyExistWithNoStatusId > -1) {
                this.productPricepopupData[tempAllreadyExistWithNoStatusId].itemStatus = 'Y';
                ++this.productPriceData[this.productPriceRowIndex].affectedCount;
                this.updatedArr[this.upPriceId] = this.productPricepopupData;
            } else {
                if (!tempVar) {

                    var proposedCost = 0;
                    if (this.productPriceData[this.productPriceRowIndex].increaseAgreed != null && this.productPriceData[this.productPriceRowIndex].increaseAgreed != '') {
                        proposedCost = this.searchResultData[indexvalue].costprice * (1 + this.productPriceData[this.productPriceRowIndex].increaseAgreed);
                    } else if (this.productPriceData[this.productPriceRowIndex].increaseRequested != null && this.productPriceData[this.productPriceRowIndex].increaseRequested != '') {
                        proposedCost = this.searchResultData[indexvalue].costprice * (1 + this.productPriceData[this.productPriceRowIndex].increaseAgreed);
                    }
                    console.log("proposedCost ::" + proposedCost);
                    ++this.productPriceData[this.productPriceRowIndex].affectedCount; // update affected count
                    this.productPricepopupData.push({
                        'vendorSKU': this.searchResultData[indexvalue].lpm,
                        'itemDesc': this.searchResultData[indexvalue].itemDescription, 'itemStatus': 'Y', 'cost_price': this.searchResultData[indexvalue].costprice,
                        'proposed_cost': proposedCost
                    });
                    this.updatedArr[this.upPriceId] = this.productPricepopupData;
                }
            }
            this.errorMsgPopup = "";

            //this.searchResultData[indexvalue]

            //this.productPricepopupData.splice(indexvalue, 1);
            console.log(this.productPricepopupData);
            console.info("add row");
        } else if (value == 'green') {
            this.oldproductRepricingData = JSON.parse(sessionStorage.getItem('filterval'));
            if (this.oldproductRepricingData[indexvalue].procuredPricingStatus != 'Y') {
                this.unsavedChange = true;
                sessionStorage.setItem("unsavedChanges", "true");
                this.counter += 1;
            } else {
                if (this.counter > 1) {
                    this.unsavedChange = true;
                    sessionStorage.setItem("unsavedChanges", "true");
                    this.counter -= 1;
                } else {
                    this.unsavedChange = false;
                    sessionStorage.setItem("unsavedChanges", "false");
                    this.counter -= 1;
                }
            }
            // accept row
            this.finalArray4Patch[indexvalue] = "Y";
        } else if (value == 'red') {
            
            this.oldproductRepricingData = JSON.parse(sessionStorage.getItem('filterval'));
            if (this.oldproductRepricingData[indexvalue].procuredPricingStatus != 'N') {
                this.unsavedChange = true;
                sessionStorage.setItem("unsavedChanges", "true");
                this.counter += 1;
            } else {
                if (this.counter > 1) {
                    this.unsavedChange = true;
                    sessionStorage.setItem("unsavedChanges", "true");
                    this.counter -= 1;
                } else {
                    this.unsavedChange = false;
                    sessionStorage.setItem("unsavedChanges", "false");
                    this.counter -= 1;
                }
            }
            // reject row
            this.finalArray4Patch[indexvalue] = "N";
        }

        if (this.updatedProductPricingRows.length == 0)
            this.updatedProductPricingRows.push(this.productPriceRowIndex);
        else if (this.updatedProductPricingRows.indexOf(this.productPriceRowIndex) == -1)
            this.updatedProductPricingRows.push(this.productPriceRowIndex);
    }

    getActivateCheckbox1(value: any) {



        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then((res) => {
            if (res == 'sessionActive') {
                if (this.showResult == 'All') {
                    this.showResult = '0';
                } else {
                    this.showResult = 'All';
                }
                this.searchValArr = [{'pricingRepeat':this.hideRepeat},{'affectedCount':this.showResult},{'procuredPricingStatus':this.searchVal}]
            } else {
                alert("adfasdfadfasfadf");
            }
        }, reason => {
            console.log(reason);
        });

    }
    getActivateCheckboxForHide(value: any) {
        console.log("value : "+value);
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {


                if (this.hideRepeat == 'NO') {
                    this.hideRepeat = 'YES';
                }
                else {
                    this.hideRepeat = 'NO';
                }
                this.searchValArr = [{'pricingRepeat':this.hideRepeat},{'affectedCount':this.showResult},{'procuredPricingStatus':this.searchVal}]
            }
        }, reason => {
            console.log(reason);
        });
    }


    getFieldVal(val: any) {
        this.filterValue = 'procuredPricingStatus';
        if (val == 'unaction') {
            this.searchVal = 'unaction';
        } else {
            this.searchVal = 'ALL';
        }
    }

    getProductPrice() {
        this.upPriceId = this.productPriceId;
        this.showLoader = true;
        console.log(this.productPriceRowIndex+"this.productPriceRowIndex")
        console.log("this.productPriceId : " + this.productPriceId);
        this._postsService.getProductPrice(this.orgApiName, this.productPriceId, this.start).subscribe(data => {
            this.errorMsgPopup = "";
            
            if (this.productPriceId != null) {
                console.log("this.productPriceId when not null (For Popup) : " + this.productPriceId);
                this.productPricepopupData = data.pricingHeaders[0].pricingDetails;
                this.popupDtlData = data.pricingHeaders[0];
                console.log(this.productPricepopupData);
              /*  this.supplier= data.pricingHeaders[0].supplier;
                this.category = data.pricingHeaders[0].category;*/
                this.productPriceId = null;
                this.searchResultData = [];
                this.searchResultDataLength = 0;
            } else {
                console.log("this.productPriceId when null : " + this.productPriceId);
                //console.log(data);
                this.productPriceData = data.pricingHeaders;
                let newTempArr:any = [];
                this.productPriceData.forEach(function(dt:any, idx:number){
                    dt.idx = idx;
                    newTempArr.push(dt);
                })
                this.productPriceData = newTempArr;

                this.productPriceDatalen = data.pricingHeaders.length;
                this.count = data.paginationMetaData.count;
                //console.log(data);
                console.log(this.productPriceData);
            }

            // For button:: count = 0 : Disable; count > 0 : Enable  button
            this.disablerepricingProductsIdentification = data.procuredPricingStatusCount == 0 ? true : false;
            //this.getPricingStatus();
            console.log("Data");
            console.log(data);
            
            this.getFieldVal(this.searchVal);
            this.showLoader = false;

        }, error => {
            console.log(error);
            this.showLoader = false;
            if (this.productPriceId != null) {
                this.errorMsgPopup = error.errorMessage;
                this.productPricepopupData = [];
            } else {
                this.productPriceDatalen = 0;
                this.productPriceDatapoplen = 0;
            }
            // do some code
            //alert(error);
        });
    }

    getPricingStatus() {
        console.log("Inside getPricingStatus Functi");
        this._postsService.getPricingStatus(this.customerName, "").subscribe(data => {
            // this.repricingStatus = data.productRepricingStatus;
            // this.configurationstatus = data.pricingConfigStatus;            
            if (data.count == 0) {
                // Disable save button
                this.saveStatus = true;
            } else {
                // Enable save button
                this.saveStatus = false;
            }
        });
    }

    // print functionality
    printOrder() {
        //this.printHeader = false;
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        var tableData = document.getElementById('hiddenDivForExport').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + tableData + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + tableData + '</body></html>');
            popup.document.close();
        }
    }

    getProductPricePrintDownload() {
        this._postsService.getProductPricePrintDownload(this.orgApiName).subscribe(data => {

            //     if (data == undefined) {
            //         //this.supplychainDatatLength = 0;
            //         this.productPricePrint = [];
            //         console.log(data);
            //     } else {
            //         this.productPricePrint = data.requestHeaders[0].requestDetails;                                
            //     }
            // }, error => {
            //     if (error.errorCode === '404.33.402' || error.httpResponseCode === 404) {
            //         //alert('nodata');
            //         this.noDataFound = true;
            //     } else {
            //         // alert("404");
            //         this._router.navigate(['/error404']);
            //     }


            this.productPricePrint = data.pricingHeaders;
            console.log("printdata");
            console.log(this.productPricePrint);
        });
    }

    exportData() {
        return this._cfunction.exportData('hiddenDivForExport', this.customerName + 'ProductIdentification');
    }

    productPricingPopup(idx: any, index: number) {

         if (this.showResult == 'All') {
                    this.showResult = '0';
                } else {
                    this.showResult = 'All';
                }
        this.hideOnLanding = true;
        console.log(this.hideOnLanding);
        this.productPriceId = idx;
        this.productPriceRowIndex = index;
         console.log(this.productPriceRowIndex+"this.productPriceRowIndex1" )

        console.log(this.updatedArr[this.productPriceId]);
        this.searchResultData = [];
        this.searchResultDataLength = 0;
        if (typeof this.updatedArr[this.productPriceId] === "undefined") {
            console.info("productPriceId undefined");
            this.getProductPrice();
        } else {
            this.productPricepopupData = this.updatedArr[this.productPriceId];
            console.info("productPriceId defined");
            //this.popupDtlData=data.pricingHeaders[0];
            console.log(this.productPricepopupData);
            this.productPriceId = null;
        }

    }

    async sleep(ms: number) {
        await this._sleep(ms);
    }

    _sleep(ms: number) {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }

    updateProductPricing() {
        this.unsavedChange = false;
        sessionStorage.setItem('unsavedChanges','false');
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                this.updatedProductPricingRows;
                let finalArr: any = [];
                console.log(this.productPriceData);
                if(sessionStorage.getItem('filterval') != undefined){
                    this.productPriceData = JSON.parse(sessionStorage.getItem('filterval'));
                }
                var dummyArr = this.productPriceData;
                // var pricingDtlArr = [];
                console.info("update noq");
                console.log(this.updatedArr);
                console.info("final added rows");
                console.log(this.finalArray4Patch);
                console.info("final added rows - END");
                var that = this;
                this.finalArray4Patch.forEach(function (val, rowIdx) {
                    if (typeof that.updatedArr[dummyArr[rowIdx].pricingId] !== undefined) {
                        finalArr.push({ "pricingId": dummyArr[rowIdx].pricingId, "procuredPricingStatus": val, "pricingDetails": that.updatedArr[dummyArr[rowIdx].pricingId] });
                    } else {
                        finalArr.push({ "pricingId": dummyArr[rowIdx].pricingId, "procuredPricingStatus": val, "pricingDetails": Array() });
                    }
                });
                this.finalArray4Patch = [];
                console.log("Final Array");
                console.log(finalArr);

                var prodString: string = "";
                prodString = '{"pricingHeaders":' + JSON.stringify(finalArr) + '}';

                console.log(prodString);
                this.showLoader = true;
                this._postsService.updateProductPricingPatch(this.orgApiName, prodString).subscribe(data => {
                    this.showLoader = false;
                    //alert("Update Done");
                    console.log(data);
                    this.updatedArr = [];
                     this.start = 0;
                    this.getProductPrice();
                    let sessionModal = document.getElementById('PricingUpdateSuccess');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('PricingUpdateSuccess').style.display = 'block';
                   
                    document.getElementById('main-table-container').scrollTop = 0;
                    // this.getProductPricePrintDownload();
                }
                    , err => {
                        console.log(err);
                        // this.showLoader = false;
                        this._router.navigate(['/error404']);
                    }
                );
            }

        }, reason => {
            console.log(reason);
        });
    }

    closePricingUpdateSuccess() {
        // this.showLoader = false;
        
        this.getFieldVal(this.searchVal);
        let sessionModal = document.getElementById('PricingUpdateSuccess');
        sessionModal.classList.add('in');
        sessionModal.classList.remove('out');
        document.getElementById('PricingUpdateSuccess').style.display = 'none';
    }

    onScrollDown() {

        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            console.log("scroll" + this.count);
            this.start = this.start + this.limit;
            if (this.start < this.count - 1) {
                this.showLoader = true;
                this._postsService.getProductPrice(this.orgApiName, null, this.start).subscribe(data => {
                    this.showLoader = false;
                    this.errorMsgPopup = "";

                    this.productPriceData = this.productPriceData.concat(data.pricingHeaders);
                    //this.productPriceData = data.pricingHeaders;
                    let newTempArr:any = [];
                    this.productPriceData.forEach(function(dt:any, idx:number){
                        dt.idx = idx;
                        newTempArr.push(dt);
                    })
                    this.productPriceDatalen = this.productPriceData.length;
                    //this.count = data.paginationMetaData.count;
                    console.log(data);
                    console.log(this.productPriceData);

                    console.log("Data");
                    console.log(data);

                }, error => {
                    console.log(error);
                    /*if(this.productPriceId != null){
                        this.errorMsgPopup = error.errorMessage;
                        this.productPricepopupData = [];
                    } else {
                        this.productPriceDatalen = 0;
                        this.productPriceDatapoplen = 0;
                    }*/
                    // do some code
                    //alert(error);

                });
            }
        });
    }

    displaySearchData(value: any) {
        this.hideOnLanding = false;
        console.log(value);
        this._postsService.displaySearchData(this.orgApiName, value.manList, value.deptList).subscribe(data => {
            console.log(data);
            if (typeof data.items != undefined) {
                this.searchResultData = data.items; // Commented on 7th Dec
            } else {
                this.searchResultData = data;
            }

            if (this.searchResultData != null) {
                this.searchResultDataLength = this.searchResultData.length;
            } else {
                this.searchResultDataLength = 0;
            }
            console.log("Length : " + this.searchResultDataLength);
        }, error => {
            console.log(error);
        });
    }


    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {
            console.log("i m first");
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.
                   
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        console.log("Session terminated");
                        this.terminateFlg = true;
                        sessionStorage.setItem('terminateFlg', 'true');
                        reject('sessionInactive');

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else{
                        sessionStorage.clear();
                       this._router.navigate(['']);
                       reject('sessionInactive');
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        console.log("Session terminated");
                        this.terminateFlg = true;
                        sessionStorage.setItem('terminateFlg', 'true');
                        reject('sessionInactive');

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        sessionStorage.setItem('terminateFlg', 'false');

                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });

                        console.log("this.organisationList234");
                        console.log(this.organisationList);
                        let that = this;
                        let repricingProductsIdentification = true;
                        let catalogueManagementMenuPermission = false;
                        let userHavingPermission = false;

                        this.organisationList.forEach(function (organisation: any) {
                            if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                                console.log("organisation : " + that.customerName);
                                console.log(organisation);
                                organisation.permissionRoleList.forEach(function (permRoleList: any) {
                                    //console.log("11");
                                    //console.log(permRoleList);
                                    permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                        //console.log("22");
                                        that.setPermissionsArray(permRoleDtlList.permissionGroup);
                                        if (permRoleDtlList.permissionGroup == 'catalogueManagement' || permRoleDtlList.permissionGroup == 'morrisonsPermissions') {
                                            permRoleDtlList.permissionNameList.forEach(function (permList: any) {

                                                userHavingPermission = true;
                                                if (permList.permissionName == 'catalogueManagement' && permList.permissionStatus == 'active') {
                                                    catalogueManagementMenuPermission = true;
                                                }
                                                console.log(permList);
                                                switch (permList.permissionName) {
                                                    case "productIdentification":
                                                        //console.log("55");
                                                        repricingProductsIdentification = false;
                                                        //alert(filfilment);
                                                        break;
                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                        that = null;
                        if (!userHavingPermission) {
                            // redirect to order
                            // catalogue/mccolls/upload
                            this._router.navigate(["cataloguemanagement", customerName]);
                        }
                        //this.disablerepricingProductsIdentification = repricingProductsIdentification;
                        resolve('sessionActive');
                    }
                }
                console.log("this.permissionArray");
                //alert(this.disabledfilfilment);
            }, (error: any) => {
                if (error.errorCode == '503.65.001') {
                    this._router.navigate(['/error404']);
                }
            });
        });
    }


    setPermissionsArray(permissionGroup: string) {
        this.assignedPermissions.push(permissionGroup);
    }

    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }
}
