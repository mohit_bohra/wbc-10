import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductPricingComponent } from './productpricing.component';
import { CanDeactivateGuard } from '../../../services/can-deactivate-guard.service';
//import { ProductRepricingComponent } from './productrepricing/productrepricing.component';

// route Configuration
const productpricingRoutes: Routes = [
  { path: '', component: ProductPricingComponent, canDeactivate: [CanDeactivateGuard] },
  { path: 'repricing', loadChildren:'./productrepricing/productrepricing.module#ProductRepricingModule'},
  { path: 'prodicrepricing', loadChildren:'./prdicprodrepricing/prdicprodrepricing.module#PrdicprodrepricingModule'},
  { path: 'configuration', loadChildren:'./productconfiguration/productconfiguration.module#ProductConfigurationModule'}
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(productpricingRoutes)],
  exports: [RouterModule]
})
export class ProductPricingRoutes { }
