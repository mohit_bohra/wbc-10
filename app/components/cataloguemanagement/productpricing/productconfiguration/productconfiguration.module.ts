import { NgModule } from '@angular/core';
import { ProductConfigurationRoutes } from './productconfiguration.routes';
import { ProductConfigurationComponent } from './productconfiguration.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../../global/global.component';
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from "ngx-tooltip";
import { CalendarModule } from 'primeng/primeng';
import { CatalogueManagePendingHeaderModule } from '../../cataloguemanagependingheader/cataloguemanagependingheader.module';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { MaxNumberModule } from '../../../../directives/maxnumber/maxnumber.module';
import { OnlyNumberModule } from '../../../../directives/onlynumber/onlynumber.module';



// import { DatepickerModule } from 'ng2-bootstrap';
import { DatepickerModule } from '../../../datepicker/ng2-bootstrap';
@NgModule({
  imports: [
    ProductConfigurationRoutes,
    CommonModule,
    FormsModule,
    DatepickerModule.forRoot(),
    CalendarModule,
    CatalogueManagePendingHeaderModule,
    TooltipModule,
    InfiniteScrollModule,
    MaxNumberModule,
    OnlyNumberModule
  ],
  exports: [],
  declarations: [ProductConfigurationComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class ProductConfigurationModule { }
