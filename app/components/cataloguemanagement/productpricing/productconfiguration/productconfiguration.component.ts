import { Component, ViewChild } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { GlobalComponent } from '../../../global/global.component';
import { Constant } from '../../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { document } from '../../../datepicker/utils/facade/browser';
import { FormBuilder } from '@angular/forms';
import { DialogService } from '../../../../services/dialog.service';

declare let saveAs: any;

@Component({
    templateUrl: 'productconfiguration.component.html',
    providers: [commonfunction]
})
export class ProductConfigurationComponent {
    @ViewChild('productConfig') productConfig: any;
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;
    showLoader: boolean;
    productRepricingData: any = [];
    pageState: number = 26;
    tradingGroupsData: any = [];
    catalogueTypes: any;
    catalogueType: string;
    catalogueType4Print: string;
    cataloguemasterListLength: any;
    cataloguedetailListLength: any;
    catalogueDetailList: any;
    tradingGroupsDataLength: number = 0;
    printHeader: boolean;
    disableLock: boolean = true;
    disableSave: boolean = false;
    successMsg: string = "";
    calculatePercentage: number = 100;
    configCommonStatusDraft: string = 'draft';
    configCommonStatusLock: string = 'lock';
    configCommonStatusLockSubmitted: string = 'locksubmitted';
    readonlyText: boolean = false;
    imgName: string = "";
    terminateMsg: any;
    terminateFlg: boolean = false;
    assignedPermissions: any = [];
    pricingConfiguration: boolean = false;
    unsavedChange: boolean = false;


    //Constructor is used to initialize variables during load time
    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, private formBuilder: FormBuilder, public dialogService: DialogService) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        //this.configurationSearch = this.formBuilder.group({});
        this.productConfig = this.formBuilder.group({})
    }

    canDeactivate(): Promise<boolean> | boolean {
        if (!this.unsavedChange || sessionStorage.getItem('unsavedChanges')!=='true') {
            return true;
        }
        return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
    }

    // This method is called on page load.
    // Hence all the basic cosmetic related elements are initialized here
    ngOnInit() {
        this.imageUrl = this.whoImgUrl;
        this.printHeader = true;

        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

        sessionStorage.setItem('pageState', '26');

        this.sub = this._route.params.subscribe((params: { orgType: string }) => {
            sessionStorage.setItem('orgType', params.orgType);
            let orgImgUrl: any;
            let cusType = params.orgType;
            this.customerName = params.orgType;
            let orgApiNameTemp;
            /* this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));
 
             this.organisationList.forEach(function (item: any) {
                 if (item.orgType === cusType) {
                     // logic for permission set
                     orgImgUrl = item.imgUrl;
                     orgApiNameTemp = item.orgName;
                 }
             });*/
            this.orgimgUrl = orgImgUrl;
            this.orgApiName = orgApiNameTemp;
            this.orgApiName = this.customerName;
            this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
            this.imgName = this.getCustomerImage(params.orgType);

            //this.getTradingGroups();
            this.getAllMasterData();
        });

    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    // On HTML, there is a dropdown, all the data of this dropdown comes from this method
    // In this method we call a method from service class (app.service)
    // We store the data in a variable and then the same variable is used in HTML. In this case, the variable is 'catalogueTypes'
    getAllMasterData() {
        this.showLoader = true;
        this._postsService.getAllMasterData(this.orgApiName).subscribe(data => {
            this.showLoader = false;
            if (data.catalogueTypes.length == 0) {
                this.cataloguemasterListLength = 0;
            } else {
                for (var k = 0; k < data.catalogueTypes.length; k++) {
                    if (data.catalogueTypes[k].catalogueType.indexOf(':') > 0) {
                        data.catalogueTypes[k].catalogueType = data.catalogueTypes[k].catalogueType.split(':')[1];
                    }
                }
                this.cataloguemasterListLength = data.catalogueTypes.length;
            }
            this.catalogueTypes = data.catalogueTypes;
        }
            , err => {
                this.cataloguemasterListLength = 0;
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
        );
    }

    // All the line items under Trading Groups column in table in the HTML page, comes from here
    // In this method we call a method from service class (app.service)
    // We store the data in a variable and then the same variable is used in HTML. In this case, the variable is 'tradingGroupsData'
    getTradingGroups(catalogueType: string) {
        this.showLoader = true;
        this.catalogueType = catalogueType;
        this.catalogueType4Print = catalogueType;
        //console.log(this.catalogueType);
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
        this._postsService.getTradingGroups(this.orgApiName, catalogueType).subscribe(data => {
            this.showLoader = false;
            this.tradingGroupsData = data;
            if (!this.tradingGroupsData.configCommon.status) {
                this.tradingGroupsData.configCommon.status = this.configCommonStatusDraft;
                this.disableLock = true;
                this.disableSave = false;
            }

            //Get length of priceconfigs array.
            this.tradingGroupsDataLength = data.priceConfigs.length;

            if (this.tradingGroupsData.configCommon.status == this.configCommonStatusDraft) {
                console.log(this.tradingGroupsData.configCommon.status);
                this.disableSave = false;
                this.readonlyText = false;
                this.checkButtonStatus();
            } else if (this.tradingGroupsData.configCommon.status == this.configCommonStatusLock || this.tradingGroupsData.configCommon.status == this.configCommonStatusLockSubmitted || this.tradingGroupsData.configCommon.status == 'RepricingSubmitted') {
                this.disableLock = true;
                this.disableSave = true;
                this.readonlyText = true;
            }
            //console.log(this.tradingGroupsData);
        });
    }

    savePricingConfigurations(status: string = this.configCommonStatusDraft) {
        //console.log(this.tradingGroupsData);
        this.unsavedChange = false;
        sessionStorage.setItem('unsavedChanges','false');
        this.showLoader = true;
        let tempJSON = this.tradingGroupsData;
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if(res = 'sessionActive'){
                
            this.tradingGroupsData.priceConfigs.forEach(function (itm: any, idx: number) {
                if (itm.priceConfig.challengeMaxTol != null && itm.priceConfig.challengeMaxTol >= 0 &&
                    itm.priceConfig.marginBranded != null && itm.priceConfig.marginBranded >= 0 &&
                    itm.priceConfig.marginOwnBranded != null && itm.priceConfig.marginOwnBranded >= 0) {
                    tempJSON.priceConfigs[idx].priceConfig.status = 'Y';
                } else {
                    tempJSON.priceConfigs[idx].priceConfig.status = 'N';
                }
            });
    
            this.tradingGroupsData = tempJSON;
            this.tradingGroupsData.configCommon.status = status;
            //console.log(this.tradingGroupsData);
            tempJSON = null;
    
            this._postsService.savePricingConfigurations(this.orgApiName, this.tradingGroupsData, this.catalogueType).subscribe(data => {
                this.showLoader = false;
                if (status == this.configCommonStatusLock) {
                    this.successMsg = "Pricing configuration locked successfully.";
                } else {
                    this.successMsg = "Pricing configuration saved successfully.";
                }
    
                let sessionModal = document.getElementById('PricingConfigSave');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('PricingConfigSave').style.display = 'block';
            }
                , error => {
                    //console.log("error - Start");
                    //console.log(error);
                    //console.log("error - End");
                    this.showLoader = false;
                    //this._router.navigate(['/error404']);
                    if (error.errorCode == '404.33.407') {
                        //console.log("error - End - 1");
    
                        if (status == this.configCommonStatusLock) {
                            //console.log("error - End - 2");
                            let sessionModal = document.getElementById('PricingConfigSave');
                            sessionModal.classList.remove('in');
                            sessionModal.classList.add('out');
                            document.getElementById('PricingConfigSave').style.display = 'block';
                            this.successMsg = error.errorMessage;
                            this.tradingGroupsData.configCommon.status = 'N';
                            this.disableLock = false;
                        } else {
                            //console.log("error - End - 3");
                        }
                    } else {
                        //console.log("error - End - 4");
                    }
    
                }
            );
            }
        }, reason =>{
            console.log(reason);
        });
    }

    calculatedValue(idx: number) {
        let calculatePercentage = this.calculatePercentage;
        /* this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeCalculation = (this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied != 0 && this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied != "" )?(this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied*this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol) : (this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied*this.tradingGroupsData.configCommon.defaultOfMaxTotal); */
        let defaultVal: number = this.tradingGroupsData.configCommon.defaultOfMaxTotal;// && this.tradingGroupsData.configCommon.defaultOfMaxTotal > 0 ? this.tradingGroupsData.configCommon.defaultOfMaxTotal : 0;
        let maxTol: number = this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol;// && this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol > 0 ? this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol : 0;
        let maxTolApplied: number = this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied; // && this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied > 0 ? this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied : 0;
        console.log(maxTol);

        if (this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied == "") {
            console.log("456");
            this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeCalculation = this.calculateValueWithDefault(idx);
        } else if (!this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied) {
            console.log("789");
            this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeCalculation = this.calculateValueWithDefault(idx);
        } else if (this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied == null) {
            console.log("123aaa");
            this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeCalculation = this.calculateValueWithDefault(idx);
        } else {
            this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeCalculation = this.calculateValueWithPerApplied(idx);
        }
        console.log("this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeCalculation : " + this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeCalculation);
        /*//console.log("Test : "+this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeCalculation);
        if(this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol && this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol != "" && 
        this.tradingGroupsData.priceConfigs[idx].priceConfig.marginBranded && this.tradingGroupsData.priceConfigs[idx].priceConfig.marginBranded != "" && 
        this.tradingGroupsData.priceConfigs[idx].priceConfig.marginOwnBranded && this.tradingGroupsData.priceConfigs[idx].priceConfig.marginOwnBranded != "" ){
            this.tradingGroupsData.priceConfigs[idx].priceConfig.status='Y';
        } else {
            this.tradingGroupsData.priceConfigs[idx].priceConfig.status='N';
        }*/
        //console.log(this.tradingGroupsData.priceConfigs[idx]);
        this.checkButtonStatus();
    }

    calculateAllValues() {
        //console.log("Default : " + this.tradingGroupsData.configCommon.defaultOfMaxTotal);
        let calculatePercentage = this.calculatePercentage;
        let defaultVal: number;
        let maxTol: number = 0;
        let maxTolApplied: number = 0;
        defaultVal = this.tradingGroupsData.configCommon.defaultOfMaxTotal && this.tradingGroupsData.configCommon.defaultOfMaxTotal > 0 ? this.tradingGroupsData.configCommon.defaultOfMaxTotal : 0;
        for (let idx = 0; idx < this.tradingGroupsData.priceConfigs.length; idx++) {
            maxTol = this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol;// && this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol > 0 ? this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol : 0;
            maxTolApplied = this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied;// && this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied > 0 ? this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied : 0;
            //console.log(idx);



            if (this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied == "") {
                console.log("456");
                this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeCalculation = this.calculateValueWithDefault(idx);
            } else if (!this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied) {
                console.log("789");
                this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeCalculation = this.calculateValueWithDefault(idx);
            } else if (this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied == null) {
                console.log("123aaa");
                this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeCalculation = this.calculateValueWithDefault(idx);
            }

            /* if (this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeCalculation == 'NaN') {
                 this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeCalculation = null;
         }*/

            /*//console.log("Test : "+this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeCalculation);
            if(this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol && this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol != "" && 
            this.tradingGroupsData.priceConfigs[idx].priceConfig.marginBranded && this.tradingGroupsData.priceConfigs[idx].priceConfig.marginBranded != "" && 
            this.tradingGroupsData.priceConfigs[idx].priceConfig.marginOwnBranded && this.tradingGroupsData.priceConfigs[idx].priceConfig.marginOwnBranded != "" ){
                this.tradingGroupsData.priceConfigs[idx].priceConfig.status='Y';
            } else {
                this.tradingGroupsData.priceConfigs[idx].priceConfig.status='N';
            }*/
        }
        this.checkButtonStatus();
        //console.log(this.tradingGroupsData);
    }

    calculateValueWithPerApplied(idx: number) {
        if (this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol >= 0 && this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol.toString().trim() != "" && this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol != null) {
            let calc = (this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol * this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTotalToBeApplied / this.calculatePercentage);
            if (isNaN(calc)) {
                return null;
            } else {
                return calc.toFixed(2);
            }
        } else {
            return null;
        }
    }

    calculateValueWithDefault(idx: number) {
        if (this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol >= 0 && this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol.toString().trim() != "" && this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol != null &&
            this.tradingGroupsData.configCommon.defaultOfMaxTotal >= 0 && this.tradingGroupsData.configCommon.defaultOfMaxTotal.toString().trim() != "" && this.tradingGroupsData.configCommon.defaultOfMaxTotal != null) {
            let calc = (this.tradingGroupsData.priceConfigs[idx].priceConfig.challengeMaxTol * this.tradingGroupsData.configCommon.defaultOfMaxTotal / this.calculatePercentage);
            if (isNaN(calc)) {
                return null;
            } else {
                return calc.toFixed(2);
            }
        } else {
            return null;
        }
    }

    checkButtonStatus() {
        console.log(this.tradingGroupsData.configCommon.deliveryChargeCaseBarnd + " : " + this.tradingGroupsData.configCommon.deliveryChargeCaseOwnBarnd + " : " + this.tradingGroupsData.configCommon.defaultOfMaxTotal + " : ");
        if (this.tradingGroupsData.configCommon.deliveryChargeCaseBarnd >= 0 && this.tradingGroupsData.configCommon.deliveryChargeCaseBarnd.toString().trim() != "" && this.tradingGroupsData.configCommon.deliveryChargeCaseBarnd != null &&
            this.tradingGroupsData.configCommon.deliveryChargeCaseOwnBarnd >= 0 && this.tradingGroupsData.configCommon.deliveryChargeCaseOwnBarnd.toString().trim() != "" && this.tradingGroupsData.configCommon.deliveryChargeCaseOwnBarnd != null &&
            this.tradingGroupsData.configCommon.defaultOfMaxTotal >= 0 && this.tradingGroupsData.configCommon.defaultOfMaxTotal.toString().trim() != "" && this.tradingGroupsData.configCommon.defaultOfMaxTotal != null) {
            this.disableLock = false;
            let tempArr: any = [];
            let itm: any;
            /*for (let idx = 0; idx < this.tradingGroupsData.priceConfigs.length; idx++) {
                itm = this.tradingGroupsData.priceConfigs[idx];*/
            let that = this;
            this.tradingGroupsData.priceConfigs.forEach(function (itm: any, idx: number) {
                if (itm.priceConfig.challengeMaxTol == undefined || itm.priceConfig.marginBranded == undefined || itm.priceConfig.marginOwnBranded == undefined) {
                    console.log("Check status 1");
                    that.disableLock = true;
                } else {
                    if (itm.priceConfig.challengeMaxTol.toString().trim() != "" && itm.priceConfig.challengeMaxTol != null && itm.priceConfig.challengeMaxTol >= 0 &&
                        itm.priceConfig.marginBranded.toString().trim() != "" && itm.priceConfig.marginBranded != null && itm.priceConfig.marginBranded >= 0 &&
                        itm.priceConfig.marginOwnBranded.toString().trim() != "" && itm.priceConfig.marginOwnBranded != null && itm.priceConfig.marginOwnBranded >= 0) {
                        console.log("Check status 2");
                    } else {
                        that.disableLock = true;
                        console.log("Check status 3");
                    }
                }
            });
            this.disableLock = that.disableLock;
            that = null;

            //console.log("Temp Array");
            //console.log(tempArr);
        } else {
            this.disableLock = true;
        }
    }

    closeConfigurationUpdateSuccess() {
        // this.showLoader = false;
        let sessionModal = document.getElementById('PricingConfigSave');
        sessionModal.classList.add('in');
        sessionModal.classList.remove('out');
        document.getElementById('PricingConfigSave').style.display = 'none';
    }

    printOrder() {
        //this.printHeader = false;

        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        var tableData = document.getElementById('hiddenDivForExport').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + tableData + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + tableData + '</body></html>');
            popup.document.close();
        }
    }

    exportData() {
        var blob = new Blob([document.getElementById('exportHeader').innerHTML, document.getElementById('hiddenDivForExport').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, this.customerName + "PricingConfiguration.xls");
    }

    validateAmount(event: any, val: any, elementId: string) {
        this._globalFunc.validateAmount(event, val, elementId);
    }

    validatePercentage(event: any, val: any, elementId: string) {
        this._globalFunc.validatePercentage(event, val, elementId);
        //console.log(this.disableLock);
    }
    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }
    disableLockButton() {
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if(res =='sessionActive'){

                if (document.getElementById('deliverChargeB').validity.valueMissing) {
                    document.getElementById('deliverChargeB').setCustomValidity("Please fill out this field.");
                } else if (document.getElementById('deliverChargeOB').validity.valueMissing) {
                    document.getElementById('deliverChargeOB').setCustomValidity("Please fill out this field.");
                } else if (document.getElementById('pMax').validity.valueMissing) {
                    document.getElementById('pMax').setCustomValidity("Please fill out this field.");
                } else {
                    if (!this.disableLock) {
                        //lock is enable;
                        this.disableLock = true;
                        this.disableSave = true;
                        this.readonlyText = true;
                        this.tradingGroupsData.configCommon.status = 'lock';
                        //console.log(this.tradingGroupsData);
                        this.savePricingConfigurations('lock');
                    }
                }
            }
        }, reason =>{
            console.log(reason);
        });
    }

    redirect(page: any) {
        
            this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res =>{
                if (page == 'cataloguemanagement') {
                    this._router.navigate(['cataloguemanagement', this.customerName]);
                }
            }, reason =>{
                console.log(reason);
            });
        //}
    }
    setPermissionsArray(permissionGroup: string) {
        this.assignedPermissions.push(permissionGroup);
    }
    
    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject)=>{
            console.log("i m first");this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.
                   
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        console.log("Session terminated");
                        this.terminateFlg = true;
                        sessionStorage.setItem('terminateFlg', 'true');
                        reject('sessionInactive');
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else{
                        sessionStorage.clear();
                        this._router.navigate(['']);
                        reject('sessionInactive');
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        console.log("Session terminated");
                        this.terminateFlg = true;
                        sessionStorage.setItem('terminateFlg', 'true');
                        reject('sessionInactive');
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        sessionStorage.setItem('terminateFlg', 'false');
    
                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
    
                        console.log("this.organisationList234");
                        console.log(this.organisationList);
                        let that = this;
                        let pricingConfiguration = true;
                        let catalogueManagementMenuPermission = false;
                        let userHavingPermission = false;
    
                        this.organisationList.forEach(function (organisation: any) {
                            if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                                console.log("organisation : " + that.customerName);
                                console.log(organisation);
                                organisation.permissionRoleList.forEach(function (permRoleList: any) {
                                    //console.log("11");
                                    //console.log(permRoleList);
                                    permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                        //console.log("22");
                                        that.setPermissionsArray(permRoleDtlList.permissionGroup);
                                        if (permRoleDtlList.permissionGroup == 'catalogueManagement' || permRoleDtlList.permissionGroup == 'morrisonsPermissions') {
                                            permRoleDtlList.permissionNameList.forEach(function (permList: any) {
    
                                                userHavingPermission = true;
                                                if (permList.permissionName == 'catalogueManagement' && permList.permissionStatus == 'active') {
                                                    catalogueManagementMenuPermission = true;
                                                }
                                                console.log(permList);
                                                switch (permList.permissionName) {
                                                    case "pricingConfiguration":
                                                        //console.log("55");
                                                        pricingConfiguration = false;
                                                        //alert(filfilment);
                                                        break;
                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                        that = null;
                        if (!userHavingPermission) {
                            // redirect to order
                            // catalogue/mccolls/upload
                            this._router.navigate(["cataloguemanagement", customerName]);
                        }
                        this.pricingConfiguration = pricingConfiguration;   
                        resolve('sessionActive');                     
                    }
                }
                console.log("this.permissionArray");
                //alert(this.disabledfilfilment);
            }, (error: any) => {
                if (error.errorCode == '503.65.001') {
                    this._router.navigate(['/error404']);
                }
            });
        });
    }

    onChange(){
        this.unsavedChange = true;
        sessionStorage.setItem('unsavedChanges','true');
    }
}