import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductConfigurationComponent } from './productconfiguration.component';
import { CanDeactivateGuard } from '../../../../services/can-deactivate-guard.service';

// route Configuration
const productconfigurationRoutes: Routes = [
  { path: '', component: ProductConfigurationComponent, canDeactivate: [CanDeactivateGuard] },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(productconfigurationRoutes)],
  exports: [RouterModule]
})
export class ProductConfigurationRoutes { }
