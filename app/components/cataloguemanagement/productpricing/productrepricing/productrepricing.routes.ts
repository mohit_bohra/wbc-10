import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductRepricingComponent } from './productrepricing.component';
import { CanDeactivateGuard } from '../../../../services/can-deactivate-guard.service';

// route Configuration
const productrepricingRoutes: Routes = [
  { path: '', component: ProductRepricingComponent, canDeactivate: [CanDeactivateGuard] },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(productrepricingRoutes)],
  exports: [RouterModule]
})
export class ProductRepricingRoutes { }
