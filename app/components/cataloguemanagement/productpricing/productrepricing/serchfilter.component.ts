import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'searchfilter'
})

@Injectable()
export class SearchFilterPipe implements PipeTransform {
    transform(items: any[], field: string, value: any): any[] {
        console.log(field);
        let temp: any[];
        console.log(value);
        if (!items) return [];
        if (value == true) { 
            sessionStorage.setItem('filterdata', JSON.stringify(items.filter(it => (it.approvalStatus=='' || it.approvalStatus== undefined))));
            return items.filter(it => (it.approvalStatus=='' || it.approvalStatus== undefined)); 
        }
        
        /* if (field == 'cost.amount') {
            console.log(field);
            return items.filter(it => it.cost.amount > value);
        } else if (field == 'cost.percIncrease') {
            console.log(field);
            return items.filter(it => it.cost.percIncrease > value);
        } else if(field == 'both'){
            console.log(field);
            console.log(value[0]);
            console.log(value[1]);
            console.log(items);
            //temp = items.filter(it => (it.cost.amount > value[1] && it.cost.percIncrease > value[0]));
            return items.filter(it => (it.cost.amount > value[0] && it.cost.percIncrease > value[1]));
        } else */ 
        if (field == 'approvalStatus') {
            if(value == true){
                sessionStorage.setItem('filterdata', JSON.stringify(items.filter(it => (it.approvalStatus=='' || it.approvalStatus== undefined))));
                return items.filter(it => (it.approvalStatus=='' || it.approvalStatus== undefined));
            } else {
                console.log('false');
                sessionStorage.setItem('olddata', JSON.stringify(items.filter(it => (it))));
                return items.filter(it => (it));
            }            
        } else {
            sessionStorage.setItem('olddata', JSON.stringify(items.filter(it => (it))));
            return items.filter(it => it);
        }

    }
}