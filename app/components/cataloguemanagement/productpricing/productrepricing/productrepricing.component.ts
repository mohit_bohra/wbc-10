import { Component, ViewChild } from '@angular/core';
//import { DomSanitizer } from '@angular/platform-browser';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { GlobalComponent } from '../../../global/global.component';
import { Constant } from '../../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
//import { CatalogueManagePendingHeaderComponent } from '../../cataloguemanagependingheader/cataloguemanagependingheader.component';
import { document } from '../../../datepicker/utils/facade/browser';
import { concat } from 'rxjs/operators/concat';
import { empty } from 'rxjs/observable/empty';
import { FormBuilder } from '@angular/forms';
import { DialogService } from '../../../../services/dialog.service';
// declare let jsPDF:any;
declare let saveAs: any;


@Component({
    templateUrl: 'productrepricing.component.html',
    providers: [commonfunction]
})
export class ProductRepricingComponent {
    @ViewChild('repricingSearch') repricingSearch: any;
    objConstant = new Constant();
    terminateFlg: boolean = false;
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;
    showLoader: boolean;
    printHeader: boolean;
    productRepricingData: any = [];
    tempproductRepricingData: any = [];
    productRepricingData1: any = [];
    oldproductRepricingData: any = [];

    pageState: number = 25;
    catalogueTypes: any;
    catalogueType: string = "";
    cataloguemasterListLength: any;
    cataloguedetailListLength: any;
    catalogueDetailList: any;
    brandType: string = 'branded';
    productRepricingSalesData: any = [];
    productRepricingSalesDatalen: any;
    successMsg: string = "";

    disablesub: boolean = false;
    disabletxtonSub: boolean = false;
    disableApprove: boolean = false;
    disableSave: boolean = false;
    disabledraft: boolean = false;
    nodatamsg: string = '';

    organisationListChange: any;
    disabledCusChange: boolean;
    btn_repricingSupplierRequestsReviewSubmit: boolean = false;
    btn_repricingPeriodicReviewSubmit: boolean = false;
    imgName: string = "";

    filterValue: string = '';
    searchVal: boolean = true;

    counter: number = 0;

    changedBrand: any;
    previousBrand: any;
    familyMemberData:any=[];
    familyMemberDatalen :any;
    unsavedChange: boolean = false;
    //Constructor is used to initialize variables during load time
    constructor(private _postsService: Services, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, public _globalFunc: GlobalComponent, private formBuilder: FormBuilder, public dialogService: DialogService) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        this.repricingSearch = this.formBuilder.group({})
    }

    canDeactivate(): Promise<boolean> | boolean {
        if (!this.unsavedChange || sessionStorage.getItem('unsavedChanges')!=='true') {
            return true;
        }
        return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
    }

    // This method is called on page load.
    // Hence all the basic cosmetic related elements are initialized here
    ngOnInit() {
        this.imageUrl = this.whoImgUrl;
        this.printHeader = true;

        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

        sessionStorage.setItem('pageState', '25');

        this.sub = this._route.params.subscribe((params: { orgType: string }) => {
            let orgImgUrl: any;
            let cusType = params.orgType;
            this.customerName = params.orgType;
            let orgApiNameTemp;
            this.orgApiName = this.customerName;
            /* this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));
 
             this.organisationList.forEach(function (item: any) {
                 if (item.orgType === cusType) {
                     // logic for permission set
                     orgImgUrl = item.imgUrl;
                     orgApiNameTemp = item.orgName;
                 }
             });*/
            this.orgimgUrl = orgImgUrl;
            this.orgApiName = orgApiNameTemp;
            //this.getProductReprising();
            this.imgName = this.getCustomerImage(params.orgType);
            this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
            this.getAllMasterData();
        });

    }

    // All the line item data on HTML comes from here ( for all the tabs )
    // In this method we call a method from service class (app.service)
    // We store the data in a variable and then the same variable is used in HTML. In this case, the variable is 'productRepricingData'
    getProductReprising(value: any) {
        console.log(value);
        this.nodatamsg = '1';
        if (value == '') {

        } else {
            this.catalogueType = value.catalogueType;
            var filteredValue = value.ritem;
        }
        this.showLoader = true;

        this.previousBrand = this.brandType;
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            console.log(res);
            if (res == 'sessionActive') {
                this.unsavedChange = false;
                sessionStorage.setItem('unsavedChanges','false');

                //this.showLoader = true;
                this._postsService.getProductRepricing(this.customerName, this.catalogueType, this.brandType).subscribe(data => {
                    console.log('data --->' + data);
                    if (Object.keys(data).length === 0) {

                        console.log('empty');
                        this.nodatamsg = 'No Records found for selected Filter!';
                        this.productRepricingData.length = 0;
                        this.productRepricingData1.length = 0;
                    } else {
                        this.productRepricingData1 = data;
                        this.productRepricingData = data.productRepricingList;

                        sessionStorage.setItem('olddata', JSON.stringify(data.productRepricingList));
                        sessionStorage.setItem('orgiData', JSON.stringify(data.productRepricingList));
                        console.log(sessionStorage.getItem('olddata'));
                        this.productRepricingData.forEach(function (item: any, i: any) {
                            item.productSummary.isModified = false
                        });
                        console.log("this.productRepricingData");
                        console.log(this.productRepricingData);
                        this.oldproductRepricingData = this.productRepricingData;

                        this.showHideButton(data);
                    }
                   
                    this.getFieldVal(true);
                }, error => {
                    console.log(error);
                    if (error.errorCode == '404.33.410' || error.httpResponseCode == '404') {
                        this.productRepricingData = [];
                        this.nodatamsg = "Sorry, No data found at this moment.";
                        this.showLoader = false;
                    } else {
                        this._router.navigate(['/error404']);
                    }
        
                });
                if(this.searchVal == true) {
                    if(JSON.parse(sessionStorage.getItem('filterdata')) == undefined){
                        this.productRepricingData = [];
                        console.log(this.productRepricingData);
                    } else {
                        this.productRepricingData = JSON.parse(sessionStorage.getItem('filterdata'));
                        console.log(this.productRepricingData);
                    }
                } else {
                    console.log(JSON.parse(sessionStorage.getItem('orgiData')));
                    this.productRepricingData = JSON.parse(sessionStorage.getItem('orgiData'));
                    console.log(this.productRepricingData);
                }
            }
            // document.getElementById('greenCheckboxForUnactionedInput').checked = true;
            this.showLoader = false;
        }, reason => {
            console.log(reason);
        });
    }

    getFieldVal(val: any) {
        console.log(val);
        console.log(JSON.parse(sessionStorage.getItem('filterdata')));
        this.filterValue = 'approvalStatus';
        if (val == true) {
            this.searchVal = true;
            //this.productRepricingData = JSON.parse(sessionStorage.getItem('filterdata'));
        } else {
            this.searchVal = false;
        }        
    }

    showHideButton(data: any) {
        if (data.status == 'RepricingCompleted' || data.status == 'lock' || data.status == 'notYetFinished') {
            this.disablesub = true;
            this.disableSave = true;
            this.disableApprove = true;
            this.disabledraft = true;
            this.disabletxtonSub = true;
        } else if (data.status == 'draft') {
            if (typeof (data.pendingStatusCount) !== 'undefined' && (data.pendingStatusCount == 0 || data.pendingStatusCount == '0')) {
                this.disableSave = true;
                this.disableApprove = true;
                this.disablesub = false;
                this.disabledraft = false;
            } else if (typeof (data.pendingStatusCount) !== 'undefined' && (data.pendingStatusCount != 0 || data.pendingStatusCount != '0')) {
                this.disableSave = false;
                this.disableApprove = false;
                this.disablesub = true;
                this.disabledraft = true;

            }
        } else if (data.status == 'draft_lock') {
            this.disabledraft = false;
            this.disablesub = false;
            this.disableSave = true;
            this.disableApprove = true;


        }
        else if (data.status == 'DraftRepricingCompleted') {
            if (typeof (data.pendingStatusCount) !== 'undefined' && (data.pendingStatusCount == 0 || data.pendingStatusCount == '0')) {
                this.disableSave = true;
                this.disableApprove = true;
                this.disablesub = false;
                this.disabledraft = false;
            } else if (typeof (data.pendingStatusCount) !== 'undefined' && (data.pendingStatusCount != 0 || data.pendingStatusCount != '0')) {
                this.disableSave = false;
                this.disableApprove = false;
                this.disablesub = true;
                this.disabledraft = true;
            }
        }
        else {
            this.disablesub = true;
            this.disabledraft = true;
            this.disableSave = false;
            this.disableApprove = false;
        }

    }

    getFinalMargin(calMargin: any, index: any) {

        //unsave data popup code 
        let val =calMargin;
        if (this.oldproductRepricingData[index].approvalStatus != val) {
            this.unsavedChange = true;
            sessionStorage.setItem("unsavedChanges", "true");
            this.counter += 1;
            this.productRepricingData[index].productSummary.isModified = true;
        } else {
            if (this.counter > 1) {
                this.unsavedChange = true;
                sessionStorage.setItem("unsavedChanges", "true");
                this.productRepricingData[index].productSummary.isModified = true;
                this.counter -= 1;
            } else {
                this.productRepricingData[index].productSummary.isModified = false;
                this.unsavedChange = false;
                sessionStorage.setItem("unsavedChanges", "false");
                this.counter -= 1;
            }
        }
        
        this.productRepricingData.forEach(function (item: any, i: any) {
             item.productSummary.isModified = false
            if(!isNaN(calMargin)){
                if (typeof (calMargin) !== 'undefined' && item.margin.overwrittenMarginPercent === calMargin && i == index) {
                    item.repricingSummary.marginPercent = item.margin.overwrittenMarginPercent;
                    item.nonPromoWsp.nonPromoWsp = (item.repricingSummary.totalCost / (1 - item.repricingSummary.marginPercent / 100)).toFixed(4);
                    item.nonPromoWsp.preChallengeWsp = item.nonPromoWsp.nonPromoWsp;
                    item.nonPromoWsp.nonPromoFinalWsp = ((item.nonPromoWsp.nonPromoWsp) * (1 - item.nonPromoWsp.nonPromoChallenge / 100)).toFixed(4);
                    item.productSummary.isModified = true;
                }
            }
        });
        this.disableSave = false;
        this.disableApprove = false;
        this.disablesub = true;
        this.disabledraft = true;
    }

    getcalOMp(nonPromoOverwrittenWsp: any, index: any) {
        console.log(JSON.parse(sessionStorage.getItem('olddata')));

        //unsave data popup code 
        let val =nonPromoOverwrittenWsp;
        if (this.oldproductRepricingData[index].approvalStatus != val) {
            this.unsavedChange = true;
            sessionStorage.setItem("unsavedChanges", "true");
            this.counter += 1;
            this.productRepricingData[index].productSummary.isModified = true;
        } else {
            if (this.counter > 1) {
                this.unsavedChange = true;
                sessionStorage.setItem("unsavedChanges", "true");
                this.productRepricingData[index].productSummary.isModified = true;
                this.counter -= 1;
            } else {
                this.productRepricingData[index].productSummary.isModified = false;
                this.unsavedChange = false;
                sessionStorage.setItem("unsavedChanges", "false");
                this.counter -= 1;
            }
        }


        let tempval = JSON.parse(sessionStorage.getItem('olddata'));
        this.productRepricingData.forEach(function (item: any, i: any) {
             item.productSummary.isModified = false;
            var nonPromoOverwrittenWsp = item.nonPromoWsp.nonPromoOverwrittenWsp;
            if(!isNaN(nonPromoOverwrittenWsp)){
                if (typeof (nonPromoOverwrittenWsp) !== 'undefined' && item.nonPromoWsp.nonPromoOverwrittenWsp === nonPromoOverwrittenWsp && i == index) {
                    if (nonPromoOverwrittenWsp == '') {
                        console.log(tempval[i].repricingSummary.marginPercent);
                        item.repricingSummary.marginPercent = tempval[i].repricingSummary.marginPercent;
                        item.margin.overwrittenMarginPercent = tempval[i].margin.overwrittenMarginPercent;
                        item.nonPromoWsp.nonPromoWsp = tempval[i].nonPromoWsp.nonPromoWsp;
                        item.nonPromoWsp.preChallengeWsp = tempval[i].nonPromoWsp.preChallengeWsp
                        item.nonPromoWsp.nonPromoFinalWsp = tempval[i].nonPromoWsp.nonPromoFinalWsp;
                        item.productSummary.isModified = false;
                        item.nonPromoWsp.nonPromoWspCase = tempval[i].nonPromoWsp.nonPromoWspCase;
                    } else {

                        var valueResult = (1 - ((1 - item.nonPromoWsp.nonPromoChallenge / 100) * (item.repricingSummary.totalCost) / (nonPromoOverwrittenWsp))) * 100;
                        if (nonPromoOverwrittenWsp == 0) {
                            var valueResult = 0
                        }
                        item.margin.overwrittenMarginPercent = valueResult.toFixed(2);

                        item.margin.overwrittenMarginPercent = valueResult.toFixed(2);
                        item.repricingSummary.marginPercent = item.margin.overwrittenMarginPercent;
                        item.nonPromoWsp.nonPromoWsp = (item.repricingSummary.totalCost / (1 - item.repricingSummary.marginPercent / 100)).toFixed(4);
                        item.nonPromoWsp.preChallengeWsp = (item.nonPromoWsp.nonPromoWsp);
                        item.nonPromoWsp.nonPromoFinalWsp = ((item.nonPromoWsp.nonPromoWsp) * (1 - item.nonPromoWsp.nonPromoChallenge / 100)).toFixed(2);
                        item.productSummary.isModified = true;
                        var valueResult1 = item.nonPromoWsp.nonPromoFinalWsp * item.cost.caseSize;
                        item.nonPromoWsp.nonPromoWspCase = valueResult1.toFixed(2);
                    }


                }
            }
        });
        this.disableSave = false;
        this.disableApprove = false;
        this.disablesub = true;
        this.disabledraft = true;
    }

    calPromoWSPCase(promowspcase: any, index: any) {

        //unsave data popup code 
        let val =promowspcase;
        if (this.oldproductRepricingData[index].approvalStatus != val) {
            this.unsavedChange = true;
            sessionStorage.setItem("unsavedChanges", "true");
            this.counter += 1;
            this.productRepricingData[index].productSummary.isModified = true;
        } else {
            if (this.counter > 1) {
                this.unsavedChange = true;
                sessionStorage.setItem("unsavedChanges", "true");
                this.productRepricingData[index].productSummary.isModified = true;
                this.counter -= 1;
            } else {
                this.productRepricingData[index].productSummary.isModified = false;
                this.unsavedChange = false;
                sessionStorage.setItem("unsavedChanges", "false");
                this.counter -= 1;
            }
        }

        this.productRepricingData.forEach(function (item: any, i: any) {
            item.productSummary.isModified = false;
            if(!isNaN(promowspcase)){
                if (typeof (promowspcase) !== 'undefined' && item.promoWsp.promoFinalWsp === promowspcase && i == index) {
                    console.log(item.cost.caseSize);
                    var valueResult = promowspcase * item.cost.caseSize;
                    item.promoWsp.promoWspCase = valueResult;
                    item.productSummary.isModified = true;
                }
            }
        });
        this.disableSave = false;
        this.disableApprove = false;
        this.disablesub = true;
        this.disabledraft = true;
    }

    /*   calnonpromowspcase(nonPromoFinalWsp: any, index: any) {
          this.productRepricingData.forEach(function (item: any, i: any) {
              if (typeof (nonPromoFinalWsp) !== 'undefined' && item.nonPromoWsp.nonPromoFinalWsp === nonPromoFinalWsp && i == index) {
                  console.log(valueResult);
                  console.log(nonPromoFinalWsp);
                  console.log(item.cost.caseSize);
                  var valueResult = nonPromoFinalWsp * item.cost.caseSize;
                  item.nonPromoWsp.nonPromoWspCase = valueResult.toFixed(2);
              }
          });
      } */

    cancelandExit() {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                this._router.navigate(['cataloguemanagement', this.customerName]);
            }
        }, reason => {
            console.log(reason);
        });
    }

    approveAll(arrLen: any) {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {


                console.log('approveAll');
                console.log(arrLen);

                for (var j = 0; j < this.productRepricingData.length; j++) {
                    this.productRepricingData[j]['approvalStatus'] = 'approved';
                     this.productRepricingData[j].productSummary.isModified = true;

                }

                this.unsavedChange = true;
                sessionStorage.setItem("unsavedChanges", "true");
                console.log(this.productRepricingData);
                // this.updateRepricing(this.productRepricingData1);
                // this.disableApprove = true;
            }
        }, reason => {
            console.log(reason);
        });
    }

    generateDraftLock() {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {

                this.productRepricingData1.status = 'draft_lock';
                status = this.productRepricingData1.status;
                console.log(this.productRepricingData1);
                this.updateRepricing(this.productRepricingData1);
            }

        }, reason => {
            console.log(reason);
        });
    }

    generateSub() {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                /* this.readonlyText = true;*/
                
                console.log(this.productRepricingData1);
                this.updateRepricing(this.productRepricingData1);
                this.productRepricingData1.status = 'lock';
                status = this.productRepricingData1.status;
                
               
                this.disabletxtonSub = true;
                this.disableSave = true;
                this.disableApprove = true;
                this.disabledraft = true;
                this.disablesub = true;
                  

            }
        }, reason => {
            console.log(reason);
        });

    }

    updateRepricing(value: any) {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {

                console.log(value);
                let tempJSON;
                if (value == 'form') {
                    tempJSON = this.productRepricingData1;
                    this.productRepricingData1.status = 'draft';
                }
                else {
                    console.log('generate');
                    tempJSON = value;
                }


                this.productRepricingData1 = tempJSON;
                this.productRepricingData1.userName = sessionStorage.getItem('name');

                console.log(this.productRepricingData1);
                tempJSON = null;
                this._postsService.updateRepricing(this.customerName, this.productRepricingData1, this.catalogueType).subscribe(data => {
                    this.showLoader = false;
                    console.log(status + "  status");
                    if (this.productRepricingData1.status == 'notYetFinished' || this.productRepricingData1.status == 'draft_lock') {
                        this.successMsg = "Your request for initiating Report has been accepted.Report generation is in progress"
                    }

                    else if (this.productRepricingData1.status == 'lock') {
                        this.successMsg = "Product Repricing locked successfully.";
                    }
                    else {
                        this.successMsg = "Product Repricing saved successfully.";
                    }

                    this.ngOnInit();
                    this.counter = 0;

                    console.log(data.batchDate);
                    console.log(this.productRepricingData1.status);
                    // data.pendingStatusCount = "0";
                    console.log(data.pendingStatusCount);
                  // this.showHideButton(data);
                    let sessionModal = document.getElementById('RepricingSave');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('RepricingSave').style.display = 'block';
                    
                  
                }, err => {
                        console.log(err);
                        this.showLoader = false;
                        //this._router.navigate(['/error404']);
                    }
                );
            }
        }, reason => {
            console.log(reason);
        });
    }

    closeRepricingUpdateSuccess() {
        // this.showLoader = false;

        this.getFieldVal(this.searchVal);
        if(this.successMsg=="Product Repricing saved successfully."){
        this.getProductReprising('');
        }
        let sessionModal = document.getElementById('RepricingSave');
        sessionModal.classList.add('in');
        sessionModal.classList.remove('out');
        document.getElementById('RepricingSave').style.display = 'none';
    }


    closeRepricingDraftModal() {
        let sessionModal = document.getElementById('RepricingDraftLock');
        sessionModal.classList.add('in');
        sessionModal.classList.remove('out');
        document.getElementById('RepricingDraftLock').style.display = 'none';
    }
    // This method is activated when one clicks on the check or uncheck button in HTML
    // This method calls another method, which is situated in common.services class
    getActivateCheckbox(val: any, index: any, value: any, eleName: string) {
        this._cfunction.getActivateCheckbox(index, value, eleName);

        if(this.disableSave && this.disablesub){
            this.disableSave = false;
            this.disableApprove = false;
            this.disablesub = true;
            this.disabledraft = true;
        }

        if (this.searchVal == true) {
            this.oldproductRepricingData = JSON.parse(sessionStorage.getItem('filterdata'));
        } else {
            this.oldproductRepricingData = JSON.parse(sessionStorage.getItem('olddata'));
        }
        if (this.oldproductRepricingData[index].approvalStatus != val) {
            this.unsavedChange = true;
            sessionStorage.setItem("unsavedChanges", "true");
            this.counter += 1;
            this.productRepricingData[index].productSummary.isModified = true;
        } else {
            if (this.counter > 1) {
                this.unsavedChange = true;
                sessionStorage.setItem("unsavedChanges", "true");
                this.productRepricingData[index].productSummary.isModified = true;
                this.counter -= 1;
            } else {
                this.productRepricingData[index].productSummary.isModified = false;
                this.unsavedChange = false;
                sessionStorage.setItem("unsavedChanges", "false");
                this.counter -= 1;
            }
        }
       // this.oldproductRepricingData = JSON.parse(sessionStorage.getItem('olddata'));
        console.log("this.productRepricingData");
        console.log(this.productRepricingData);
        console.log("this.productRepricingData1");
        this.productRepricingData1.productRepricingList = this.productRepricingData;
        console.log(this.productRepricingData1);
    }
   familyMemberpopup(idx: number, leadPrd: any) {
        let leadProduct = leadPrd;
        let filterKey = "getFamliyMember";
        console.log("idx = " + idx);
        this._postsService.getFamilyMemberDataforRepricing(this.customerName, this.catalogueType,this.brandType, filterKey, leadProduct).subscribe(data => {
            this.familyMemberData = data.leadProductDetails;
            this.familyMemberDatalen = 1;
            console.log(this.productRepricingData);
        }, error => {
            console.log(error);
            if (error.errorCode == '404.33.410' || error.httpResponseCode == '404') {
                this.familyMemberDatalen = 0;
                this.showLoader = false;
            } else {
                this._router.navigate(['/error404']);
            }

        });
    }
    checkForCharacter(value: any) {
        var pattern = /^(\d{1,2}|\d{0,2}\.\d{1,2})$/;
        //let inputChar = String.fromCharCode(value.charCode);
        var iValue = value.target.value;
        if (pattern.test(iValue)) {

        } else {
            //invalid character, prevent input
            value.preventDefault();

        }
    }

    // On HTML, there is a dropdown, all the data of this dropdown comes from this method
    // In this method we call a method from service class (app.service)
    // We store the data in a variable and then the same variable is used in HTML. In this case, the variable is 'catalogueTypes'
    getAllMasterData() {
        this.showLoader = true;
        this._postsService.getAllMasterData(this.customerName).subscribe(data => {
            this.showLoader = false;
            if (data.catalogueTypes.length == 0) {
                this.cataloguemasterListLength = 0;
            } else {
                for (var k = 0; k < data.catalogueTypes.length; k++) {
                    if (data.catalogueTypes[k].catalogueType.indexOf(':') > 0) {
                        data.catalogueTypes[k].catalogueType = data.catalogueTypes[k].catalogueType.split(':')[1];
                    }
                }
                this.cataloguemasterListLength = data.catalogueTypes.length;
            }
            this.catalogueTypes = data.catalogueTypes;
        }
            , err => {
                this.cataloguemasterListLength = 0;
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
        );
    }

    // This method is activated when we click on print icon
    printOrder() {
        //this.printHeader = false;
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        var tableData = document.getElementById('exportable').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + tableData + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + tableData + '</body></html>');
            popup.document.close();
        }
    }

    //  // This method is activated when we click on download icon
    exportData() {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Product Repricing.xls");
    }

    salesPopup(idx: number, legacy: any, pin: any) {
        console.log("idx = " + idx);
        this._postsService.getProductRepricingSalesData(this.customerName, legacy, pin).subscribe(data => {
            this.productRepricingSalesData = data.salesDetails;
            this.productRepricingSalesDatalen = 1;
            console.log(this.productRepricingData);
        }, error => {
            console.log(error);
            if (error.errorCode == '404.33.410' || error.httpResponseCode == '404') {
                this.productRepricingSalesDatalen = 0;
                this.showLoader = false;
            } else {
                this._router.navigate(['/error404']);
            }

        });
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }
    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }


    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.
                    
                     if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        console.log("Session terminated");
                        this.terminateFlg = true;
                        sessionStorage.setItem('terminateFlg', 'true');
                        reject('sessionInactive');
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else{
                        sessionStorage.clear();
                        this._router.navigate(['']);
                        reject('sessionInactive');
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        console.log("Session terminated");
                        this.terminateFlg = true;
                        sessionStorage.setItem('terminateFlg', 'true');
                        reject('sessionInactive');
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {

                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
                        this.organisationListChange = changeOrgAvailable;
                        this.orgimgUrl = orgImgUrl;
                        this.orgApiName = orgApiNameTemp;
                        // this.disabledCusChange = false;
                        if (this.organisationList.length == 1) {
                            this.disabledCusChange = true;
                        }

                        console.log("this.organisationList234");
                        console.log(this.organisationList);
                        let that = this;
                        let btn_repricingSupplierRequestsReviewSubmit = false;
                        let btn_repricingPeriodicReviewSubmit = false;
                        let userHavingPermission = false;


                        let permArray = [];
                        let permArrayWholesaleCatalogues: any = [];
                        let permArrayNewRequests: any = [];
                        let permArrayPricing: any = [];
                        let permArrayMainEstateRange: any = [];

                        this.organisationList.forEach(function (organisation: any) {
                            if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                                console.log("organisation : " + that.customerName);
                                console.log(organisation);
                                organisation.permissionRoleList.forEach(function (permRoleList: any) {
                                    //console.log("11");
                                    //console.log(permRoleList);
                                    permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                        //console.log("22");
                                        if (permRoleDtlList.permissionGroup == 'catalogueManagement' || permRoleDtlList.permissionGroup == 'morrisonsPermissions') {
                                            permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                                /*if(permList.permissionName == 'catalogueManagement' && permList.permissionStatus == 'active'){
                                                    dashboardMenuPermission = true;
                                                }*/
                                                userHavingPermission = true;
                                                console.log(permList);
                                                switch (permList.permissionName) {
                                                    case "btn_repricingSupplierRequestsReviewSubmit":
                                                        btn_repricingSupplierRequestsReviewSubmit = true;
                                                        break;
                                                    case "btn_repricingPeriodicReviewSubmit":
                                                        btn_repricingPeriodicReviewSubmit = true;
                                                        break;
                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                        that = null;
                        if (!userHavingPermission) {
                            // redirect to order
                            // catalogue/mccolls/upload
                            this._router.navigate(["cataloguemanagement", customerName]);
                        }
                        this.btn_repricingSupplierRequestsReviewSubmit = btn_repricingSupplierRequestsReviewSubmit;
                        this.btn_repricingPeriodicReviewSubmit = btn_repricingPeriodicReviewSubmit;
                        resolve('sessionActive');
                    }
                }
            });
        });
    }

    /*only show unactioned when checkbox is clicked starts here*/
    getActivateCheckboxForUnactioned(event: any) {

        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {

                this.filterValue = 'approvalStatus';
                if (event.target.checked) {
                    this.searchVal = true;
                    //this.productRepricingData = JSON.parse(sessionStorage.getItem('filterdata'));
                } else {
                    this.searchVal = false;
                    //this.productRepricingData = JSON.parse(sessionStorage.getItem('orgiData'));
                }
            }
        }, reason => {
            console.log(reason);
        });

    }
    /*only show unactioned when checkbox is clicked ends here*/

    onChangeBrand(event: any) {
        this.changedBrand = event.target.value;
        console.log("Selected event", event.target.value);
        if (sessionStorage.getItem("unsavedChanges") == 'true') {
            this.changeModalPageLevel();
            //sessionStorage.setItem("unsavedChanges", 'false');
            sessionStorage.setItem("pageLevel",'true');
            sessionStorage.setItem('changedBrandType',this.changedBrand);
            sessionStorage.setItem('previousBrandType',this.previousBrand);
        }else{
            return false;
        }
    }

    changeModalPageLevel() {
        let sessionModal = document.getElementById('unsaveModalPageLevel');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('unsaveModalPageLevel').style.display = 'block';
        setTimeout(() => {
        }, this.objConstant.WARNING_TIMEOUT);
    }

    keepMeonSamePage() {
        let brandType = sessionStorage.getItem('previousBrandType');
        console.log('Brand Type', document.getElementById(brandType));
        let sessionModal = document.getElementById('unsaveModalPageLevel');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('unsaveModalPageLevel').style.display = 'none';
        document.getElementById(brandType).checked = 'true';
    }

    navigatTopage() {
        let sessionModal = document.getElementById('unsaveModalPageLevel');
       // this.getprodicProductReprising('');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('unsaveModalPageLevel').style.display = 'none';
        this.showLoader = false;
        this.previousBrand = this.changedBrand;
    }
}