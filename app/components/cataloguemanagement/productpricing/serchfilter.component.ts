import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'searchfilter'
})

@Injectable()
export class SearchFilterPipe implements PipeTransform {
    transform(items: any[], field: string, value: string): any[] {
        console.log(field);
        let temp: any[];
        console.log(value);
        if (!items) return [];
        if (value == 'unaction') {
            return items.filter(it => (it.procuredPricingStatus == '' || it.procuredPricingStatus == undefined));
        }
        if (field == 'procuredPricingStatus') {
            if (value == 'unaction') {
                return items.filter(it => (it.procuredPricingStatus == '' || it.procuredPricingStatus == undefined));
            } else {
                return items.filter(it => (it));
            }
        } else {
            return items.filter(it => it);
        }

    }
}