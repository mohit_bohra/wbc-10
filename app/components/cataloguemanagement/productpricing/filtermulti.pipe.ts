import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filtermulti'
})
export class FiltermultiPipe implements PipeTransform {
    transform(myobjects: Array<object>, args?: Array<object>): any {
        if (args && Array.isArray(myobjects)) {
            // copy all objects of original array into new array of objects
            var returnobjects = myobjects;
            console.log(myobjects);
            console.log(returnobjects);

            // args are the compare oprators provided in the *ngFor directive
            args.forEach(function (filterobj) {
                let filterkey = Object.keys(filterobj)[0];
                console.log(filterkey);

                let filtervalue = filterobj[filterkey];
                console.log(filtervalue);

                myobjects.forEach(function (objectToFilter) {
                    if (filterkey == 'affectedCount') {
                        console.log(objectToFilter[filterkey]);
                        if (filtervalue == 'All') {
                            if (objectToFilter[filterkey] >= filtervalue && filtervalue != '') {
                                // object didn't match a filter value so remove it from array via filter
                                returnobjects = returnobjects.filter(obj => obj !== objectToFilter);
                            }
                        } else {
                            if (objectToFilter[filterkey] == filtervalue && filtervalue != '') {
                                // object didn't match a filter value so remove it from array via filter
                                returnobjects = returnobjects.filter(obj => obj !== objectToFilter);
                            }
                        }
                    } else if (filterkey == 'pricingRepeat') {
                        if (filtervalue == 'NO') {
                            if ((objectToFilter[filterkey] !== filtervalue)) {
                                // object didn't match a filter value so remove it from array via filter
                                console.log(objectToFilter);
                                returnobjects = returnobjects.filter(obj => obj !== objectToFilter);
                                console.log(returnobjects);
                            }
                        } else {
                            if (objectToFilter[filterkey] == filtervalue) {
                                // object didn't match a filter value so remove it from array via filter
                                returnobjects = returnobjects.filter(obj => obj);
                            }
                        }
                    } else if (filterkey == 'procuredPricingStatus') {
                        if (filtervalue == 'unaction') {
                            if (!(objectToFilter[filterkey] != 'Y'  && objectToFilter[filterkey] != 'N')) {
                                // object didn't match a filter value so remove it from array via filter
                                console.log(objectToFilter);
                                returnobjects = returnobjects.filter(obj => obj !== objectToFilter);
                            }
                        } else {
                            if (objectToFilter[filterkey] == filtervalue && filtervalue != "") {
                                // object didn't match a filter value so remove it from array via filter
                                returnobjects = returnobjects.filter(obj => obj);
                            }
                        }
                    }
                })
            });
            // return new array of objects to *ngFor directive
            sessionStorage.setItem('filterval',JSON.stringify(returnobjects));
            return returnobjects;
        }
    }
}