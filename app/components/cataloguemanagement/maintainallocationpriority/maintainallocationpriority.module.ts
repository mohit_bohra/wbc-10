import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaintainallocationpriorityRoutes } from './maintainallocationpriority.routes';
import { MaintainallocationpriorityComponent } from './maintainallocationpriority.component';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    MaintainallocationpriorityRoutes,
    CommonModule,
    NavigationModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [MaintainallocationpriorityComponent]
})
export class MaintainallocationpriorityModule { }
