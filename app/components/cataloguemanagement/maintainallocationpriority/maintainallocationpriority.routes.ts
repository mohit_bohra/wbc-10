import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { MaintainallocationpriorityComponent } from './maintainallocationpriority.component';
import { CanDeactivateGuard } from '../../../services/can-deactivate-guard.service';

// route Configuration
const maintainsupplydepotRoutes: Routes = [
  { path: '', component: MaintainallocationpriorityComponent, canDeactivate: [CanDeactivateGuard] }
];


@NgModule({
  imports: [RouterModule.forChild(maintainsupplydepotRoutes)],
  exports: [RouterModule]
})

export class MaintainallocationpriorityRoutes{}
