import { Component, OnInit, ViewChild } from '@angular/core';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormBuilder } from '@angular/forms';
import { Services } from '../../../services/app.services';
import { ActivatedRoute, Router } from '@angular/router';
import { PlatformLocation } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { DialogService } from '../../../services/dialog.service';
import { error } from 'util';
declare let saveAs: any;

@Component({
	selector: 'app-maintainallocationpriority',
	templateUrl: './maintainallocationpriority.component.html',
	providers: [commonfunction]
})
export class MaintainallocationpriorityComponent implements OnInit {

	//variable declaration
	@ViewChild('editAllocationPriority') editAllocationPriority: any;
	objConstant = new Constant();
	whoImgUrl: any;
	orgimgUrl: string;
	homeUrl: any;
	imageUrl: string;
	showLoader: boolean;
	printHeader: boolean;
	sub: any;
	customerName: string;
	orgApiName: string;
	imgName: string = "";
	terminateFlg: boolean;
	organisationList: any;
	organisationListChange: any;
	disabledCusChange: boolean;
	rowId: number = 1;
	unsavedChange: boolean = false;
	originalData: any = [];
	allocationPriorities: any;
	//allocationPrioritiesLength: any;
	initialValue: any;
	finalJSON: any = [];
	popupMsg: string;
	hasDuplicateAP: boolean = false;
	chkDuplicateAP: boolean[] = [];
	newRow: boolean = false;
	username: string

	custPermission: any[];
	moduleName: string;
	siteMaintanceFlg: boolean;
	siteMSg: any;
	errorMsg: string;
	deploymentDetailsMsg: string;
	featureDetailsMsg: string;
	businessMessageDetailsMsg: string;
	deploymentDetailsMsglength: any;
	featureDetailsMsglength: any;
	businessMessageDetailsMsglength: any;
	editForm: boolean = false;
	disableSave: boolean = true;
	showEdit: boolean = true;
	disableAddBtn: boolean = false;
	allocationPrioritiesOriginal: string;
	addAllocationPrioritiesPermission: boolean = true;
	editAllocationPrioritiesPermission: boolean = true;
	disablePrintHeader: boolean = false;

	constructor(private _postsService: Services, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _globalFunc: GlobalComponent, private dialogService: DialogService, private formbuilder: FormBuilder, private _cfunction: commonfunction) {
		this.whoImgUrl = this.objConstant.WHOIMG_URL;
		this.homeUrl = this.objConstant.HOME_URL;
		this.showLoader = false;
		this.editAllocationPriority = this.formbuilder.group({});
	}

	ngOnInit() {
		this.imageUrl = this.whoImgUrl;
		this.printHeader = true;
		sessionStorage.setItem("unsaveChagne", "false");
		(<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

		sessionStorage.setItem('pageState', '50');
		this.moduleName = "catalogueManagement";
		this.sub = this._route.params.subscribe((params: { orgType: string }) => {
			let orgImgUrl: any;
			let cusType = params.orgType;
			this.customerName = params.orgType;
			let orgApiNameTemp;
			this.orgimgUrl = orgImgUrl;
			this.orgApiName = orgApiNameTemp;
			this.orgApiName = this.customerName;
			//this.getprodicProductReprising();
			this.imgName = this.getCustomerImage(params.orgType);
			//this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));

			let custPerm: any = [];
			this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
				if (res == 'sessionActive') {
					this.imgName = this.getCustomerImage(params.orgType);
					this.custPermission = custPerm;
					this.getConfigService(this.moduleName);
				}
			}, reason => {
				console.log(reason);
			});

			this.getAllocationPriorities();

			let permissions = JSON.parse(sessionStorage.getItem('tempPermissionArray'));
			if(permissions[this.customerName]['morrisonsPermissions'].indexOf('addAllocationPriorities')== -1){
				this.addAllocationPrioritiesPermission = false;
			}else{
				this.addAllocationPrioritiesPermission = true;
			}

			if(permissions[this.customerName]['morrisonsPermissions'].indexOf('editAllocationPriorities')== -1){
				this.editAllocationPrioritiesPermission = false;
			}else{
				this.editAllocationPrioritiesPermission = true;
			}
		});
		this.username = sessionStorage.getItem('name');
	}

	/* unsaved changes functionality usng candeactivate guard start */
	canDeactivate(): Promise<boolean> | boolean {
		if (!this.unsavedChange || sessionStorage.getItem('unsavedChanges') !== 'true') {
			return true;
		}
		return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
	}
	/* unsaved changes functionality usng candeactivate guard end */

	/* rbac implementation start*/
	getCustomerImage(custName: string) {
		return this._globalFunc.getCustomerImage(custName);
	}

	getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
		return new Promise((resolve, reject) => {
			this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
				if (!userData.isSessionActive) { // false
					// User session has expired.
					if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
						console.log("Session terminated");
						reject('sessionInactive');
						this.terminateFlg = true;
						let sessionModal = document.getElementById('popup_SessionExpired');
						sessionModal.classList.remove('in');
						sessionModal.classList.add('out');
						document.getElementById('popup_SessionExpired').style.display = 'block';
					} else {
						reject('sessionInactive');
						sessionStorage.clear();
						this._router.navigate(['']);
					}
				} else {
					if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
						console.log("Session terminated");
						reject('sessionInactive');
						this.terminateFlg = true;
						let sessionModal = document.getElementById('popup_SessionExpired');
						sessionModal.classList.remove('in');
						sessionModal.classList.add('out');
						document.getElementById('popup_SessionExpired').style.display = 'block';
					} else {
						resolve('sessionActive');
						this.terminateFlg = false;
						this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
						let changeOrgAvailable: any = [];
						let orgImgUrl = "";
						let orgApiNameTemp = "";
						this.organisationList.forEach(function (item: any) {
							if (item.customerName !== customerName) {
								changeOrgAvailable.push(item);
							} else {
								orgImgUrl = item.customerName + ".png";
								orgApiNameTemp = item.customerName;
							}
						});
						this.organisationListChange = changeOrgAvailable;
						this.orgimgUrl = orgImgUrl;
						this.orgApiName = orgApiNameTemp;
						// this.disabledCusChange = false;
						if (this.organisationList.length == 1) {
							this.disabledCusChange = true;
						}
					}

				}
			});
		});
	}
	/* rbac implementation end */

	//get all allocation priorities on load
	getAllocationPriorities() {
		console.log('get allocation priorities');
		this._postsService.getAllocationPriority(this.customerName, 'dhlAllocationPriority', '@all').subscribe((data: any) => {
			this.originalData = data;

			let allocationPriority;
			if(data.priorityList != undefined){
				this.showEdit = true;
				allocationPriority = data.priorityList;
				this.allocationPrioritiesOriginal = JSON.stringify(data.priorityList);
				this.sortAllocationPriorities();
			}else{
				allocationPriority = [];
				this.showEdit = false;
			}

			this.allocationPriorities = allocationPriority;
		});
	}

	//sort allocation priorities
	sortAllocationPriorities() {
		return new Promise((resolve) => {
			let tempArr = this.allocationPriorities.sort((a: any, b: any) => {
				/*console.log(a);
				var charPart = [a.allocationPriorityCode.substring(0, 1), b.allocationPriorityCode.substring(0, 1)],
					numPart = [a.allocationPriorityCode.substring(1) * 1, b.allocationPriorityCode.substring(1) * 1];
				if (charPart[0] < charPart[1]) return -1;
				else if (charPart[0] > charPart[1]) return 1;
				else { //(charPart[0] == charPart[1]){
					if (numPart[0] < numPart[1]) return -1;
					else if (numPart[0] > numPart[1]) return 1;
					return 0;
				}*/

				if (a.allocationId < b.allocationId) {
					return 1;
				} else if (a.allocationId > b.allocationId) {
					return -1;
				} else {
					return 0;
				}
			});
			this.allocationPriorities = tempArr;
			resolve(true);
		});
	}

	saveAllocationPriorities() {
		//save allocation priorities
		this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
			if (res = 'sessionActive') {
				let msgList = Object.keys(this._cfunction.errorMsgList);

				if (msgList.length == 0 && !this.hasDuplicateAP) {
					this.unsavedChange = false;
					sessionStorage.setItem('unsavedChanges', 'false');
					let saveJSON = {
						"priorityList": this.allocationPriorities
					}
					console.log(saveJSON);
					this._postsService.saveAllocationPriority('default', 'dhlAllocationPriority', '@all', saveJSON).subscribe((data: any) => {
						this.getAllocationPriorities();
						this.popupMsg = "Allocation Priorities updated successfully.";
						let sessionModal = document.getElementById('confirmationModal');
						sessionModal.classList.remove('in');
						sessionModal.classList.add('out');
						document.getElementById('confirmationModal').style.display = "block";
						this.editForm = false;
						this.disableAddBtn = false;
						this.showEdit = true;
						this.newRow = false;
						this.disableSave = true;
						this.disablePrintHeader = false;
						this.sortAllocationPriorities();
						this.unsavedChange = false;
						sessionStorage.setItem('unsavedChanges', 'false');
					});
				} else {
					this.popupMsg = "";
					let that = this;
					let tempArr:any = [];
					let DupMsg:any = "";
					msgList.forEach(function (key: any) {
						//that.popupMsg += that._cfunction.errorMsgList[key] + ".<br/> ";
						tempArr.push(that._cfunction.errorMsgList[key]);
					});
					if (this.hasDuplicateAP) {
						//this.popupMsg += " There is some duplicate values entered";
						DupMsg = " There is some duplicate values entered.";
					}
					let tmp = Array.from(new Set(tempArr));
					console.log(tmp);

					tmp.forEach(function (itm: any) {
						that.popupMsg += itm + "<br/> ";
					});
					//this.popupMsg = tmp.join(". <br/>");
					this.popupMsg += DupMsg;
					let sessionModal = document.getElementById('errorMsgPopup');
					sessionModal.classList.remove('in');
					sessionModal.classList.add('out');
					document.getElementById('errorMsgPopup').style.display = "block";
				}
			}
		}, reason => {
			console.log(reason);
		});
	}

	//check for duplicate values
	checkDuplicateAP(ap: string, id: number, errId: string, e: any, elementId:string = null) {
		console.log('Event', e.target.value);
		let that = this;
		let isDuplicate: boolean = false;
		this.allocationPriorities.forEach(function (itm: any, idx: number) {
			if (e.target.value !== '') {
				if (itm.allocationPriorityCode.toUpperCase() == ap.toUpperCase() && idx != id) {
					isDuplicate = true;
					that.hasDuplicateAP = true;
				}
			}
		});
		this.chkDuplicateAP[id] = isDuplicate;
		this.hasDuplicateAP = this.checkAllForDuplicate();
		console.log("this.chkDuplicateAP");
		console.log(this.hasDuplicateAP);
		console.log("id : " + id);
		let divElement = document.getElementById(errId);
		let existingMsg = divElement.innerHTML;
		
		if (this.chkDuplicateAP[id]) {
			this.hasDuplicateAP = true;
			divElement.innerHTML = "This Allocation Priority Code already exist.";
			document.getElementById(errId).classList.add('red');
			if(elementId != null){
				document.getElementById(elementId).classList.add('error-border-red');
			}
		} else {
			divElement.innerHTML = "";
			if(elementId != null){
				//document.getElementById(elementId).classList.remove('error-border-red');
			}
		}
	}

	checkAllForDuplicate() {
		if (this.chkDuplicateAP.indexOf(true) != -1) {
			return true;
		} else {
			return false;
		}
	}

	closePopup(id: string) {
		let sessionModal = document.getElementById(id);
		sessionModal.classList.remove('out');
		sessionModal.classList.add('in');
		document.getElementById(id).style.display = "none";
	}

	//add new row functionality
	addNewRow() {
		this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
			if (res == 'sessionActive') {
				this.showEdit = false;
				this.newRow = true;
				this.disableAddBtn = true;
				this.disablePrintHeader = true;
				if(this.allocationPriorities.length != 0){
					if(this.allocationPriorities.length == (JSON.parse(this.allocationPrioritiesOriginal)).length) {
						this.allocationPriorities = JSON.parse(this.allocationPrioritiesOriginal);
						this.sortAllocationPriorities();

						let temp = {
							"allocationId": 0,
							"allocationPriorityCode": "",
							"allocationPriorityDesc": ""
						};
		
						this.allocationPriorities.unshift(temp);
					}
				}else{
					let temp = {
						"allocationId": 0,
						"allocationPriorityCode": "",
						"allocationPriorityDesc": ""
					};
	
					this.allocationPriorities.push(temp);
				}
			}
		}, reason => {
			console.log(reason);
		});
	}

	//cancel and redirect to catalogue management
	cancelAndRedirect() {
		console.log('Cancel and redirect to catalogue management');
		this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
			console.log(res);
			if (res == 'sessionActive') {
				console.log("this.showEdit");
				console.log(this.showEdit);
				console.log(this.newRow);
				if (this.canDeactivate()) {
					this.unsavedChange = false;
					this.disableSave = true;
					sessionStorage.setItem('unsavedChanges', 'false');
					if (!this.showEdit || this.newRow) {
						this.editForm = false;
						this.disableAddBtn = false;
						this.showEdit = true;
						this.newRow = false;
						this.disablePrintHeader = false;
						this._cfunction.errorMsgList = [];
						this.hasDuplicateAP = false;
						this.allocationPriorities = JSON.parse(this.allocationPrioritiesOriginal);
						this.sortAllocationPriorities();
					} else {
						this._router.navigate(['cataloguemanagement', this.customerName]);
					}
				}
			} else {

			}
		}, reason => {
			console.log(reason);
		});
	}

	getOriginalValue(e: any) {
		this.initialValue = e.target.value;
	}

	//check if changed
	onChange(e: any, index: number) {
		let that = this;
		let changedAp = this.allocationPriorities;
		let customerName = sessionStorage.getItem('name');
		this.unsavedChange = true;
		this.disableSave = false;
		sessionStorage.setItem('unsavedChanges', 'true');
		this.allocationPriorities.forEach(function (itm: any, idx: number) {
			if (itm.allocationId == changedAp[index].allocationId) {
				if (itm.allocationId == 0) {
					itm.createdBy = customerName;
				} else {
					itm.updatedBy = customerName;
				}
			}
		});
		console.log('Allocation hdfhsdf', this.allocationPriorities);
	}

	//print allocation priority
	printOrder() {
		//this.printHeader = false;

		var printHeaderNav = document.getElementById('printHeader').innerHTML;
		var tableData = document.getElementById('exportable').innerHTML;
		if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
			var popupWin = window.open('', '_blank');
			popupWin.document.open();
			popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + tableData + '</body></html>');
			popupWin.document.close();
			setTimeout(function () { popupWin.close(); }, 1000);
		} else {
			var popup = window.open('', '_blank');
			popup.document.open();
			popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + tableData + '</body></html>');
			popup.document.close();
		}
	}

	//download allocation priority
	exportData() {
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
			type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
		});
		saveAs(blob, "Allocation Priority.xls");
	}

	validateInputText(e: any, errorMsgId: string, errMsgFieldName: string) {
		if (e.target.value == '') {
			document.getElementById(errorMsgId).innerHTML = errMsgFieldName + ' is required.';
			document.getElementById(errorMsgId).classList.add('red');
		} else {
			document.getElementById(errorMsgId).innerHTML = '';
			document.getElementById(errorMsgId).classList.remove('red');
		}
	}

	//site maintenance
	getConfigService(pageName: any) {
		this._postsService.siteMaintanceCall('1', 'all').subscribe(userData => {
			console.log(userData);
			console.log(userData.maintenanceDetails[0].message);
			console.log(userData.maintenanceDetails[0].start);
			console.log(userData.maintenanceDetails[0].status);
			console.log(userData.deploymentDetails);
			if (userData.deploymentDetails != undefined && userData.deploymentDetails[0].status != 'NO') {
				this.deploymentDetailsMsg = userData.deploymentDetails[0].message + ' ' + userData.deploymentDetails[0].start;
				this.deploymentDetailsMsglength = this.deploymentDetailsMsg.length;
				if (sessionStorage.getItem('deploymentDetailsMsglength') == undefined || sessionStorage.getItem('deploymentDetailsMsglength') == null) {
					sessionStorage.setItem('deploymentDetailsMsglength', this.deploymentDetailsMsglength);
					console.log(this.deploymentDetailsMsglength + "this.deploymentDetailsMsglength");
				} else {
					this.deploymentDetailsMsglength = sessionStorage.getItem('deploymentDetailsMsglength');
					console.log(this.deploymentDetailsMsglength + "this.deploymentDetailsMsglength");
				}

			} else {
				this.deploymentDetailsMsg = '';
			}


			console.log(userData.featureDetails);
			if (userData.featureDetails != undefined && userData.featureDetails[0].status != 'NO') {
				this.featureDetailsMsg = userData.featureDetails[0].message + ' ' + userData.featureDetails[0].start;
				this.featureDetailsMsglength = this.featureDetailsMsg.length;
				if (sessionStorage.getItem('featureDetailsMsglength') == undefined || sessionStorage.getItem('featureDetailsMsglength') == null) {
					sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
				}
				else {
					this.deploymentDetailsMsglength = sessionStorage.getItem('featureDetailsMsglength');
				}
			} else {
				this.featureDetailsMsg = '';
			}

			if (userData.businessMessageDetails != undefined && userData.businessMessageDetails[0].status != 'NO') {
				this.businessMessageDetailsMsg = userData.businessMessageDetails[0].message + ' ' + userData.businessMessageDetails[0].start;
				this.businessMessageDetailsMsglength = this.businessMessageDetailsMsg.length;
				if (sessionStorage.getItem('businessMessageDetailsMsglength') == undefined || sessionStorage.getItem('businessMessageDetailsMsglength') == null) {
					sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
					sessionStorage.setItem('businessMessageDetailsMsglength', this.businessMessageDetailsMsglength);
				}
				else {
					this.businessMessageDetailsMsglength = sessionStorage.getItem('businessMessageDetailsMsglength');
				}
			} else {
				this.businessMessageDetailsMsg = '';
			}

			(<HTMLLabelElement>document.getElementById('deploymentDetailsId')).textContent = this.deploymentDetailsMsg;
			(<HTMLLabelElement>document.getElementById('featureDetailsId')).textContent = this.featureDetailsMsg;
			(<HTMLLabelElement>document.getElementById('businessMessageDetailsId')).textContent = this.businessMessageDetailsMsg;
			if (userData.maintenanceDetails[0].moduleName == 'all') {
				if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
					this.siteMaintanceFlg = true;
					this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
				} else {
					this.siteMaintanceFlg = false;
					this._postsService.siteMaintanceCall(this.customerName, pageName).subscribe(userData => {
						console.log(userData);
						console.log(userData.maintenanceDetails[0].message);
						console.log(userData.maintenanceDetails[0].start);
						console.log(userData.maintenanceDetails[0].status);
						if (userData.maintenanceDetails[0].moduleName == pageName) {
							if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
								this.siteMaintanceFlg = true;
								this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
							} else {
								this.siteMaintanceFlg = false;
							}
						} else {
							this.siteMaintanceFlg = false;
						}
						this.showLoader = false;
					}, error => {
						console.log(error);
						if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
							this.siteMaintanceFlg = true;
							this.siteMSg = "Site is under maintenance";
						} else if (error.errorCode == '404.26.408') {
							this.errorMsg = "User is Inactive";
						}
						else {
							this.siteMaintanceFlg = false;
						}
						this.showLoader = false;
					});
				}
			} else {
				this.siteMaintanceFlg = false;
			}
			this.showLoader = false;
		}, error => {
			console.log(error);
			if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
				this.siteMaintanceFlg = true;
				this.siteMSg = "Site is under maintenance";
			} else if (error.errorCode == '404.26.408') {
				this.errorMsg = "User is Inactive";
			}
			else {
				this.siteMaintanceFlg = false;
			}
			this.showLoader = false;
		});
	}

	editAP() {
		this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
			if (res == 'sessionActive') {
				this.editForm = true;
				this.disableAddBtn = true;
				this.showEdit = false;
				this.disablePrintHeader = true;
				this.allocationPriorities = JSON.parse(this.allocationPrioritiesOriginal);
				this.sortAllocationPriorities();
			}
		}, error => {
			console.log(error);
		});
	}
}