import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { commonfunction } from '../../services/commonfunction.services';
import { GlobalComponent } from '../global/global.component';
import { Constant } from '../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';

// not used old code
@Component({
    templateUrl: 'fulrangeexport.component.html',
    providers: [commonfunction]
})
export class fulRangeExportComponent {

    objConstant = new Constant();
    whoImgUrl: any;

    productid: string;
    errorMsg: boolean;
    errorMsgString: string;
    showLoader: boolean;
    printHeader: boolean;
    organisationList: any;

    pageState: number = 24;
    customerName: string;
    orgimgUrl: string;
    orgApiName: string;
    productDetails: any;

    productName: any;
    mapLen: any;
    description: any;
    homeUrl: any;
    private _sub: any;
    availabilityMaster: any;
    WorkflowMaster: any;
    catalogueTypeMaster: any;
    availabilityStatus: string;
    workflowStatusName: string;
    mapTypesMaster: any;
    mapTypesMasterDisplay: any;
    lookupTypesMaster: any;
    lookupTypesMasterDisplay: any;
    financialTypesMaster: any;
    financialTypesMasterDisplay: any;
    financialCurrencies: any;
    catalogueType: string;
    imageUrl: string;
    productMaps: any;
    productPrices: any;
    productmin: string;
    maptypeValue: string;
    isProductIdEntered: boolean;
    organisationListChange: any;
    showSuccessModel: boolean;
    minDate: any;
    addedProdId: any;
    addedMin: any;
    identifierRow: number = 1;
    pricingRow: number = 1;
    attributeRow: number = 1;

    prodIdentifierRowHTML: any;

    todayDateStr: string;

    hideOrganisation: boolean = true;
    tempCalId: any;
    calDefaultDate: Date;

    allowWrite: boolean;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disabledOrder: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;

    constructor(private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction) {


        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;

        // set default date null to calender
        this.errorMsgString = '';
        this.errorMsg = false;
        // document.getElementById("topBtn").style.display = "none";
    }

    // function works on page load

    ngOnInit() {

        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {

            this.showLoader = false;

            // user permission

            this.disabledPermission = false;
            this.disabledReports = false;
            this.disabledCatalogue = false;
            this.disabledOrder = false;

            this.disabledCatalogueRaise = false;
            this.disabledManageCatalogue = false;



            // window scroll function
            this._globalFunc.autoscroll();

            // set header basket data
            this.pageState = 24;

            sessionStorage.setItem('pageState', '24');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                let reports;
                let orders;
                let catalogues;
                let cataloguesRaise;
                let accessMgmt;
                let redirct;
                let changeOrgAvailable = new Array;
                let catManagement;
                // list of org assigned to user
                this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));

                this.organisationList.forEach(function (item: any) {
                    if (item.orgType !== cusType) {
                        changeOrgAvailable.push(item);
                    }
                });
                this.organisationListChange = changeOrgAvailable;
                // this.disabledCusChange = false;
                if (this.organisationList.length == 1) {
                    this.disabledCusChange = true;
                }

                document.getElementById('orderArrowSpan').style.display = 'block';
                document.getElementById('orderMob').style.display = 'block';
                document.getElementById('orderRaiseMob').style.display = 'none';

                document.getElementById('catalogueMob').style.display = 'block';
                document.getElementById('catalogueArrowSpan').style.display = 'none';
                document.getElementById('catalogueRaiseMob').style.display = 'none';
                document.getElementById('reportsMob').style.display = 'block';
                document.getElementById('permissonsMob').style.display = 'block';

                if (document.getElementById('orderArrow').classList.contains('glyphicon-menu-up') === true) {
                    let sublinks = document.getElementById('orderArrow');
                    sublinks.classList.remove('glyphicon-menu-up');
                    sublinks.classList.add('glyphicon-menu-down');
                    document.getElementById('orderRaiseMob').style.display = 'none';
                }

                if (document.getElementById('catalogueArrow').classList.contains('glyphicon-menu-up') === true) {
                    let sublinks = document.getElementById('catalogueArrow');
                    sublinks.classList.remove('glyphicon-menu-up');
                    sublinks.classList.add('glyphicon-menu-down');
                    document.getElementById('catalogueRaiseMob').style.display = 'none';
                }


                this.organisationList.forEach(function (item: any) {
                    if (item.orgType === cusType) {
                        // logic for permission set
                        orgImgUrl = item.imgUrl;
                        orgApiNameTemp = item.orgName;

                        if ((item.permissions.menuPermission.orders).toLowerCase() == 'none') {
                            orders = true;
                            document.getElementById('orderMob').style.display = 'none';
                            document.getElementById('orderRaiseMob').style.display = 'none';
                        }

                        if ((item.permissions.menuPermission.raiseNewOrder).toLowerCase() == 'none') {
                            document.getElementById('orderArrowSpan').style.display = 'none';
                            document.getElementById('orderRaiseMob').style.display = 'none';

                        }

                        if (item.permissions.menuPermission.catalogueManagement == 'W') {
                            catManagement = true;
                        } else {
                            catManagement = false;
                        }


                        if ((item.permissions.menuPermission.catalogueEnquiry).toLowerCase() == 'none') {

                            catalogues = true;
                            document.getElementById('catalogueMob').style.display = 'none';
                            document.getElementById('catalogueRaiseMob').style.display = 'none';
                            //redirct = 1;
                            if (sessionStorage.getItem('page_redirect') == '0') {
                                sessionStorage.removeItem('page_redirect');
                            } else {
                                sessionStorage.setItem('page_redirect', '1');
                            }


                        } else {
                            // write logic to show add new product

                            //document.getElementById('catalogueRaiseMob').style.display = 'block';
                        }

                        if ((item.permissions.catalogAdminPermission.addNewProducts).toLowerCase() == 'none') {
                            cataloguesRaise = true;
                            document.getElementById('catalogueArrowSpan').style.display = 'none';
                            document.getElementById('catalogueRaiseMob').style.display = 'none';


                        } else {
                            // write logic to show add new product

                            //document.getElementById('catalogueRaiseMob').style.display = 'block';
                        }

                        if ((item.permissions.menuPermission.reports).toLowerCase() == 'none') {
                            reports = true;
                            document.getElementById('reportsMob').style.display = 'none';

                        }

                        if ((item.permissions.menuPermission.accessMgmt).toLowerCase() == 'none') {
                            accessMgmt = true;
                            document.getElementById('permissonsMob').style.display = 'none';
                        }
                    }
                });

                if (redirct == 1) {
                    redirct = 0;
                    this.redirect('/reports', this.customerName);
                }

                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                this.disabledReports = reports;
                this.disabledOrder = orders;
                this.disabledOrderRaise = orders;
                this.disabledCatalogue = catalogues;
                this.disabledCatalogueRaise = cataloguesRaise;
                this.disabledPermission = accessMgmt;
                this.disabledManageCatalogue = catManagement;
                //  this.getAllCatalogues();
                //this.getAllMasterData();
            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);
            console.log(screen);
            // logic for resize pop up 
            window.onresize = () => {
                if (screen == "desktop") {

                    if (window.innerWidth < this._globalFunc.stdWidth) {
                        // this.catalogueResult = false;

                    } else {
                        //  this.catalogueResult = true;
                    }
                } else {

                    if (window.innerWidth > this._globalFunc.stdWidth) {
                        console.log('in function');
                        //   this.catalogueResult = true;
                        // document.getElementById("resultModalClose").click();
                    } else {
                        //  this.catalogueResult = false;

                    }
                }

                // get device to populate some part of ui
                screen = this._globalFunc.getDevice(window.innerWidth);

            };
            window.onscroll = () => {
                //    this.scrollFunction();
            };
        }
    }
    onScrolltop() {
        this._globalFunc.autoscroll();
    }

    scrollFunction() {
        console.log(document.body.scrollTop);
        if (document.body.scrollTop > 800 || document.documentElement.scrollTop > 800) {
            document.getElementById("topBtn").style.display = "block";
        } else {
            document.getElementById("topBtn").style.display = "none";
        }
    }

    // navigate function
    redirect(page: any, orgType: any) {
        this._router.navigate([page, orgType]);
    }

    refresh() {
        //window.location.reload();
        this.ngOnInit();
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
    }

    toggleOrganisation() {
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            //  document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
            border.classList.remove('searchborder');
        } else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            // document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
            border.classList.add('searchborder');

        }
    }
}