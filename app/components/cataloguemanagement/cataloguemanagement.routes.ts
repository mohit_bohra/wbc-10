import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogueManagementComponent } from './cataloguemanagement.component';
import { SupplyDepotResolve } from './maintainsupplydepot/supplydepot.resolve.service';
import { SupplyDepotCategoryDDResolve } from './maintainsupplydepot/supplydepotCategoryDD.resolve.service';

// route Configuration
const catalogueManagementRoutes: Routes = [
  { path: '', component: CatalogueManagementComponent },
  { path: 'review/data', loadChildren: './reviewprocess/datateam/data.module#DataModule' },
  { path: 'review/supplychain', loadChildren: './reviewprocess/supplychainteam/supplychain.module#SupplychainModule' },
  { path: 'review/commercial', loadChildren: './reviewprocess/commercialteam/commercial.module#CommercialModule' },
  { path: 'npr/singleproductrequest', loadChildren: './singleproductrequest/singleproductrequest.module#SingleProductRequestModule' },
  { path: 'npr/errorcorrection', loadChildren: './errorcorrection/errorcorrection.module#ErrorCorrectionModule' },
  { path: 'npr/batchlines', loadChildren: './batchlines/batchlines.module#BatchLinesModule' },
  { path: 'npr/manage', loadChildren: './npr/managerequest.module#ManageRequestModule' },
  { path: 'productpricing', loadChildren:'./productpricing/productpricing.module#ProductPricingModule'},
  { path: 'nisfeederexport', loadChildren: './nisfeederexport/nisfeederexport.module#NisFeederExportModule' },
  { path: 'fullrangereview', loadChildren: './reviewprocess/fullrange/fullrangereview.module#fullrangereviewModule' },


  { path: 'maintainsupplydepot', loadChildren: './maintainsupplydepot/maintainsupplydepot.module#MaintainSupplyDepotModule', resolve: {supplyDepot: SupplyDepotResolve, supplyDepotCategoryDD: SupplyDepotCategoryDDResolve} },
  { path: 'maintainallocationpriority', loadChildren: './maintainallocationpriority/maintainallocationpriority.module#MaintainallocationpriorityModule' },
  //{ path: 'addstore', loadChildren: './addstore/addstore.module#AddstoreComponentModule' }



  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(catalogueManagementRoutes)],
  exports: [RouterModule],
  providers: [SupplyDepotResolve, SupplyDepotCategoryDDResolve]
})
export class CatalogueManagementComponentRoutes { }
