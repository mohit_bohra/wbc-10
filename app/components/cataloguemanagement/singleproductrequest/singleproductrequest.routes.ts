import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SingleProductRequestComponent } from './singleproductrequest.component';

// route Configuration
const singleproductrequestRoutes: Routes = [
  { path: '', component: SingleProductRequestComponent },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(singleproductrequestRoutes)],
  exports: [RouterModule]
})
export class SingleProductRequestRoutes { }
