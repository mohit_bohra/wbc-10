import { Component } from '@angular/core';
// import { DomSanitizer } from '@angular/platform-browser';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'singleproductrequest.component.html',
    providers: [commonfunction]
})
export class SingleProductRequestComponent {

    objConstant = new Constant();
    whoImgUrl: any;
    catalogueCode: any;
    hideImgindex: any;

    productid: string;
    errorMsg: boolean;
    errorMsgString: string;
    showLoader: boolean;
    printHeader: boolean;
    organisationList: any;
    disabledOrder: boolean;

    pageState: number = 13;
    customerName: string;
    orgimgUrl: string;
    orgApiName: string;
    productDetails: any;

    productName: any;
    mapLen: any;
    description: any;
    homeUrl: any;
    private _sub: any;
    availabilityMaster: any;
    organisationListChange: any;
    WorkflowMaster: any;
    catalogueTypeMaster: any;
    availabilityStatus: string;
    workflowStatusName: string;
    mapTypesMaster: any;
    mapTypesMasterDisplay: any;
    lookupTypesMaster: any;
    lookupTypesMasterDisplay: any;
    disabledCatalogue: boolean;
    financialTypesMaster: any;
    financialTypesMasterDisplay: any;
    financialCurrencies: any;
    catalogueType: string;
    imageUrl: string;
    productMaps: any;
    productPrices: any;
    productmin: string;
    maptypeValue: string;
    isProductIdEntered: boolean;
    disabledManageCatalogue: boolean;
    myModelLmp: any;

    showSuccessModel: boolean;
    minDate: any;
    addedProdId: any;
    addedMin: any;
    identifierRow: number = 1;
    pricingRow: number = 1;
    attributeRow: number = 1;

    prodIdentifierRowHTML: any;

    todayDateStr: string;

    hideOrganisation: boolean = true;
    tempCalId: any;
    calDefaultDate: Date;

    allowWrite: boolean;

    requestType: any;
    requestChannel: any;
    start: number;
    limit: number;
    count: number;

    catalogueCodeSelected: any;
    catalogueDetailList: any;
    cataloguedetailListLength: any;
    hitNext: boolean;
    lmpNumbers: any;
    lpmlist: string;
    setlpmlist: string;
    batchnumber: any;

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction) {


        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;

        // set default date null to calender
        this.errorMsgString = '';
        this.errorMsg = false;

        location.onPopState(() => {

            console.log('pressed back!');
            document.getElementById('close-popup').click();

        });
    }

    // function works on page load

    ngOnInit() {

        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {


            this.printHeader = true;
            this.lpmlist = '';

            this.setlpmlist = '';

            this.availabilityStatus = '';
            this.workflowStatusName = '';
            this.catalogueType = '';
            this.productid = '';
            this.productName = '';
            this.productmin = '';
            this.description = '';
            this.imageUrl = this.whoImgUrl;
            this.isProductIdEntered = false;
            this.allowWrite = false;
            this.disabledCatalogue = false;
            this.requestType = '';
            this.requestChannel = '';
            this.disabledManageCatalogue = false;

            this.tempCalId = "";
            //this.calDefaultDate = "30/07/2017";

            // window scroll function
            this._globalFunc.autoscroll();
            this.maptypeValue = '';

            this.minDate = new Date();
            /*this.minDate.setMonth(prevMonth);
            this.minDate.setFullYear(prevYear);*/
            this.todayDateStr = this._cfunction.todayDateStrFunction();
            this.start = 0;
            this.limit = this.objConstant.SCROLL_RECORD_LIMIT;
            this.count = 0;
            this.hitNext = false;

            // set page state variable
            this.pageState = 13;
            this.identifierRow = 1;
            this.pricingRow = 1;
            this.attributeRow = 1;
            sessionStorage.setItem('pageState', '13');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                this.showLoader = true;
                this.allowWrite = false;
                //alert("in");

                //    let sessionModal = document.getElementById('addConfirmationModal');
                //    document.getElementById('addConfirmationModal').style.display = 'none';

                this.customerName = params.orgType;
                let cusType = params.orgType;

                let orgImgUrl;
                let orgApiNameTemp;
                let orders;

                let writePermission;
                let catalogues;

                let changeOrgAvailable = new Array;
                let catManagement;

                // list of org assigned to user
                this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));
                // this.disabledCusChange = false;
                this.organisationList.forEach(function (item: any) {
                    if (item.orgType !== cusType) {
                        changeOrgAvailable.push(item);
                    }
                });

                this.organisationListChange = changeOrgAvailable;
                /*    document.getElementById('orderArrowSpan').style.display = 'block';
                    document.getElementById('orderMob').style.display = 'block';
                    document.getElementById('orderRaiseMob').style.display = 'none';
    
                    document.getElementById('catalogueMob').style.display = 'block';
                    document.getElementById('catalogueArrowSpan').style.display = 'none';
                    document.getElementById('catalogueRaiseMob').style.display = 'none';
                    document.getElementById('reportsMob').style.display = 'block';
                    document.getElementById('permissonsMob').style.display = 'block';*/


                this.organisationList.forEach(function (item: any) {
                    if (item.orgType === cusType) {
                        // logic for permission set
                        orgImgUrl = item.imgUrl;
                        orgApiNameTemp = item.orgName;

                        if ((item.permissions.menuPermission.orders).toLowerCase() == 'none') {
                            orders = true;
                            document.getElementById('orderMob').style.display = 'none';
                            // add disa class for orderRaiseMob
                            document.getElementById('orderRaiseMob').style.display = 'none';

                        } else {
                            orders = false;
                            // write logic to show raise new order
                            document.getElementById('orderMob').style.display = 'block';
                            // write logic to show order
                            document.getElementById('orderRaiseMob').style.display = 'block';

                        }

                        if ((item.permissions.menuPermission.raiseNewOrder).toLowerCase() == 'none') {
                            document.getElementById('orderArrowSpan').style.display = 'none';
                            document.getElementById('orderRaiseMob').style.display = 'none';

                        } else {
                            // write logic to show raise new order

                        }

                        if ((item.permissions.menuPermission.catalogueEnquiry).toLowerCase() == 'none') {
                            catalogues = true;
                            document.getElementById('catalogueMob').style.display = 'none';
                            document.getElementById('catalogueRaiseMob').style.display = 'none';
                        } else {
                            catalogues = false;
                            // write logic to show add new product
                            document.getElementById('catalogueMob').style.display = 'block';
                            document.getElementById('catalogueRaiseMob').style.display = 'block';

                        }

                        if ((item.permissions.catalogAdminPermission.addNewProducts).toLowerCase() == 'none') {
                            document.getElementById('catalogueArrowSpan').style.display = 'none';
                            document.getElementById('catalogueRaiseMob').style.display = 'none';


                        } else {
                            // write logic to show add new product

                            //document.getElementById('catalogueRaiseMob').style.display = 'block';
                        }


                        if ((item.permissions.menuPermission.catalogueManagement).toLowerCase() == 'none') {
                            catManagement = true;
                        } else {
                            catManagement = false;
                        }

                        if ((item.permissions.menuPermission.reports).toLowerCase() == 'none') {
                            document.getElementById('reportsMob').style.display = 'none';

                        }

                        if ((item.permissions.menuPermission.accessMgmt).toLowerCase() == 'none') {
                            document.getElementById('permissonsMob').style.display = 'none';
                        }

                    }
                });

                this.allowWrite = writePermission;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                this.disabledOrder = orders;
                this.disabledCatalogue = catalogues;
                this.disabledManageCatalogue = catManagement;

            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);


            // logic for resize pop up 
            window.onresize = () => {

                if (screen == "desktop") {
                    /*   document.getElementById('prouctDetailTitle').style.display = 'block';
                       document.getElementById('productDetailHeader').style.display = 'block';
                       document.getElementById('productDetailImg').style.display = 'block';*/
                }

                screen = this._globalFunc.getDevice(window.innerWidth);

            };
            this.showLoader = false;
        }
    }

    // key press event to disable special charater
    keyPress(event: any) {
        this._globalFunc.alphaNumericWithSpace(event);
    }

    cancelPopup() {
        //document.getElementById('singleproductrequest').click();
        this.closeLPMPopup();
        this.cataloguedetailListLength = 0;
        let lmpsetbtn = document.getElementById('lmpsetbtn');
        lmpsetbtn.classList.add('disable');
        let productcode = document.getElementById('productcode');
        productcode.textContent = '';
        this.catalogueCode = '';
    }

    setLPMnumbers(lmpNumbers: any) {
        console.log(lmpNumbers.toLocaleString());
        var res = lmpNumbers.toLocaleString().replace(/[, ]+/g, ",").trim();
        this.setlpmlist = res;
        //document.getElementById('singleproductrequest').click();
        let lmpsetbtn = document.getElementById('lmpsetbtn');
        lmpsetbtn.classList.add('disable');
        this.cataloguedetailListLength = 0;
        console.log(this.requestChannel);
        this.closeLPMPopup();

    }

    closeLPMPopup(){
        let sessionModal = document.getElementById('singleproductrequest');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('singleproductrequest').style.display = 'none';
    }
    settextArea(lmp: any) {
        if(this.requestChannel != '' && this.requestChannel != null){
        //this.closePopup();
        console.log("requestChannel not null");
        let list;
        if (lmp.indexOf('\n') != -1) {
            list = lmp.split('\n');
            console.log('indexof');
        } else {
            if (lmp.indexOf(',') != -1) {
                list = lmp.split(',');
                console.log('comma');
            } else {
                console.log('direct');
                list = lmp;
            }
        }
        console.log(list);
        this.lpmlist = list;
        let sessionModal = document.getElementById('singleproductrequest');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('singleproductrequest').style.display = 'block';
        } else {
            let sessionModal1 = document.getElementById('errorChannel');
            sessionModal1.classList.remove('in');
            sessionModal1.classList.add('out');
            document.getElementById('errorChannel').style.display = 'block';

            // Error - Requesting channel is mandatory
            /*let sessionModal = document.getElementById('singleproductrequest');
            sessionModal.classList.remove('out');
            sessionModal.classList.add('in');
            document.getElementById('singleproductrequest').style.display = 'none';*/
        }
    }

    getLPMNumber(lmpNumbers: any, index: any) {
        var res = '';
        if (this.lpmlist == '') {
            if (this.lpmlist.indexOf(lmpNumbers) == -1) {
                this.lpmlist = this.lpmlist.concat(lmpNumbers, ',');
              
        res = this.lpmlist.toLocaleString().replace(/[, ]+/g, ",").trim();
        res = res.substr(0, res.length - 1);
            }
        } else {
            console.log(this.lpmlist.length);
            if (this.lpmlist.length <= 35) {
                if (this.lpmlist.indexOf(lmpNumbers) == -1) {
                    this.lpmlist = this.lpmlist.concat(',', lmpNumbers);
                    console.log(this.lpmlist);
                }
            } else if (this.lpmlist.length > 1 && this.lpmlist.length < 28 || this.lpmlist.length > 28) {
                if (this.lpmlist.indexOf(lmpNumbers) == -1) {
                    this.lpmlist = this.lpmlist.concat(',', lmpNumbers);
                }
                console.log(this.lpmlist);
                console.log(this.lpmlist.length);
            } else {
                console.log(this.lpmlist.length);
            }
        }
        res = this.lpmlist.toLocaleString().replace(/[, ]+/g, ",").trim();
        var n = this.lpmlist.search(",") ;
        if( n > 0 ){
            if(n == res.length - 1){
               res = res.substr(0, res.length - 1);
            }             
        }
        
        this.lpmlist = res;
        let lmpsetbtn = document.getElementById('lmpsetbtn');
        lmpsetbtn.classList.remove('disable');
        this.catalogueCode = '';

    }

    // get all catalogue detail
    getCatalogueItem(value: any) {
        //this.requestChannel = 'fresh';
        if (this.requestChannel == '' || this.requestChannel == null) {
            console.log('err');
        } else {
            let str = value.productcode;
            //let searchTerm = 'fresh'; //this.requestChannel; 
            let searchTerm = this.requestChannel;
            console.log(searchTerm);

            this.showLoader = true;
            this.start = 0;
            this.count = 0;
            // console.log(value);
            this._postsService.getProductDetailsUsingItemId(str).subscribe(data => {

                this.cataloguedetailListLength = 1;
                console.log(data);
                let legacyItemNumbers = [];
                legacyItemNumbers = data.legacyItemNumbers;
                console.log(legacyItemNumbers.length);

                let legacyCount = legacyItemNumbers.length;
                let packCount = data.packs.length;
                let iloopCnt;
                let jLoopCnt;
                let showResult;
                showResult = true;
                iloopCnt = legacyCount;
                jLoopCnt = packCount;
                var searchTermBool = false;

                if (legacyCount == 0) {
                    showResult = false;
                }

                if (data.itemNumber == str) {
                    // true
                    searchTermBool = true;

                }
                if (data.parentItemNumber == str) {
                    showResult = false;
                }

                var cataItemArray = new Array;

                if (showResult == true) {
                    let packarr = '';
                    if (legacyCount > 1 || packCount > 1) {
                        console.log('multile');
                        for (let i = 0; i < iloopCnt; i++) {
                            console.log('i=' + i);
                            if (searchTermBool == true) {
                                // if (data.legacyItemNumbers[i] == str) {
                                for (let j = 0; j < jLoopCnt; j++) {

                                    if (data.packs.length == 0) {
                                        packarr = ''
                                    } else {
                                        packarr = data.packs[j].packNumber;
                                    }
                                    console.log('j=' + j);
                                    cataItemArray.push('L' + data.legacyItemNumbers[i] + '-P' + packarr + '-M' + data.itemNumber + ':' + data.itemDescription + ':' + data.status);
                                }
                                // }
                            } else {
                                if (data.legacyItemNumbers[i] == str) {
                                    for (let j = 0; j < jLoopCnt; j++) {

                                        if (data.packs.length == 0) {
                                            packarr = ''
                                        } else {
                                            packarr = data.packs[j].packNumber;
                                        }
                                        console.log('j=' + j);
                                        cataItemArray.push('L' + data.legacyItemNumbers[i] + '-P' + packarr + '-M' + data.itemNumber + ':' + data.itemDescription + ':' + data.status);
                                    }
                                }
                            }
                        }
                    } else {
                        console.log('single');
                        if (data.packs.length == 0) {
                            packarr = ''
                        } else {
                            packarr = data.packs[0].packNumber;
                        }
                        cataItemArray.push('L' + data.legacyItemNumbers[0] + '-P' + packarr + '-M' + data.itemNumber + ':' + data.itemDescription + ':' + data.status);
                    }
                    this.catalogueDetailList = cataItemArray;
                    console.log(this.catalogueDetailList);
                } else {
                    this.catalogueDetailList = '';
                    console.log(this.catalogueDetailList);
                    this.showLoader = false;
                    this.errorMsgString = "Invalid Product Code";
                    document.getElementById('addProdErrMsg').style.display = 'block';
                    this._globalFunc.autoscroll();
                    setTimeout(() => {
                        document.getElementById('addProdErrMsg').style.display = 'none';
                        this.catalogueCode = '';
                    }, 3000);
                }
                this.showLoader = false;
            }
                , err => {
                    this.showLoader = false;
                    this.errorMsgString = "Invalid Product Code";
                    document.getElementById('addProdErrMsg').style.display = 'block';
                    this._globalFunc.autoscroll();
                    setTimeout(() => {
                        document.getElementById('addProdErrMsg').style.display = 'none';
                        this.catalogueCode = '';
                    }, 3000);
                }
            );
        }
    }

    formatString(val: any, num: any) {
        var tmpStr = val.split(':');
        console.log(tmpStr[num]);
        return tmpStr[num];
    }


    // navigate function
    redirect(page: any, orgType: any) {
        this._router.navigate([page, orgType]);
    }

    onBlurMethod() {
        let varArry = this.myModelLmp.split(" ");
        this.myModelLmp = varArry;
    }

    refresh() {
        //window.location.reload();
        this.ngOnInit();
    }

    closePopup() {
        let sessionModal = document.getElementById('addConfirmationModal');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('addConfirmationModal').style.display = 'none';
    }

    closePopupError(){
        let sessionModal = document.getElementById('errorLPMConfirmationModal');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('errorLPMConfirmationModal').style.display = 'none';
    }

    closePopupErrorChannel(){
        let sessionModal = document.getElementById('errorChannel');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('errorChannel').style.display = 'none';
    }

    //createNewRequest
    createNewRequest(value: any, formname: any) {
        if (formname == 'cnrequset' && value.comment != '') {
            console.log(value);
            let lpmArr;
            let custName;
            let bdescription;
            let requestChannel;
            let requestType;
            let lpms;
            requestChannel = value.requestChannel;
            requestType = value.requestType;
            bdescription = value.bdescription;
            lpmArr = value.comment;
            custName = this.customerName;
            if (lpmArr.indexOf(/ /g) == -1) {
                console.log('abc');
                lpmArr = lpmArr.replace(/ /g, ',');
            } else {
                console.log('pqy');
            }

            if (lpmArr.indexOf(/\n/g) == -1) {
                console.log('newline');
                lpmArr = lpmArr.replace(/\n/g, ',');
            } else {
                console.log('no new line');
            }
            lpms = lpmArr.split(",");
            console.log(lpms.length);
            var newRequset;
            var reqeustDetailsStr;

            // generate reqeustDetailsStr
            if (lpms.length == 1) {
                reqeustDetailsStr = '{"lpm":"' + lpmArr + '"}';
            } else {
                console.log('in');
                var strDtls = '';
                for (let i = 0; i < lpms.length; i++) {
                    strDtls += '{"lpm":"' + lpms[i] + '"},';
                }
                strDtls = strDtls.substr(0, strDtls.length - 1);

                reqeustDetailsStr = strDtls;
            }

            // generate new cnrequset
            newRequset = '{"requestHeaders":';
            newRequset += '[{"customer":"' + custName;
            newRequset += '","channel":"' + requestChannel;
            newRequset += '","requestType":"' + requestType;
            newRequset += '","batchDescription":"' + bdescription;
            newRequset += '","createdBy":"' + sessionStorage.getItem('username');
            newRequset += '","requestDetails": [';
            newRequset += reqeustDetailsStr;
            newRequset += ']}]}';
            console.log(newRequset);
            this.showLoader = true;

            this._postsService.addNewCreateReq(this.orgApiName, newRequset).subscribe(data => {
                this.showLoader = false;
                this.showSuccessModel = true;

                this.requestType = '';
                this.requestChannel = '';
                this.lpmlist = '';
                this.setlpmlist = '';
                this.description = '';
                //console.log(data.requestHeaders[0].batchId);
                this.batchnumber = data.requestHeaders[0].batchId;
                if (this.batchnumber == 0) {
                    let sessionModal = document.getElementById('errorLPMConfirmationModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('errorLPMConfirmationModal').style.display = 'block';
                } else {
                    let sessionModal = document.getElementById('addConfirmationModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('addConfirmationModal').style.display = 'block';
                }
            }
                , err => {
                    this.showLoader = false;
                    this._router.navigate(['/error404']);
                }
            );
        }
    }



    ngOnDestroy() {
        // clean sub to avoid memory leak
        //  document.getElementById('close-popup').click();

    }
}
