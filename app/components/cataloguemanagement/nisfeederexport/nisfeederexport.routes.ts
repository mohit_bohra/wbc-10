import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NisFeederExportComponent } from './nisfeederexport.component';

// route Configuration
const nisfeederexportRoutes: Routes = [
  { path: '', component: NisFeederExportComponent },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(nisfeederexportRoutes)],
  exports: [RouterModule]
})
export class NisFeederExportRoutes { }
