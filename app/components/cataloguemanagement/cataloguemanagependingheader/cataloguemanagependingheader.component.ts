import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { Services } from '../../../services/app.services';
import { Router, ActivatedRoute } from '@angular/router';
import { Constant } from '../../constants/constant';
import { GlobalComponent } from '../../global/global.component';

@Component({
    selector: 'catalogue-management-pending-request-header',
    templateUrl: 'cataloguemanagependingheader.component.html',
})
export class CatalogueManagePendingHeaderComponent {

    errorMsgString: string;
    siteMSg: string;
    siteMaintanceFlg: boolean;
    objConstant = new Constant();
    whoImgUrl: string;
    showLoader: boolean;
    orgApiName: string;
    dataTeamPendingRequests: number = 0;
    commercialPendingRequests: number = 0;
    supplyChainPendingRequests: number = 0;
    totalPendingRequests: number = 0;
    _sub: any;
    customerName: any;
    organisationList: any;
    channels: string;
    requesttypes: string;
    teamname: string;
    requestid: string;
    organisationListChange: any;
    orgimgUrl: string;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disableduploadCatalogue: boolean;
    disabledOrder: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    disabledDashboard: boolean = false;
    showPendingReq: boolean;
    donotshow: boolean;
    showLogo:boolean = true;
    hideOrganisation: boolean = true;
    custPermission: any[];
    imgName: string = "";

    constructor(private _globalFunc: GlobalComponent, private _postsService: Services, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.showLoader = false;
        this.channels = "@all";
        this.requesttypes = "@all";
        this.teamname = "@all";
        this.requestid = "@all";
        this.showPendingReq = true;
        this.donotshow = false;
    }

    // function works on page load
    ngOnInit() {
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {

            this.disabledCusChange = false;
            this.disabledPermission = false;
            this.disabledReports = false;
            this.disabledCatalogue = false;
            this.disabledOrder = false;
            this.disabledOrderRaise = false;
            this.disabledCatalogueRaise = false;
            this.disabledManageCatalogue = false;

            // subscribe to route params
            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                let reports;
                let orders;
                let catalogues;
                let uploadCatalogue;
                let cataloguesRaise;
                let accessMgmt;
                let redirct;
                let changeOrgAvailable = new Array;
                let catManagement;
                let dashboardAccess;

                this.imgName = this.getCustomerImage(params.orgType);

                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
                // list of org assigned to user
                // this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));

                // this.organisationList.forEach(function (item: any) {
                //     if (item.orgType !== cusType) {
                //         changeOrgAvailable.push(item);
                //     }
                // });
                // this.organisationListChange = changeOrgAvailable;
                
                // // this.disabledCusChange = false;
                // if (this.organisationList.length == 1) {
                //     this.disabledCusChange = true;
                // }

                // document.getElementById('orderArrowSpan').style.display = 'block';
                // document.getElementById('orderMob').style.display = 'block';
                // document.getElementById('orderRaiseMob').style.display = 'none';

                // document.getElementById('catalogueMob').style.display = 'block';
                // document.getElementById('catalogueArrowSpan').style.display = 'none';
                // document.getElementById('catalogueRaiseMob').style.display = 'none';
                // document.getElementById('reportsMob').style.display = 'block';
                // document.getElementById('permissonsMob').style.display = 'block';

                // if (document.getElementById('orderArrow').classList.contains('glyphicon-menu-up') === true) {
                //     let sublinks = document.getElementById('orderArrow');
                //     sublinks.classList.remove('glyphicon-menu-up');
                //     sublinks.classList.add('glyphicon-menu-down');
                //     document.getElementById('orderRaiseMob').style.display = 'none';
                // }

                // if (document.getElementById('catalogueArrow').classList.contains('glyphicon-menu-up') === true) {
                //     let sublinks = document.getElementById('catalogueArrow');
                //     sublinks.classList.remove('glyphicon-menu-up');
                //     sublinks.classList.add('glyphicon-menu-down');
                //     document.getElementById('catalogueRaiseMob').style.display = 'none';
                // }

                // let custPerm: any = [];
                // let that = this;
                // this.organisationList.forEach(function (item: any) {
                //     if (item.orgType === cusType) {
                //         // logic for permission set
                //         orgImgUrl = item.imgUrl;
                //         orgApiNameTemp = item.orgName;

                //         if ((item.permissions.menuPermission.orders).toLowerCase() == 'none') {
                //             orders = true;
                //             document.getElementById('orderMob').style.display = 'none';
                //             document.getElementById('orderRaiseMob').style.display = 'none';
                //         }

                //         if ((item.permissions.catalogAdminPermission.uploadCatalogue).toLowerCase() == 'none') {
                //             uploadCatalogue = true;
                //         } else {
                //             uploadCatalogue = false;
                //         }

                //         if ((item.permissions.menuPermission.raiseNewOrder).toLowerCase() == 'none') {
                //             document.getElementById('orderArrowSpan').style.display = 'none';
                //             document.getElementById('orderRaiseMob').style.display = 'none';

                //         }

                //         if ((item.permissions.menuPermission.catalogueManagement).toLowerCase() == 'none') {
                //             catManagement = true;
                //         } else {
                //             catManagement = false;
                //         }


                //         if ((item.permissions.menuPermission.catalogueEnquiry).toLowerCase() == 'none') {

                //             catalogues = true;
                //             document.getElementById('catalogueMob').style.display = 'none';
                //             document.getElementById('catalogueRaiseMob').style.display = 'none';
                //             redirct = 1;
                //             if (sessionStorage.getItem('page_redirect') == '0') {
                //                 sessionStorage.removeItem('page_redirect');
                //             } else {
                //                 sessionStorage.setItem('page_redirect', '1');
                //             }


                //         } else {
                //             // write logic to show add new product

                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.catalogAdminPermission.addNewProducts).toLowerCase() == 'none') {
                //             cataloguesRaise = true;
                //             document.getElementById('catalogueArrowSpan').style.display = 'none';
                //             document.getElementById('catalogueRaiseMob').style.display = 'none';


                //         } else {
                //             // write logic to show add new product

                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.menuPermission.reports).toLowerCase() == 'none') {
                //             reports = true;
                //             document.getElementById('reportsMob').style.display = 'none';

                //         }

                //         if ((item.permissions.menuPermission.accessMgmt).toLowerCase() == 'none') {
                //             accessMgmt = true;
                //             document.getElementById('permissonsMob').style.display = 'none';
                //         }

                //         if ((item.permissions.menuPermission.dashboards).toLowerCase() == 'none') {
                //             dashboardAccess = true;
                //         //    document.getElementById('permissonsMob').style.display = 'none';
                //         }
                //         custPerm[item.orgType] = that._globalFunc.checkPermissionAvailableForCustomer(item.permissions.menuPermission);
                //     }
                // });
                
                /*if (redirct == 1) {
                    redirct = 0;
                    this.redirect('/reports', this.customerName);
            }*/

                // this.custPermission = custPerm;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                // this.disabledReports = reports;
                // this.disabledOrder = orders;
                // this.disabledOrderRaise = orders;
                // this.disableduploadCatalogue = uploadCatalogue;
                // this.disabledCatalogue = catalogues;
                // this.disabledCatalogueRaise = cataloguesRaise;
                // this.disabledPermission = accessMgmt;
                // this.disabledManageCatalogue = catManagement;
                // this.disabledDashboard = dashboardAccess;
                this.getPendingRequests();
                this.showToggleCustomerLink();
            });
        }
    }

    // get relevant order detail
    getPendingRequests() {
        this.showLoader = true;
        let hidePendingReq4PageState = ['23', '15','16', '19','25','26','33','51','52','53','54','55', '56'];
        this._postsService.getCataloguePendingRequests(this.orgApiName, this.channels, this.requesttypes, this.teamname, this.requestid).subscribe(data => {
            this.showLoader = false;
            if (sessionStorage.getItem('pageState') == '9') {
                this.showPendingReq = true;
                this.donotshow = false;
                let borderClass = document.getElementById('setBorder');
                borderClass.classList.remove('border-right-yellow');
            }
            else if (sessionStorage.getItem('pageState') == '999') {
                this.showPendingReq = true;
                this.donotshow = false;
                let borderClass = document.getElementById('setBorder');
                borderClass.classList.remove('border-right-yellow');
            } else if(sessionStorage.getItem('pageState') == '22'){
                this.donotshow = false;
                this.showPendingReq = true;
                this.showLogo = false;
            } else if(sessionStorage.getItem('pageState') == '30'){
                this.donotshow = true;
                this.showPendingReq = true; 
                this.showLogo = true;
            }
            else if(sessionStorage.getItem('pageState') == '16' || sessionStorage.getItem('pageState') == '33' || sessionStorage.getItem('pageState') == '54'){
                // this.donotshow = true;
                // this.showPendingReq = false; 
            }else if(hidePendingReq4PageState.indexOf(sessionStorage.getItem('pageState')) >= 0){
                this.donotshow = false;
                this.showPendingReq = true;
            } else {
                this.showPendingReq = true;
                this.donotshow = true;
                let borderClass = document.getElementById('setBorder');
                borderClass.classList.add('border-right-yellow');
            }

            this.dataTeamPendingRequests = data.pendingRequestSummary.pendingDataTeamReviewCount;
            this.commercialPendingRequests = data.pendingRequestSummary.pendingCommercialReviewCount;
            this.supplyChainPendingRequests = data.pendingRequestSummary.pendingSupplychainReviewCount;
            this.totalPendingRequests = this.supplyChainPendingRequests + this.commercialPendingRequests + this.dataTeamPendingRequests;

        }, err => {
            this.showLoader = false;
            this._router.navigate(['/error404']);
        });
    }

    // navigate function
    redirect(page: any, orgType: any) {
        this._router.navigate([page, orgType]);
    }

    redirect_review(page: any, orgType: any) {
        if(page == '/datareview')
            this._router.navigate(['cataloguemanagement', orgType, 'review','data']);
        else if(page == '/commercialreview')
            this._router.navigate(['cataloguemanagement', orgType, 'review','commercial']);
        else if(page == '/supplychainreview')
            this._router.navigate(['cataloguemanagement', orgType, 'review','supplychain']);
        else 
            this._router.navigate([page, orgType]);
    }

    toggleOrganisation() {
        this._globalFunc.logger("Toggle customer list.");
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('setBorder');
            border.classList.remove('searchborder');
        }
        else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('setBorder');
            border.classList.add('searchborder');
        }
    }

    // change organisation
    changeOrgType(orgType: string) {
        console.log(orgType);
        let pageState = sessionStorage.getItem('pageState');
        switch(pageState){
            case '9': // cataloguement management landing screen
                this._router.navigate(this._globalFunc.redirectToComponent('cataloguemanagement', orgType));
                //this._router.navigate(['cataloguemanagement', orgType]);
                break;
            case '16': 
                this._router.navigate(this._globalFunc.redirectToComponent('catalogueupload',orgType));
                break;
            case '999': // cataloguement management landing screen
                this._router.navigate(this._globalFunc.redirectToComponent('cataloguemanagement', orgType));
                //this._router.navigate(['cataloguemanagement', orgType]);
                break;    
            case '26': // Configuration
                // check user have permission to product configuration and link is enabled
                this.checkProductConfiguration(orgType);
                break;
            case '25': // Product repricing
                // check user have permission to product repricing and link is enabled
                this.checkProductRepricing(orgType);
                break;
            case '33': // Product repricing
                // check user have permission to product repricing and link is enabled
                this.checkPeriodicRepricing(orgType);
                break;
            case '51': // Product repricing
                // check user have permission to product repricing and link is enabled
                this._router.navigate(this._globalFunc.redirectToComponent('maintainSupplyDepot', orgType));
                break;
            case '54': // Store Bulk Upload
                // check user have permission to store bulk upload view and link is enabled
                this._router.navigate(this._globalFunc.redirectToComponent('storebulkupload', orgType));
                break;
            case '56': // Store Bulk Upload
                // check user have permission to store bulk upload view and link is enabled
                this._router.navigate(this._globalFunc.redirectToComponent('storeExport', orgType));
                break;
        }
        this.ngOnInit();
        this.toggleOrganisation();
    }

    showToggleCustomerLink(){
        let pageState = sessionStorage.getItem('pageState');
        this.disabledCusChange = true;
        switch(pageState){
            case '9':
            case '16':
            case '999':
            case '26':
            case '25':
            case '33':
            case '51':
            case '54':
            case '56':
                this.disabledCusChange = false;
                break;
        }
    }

    checkProductConfiguration(orgType: any){
        console.log("configuration menu : "+orgType);
        let that = this;
        // let orgType = (sessionStorage.getItem('orgType'));
        // this.organisationList.forEach(function (item: any) {
        //     if (item.orgType === orgType) {
                that._globalFunc.logger("check User have permission to access product configuration");
                if (this.checkPermission(orgType, "pricingConfiguration")) {
                    that._globalFunc.logger("User having permission to access configuration");
                    // User have permission to access configuration
                    that._postsService.getPricingStatus(orgType, "").subscribe((responseData:any) => {
                        that._globalFunc.logger("Pricing status for configuration");
                        that._globalFunc.logger(responseData);
                        if (responseData.count == 0) {
                            that._router.navigate(['cataloguemanagement', orgType, 'productpricing', 'configuration']);
                        } else {
                            that._globalFunc.logger("check User have permission to access product identification");
                            /* if((item.permissions.catalogManagementPermission.priceIncreaseReviewPermission.productsIdentification).toLowerCase() != 'none') {
                                that._router.navigate(['cataloguemanagement', orgType, 'productpricing']);
                            } else { */
                                that._router.navigate(['cataloguemanagement', orgType]);
                            /* } */
                        }
                    }, (err:any) => {
                        that._globalFunc.logger("Error while checking count for configuration");
                    });
                } else {
                    that._globalFunc.logger("Permission has not given to configuration");
                }
        //     }
        // });
    }

    checkPermission(orgType:string, screen2Check:string){
        let that = this;
        let catalogueManagementMenuPermission = false;
        let returnStatus = false;
        this.organisationList.forEach(function (organisation: any) {
            if (organisation.customerName.toUpperCase() == orgType.toUpperCase()) {
                console.log("organisation : "+orgType);
                //console.log(organisation);
                organisation.permissionRoleList.forEach(function(permRoleList: any){
                    //console.log("11");
                    //console.log(permRoleList);
                    permRoleList.permissionDetailsList.forEach(function(permRoleDtlList: any){
                        if(permRoleDtlList.permissionGroup == 'catalogueManagement' || permRoleDtlList.permissionGroup == 'morrisonsPermissions'){
                            permRoleDtlList.permissionNameList.forEach(function(permList: any){
                                if(permList.permissionName == 'catalogueManagement' && permList.permissionStatus == 'active'){
                                    catalogueManagementMenuPermission = true;
                                }
                                
                                switch(permList.permissionName){
                                    case "pricingConfiguration":
                                        returnStatus = true;
                                        break;
                                    case "repricingSupplierRequestsReview":
                                        returnStatus = true;
                                        break;
                                    case "repricingPeriodicReview":
                                        returnStatus = true;
                                        break;
                                }
                            });
                        }
                    });
                });
            }
        });
        if(catalogueManagementMenuPermission){
            console.log("4654");
            return returnStatus;
        } else {
            that._router.navigate(this._globalFunc.redirectToComponent('catalogueManagement', orgType));
            //that._router.navigate(['cataloguemanagement', orgType]);
        }
        
    }

    checkProductRepricing(orgType: any){
        let that = this;
        this._globalFunc.logger("check user have permission to product repricing and link is enabled");
        if (this.checkPermission(orgType, "repricingSupplierRequestsReview")) {
            // User have permission to access product repricing
            that._globalFunc.logger("check User have permission to access product repricing");
            that._postsService.getPricingStatus(orgType, "Y").subscribe((responseData:any) => {
                if (responseData.count == 0) {
                    that._router.navigate(['cataloguemanagement', orgType, 'productpricing', 'repricing']);
                } else {
                    that._globalFunc.logger("check User have permission to access product configuration");
                    if (this.checkPermission(orgType, "pricingConfiguration")) {
                        // User have permission to access configuration
                        that._postsService.getPricingStatus(orgType, "").subscribe((responseData1:any) => {
                            if (responseData1.count == 0) {
                                that._router.navigate(['cataloguemanagement', orgType, 'productpricing', 'configuration']);
                            } else {
                                that._globalFunc.logger("check User have permission to access product identification");
                                /* if((item.permissions.catalogManagementPermission.priceIncreaseReviewPermission.productsIdentification).toLowerCase() != 'none') {
                                    that._router.navigate(['cataloguemanagement', orgType, 'productpricing']);
                                } else { */
                                    that._router.navigate(['cataloguemanagement', orgType]);
                                /*   } */
                            }
                        }, (err:any) => {
                            that._globalFunc.logger("Error while checking count for configuration");
                        });
                    }
                }
            }, (err:any) => {
                that._globalFunc.logger("Error while checking count for Repricing");
            });
        }
    }

    checkProductRepricing1(orgType: any){
        this._globalFunc.logger("check user have permission to product repricing and link is enabled");
        // let orgType = JSON.parse(sessionStorage.getItem('orgType'));
        let that = this;
        this.organisationList.forEach(function (item: any) {
            if (item.orgType === orgType) {
                if ((item.permissions.catalogManagementPermission.priceIncreaseReviewPermission.productsRepricing).toLowerCase() != 'none') {
                    // User have permission to access product repricing
                    that._globalFunc.logger("check User have permission to access product repricing");
                    that._postsService.getPricingStatus(item.orgName, "Y").subscribe((responseData:any) => {
                        if (responseData.count == 0) {
                            that._router.navigate(['cataloguemanagement', orgType, 'productpricing', 'repricing']);
                        } else {
                            that._globalFunc.logger("check User have permission to access product configuration");
                            if ((item.permissions.catalogManagementPermission.priceIncreaseReviewPermission.pricingConfiguration).toLowerCase() != 'none') {
                                // User have permission to access configuration
                                that._postsService.getPricingStatus(item.orgName, "").subscribe((responseData1:any) => {
                                    if (responseData1.count == 0) {
                                        that._router.navigate(['cataloguemanagement', orgType, 'productpricing', 'configuration']);
                                    } else {
                                        that._globalFunc.logger("check User have permission to access product identification");
                                        /* if((item.permissions.catalogManagementPermission.priceIncreaseReviewPermission.productsIdentification).toLowerCase() != 'none') {
                                            that._router.navigate(['cataloguemanagement', orgType, 'productpricing']);
                                        } else { */
                                            that._router.navigate(['cataloguemanagement', orgType]);
                                      /*   } */
                                    }
                                }, (err:any) => {
                                    that._globalFunc.logger("Error while checking count for configuration");
                                });
                            }
                        }
                    }, (err:any) => {
                        that._globalFunc.logger("Error while checking count for Repricing");
                    });
                }
            }
        });
    }
    
    checkPeriodicRepricing(orgType: any){
        this._globalFunc.logger("check user have permission to product repricing and link is enabled");
        // let orgType = JSON.parse(sessionStorage.getItem('orgType'));
        let that = this;
        // this.organisationList.forEach(function (item: any) {
        //     if (item.orgType === orgType) {
                if (this.checkPermission(orgType, "repricingSupplierRequestsReview")) {
                    // User have permission to access product repricing
                    that._globalFunc.logger("check User have permission to access product repricing");
                    that._postsService.getPricingStatus(orgType, "Y").subscribe((responseData:any) => {
                        if (responseData.count == 0) {
                            that._router.navigate(['cataloguemanagement', orgType, 'productpricing', 'repricing']);
                        } else {
                            that._globalFunc.logger("check User have permission to access product configuration");
                            if (this.checkPermission(orgType, "pricingConfiguration")) {
                                // User have permission to access configuration
                                that._postsService.getPricingStatus(orgType, "").subscribe((responseData1:any) => {
                                    if (responseData1.count == 0) {
                                        that._router.navigate(['cataloguemanagement', orgType, 'productpricing', 'configuration']);
                                    } else {
                                        that._globalFunc.logger("check User have permission to access product identification");
                                        /* if((item.permissions.catalogManagementPermission.priceIncreaseReviewPermission.productsIdentification).toLowerCase() != 'none') {
                                            that._router.navigate(['cataloguemanagement', orgType, 'productpricing']);
                                        } else { */
                                            that._router.navigate(['cataloguemanagement', orgType]);
                                      /*   } */
                                    }
                                }, (err:any) => {
                                    that._globalFunc.logger("Error while checking count for configuration");
                                });
                            }
                        }
                    }, (err:any) => {
                        that._globalFunc.logger("Error while checking count for Repricing");
                    });
                }
        //     }
        // });
    }

    getUserSession(customerName:any, loggedInUserGroup:any, loggedInUserId:any){
        this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData:any) => {
            if(!userData.isSessionActive) { // false
                // User session has expired.
                sessionStorage.clear();
                //this._router.navigate(['']);
            } else {
                this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                let changeOrgAvailable: any = [];
                let orgImgUrl = "";
                let orgApiNameTemp = "";
                this.organisationList.forEach(function (item: any) {
                    if (item.customerName !== customerName) {
                        changeOrgAvailable.push(item);
                    } else {
                        orgImgUrl = item.customerName+".png";
                        orgApiNameTemp = item.customerName;
                    }
                });
                this.organisationListChange = changeOrgAvailable;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                // this.disabledCusChange = false;
                if (this.organisationList.length == 1) {
                    this.disabledCusChange = true;
                }

                
            }
            //alert(this.disabledfilfilment);
        });
    }

    getCustomerImage(custName:string){
        return this._globalFunc.getCustomerImage(custName);
    }

    getConfigService(pageName: any) {
        this._postsService.siteMaintanceCall('1', 'all').subscribe(userData => {
            console.log(userData);
            console.log(userData.maintenanceDetails[0].message);
            console.log(userData.maintenanceDetails[0].start);
            console.log(userData.maintenanceDetails[0].status);
            if (userData.maintenanceDetails[0].moduleName == 'all') {
                if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                    this.siteMaintanceFlg = true;
                    this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                } else {
                    this.siteMaintanceFlg = false;
                    this._postsService.siteMaintanceCall(this.customerName, pageName).subscribe(userData => {
                        console.log(userData);
                        console.log(userData.maintenanceDetails[0].message);
                        console.log(userData.maintenanceDetails[0].start);
                        console.log(userData.maintenanceDetails[0].status);
                        if (userData.maintenanceDetails[0].moduleName == pageName) {
                            if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                                this.siteMaintanceFlg = true;
                                this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                            } else {
                                this.siteMaintanceFlg = false;
                            }
                        } else {
                            this.siteMaintanceFlg = false;
                        }
                        this.showLoader = false;
                    }
                        , error => {
                            console.log(error);
                            if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                                this.siteMaintanceFlg = true;
                                this.siteMSg = "Site is under maintenance";
                            } else if (error.errorCode == '404.26.408') {
                                this.errorMsgString = "User is Inactive";
                            }
                            else {
                                this.siteMaintanceFlg = false;
                            }
                            this.showLoader = false;
                        });
                }
            } else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        }, error => {
            console.log(error);
            if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                this.siteMaintanceFlg = true;
                this.siteMSg = "Site is under maintenance";
            } else if (error.errorCode == '404.26.408') {
                this.errorMsgString = "User is Inactive";
            }
            else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        });
    }
}