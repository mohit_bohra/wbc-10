import { NgModule, ModuleWithProviders } from '@angular/core';

import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { FormsModule } from '@angular/forms';
import { CatalogueManagePendingHeaderComponent } from './cataloguemanagependingheader.component';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NavigationModule
  ],
  exports: [CatalogueManagePendingHeaderComponent],
  declarations: [CatalogueManagePendingHeaderComponent],
  providers: [Services, GlobalComponent]
})
export class CatalogueManagePendingHeaderModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CatalogueManagePendingHeaderModule
    }
  }
}
