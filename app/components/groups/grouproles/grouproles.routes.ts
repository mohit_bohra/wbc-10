import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GroupRolesComponent } from './grouproles.component';
import { CanDeactivateGuard } from '../../../services/can-deactivate-guard.service';

// route Configuration
const groupRolesRoutes: Routes = [
  { path: '', component: GroupRolesComponent, canDeactivate: [CanDeactivateGuard] }
];


@NgModule({
  imports: [RouterModule.forChild(groupRolesRoutes)],
  exports: [RouterModule]
})
export class GroupRolesRoutes { }
