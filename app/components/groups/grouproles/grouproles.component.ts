import { Component, ViewChild } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DialogService } from '../../../services/dialog.service';

@Component({
    templateUrl: 'grouproles.component.html',
    providers: [commonfunction]
})
export class GroupRolesComponent {
    @ViewChild('addGrpRole') addGrpRole: any;
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    _sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;
    showLoader: boolean;
    pageState: string = '39';
    customerName4Service: string = 'default';
    roleList: any = [];
    roleListLength: any;
    groupName: string;
    groupId: any;
    groupDescription: string;
    groupStatus: any;
    errTxt: string = '';
    noDataFound: boolean = false;
    unsavedRole: boolean = false;
    duplicateRoleNameArray: any = new Array;
    selectedRole: any = "";
    showTooltip: boolean = false;
    popupMsg: string = "";
    validRolename: boolean = true;
    orgGetData: any = [];
    roleListLengthOriginal: number = 0;
    rowToDelete: number = 0;
    statusChanged: boolean = false;

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, private formBuilder: FormBuilder, public dialogService: DialogService) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        this.addGrpRole = this.formBuilder.group({});
    }

    canDeactivate(): Promise<boolean> | boolean {
        if (!(this.unsavedRole || this.addGrpRole.dirty) || sessionStorage.getItem('unsavedChanges')!=='true') {
            return true;
        }
        return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
    }

    ngOnInit() {
        this.imageUrl = this.whoImgUrl;
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
        sessionStorage.setItem('pageState', this.pageState);

        this.groupId = sessionStorage.getItem('groupId4Users');

        this._sub = this._route.params.subscribe((params: { orgType: string, grpID: string, userId: string }) => {

            console.log(this._router.url);
            console.log(params);
            sessionStorage.setItem('orgType', params.orgType);
            this.customerName = params.orgType;
            this.groupId = sessionStorage.getItem("groupId4Users");

        });
        this.getGroupRoles();
    }

    getGroupRoles() {
        this.showLoader = true;
        this._postsService.getGroupRoles(this.customerName4Service, this.groupId).subscribe((data: any) => {
            this.showLoader = false;
            this.orgGetData = data;
            this.groupId = data.groupId;
            sessionStorage.setItem('groupId4Users',this.groupId);

            this.groupName = data.groupName;
            this.groupDescription = data.groupDescription;
            this.roleListLength = data.roleList.length;
            this.roleListLengthOriginal = data.roleList.length;
            this.groupStatus = data.groupStatus;
            this.roleList = data.roleList;
            this.noDataFound = false;
            console.log(data);
            console.log( this.roleList.length );
            if(this.roleList.length == 0){
                this.errTxt = 'Sorry, No data found at this moment.';
            }
        }
            , (err: any) => {
                this.groupName = sessionStorage.getItem("groupname4User");
                this.groupDescription = sessionStorage.getItem("groupDescription");
                this.groupStatus = sessionStorage.getItem("groupStatus");
                console.log(err);
                this.showLoader = false;
                this.roleListLength = 0;
                if (err.errorCode == '404.33.401') {
                    this.noDataFound = true;
                    this.errTxt = 'Sorry, No data found at this moment.';
                } else if (err.httpResponseCode == '503' || err.httpResponseCode == '404') {
                    this.noDataFound = true;
                    this.errTxt = 'Site is under maintenance. Please try after some time!';
                } else {
                    this._router.navigate(['/error404']);
                }
                //this._router.navigate(['/error404']);
            }
        );
    }

    addNewRole() {
        this.unsavedRole = true;
        sessionStorage.setItem('unsavedChanges','true');
        this.showTooltip = true;
        this.noDataFound = false;
        console.log(this.roleListLength);

        this.roleList[this.roleListLength] = {
            "roleName": "",
            "roleDescription": "",
            "roleStatus": 'active',
            "duplicate": false
        }
        ++this.roleListLength;

        console.log(this.roleList);

        setTimeout(() => {
            var objDiv = document.getElementById("tableBody");
            objDiv.scrollTop = objDiv.scrollHeight;
        }, 500);
    }

    saveGroupRoles() {
        if (this.unsavedRole) {
            this.showTooltip = false;
            this.showLoader = true;
            this.checkDuplicationOnSave();
            if (this.duplicateRoleNameArray.length == 0 && this.validRolename) {
                console.log(this.roleList);
                let FinalArray2Insert: any = [];
                this.roleList.forEach(function (role: any) {
                    if (role.roleName1) {
                        role.roleName = role.roleName1;
                        role.roleDescription = role.roleDescription1;
                    }
                    FinalArray2Insert.push(role);
                });
                console.log(this.orgGetData);
                if (this.orgGetData.length == 0 || this.orgGetData == null) {
                    this.orgGetData = {
                        "customerid": "default",
                        "groupId": this.groupId,
                        "groupName": this.groupName,
                        "groupDescription": this.groupDescription,
                        "groupStatus": this.groupStatus,
                        "userRoleAuditList": []
                    }
                }

                this.orgGetData.roleList = FinalArray2Insert;
                console.log(JSON.stringify(this.orgGetData));

                this._postsService.updateManageGroupRole(this.customerName4Service, this.groupId, this.orgGetData).subscribe((data: any) => {
                    this.showLoader = false;
                    this.orgGetData = data;
                    this.groupId = data.groupId;
                    this.groupName = data.groupName;
                    this.groupDescription = data.groupDescription;
                    this.roleListLength = data.roleList.length;
                    this.roleListLengthOriginal = data.roleList.length;
                    this.groupStatus = data.groupStatus;
                    this.roleList = data.roleList;
                    this.noDataFound = false;
                    this.unsavedRole = false;
                    sessionStorage.setItem('unsavedChanges','false');
                    this.selectedRole = "";

                    this.popupMsg = 'Group role(s) added/updated successfully.';
                    let sessionModal = document.getElementById('messageModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('messageModal').style.display = "block";
                }, (err: any) => {
                    console.log(err);
                    //this.unsavedRole = false;
                    if (err.httpResponseCode == '503' || err.httpResponseCode == '404') {
                        this.popupMsg = 'Site is under maintenance. Please try after some time!';
                        let sessionModal = document.getElementById('messageModal');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('messageModal').style.display = "block";
                    } else {
                        this._router.navigate(['/error404']);
                    }
                    this.showLoader = false;
                })
            } else {
                //this.unsavedRole = false;
                if (!this.validRolename && this.duplicateRoleNameArray.length > 0) {
                    this.popupMsg = "Role(s) " + this.duplicateRoleNameArray.join() + " already exist also insert valid role name";
                } else if (!this.validRolename) {
                    this.popupMsg = "Please enter valid role name.";
                } else {
                    this.popupMsg = "Role(s) " + this.duplicateRoleNameArray.join() + " already exist.";
                }
                //this.popupMsg = this.duplicateGrpNameArray.join() + " already exist.";
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
            }
            this.showLoader = false;
        }
    }


    checkDuplicationOnSave() {
        let validRlName = true;
        let tempRoleList = this.roleList;
        let dummyArray: any = new Array;

        this.roleList.forEach(function (item1: any) {
            if (item1.duplicate) {
                dummyArray.push(item1.roleName1);
            } else if (item1.roleName1 && item1.roleName1.trim() == "") {
                validRlName = false;
            }
        });

        console.log(dummyArray);
        this.duplicateRoleNameArray = dummyArray;
        this.validRolename = validRlName;
    }

    checkDuplicateRole(enteredRole: string) {
        let duplicateFlag = false;
        let invalidGroup = false;
        if (enteredRole != "") {
            this.roleList.forEach(function (item: any) {
                console.log(enteredRole + "==" + item.roleName);
                if (enteredRole == item.roleName) {
                    duplicateFlag = true;
                }
            });

            if (duplicateFlag) {
                //this.unsavedRole = false;
                this.popupMsg = enteredRole + " already exist.";
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
            } else {
                //this.unsavedRole = true;
            }
        }
    }

    checkDuplicateUsingService(username: string, e: any = null, idx: number = null) {
        this.showLoader = true;
        this._postsService.checkDuplicate(this.customerName4Service, 'role', username).subscribe((data: any) => {
            console.log(data.isAvailable);
            if (data.isAvailable == "false") {
                console.log("123456");
                this.popupMsg = "Role " + username + " already exist.";
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
                //(<HTMLInputElement>e.target).focus();
                this.roleList[idx].duplicate = true;
            } else {
                this.roleList[idx].duplicate = false;
            }
            this.showLoader = false;
        });
    }

    focusOutFunction(e: any, idx: number) {
        console.log(e.srcElement.value);
        let enteredRole = (<HTMLInputElement>event.target).value.trim();
        if(enteredRole != ''){
            this.checkDuplicateUsingService(enteredRole, e, idx);
        }        
    }

    resetRoles() {
        /*if (this.unsavedRole) {
            this.roleListLength = 0;
            this.unsavedRole = false;
            this.showTooltip = false;
            this.selectedRole = ""; 
            this.roleList.length = 0;

            this.getGroupRoles();
            if(this.noDataFound == true){
                setTimeout(() => {
                    var objDiv = document.getElementById("tableBody");
                    objDiv.scrollTop = 0;
                }, 500);
            }
            
        } else{*/
            this._router.navigate(['groups', this.customerName]);
        //}
    }

    setStatus(idx: number, status: string) {
        this.unsavedRole = true;
        sessionStorage.setItem('unsavedChanges','true');
        this.statusChanged = true;
        this.roleList[idx].roleStatus = status;
    }

    goToManageRoleCustomers() {
        if (!this.unsavedRole && this.selectedRole != '') {
            this._router.navigate(['groups', this.customerName, this.groupName, 'roles', this.selectedRole, 'customers'])
        }
    }

    goToViewRoleUsers() {
        if (!this.unsavedRole && this.selectedRole != '') {
            let url = this.homeUrl + 'groups/' + encodeURIComponent(this.customerName) + '/' +this.groupName + '/roles/' + this.selectedRole + '/roleuser';
            sessionStorage.setItem("pageState",'999');
            window.open(url);
            /* this._router.navigate(['groups', this.customerName, this.groupName, 'roles', this.selectedRole, 'roleuser']) */
        }
    }

    selectRole(role: any) { //roleId: any, roleName: any, roleDesc: any, roleStatus: any) {
        //this.unsavedRole = false;
        this.selectedRole = role.roleName;
        sessionStorage.setItem('roleId', role.roleId);
        sessionStorage.setItem('roleName', role.roleName);
        sessionStorage.setItem('roleDesc', role.roleDescription);
        sessionStorage.setItem('roleStatus', role.roleStatus);
    }

    closePopup(popupName: string = 'messageModal') {
        this.popupMsg = "";
        let sessionModal = document.getElementById(popupName);
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById(popupName).style.display = "none";
    }

    deleteRowPopup(i_no: number) {
        this.rowToDelete = i_no;
        console.log(this.rowToDelete);
        let sessionModal = document.getElementById('deleteModal');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('deleteModal').style.display = "block";
    }

    deleteRow(i_no: number) {
        let dummyArr: any = [];
        this.roleList.forEach(function (item: any, idx: number) {
            if (i_no != idx) {
                dummyArr.push(item);
            }
        });
        this.roleList = dummyArr;
        --this.roleListLength;

        if (this.roleListLengthOriginal == this.roleListLength) {
            console.log(this.statusChanged);
            if (!this.statusChanged) {
                this.unsavedRole = false;
                sessionStorage.setItem('unsavedChanges','false');
            }
        }

        this.rowToDelete = 0;
        let sessionModal = document.getElementById('deleteModal');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('deleteModal').style.display = "none";
    }

}