import { Component, ViewChild } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Services } from '../../services/app.services';
import { commonfunction } from '../../services/commonfunction.services';
import { GlobalComponent } from '../global/global.component';
import { Constant } from '../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DialogService } from '../../services/dialog.service';

@Component({
    templateUrl: 'groups.component.html',
    providers: [commonfunction]
})

export class GroupsComponent {
    @ViewChild('addNewGrp') addNewGrp: any;
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    _sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;
    showLoader: boolean;
    pageState: string = '34';
    unsavedGroup: boolean = false;
    newRowCounter: number;
    showButtons: boolean = true;


    groupListLength: number = 0;
    groupListLengthOriginal: number = 0;
    groupList: any = [];
    groupJSON: any = [];
    selectedGroup: string = "";

    customerName4Service: string = 'default';
    popupMsg: string = "";
    duplicateGrpNameArray: any = new Array;
    noDataFound: boolean = false;
    errTxt: string = "";

    showTooltip: boolean = false;
    validGroupname: boolean = true;
    statusChanged: boolean = false;
    rowToDelete: number = 0;
    unsavedDataWarningMsg: string = "";
    loggedInUserGroup: string = "";
    superAdminGroup: string = "";
    updatedGroupIndexArray: any = [];

    moduleName: string;
    siteMaintanceFlg: boolean;
    siteMaintanceFlg1: boolean;
    siteMSg: any;
    errorMsgString: string;

    //Constructor is used to initialize variables during load time
    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, private formBuilder: FormBuilder, public dialogService: DialogService) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.superAdminGroup = this.objConstant.SUPERADMIN_GROUP;
        this.showLoader = false;
        this.showButtons = true;
        this.addNewGrp = this.formBuilder.group({});
    }

    canDeactivate(): Promise<boolean> | boolean {
        if (!this.unsavedGroup || sessionStorage.getItem('unsavedChanges')!=='true') {
            this.unsavedGroup=false;
            sessionStorage.setItem('unsavedChanges','false');
            return true;
        }
        return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
        
    }

    // This method is called on page load.
    // Hence all the basic cosmetic related elements are initialized here
    ngOnInit() {
        this.moduleName = "managePermission";
        this.loggedInUserGroup = sessionStorage.getItem('loggedInUserGroupName');
        this.imageUrl = this.whoImgUrl;
        //this.unsavedGroup = true;
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
        sessionStorage.setItem('pageState', this.pageState);

        this.getConfigService(this.moduleName);

        this._sub = this._route.params.subscribe((params: { orgType: string }) => {
            this.customerName = params.orgType;
            if (this.loggedInUserGroup != this.superAdminGroup) {
                // User is not superadmin, hence is redirected to user screen as per user story
                sessionStorage.setItem('groupId4Users', sessionStorage.getItem('loggedInUserGroupId'));
                sessionStorage.setItem('groupname4User', sessionStorage.getItem('loggedInUserGroupName'));
                this._router.navigate(['groups', this.customerName, this.loggedInUserGroup, 'users']);
            }
        });

        this.getGroups();

         //this.siteMaintanceFlg1 = sessionStorage.getItem('site');
        //  if(sessionStorage.getItem('site') == "true"){
            
        //     this.siteMaintanceFlg1 = false;
        // }else { 
        //     this.siteMSg = "Site is under maintenance";
        //     this.siteMaintanceFlg1 = true;
        // }
    }

    getGroups() {
        if(this.unsavedGroup){
            sessionStorage.setItem('unsavedGroup','true');
            sessionStorage.setItem('unsavedChanges','true');
            this.canDeactivate();
        }else{
            this.showLoader = true;
            this.newRowCounter = 0;
            this._postsService.getRoleBasedGroups(this.customerName4Service).subscribe((data: any) => {
                this.showLoader = false;
                this.groupJSON = data;
                this.groupListLength = data.groupList.length;
                this.groupListLengthOriginal = data.groupList.length;
                this.groupList = data.groupList;
                sessionStorage.setItem('tempGroupArr', JSON.stringify(this.groupList));
                this.noDataFound = false;
                console.log(this.groupList);
            }
                , (err: any) => {
                    console.log("error code : " + err.errorCode);
                    if (err.errorCode == '404.33.401') {
                        this.noDataFound = true;
                        this.errTxt = 'Sorry, No data found at this moment.';
                    } else if (err.httpResponseCode == '503' || err.httpResponseCode == '404') {
                        this.noDataFound = true;
                        this.errTxt = 'Site is under maintenance. Please try after some time!';
                    } else {
                        this._router.navigate(['/error404']);
                    }
                    console.log(err);
                    this.showLoader = false;
                }
            );
        }
    }

    selectGroup(grpName: string, grpId: string, groupDescription: any, groupStatus: any) {
        //this.unsavedGroup = false;
        this.selectedGroup = grpName;
        sessionStorage.setItem('groupId4Users', grpId);
        sessionStorage.setItem('groupname4User', grpName);
        sessionStorage.setItem('groupDescription', groupDescription);
        sessionStorage.setItem('groupStatus', groupStatus);
    }

    setStatus(idx: number, status: string) {
        this.unsavedGroup = true;
        sessionStorage.setItem('unsavedChanges','true');
        this.statusChanged = true;
        this.groupList[idx].groupStatus = status;
        this.updatedGroupIndexArray.push(idx);
    }

    saveGroup() {
        if (this.unsavedGroup) {
            this.unsavedGroup = false;
            sessionStorage.setItem('unsavedChanges','false');
            this.showTooltip = false;
            this.showLoader = true;
            this.checkDuplicationOnSave();
            if (this.duplicateGrpNameArray.length == 0 && this.validGroupname) {
                let tempGroupArrOriginal = JSON.parse(sessionStorage.getItem('tempGroupArr'));

                let unique = this.updatedGroupIndexArray.filter(function (item: any, i: number, ar: any) { return ar.indexOf(item) === i; });
                let that = this;
                let dummyGroupArray: any = [];
                unique.forEach(function (id: number) {
                    if (that.groupList[id].groupStatus != tempGroupArrOriginal[id].groupStatus) {
                        dummyGroupArray.push(that.groupList[id]);
                    }
                });
                that = null;

                this.groupList.forEach(function (item: any) {
                    if (item.groupName1) {
                        item.groupName = item.groupName1;
                        item.groupDescription = item.groupDescription1;
                        delete item.groupName1;
                        delete item.groupDescription1;
                        dummyGroupArray.push(item);
                    }
                });



                // console.log(this.groupList);
                // let FinalArray2Insert:any = [];
                // this.groupList.forEach(function(item:any){
                //     if(item.groupName1){
                //         item.groupName = item.groupName1;
                //         item.groupDescription = item.groupDescription1;
                //     }
                //     FinalArray2Insert.push(item);
                // });

                this.groupJSON.groupList = dummyGroupArray;


                console.log(this.groupJSON);
                this._postsService.createAuthorisationGroup(this.customerName4Service, this.groupJSON).subscribe((data: any) => {
                    console.log("response");
                    console.log(data);
                    this.showLoader = false;
                    this.groupJSON = data;
                    this.groupListLength = data.groupList.length;
                    this.groupList = data.groupList;
                    sessionStorage.setItem('tempGroupArr', JSON.stringify(this.groupList));
                    this.noDataFound = false;
                    this.selectedGroup = "";
                    this.unsavedGroup = false;
                    sessionStorage.setItem('unsavedChanges','false');
                    this.updatedGroupIndexArray = [];

                    this.popupMsg = 'Group(s) added/updated successfully.';
                    let sessionModal = document.getElementById('messageModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('messageModal').style.display = "block";
                },
                    (err: any) => {
                        if (err.httpResponseCode == '503' || err.httpResponseCode == '404') {
                            this.popupMsg = 'Site is under maintenance. Please try after some time!';
                            let sessionModal = document.getElementById('messageModal');
                            sessionModal.classList.remove('in');
                            sessionModal.classList.add('out');
                            document.getElementById('messageModal').style.display = "block";
                        } else {
                            this._router.navigate(['/error404']);
                        }
                        console.log(err);
                        this.showLoader = false;
                    }
                );
            } else {
                if (!this.validGroupname && this.duplicateGrpNameArray.length > 0) {
                    this.popupMsg = "Group(s) " + this.duplicateGrpNameArray.join() + " already exist also insert valid group name";
                } else if (!this.validGroupname) {
                    this.popupMsg = "Please enter valid group name.";
                } else {
                    this.popupMsg = this.duplicateGrpNameArray.join() + " already exist.";
                }
                //this.popupMsg = this.duplicateGrpNameArray.join() + " already exist.";
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
            }
            this.showLoader = false;
        }
    }

    checkDuplicationOnSave() {
        let validGrpName = true;
        let tempGrpList = this.groupList;
        let dummyArray: any = new Array;
        this.groupList.forEach(function (item1: any) {
            if (item1.groupName1 && item1.groupName1.trim() != "") {
                tempGrpList.forEach(function (item: any) {
                    if (item1.groupName1.trim() == item.groupName.trim()) {
                        console.log("1");
                        dummyArray.push(item1.groupName1);
                    }
                });
            } else if (item1.groupName1 && item1.groupName1.trim() == "") {
                validGrpName = false;
            }
        });
        console.log(dummyArray);
        this.duplicateGrpNameArray = dummyArray;
        this.validGroupname = validGrpName;
    }

    resetGroups() {
        //this.unsavedGroup = false;
        this.showTooltip = false;
        this.selectedGroup = "";

        this.getGroups();
        setTimeout(() => {
            var objDiv = document.getElementById("tableBody");
            objDiv.scrollTop = 0;
        }, 500);
    }

    addNewGroup() {
        this.unsavedGroup = true;
        sessionStorage.setItem('unsavedChanges','true');
        this.showTooltip = true;
        // Adding empty row
        this.groupList[this.groupListLength] = {
            "groupName": "",
            "groupDescription": "",
            "groupStatus": 'active',
        }
        ++this.groupListLength;

        setTimeout(() => {
            var objDiv = document.getElementById("tableBody");
            objDiv.scrollTop = objDiv.scrollHeight;
        }, 500);
    }

    gotoGroupUsers() {
        if (!this.unsavedGroup && this.selectedGroup != '') {
            this._router.navigate(['groups', this.customerName, this.selectedGroup, 'users']);
        }
    }

    gotoGroupRoles() {
        if (!this.unsavedGroup && this.selectedGroup != '') {
            this._router.navigate(['groups', this.customerName, this.selectedGroup, 'roles']);
        }
    }

    gotoSearchUsers() {
        if (this.unsavedGroup) {
            this.navigateSearchUsers();
        } else {
            this.navigateSearchUsers();
        }
    }

    navigateSearchUsers() {
        this._router.navigate(['groups', this.customerName, 'users', 'search']);
    }

    focusOutFunction(e: any) {
        console.log(e.srcElement.value);
        let enteredGroup = (<HTMLInputElement>event.target).value.trim();
        if (enteredGroup != "") {
            if (/^[A-Za-z][0-9A-Za-z\d_]*$/.test(enteredGroup)) {
                this.checkDuplicateGroup(enteredGroup);
            } else {
                //alert("Please enter only letter and numeric characters");
                this.popupMsg = "Please enter only Alpha numeric along with underscore characters ";
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
                (<HTMLInputElement>event.target).value = '';
                (<HTMLInputElement>event.target).focus();
                return (false);
            }
        }
    }

    checkDuplicateGroup(enteredGroup: string) {
        let duplicateFlag = false;
        let invalidGroup = false;
        if (enteredGroup != "") {
            this.groupList.forEach(function (item: any) {
                console.log(enteredGroup + "==" + item.groupName);
                if (enteredGroup == item.groupName) {
                    duplicateFlag = true;
                }
            });

            if (duplicateFlag) {
                this.popupMsg = "Group " + enteredGroup + " already exist.";
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
                return true;
            }
        } else {
            // Enter valid group name
            this.popupMsg = "Please enter valid group name.";
            let sessionModal = document.getElementById('messageModal');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('messageModal').style.display = "block";
            return true;
        }
        return false;
    }

    closePopup(popupName: string = 'messageModal') {
        this.popupMsg = "";
        let sessionModal = document.getElementById(popupName);
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById(popupName).style.display = "none";
    }

    keyPress(event: any) {
        this._globalFunc.alphaNumeric(event);
    }

    keyPressWithSpace(event: any) {
        this._globalFunc.alphaNumericWithSpace(event);
    }

    deleteRowPopup(i_no: number) {
        this.rowToDelete = i_no;
        console.log(this.rowToDelete);
        let sessionModal = document.getElementById('deleteModal');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('deleteModal').style.display = "block";
    }

    deleteRow(i_no: number) {
        let dummyArr: any = [];
        this.groupList.forEach(function (item: any, idx: number) {
            if (i_no != idx) {
                dummyArr.push(item);
            }
        });
        this.groupList = dummyArr;
        --this.groupListLength;

        if (this.groupListLengthOriginal == this.groupListLength) {
            console.log(this.statusChanged);
            if (!this.statusChanged) {
                this.unsavedGroup = false;
                sessionStorage.setItem('unsavedChanges','false');
            }
        }

        this.rowToDelete = 0;
        let sessionModal = document.getElementById('deleteModal');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('deleteModal').style.display = "none";
    }

    getConfigService(pageName: any) {
        this._postsService.siteMaintanceCall('1', 'all').subscribe(userData => {
            console.log(userData);
            console.log(userData.maintenanceDetails[0].message);
            console.log(userData.maintenanceDetails[0].start);
            console.log(userData.maintenanceDetails[0].status);
            if (userData.maintenanceDetails[0].moduleName == 'all') {
                if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                    this.siteMaintanceFlg = true;
                    this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                } else {
                    this.siteMaintanceFlg = false;
                    this._postsService.siteMaintanceCall(this.customerName, pageName).subscribe(userData => {
                        console.log(userData);
                        console.log(userData.maintenanceDetails[0].message);
                        console.log(userData.maintenanceDetails[0].start);
                        console.log(userData.maintenanceDetails[0].status);
                        if (userData.maintenanceDetails[0].moduleName == pageName) {
                            if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                                this.siteMaintanceFlg = true;
                                this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                            } else {
                                this.siteMaintanceFlg = false;
                            }
                        } else {
                            this.siteMaintanceFlg = false;
                        }
                        this.showLoader = false;
                    }
                        , error => {
                            console.log(error);
                            if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                                this.siteMaintanceFlg = true;
                                this.siteMSg = "Site is under maintenance";
                            } else if (error.errorCode == '404.26.408') {
                                this.errorMsgString = "User is Inactive";
                            }
                            else {
                                this.siteMaintanceFlg = false;
                            }
                            this.showLoader = false;
                        });
                }
            } else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        }, error => {
            console.log(error);
            if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                this.siteMaintanceFlg = true;
                this.siteMSg = "Site is under maintenance";
            } else if (error.errorCode == '404.26.408') {
                this.errorMsgString = "User is Inactive";
            }
            else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        });
    }
}