import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GroupsComponent } from './groups.component';
import { CanDeactivateGuard } from '../../services/can-deactivate-guard.service';

// route Configuration
const groupsRoutes: Routes = [
  { path: '', component: GroupsComponent, canDeactivate: [CanDeactivateGuard] },
  { path: 'users/search', loadChildren: './users/search/search-users.module#SearchUsersModule'}, // MWEE-2496
  { path: ':groupName/users', loadChildren: './users/users.module#UsersModule'  }, // MWEE-2794
  { path: ':groupName/roles/:roleName/users', loadChildren: './users/users.module#UsersModule'}, // MWEE-2797
  { path: ':groupName/users/:userId/amendment', loadChildren: './viewuseramendment/viewuseramendment.module#ViewUserAmendmentModule'}, // MWEE-2798
  { path: ':groupName/users/:userId/setRoles', loadChildren: './roles/roles.module#RolesModule'}, // MWEE-2795
  { path: ':groupName/users/:userId/setPermissions', loadChildren: './view-permission/view-permission.module#ViewPermissionModule'}, // MWEE-2800
  { path: ':groupName/users/:userId/history', loadChildren: './viewuserhistory/viewuserhistory.module#ViewUserHistoryModule'}, // MWEE-2799
  { path: ':groupName/roles', loadChildren: './grouproles/grouproles.module#GroupRolesModule'},// MWEE-2471  
  { path: ':groupName/roles/:roleName/customers', loadChildren: './managerolecustomers/managerolecustomers.module#ManageRoleCustomersModule'}, //2792 
  {path: ':groupName/users/:userId/historyforalluser', loadChildren: './LiveUserMonitoring/LiveUserMonitoring.module#LiveUserMonitoringModule'}, // MWEE-2798

  { path: ':groupName/roles/:roleName/customers/:customerName', loadChildren: './managerolepermission/managerolepermission.module#ManageRolePermissionModule'}, //2793 

  { path: ':groupName/roles/:roleName/roleuser', loadChildren: './viewrolecutomers/viewrolecutomers.module#ViewRoleCustomersModule'}, //2797 
];

@NgModule({
  imports: [RouterModule.forChild(groupsRoutes)],
  exports: [RouterModule]
})
export class GroupsRoutes { }
