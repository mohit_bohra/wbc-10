import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RolesComponent } from './roles.component';
import { CanDeactivateGuard } from '../../../services/can-deactivate-guard.service';

// route Configuration
const rolesRoutes: Routes = [
  { path: '', component: RolesComponent, canDeactivate: [CanDeactivateGuard] },
  //{ path: ':userId/setRoles', loadChildren: '../roles/roles.module#RolesModule'  }, // MWEE-2795
  //{ path: ':userId/permissions', loadChildren: '../permissions/permissions.module#PermissionsModule'  }, // MWEE-2800
];


@NgModule({
  imports: [RouterModule.forChild(rolesRoutes)],
  exports: [RouterModule]
})
export class RolesRoutes { }
