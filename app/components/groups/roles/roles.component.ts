import { Component, ViewChild } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DialogService } from '../../../services/dialog.service';

@Component({
    templateUrl: 'roles.component.html',
    providers: [commonfunction]
})

export class RolesComponent {
    @ViewChild('addNewRole') addNewRole: any;
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    _sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;
    showLoader: boolean;
    pageState: string = '36';

    unsavedRole: boolean = false;
   
    newRowCounter: number;
    userRoleList:any = [];
    //userRoleListLengthOriginal:any = [];
    groupId:any;
    grpId:any;
    grpID:string;
    selectedUser:string = "";
    userRoleListLength: number = 0;
    customerName4Service:string = 'default';
    groupName:string;
    groupDescription:string;
    groupStatus: any;
    userId: any;
    errTxt: string = '';
    noDataFound: boolean = false;
    userRoleListLengthOriginal:number = 0;

    userName:string;
    userDescription:string;
    userLogon:string;
    adminFlag:string;

    popupMsg:string = "";
    duplicateUserNameArray:any = new Array;
    showTooltip: boolean = false;
    userRoleJSON:any = [];
    validUsername: boolean = true;
    statusChanged:boolean = false;
    rowToDelete: number = 0;
    finalRoleIndexArray:any = [];

    loggedInUserGroup:string = "";
    loggedInUserAdminFlag:string = "";
    superAdminGroup:string = "";


    //Constructor is used to initialize variables during load time
    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, private formBuilder: FormBuilder, public dialogService: DialogService) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        //this.unsavedRole = true;
        this.groupId = sessionStorage.getItem('groupId4Users');
        this.groupDescription = sessionStorage.getItem('groupDescription');
        this.groupName = sessionStorage.getItem('groupname4User');
        this.customerName = sessionStorage.getItem('orgType');
        this.superAdminGroup = this.objConstant.SUPERADMIN_GROUP;
        this.addNewRole = this.formBuilder.group({})

    }

    canDeactivate(): Promise<boolean> | boolean {
        if (!this.unsavedRole || sessionStorage.getItem('unsavedChanges')!=='true') {
            return true;
        }
        return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
      }

    // This method is called on page load.
    // Hence all the basic cosmetic related elements are initialized here
    ngOnInit() {
        this.imageUrl = this.whoImgUrl;
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
        sessionStorage.setItem('pageState', this.pageState);
        this.loggedInUserGroup = sessionStorage.getItem('loggedInUserGroupName');
        this.loggedInUserAdminFlag = sessionStorage.getItem('loggedInUserAdminFlag');

        this._sub = this._route.params.subscribe((params: { orgType: string, grpID: string , userId:string }) => {

            console.log(this._router.url);
                console.log(params);
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;

                console.log(sessionStorage.getItem('groupId4Users'));
                if (sessionStorage.getItem('groupId4Users') == null || sessionStorage.getItem('groupId4Users') == 'undefined') {
                    sessionStorage.setItem('groupId4Users', params.grpID);
                }
                this.grpID = sessionStorage.getItem('groupId4Users');
                   

                if (sessionStorage.getItem('userId4Users') == null || sessionStorage.getItem('userId4Users') == 'undefined') {
                    sessionStorage.setItem('userId4Users', params.userId);
                }
                this.userId = sessionStorage.getItem('userId4Users');
         }); 
           this.getGroupUserRoles();
      }

    getGroupUserRoles() {
        this.showLoader = true;
        this.newRowCounter = 0;
        let grpId = sessionStorage.getItem('groupId4Users');
        let usrID = sessionStorage.getItem('userIdForGroup');
        this._postsService.getUserRoleGroups(this.customerName4Service, grpId ,usrID).subscribe(data => { 
            this.showLoader = false;
            this.userRoleListLength = data.roleListlength;
            this.userRoleList = data.roleList;
            this.groupName = data.groupName;
            this.groupDescription = data.groupDescription;
            this.userName = data.userName;
            this.userDescription = data.userDescription;
            this.userLogon = data.userLogon;
            this.adminFlag = data.adminFlag;
            this.groupStatus = data.groupStatus;
            this.userRoleJSON = data;
        }
            , err => {
                this.userRoleListLength = 0;
                console.log(err);
                if(err.errorCode == '404.33.401'){
                    this.noDataFound = true;
                    this.errTxt = 'Sorry, No data found at this moment.';
                } else if(err.httpResponseCode == '503' || err.httpResponseCode == '404'){
                    this.noDataFound = true;
                    this.errTxt = 'Site is under maintenance. Please try after some time!';
                } else{ 
                    this._router.navigate(['/error404']);
                }
                this.showLoader = false;
            }
        );
    }

    resetRoles(){
        // this.unsavedRole = false;
        // this.showTooltip = false;
        // this.selectedUser = "";
        // this.getGroupUserRoles();
        this._router.navigate(['groups', this.customerName, this.groupName, 'users']);
    }

    selectRole(roleName: any, roleId: any, roleDescription: any, roleStatus: any) {
        this.selectRole = roleId;


        // alert(this.selectRole)


        // alert(this.selectRole)
        sessionStorage.setItem('roleId', roleId);
        sessionStorage.setItem('roleName', roleName);
        sessionStorage.setItem('roleDescription', roleDescription);
        sessionStorage.setItem('roleStatus', roleStatus);
    }

    createFinalJSON(idx:number, status:any){
        this.unsavedRole = true;
        sessionStorage.setItem('unsavedChanges','true');
        this.finalRoleIndexArray.push(idx);
        this.userRoleList[idx].status = status;
    }

    saveRole(){
        console.log(this.userRoleList[4]);
        let unique = this.finalRoleIndexArray.filter(function(item:any, i:number, ar:any){ return ar.indexOf(item) === i; });
        console.log(unique);
        let roleArr:any = [];
        let that = this;
        unique.forEach(function(id:number){
            console.log(id);
            console.log(that.userRoleList[id]);
            roleArr.push(that.userRoleList[id]);
        });
        that = null;

        this.userRoleJSON.roleList = roleArr;
        this.userRoleJSON.sessionUserName = sessionStorage.getItem("email");

        let grpId = sessionStorage.getItem('groupId4Users');
        let usrID = sessionStorage.getItem('userIdForGroup');
        console.log(JSON.stringify(this.userRoleJSON));
        this._postsService.updateUserRoleStatus(this.customerName4Service, grpId, usrID, this.userRoleJSON).subscribe((data:any) => {
            console.log("response");
            console.log(data);
            this.showLoader = false;
            this.userRoleListLength = data.roleListlength;
            this.userRoleList = data.roleList;
            this.groupName = data.groupName;
            this.groupDescription = data.groupDescription;
            this.userName = data.userName;
            this.userDescription = data.userDescription;
            this.userLogon = data.userLogon;
            this.adminFlag = data.adminFlag;
            this.groupStatus = data.groupStatus;
            this.userRoleJSON = data;
            this.finalRoleIndexArray = [];

            this.popupMsg = 'User role updated successfully.';
            let sessionModal = document.getElementById('messageModal');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('messageModal').style.display = "block";

            this.unsavedRole = false;
            sessionStorage.setItem('unsavedChanges','false');

        }, (err:any) => {
                if(err.httpResponseCode == '503' || err.httpResponseCode == '404'){
                    this.popupMsg = 'Site is under maintenance. Please try after some time!';
                    let sessionModal = document.getElementById('messageModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('messageModal').style.display = "block";
                } else{ 
                    this._router.navigate(['/error404']);
                }
                console.log(err);
                this.showLoader = false;
            }
        );
    }

    navigateToViewPermission(){
        if(!this.unsavedRole){
            // http://localhost:7777/#/groups/mccolls/Group_Admin/users/48/setPermissions

            let usrID = sessionStorage.getItem('userIdForGroup');
            console.log(['groups', this.customerName, this.groupName, 'users', usrID, 'setPermissions']);


            let url = this.homeUrl + 'groups/' + encodeURIComponent(this.customerName) + '/' +this.groupName + '/users/' + usrID + '/setPermissions';
            sessionStorage.setItem("pageState",'999');
            window.open(url);

            // this._router.navigate(['groups', this.customerName, this.groupName, 'users', usrID, 'setPermissions']);        
        }
    }

    closePopup(popupName:string = 'messageModal'){
        this.popupMsg = "";
        let sessionModal = document.getElementById(popupName);
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById(popupName).style.display = "none";
    }
}