import { NgModule } from '@angular/core';
import { RolesRoutes } from './roles.routes';
import { RolesComponent } from './roles.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';
import { TooltipModule } from "ngx-tooltip";

@NgModule({
  imports: [
    RolesRoutes,
    CommonModule,
    FormsModule,
    NavigationModule,
    TooltipModule
  ],
  exports: [],
  declarations: [RolesComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class RolesModule { }
