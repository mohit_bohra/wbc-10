import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from '../users/users.component';
import { LiveUserMonitoringComponent } from './LiveUserMonitoring.component';

// route Configuration
const viewUserAmendmentRoutes: Routes = [
  { path: '', component: LiveUserMonitoringComponent }
];


@NgModule({
  imports: [RouterModule.forChild(viewUserAmendmentRoutes)],
  exports: [RouterModule]
})
export class LiveUserMonitoringRoutes { }
