import { NgModule } from '@angular/core';
import { LiveUserMonitoringComponent } from './LiveUserMonitoring.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';
import { LiveUserMonitoringRoutes } from './LiveUserMonitoring.routes';

@NgModule({
  imports: [
    LiveUserMonitoringRoutes,
    CommonModule,
    FormsModule,
    NavigationModule
  ],
  exports: [],
  declarations: [LiveUserMonitoringComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class LiveUserMonitoringModule { }
