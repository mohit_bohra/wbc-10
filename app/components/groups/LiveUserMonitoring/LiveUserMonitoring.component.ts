import { Component } from '@angular/core';
import { PlatformLocation, Location } from '@angular/common';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'LiveUserMonitoring.component.html',
    providers: [commonfunction]
})

export class LiveUserMonitoringComponent {
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    _sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;
    showLoader: boolean;
    pageState: string = '50';
    groupName: any;
    groupId: any;

    noDataFound:boolean = false;

    customerName4Service:string = 'default';
    userId:any;
    userLoginHistory: any;
    userLoginHistoryLength: number = 0;
    userDataLength:number = 0;
    errTxt:string = "";
    userData:any = [];

    groupDescription:string;
    groupStatus: any;
    userName:string;
    userDescription:string;
    userLogon:string;
    adminFlag:string;
    username :string;
   liveCustomerList:any;
   startTimeforLiveUser:any;
    userIDforLive:any;

    loggedInUserGroup:string = "";
    loggedInUserAdminFlag:string = "";
    superAdminGroup:string = "";

    
    //Constructor is used to initialize variables during load time
    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, public _location: Location) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        this.groupName = sessionStorage.getItem('groupname4User');
        this.groupId = sessionStorage.getItem('groupId4Users');
        this.customerName = sessionStorage.getItem('orgType');

        this.superAdminGroup = this.objConstant.SUPERADMIN_GROUP;
    }

    // This method is called on page load.
    // Hence all the basic cosmetic related elements are initialized here
    ngOnInit() {
        this.imageUrl = this.whoImgUrl;
        this.loggedInUserGroup = sessionStorage.getItem('loggedInUserGroupName');
        this.loggedInUserAdminFlag = sessionStorage.getItem('loggedInUserAdminFlag');
       this.getUserHistory();
  
    }

    killsession(id:any){
        let orgType = 'default';
        let userId = id;
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(orgType, sessionStorage.getItem('loggedInUserGroupId'), userId, '' , dt).subscribe((userData: any) => {
            console.log(userData);
            this.getUserHistory();
     
        }, (error: any) =>{
            console.log(error);
        });
   }

    getUserHistory() {
        console.log("User history");
        this.showLoader = true;
        let LivecustomerName:any;
        let starttimeforuser:any;
        let UserIDforLiveUser:any;
        let grpId = '@all'
        this.userId = '@all'
        this._postsService.getUserHistoryLiveUser(this.customerName4Service, this.groupId, this.userId).subscribe((data:any) => { 
            this.showLoader = false;
            this.userData = data.allUserSessionDetails;
           this.userDataLength = data.allUserSessionDetails.length;

            data.allUserSessionDetails.forEach(function (allUserSessionDetails: any) {
                UserIDforLiveUser= allUserSessionDetails.userId;
               allUserSessionDetails.userLoginDetails.forEach(function(userLoginDetails:any){
                    starttimeforuser=userLoginDetails.start; 
                });

            });
            data.allUserSessionDetails.forEach(function (allUserSessionDetails: any) {
                 allUserSessionDetails.customerAccess.forEach(function(customerAccess:any){
                 LivecustomerName= LivecustomerName + customerAccess.customerName ;
                });
             });
          this.startTimeforLiveUser=starttimeforuser;
          this.liveCustomerList = LivecustomerName;
          this.userIDforLive = UserIDforLiveUser
          
            console.log(this.userData);
        }
            , (err:any) => {
                console.log(err);
                this.showLoader = false;
                this.userData = 0;
                if(err.errorCode == '404.33.401'){
                    this.noDataFound = true;
                    this.errTxt = 'Sorry, No data found at this moment.';
                } else if(err.httpResponseCode == '503' || err.httpResponseCode == '404'){
                    this.noDataFound = true;
                    this.errTxt = 'Site is under maintenance. Please try after some time!';
                } else{ 
                    this._router.navigate(['/error404']);
                }
          }
        );
    }

    backClicked() {        
        window.top.close();
    }
}