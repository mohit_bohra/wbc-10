import { NgModule } from '@angular/core';
import { GroupsRoutes } from './groups.routes';
import { GroupsComponent } from './groups.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../global/global.component';
import { Services } from '../../services/app.services';
import { commonfunction } from '../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../navigationFulfilment/navigation.module';
import { TooltipModule } from "ngx-tooltip";

@NgModule({
  imports: [
    GroupsRoutes,
    CommonModule,
    FormsModule,
    NavigationModule,
    TooltipModule
  ],
  exports: [],
  declarations: [GroupsComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class GroupsModule { }
