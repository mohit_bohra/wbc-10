import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users.component';
import { CanDeactivateGuard } from '../../../services/can-deactivate-guard.service';

// route Configuration
const usersRoutes: Routes = [
  { path: '', component: UsersComponent, canDeactivate: [CanDeactivateGuard] }
];


@NgModule({
  imports: [RouterModule.forChild(usersRoutes)],
  exports: [RouterModule]
})
export class UsersRoutes { }
