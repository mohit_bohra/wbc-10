import { Component, ViewChild } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { FormIntactChecker } from '../../../services/formintactchecker.services';
import { DialogService } from '../../../services/dialog.service';
import { ReplaySubject } from 'rxjs';

@Component({
    templateUrl: 'groupusers.component.html',
    providers: [commonfunction]
})

export class UsersComponent {
    @ViewChild('addNewUserForm') addNewUserForm:any;
    resetPwdFlag: boolean = false;
    resetPasswordMsg: any;
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    _sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;
    showLoader: boolean;
    pageState: string = '35';

    unsavedUser: boolean = false;
    newRowCounter: number;
    userList:any = [];
    //userListLengthOriginal:any = [];
    groupId:any;
    grpId:any;
    selectedUser:string = "";
    userListLength: number = 0;
    customerName4Service:string = 'default';
    groupName:string;
    groupDescription:string;
    groupStatus: any;
    userId: any;
    errTxt: string = '';
    noDataFound: boolean = false;
    userListLengthOriginal:number = 0;
    userName: any;

    popupMsg:string = "";
    duplicateUserNameArray:any = new Array;
    showTooltip: boolean = false;
    userJSON:any = [];
    userJSONOrg:any = [];
    validUsername: boolean = true;
    validEmailId: boolean = true;
    statusChanged:boolean = false;
    rowToDelete: number = 0;

    loggedInUserID:string = "";
    loggedInUserGroup:string = "";
    superAdminGroup:string = "";
    updatedUsers:any = [];
    updatedUsersAmendments:any = [];

    userCred: boolean = false;
    userDesc: boolean = false;
    userAdminFlag: boolean = false;

    duplicateLogonCredArray:any = [];
    validLogCred: boolean = false;
   showcancel:boolean = false;
   loggedinCred: any;
   userStatusToEnableLiveUser:boolean = false;
    invalidEmailArray: any;

    private _formIntactChecker: FormIntactChecker;

    //Constructor is used to initialize variables during load time
    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, private formBuilder: FormBuilder, public dialogService: DialogService) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        //this.unsavedUser = true;
        this.groupId = sessionStorage.getItem('groupId4Users');
        this.groupDescription = sessionStorage.getItem('groupDescription');
        this.groupName = sessionStorage.getItem('groupname4User');
        console.log(this.groupName+"this.groupName");
        this.customerName = sessionStorage.getItem('orgType');
        this.superAdminGroup = this.objConstant.SUPERADMIN_GROUP;
        this.userName = sessionStorage.getItem('username');
        this.addNewUserForm = this.formBuilder.group({});
    }

    canDeactivate(): Promise<boolean> | boolean {
        if (!this.unsavedUser || sessionStorage.getItem('unsavedChanges')!=='true') {
            return true;
        }
        return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
      }
    
    // This method is called on page load.
    // Hence all the basic cosmetic related elements are initialized here
    ngOnInit() {
        this.imageUrl = this.whoImgUrl;
        //this.unsavedUser = true;
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
        sessionStorage.setItem('pageState', this.pageState);
        this.loggedInUserGroup = sessionStorage.getItem('loggedInUserGroupName');
        this.loggedInUserID = sessionStorage.getItem('userId');
        this.getGroupUser();
        console.log("selectedUser : " + this.selectedUser);
        console.log("unsavedUser : " + this.unsavedUser);
    }

    getGroupUser() {
        this.showLoader = true;
        this.newRowCounter = 0;
        let grpId = sessionStorage.getItem('groupId4Users');
        this._postsService.getGroupUser(this.customerName4Service, grpId).subscribe(data => { 
            this.showLoader = false;
            this.userJSON = data;
            this.userJSONOrg = data;
            
            this.userListLength = data.userList.length;
            this.userListLengthOriginal = data.userList.length;
            console.log(data);
            this.userList = data.userList;
            sessionStorage.setItem('ujs', JSON.stringify(this.userList));
            console.log(this.userListLengthOriginal);
            this.groupStatus = data.groupStatus;
            this.groupId = data.groupId;
            this.groupDescription = data.groupDescription;
        }
            , err => {
                console.log(err);
                this.showLoader = false;
                this.userListLength = 0;
                if(err.errorCode == '404.33.401'){
                    this.noDataFound = true;
                    this.errTxt = 'Sorry, No data found at this moment.';
                } else if(err.httpResponseCode == '503' || err.httpResponseCode == '404'){
                    this.noDataFound = true;
                    this.errTxt = 'Site is under maintenance. Please try after some time!';
                } else{ 
                    this._router.navigate(['/error404']);
                }
                //this._router.navigate(['/error404']);
            }
        );
    }

    resetPasswordFunction(selectedUser: any, pwdFlag: any){
        console.log(sessionStorage.getItem("userNameForGroup")); 
        console.log(pwdFlag)       ;
        if(pwdFlag == false){
            this._postsService.resetPassword(sessionStorage.getItem("userNameForGroup")).subscribe(userData => {
                this.showcancel=false;

                this.resetPasswordMsg = "Password has been reset and email notification sent to "+ sessionStorage.getItem("userNameForGroup");

                 this.resetPwdFlag = true;
            }, error => {
                 this.showcancel=false;
                this.resetPasswordMsg = "Password could not be reset for "+ sessionStorage.getItem("userNameForGroup") + ". Please retry.";
                this.resetPwdFlag = false;
            });
        }else{
            this.closePopup('resetPassword');
        }
                this.resetPwdFlag = false;
        
    }

    resetPassword(){        
        console.log(sessionStorage.getItem("userNameForGroup"));
        this.resetPasswordMsg = "Password will be reset for "+ sessionStorage.getItem("userNameForGroup");
        let sessionModal = document.getElementById('resetPassword');
        this.showcancel=true;
        
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('resetPassword').style.display = "block";
    }

    addNewUser() {
        this.unsavedUser = true;
        sessionStorage.setItem('unsavedChanges','true');
        this.showTooltip = true;
        let adminFlag:boolean = false;
        let userStatus:any;
        if(this.loggedInUserGroup == this.superAdminGroup){
            adminFlag = true;
            userStatus = 'active'

        }

        // Adding empty row
        this.userList[this.userListLength] = {
            "userId": "",
            "userName": "",
            "userDescription": "",
            "userLogon": "",
            "adminFlag": adminFlag,
            "duplicate":false,
            "newRow":true,
            "userStatus":userStatus
        }
        ++this.userListLength;
        //console.log(this.userListLengthOriginal);
        setTimeout(() => {
            var objDiv = document.getElementById("tableBody");
            objDiv.scrollTop = objDiv.scrollHeight;
        }, 500);
    }
    setStatus(idx: number, status: string) {
            this.unsavedUser = true;
            sessionStorage.setItem('unsavedChanges','true');
    //this.finalRoleIndexArray.push(idx);
            this.userList[idx].userStatus = status;
          
            if(this.userList[idx].userStatus!="active"){
             this.userStatusToEnableLiveUser = true;
            }
            else{
                this.userStatusToEnableLiveUser= false;
            }
        }
    resetUsers(){
        /*this.unsavedUser = false;
        this.showTooltip = false;
        this.selectedUser = "";
        this.getGroupUser();*/
        this._router.navigate(['groups', this.customerName]);
      
    }

    selectUser(item:any, idx:number) { //}) userName: any, userId: any, userDesc: any, userLogon: any){
        this.selectedUser = item.userId;
        sessionStorage.setItem('userNameForGroup', item.userName);
        sessionStorage.setItem('userLogOnCredentials', item.userLogon);
        sessionStorage.setItem('userDescription', item.userDesc);
        sessionStorage.setItem('userIdForGroup', item.userId);
        sessionStorage.setItem('userStatus', item.userStatus);
        this.userId = item.userId;
        if(this.loggedInUserGroup != this.superAdminGroup){
            if(item.adminFlag == true){
                this.selectedUser = '';
                this.showTooltip = true;
            }else{
                this.showTooltip = false;
            }
        }
        
        console.log("Selected User : "+ this.selectedUser);
    }

    saveUser(){
        if(this.unsavedUser){
            //this.unsavedUser = true;
            this.showTooltip = false;
            this.showLoader = true;
            this.checkDuplicationOnSave();
            if(this.duplicateUserNameArray.length == 0 && this.validUsername && this.validEmailId && this.duplicateLogonCredArray.length == 0){
               // console.log(this.userList);
                let FinalArray2Insert:any = [];
                let newFinalArray:any = [];
                console.log(this.updatedUsersAmendments)
                this.userList.forEach(function(item:any, idx:number){
                 if(item.newRow) {
                        item.userName = item.userName1.trim();
                        item.userLogon = item.userLogon.trim();
                        item.userDescription = item.userDescription.trim();
                        item.amendments = 'added';
                        newFinalArray.push(item);
                    } else {
                        if(item.userLogonFlag || item.userDescriptionFlag || item.adminFlagFlag || item.userStatusFlag){
                            item.amendments = 'updated';
                            let dummArr:any = [];
                            item.changedData = '';
                            if(item.userLogonFlag) {
                                dummArr.push('user credential');
                            }

                            if(item.userDescriptionFlag) {
                                dummArr.push('user status flag');
                            }

                            if(item.adminFlagFlag) {
                                dummArr.push('administrator rights');
                            }

                            if(item.userStatusFlag) {
                                dummArr.push('administrator rights');
                            }
                            
                            item.changedData = dummArr.join(", ");
                            item.userLogon = item.userLogon.trim();
                            item.userDescription = item.userDescription.trim();
                            newFinalArray.push(item);
                        }
                        
                    }
                    //FinalArray2Insert.push(item);
                    
                });
                 console.log("final array to insert")
                 console.log(newFinalArray)

                if (this.userJSON.length == 0 || this.userJSON == null) {
                    this.userJSON = {
                        "customerid": "default",
                        "groupId": this.groupId,
                        "groupName": this.groupName,
                        "groupDescription": this.groupDescription,
                        "groupStatus": this.groupStatus,
                        "sessionUserName": this.userName,
                        "userRoleAuditList": []
                    }
                }
               
                console.log("newFinalArray");
                console.log(newFinalArray);
                this.userJSON.userList = newFinalArray;
                this.userJSON.sessionUserName = sessionStorage.getItem("email");
                console.log("Final user json");
                console.log(this.userJSON);
                let groupID = sessionStorage.getItem('groupId4Users');
                this._postsService.createAuthorisationGroupUser(this.customerName4Service, groupID, this.userJSON).subscribe(data => {
                    console.log("response");
                    console.log(data);
                    this.showLoader = false;
                    this.userJSON = data;
                    this.userListLength = data.userList.length;
                    this.userList = data.userList;
                    this.noDataFound = false;

                    this.popupMsg = 'Group user(s) added/updated successfully.';
                    let sessionModal = document.getElementById('messageModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('messageModal').style.display = "block";
                    this.unsavedUser = false;
                    sessionStorage.setItem('unsavedChanges','false');
                    this.getGroupUser();
                }, 
                    (err:any) => {
                        if(err.httpResponseCode == '503' || err.httpResponseCode == '404'){
                            this.popupMsg = 'Site is under maintenance. Please try after some time!';
                            let sessionModal = document.getElementById('messageModal');
                            sessionModal.classList.remove('in');
                            sessionModal.classList.add('out');
                            document.getElementById('messageModal').style.display = "block";
                        } else{ 
                            this._router.navigate(['/error404']);
                        }
                        //console.log(err);
                        this.showLoader = false;
                    }
                );
            } else {
                this.popupMsg = "";
                if(this.duplicateUserNameArray.length > 0){
                    this.popupMsg += "User name(s) "+this.duplicateUserNameArray.join() + " already exist. \n";
                }
                if(!this.validUsername){
                    this.popupMsg += "Please enter valid user name. \n";
                }
                if(!this.validEmailId){
                    this.popupMsg += "Please enter valid email id. \n";
                }
                
                if(this.invalidEmailArray.length > 0){
                    this.popupMsg += "Email(s) "+this.invalidEmailArray.join() + " is invalid. \n";
                }
                if(this.duplicateLogonCredArray.length > 0){
                    this.popupMsg += "Login credential(s) "+this.duplicateLogonCredArray.join() + " already exist. \n";
                }
                // if(!this.validLogCred){
                //     this.popupMsg += "Please enter valid Logon credential.\n";
                // }

                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
            }
            this.showLoader = false;
        }
    }

    checkDuplicateUsingServiceCredentials(username:string, e:any=null, idx:number = null){
        this.showLoader = true;
        this._postsService.checkDuplicateCred(this.customerName4Service, 'logon', username).subscribe((data:any) => {
            console.log(data.isAvailable);
            //return data.isAvailable;
            if(data.isAvailable == "false") {
                console.log("123456");
                this.popupMsg = "Login credential " + username + " already exist.";
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
                //(<HTMLInputElement>e.target).focus();
                this.userList[idx].duplicateCred = true;
            } else {
                this.userList[idx].duplicateCred = false;
            }
            this.showLoader = false;
        });
    }

    checkDuplicateUsingService(username:string, e:any=null, idx:number = null){
        this.showLoader = true;
        this._postsService.checkDuplicate(this.customerName4Service, 'user', username).subscribe((data:any) => {
            console.log(data.isAvailable);
            //return data.isAvailable;
            if(data.isAvailable == "false") {
                console.log("123456");
                this.popupMsg = "User " + username + " already exist.";
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
                //(<HTMLInputElement>e.target).focus();
                this.userList[idx].duplicate = true;
            } else {
                this.userList[idx].duplicate = false;
            }
            this.showLoader = false;
        });
    }

    checkDuplicationOnSave(){
        let validUserName = true;
        let validEmailId = true;
        let validLogCred = true;
        let tempUserList = this.userList;
        let dummyArray:any = new Array;
        let dummyArrayCred:any = new Array;
        let invalidEmailArray:any = new Array;
        let that = this;
        this.userList.forEach(function(item1:any){
            if(item1.duplicate){
                dummyArray.push(item1.userName1);
            } else if(item1.userName1 && item1.userName1.trim() == ""){
                validUserName = false;
            }
            if(item1.duplicateCred){
                dummyArrayCred.push(item1.userLogon);
            } else if(item1.userLogon && item1.userLogon.trim() == ""){
                validLogCred = false;
            }

            if(item1.newRow && !item1.validEmail) {
                invalidEmailArray.push(item1.userName1);
                validEmailId = false;
            }
        });
        //console.log(dummyArray);
        this.duplicateUserNameArray = dummyArray;
        this.validUsername = validUserName;
        this.duplicateLogonCredArray = dummyArrayCred;
        this.validUsername = validUserName;
        this.invalidEmailArray = invalidEmailArray;
        this.validEmailId = validEmailId;
    }

    focusOutFunction(e:any, idx:number){
        console.log(e.srcElement.value);
        //let validEmailid=true;
        let enteredUsername = (e.srcElement.value).trim();
        console.log(enteredUsername);
        if (enteredUsername != "") {
           /*if ( /^[A-Za-z][0-9A-Za-z\d_.@-]*$/.test(enteredUsername)) {[a-z0-9._%+-]+@morrisonsplc.co.uk
                this.checkDuplicateUsingService(enteredUsername, e, idx);
           }*/
           if ( /^[a-z0-9._%+-]+@morrisonsplc.co.uk$/.test(enteredUsername)) {
                this.checkDuplicateUsingService(enteredUsername, e, idx);
                this.validEmailId=true;
                this.userList[idx].validEmail = true;
            } else {
                //alert("Please enter only letter and numeric characters");
                this.validEmailId=false;
                this.userList[idx].validEmail = false;
                this.popupMsg = "Invalid user email id"; 
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
                //(<HTMLInputElement>event.target).value = '';
                //(<HTMLInputElement>event.target).focus();
                return (false);
            }            
        }else{
            this.popupMsg = "Invalid - Can't be empty"; 
            let sessionModal = document.getElementById('messageModal');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('messageModal').style.display = "block";
            //(<HTMLInputElement>event.target).value = '';
            //(<HTMLInputElement>event.target).focus();
            return (false);
        }  
        //this.validEmailId = validEmailid;  
        console.log('in focus out function')   
    }

    storeInputVal(e:any, id:number){
        console.log('input value',e.srcElement.value);
        this.loggedinCred=e.srcElement.value;
    }

    focusOutFunctioncred(e:any, idx:number){
        console.log('test',this.loggedinCred);
        let prevValue=this.loggedinCred;
        console.log(e.srcElement.value);
        //console.log('test',idx);
        let enteredUsername = (e.srcElement.value).trim();
        console.log(enteredUsername);
        if (enteredUsername != "") {
           if ( /^[A-Za-z][0-9A-Za-z\d_.@-]*$/.test(enteredUsername)) {
               if(enteredUsername == prevValue){
                   return false;
               }else{
                    this.checkDuplicateUsingServiceCredentials(enteredUsername, e, idx);
               }
           } else {
                //alert("Please enter only letter and numeric characters");
                this.popupMsg = "Only the following special characters are permitted - full stop '.', underscore '_', hyphen '-', and asperand '@'"; 
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
                //(<HTMLInputElement>event.target).value = '';
                //(<HTMLInputElement>event.target).focus();
                return (false);
            }            
        }else{
            this.popupMsg = "Invalid - Can't be empty"; 
            let sessionModal = document.getElementById('messageModal');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('messageModal').style.display = "block";
            //(<HTMLInputElement>event.target).value = '';
            //(<HTMLInputElement>event.target).focus();
            return (false);
        }       
    }

    checkDuplicateUser(enteredUser:string, e:any){
        let duplicateFlag = false;
        let invalidGroup = false;
        if(enteredUser != ""){
            let that = this;
            that.checkDuplicateUsingService(enteredUser);
        } else {
            // Enter valid group name
            this.popupMsg = "Please enter valid user name.";
            let sessionModal = document.getElementById('messageModal');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('messageModal').style.display = "block";
        }
    }

    closePopup(popupName:string = 'messageModal'){
        this.popupMsg = "";
        let sessionModal = document.getElementById(popupName);
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById(popupName).style.display = "none";
    }

    navigateToLiveUser(){
        let url = 'groups/'+this.customerName+'/'+'all/users/all'+'/historyforalluser';
      setTimeout(() => this._router.navigateByUrl('/groups/'+this.customerName+'/all/users/all/historyforalluser'), 10);
    }
    navigateToUserRole(){
        //console.log(unsavedUser || selectedUser == '')
        if(this.selectedUser != '' && !this.unsavedUser){
            this._router.navigate(['groups', this.customerName, this.groupName, 'users',this.userId, 'setRoles']);
        }
    }

    navToViewUserPermission(){
        if(this.selectedUser != '' && !this.unsavedUser){
            // /groups/mccolls/MWEE-Admin/users/4/setPermissions
            let url = this.homeUrl + 'groups/' + encodeURIComponent(this.customerName) + '/' +this.groupName + '/users/' +  this.selectedUser + '/setPermissions';
            sessionStorage.setItem("pageState",'999');
            window.open(url); 
            /* this._router.navigate(['groups', this.customerName, this.groupName, 'users', this.selectedUser, 'setPermissions']);         */
        }
    }

    navToViewUserAmendement(){
        if(this.selectedUser != '' && !this.unsavedUser){
            let url = this.homeUrl + 'groups/' + encodeURIComponent(this.customerName) + '/' +this.groupName + '/users/' +  this.userId + '/amendment';
            sessionStorage.setItem("pageState",'999');
            window.open(url); 
            // this._router.navigate(['groups', this.customerName, this.groupName, 'users', this.userId, 'amendment']);
        }
    }

    navToViewUseristory(){
        if(this.selectedUser != '' && !this.unsavedUser){
            let url = this.homeUrl + 'groups/' + encodeURIComponent(this.customerName) + '/' +this.groupName + '/users/' +  this.userId + '/history';
            sessionStorage.setItem("pageState",'999');
            window.open(url); 
            // this._router.navigate(['groups', this.customerName, this.groupName, 'users', this.userId, 'history']);
        }
    }

    deleteRowPopup(i_no:number){
        this.rowToDelete = i_no;
        console.log(this.rowToDelete);
        let sessionModal = document.getElementById('deleteModal');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('deleteModal').style.display = "block";
    }

    deleteRow(i_no:number) {
        let dummyArr:any = [];
        this.userList.forEach(function(item:any, idx:number){
            if(i_no != idx){
                dummyArr.push(item);
            }
        });
        this.userList = dummyArr;
        --this.userListLength;
        console.log(this.userListLength + ' :: ' + this.userListLengthOriginal);
        if(this.userListLengthOriginal == this.userListLength) {
            console.log(this.statusChanged);
            if(!this.statusChanged){
                this.unsavedUser = false;
                sessionStorage.setItem('unsavedChanges','false');
            }
        }

        this.rowToDelete = 0;
        let sessionModal = document.getElementById('deleteModal');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('deleteModal').style.display = "none";
    }

    toggeleFlag(idx: number){
        console.log("model changed");
        this.showTooltip = true;
        this.unsavedUser = true;
        sessionStorage.setItem('unsavedChanges','true');
        this.statusChanged = true;
        this.userAdminFlag = true;
        this.updatedUsers.push(idx);
        console.log(this.updatedUsers);
    }

    // onValueUpdate(idx: number){
        
    // }

    onChange(val: any, idx: number){
        let userListOrg = JSON.parse(sessionStorage.getItem('ujs'));
        console.log("Org");
        console.log(userListOrg);
        this.unsavedUser = true;
        sessionStorage.setItem('unsavedChanges','true');
        if(typeof userListOrg[idx] !== "undefined"){
            this.updatedUsers.push(idx);
            console.log(this.updatedUsers);
            switch(val) {
                case "user credential":
                    if(userListOrg[idx].userLogon.trim() == this.userList[idx].userLogon.trim()) {
                        this.userList[idx].userLogonFlag = false;
                    } else {
                        this.userList[idx].userLogonFlag = true;
                    }
                    break;
                case "user description":
                    if(userListOrg[idx].userDescription.trim() == this.userList[idx].userDescription.trim()) {
                        this.userList[idx].userDescriptionFlag = false;
                    } else {
                        this.userList[idx].userDescriptionFlag = true;
                    }
                    break;
                case "administrator rights":
                    if(userListOrg[idx].adminFlag == this.userList[idx].adminFlag) {
                        this.userList[idx].adminFlagFlag = false;
                    } else {
                        this.userList[idx].adminFlagFlag = true;
                    }
                    break;
                 case"user status flag" :  
                      if(userListOrg[idx].userStatus == this.userList[idx].userStatus) {
                        this.userList[idx].userStatusFlag = false;
                    } else {
                        this.userList[idx].userStatusFlag = true;
                    }
                    break;
            }
        }
    }

    keyPress(){
        this.unsavedUser = true;
        sessionStorage.setItem('unsavedChanges','true');
    }
}