import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { GlobalComponent } from '../../../global/global.component';
import { Constant } from '../../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'search-users.component.html',
    providers: [commonfunction]
})

export class SearchUsersComponent {
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    _sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;
    showLoader: boolean;
    pageState: string = '40';

    userList:any = [];
    userListOrg:any = [];
    groupId:number = 0;
    grpId:string;
    selectedUser:string = "";
    selectedGroup:string = "";
    userListLength: number = 0;
    customerName4Service:string = 'default';
    groupName:string;
    groupDescription:string;
    groupStatus: any;
    userId: any;
    errTxt: string = '';
    noDataFound: boolean = false;
    searchUsername: string = "";
    showResult: boolean = false;
    selectedGroupName: string = "";

    //Constructor is used to initialize variables during load time
    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
    }

    // This method is called on page load.
    // Hence all the basic cosmetic related elements are initialized here
    ngOnInit() {
        this.imageUrl = this.whoImgUrl;
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
        sessionStorage.setItem('pageState', this.pageState);
        this.customerName = sessionStorage.getItem('orgType');
    }

    searchUsers() {
        this.showResult = true;
        this._postsService.searchUser('default', this.searchUsername).subscribe((data: any) => {
            console.log(data);
            this.showLoader = false;
            this.userListLength = data.userList.length;
            this.userList = data.userList;
            this.showResult = true;
        }, (err: any) => {
            console.log(err);
            this.showLoader = false;
            this.userListLength = 0;
            if(err.errorCode == '404.33.401'){
                this.noDataFound = true;
                this.errTxt = 'Sorry, No data found at this moment.';
            } else if(err.httpResponseCode == '503' || err.httpResponseCode == '404'){
                this.noDataFound = true;
                this.errTxt = 'Site is under maintenance. Please try after some time!';
            } else{ 
                this._router.navigate(['/error404']);
            }
        });
    }

    selectUser(user: any) {
        this.selectedUser = user.userId;
        this.selectedGroup = user.groupId;
        this.selectedGroupName = user.groupName;
        sessionStorage.setItem('userIdForGroup',  user.userId);
        console.log(sessionStorage.getItem(""))
        //sessionStorage.setItem('groupname4User', userId);
        sessionStorage.setItem('groupId4Users', user.groupId);
        sessionStorage.setItem('groupDescription', user.groupDescription);
        sessionStorage.setItem('groupname4User', user.groupName);
    }
    
    navigateToUserPermissions(){
        if(this.selectedUser != ''){
            // groups/mccolls/morrisons_admin/users/1/setPermissions
            let url = this.homeUrl + 'groups/' + encodeURIComponent(this.customerName) + '/' +this.selectedGroupName + '/users/' + this.selectedUser + '/setPermissions';
            sessionStorage.setItem("pageState",'999');
            window.open(url);
            // this._router.navigate(['groups', this.customerName, this.selectedGroupName, 'users', this.selectedUser, 'setPermissions']);
        }
    }

    navigateToUserAmendments(){
        if(this.selectedUser != ''){
            // http://localhost:7777/#/groups/mccolls/morrisons_admin/users/6/amendment
            let url = this.homeUrl + 'groups/' + encodeURIComponent(this.customerName) + '/' +this.selectedGroupName + '/users/' + this.selectedUser + '/amendment';
            sessionStorage.setItem("pageState",'999');
            window.open(url);
            // this._router.navigate(['groups', this.customerName, this.selectedGroupName, 'users', this.selectedUser, 'amendment']);
        }
    }

    navigatetoUserHistory() {
        if(this.selectedUser != ''){
            // http://localhost:7777/#/groups/mccolls/morrisons_admin/users/6/history
            let url = this.homeUrl + 'groups/' + encodeURIComponent(this.customerName) + '/' +this.selectedGroupName + '/users/' + this.selectedUser + '/history';
            sessionStorage.setItem("pageState",'999');
            window.open(url);
           /*  this._router.navigate(['groups', this.customerName, this.selectedGroupName, 'users', this.selectedUser, 'history']); */
        }
    }

    navigateToUserRoles(){
        if(this.selectedUser != ''){
            // http://localhost:7777/#/groups/mccolls/morrisons_admin/users/5/setRoles
            this._router.navigate(['groups', this.customerName, this.selectedGroupName, 'users', this.selectedUser, 'setRoles']);
        }
    }

    keyPress(event: any) {
        this._globalFunc.alphaNumeric(event);
    }
}