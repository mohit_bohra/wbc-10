import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchUsersComponent } from './search-users.component';

// route Configuration
const searchusersRoutes: Routes = [
  { path: '', component: SearchUsersComponent }
];


@NgModule({
  imports: [RouterModule.forChild(searchusersRoutes)],
  exports: [RouterModule]
})
export class SearchUsersRoutes { }
