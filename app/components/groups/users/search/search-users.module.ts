import { NgModule } from '@angular/core';
import { SearchUsersRoutes } from './search-users.routes';
import { SearchUsersComponent } from './search-users.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../../global/global.component';
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../../../navigationFulfilment/navigation.module';

@NgModule({
  imports: [
    SearchUsersRoutes,
    CommonModule,
    FormsModule,
    NavigationModule
  ],
  exports: [],
  declarations: [SearchUsersComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class SearchUsersModule { }
