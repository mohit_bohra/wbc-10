import { NgModule } from '@angular/core';
import { UsersRoutes } from './users.routes';
import { UsersComponent } from './users.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';
import { TooltipModule } from "ngx-tooltip";
@NgModule({
  imports: [
    UsersRoutes,
    CommonModule,
    FormsModule,
    NavigationModule,
    TooltipModule
  ],
  exports: [],
  declarations: [UsersComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class UsersModule { }
