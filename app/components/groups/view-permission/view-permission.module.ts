import { NgModule } from '@angular/core';
import { ViewPermissionComponent } from './view-permission.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';
import { ViewPermissionRoutes } from './view-permission.routes';
console.log("234");
@NgModule({
  imports: [
    ViewPermissionRoutes,
    CommonModule,
    FormsModule,
    NavigationModule
  ],
  exports: [],
  declarations: [ViewPermissionComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class ViewPermissionModule { }
