import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from '../users/users.component';
import { ViewPermissionComponent } from './view-permission.component';

// route Configuration
const viewPermissionRoutes: Routes = [
  { path: '', component: ViewPermissionComponent },
  //{ path: ':userId/setRoles', loadChildren: '../view-permission/view-permission.module#ViewPermissionModule'  }, // MWEE-2795
  //{ path: ':userId/permissions', loadChildren: '../permissions/permissions.module#PermissionsModule'  }, // MWEE-2800
];


@NgModule({
  imports: [RouterModule.forChild(viewPermissionRoutes)],
  exports: [RouterModule]
})
export class ViewPermissionRoutes { }
