import { Component } from '@angular/core';
import { PlatformLocation, Location } from '@angular/common';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'view-permission.component.html',
    providers: [commonfunction]
})

export class ViewPermissionComponent {
    prevPageState: string;
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    _sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;
    showLoader: boolean;
    pageState: string = '38';

    unsavedGroup: boolean = false;
    newRowCounter: number = 0;
    userPermissionList: any = [];
    selectedUser: string = "";
    userPermissionListLength: number = 0;
    customerName4Service: string = 'default';

    grpID: string;
    userId: String;
    role: string;
    groupName: string;
    groupDescription: string;
    groupId: any;
    userName: string;
    userDescription: string;
    userLogon: string;
    adminFlag: string;
    groupStatus: string;
    finalUserPermissionArrayForPlotting: any = [];
    noDataFound: boolean = false;
    errTxt: any;

    //Constructor is used to initialize variables during load time
    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, public _location: Location) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        this.groupId = sessionStorage.getItem('groupId4Users');
        this.groupDescription = sessionStorage.getItem('groupDescription');
        this.groupName = sessionStorage.getItem('groupname4User');
        this.customerName = sessionStorage.getItem('orgType');

    }

    // This method is called on page load.
    // Hence all the basic cosmetic related elements are initialized here
    ngOnInit() {
        console.log("abcd");
        this.imageUrl = this.whoImgUrl;
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');    
        this.prevPageState = sessionStorage.getItem('pageState');
        sessionStorage.setItem('pageState', this.pageState);     
        sessionStorage.setItem('pageState', '999');
        document.getElementById('diableLogo').style.cursor = "not-allowed";

        this._sub = this._route.params.subscribe((params: { orgType: string, grpID: string, userId: string }) => {

            console.log(this._router.url);
            console.log(params);
            sessionStorage.setItem('orgType', params.orgType);
            this.userId = params.userId;
        });
        this.getUserPermission();
    }

    getUserPermission() {
        this.showLoader = true;
        this.newRowCounter = 0;
        let grpId = sessionStorage.getItem('groupId4Users');
        let usrID = this.userId;
        let role = sessionStorage.getItem('role4Users');
        this._postsService.getUserPermission(this.customerName4Service, grpId, usrID, role).subscribe((data:any) => {
            this.showLoader = false;
            this.userPermissionListLength = data.permissionMasterList;
            this.userPermissionList = data.permissionMasterList;
            this.userName = data.userName;
            this.userDescription = data.userDescription;
            this.userLogon = data.userLogon;
            this.adminFlag = data.adminFlag;
            this.groupStatus = data.groupStatus;
            this.noDataFound = false;
            console.log(data)
            this.userPermissionArrayForPlotting();
        }
            , (err:any) => {
                if (err.errorCode == '404.33.401') {
                    this.noDataFound = true;
                    this.errTxt = 'Sorry, No data found at this moment.';
                } else if (err.httpResponseCode == '503' || err.httpResponseCode == '404') {
                    this.noDataFound = true;
                    this.errTxt = 'Site is under maintenance. Please try after some time!';
                } else {
                    this._router.navigate(['/error404']);
                }
                console.log(err);
                this.showLoader = false;
                //this._router.navigate(['/error404']);
            }
        );
    }

    userPermissionArrayForPlotting() {
        let dummyArr: any = [];
        this.userPermissionList.forEach(function (itm: any) {
            itm.permissionGroupList.forEach(function (itmGrpList: any) {
                console.log(itm.customerName);
                itmGrpList.permissionNameList.forEach(function (itmNameList: any) {
                    console.log(itmNameList);
                    if (itmNameList.permissionStatus == 'active') {
                        dummyArr.push({
                            'customer': itm.customerName,
                            'permGroup': itmGrpList.permissionGroup,
                            'permName': itmNameList.permissionName
                        });
                    }
                });
            });
        });
        this.finalUserPermissionArrayForPlotting = dummyArr;
        console.log("abc");
        console.log(this.finalUserPermissionArrayForPlotting);
        console.log(this.finalUserPermissionArrayForPlotting.length);
    }

    backClicked() {        
        window.top.close();
    }
}