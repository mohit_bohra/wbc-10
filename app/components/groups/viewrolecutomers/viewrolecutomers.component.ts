import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'viewrolecutomers.component.html',
    providers: [commonfunction]
})
export class ViewRoleCustomersComponent {
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    _sub: any;
    organisationList: any;
    customerName: string;
    customerName4Service: string;
    orgApiName: string;
    showLoader: boolean;
    pageState: string = '41';

    roleId: any;
    roleName: any;
    groupId: any;
    groupName: any;
    groupDescription: any;
    roleDescription: any;
    groupStatus: string = 'active';
    roleStatus: string = 'active';
    userRoleListLength: any;
    userRoleList: any;
    errTxt: string = 'Sorry, No data found at this moment.';
    noDataFound: boolean;


    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        this.userRoleListLength = 0;
        this.groupId = sessionStorage.getItem('groupId4Users');
        /*this.groupDescription = sessionStorage.getItem('groupDescription');
        this.groupName = sessionStorage.getItem('groupname4User'); */
        this.customerName4Service = this.objConstant.customerName4Service;
        this.customerName = sessionStorage.getItem('orgType');
        this.roleId = sessionStorage.getItem('roleId');
        /*this.roleName = sessionStorage.getItem('roleName');
        this.roleDescription = sessionStorage.getItem('roleDesc'); */

    }

    ngOnInit() {
        this.imageUrl = this.whoImgUrl;
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name'); 
        sessionStorage.setItem('pageState', '999');
        document.getElementById('diableLogo').style.cursor = "not-allowed"; 

        this._sub = this._route.params.subscribe((params: { orgType: string, groupName: string, roleName: string }) => {
            console.log(params);
            sessionStorage.setItem('orgType', params.orgType);
        });

        this.showLoader = true;
        this._postsService.getViewRoleUsersdata(this.customerName4Service, this.groupId, this.roleId).subscribe((data: any) => {
            this.showLoader = false;
            this.userRoleListLength = data.roleUsers.length;
            this.userRoleList = data.roleUsers;
            console.log(this.userRoleList);
            this.groupStatus = data.groupStatus;
            this.roleStatus = data.roleStatus;

            this.groupId = data.groupId;
            this.groupDescription = data.groupDescription;
            this.groupName = data.groupName;

            this.roleId = data.roleId;
            this.roleName = data.roleName;
            this.roleDescription = data.roleDescription;
        }
            , (err: any) => {
                console.log(err);
                this.showLoader = false;
                this.userRoleListLength = 0;
                if (err.errorCode == '404.33.401') {
                    this.noDataFound = true;
                    this.errTxt = 'Sorry, No data found at this moment.';
                } else if (err.httpResponseCode == '503' || err.httpResponseCode == '404') {
                    this.noDataFound = true;
                    this.errTxt = 'Site is under maintenance. Please try after some time!';
                } else {
                    this._router.navigate(['/error404']);
                }
                //this._router.navigate(['/error404']);
            }
        );

    }

    closeWindow() {
        window.top.close();
    }
}