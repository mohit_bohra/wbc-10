import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewRoleCustomersComponent } from './viewrolecutomers.component';

// route Configuration
const viewRoleCustomersRoutes: Routes = [
  { path: '', component: ViewRoleCustomersComponent }
];


@NgModule({
  imports: [RouterModule.forChild(viewRoleCustomersRoutes)],
  exports: [RouterModule]
})
export class ViewRoleCustomersRoutes { }
