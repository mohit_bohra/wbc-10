import { NgModule } from '@angular/core';
import { ViewUserHistoryComponent } from './viewuserhistory.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';
import { ViewUserHistoryRoutes } from './viewuserhistory.routes';

@NgModule({
  imports: [
    ViewUserHistoryRoutes,
    CommonModule,
    FormsModule,
    NavigationModule
  ],
  exports: [],
  declarations: [ViewUserHistoryComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class ViewUserHistoryModule { }
