import { Component } from '@angular/core';
import { PlatformLocation, Location } from '@angular/common';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'viewuserhistory.component.html',
    providers: [commonfunction]
})

export class ViewUserHistoryComponent {
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    _sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;
    showLoader: boolean;
    pageState: string = '47';
    groupName: any;
    groupId: any;

    noDataFound:boolean = false;

    customerName4Service:string = 'default';
    userId:any;
    userLoginHistory: any;
    userLoginHistoryLength: number = 0;
    errTxt:string = "";
    userData:any = [];

    groupDescription:string;
    groupStatus: any;
    userName:string;
    userDescription:string;
    userLogon:string;
    adminFlag:string;
    

    
    //Constructor is used to initialize variables during load time
    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, public _location: Location) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        this.groupName = sessionStorage.getItem('groupname4User');
        this.groupId = sessionStorage.getItem('groupId4Users');
        this.customerName = sessionStorage.getItem('orgType');
    }

    // This method is called on page load.
    // Hence all the basic cosmetic related elements are initialized here
    ngOnInit() {
        this.imageUrl = this.whoImgUrl;
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
        sessionStorage.setItem('pageState', this.pageState);
        this.groupName = sessionStorage.getItem('groupname4User');
        this.groupDescription = sessionStorage.getItem('groupDescription')
        this.groupId = sessionStorage.getItem('groupId4Users');
        this.customerName = sessionStorage.getItem('orgType');
        this.userName = sessionStorage.getItem('userNameForGroup');
        this.userId = sessionStorage.getItem('userIdForGroup');
        this.userDescription = sessionStorage.getItem('userDescription');
        this.userLogon = sessionStorage.getItem('userLogOnCredentials');
        this.adminFlag = sessionStorage.getItem('userStatus');
        sessionStorage.setItem('pageState', '999');
        this.getUserHistory();
        document.getElementById('diableLogo').style.cursor = "not-allowed"; 
    }

    getUserHistory() {
        console.log("User history");
        this.showLoader = true;
        let grpId = sessionStorage.getItem('groupId4Users');
        this.userId = sessionStorage.getItem('userIdForGroup');
        this._postsService.getUserHistory(this.customerName4Service, this.groupId, this.userId).subscribe((data:any) => { 
            this.showLoader = false;
            this.userData = data;
            
            this.groupName = data.groupName;
            this.groupDescription = data.groupDescription;
            this.userName = data.userName;
            this.userDescription = data.userDescription;
            this.userLogon = data.userLogon;
            this.adminFlag = data.adminFlag;
            console.log("Nile123");
            console.log(data.userLoginDetails);
            this.userLoginHistoryLength = data.userLoginDetails.length;
            if(this.userLoginHistoryLength == 0){
                this.noDataFound = true;
            }
            this.userLoginHistory = data.userLoginDetails;
            console.log(this.userLoginHistory);
        }
            , (err:any) => {
                console.log(err);
                this.showLoader = false;
                this.userLoginHistoryLength = 0;
                if(err.errorCode == '404.33.401'){
                    this.noDataFound = true;
                    this.errTxt = 'Sorry, No data found at this moment.';
                } else if(err.httpResponseCode == '503' || err.httpResponseCode == '404'){
                    this.noDataFound = true;
                    this.errTxt = 'Site is under maintenance. Please try after some time!';
                } else{ 
                    this._router.navigate(['/error404']);
                }
                //this._router.navigate(['/error404']);
            }
        );
    }/* 

    backClicked(){
        this._location.back();
        // this._router.navigate(['groups', this.customerName, this.groupName, 'users']);
    } */

    backClicked() {        
        window.top.close();
    }
}