import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from '../users/users.component';
import { ViewUserHistoryComponent } from './viewuserhistory.component';

// route Configuration
const viewUserHistoryRoutes: Routes = [
  { path: '', component: ViewUserHistoryComponent }
];


@NgModule({
  imports: [RouterModule.forChild(viewUserHistoryRoutes)],
  exports: [RouterModule]
})
export class ViewUserHistoryRoutes { }
