import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from '../users/users.component';
import { ViewUserAmendmentComponent } from './viewuseramendment.component';

// route Configuration
const viewUserAmendmentRoutes: Routes = [
  { path: '', component: ViewUserAmendmentComponent }
];


@NgModule({
  imports: [RouterModule.forChild(viewUserAmendmentRoutes)],
  exports: [RouterModule]
})
export class ViewUserAmendmentRoutes { }
