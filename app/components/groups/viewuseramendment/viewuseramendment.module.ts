import { NgModule } from '@angular/core';
import { ViewUserAmendmentComponent } from './viewuseramendment.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';
import { ViewUserAmendmentRoutes } from './viewuseramendment.routes';

@NgModule({
  imports: [
    ViewUserAmendmentRoutes,
    CommonModule,
    FormsModule,
    NavigationModule
  ],
  exports: [],
  declarations: [ViewUserAmendmentComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class ViewUserAmendmentModule { }
