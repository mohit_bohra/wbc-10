import { Component } from '@angular/core';
import { PlatformLocation, Location } from '@angular/common';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'viewuseramendment.component.html',
    providers: [commonfunction]
})

export class ViewUserAmendmentComponent {
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    _sub: any;
    organisationList: any;
    customerName: string;
    orgApiName: string;
    showLoader: boolean;
    pageState: string = '46';
    groupName: any;
    groupDesc: any;
    groupId: any;
    groupStatus:string  = "";
    userName: any;
    userDesc: any;
    userLogOn: any;
    userId: any;
    userStatus: any;
    adminFlag: any;
    userAmendmentList: any =[];
    userAmendmentListLength: number;
    errTxt: any;

    noDataFound:boolean = false;

    //Constructor is used to initialize variables during load time
    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, public _location: Location) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        this.groupName = sessionStorage.getItem('groupname4User');
        this.groupDesc = sessionStorage.getItem('groupDescription')
        this.groupId = sessionStorage.getItem('groupId4Users');
        this.customerName = sessionStorage.getItem('orgType');
        this.userName = sessionStorage.getItem('userNameForGroup');
        this.userId = sessionStorage.getItem('userIdForGroup');
        this.userDesc = sessionStorage.getItem('userDescription');
        this.userLogOn = sessionStorage.getItem('userLogOnCredentials');
        this.userStatus = sessionStorage.getItem('userStatus');
    }

    // This method is called on page load.
    // Hence all the basic cosmetic related elements are initialized here
    ngOnInit() {
        this.imageUrl = this.whoImgUrl;
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
        sessionStorage.setItem('pageState', this.pageState);
        sessionStorage.setItem('pageState', '999');
        document.getElementById('diableLogo').style.cursor = "not-allowed"; 

        this.showLoader = true;
        this._postsService.getViewUserAmendments(this.customerName, this.groupId, this.userId).subscribe((data: any) => {
            this.showLoader = false;
            this.userAmendmentList = data.userRoleAuditList;
            this.userAmendmentListLength = data.userRoleAuditList.length;
            this.adminFlag = data.adminFlag;
            if(this.adminFlag == true){
                this.adminFlag = 'Yes'
            }else{
                this.adminFlag = 'No'
            }
            this.userName = data.userName;
            this.userDesc = data.userDescription;
            this.userLogOn = data.userLogon;
            this.groupStatus = data.groupStatus;
            if(this.userAmendmentListLength == 0){
                this.errTxt = "Sorry, No data found at this moment.";
            }
        },  (err:any) => {
            if(err.errorCode == '404.33.401'){
                this.noDataFound = true;
                this.errTxt = 'Sorry, No data found at this moment.';
            } else if(err.httpResponseCode == '503' || err.httpResponseCode == '404'){
                this.noDataFound = true;
                this.errTxt = 'Site is under maintenance. Please try after some time!';
            } else{ 
                this._router.navigate(['/error404']);
            }
            console.log(err);
            this.showLoader = false;
        });
    }

    /* backClicked(){
        //this._router.navigate(['groups', this.customerName, this.groupName, 'users'])
        this._location.back();
    }
     */

    backClicked() {        
        window.top.close();
    }

}