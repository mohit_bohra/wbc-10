import { NgModule } from '@angular/core';
import { ManageRoleCustomersRoutes } from './managerolecustomers.routes';
import { ManageRoleCustomersComponent } from './managerolecustomers.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';
import { TooltipModule } from "ngx-tooltip";

@NgModule({
  imports: [
    ManageRoleCustomersRoutes,
    CommonModule,
    FormsModule,
    NavigationModule,
    TooltipModule
  ],
  exports: [],
  declarations: [ManageRoleCustomersComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class ManageRoleCustomersModule { }
