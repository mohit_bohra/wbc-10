import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageRoleCustomersComponent } from './managerolecustomers.component';

// route Configuration
const manageRoleCustomersRoutes: Routes = [
  { path: '', component: ManageRoleCustomersComponent }
];


@NgModule({
  imports: [RouterModule.forChild(manageRoleCustomersRoutes)],
  exports: [RouterModule]
})
export class ManageRoleCustomersRoutes { }
