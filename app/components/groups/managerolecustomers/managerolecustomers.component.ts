import { Component } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { stat } from 'fs';

@Component({
    templateUrl: 'managerolecustomers.component.html',
    providers: [commonfunction]
})
export class ManageRoleCustomersComponent {
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
     _sub: any;
    groupId:any;
    groupName:string;
    groupDescription:string;
    organisationList: any;
    customerName: string;
    orgApiName: string;
    showLoader: boolean;
    pageState: string = '45';
    customerName4Service:string;
    noDataFound: boolean;
    errTxt: string;
    orgType: any;
    roleId: any;
    customerList:any;
    unsavedGroup: boolean = false;   
    customerListLength: any;
    roleName: any;
    roleDescription: any;
    groupStatus: any;
    roleStatus: any;
    selectCust: string = "";
    roleJSON: any =[];
    popupMsg:string = "";
    statusChanged:boolean = false;
    finalRoleIndexArray:any = [];
    showTooltip: boolean = false;

    
    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        this.customerName = sessionStorage.getItem('orgType');
        this.customerName4Service = this.objConstant.customerName4Service;
        this.roleDescription = sessionStorage.getItem("roleDesc");
        this.roleStatus = sessionStorage.getItem("roleStatus");
        this.groupStatus = sessionStorage.getItem("groupStatus");
    }

    ngOnInit() {
        this.imageUrl = this.whoImgUrl;
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
        sessionStorage.setItem('pageState', this.pageState);

        this._sub = this._route.params.subscribe((params: { orgType: string, groupName: string , roleName:string }) => {
                sessionStorage.setItem('orgType', params.orgType);        
                this.orgType =this.customerName4Service;
                this.groupId = sessionStorage.getItem('groupId4Users');
                this.roleId = sessionStorage.getItem("roleId");
                this.roleName = params.roleName;

         sessionStorage.setItem("roleName",this.roleName);

         }); 

         this.getGroupRoleCustomers();
                  
    }

    resetCustomers(){
        if(this.unsavedGroup){
            this.unsavedGroup = false;
            this.showTooltip = false;
            this.selectCust = "";
            
            this.getGroupRoleCustomers();
            setTimeout(() => {
                var objDiv = document.getElementById("tableBody");
                objDiv.scrollTop = 0;
            }, 500);
            } else{
                this._router.navigate(['groups', this.customerName, this.groupName, 'roles' ]); 
            }
    }

    saveRoleCustomers(){
        if(this.unsavedGroup){
            this.showTooltip = false;
            let unique = this.finalRoleIndexArray.filter(function(item:any, i:number, ar:any){ return ar.indexOf(item) === i; });
            console.log(unique);
            let roleArr:any = [];
            let that = this;
            unique.forEach(function(id:number){
                console.log(id);
                console.log(that.customerList[id]);
                roleArr.push(that.customerList[id]);
            });
            that = null;
            this.roleJSON.customerList = roleArr;
            console.log(JSON.stringify(this.roleJSON));
            this._postsService.updateGroupRoleCustomers(this.orgType, this.groupId, this.roleId, this.roleJSON).subscribe((data: any) => {
                this.showLoader = false;
                this.roleJSON = data;
                this.customerListLength = data.customerList.length;
                this.customerList = data.customerList;
                this.noDataFound = false;
                this.selectCust = "";
                this.unsavedGroup = false;

                this.popupMsg = 'Customer(s) updated successfully.';
                let sessionModal = document.getElementById('messageModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('messageModal').style.display = "block";
            })
        }
    }

    selectCustomer(customerId: any, customerName: any, status:any){
        this.selectCust = customerName;
        sessionStorage.setItem('roleCustId', customerId);
        sessionStorage.setItem('roleCustName', customerName);
        sessionStorage.setItem('roleCustStatus', status);
    }

    getGroupRoleCustomers(){
        this.showLoader = true;
        this._postsService.getGroupRoleCustomers(this.orgType, this.groupId, this.roleId).subscribe((data:any) => { 
            this.showLoader = false;
            this.groupName = data.groupName;
            this.groupDescription = data.groupDescription;
            this.groupId = data.groupId;
            this.groupStatus = data.groupStatus;
            this.roleId = data.roleId;
            this.roleName = data.roleName;
            this.roleDescription = data.roleDescription;
            this.roleStatus = data.roleStatus;
            this.roleJSON = data;
            this.customerListLength = data.customerList.length;
            console.log(data);
            this.customerList = data.customerList;
        }
        , (err:any) => {
                console.log(err);
                this.showLoader = false;
                this.customerListLength = 0;
                if(err.errorCode == '404.33.401'){
                    this.noDataFound = true;
                    this.errTxt = 'Sorry, No data found at this moment.';
                } else if(err.httpResponseCode == '503' || err.httpResponseCode == '404'){
                    this.noDataFound = true;
                    this.errTxt = 'Site is under maintenance. Please try after some time!';
                } else{ 
                    this._router.navigate(['/error404']);
                }
                //this._router.navigate(['/error404']);
            }
        );
    }

    

    setStatus(idx: number, status: string) {
        this.unsavedGroup = true;
        this.finalRoleIndexArray.push(idx);
        this.customerList[idx].status = status;
    }

    goToManageCustomerPermission(){
        if(!this.unsavedGroup && this.selectCust != ''){
        this._router.navigate(['groups', this.customerName, this.groupName, 'roles', this.roleName, 'customers',this.selectCust ]); 
    }
}

    closePopup(popupName:string = 'messageModal'){
        this.popupMsg = "";
        let sessionModal = document.getElementById(popupName);
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById(popupName).style.display = "none";
    }
}