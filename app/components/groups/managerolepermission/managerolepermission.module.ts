import { NgModule } from '@angular/core';
import { ManageRolePermissionRoutes } from './managerolepermission.routes';
import { ManageRolePermissionComponent } from './managerolepermission.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';

@NgModule({
  imports: [
    ManageRolePermissionRoutes,
    CommonModule,
    FormsModule,
    NavigationModule
  ],
  exports: [],
  declarations: [ManageRolePermissionComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class ManageRolePermissionModule { }
