import { Component, ViewChild } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from '../../../services/dialog.service';
import { FormBuilder } from '@angular/forms';

@Component({
    templateUrl: 'managerolepermission.component.html',
    providers: [commonfunction]
})
export class ManageRolePermissionComponent {
    @ViewChild('saverolepermission') saverolepermission: any;
    displaydata: any;
    objConstant = new Constant();
    whoImgUrl: any;
    orgimgUrl: string;
    homeUrl: any;
    imageUrl: string;
    _sub: any;
    groupId: any;
    userId: any;
    roleId: any;
    groupName: string;
    groupDescription: string;
    organisationList: any;
    orgType: string;
    orgApiName: string;
    rolename: any;
    customerName: any;
    customerId: any;
    showLoader: boolean;
    pageState: string = '48';
    customerName4Service: string = 'default';
    noDataFound: boolean;
    errTxt: string;
    err: any;
    permissionName: string;
    unsavedGroup: boolean = true;
    roleDescription: any;
    manageRolePermissionListLength: number = 0;
    manageRolePermissionList: any = [];
    groupStatus: any;
    roleStatus: any;
    statusChanged: boolean;
    showTooltip: boolean = false;
    manageRoleJSON: any = [];
    popupMsg: string = ""; 
    status:any;
    urlCustomerName:string = "";
    permisionName:string;
    manageRoleJSONOrg:any = [];


    newRowCounter: number = 0;
    updatedRowsArray:any = [];

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, location: PlatformLocation, public _cfunction: commonfunction, private formBuilder: FormBuilder, public dialogService: DialogService) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        this.groupId = sessionStorage.getItem('groupId4Users');
        this.groupDescription = sessionStorage.getItem('groupDescription');
        this.groupName = sessionStorage.getItem('groupname4User');
        this.orgType = sessionStorage.getItem('orgType');
        this.customerName4Service = this.objConstant.customerName4Service;
        this.customerId = sessionStorage.getItem('roleCustId');
        this.roleId = sessionStorage.getItem("roleId");
        this.roleDescription = sessionStorage.getItem("roleDesc");
        this.roleStatus = sessionStorage.getItem("roleStatus");
        this.groupStatus = sessionStorage.getItem("groupStatus");
        this.saverolepermission = this.formBuilder.group({});

    }

    canDeactivate(): Promise<boolean> | boolean {
        if (!this.unsavedGroup || sessionStorage.getItem('unsavedChanges')!=='true') {
            return true;
        }
        return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
    }

    ngOnInit() {
        this.imageUrl = this.whoImgUrl;
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
        sessionStorage.setItem('pageState', this.pageState);

        this._sub = this._route.params.subscribe((params: { orgType: string, groupName: string, roleName: string, customerName: string }) => {
            console.log(params);
            sessionStorage.setItem('orgType', params.orgType);
            if (sessionStorage.getItem('groupname4User') == null || sessionStorage.getItem('groupname4User') == 'undefined') {
                sessionStorage.setItem('groupname4User', params.groupName);
            }
            this.groupName = sessionStorage.getItem('groupname4User');

            if (sessionStorage.getItem('roleName') == null || sessionStorage.getItem('roleName') == 'undefined') {
                sessionStorage.setItem('roleName', params.roleName);
            }
            this.rolename = sessionStorage.getItem('roleName');

            if (sessionStorage.getItem('roleCustName') == null || sessionStorage.getItem('roleCustName') == 'undefined') {
                sessionStorage.setItem('roleCustName', params.customerName);
            }
            this.customerName = sessionStorage.getItem('roleCustName');
            this.urlCustomerName = params.orgType;


        });

        this.getManageRolePermission();
    }

    resetCustomers(){
        console.log(['/groups', this.urlCustomerName, this.groupName, 'roles', this.rolename, 'customers' ]);
        this._router.navigate(['/groups', this.urlCustomerName, this.groupName, 'roles', this.rolename, 'customers' ]); 
        
    }


    getManageRolePermission() {
        this.showLoader = true;
        this.newRowCounter = 0;
        let grpId = sessionStorage.getItem('groupId4Users');
        let usrID = '@all';
        let roleId = sessionStorage.getItem("roleId");
        this.customerName4Service = sessionStorage.getItem("roleCustId");

        this._postsService.getManageRolePermission(this.customerName4Service, grpId, usrID, roleId).subscribe((data: any) => {
            this.showLoader = false;
            this.manageRoleJSON = data;
            this.manageRoleJSONOrg = this.manageRoleJSON;
            console.log(JSON.stringify(this.manageRoleJSON))
            this.manageRolePermissionListLength = data.permissionMasterList;
            this.manageRolePermissionList = data.permissionMasterList;
            this.noDataFound = false;
            this.groupDescription = data.groupDescription;
            this.groupName = data.groupName;
            this.rolename = data.roleName;
            this.roleDescription = data.roleDescription;
           this.groupStatus = data.groupStatus;
           this.roleStatus = data.roleStatus;
           this.customerName =data.customerName;
           /* this.groupDescription = sessionStorage.getItem('groupDescription');
            this.groupName = sessionStorage.getItem('groupname4User');
            this.roleDescription = sessionStorage.getItem("roleDesc");
            this.roleStatus = sessionStorage.getItem("roleStatus");
            this.groupStatus = sessionStorage.getItem("groupStatus");*/

            this.finalmanageRolePermissionArrayForPlotting();

            this.displaydata = data.permissionMasterList.permissionGroupList;

        }
            , (err: any) => {
                if (err.errorCode == '404.33.401') {
                    this.noDataFound = true;
                    this.errTxt = 'Sorry, No data found at this moment.';
                } else if (err.httpResponseCode == '503' || err.httpResponseCode == '404') {
                    this.noDataFound = true;
                    this.errTxt = 'Site is under maintenance. Please try after some time!';
                } else {
                    this._router.navigate(['/error404']);
                }
                console.log(err);
                this.showLoader = false;
            });

    }
    
       closePopup(popupName:string = 'messageModal'){
        this.popupMsg = "";
        let sessionModal = document.getElementById(popupName);
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById(popupName).style.display = "none";
    }

    setStatus(idx: number, status: string, orgStatus:string) {
        
        console.log(idx);
        console.log(status);
        console.log(orgStatus);
        if(status != orgStatus){
            this.updatedRowsArray.push(idx);    
            this.unsavedGroup = true;
            sessionStorage.setItem('unsavedChanges','true');
            this.statusChanged = true;
            this.finalmanageRolePermissionArrayForPlotting[idx].status = status;
            this.status= this.finalmanageRolePermissionArrayForPlotting[idx].status;
            this.permisionName=this.finalmanageRolePermissionArrayForPlotting[idx].permName;
            let spl = this.finalmanageRolePermissionArrayForPlotting[idx].seq.split(":");
            this.manageRoleJSONOrg.permissionMasterList[spl[0]].changed = true;
            this.manageRoleJSONOrg.permissionMasterList[spl[0]].permissionGroupList[spl[1]].changed = true;
            this.manageRoleJSONOrg.permissionMasterList[spl[0]].permissionGroupList[spl[1]].permissionNameList[spl[2]].changed = true;
            this.manageRoleJSONOrg.permissionMasterList[spl[0]].permissionGroupList[spl[1]].permissionNameList[spl[2]].permissionStatus = status;
        } else {
            this.finalmanageRolePermissionArrayForPlotting[idx].status = status;
            this.status= this.finalmanageRolePermissionArrayForPlotting[idx].status;
            this.permisionName=this.finalmanageRolePermissionArrayForPlotting[idx].permName;
            let spl = this.finalmanageRolePermissionArrayForPlotting[idx].seq.split(":");
            this.manageRoleJSONOrg.permissionMasterList[spl[0]].changed = false;
            this.manageRoleJSONOrg.permissionMasterList[spl[0]].permissionGroupList[spl[1]].changed = false;
            this.manageRoleJSONOrg.permissionMasterList[spl[0]].permissionGroupList[spl[1]].permissionNameList[spl[2]].changed = false;
            this.manageRoleJSONOrg.permissionMasterList[spl[0]].permissionGroupList[spl[1]].permissionNameList[spl[2]].permissionStatus = status;

            let dummyArray:any = [];
            this.updatedRowsArray.forEach(function(itm:any){
                if(itm != idx){
                    dummyArray.push(itm);
                }
            });
            this.updatedRowsArray = dummyArray;
            if(this.updatedRowsArray.length == 0){
                this.unsavedGroup = false;
                sessionStorage.setItem('unsavedChanges','false');
                this.statusChanged = false;
            }
        }
        console.log(this.manageRoleJSONOrg);
    }
    saveManageRolePermission() {
        if (this.unsavedGroup) {
            this.unsavedGroup = true;
            sessionStorage.setItem('unsavedChanges','true');
            this.showTooltip = false;
            this.showLoader = true;
            
            console.log(this.manageRolePermissionList);
            let FinalArray2Insert: any = [];
            let dummyArr: any = [];
            let that = this;
            console.log(this.manageRoleJSON);
            let unique = this.finalmanageRolePermissionArrayForPlotting;
            console.log(unique+"unique");
            console.log(JSON.stringify(this.manageRoleJSON))
            // this.manageRoleJSON.permissionGroupList.permissionNameList = FinalArray2Insert;
       
            /*this.manageRolePermissionList.forEach(function (itm: any) {
                itm.permissionGroupList.forEach(function (itmGrpList: any) {
                    itmGrpList.permissionNameList.forEach(function (itmNameList: any) {
                        console.log(itmNameList);
                        if(that.permisionName==itmNameList.permissionName){
                        itmNameList.permissionStatus = that.status;
                        FinalArray2Insert.push(itmNameList);
                        itmGrpList.permissionNameList = FinalArray2Insert;
                        }
                      
                    });
                });
            });*/
            let manageRoleJSONOrg1:any = this.manageRoleJSONOrg;
            let permissionNameList:any = [];
            let permissionMasterList:any = [];
            let permissionGroupList:any = [];
            let permissionList:any = [];
            console.log("this.manageRoleJSONOrg");
            console.log(this.manageRoleJSONOrg);
            this.manageRoleJSONOrg.permissionMasterList.forEach(function (itm1:any, index1:number){
                if(itm1.changed){
                    permissionGroupList = [];
                    itm1.permissionGroupList.forEach(function (itmGrpList: any, index2:number) {
                        console.log(itmGrpList.changed);
                        if(itmGrpList.changed){
                            permissionNameList = [];
                            itmGrpList.permissionNameList.forEach(function (itmNameList: any, index3:number) {
                                if(itmNameList.changed){
                                    if(itmNameList != null)
                                        permissionNameList.push(itmNameList);

                                }
                            });
                            if(permissionNameList.length > 0){
                                permissionGroupList.push({
                                    "permissionNameList":permissionNameList,
                                    "permissionGroup":itmGrpList.permissionGroup
                                });
                            }
                        }
                    });
                    if(permissionGroupList.length > 0){
                        permissionMasterList.push({"permissionGroupList":permissionGroupList});
                    }
                    
                }
            });
            
            //console.log(this.manageRoleJSON);
            // this.manageRoleJSON.permissionGroupList.permissionNameList = FinalArray2Insert;
            permissionList.push({"permissionMasterList":permissionMasterList});

            console.log("Updated json");
            console.log(permissionMasterList);
            if(permissionMasterList.length > 0){
                manageRoleJSONOrg1.permissionMasterList = permissionMasterList;
                console.log(manageRoleJSONOrg1);
                console.log(JSON.stringify(manageRoleJSONOrg1));
                let grpId = sessionStorage.getItem('groupId4Users');
                let usrID = sessionStorage.getItem('userId4Users');
                usrID = '@all';
                let roleId = sessionStorage.getItem("roleId");
                //let that = this;
                this._postsService.updateManageRolePermissionGroup(this.customerName4Service, grpId, usrID, roleId, manageRoleJSONOrg1).subscribe((data: any) => {
                    console.log("response");
                    console.log(data);
                    this.showLoader = false;
                    this.manageRoleJSON = data;
                    this.manageRoleJSONOrg = this.manageRoleJSON;
                    console.log(JSON.stringify(this.manageRoleJSON))
                    this.manageRolePermissionListLength = data.permissionMasterList;
                    this.manageRolePermissionList = data.permissionMasterList;
                    //this.finalmanageRolePermissionArrayForPlotting();

                    let dummyArr: any = [];
                    this.manageRolePermissionList.forEach(function (itm: any, i:number) {
                        itm.permissionGroupList.forEach(function (itmGrpList: any, j:number) {
                            itmGrpList.permissionNameList.forEach(function (itmNameList: any, k:number) {
                                console.log(itmNameList);
                                if(sessionStorage.getItem('roleCustName') == 'All_Wholesale_Customers'){
                                    if(itmGrpList.permissionGroup == 'morrisonsPermissions'){
                                        dummyArr.push({
                                            'status': itmNameList.permissionStatus,
                                            'customer': itm.customer,
                                            'permGroup': itmGrpList.permissionGroup,
                                            'permName': itmNameList.permissionName,
                                            'seq': i+":"+j+":"+k
                                        });
                                    }
                                } else{
                                    if(itmGrpList.permissionGroup != 'morrisonsPermissions'){
                                        dummyArr.push({
                                            'status': itmNameList.permissionStatus,
                                            'customer': itm.customer,
                                            'permGroup': itmGrpList.permissionGroup,
                                            'permName': itmNameList.permissionName,
                                            'seq': i+":"+j+":"+k
                                        });
                                    }
                                }
                            });
                        });
                    });
                    this.finalmanageRolePermissionArrayForPlotting = dummyArr;


                    this.noDataFound = false;
                    this.groupDescription = data.groupDescription;
                    this.groupName = data.groupName;
                    this.rolename = data.roleName;
                    this.roleDescription = data.roleDescription;
                    this.groupStatus = data.groupStatus;
                    this.roleStatus = data.roleStatus;
                    this.customerName =data.customerName;
                    //that.finalmanageRolePermissionArrayForPlotting();

                    this.displaydata = data.permissionMasterList.permissionGroupList;
                    
                    
                    this.noDataFound = false;

                    this.unsavedGroup = false;
                    sessionStorage.setItem('unsavedChanges','false');

                    this.popupMsg = 'Manage Role Permission added/updated successfully.';
                    let sessionModal = document.getElementById('messageModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('messageModal').style.display = "block";
                },
                    (err: any) => {
                        if (err.httpResponseCode == '503' || err.httpResponseCode == '404') {
                            this.popupMsg = 'Site is under maintenance. Please try after some time!';
                            let sessionModal = document.getElementById('messageModal');
                            sessionModal.classList.remove('in');
                            sessionModal.classList.add('out');
                            document.getElementById('messageModal').style.display = "block";
                        } else {
                            this._router.navigate(['/error404']);
                        }
                        console.log(err);
                        this.showLoader = false;
                    }
                );
            } else {

            }
            this.statusChanged = false;
        }
        this.showLoader = false;
    }

    finalmanageRolePermissionArrayForPlotting() {
        let dummyArr: any = [];
        this.manageRolePermissionList.forEach(function (itm: any, i:number) {
            itm.permissionGroupList.forEach(function (itmGrpList: any, j:number) {
                itmGrpList.permissionNameList.forEach(function (itmNameList: any, k:number) {
                    console.log(itmNameList);
                    if(sessionStorage.getItem('roleCustName') == 'All_Wholesale_Customers'){
                        if(itmGrpList.permissionGroup == 'morrisonsPermissions'){
                            dummyArr.push({
                                'status': itmNameList.permissionStatus,
                                'customer': itm.customer,
                                'permGroup': itmGrpList.permissionGroup,
                                'permName': itmNameList.permissionName,
                                'seq': i+":"+j+":"+k
                            });
                        }
                    } else{
                        if(itmGrpList.permissionGroup != 'morrisonsPermissions'){
                            dummyArr.push({
                                'status': itmNameList.permissionStatus,
                                'customer': itm.customer,
                                'permGroup': itmGrpList.permissionGroup,
                                'permName': itmNameList.permissionName,
                                'seq': i+":"+j+":"+k
                            });
                        }
                    }

                    /* dummyArr.push({
                        'status': itmNameList.permissionStatus,
                        'orgStatus': itmNameList.permissionStatus,
                        'customer': itm.customer,
                        'permGroup': itmGrpList.permissionGroup,
                        'permName': itmNameList.permissionName,
                        'seq': i+":"+j+":"+k
                    }); */


                });
            });
        });
        this.finalmanageRolePermissionArrayForPlotting = dummyArr;

        console.log(this.finalmanageRolePermissionArrayForPlotting + "finalmanageRolePermissionArrayForPlotting");
    }
}

