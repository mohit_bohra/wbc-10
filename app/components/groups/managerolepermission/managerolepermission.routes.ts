import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageRolePermissionComponent } from './managerolepermission.component';
import { CanDeactivateGuard } from '../../../services/can-deactivate-guard.service';

// route Configuration
const manageRolePermissionRoutes: Routes = [
 { path: '', component: ManageRolePermissionComponent, canDeactivate: [CanDeactivateGuard] }
];


@NgModule({
  imports: [RouterModule.forChild(manageRolePermissionRoutes)],
  exports: [RouterModule]
})
export class ManageRolePermissionRoutes {}
