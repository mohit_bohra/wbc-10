import { Component, ViewEncapsulation } from '@angular/core';
import { Services } from '../../services/app.services';
import { GlobalComponent } from '../global/global.component';
import { Constant } from '../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { commonfunction } from '../../services/commonfunction.services';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { isArray } from 'util';

import {PagerService} from '../../services/pager.service'

@Component({
    selector: 'reports',
    templateUrl: 'reports.component.html',
    styles: [`

        .page{
            width: 375px; 
            position: absolute; 
            top: 0; 
            right: 0; 
            text-align: right; 
            margin-right: 30px; 
            font-size: 12px;
        }
        
        .page  .pagination { margin: 10px 0 !important;}
        
        .page .pagination>.active>a, .page .pagination>.active>a:focus, .page .pagination>.active>a:hover, .page .pagination>.active>span, .page .pagination>.active>span:focus, .page .pagination>.active>span:hover { background-color : #004e36 !important;  } 
        
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
            border-color: #004e36 !important;
        }

    `],

    encapsulation: ViewEncapsulation.Emulated,
    
    providers: [commonfunction]
})
export class ReportsComponent {
    public daterange: any = {};
    public options: any = {
        locale: { format: 'DD-MM-YYYY' },
        alwaysShowCalendars: true,
        "maxSpan": {
            "days": 7
        }
    };
    // var for datepicker outside click
    public showDatepicker: boolean = true;
    advSearch: boolean = false;
    advSearchforRange: boolean = false;
    dealSearch1: boolean = false;
    orderno: string;
    productcode: string;
    public catalgoueDate: Date = new Date();
    objConstant = new Constant();
    errorMsgString: string;
    showErrorMsg: boolean;
    errorMsg: boolean = false;
    whoImgUrl: any;
    errorMessage: any;
    catalogueCode: any;
    catalogueCodeSelected: any;
    catalogueList: any;
    catalogueListLength: any;
    catalogueUpdatedDate: any;
    catalogueno: string;
    showLoader: boolean;
    hideOrganisation: boolean = true;
    catalogueResult: boolean = true;
    pageState: number = 5;
    customerName: string;
    customerDispayName: string;
    style: string;
    organisationList: any;
    organisationListChange: any;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disabledOrder: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    disabledDashboard: boolean = false;
    private _sub: any;
    disabledExportCatalogue: boolean;
    disabledUploadCatalogue: boolean;
    succMSg: string;
    reportTypes: any;
    reportTypeID: any;
    upReportId: any;
    reportData: any;
    reportDataLength: any;
    reportPermission: boolean;
    dateRangeReportPermission: boolean;
    errorMsgPopup: any;
    reportDataDownload: any;
    requestReportData: any;

    //for catalogue Enquiry redevelopment
    catalogueTypes: any;
    catalogueType: string;
    cataloguemasterListLength: any;
    cataloguedetailListLength: any;
    catalogueDetailList: any;
    strInfoNotAvail: string;
    start: number;
    limit: number;
    count: number;
    hitNext: boolean;
    catalogueFilename: string;
    printHeader: boolean;
    s3path: any;
    errorMsg1: boolean;

    public todayDate: Date = new Date();

    homeUrl: any;

    imgName: string = "";

    selectStatus: any;
    selectChannels: any;
    public maxDate: Date = new Date();
    public myDate = new Date();
    // primary fields
    channel: any;
    status: any;
    shipto: any;
    public deliverydate: Date = new Date();
    invoiceId: any;

    // disable variable for primary
    channelsshow: boolean;
    channelHide: boolean;
    statusshow: boolean;
    statusHide: boolean;
    shiptoshow: boolean;
    shipToHide: boolean;
    deliverydateshow: boolean;
    deliverydateHide: boolean;
    invoiceIdshow: boolean;
    invoiceIdHide: boolean;

    // secondary fields
    public orderdate: Date = new Date();
    public invoicedate: Date = new Date();
    shipfrom: any;

    // disable variable for secondary
    orderdateshow: boolean;
    orderdateHide: boolean;
    invoicedateshow: boolean;
    invoicedateHide: boolean;
    shipfromshow: boolean;
    shipfromHide: boolean;

    // field selected number
    fieldSelected = new Array;
    fieldSelectedCount: number = 0;

    sortByColumn: string = '';
    reverseSorting: boolean = false;
    orderListLength: number = 0;
    orderList: any = [];
    orderResult: boolean = false;
    loggedUser: string = "";

    reportTypeforDateRange: any;


    AllLinesPostJSON: any = {};
    showErrorMsgNew: boolean = false;

    orderdate1: string = "";
    deliverydate1: string = "";
    dispatchDate1: string = "";
    invoicedate1: string = "";

    showDateDiffError: boolean = false;
    orderDateDiff: boolean = false;
    invoiceDateDiff: boolean = false;
    dispatchDateDiff: boolean = false;
    deliverydateDiff: boolean = false;
    dispatchDAteParamID: any;

    allOrderLinesReportSubmit: boolean = true;
    ordersReceivedByStoreAndCategoryView: boolean = true;
    periodicRepricingDraftSubmissionReportView: boolean = true;
    periodicRepricingSubmissionReportView: boolean = true;
    productRepricingDraftSubmissionReportView: boolean = true;
    shortedOrderLinesReportView: boolean = true;
    productRepricingSubmissionReportView: boolean = true;
    shortedOrderLinesReportSubmit: boolean = false;
    allOrderLinesReportView: boolean = true;
    allOrderLinesDateRangeReportView: boolean = true;
    dealReportView: boolean = true;
    itemnotFoundReportView: boolean = true;
    netNegativeReportView: boolean = true;
    outOfRangeReportView: boolean = true;
    storepickUnexpectedFeesErrorReportView : boolean = true;
    storepickCommisionMismatchReportView : boolean = true;
    storepickSellerCentralErrorReportView : boolean = true;    
    storepickInDayInventoryReportView : boolean = true;

    reportArr: any = [];
    fianlArrayReportTyes: any = []
    reportArrBtn: any = [];
    permissionArray: any = [];
    reporttypeBtn: boolean = false;

    moduleName: string = 'reports';

    terminateFlg: boolean = false;

    siteMaintanceFlg: boolean;
    siteMaintanceFlg1: boolean;
    siteMSg: any;
    maintenanceErrorMsg: string;
    rangeDateVal: any;
    order: string;
    ascending: boolean;
    deploymentDetailsMsg: string;
    featureDetailsMsg: string;
    businessMessageDetailsMsg: string;
    deploymentDetailsMsglength: any;
    featureDetailsMsglength: any;
    businessMessageDetailsMsglength: any;
    dealStratDate =  new Date();
    dealEndDate =  new Date();
    dealenddate = new Date();
    dealstartdate = new Date();
   // dealReportCustId: Array<string> = ['41','42','43','44','45'];

   // pager object
   pager: any = {};

   // paged items

   pageDisplay: number = 5;
   firstDisplay: number = 3;
   lastDisplay: number = 2;

   pagedItems: any[];

   paginationDisplay:boolean =false;
   
   finalPageDisplay :boolean =false;

   paginationReport: any[] = ['Storepick '];

   loaderSet: boolean = false;

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, private daterangepickerOptions: DaterangepickerConfig, public _cfunction: commonfunction, private pagerService: PagerService) {
        // img urls
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.s3path = this.objConstant.s3pathBucket;
        this.rangeDateVal = this.objConstant.rangeDateVal;

        // set default date null to calender
        this.orderdate = void 0;
        this.deliverydate = void 0;
        this.invoicedate = void 0;
        this.maxDate = new Date();
        this.myDate.setDate(this.maxDate.getDate() + 7);

        this.dealStratDate.setDate(this.maxDate.getDate() - 42 );
        this.dealEndDate.setDate(this.maxDate.getDate() + 42);

        this.shiptoshow = false;
        this.statusshow = false;
        this.channelsshow = true;
        this.invoiceIdshow = false;
        this.shipfromshow = false;
        this.deliverydateshow = false;
        this.orderdateshow = false;
        this.invoicedateshow = false;

        this.order = "reportType";
        this.ascending = true;

        this.channelHide = true;
        this.statusHide = true;
        this.orderdateHide = true;
        this.invoicedateHide = true;
        this.invoiceIdHide = true;
        this.shipToHide = true;
        this.shipfromHide = true;
        this.deliverydateHide = true;
        let today = new Date();
        let dd: any = today.getDate();
        let mm: any = today.getMonth() + 1;
        let year: any = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        let dateStr = dd + "-" + mm + "-" + year;
        console.log(dateStr);
        this.daterangepickerOptions.settings = {
            maxDate: dateStr,
            autoUpdateInput: false,
            locale: { format: 'DD-MM-YYYY' },
            maxSpan: { days: 3 }
        };

       
    }


    setPage(page: number) {

        this.loaderSet = true;

        var recordPerPage =15;
        var totalPage =0;

        if (page < 1 || page > this.pager.totalPages) {
            return;
        }


        //debugger;
        
        this.start = ((page-1) * this.limit);
        
        this._postsService.getReports(this.customerName, this.reportTypeID, this.start).subscribe(data => {
            
            
            console.log(data.reports);
            this.errorMsgPopup = "";
            if (this.reportTypeID != null) {
                this.reportData = data.reports;
                if (data.reportMaster == undefined || data.reportMaster == '') {
                    this.reportTypes = JSON.parse(sessionStorage.getItem('report_master'));
                }
                console.log("orgi data ids reports");
                console.log(this.reportData);
                this.reportDataLength = this.reportData.length;
                this.count = data.paginationMetaData.count;
                this.getReportsDownload();
                this.errorMsg1 = false;
            } else {
                this.reportTypes = data.reportMaster;
                console.log('service report arrat');
                console.log(this.reportTypes);
                sessionStorage.setItem("report_master", JSON.stringify(this.reportTypes));
            }
            this.loaderSet=false;
        }, error => {
            console.log(error);
            this.showLoader = false;
            if (this.reportTypeID != null) {
                this.errorMsg1 = true;
                this.reportDataLength = 0;
                this.errorMsgPopup = error.errorMessage;
                this.reportTypes = JSON.parse(sessionStorage.getItem('report_master'));
                let that = this;
                this.reportArr.forEach(function (item: any, index: any) {
                    console.log(item);
                    that.reportArrBtn.forEach(function (itemBtn: any, idx: any) {
                        console.log(itemBtn);
                        if (item.reportType == itemBtn.reportType) {
                            item['submitBtm'] = itemBtn.submitBtm;
                        }
                        console.log(itemBtn);
                    });
                });


                let thatt = this;
                this.reportArr.forEach(function (item: any, index: any) {
                    console.log(item);
                    thatt.reportTypes.forEach(function (itemBtn: any, idx: any) {
                        console.log(itemBtn);
                        if (item.reportType.toLowerCase().trim() == itemBtn.reportType.toLowerCase().trim()) {
                            item.reportTypeId = itemBtn.reportTypeId;
                            item['iddd'] = itemBtn.reportTypeId;
                        }
                    });
                });
                var uniqueArray = this.removeDuplicates(this.reportArr, "reportTypeId");
                console.log((uniqueArray));

                this.reportArr = uniqueArray;
                console.log("master lsit");
                console.log(this.reportArr);
                console.log("final array for mapping ");

                console.log(this.reportArr);
                console.log(this.reportArr);
                this.fianlArrayReportTyes = this.reportArr;

            } else {
                this.errorMsg1 = false;
            }

            this.loaderSet = false;
           
        });

        // get pager object from service
        totalPage = this.count;
        this.pager = this.pagerService.getPager(totalPage, page, recordPerPage);

        // get current page of items
        this.pagedItems = this.reportData.slice(this.pager.startIndex, this.pager.endIndex + 1);
        
        this.reportData =this.pagedItems;

        
    }

    // function works on page load
    ngOnInit() {
        //debugger;
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {

            this.loggedUser = sessionStorage.getItem('name');
            /* document.getElementById("orderDatePicker").style.display = "none";
            document.getElementById("deliveryDatePicker").style.display = "none";
            document.getElementById("invoiceDatePicker").style.display = "none";
 */
            this.shipfromshow = true;
            /*  document.getElementById("orderdateshow").classList.remove('disable');
             document.getElementById("invoicedateshow").classList.add('disable'); */


            this.printHeader = true;
            this.showLoader = false;
            this.catalogueno = '';
            this.showErrorMsg = false;
            this.reportPermission = false;
            this.dateRangeReportPermission = false;
            this.orderno = '';
            this.productcode = '';
            this.shipfrom = '';
            this.shipto = '';
            this.status = '';
            this.channel = '';
            this.invoiceId = '';
            this.errorMsgPopup = "";
            // initially show primary field on load
            this.deliverydateshow = false;

            this.disabledManageCatalogue = false;

            // user permission
            this.disabledCusChange = false;
            this.disabledPermission = false;
            this.disabledReports = false;
            this.disabledCatalogue = false;
            this.disabledOrder = false;
            this.disabledOrderRaise = false;
            this.disabledCatalogueRaise = false;
            this.disabledManageCatalogue = false;
            this.disabledExportCatalogue = false;
            this.disabledUploadCatalogue = false;
            this.strInfoNotAvail = "-";
            this.catalogueType = "";
            this.succMSg = this.strInfoNotAvail;

            this.selectStatus = [{ status: 'Please select', value: '' },
            { status: 'Raised', value: 'raised' },
            { status: 'Confirmed', value: 'confirmed' },
            { status: 'Confirmed Partial', value: 'confirmed-partial' },
            { status: 'Picked', value: 'picked' },
            { status: 'Picked Partial', value: 'picked-partial' },
            { status: 'Shipped', value: 'shipped' },
            { status: 'Shipped Partial', value: 'shipped-partial' },
            { status: 'Receipted', value: 'receipted' },
            { status: 'Receipted Partial', value: 'receipted-partial' },
            { status: 'Accounted', value: 'accounted' },
            { status: 'Accounted Partial', value: 'accounted-partial' },
            { status: 'Invoiced', value: 'invoiced' },
            { status: 'Invoiced Partial', value: 'invoiced-partial' },
            { status: 'Complete', value: 'complete' },
            { status: 'Errored', value: 'errored' },
            { status: 'Errored Price', value: 'errored-price' },
            { status: 'Deferred', value: 'deferred' },
            { status: 'Error Reported', value: 'error-reported' },
            { status: 'Order On Hold', value: 'order-on-hold' }

            ];

            // window scroll function
            this._globalFunc.autoscroll();

            // set header basket data
            this.pageState = 5;
            this.start = 0;
            this.limit = 15;
            this.count = 0;
            this.hitNext = false;
            sessionStorage.setItem('pageState', '5');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                let cusType = params.orgType;
                let orgApiNameTemp;
                console.log("customer name");
                this.orgApiName = cusType;
                console.log(cusType);
                this.imgName = this.getCustomerImage(params.orgType);
                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
                    if (res == 'sessionActive') {
                        this.getConfigService(this.moduleName);
                        switch (this.customerName) {
                            case "Amazon":
                                this.selectChannels = [{ channel: 'Please select', value: '' },
                                { channel: 'Fresh (Dabba)', value: 'PRXKD' },
                                { channel: 'Prime Now', value: 'THSV2' },
                                { channel: 'Pantry', value: 'THSV3' },
                                { channel: 'Core', value: 'MP8QW' },
                                { channel: 'Milk Prime now', value: 'MJB7O' },
                                { channel: 'Milk Dabba', value: 'MJBE7' },
                                { channel: 'Store Pick', value: 'OF' }

                                ];
                                break;

                            case "RONTEC":
                                this.selectChannels = [{ channel: 'Please select', value: '' },
                                { channel: 'Convenience', value: 'MOR' },
                                { channel: 'Safeway', value: 'SAF' },
                                ];
                                break;
                            case "RONTEC-B":
                                this.selectChannels = [{ channel: 'Please select', value: '' },
                                { channel: 'Convenience', value: 'MOR' },
                                { channel: 'Safeway', value: 'SAF' },
                                ];
                            break;

                            case "mccolls":
                                this.selectChannels = [{ channel: 'Please select', value: '' }, { channel: 'CNV', value: 'CNV' }];
                                break;
                        }
                        console.log("orgApiNameTemp" + this.customerName);

                        console.log('reportas array');
                        console.log(this.reportArr);
                        console.log('reportas btn arry list');
                        console.log(this.reportArrBtn);
                        this.getReports().then(res => {
                            console.log(JSON.stringify(res));
                            console.log('reportas btn arry list');
                        console.log(this.reportArrBtn);
                            if (res == 'reportMaster') {
                                console.log(this.reportArr);
                                console.log(JSON.stringify(this.reportArr));
                                /* this.reportArr.forEach(function (item: any, index: any) {
                                    console.log(item);
                                    that.reportArrBtn.forEach(function (itemBtn: any, idx: any) {
                                        console.log(itemBtn);
                                        if (item.reportTypeId == itemBtn.reportTypeId) {
                                            item['submitBtm'] = itemBtn.submitBtm;
                                        }
                                        console.log(itemBtn);
                                    });
                                }); */
                                let that = this;
                                
                                this.reportArr.forEach(function (item: any, index: any) {
                                    //console.log(item);
                                    that.reportArrBtn.forEach(function (itemBtn: any, idx: any) {
                                        console.log(itemBtn);
                                        if (item.reportType == itemBtn.reportType) {
                                            item['submitBtm'] = itemBtn.submitBtm;
                                        }
                                        //console.log(itemBtn);
                                    });
                                });
                                //console.log('Report : ' +  JSON.stringify(this.reportArr));
                                //debugger;
                                let thatt = this;
                                this.reportArr.forEach(function (item: any, index: any) {
                                    //console.log(item);
                                    thatt.reportTypes.forEach(function (itemBtn: any, idx: any) {
                                        //console.log(itemBtn);
                                        if (item.reportType.toLowerCase().trim() == itemBtn.reportType.toLowerCase().trim()) {
                                            item.reportTypeId = itemBtn.reportTypeId;
                                            item['iddd'] = itemBtn.reportTypeId;
                                        }
                                    });
                                });
                                
                                console.log(this.reportArr);
                                var uniqueArray = this.removeDuplicates(this.reportArr, "reportTypeId");
                                console.log((uniqueArray));

                                this.reportArr = uniqueArray;
                                console.log("master lsit");
                                console.log(this.reportArr);
                                console.log("final array for mapping ");

                                console.log(this.reportArr);
                                console.log(this.reportArr);
                                this.fianlArrayReportTyes = this.reportArr;
                            }
                        });


                        /* let result = arr1.map(val => {
                            return Object.assign({}, val, arr2.filter(v => v.columnId === val.columnId)[0]);
                        });
                        
                        console.log(result); */
                    }
                }, reason => {
                    console.log(reason);
                });
            });
        }


        //this.siteMaintanceFlg1 = sessionStorage.getItem('site');
        // if(sessionStorage.getItem('site') == "true"){
            
        //     this.siteMaintanceFlg1 = false;
        // }else { 
        //     this.siteMSg = "Site is under maintenance";
        //     this.siteMaintanceFlg1 = true;
        // }

    }
    setEndDate(val:any){
        console.log(val);
        //debugger;
    }
    removeDuplicates(originalArray: any, prop: any) {        
        var newArray = [];
        var lookupObject = {};
        console.log("removeduplicate");
        console.log(originalArray);
        for (var i in originalArray) {
            lookupObject[originalArray[i][prop]] = originalArray[i];
        }

        for (i in lookupObject) {
            newArray.push(lookupObject[i]);
        }
        return newArray;
    }

    formatSelection(val: any) {
        return this._globalFunc.formatSelection(val);
    }

    onScrolltop() {
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));

        this._globalFunc.autoscroll();
    }


    resetOnClose() {
        this.hitNext = false;
    }

    // onScrollDown(catalogueCode: any, catalogueType: any)
    onScrollDown() {
        return new Promise((resolve, reject) => {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            //console.log(res);debugger;
            if (res == 'sessionActive') {

                this.start = this.start + this.limit;
                if (this.count > this.start) {
                    this.showLoader = true;
                    this._postsService.getReports(this.customerName, this.reportTypeID, this.start).subscribe(data => {
                        this.showLoader = false;
                        this.count = data.paginationMetaData.count;
                        this.reportData = this.reportData.concat(data.reports);
                           console.log(isArray(this.reportArr));
                                /* this.reportArr.forEach(function (item: any, index: any) {
                                    console.log(item);
                                    that.reportArrBtn.forEach(function (itemBtn: any, idx: any) {
                                        console.log(itemBtn);
                                        if (item.reportTypeId == itemBtn.reportTypeId) {
                                            item['submitBtm'] = itemBtn.submitBtm;
                                        }
                                        console.log(itemBtn);
                                    });
                                }); */
                                let that = this;
                                this.reportArr.forEach(function (item: any, index: any) {
                                    console.log(item);
                                    that.reportArrBtn.forEach(function (itemBtn: any, idx: any) {
                                        console.log(itemBtn);
                                        if (item.reportType == itemBtn.reportType) {
                                            item['submitBtm'] = itemBtn.submitBtm;
                                        }
                                        console.log(itemBtn);
                                    });
                                });


                                let thatt = this;
                                this.reportArr.forEach(function (item: any, index: any) {
                                    console.log(item);
                                    thatt.reportTypes.forEach(function (itemBtn: any, idx: any) {
                                        console.log(itemBtn);
                                        if (item.reportType.toLowerCase().trim() == itemBtn.reportType.toLowerCase().trim()) {
                                            item.reportTypeId = itemBtn.reportTypeId;
                                            item['iddd'] = itemBtn.reportTypeId;
                                        }
                                    });
                                });
                                var uniqueArray = this.removeDuplicates(this.reportArr, "reportTypeId");
                                console.log((uniqueArray));

                                this.reportArr = uniqueArray;
                                console.log("master lsit");
                                console.log(this.reportArr);
                                console.log("final array for mapping ");

                                console.log(this.reportArr);
                                console.log(this.reportArr);
                                this.fianlArrayReportTyes = this.reportArr;
                            
                                }, err => {
                            
                            console.log(err);
                            this.catalogueno = '';
                            this.showLoader = false;
                            this.reportTypes = JSON.parse(sessionStorage.getItem('report_master'));
                            let that = this;
                            this.reportArr.forEach(function (item: any, index: any) {
                                console.log(item);
                                that.reportArrBtn.forEach(function (itemBtn: any, idx: any) {
                                    console.log(itemBtn);
                                    if (item.reportType == itemBtn.reportType) {
                                        item['submitBtm'] = itemBtn.submitBtm;
                                    }
                                    console.log(itemBtn);
                                });
                            });
                            let thatt = this;
                            this.reportArr.forEach(function (item: any, index: any) {
                                console.log(item);
                                thatt.reportTypes.forEach(function (itemBtn: any, idx: any) {
                                    console.log(itemBtn);
                                    if (item.reportType.toLowerCase().trim() == itemBtn.reportType.toLowerCase().trim()) {
                                        item.reportTypeId = itemBtn.reportTypeId;
                                        item['iddd'] = itemBtn.reportTypeId;
                                    }
                                });
                            });
                            var uniqueArray = this.removeDuplicates(this.reportArr, "reportTypeId");
                            console.log((uniqueArray));
                            this.reportArr = uniqueArray;
                            console.log("master lsit");
                            console.log(this.reportArr);
                            console.log("final array for mapping ");
                            console.log(this.reportArr);
                            console.log(this.reportArr);
                            this.fianlArrayReportTyes = this.reportArr;
                            this.reportDataLength = 0;
                            this._router.navigate(['/error404']);
                        }
                    );
                } else {
                    let that = this;
                    this.reportArr.forEach(function (item: any, index: any) {
                        console.log(item);
                        that.reportArrBtn.forEach(function (itemBtn: any, idx: any) {
                            console.log(itemBtn);
                            if (item.reportType == itemBtn.reportType) {
                                item['submitBtm'] = itemBtn.submitBtm;
                            }
                            console.log(itemBtn);
                        });
                    });


                    let thatt = this;
                    this.reportArr.forEach(function (item: any, index: any) {
                        console.log(item);
                        thatt.reportTypes.forEach(function (itemBtn: any, idx: any) {
                            console.log(itemBtn);
                            if (item.reportType.toLowerCase().trim() == itemBtn.reportType.toLowerCase().trim()) {
                                item.reportTypeId = itemBtn.reportTypeId;
                                item['iddd'] = itemBtn.reportTypeId;
                            }
                        });
                    });
                    var uniqueArray = this.removeDuplicates(this.reportArr, "reportTypeId");
                    console.log((uniqueArray));

                    this.reportArr = uniqueArray;
                    console.log("master lsit");
                    console.log(this.reportArr);
                    console.log("final array for mapping ");

                    console.log(this.reportArr);
                    console.log(this.reportArr);
                    this.fianlArrayReportTyes = this.reportArr;
                }
             }
         }, reason => {
            console.log(reason);
        });
});
    }

    // navigate function
    redirect(page: any, orgType: any) {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {

                if (page == 'addproduct') {
                    let url = this.homeUrl + page + '/' + encodeURIComponent(orgType);
                    window.open(url);
                } else {
                    this._router.navigate([page, orgType]);
                }

            }
        }, reason => {
            console.log(reason);
        });
    }
    // change organisation
    changeOrgType(page: any, orgType: any) {


        // this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
        // if (res == 'sessionActive') {
        console.log(orgType);
        console.log(page);
        // reset field on change of organisation 
        this.catalogueno = '';
        this.catalogueList = [];
        this.catalogueListLength = 0;
        this.catalogueResult = true;
        this.catalogueType = '';
        this.catalogueCode = '';
        this.hitNext = false;
        this.start = 0;
        this.count = 0;
        this.reportPermission = false;
        this.dateRangeReportPermission = false;
        this.advSearch = true;
        this.reportData = [];
        this.reportDataLength = 0;
        this.reportTypes = [];
        this.advSearch = false;
        this.advSearchforRange = false;
        this.dealSearch1 = false;
        this.reporttypeBtn = false;
        sessionStorage.removeItem('report_master');
        this.toggleOrganisation();
        //this._router.navigate([page, orgType]);
        this._router.navigate(this._globalFunc.redirectToComponent('reports', orgType));
        //     }
        // }, reason => {
        //     console.log(reason);
        // });

    }

    // download functionality
    exportData() {       
        return this._globalFunc.exportData('hiddenDivForExport', this.customerName + 'reports');       
    }

    printCatalogue() {
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        var printContentsNav = document.getElementById('printNavCatalogue').innerHTML;
        var printContents = document.getElementById('exportable').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContentsNav + printContents + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + printContentsNav + printContents + '</body></html>');
            popup.document.close();

        }


    }


    ngOnDestroy() {
        // clean sub to avoid memory leak

    }

    // change organisation div show and hide
    toggleOrganisation() {
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
            border.classList.remove('searchborder');
        }
        else {

            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
            border.classList.add('searchborder');

            this.reportTypeID = undefined;

        }
    }

    // close operation of pop up
    closeOperation() {

        this.catalogueList = '';

    }

    /*// key press event to disable special charater
    keyPress(event: any) {
        this._globalFunc.alphaNumericWithSpace(event);
    }*/


    getReports() {

        return new Promise((resolve, reject) => {
            this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
                if (res == 'sessionActive') {
                    this._postsService.getReports(this.customerName, this.reportTypeID, this.start).subscribe(data => {
                        //debugger;
                        console.log(data);
                        this.errorMsgPopup = "";
                        if (this.reportTypeID != null) {
                            this.reportData = data.reports;
                            if (data.reportMaster == undefined || data.reportMaster == '') {
                                this.reportTypes = JSON.parse(sessionStorage.getItem('report_master'));
                            }
                            console.log("orgi data ids reports");
                            console.log(this.reportData);
                            this.reportDataLength = this.reportData.length;
                            this.count = data.paginationMetaData.count;
                            this.getReportsDownload();
                            this.errorMsg1 = false;
                        } else {
                            this.reportTypes = data.reportMaster;
                            console.log('service report arrat');
                            console.log(this.reportTypes);
                            sessionStorage.setItem("report_master", JSON.stringify(this.reportTypes));
                        }
                        resolve("reportMaster");
                    }, error => {
                        console.log(error);
                        this.showLoader = false;
                        if (this.reportTypeID != null) {
                            this.errorMsg1 = true;
                            this.reportDataLength = 0;
                            this.errorMsgPopup = error.errorMessage;
                            this.reportTypes = JSON.parse(sessionStorage.getItem('report_master'));
                            let that = this;
                            this.reportArr.forEach(function (item: any, index: any) {
                                console.log(item);
                                that.reportArrBtn.forEach(function (itemBtn: any, idx: any) {
                                    console.log(itemBtn);
                                    if (item.reportType == itemBtn.reportType) {
                                        item['submitBtm'] = itemBtn.submitBtm;
                                    }
                                    console.log(itemBtn);
                                });
                            });


                            let thatt = this;
                            this.reportArr.forEach(function (item: any, index: any) {
                                console.log(item);
                                thatt.reportTypes.forEach(function (itemBtn: any, idx: any) {
                                    console.log(itemBtn);
                                    if (item.reportType.toLowerCase().trim() == itemBtn.reportType.toLowerCase().trim()) {
                                        item.reportTypeId = itemBtn.reportTypeId;
                                        item['iddd'] = itemBtn.reportTypeId;
                                    }
                                });
                            });
                            var uniqueArray = this.removeDuplicates(this.reportArr, "reportTypeId");
                            console.log((uniqueArray));

                            this.reportArr = uniqueArray;
                            console.log("master lsit");
                            console.log(this.reportArr);
                            console.log("final array for mapping ");

                            console.log(this.reportArr);
                            console.log(this.reportArr);
                            this.fianlArrayReportTyes = this.reportArr;

                        } else {
                            this.errorMsg1 = false;
                        }
                        reject("reportNull");
                    });
                }
            }, reason => {
                console.log(reason);
            });
        });
    }

    getRequestReportForDateRange(idx: any) {
        alert(idx);
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {

                this.orderdate1 = "";
                this.deliverydate1 = "";
                this.dispatchDate1 = "";
                this.invoicedate1 = "";
                this.status = "";
                this.shipto = "";
                this.shipfrom = "";
                this.invoiceId = "";

                document.getElementById("OrderDatefiled").classList.remove('disable');
                document.getElementById("DesptchDatefiled").classList.remove('disable');
                document.getElementById("InvoiceDatefiled").classList.remove('disable');
                document.getElementById("DeliveryDatefiled").classList.remove('disable');
                this.toggleAdvSearchForDateRange();

            }
        }, reason => {
            console.log(reason);
        });
    }
    
    getToggleCall() {
              
        this._postsService.validateUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).subscribe((userData: any) => {            

            if (!userData.isSessionActive) {
               
                    // User session has expired.
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        console.log("Session terminated");
                        //reject('sessionInactive');
                        this.terminateFlg = true;
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else {
                        //reject('sessionInactive');
                        sessionStorage.clear();
                        this._router.navigate(['']);
                    }  
                                  
            }else{
                
                if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                    console.log("Session terminated");
                    //reject('sessionInactive');
                    this.terminateFlg = true;
                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                }

                let that = this;      
                console.log('Deal : ' + this.dealSearch1 + '  ' + that.dealSearch1);
                this.reportArr.forEach(function (item: any) {
                    if (item.reportTypeId == that.catalogueType) {
                        if (item.reportType.toLowerCase() == "all order lines daily report") {
                            that.advSearch = true;
                            that.advSearchforRange = false;
                            that.dealSearch1 = false;
                        } else if (item.reportType.toLowerCase() == "all order lines date range report") {
                            that.advSearch = false;
                            that.dealSearch1 = false;
                            that.advSearchforRange = true;
                        } else if(item.reportType.toLowerCase() == "promotion report") {
                            //debugger;
                            that.dealSearch1 = true;
                            that.advSearch = false;
                            that.advSearchforRange = false;
                        }else {
                            that.advSearch = false;
                            that.advSearchforRange = false;
                            that.dealSearch1 = false;
                        }
                    }
                });
                console.log('Deal : ' + this.dealSearch1 + '  ' + that.dealSearch1);
                if (this.advSearch == true) {
                    this.orderno = '';
                    this.productcode = '';
                    this.shipto = '';
                    this.shipfrom = '';
                    this.status = '';
                    this.channel = '';
                    this.invoiceId = '';
                    this.orderdate = void 0;
                    this.deliverydate = void 0;
                    this.invoicedate = void 0;
                    this.fieldSelected = new Array;
                    this.fieldSelectedCount = 0;

                    // enable primary field
                    this.shiptoshow = false;
                    this.statusshow = false;
                    this.channelsshow = false;
                    this.invoiceIdshow = false;
                    document.getElementById("deliverydateshow").classList.remove('disable');
                    // document.getElementById("deliverydateshowmob").classList.remove('disable');

                    // hide calender initially
                    document.getElementById("orderDatePicker").style.display = "none";
                    document.getElementById("deliveryDatePicker").style.display = "none";
                    document.getElementById("invoiceDatePicker").style.display = "none";

                    // disable secondary field
                    this.shipfromshow = true;
                    document.getElementById("orderdateshow").classList.remove('disable');
                    document.getElementById("invoicedateshow").classList.add('disable');
                    console.log("this.advSearch is true");
                } else if (this.advSearchforRange == true) {
                    console.log("this.advSearchforRange is true");
                } else if(this.dealSearch1 == true) {
                    this.clear(4);
                    this.clear(5);
                    document.getElementById("dealstartDatePicker").style.display = "none";
                    document.getElementById("dealendDatePicker").style.display = "none";
                   
                }else {
                    console.log("false");
                }
            }
        });

    }

    //getRequestReport(idx: any, index: number) {
    getRequestReport(idx: any) {
        this.getUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
            if (res == 'sessionActive') {
                this.toggleAdvSearch();
            }
        }, reason => {
            console.log(reason);
        });
    }

    getReportsDownload() {

        this._postsService.getReportsDownload(this.customerName, this.reportTypeID).subscribe(data => {
            this.reportDataDownload = data.reports;
            console.log("download data");
            console.log(this.reportDataDownload);
        });

    }

    viewReports(idx: any) {
        //debugger;
        
        if (this.reportTypeID == '') {
            
        } else {
            //debugger;
            this.errorMsg = false;

            this.reportData = [];
            //console.log(this.reportData);
            this.reportDataLength = 0;
            this.count = 0;
            this.start = 0;
            this.catalogueResult = false;
            
            this.getReports().then(res => {
                //console.log(JSON.stringify(res));
                //debugger;
                if (res == 'reportMaster') {
                    console.log(isArray(this.reportArr));
                    let that = this;
                    this.reportArr.forEach(function (item: any, index: any) {
                        console.log(item);
                        that.reportArrBtn.forEach(function (itemBtn: any, idx: any) {
                            console.log(itemBtn);
                            if (item.reportType == itemBtn.reportType) {
                                item['submitBtm'] = itemBtn.submitBtm;
                            }
                            console.log(itemBtn);
                        });
                    });


                    let thatt = this;
                    this.reportArr.forEach(function (item: any, index: any) {
                        console.log(item);
                        thatt.reportTypes.forEach(function (itemBtn: any, idx: any) {
                            console.log(itemBtn);
                            if (item.reportType.toLowerCase().trim() == itemBtn.reportType.toLowerCase().trim()) {
                                item.reportTypeId = itemBtn.reportTypeId;
                                item['iddd'] = itemBtn.reportTypeId;
                            }
                        });
                    });
                    console.log(this.reportArr);
                    var uniqueArray = this.removeDuplicates(this.reportArr, "reportTypeId");
                    console.log((uniqueArray));

                    this.order = "reportType";
                    this.ascending = true;

                    this.reportArr = uniqueArray;
                    console.log("master lsit");
                    console.log(this.reportArr);
                    console.log("final array for mapping ");

                    console.log(this.reportArr);
                    this.fianlArrayReportTyes = this.reportArr;

                    
                    if(this.paginationDisplay){
                        this.setPage(1);
                    }
                    

                    this.finalPageDisplay = this.paginationDisplay;
                }
            });
            this.errorMsg = false;
        }

    }


    fileReportDownload(idx: any) {
        //window.open(this.whoImgUrl + filepath);
        var reportId = this.reportData[idx].reportId;
        var reportFileName = this.reportData[idx].reportName;
        this._postsService.fileReportDownload(this.orgApiName, this.reportTypeID, reportId).subscribe(data => {
            if (data.reports[0].reportStatus == 'inprogress') {
                this.succMSg = "Report generation is in progress";
                let sessionModal = document.getElementById('addConfirmationModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');

                document.getElementById('addConfirmationModal').style.display = 'block';
            } else if (data.reports[0].reportStatus == 'error') {
                this.succMSg = "Report generation is Fail. Please try again!";
                let sessionModal = document.getElementById('addConfirmationModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('addConfirmationModal').style.display = 'block';
            } else {
                //window.open('https://mprod.xxwmm.wholesaleweb.catalogue.zzzzzzzzzz.s3-eu-west-1.amazonaws.com/reports/mccolls/OrderLine.csv?Content-Type=application%2Foctet-stream&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20171221T173603Z&X-Amz-SignedHeaders=host&X-Amz-Expires=604754&X-Amz-Credential=AKIAIZ6IKJV5RIPMA7XQ%2F20171221%2Feu-west-1%2Fs3%2Faws4_request&X-Amz-Signature=73faf55432a3c0807230704e4afdbc855362fea4018e0580dbe7a54f69bd19e8');
                this._postsService.fileReportDownloadUsingPresignedURL(this.orgApiName, reportFileName, 'report', 'download').subscribe(data => {
                    console.log(data.preSignedS3Url);

                    window.open(data.preSignedS3Url);
                }, error => {
                    console.log(error);
                    this.succMSg = reportFileName + " doesnot exist.";
                    let sessionModal = document.getElementById('addConfirmationModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                });
            }
        });
    }

    closeInProgressDownload() {
        // this.showLoader = false;
        let sessionModal = document.getElementById('downloadInProgress');
        sessionModal.classList.add('in');
        sessionModal.classList.remove('out');
        document.getElementById('downloadInProgress').style.display = 'none';
    }

    closeReportProgress() {
        // this.showLoader = false;
        let sessionModal = document.getElementById('reportInProgress');
        sessionModal.classList.add('in');
        sessionModal.classList.remove('out');
        document.getElementById('reportInProgress').style.display = 'none';
    }

    closeTimeElapse() {
        // this.showLoader = false;
        let sessionModal = document.getElementById('timeElapse');
        sessionModal.classList.add('in');
        sessionModal.classList.remove('out');
        document.getElementById('timeElapse').style.display = 'none';
    }

    onChange(some: any) {
        
        this.errorMsg = false;
        console.log(some);
        console.log(this.catalogueType);
        this.reportTypeID = some;
        console.log(this.reportTypeID);
        this.reportData = [];
        this.reportDataLength = 0;
        let that = this;
        console.log(this.fianlArrayReportTyes);
        
        var rptname : string = "";

        this.fianlArrayReportTyes.filter(function(data:any) {
            if(data.reportTypeId == some){
                rptname = data.reportType;                
            }
        });
        //debugger;
        console.log(rptname);
        this.paginationDisplay = false;
        this.finalPageDisplay = false;  
        this.paginationReport.forEach(element =>{
            console.log(rptname.indexOf(element));
            if(rptname.indexOf(element)>=0){
                this.paginationDisplay = true;
            }
        })

        if (some == '') {
            this.reporttypeBtn = false;
        } else {
            this.reportArr.forEach(function (item: any) {
                if (item.reportTypeId == some) {
                    console.log(item);
                    console.log(item.submitBtm);
                    that.reporttypeBtn = item.submitBtm ? item.submitBtm : false;
                }
            });
            this.advSearch = false;
            this.advSearchforRange = false;
            this.dealSearch1 = false;
            console.log(this.reporttypeBtn);
        }
    }

    public clear(calId: number): void {

        if (calId == 1) {
            this.orderdate = void 0;
        } else if (calId == 2) {
            this.deliverydate = void 0;
        } else if (calId == 3) {
            this.invoicedate = void 0;
        } else if(calId == 4){      
           
            var element = <HTMLInputElement>document.getElementById("dealdatefrom");
            element.value = "";
            
            var element2 = <HTMLInputElement>document.getElementById("dealdateto");
            element2.value = "";
            

        } else if(calId == 5){           
            var element1 = <HTMLInputElement>document.getElementById("dealdateto");
            element1.value = "";
        }
        this.toggaleCalender(calId);


    }
    resetField(myval: any, id: number){
        console.log(myval);
        
        //debugger;
        if(id == 4){
            myval.dealdatefrom ="";
            myval.dealdateto="";
            this.dealStratDate.setFullYear(1970,1,1);
            this.dealEndDate.setFullYear((this.todayDate.getFullYear()+5),1,1);

        }else if(id == 5){
            myval.dealdateto="";
            
        }

    }
    toggleAdvSearchForDateRange() {
        this.orderno = '';
        this.productcode = '';
        this.shipto = '';
        this.shipfrom = '';
        this.status = '';
        this.channel = '';
        this.invoiceId = '';
        this.orderdate = void 0;
        this.deliverydate = void 0;
        this.invoicedate = void 0;
        this.fieldSelected = new Array;
        this.fieldSelectedCount = 0;

        // enable primary field
        this.shiptoshow = false;
        this.statusshow = false;
        this.channelsshow = false;
        this.invoiceIdshow = false;
        document.getElementById("deliverydateshow").classList.remove('disable');

        // hide calender initially
        document.getElementById("orderDatePicker").style.display = "none";
        document.getElementById("deliveryDatePicker").style.display = "none";
        document.getElementById("invoiceDatePicker").style.display = "none";

        this.shipfromshow = true;
        document.getElementById("orderdateshow").classList.remove('disable');
        document.getElementById("invoicedateshow").classList.add('disable');
        let that = this;
        this.reportArr.forEach(function (item: any) {
            if (item.reportTypeId == that.reportTypeID) {
                console.log(item.submitBtm);
                that.reporttypeBtn = item.submitBtm ? item.submitBtm : false;
                if (item.reportTypeId == 32) {
                    that.advSearch = true;
                    that.advSearchforRange = false;
                    that.dealSearch1 = false;
                } else if (item.reportTypeId == 1) {
                    that.advSearch = false;
                    that.advSearchforRange = true;
                    that.dealSearch1 = false;
                } else if (item.reportTypeId >= 41 && item.reportTypeId<=45) {                    
                
                    that.advSearch = false;
                    that.advSearchforRange = false;
                    that.dealSearch1 = true;
                }else {
                    that.advSearch = false;
                    that.advSearchforRange = false;
                    that.dealSearch1 = false;
                }
            }
        });
        console.log("this.advSearch");
        console.log(this.advSearch);
        console.log("this.advSearchforRange");
        console.log(this.advSearchforRange);
        console.log("this.dealSearch1");
        console.log(this.dealSearch1);


        if (this.advSearch) {

            this.orderno = '';
            this.productcode = '';
            this.shipto = '';
            this.shipfrom = '';
            this.status = '';
            this.channel = '';
            this.invoiceId = '';
            this.orderdate = void 0;
            this.deliverydate = void 0;
            this.invoicedate = void 0;
            this.fieldSelected = new Array;
            this.fieldSelectedCount = 0;

            // enable primary field
            this.shiptoshow = false;
            this.statusshow = false;
            this.channelsshow = false;
            this.invoiceIdshow = false;
            document.getElementById("deliverydateshow").classList.remove('disable');
            // document.getElementById("deliverydateshowmob").classList.remove('disable');

            // hide calender initially
            document.getElementById("orderDatePicker").style.display = "none";
            document.getElementById("deliveryDatePicker").style.display = "none";
            document.getElementById("invoiceDatePicker").style.display = "none";

            // disable secondary field
            this.shipfromshow = true;
            document.getElementById("orderdateshow").classList.remove('disable');
            document.getElementById("invoicedateshow").classList.add('disable');
        }
        else {




        }
    }

    toggleAdvSearch() {
        this.orderno = '';
        this.productcode = '';
        this.shipto = '';
        this.shipfrom = '';
        this.status = '';
        this.channel = '';
        this.invoiceId = '';
        this.orderdate = void 0;
        this.deliverydate = void 0;
        this.invoicedate = void 0;
        this.fieldSelected = new Array;
        this.fieldSelectedCount = 0;

        // enable primary field
        this.shiptoshow = false;
        this.statusshow = false;
        this.channelsshow = false;
        this.invoiceIdshow = false;
        document.getElementById("deliverydateshow").classList.remove('disable');

        // hide calender initially
        document.getElementById("orderDatePicker").style.display = "none";
        document.getElementById("deliveryDatePicker").style.display = "none";
        document.getElementById("invoiceDatePicker").style.display = "none";

        this.shipfromshow = true;
        document.getElementById("orderdateshow").classList.remove('disable');
        document.getElementById("invoicedateshow").classList.add('disable');


        if (this.advSearch) {

            this.orderno = '';
            this.productcode = '';
            this.shipto = '';
            this.shipfrom = '';
            this.status = '';
            this.channel = '';
            this.invoiceId = '';
            this.orderdate = void 0;
            this.deliverydate = void 0;
            this.invoicedate = void 0;
            this.fieldSelected = new Array;
            this.fieldSelectedCount = 0;

            // enable primary field
            this.shiptoshow = false;
            this.statusshow = false;
            this.channelsshow = false;
            this.invoiceIdshow = false;
            document.getElementById("deliverydateshow").classList.remove('disable');
            // document.getElementById("deliverydateshowmob").classList.remove('disable');

            // hide calender initially
            document.getElementById("orderDatePicker").style.display = "none";
            document.getElementById("deliveryDatePicker").style.display = "none";
            document.getElementById("invoiceDatePicker").style.display = "none";

            //  document.getElementById("orderDatePickerMob").style.display = "none";
            //  document.getElementById("deliveryDatePickerMob").style.display = "none";
            //  document.getElementById("invoiceDatePickerMob").style.display = "none";
            // disable secondary field
            this.shipfromshow = true;
            document.getElementById("orderdateshow").classList.remove('disable');
            document.getElementById("invoicedateshow").classList.add('disable');

            //  document.getElementById("orderdateshowmob").classList.add('disable');
            //   document.getElementById("invoicedateshowmob").classList.add('disable');

            // enable primary field

            //this.advSearch = false;
            /*  let advSearchIcon = document.getElementById('advSearchIcon');
              advSearchIcon.classList.remove('glyphicon-menu-down');
              advSearchIcon.classList.add('glyphicon-menu-up');
  
              let getOrderDiv = document.getElementById('orderIdSearch');
              getOrderDiv.classList.add('disable');
  
              let getProductDiv = document.getElementById('productcodeSearch');
              getProductDiv.classList.add('disable'); */


        }
        else {
            /* if (this.requestReportData[0].reportColumn[0].channels == 'Y') {
                this.channelHide = true;
            } else {
                this.channelHide = false;
            }

            if (this.requestReportData[0].reportColumn[0].deliveryDate == 'Y') {
                this.deliverydateHide = true;
            } else {
                this.deliverydateHide = false;
            }

            if (this.requestReportData[0].reportColumn[0].invoiceDate == 'Y') {
                this.invoicedateHide = true;
            } else {
                this.invoicedateHide = false;
            }

            if (this.requestReportData[0].reportColumn[0].invoiceId == 'Y') {
                this.invoiceIdHide = true;
            } else {
                this.invoiceIdHide = false;
            }

            if (this.requestReportData[0].reportColumn[0].orderDate == 'Y') {
                this.orderdateHide = true;
            } else {
                this.orderdateHide = false;
            }

            if (this.requestReportData[0].reportColumn[0].shippedFrom == 'Y') {
                this.shipfromHide = true;
            } else {
                this.shipfromHide = false;
            }

            if (this.requestReportData[0].reportColumn[0].shippedTo == 'Y') {
                this.shipToHide = true;
            } else {
                this.shipToHide = false;
            }

            if (this.requestReportData[0].reportColumn[0].status == 'Y') {
                this.statusHide = true;
            } else {
                this.statusHide = false;
            } */
            //this.advSearch = true;
            /*    let advSearchIcon = document.getElementById('advSearchIcon');
                advSearchIcon.classList.remove('glyphicon-menu-up');
                advSearchIcon.classList.add('glyphicon-menu-down');
    
                let getOrderDiv = document.getElementById('orderIdSearch');
                getOrderDiv.classList.remove('disable');
    
                let getProductDiv = document.getElementById('productcodeSearch');
                getProductDiv.classList.remove('disable'); */



        }

    }

    toggaleCalender(calId: number) {



        if (calId == 1) {

            if (document.getElementById("orderDatePicker").style.display == "none") {
                document.getElementById("orderDatePicker").style.display = "block";
                document.getElementById("deliveryDatePicker").style.display = "none";
                document.getElementById("invoiceDatePicker").style.display = "none";
            } else {
                document.getElementById("orderDatePicker").style.display = "none";
                document.getElementById("deliveryDatePicker").style.display = "none";
                document.getElementById("invoiceDatePicker").style.display = "none";
            }

            /* if (document.getElementById("orderDatePickerMob").style.display == "none") {
                 document.getElementById("orderDatePickerMob").style.display = "block";
                 document.getElementById("deliveryDatePickerMob").style.display = "none";
                 document.getElementById("invoiceDatePickerMob").style.display = "none";
             } else {
                 document.getElementById("orderDatePickerMob").style.display = "none";
                 document.getElementById("deliveryDatePickerMob").style.display = "none";
                 document.getElementById("invoiceDatePickerMob").style.display = "none";
             } */




        } else if (calId == 2) {
            if (document.getElementById("deliveryDatePicker").style.display == "none") {
                document.getElementById("deliveryDatePicker").style.display = "block";
                document.getElementById("orderDatePicker").style.display = "none";
                document.getElementById("invoiceDatePicker").style.display = "none";
            } else {
                console.log("In");
                document.getElementById("orderDatePicker").style.display = "none";
                document.getElementById("deliveryDatePicker").style.display = "none";
                document.getElementById("invoiceDatePicker").style.display = "none";
            }

            /*   if (document.getElementById("deliveryDatePickerMob").style.display == "none") {
                   document.getElementById("deliveryDatePickerMob").style.display = "block";
                   document.getElementById("orderDatePickerMob").style.display = "none";
                   document.getElementById("invoiceDatePickerMob").style.display = "none";
               } else {
                   document.getElementById("orderDatePickerMob").style.display = "none";
                   document.getElementById("deliveryDatePickerMob").style.display = "none";
                   document.getElementById("invoiceDatePickerMob").style.display = "none";
               } */



        } else if (calId == 3) {
            if (document.getElementById("invoiceDatePicker").style.display == "none") {
                document.getElementById("invoiceDatePicker").style.display = "block";
                document.getElementById("orderDatePicker").style.display = "none";
                document.getElementById("deliveryDatePicker").style.display = "none";
            } else {
                document.getElementById("orderDatePicker").style.display = "none";
                document.getElementById("deliveryDatePicker").style.display = "none";
                document.getElementById("invoiceDatePicker").style.display = "none";
            }

            /*   if (document.getElementById("invoiceDatePickerMob").style.display == "none") {
                   document.getElementById("invoiceDatePickerMob").style.display = "block";
                   document.getElementById("orderDatePickerMob").style.display = "none";
                   document.getElementById("deliveryDatePickerMob").style.display = "none";
               } else {
                   document.getElementById("orderDatePickerMob").style.display = "none";
                   document.getElementById("deliveryDatePickerMob").style.display = "none";
                   document.getElementById("invoiceDatePickerMob").style.display = "none";
               } */



        }
        else if (calId == 4) {
            if (document.getElementById("dealstartDatePicker").style.display == "none") {
                document.getElementById("dealstartDatePicker").style.display = "block"; 
                document.getElementById("dealendDatePicker").style.display = "none";   
                console.log(this.dealstartdate);
                var element = <HTMLInputElement>document.getElementById("dealdatefrom");
               
                //debugger;       
                if(element.value == ""){
                    this.dealstartdate =this.todayDate;
                }
            } else {
                
                document.getElementById("dealstartDatePicker").style.display = "none";
               
            }         

        }
        else if (calId == 5) {
            if (document.getElementById("dealendDatePicker").style.display == "none") {
                document.getElementById("dealendDatePicker").style.display = "block";
                document.getElementById("dealstartDatePicker").style.display = "none";
                console.log(this.dealenddate);
                var element2 = <HTMLInputElement>document.getElementById("dealdateto");
               
                var element3 = <HTMLInputElement>document.getElementById("dealdatefrom");
                //debugger;   
                if(element2.value == "" && element3.value == ""){
                    this.dealenddate= this.todayDate;
                }
                
            } else {
                
                document.getElementById("dealendDatePicker").style.display = "none";
            }         

        }


    }

    // key press logic for filter
    onKeypress(myval: any, filed: any) {
        //debugger;
        //document.getElementById('adv-search-desk').focus();
        console.log(myval);
        console.log(JSON.stringify(myval));
        console.log(filed);
        switch (filed) {
            case 1:
                this.switchAction(myval, 1);
                break;
            case 2:
                this.switchAction(myval, 2);
                break;
            case 3:
                this.switchAction(myval, 3);
                break;
            case 4:
                this.switchAction(myval, 4);
                break;
            case 5:
                this.switchAction(myval, 5);
                break;
            case 6:
                this.switchAction(myval, 6);
                break;
            case 7:
                this.switchAction(myval, 7);
                break;
            case 8:
                this.switchAction(myval, 8);
                break;
            
            case 9: 
                var selDate =JSON.stringify(myval).replace('/"/g','');
                console.log(selDate);
                var firstArr = selDate.split('T');
                var secArr = firstArr[0].split('-');

                console.log(JSON.stringify(firstArr));
                console.log(JSON.stringify(secArr));
                console.log(secArr[0]);
                console.log(secArr[1]);
                console.log(secArr[2]);
                var year: number = Number(secArr[0].replace('"',""));
                var month: number = Number(secArr[1]);
                var day : number = Number(secArr[2]);
                console.log(year);
                console.log(month);
                console.log(day);
                if(this.todayDate.getDate() == (day+1)){
                  
                    //var actualDate = new Date(year, (month-1), (day+1));  
                    var tmpele = <HTMLInputElement>document.getElementById("dealdatefrom");
                   
                    tmpele.value= (day+1) + '/' + month + '/' + year;
                }
               
                var nDate = new Date(year, (month-1), (day+2));  //(day+1)

                console.log('new date '+ nDate);

                this.dealStratDate = nDate;
                console.log(nDate.getDate());

                var neDate = new Date(nDate.getFullYear(), (nDate.getMonth()), (nDate.getDate()+ 42));
                console.log('end Date ' + neDate);
                this.dealEndDate = neDate;
                this.dealenddate = neDate;
                break;
            case 10: 
                var selDate1 =JSON.stringify(myval).replace('/"/g','');
                console.log(selDate1);
                var firstArr1 = selDate1.split('T');
                var secArr1 = firstArr1[0].split('-');

                console.log(JSON.stringify(firstArr1));
                console.log(JSON.stringify(secArr1));
                console.log(secArr1[0]);
                console.log(secArr1[1]);
                console.log(secArr1[2]);
                var year1: number = Number(secArr1[0].replace('"',""));
                var month1: number = Number(secArr1[1]);
                var day1 : number = Number(secArr1[2]);
                console.log(year1);
                console.log(month1);
                console.log(day1);
                if(this.todayDate.getDate() == (day1+1)){
                
                    //var actualDate = new Date(year, (month-1), (day+1));  
                    var tmpele1 = <HTMLInputElement>document.getElementById("dealdateto");
                
                    tmpele1.value= (day1+1) + '/' + month1 + '/' + year1;
                }    
                
                break;    
               // debugger;
        }
        if (this.fieldSelectedCount == 0) {

            this.shipfrom = '';
            this.orderdate = void 0;
            this.invoicedate = void 0;
            this.shipfromshow = true;
            console.log("TEsting");
            document.getElementById("orderdateshow").classList.remove('disable');
            document.getElementById("invoicedateshow").classList.add('disable');
            //document.getElementById("channel").classList.add('disable');

            //  document.getElementById("orderdateshowmob").classList.add('disable');
            //  document.getElementById("invoicedateshowmob").classList.add('disable');

        } else if (this.fieldSelectedCount == 1) {


            // primary fields
            this.channelsshow = false;
            this.statusshow = false;
            this.shiptoshow = false;
            this.invoiceIdshow = false;
            //document.getElementById("channel").classList.remove('disable');
            document.getElementById("deliverydateshow").classList.remove('disable');
            //   document.getElementById("deliverydateshowmob").classList.remove('disable');

            // secondary fields
            this.shipfromshow = false;
            document.getElementById("orderdateshow").classList.remove('disable');
            document.getElementById("invoicedateshow").classList.remove('disable');
            //  document.getElementById("orderdateshowmob").classList.remove('disable');
            //  document.getElementById("invoicedateshowmob").classList.remove('disable');
        } else if (this.fieldSelectedCount == 2) {

            this.channelsshow = true;
            this.statusshow = true;
            this.shiptoshow = true;

            this.invoiceIdshow = true;

            document.getElementById("deliverydateshow").classList.add('disable');
            document.getElementById("orderdateshow").classList.add('disable');
            document.getElementById("invoicedateshow").classList.add('disable');

            /*    document.getElementById("deliverydateshowmob").classList.add('disable');
                document.getElementById("orderdateshowmob").classList.add('disable');
                document.getElementById("invoicedateshowmob").classList.add('disable'); */
            this.shipfromshow = true;


            for (var i = 0; i < this.fieldSelected.length; i++) {
                for (var j = 1; j <= 8; j++) {
                    if (this.fieldSelected[i] == j) {
                        console.log("j" + j);
                        switch (j) {
                            case 1:
                                this.channelsshow = false;
                                break;
                            case 2:
                                this.statusshow = false;
                                break;
                            case 3:
                                this.shiptoshow = false;
                                break;
                            case 4:
                                document.getElementById("deliverydateshow").classList.remove('disable');
                                //   document.getElementById("deliverydateshowmob").classList.remove('disable');
                                break;
                            case 5:
                                this.invoiceIdshow = false;
                                break;
                            case 6:
                                document.getElementById("orderdateshow").classList.remove('disable');
                                //   document.getElementById("orderdateshowmob").classList.remove('disable');
                                break;
                            case 7:
                                document.getElementById("invoicedateshow").classList.remove('disable');
                                //   document.getElementById("invoicedateshowmob").classList.remove('disable');
                                break;
                            case 8:
                                this.shipfromshow = false;
                                break;
                        }
                    }
                }
            }
        }

        
    }

    switchAction(myval: any, id: number) {
        console.log("Selected Fields");
        console.log(this.fieldSelected);
        if (myval != '') {
            if (this.fieldSelected.indexOf(id) == -1) {
                this.fieldSelected.push(id);
                this.fieldSelectedCount = this.fieldSelectedCount + 1;
            }

        } else {

            if (this.fieldSelected.indexOf(id) != -1) {
                this.fieldSelectedCount = this.fieldSelectedCount - 1;
                this.fieldSelected.splice((this.fieldSelected.indexOf(id)), 1);
            }

        }

    }

    showPopup() {
        this.showDatepicker = true;
        
    }


    keyPress(event: any) {
        this._globalFunc.alphaNumeric(event);
    }

    printOrder() {
        //this.printHeader = false;
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        var tableData = document.getElementById('hiddenDivForExport').innerHTML;
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + tableData + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + tableData + '</body></html>');
            popup.document.close();
        }
    }



    // use this function to generate catalgoue file api event
    generateCatalogue(catalogueType: any) {
        console.log(catalogueType);
        if (catalogueType == '' || catalogueType == null) {
            this.showErrorMsg = true;
            this.errorMsgString = 'Please Select report Type';
            setTimeout(() => {
                this.showErrorMsg = false;
            }, 5000);
            return false;
        } else {
            //this.showLoader = true;
            console.log(catalogueType);
            this.succMSg = 'Your request for generation of new report has been accepted. File will be generated as Report_26102017';
            let sessionModal = document.getElementById('addConfirmationModal');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('addConfirmationModal').style.display = 'block';
            /*   this._postsService.generateCatalogue(this.orgApiName, catalogueType).subscribe(data => {
   
                   this.catalogueFilename = data.reportName;
                   // data = '{"isFileExist": true,"isFileInProcess": true,"reportName": "string"}';
   
                   // this.catalogueFilename = this.orgApiName + "-" + catalogueType + "-20171024121212.csv";
                   if (data.isFileExist == true || data.isFileExist == 'true') {
                       this.succMSg = 'Generation of Export Catalogue is already in Progress or just completed. See file ' + this.catalogueFilename;
                   } else if (data.isFileInProcess == true) {
                       this.succMSg = 'Your request for initiating Catalogue Export has been accepted. File will be generated as ' + this.catalogueFilename;
                   }
                   //alert(this.orgApiName + " " + catalogueType);
                   let sessionModal = document.getElementById('addConfirmationModal');
                   sessionModal.classList.remove('in');
                   sessionModal.classList.add('out');
                   document.getElementById('addConfirmationModal').style.display = 'block';
                   this.showLoader = false;
               }
                   , err => {
                       this.cataloguemasterListLength = 0;
                       this.showLoader = false;
                       this._router.navigate(['/error404']);
                   }); */
        }
        //some coding
    }


    // get relevant order detail
    getAllMasterData() {

        this.showLoader = true;
        this._postsService.getAllMasterData(this.orgApiName).subscribe(data => {
            this.showLoader = false;

            if (data.catalogueTypes.length == 0) {
                this.cataloguemasterListLength = 0;
            } else {
                for (var k = 0; k < data.catalogueTypes.length; k++) {
                    if (data.catalogueTypes[k].catalogueType.indexOf(':') > 0) {
                        data.catalogueTypes[k].catalogueType = data.catalogueTypes[k].catalogueType.split(':')[1];
                    }
                }
                this.cataloguemasterListLength = data.catalogueTypes.length;


            }
            this.catalogueTypes = data.catalogueTypes;

        }
            , err => {
                this.cataloguemasterListLength = 0;
                this.showLoader = false;
                this._router.navigate(['/error404']);
            }
        );
    }

    // to show specific content in table 
    getMinValue(mapValues: any) {

        var dataAvail: boolean = false;
        for (var i = 0; i < mapValues.length; i++) {
            if ((mapValues[i].type).toUpperCase() == 'MIN') {
                dataAvail = true;
                return mapValues[i].values[0].value;
            }
        }

        if (!dataAvail)
            return this.strInfoNotAvail;
    }

    // to show specific CLIENTID value if avaliable
    getAsinValue(mapValues: any) {

        var dataAvail: boolean = false;
        for (var i = 0; i < mapValues.length; i++) {
            if ((mapValues[i].type).toUpperCase() == 'CLIENTID') {
                dataAvail = true;
                return mapValues[i].values[0].value;
            }
        }

        if (!dataAvail)
            return '-';
    }

    // to show specific content in table 
    getWholesalePriceValue(pricesValues: any) {
        var dataAvail: boolean = false;

        for (var i = 0; i < pricesValues.length; i++) {
            if ((pricesValues[i].type).toUpperCase() == 'WSP') {
                /* console.log(this.formatDate(pricesValues[i].values[0].start));
                console.log(this.formatDate(this.todayDate.toDateString()));

                 console.log(this.formatDate(pricesValues[i].values[0].end));
                console.log(this.formatDate(this.todayDate.toDateString())); */
                if ((pricesValues[i].values[0].currency).toUpperCase() == 'GBP' && (this.formatDate(pricesValues[i].values[0].start) <= this.formatDate(this.todayDate.toDateString())) && (this.formatDate(pricesValues[i].values[0].end) >= this.formatDate(this.todayDate.toDateString()))) {
                    {
                        dataAvail = true;
                        return pricesValues[i].values[0].value;
                    }
                }
            }
        }
        if (!dataAvail)
            return this.strInfoNotAvail;
    }
    // to show specific content in table 
    getWholesalePackSizeValue(pricesValues: any) {
        var dataAvail: boolean = false;

        for (var i = 0; i < pricesValues.length; i++) {
            if ((pricesValues[i].type).toUpperCase() == 'CS') {
                if (this.formatDate(pricesValues[i].values[0].start) <= this.formatDate(this.todayDate.toDateString()) && this.formatDate(pricesValues[i].values[0].end) >= this.formatDate(this.todayDate.toDateString())) {
                    {
                        dataAvail = true;
                        return pricesValues[i].values[0].value;
                    }
                }
            }
        }

        if (!dataAvail)
            return this.strInfoNotAvail;
    }
    // to show specific content in table 
    getWholesalePackTypeValue(pricesValues: any) {
        var dataAvail: boolean = false;

        for (var i = 0; i < pricesValues.length; i++) {
            if (pricesValues[i].type.toUpperCase() == 'CS' || pricesValues[i].type.toUpperCase() == 'EA') {

                dataAvail = true;
                // return pricesValues[i].values[0].value;
                return pricesValues[i].type.toUpperCase();
            }
        }

        if (!dataAvail)
            return this.strInfoNotAvail;
    }

    // format date function
    formatDate(createdAtDate: string) {
        if (createdAtDate != undefined && createdAtDate != null && createdAtDate.length > 0) {
            if (createdAtDate.indexOf('/') >= 0) {
                return createdAtDate;
            } else {
                var d = new Date(createdAtDate);
                var yyyy = d.getFullYear();
                var mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1); // getMonth() is zero-based
                var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
                // var hh = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
                // var min = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
                return yyyy + "/" + mm + "/" + dd;
            }
        } else {
            return '-';
        }
    }


    // open in new tab for detail
    redirect_detail(page: any, orgType: any, catalogueId: any) {

        let url = this.homeUrl + page + '/' + encodeURIComponent(orgType) + '/' + this.catalogueCodeSelected + '/' + catalogueId;
        window.open(url);
    }

    getProductDetailByID(id: string) {
        var obj = this.catalogueDetailList.filter(function (node: any) {
            return node.id == id;
        });
        console.log(obj[0]);
        return obj[0];
    }

    redirect_home(page: any) {
        this._router.navigate([page]);
    }

    closePopupp() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }


    closePopup() {
        let sessionModal = document.getElementById('addConfirmationModal');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('addConfirmationModal').style.display = 'none';
    }

    getUnifiedSearchOrders() {
        console.log("getUnifiedSearchOrders")
        if (this.AllLinesPostJSON["shipToDeliverAt"] || this.AllLinesPostJSON["dispatchDate"] || this.AllLinesPostJSON["billToAt"] || this.AllLinesPostJSON["orderRaisedDate"]) {
            this.showLoader = true;
            this.AllLinesPostJSON["customer"] = this.orgApiName;
            this.AllLinesPostJSON["reportTypeId"] = this.reportTypeID;
            this.AllLinesPostJSON['createdBy'] = this.loggedUser;
            console.log(JSON.stringify(this.AllLinesPostJSON));
            this._postsService.getAllOrderLinesReport(this.orgApiName, this.reportTypeID, this.AllLinesPostJSON).subscribe((data: any) => {
                console.log(data);
                this.catalogueFilename = data.reportName ? data.reportName : "filename 2 provide from service";
                this.succMSg = 'Your request for initiating Report has been accepted. File will be generated as ' + this.catalogueFilename;
                let sessionModal = document.getElementById('addConfirmationModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('addConfirmationModal').style.display = 'block';
                this.showLoader = false;
            }, (error: any) => {
                console.log(error.errorCode);
                if (error.errorCode == '404.33.517') {
                    this.errorMessage = error.errorMessage;

                    this.showLoader = false;
                    /*let sessionModal = document.getElementById('reportInProgress');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('reportInProgress').style.display = 'block';*/

                    this.succMSg = 'Report generation is already in progress.';//. See file ' + err.catalogueFilename;
                    let sessionModal = document.getElementById('addConfirmationModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('addConfirmationModal').style.display = 'block';
                } else if (error.httpResponseCode == '404') {
                    this.errorMessage = error.errorMessage;
                    this.showLoader = false;
                    /*let sessionModal = document.getElementById('reportInProgress');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('timeElapse').style.display = 'block';*/

                    this.succMSg = error.errorMessage;// See file ' + this.catalogueFilename;
                    let sessionModal = document.getElementById('addConfirmationModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('addConfirmationModal').style.display = 'block';
                    this.showLoader = false;
                } else if (error.errorCode == '500.65.001') {
                    this.errorMessage = "There was an error processing your request"
                    this.succMSg = error.errorMessage;// See file ' + this.catalogueFilename;
                    let sessionModal = document.getElementById('addConfirmationModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('addConfirmationModal').style.display = 'block';
                    this.showLoader = false;
                } else {
                    this.errorMessage = error.errorMessage;
                    this.showLoader = false;
                }
            });
        } else {
            // Show error message
            this.showErrorMsgNew = true;
        }
    }


    // get relevant order detail
    getReleventOrders(value: any) {
        console.log(value);
        //  value.status = this.status;
        //  value.channel = this.channel;

        //debugger;
        let indexkey1 = '', indexvalue1 = '', indexkey2 = '', indexvalue2 = '', filterkey, filtervalue;
        // logic to select 1st index key and value
        var changefrom: string ='';
        var changeto: string = '';
        
        if(value.dealdatefrom != undefined  && value.dealdatefrom !=''){
            if(value.dealdateto != undefined  && value.dealdateto !=''){
                var ele = <HTMLInputElement>document.getElementById("dealdatefrom");
                var ele1 = <HTMLInputElement>document.getElementById("dealdateto");
                if(ele.value != "" && ele1.value != ""){
                    if (indexkey1 == undefined || indexkey1 == '') {
                        indexkey1 = 'promotion';
                        indexvalue1 = "immediate";
                    } else {
                        indexkey2 = 'promotion';
                        indexvalue2 = "immediate";
                    }

                    var tmpfrom = value.dealdatefrom.split("/");
                    var tmpto = value.dealdateto.split("/");

                    console.log(JSON.stringify(tmpfrom));
                    console.log(JSON.stringify(tmpto));

                    changefrom = tmpfrom[2] + '-' + tmpfrom[1] + '-' + tmpfrom[0];
                    changeto = tmpto[2] + '-' + tmpto[1] + '-' + tmpto[0];

                    console.log(changefrom);
                    console.log(changeto);
                    //debugger;
                }
            }
        }

        if (value.status != undefined && value.status != '') {
            if (indexkey1 == undefined || indexkey1 == '') {
                indexkey1 = 'statusCurrent';
                indexvalue1 = value.status;
            } else {
                indexkey2 = 'statusCurrent';
                indexvalue2 = value.status;
            }
        }

        if (value.shipto != undefined && value.shipto != '') {
            if (indexkey1 == undefined || indexkey1 == '') {
                indexkey1 = 'shipToLocationId';
                indexvalue1 = value.shipto;
            } else {
                indexkey2 = 'shipToLocationId';
                indexvalue2 = value.shipto;
            }
        }

        if (value.deliverydatefrom != undefined && value.deliverydatefrom != '') {

            let deliverydatefrom = value.deliverydatefrom.split('/');
            let shipToDeliverAt = deliverydatefrom[2] + '-' + deliverydatefrom[1] + '-' + deliverydatefrom[0];

            if (indexkey1 == undefined || indexkey1 == '') {
                indexkey1 = 'shipToDeliverAt';
                indexvalue1 = shipToDeliverAt;

            } else {
                indexkey2 = 'shipToDeliverAt';
                indexvalue2 = shipToDeliverAt;

            }
        }

        if (value.invoiceId != undefined && value.invoiceId != '') {
            if (indexkey1 == undefined || indexkey1 == '') {
                indexkey1 = 'billToId';
                indexvalue1 = value.invoiceId;
            } else {
                indexkey2 = 'billToId';
                indexvalue2 = value.invoiceId;
            }
        }

        if (value.orderdatefrom != undefined && value.orderdatefrom != '') {
            let orderdatefrom = value.orderdatefrom.split('/');
            let createdAt = orderdatefrom[2] + '-' + orderdatefrom[1] + '-' + orderdatefrom[0];
            if (indexkey1 == undefined || indexkey1 == '') {
                indexkey1 = 'orderRaisedDate';
                indexvalue1 = createdAt;
            } else {
                indexkey2 = 'orderRaisedDate';
                indexvalue2 = createdAt;
            }
        }

        // selection of filter key
        if (value.invoicedatefrom != undefined && value.invoicedatefrom != '') {
            let invoicedatefrom = value.invoicedatefrom.split('/');
            let billToAt = invoicedatefrom[2] + '-' + invoicedatefrom[1] + '-' + invoicedatefrom[0];

            filterkey = 'billToDate';
            filtervalue = billToAt;
        } else if (value.shipfrom != undefined && value.shipfrom != '') {
            filterkey = 'shipFromLocationId';
            filtervalue = value.shipfrom;
        } else if (value.channel != undefined && value.channel != '') {
            filterkey = 'messageType';
            filtervalue = value.channel;
        } else {
            filterkey = '';
            filtervalue = '';
        }

        if (indexkey1 == '') {  
            this.errorMsg = true;
            setTimeout(() => {
                this.errorMsg = false;
            }, 5000);

        } else {

            // document.getElementById('advSearchClose').click();
            this.showLoader = true;
            this.sortByColumn = '';
            this.reverseSorting = false;
            // alert(this.orgApiName);
            var finalArr: any = Array();

            //if(this.reportTypeID >= 41 && this.reportTypeID <= 45) {   
            if((value.dealdatefrom != undefined  && value.dealdatefrom !='') && (value.dealdateto != undefined  && value.dealdateto !='')){         
                //debugger;                
                finalArr.push({ 'orderRaisedDate': {'from' : changefrom, 'to' : changeto}, 'indexKey': indexkey1, 'indexValue': indexvalue1, 'customer': this.orgApiName, 'reportTypeId': this.reportTypeID, 'createdBy': this.loggedUser });
                console.log(JSON.stringify(finalArr));
            } else {
                if (filterkey == '' && indexkey2 == '') {
                    finalArr.push({'indexKey': indexkey1, 'indexValue': indexvalue1, 'customer': this.orgApiName, 'reportTypeId': this.reportTypeID, 'createdBy': this.loggedUser });
                } else if (filterkey == '' && indexkey2 != '') {
                    finalArr.push({ 'indexKey': indexkey1, 'indexValue': indexvalue1, 'filterKey': indexkey2, 'filterValue': indexvalue2, 'customer': this.orgApiName, 'reportTypeId': this.reportTypeID, 'createdBy': this.loggedUser });
                } else {
                    finalArr.push({ 'indexKey': indexkey1, 'indexValue': indexvalue1, 'filterKey': filterkey, 'filterValue': filtervalue, 'customer': this.orgApiName, 'reportTypeId': this.reportTypeID, 'createdBy': this.loggedUser });
                }
            }

            var str1 = JSON.stringify(finalArr).replace("[", "").replace("]", "");

            console.log(str1);
            // service cal to get relevent order data
            this._postsService.submitReportRequest(this.orgApiName, this.reportTypeID, str1).subscribe(data => {
                console.log(data);
                this.catalogueFilename = data.reportName ? data.reportName : "filename 2 provide from service";
                // data = '{"isFileExist": true,"isFileInProcess": true,"reportName": "string"}';

                // this.catalogueFilename = this.orgApiName + "-" + catalogueType + "-20171024121212.csv";
                /*if (data.isFileExist == true || data.isFileExist == 'true') {
                    this.succMSg = 'Generation of Export Catalogue is already in Progress or just completed. See file ' + this.catalogueFilename;
                } else if (data.isFileInProcess == true) {*/
                this.succMSg = 'Your request for initiating Report has been accepted. File will be generated as ' + this.catalogueFilename;
                // }
                //alert(this.orgApiName + " " + catalogueType);
                let sessionModal = document.getElementById('addConfirmationModal');
                sessionModal.classList.remove('in');
                sessionModal.classList.add('out');
                document.getElementById('addConfirmationModal').style.display = 'block';
                this.showLoader = false;
            }, err => {
                if (err.errorCode === '404.33.517') {
                    this.errorMessage = err.errorMessage;
                    this.showLoader = false;
                    /*let sessionModal = document.getElementById('reportInProgress');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('reportInProgress').style.display = 'block';*/

                    this.succMSg = 'Generation of Report is already in Progress.';//. See file ' + err.catalogueFilename;
                    let sessionModal = document.getElementById('addConfirmationModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('addConfirmationModal').style.display = 'block';
                } else if (err.httpResponseCode == 404) {
                    this.errorMessage = err.errorMessage;
                    this.showLoader = false;
                    /*let sessionModal = document.getElementById('reportInProgress');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('timeElapse').style.display = 'block';*/

                    this.succMSg = err.errorMessage;// See file ' + this.catalogueFilename;
                    let sessionModal = document.getElementById('addConfirmationModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('addConfirmationModal').style.display = 'block';
                    this.showLoader = false;
                } else if (err.errorCode == '500.65.001') {
                    this.errorMessage = "There was an error processing your request"
                    this.succMSg = err.errorMessage;// See file ' + this.catalogueFilename;
                    let sessionModal = document.getElementById('addConfirmationModal');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('addConfirmationModal').style.display = 'block';
                    this.showLoader = false;
                } else {
                    this.errorMessage = err.errorMessage;
                    this.showLoader = false;
                }

            }
            );

        }

    }
    checkDateDiff(dateFrom: string, dateTo: string) {
        var date1 = new Date(dateFrom);
        var date2 = new Date(dateTo);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        let datediff = Math.ceil(timeDiff / (1000 * 3600 * 24));
        console.log(datediff);
        return datediff;
    }
    formatedDate(Dt: any, formatStr: string = "dmy") {
        let today = new Date(Dt);
        let dd: any = today.getDate();
        let mm: any = today.getMonth() + 1;
        let year: any = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        if (formatStr == "dmy") {
            return dd + "-" + mm + "-" + year;
        } else {
            return year + "-" + mm + "-" + dd;
        }

    }
    selectedValues(value: any, datepicker?: any) {
        // this is the date the iser selected
        switch (datepicker) {
            case "deliverydate":
                if (this.checkDateDiff(value.start, value.end) <= this.rangeDateVal) {
                    this.AllLinesPostJSON["shipToDeliverAt"] = {
                        "from": this.formatedDate(value.start, "ymd"),
                        "to": this.formatedDate(value.end, "ymd")
                    }
                    this.deliverydate1 = this.formatedDate(value.start) + " - " + this.formatedDate(value.end);
                    this.showErrorMsgNew = false;
                    document.getElementById("OrderDatefiled").classList.add('disable');
                    document.getElementById("DesptchDatefiled").classList.add('disable');
                    document.getElementById("InvoiceDatefiled").classList.add('disable');
                } else {
                    let sessionModal = document.getElementById('dateSpan');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('dateSpan').style.display = 'block';
                }

                break;
            case "dispatchDate":
                if (this.checkDateDiff(value.start, value.end) <= this.rangeDateVal) {
                    this.AllLinesPostJSON["dispatchDate"] = {
                        "from": this.formatedDate(value.start, "ymd"),
                        "to": this.formatedDate(value.end, "ymd")
                    }
                    this.dispatchDate1 = this.formatedDate(value.start) + " - " + this.formatedDate(value.end);
                    this.showErrorMsgNew = false;
                    document.getElementById("OrderDatefiled").classList.add('disable');
                    document.getElementById("DeliveryDatefiled").classList.add('disable');
                    document.getElementById("InvoiceDatefiled").classList.add('disable');
                } else {
                    let sessionModal = document.getElementById('dateSpan');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('dateSpan').style.display = 'block';
                }
                break;
            case "invoiceDate":
                if (this.checkDateDiff(value.start, value.end) <= this.rangeDateVal) {
                    this.AllLinesPostJSON["billToAt"] = {
                        "from": this.formatedDate(value.start, "ymd"),
                        "to": this.formatedDate(value.end, "ymd")
                    }
                    this.invoicedate1 = this.formatedDate(value.start) + " - " + this.formatedDate(value.end);
                    this.showErrorMsgNew = false;
                    document.getElementById("OrderDatefiled").classList.add('disable');
                    document.getElementById("DeliveryDatefiled").classList.add('disable');
                    document.getElementById("DesptchDatefiled").classList.add('disable');
                } else {
                    let sessionModal = document.getElementById('dateSpan');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('dateSpan').style.display = 'block';
                }
                break;
            case "orderDate":
                if (this.checkDateDiff(value.start, value.end) <= this.rangeDateVal) {
                    this.AllLinesPostJSON["orderRaisedDate"] = {
                        "from": this.formatedDate(value.start, "ymd"),
                        "to": this.formatedDate(value.end, "ymd")
                    }
                    this.orderdate1 = this.formatedDate(value.start) + " - " + this.formatedDate(value.end);
                    this.showErrorMsgNew = false;
                    document.getElementById("DeliveryDatefiled").classList.add('disable');
                    document.getElementById("InvoiceDatefiled").classList.add('disable');
                    document.getElementById("DesptchDatefiled").classList.add('disable');

                } else {
                    let sessionModal = document.getElementById('dateSpan');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('dateSpan').style.display = 'block';
                }
                break;
            case "statusCurrent":
                this.AllLinesPostJSON["statusCurrent"] = this.status;
                break;
            case "shipToLocationId":
                this.AllLinesPostJSON["shipToLocationId"] = this.shipto;
                break;
            case "shipFromLocationId":
                this.AllLinesPostJSON["shipFromLocationId"] = this.shipfrom;
                break;
            case "billToId":
                this.AllLinesPostJSON["billToId"] = this.invoiceId;
                break;
            case "dispatchDAteParam":
                this.AllLinesPostJSON["isFlag"] = this.dispatchDAteParamID ? false : true;
                break;
        }
    }
    closedateSpan() {
        let sessionModal = document.getElementById('dateSpan');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('dateSpan').style.display = 'none';
    }
    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else {
                        reject('sessionInactive');
                        sessionStorage.clear();
                        this._router.navigate(['']);
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;
                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        this.terminateFlg = false;
                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            console.log("org list loop");
                            console.log(item.customerName);
                            console.log("customerName loop");
                            console.log(customerName)
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
                        this.organisationListChange = changeOrgAvailable;
                        this.orgimgUrl = orgImgUrl;
                        this.orgApiName = orgApiNameTemp;
                        // this.disabledCusChange = false;
                        if (this.organisationList.length == 1) {
                            this.disabledCusChange = true;
                        }

                        console.log("this.organisationList234");
                        console.log(this.organisationList);
                        console.log(this.organisationListChange);
                        let that = this;
                        let reports = true;
                        let claimsReview = true;
                        let preArrayWholesaleStores: any = [];
                        let preArrayreportArrBtn: any = [];
                        preArrayWholesaleStores = [];
                        let dashboardMenuPermission = false;
                        let userHavingPermission = false;
                        let allOrderLinesReportSubmit: boolean = true;
                        let ordersReceivedByStoreAndCategoryView: boolean = true;
                        let periodicRepricingDraftSubmissionReportView: boolean = true;
                        let periodicRepricingSubmissionReportView: boolean = true;
                        let productRepricingDraftSubmissionReportView: boolean = true;
                        let shortedOrderLinesReportView: boolean = true;
                        let productRepricingSubmissionReportView: boolean = true;
                        let shortedOrderLinesReportSubmit: boolean = true;
                        let allOrderLinesReportView: boolean = true;
                        let allOrderLinesDateRangeReportView: boolean = true;
                        let allOrderLinesDateRangeReportSubmit: boolean = true;
                        let dealReportSubmit: boolean = true;
                        let dealReportView: boolean = true;
                        let storepickItemNotFoundReportView: boolean = true;
                        let netNegativeReportView: boolean = true;
                        let storepickPriceMismatchReportView: boolean = true;
                        let storepickUnexpectedFeesErrorReportView: boolean = true;
                        let storepickInDayInventoryReportView: boolean = true;
                        let storepickCommisionMismatchReportView: boolean = true;
                        let storepickSellerCentralErrorReportView: boolean = true;
                        this.organisationList.forEach(function (organisation: any) {

                            if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                                console.log("organisation : " + that.customerName);
                                console.log(organisation);
                                organisation.permissionRoleList.forEach(function (permRoleList: any) {
                                    console.log("11");
                                    console.log(permRoleList);
                                    permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                        console.log("22");
                                        if (permRoleDtlList.permissionGroup == 'reports') {
                                            userHavingPermission = true;
                                            permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                                // if(permList.permissionName == 'reports'){
                                                //     dashboardMenuPermission = true;
                                                // }
                                                //debugger;
                                                console.log(permList.permissionName)
                                                switch (permList.permissionName.trim()) {
                                                    case "reports":
                                                        console.log("55");
                                                        reports = false;
                                                        break;
                                                    case "allOrderLinesReportView":
                                                        console.log("55");
                                                        allOrderLinesReportView = false;
                                                        preArrayWholesaleStores.push({ "allOrderLinesReportView": true, "reportType": "All Order Lines Daily Report" });
                                                        break;
                                                    case "allOrderLinesReportSubmit":
                                                        console.log("55");
                                                        allOrderLinesReportSubmit = false;
                                                        preArrayreportArrBtn.push({ "submitBtm": true, "reportType": "All Order Lines Daily Report" });
                                                        break;
                                                    case "allOrderLinesDateRangeReportView":
                                                        console.log("55");
                                                        allOrderLinesDateRangeReportView = false;
                                                        preArrayWholesaleStores.push({ "allOrderLinesDateRangeReportView": true, "reportType": "All Order Lines Date Range report" });
                                                        break;
                                                    case "allOrderLinesDateRangeReportSubmit":
                                                        console.log("55");
                                                        allOrderLinesDateRangeReportSubmit = false;
                                                        preArrayreportArrBtn.push({ "submitBtm": true, "reportType": "All Order Lines Date Range report" });
                                                        break;
                                                    case "ordersReceivedByStoreAndCategoryView":
                                                        console.log("55");
                                                        ordersReceivedByStoreAndCategoryView = false;
                                                        preArrayWholesaleStores.push({ "ordersReceivedByStoreAndCategoryView": true, "reportType": "Orders Received Store Category Daily Report" });
                                                        break;
                                                    case "periodicRepricingDraftSubmissionReportView":
                                                        console.log("55");
                                                        periodicRepricingDraftSubmissionReportView = false;
                                                        preArrayWholesaleStores.push({ "periodicRepricingDraftSubmissionReportView": true, "reportType": "Periodic Repricing Draft Submission Report" });
                                                        break;
                                                    case "periodicRepricingSubmissionReportView":
                                                        console.log("55");
                                                        periodicRepricingSubmissionReportView = false;
                                                        preArrayWholesaleStores.push({ "periodicRepricingSubmissionReportView": true, "reportType": "Periodic Repricing Submission Report" });
                                                        break;
                                                    case "productRepricingDraftSubmissionReportView":
                                                        console.log("55");
                                                        productRepricingDraftSubmissionReportView = false;
                                                        preArrayWholesaleStores.push({ "productRepricingDraftSubmissionReportView": true, "reportType": "Product Repricing Draft Submission Report" });
                                                        break;
                                                    case "productRepricingSubmissionReportView":
                                                        console.log("55");
                                                        productRepricingSubmissionReportView = false;
                                                        preArrayWholesaleStores.push({ "productRepricingSubmissionReportView": true, "reportType": "Product Repricing Submission Report" });
                                                        break;
                                                    case "shortedOrderLinesReportSubmit":
                                                        console.log("55");
                                                        shortedOrderLinesReportSubmit = false;
                                                        preArrayreportArrBtn.push({ "submitBtm": false, "reportType": "Rejected (Shorted) Order Lines Report" });
                                                        break;
                                                    case "shortedOrderLinesReportView":
                                                        console.log("55");
                                                        shortedOrderLinesReportView = false;
                                                        preArrayWholesaleStores.push({ "shortedOrderLinesReportView": true, "reportType": "Rejected (Shorted) Order Lines Report" });
                                                        break;
                                                    case "dealReportSubmit":
                                                        console.log("55");
                                                        dealReportSubmit = false;
                                                        preArrayreportArrBtn.push({ "submitBtm": true, "reportType": "Promotion Report" });
                                                        break;
                                                    case "dealReportView":
                                                        console.log("55");
                                                        dealReportView = false;
                                                        preArrayWholesaleStores.push({ "dealReportView": true, "reportType": "Promotion Report" });
                                                        break;
                                                    case "storepickItemNotFoundReportView":
                                                        storepickItemNotFoundReportView = false;
                                                        preArrayWholesaleStores.push({ "storepickItemNotFoundReportView": true, "reportType": "Storepick - Item Not Found Report" });
                                                        break;
                                                    case "netNegativeReportView":
                                                        netNegativeReportView = false;
                                                        preArrayWholesaleStores.push({ "netNegativeReportView": true, "reportType": "DSD Net negative Report" });
                                                        break;
                                                    case "storepickPriceMismatchReportView":                                                        
                                                        storepickPriceMismatchReportView = false;
                                                        preArrayWholesaleStores.push({ "storepickPriceMismatchReportView": true, "reportType": "Storepick - Price Mismatch Report" });
                                                        break;
                                                    case "storepickUnexpectedFeesErrorReportView":
                                                        storepickUnexpectedFeesErrorReportView = false;
                                                        preArrayWholesaleStores.push({ "storepickUnexpectedFeesErrorReportView": true, "reportType": "Storepick - Unexpected Fees Report" });
                                                        break;
                                                    case "storepickInDayInventoryReportView":
                                                        storepickInDayInventoryReportView = false;
                                                        preArrayWholesaleStores.push({ "storepickInDayInventoryReportView": true, "reportType": "Storepick - In day Inventory Report" });
                                                        break;    
                                                    case "storepickCommisionMismatchReportView":
                                                        storepickCommisionMismatchReportView = false;
                                                        preArrayWholesaleStores.push({ "storepickCommisionMismatchReportView": true, "reportType": "Storepick - Commission Discrepancies Report" });
                                                        break;
                                                    case "storepickSellerCentralErrorReportView":
                                                        storepickSellerCentralErrorReportView = false;
                                                        preArrayWholesaleStores.push({ "storepickSellerCentralErrorReportView": true, "reportType": "Storepick - Seller Central Product Missing Report" });
                                                        break;
                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                        that = null;

                        this.ordersReceivedByStoreAndCategoryView = ordersReceivedByStoreAndCategoryView;
                        this.allOrderLinesReportView = allOrderLinesReportView;
                        this.allOrderLinesDateRangeReportView = allOrderLinesDateRangeReportView;
                        this.allOrderLinesReportSubmit = allOrderLinesReportSubmit;
                        this.periodicRepricingDraftSubmissionReportView = periodicRepricingDraftSubmissionReportView;
                        this.periodicRepricingSubmissionReportView = periodicRepricingSubmissionReportView;
                        this.productRepricingDraftSubmissionReportView = productRepricingDraftSubmissionReportView;
                        this.productRepricingSubmissionReportView = productRepricingSubmissionReportView;
                        this.shortedOrderLinesReportSubmit = shortedOrderLinesReportSubmit;
                        this.shortedOrderLinesReportView = shortedOrderLinesReportView;
                        this.dealReportView = dealReportView;
                        this.itemnotFoundReportView = storepickItemNotFoundReportView;
                        this.netNegativeReportView = netNegativeReportView;
                        this.storepickUnexpectedFeesErrorReportView = storepickUnexpectedFeesErrorReportView;
                        this.storepickCommisionMismatchReportView = storepickCommisionMismatchReportView;
                        this.storepickSellerCentralErrorReportView = storepickSellerCentralErrorReportView;
                        this.outOfRangeReportView =  storepickPriceMismatchReportView;
                        this.reportArr = [];
                        this.reportArr = preArrayWholesaleStores;
                        this.reportArrBtn = [];
                        this.reportArrBtn = preArrayreportArrBtn;
                        console.log(preArrayWholesaleStores);
                        resolve('sessionActive');

                        if (!userHavingPermission) {
                            // redirect to order
                            this._router.navigate(["order", customerName]);
                        }
                    }
                }
            });
        });
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    updateJSONONEmpty(datepicker?: any) {
        console.log("abcd");
        switch (datepicker) {
            case "deliverydate":
                if (typeof this.AllLinesPostJSON["shipToDeliverAt"] !== 'undefined') {
                    delete this.AllLinesPostJSON["shipToDeliverAt"];
                }
                if (typeof this.AllLinesPostJSON["shipToDeliverAt"] == 'undefined') {
                    document.getElementById("DesptchDatefiled").classList.remove('disable');
                    document.getElementById("InvoiceDatefiled").classList.remove('disable');
                    document.getElementById("OrderDatefiled").classList.remove('disable');
                }
                break;
            case "dispatchDate":
                if (typeof this.AllLinesPostJSON["dispatchDate"] !== 'undefined') {
                    delete this.AllLinesPostJSON["dispatchDate"];
                }
                if (typeof this.AllLinesPostJSON["dispatchDate"] == 'undefined') {
                    document.getElementById("DeliveryDatefiled").classList.remove('disable');
                    document.getElementById("InvoiceDatefiled").classList.remove('disable');
                    document.getElementById("OrderDatefiled").classList.remove('disable');
                }
                break;
            case "invoiceDate":
                if (typeof this.AllLinesPostJSON["billToAt"] !== 'undefined') {
                    delete this.AllLinesPostJSON["billToAt"];
                }
                if (typeof this.AllLinesPostJSON["billToAt"] == 'undefined') {
                    document.getElementById("DeliveryDatefiled").classList.remove('disable');
                    document.getElementById("OrderDatefiled").classList.remove('disable');
                    document.getElementById("DesptchDatefiled").classList.remove('disable');
                }
                break;
            case "orderDate":
                if (typeof this.AllLinesPostJSON["orderRaisedDate"] !== 'undefined') {
                    delete this.AllLinesPostJSON["orderRaisedDate"];
                }
                if (typeof this.AllLinesPostJSON["orderRaisedDate"] == 'undefined') {
                    document.getElementById("DeliveryDatefiled").classList.remove('disable');
                    document.getElementById("InvoiceDatefiled").classList.remove('disable');
                    document.getElementById("DesptchDatefiled").classList.remove('disable');
                }
                break;
        }
    }

    getConfigService(pageName: any) {
        this._postsService.siteMaintanceCall('1', 'all').subscribe(userData => {
            console.log(userData);
            console.log(userData.maintenanceDetails[0].message);
            console.log(userData.maintenanceDetails[0].start);
            console.log(userData.maintenanceDetails[0].status);
            console.log(userData.deploymentDetails);
            if (userData.deploymentDetails != undefined && userData.deploymentDetails[0].status != 'NO') {
                this.deploymentDetailsMsg = userData.deploymentDetails[0].message + ' ' + userData.deploymentDetails[0].start;
                this.deploymentDetailsMsglength = this.deploymentDetailsMsg.length;
                sessionStorage.setItem('deploymentDetailsMsglength', this.deploymentDetailsMsglength);
            } else {
                this.deploymentDetailsMsg = '';
            }


            console.log(userData.featureDetails);
            if (userData.featureDetails != undefined && userData.featureDetails[0].status != 'NO') {
                this.featureDetailsMsg = userData.featureDetails[0].message + ' ' + userData.featureDetails[0].start;
                this.featureDetailsMsglength = this.featureDetailsMsg.length;
                sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
            } else {
                this.featureDetailsMsg = '';
            }

            console.log(userData.businessMessageDetails);
            if (userData.businessMessageDetails != undefined && userData.businessMessageDetails[0].status != 'NO') {
                this.businessMessageDetailsMsg = userData.businessMessageDetails[0].message + ' ' + userData.businessMessageDetails[0].start;
                this.businessMessageDetailsMsglength = this.businessMessageDetailsMsg.length;
                sessionStorage.setItem('businessMessageDetailsMsglength', this.businessMessageDetailsMsglength);

            } else {
                this.businessMessageDetailsMsg = '';
            }

            (<HTMLLabelElement>document.getElementById('deploymentDetailsId')).textContent = this.deploymentDetailsMsg;
            (<HTMLLabelElement>document.getElementById('featureDetailsId')).textContent = this.featureDetailsMsg;
            (<HTMLLabelElement>document.getElementById('businessMessageDetailsId')).textContent = this.businessMessageDetailsMsg;
            if (userData.maintenanceDetails[0].moduleName == 'all') {
                if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                    this.siteMaintanceFlg = true;
                    this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                } else {
                    this.siteMaintanceFlg = false;
                    this._postsService.siteMaintanceCall(this.customerName, pageName).subscribe(userData => {
                        console.log(userData);
                        console.log(userData.maintenanceDetails[0].message);
                        console.log(userData.maintenanceDetails[0].start);
                        console.log(userData.maintenanceDetails[0].status);
                        if (userData.maintenanceDetails[0].moduleName == pageName) {
                            if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                                this.siteMaintanceFlg = true;
                                this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                            } else {
                                this.siteMaintanceFlg = false;
                            }
                        } else {
                            this.siteMaintanceFlg = false;
                        }
                        this.showLoader = false;
                    }, error => {
                        console.log(error);
                        if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                            this.siteMaintanceFlg = true;
                            this.siteMSg = "Site is under maintenance";
                        } else if (error.errorCode == '404.26.408') {
                            this.maintenanceErrorMsg = "User is Inactive";
                        }
                        else {
                            this.siteMaintanceFlg = false;
                        }
                        this.showLoader = false;
                    });
                }
            } else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        }, error => {
            console.log(error);
            if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                this.siteMaintanceFlg = true;
                this.siteMSg = "Site is under maintenance";
            } else if (error.errorCode == '404.26.408') {
                this.maintenanceErrorMsg = "User is Inactive";
            }
            else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        });
    }
}