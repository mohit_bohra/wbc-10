import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportsComponent } from './reports.component';

// route Configuration
const reportsRoutes: Routes = [
  { path: '', component: ReportsComponent },
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(reportsRoutes)],
  exports: [RouterModule]
})
export class ReportsComponentRoutes { }
