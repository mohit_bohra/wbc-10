import { NgModule } from '@angular/core';
import { ReportsComponentRoutes } from './reports.routes';
import { ReportsComponent } from './reports.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../global/global.component';
import { Services } from '../../services/app.services';
import { FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
// import { DatepickerModule } from 'ng2-bootstrap';
import { TooltipModule } from "ngx-tooltip";
import { DatepickerModule } from '../datepicker/ng2-bootstrap';
import { Daterangepicker } from 'ng2-daterangepicker';
//import { NavigationModule } from '../navigation/navigation.module';
import { NavigationModule } from '../navigationFulfilment/navigation.module';
import { OrderBy } from './report.orderby.pipe';

@NgModule({
  imports: [
    ReportsComponentRoutes,
    CommonModule,
    FormsModule,
    InfiniteScrollModule,
    TooltipModule,
    NavigationModule,
    DatepickerModule.forRoot(),
    Daterangepicker
  ],
  exports: [],
  declarations: [ReportsComponent,OrderBy],
  providers: [Services, GlobalComponent]
})
export class ReportsModule { }
