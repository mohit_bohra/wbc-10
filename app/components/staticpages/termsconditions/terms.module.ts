import { NgModule } from '@angular/core';
import { TermsComponentModule } from './terms.routes';
import { TermsComponent }   from './terms.component';
import { CommonModule } from '@angular/common';  
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services'; 

@NgModule({
  imports: [
    TermsComponentModule,
    CommonModule
    
  ],
  exports: [],
  declarations: [TermsComponent],
  providers: [Services, GlobalComponent]
})
export class TermsModule { }