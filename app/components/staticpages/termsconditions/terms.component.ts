import { Component, OnInit } from '@angular/core';
import { GlobalComponent } from '../../global/global.component';
@Component({
  selector: 'termscond',
  templateUrl: 'terms.component.html',
  providers: [GlobalComponent]
})
export class TermsComponent implements OnInit {

    constructor(private _globalFunc: GlobalComponent) {
     
    }
    // function execute on load
    ngOnInit() {
       this._globalFunc.autoscroll();
    }

    goToTop(){
      this._globalFunc.autoscroll();
    }
}
