import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalComponent } from '../../global/global.component';
@Component({
    selector: 'cookiespol',
    templateUrl: 'cookies.component.html',
    //  template: require('./cookies.component.html'),
    providers: [GlobalComponent]
})
export class CookiesComponent implements OnInit {

    constructor(private _router: Router, private _globalFunc: GlobalComponent) {


    }
    // function execute on load
    ngOnInit() {


        this._globalFunc.autoscroll();

    }

    goToTop() {
        this._globalFunc.autoscroll();
    }

    redirect(page: any) {

        this._router.navigate([page]);

    }

}

