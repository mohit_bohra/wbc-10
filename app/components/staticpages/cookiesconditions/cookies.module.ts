import { NgModule } from '@angular/core';
import { CookiesComponentModule } from './cookies.routes';
import { CookiesComponent } from './cookies.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';

@NgModule({
  imports: [
    CookiesComponentModule,
    CommonModule

  ],
  exports: [],
  declarations: [CookiesComponent],
  providers: [Services, GlobalComponent]
})
export class CookiesModule { }