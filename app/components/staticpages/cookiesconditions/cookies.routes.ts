import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CookiesComponent } from './cookies.component';

// route Configuration
export const cookiesRoutes: Routes = [
  { path: '', component: CookiesComponent }
];



@NgModule({
  imports: [RouterModule.forChild(cookiesRoutes)],
  exports: [RouterModule]
})
export class CookiesComponentModule { }
