import { NgModule } from '@angular/core';
import { PrivacyComponentModule } from './privacy.routes';
import { PrivacypolicyComponent } from './privacypolicy.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';

@NgModule({
  imports: [
    PrivacyComponentModule,
    CommonModule

  ],
  exports: [],
  declarations: [PrivacypolicyComponent],
  providers: [Services, GlobalComponent]
})
export class PrivacyModule { }