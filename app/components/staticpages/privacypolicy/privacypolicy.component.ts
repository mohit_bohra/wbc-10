import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
@Component({
    selector: 'privacypol',
    templateUrl: 'privacy.component.html',
    providers: [GlobalComponent]
})
export class PrivacypolicyComponent implements OnInit {
    objConstant = new Constant();
    contactUsUrl: any;
    constructor(private _router: Router, private _globalFunc: GlobalComponent) {

        this.contactUsUrl = this.objConstant.CONTACT_US;
    }
    ngOnInit() {

        this._globalFunc.autoscroll();

    }

    goToTop() {
        this._globalFunc.autoscroll();
    }

    redirect(page: any) {

        this._router.navigate([page]);

    }
}

