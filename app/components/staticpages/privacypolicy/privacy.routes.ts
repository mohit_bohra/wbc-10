import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrivacypolicyComponent } from './privacypolicy.component';

export const ppRoutes: Routes = [
  { path: '', component: PrivacypolicyComponent }
];


@NgModule({
  imports: [RouterModule.forChild(ppRoutes)],
  exports: [RouterModule]
})
export class PrivacyComponentModule { }
