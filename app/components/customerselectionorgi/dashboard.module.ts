import { NgModule } from '@angular/core';
import { DashboardComponentRoutes } from './dashboard.routes';
import { DashboardComponent } from './dashboard.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../global/global.component';
import { Services } from '../../services/app.services';

@NgModule({
  imports: [
    DashboardComponentRoutes,
    CommonModule
  ],
  exports: [],
  declarations: [DashboardComponent],
  providers: [Services, GlobalComponent]
})
export class DashboardModule { }
