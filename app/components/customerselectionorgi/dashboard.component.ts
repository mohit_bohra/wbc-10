import { Component, ElementRef, Renderer } from '@angular/core';
// import { Services } from '../../services/app.services';
import { GlobalComponent } from '../global/global.component';
import { Constant } from '../constants/constant';
import { Services } from '../../services/app.services';
import { Router } from '@angular/router';

@Component({
    selector: 'dashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardComponent {
    // globalListenFunc will hold the function returned by "renderer.listenGlobal"
    globalListenFunc: Function;

    objConstant = new Constant();
    whoImgUrl: any;
    organisationList: any = [];
    showLoader: boolean;
    pageState: number = 1;
    allCustPermFalse: boolean = true;

    custPermission: any[];
    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _router: Router, elementRef: ElementRef, renderer: Renderer) {
        this.whoImgUrl = this.objConstant.WHOIMG_URL;

        if (sessionStorage.getItem('name') !== null && sessionStorage.getItem('name') !== 'undefined') {
            let timeoutId = setTimeout(() => {
                if (sessionStorage.getItem('pageState') !== '0') {
                    sessionStorage.setItem('timeoutState', '1');
                    let sessionModal = document.getElementById('timeoutModal');
                    sessionModal.classList.remove('out');
                    sessionModal.classList.add('in');
                    document.getElementById('timeoutModal').style.display = 'block';
                    setTimeout(() => {
                        this.timeOutNoReaction();
                    }, this.objConstant.WARNING_TIMEOUT);
                }
            }, this.objConstant.SESSION_TIMEOUT);
            // We cache the function "listenGlobal" returns
            this.globalListenFunc = renderer.listenGlobal('document', 'click', (event: any) => {
                // Do something with 'event'
                clearTimeout(timeoutId);
                timeoutId = setTimeout(() => {
                    if (sessionStorage.getItem('pageState') !== '0') {
                        sessionStorage.setItem('timeoutState', '1');
                        let sessionModal = document.getElementById('timeoutModal');
                        sessionModal.classList.remove('out');
                        sessionModal.classList.add('in');
                        document.getElementById('timeoutModal').style.display = 'block';
                        setTimeout(() => {
                            this.timeOutNoReaction();
                        }, this.objConstant.WARNING_TIMEOUT);
                    }
                }, this.objConstant.SESSION_TIMEOUT);
            });

            this.globalListenFunc = renderer.listenGlobal('document', 'keypress', (event: any) => {
                // Do something with 'event'
                clearTimeout(timeoutId);
                timeoutId = setTimeout(() => {
                    if (sessionStorage.getItem('pageState') !== '0') {
                        sessionStorage.setItem('timeoutState', '1');
                        let sessionModal = document.getElementById('timeoutModal');
                        sessionModal.classList.remove('out');
                        sessionModal.classList.add('in');
                        document.getElementById('timeoutModal').style.display = 'block';
                        setTimeout(() => {
                            this.timeOutNoReaction();
                        }, this.objConstant.WARNING_TIMEOUT);

                    }

                }, this.objConstant.SESSION_TIMEOUT);
            });
        }
    }

    // function works on page load
    ngOnInit() {
        this._globalFunc.logger("Loading Customer Selection on UI");
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._globalFunc.logger("User Session Not Found");
            this._router.navigate(['']);
        } else {
            this.getUserSession('default', sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
            let path: string = window.location.hash;
            if (path && path.length > 0) {
                this._router.navigate([path.substr(2)]);
            }

            // window scroll function
            this._globalFunc.autoscroll();
            this.pageState = 1;
            sessionStorage.setItem('pageState', '1');
            document.getElementById('login').style.display = 'inline';
            // initial page width
            let wid: any;
            wid = window.innerWidth;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);

            window.onresize = () => {
                // get device to populate some part of ui
                screen = this._globalFunc.getDevice(window.innerWidth);

            }

            this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));
            let custPerm: any = [];
            let allFalse = true;
            

            this.organisationList.forEach(function (item: any) {
                if (item.permissions.menuPermission.catalogueEnquiry == 'none' &&
                    item.permissions.menuPermission.catalogueManagement == 'none' &&
                    item.permissions.menuPermission.orders == 'none' &&
                    item.permissions.menuPermission.reports == 'none' &&
                    item.permissions.menuPermission.accessMgmt == 'none' &&
                    item.permissions.menuPermission.dashboards == 'none') {
                    custPerm[item.orgType] = false;
                } else {
                    custPerm[item.orgType] = true;
                    allFalse = false;
                }
            });
            this.custPermission = custPerm;
            this.allCustPermFalse = allFalse;
            if (allFalse) {
               this._globalFunc.logger("Permission is not given for any customer to user.");
                // User doesnot have access to any customer
                //alert("You are not allow to access");
            } else {
                if (this.organisationList.length == 1) {
                    //If there is only on customer
                    this.redirect('/orders', this.organisationList[0].customerName);
                }
            }

            

            document.getElementById('orderMob').style.display = 'none';
            document.getElementById('orderRaiseMob').style.display = 'none';
            document.getElementById('catalogueMob').style.display = 'none';
            document.getElementById('catalogueRaiseMob').style.display = 'none';
            document.getElementById('reportsMob').style.display = 'none';
            document.getElementById('permissonsMob').style.display = 'none';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
            (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
        }
    }

    timeOutNoReaction() {
        if (sessionStorage.getItem('timeoutState') == '1') {
            sessionStorage.setItem('timeoutState', '0');
            let sessionModal = document.getElementById('timeoutModal');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('timeoutModal').style.display = 'none';
            sessionStorage.clear();
            document.getElementById('login').style.display = 'none';
            this._router.navigate(['']);
        }
    }

    // navigate function
    redirect(page: any, orgType: any) {
        try{
            this._globalFunc.logger("Redirecting to " + page + " - " + orgType);
            this._router.navigate([page, orgType]);
        } catch(ex){
            this._globalFunc.logger("Exception occured on redirection - " + ex.name + " : " + ex.message);
            throw(ex);
        }
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
        this._globalFunc.logger("Unloading Customer selection screen.");
        this._globalFunc.sentLoggerBatchToServer();
    }

    getUserSession(customerName:any, loggedInUserGroup:any, loggedInUserId:any){
        this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData:any) => {
            console.log(userData);
            if(!userData.isSessionActive) { // false
                // User session has expired.
                sessionStorage.clear();
                this._router.navigate(['']);
            } else {
                // this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                // console.log("this.organisationList123");
                // console.log(this.organisationList);

                // if (this.organisationList.length == 1) {
                //     //If there is only on customer
                //     this.redirect('/orders', this.organisationList[0].customerName);
                // } else if(this.organisationList.length == 0){
                //     this._globalFunc.logger("Permission is not given for any customer to user.");
                //     // User doesnot have access to any customer
                // }
            }
        });
    }
}

