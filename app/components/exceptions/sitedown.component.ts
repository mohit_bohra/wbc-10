import { Component, OnInit } from '@angular/core';
import { GlobalComponent } from '../global/global.component';
import { Constant } from '../constants/constant';
import { commonfunction } from '../../services/commonfunction.services';

@Component({
    selector: 'sitedown',
    templateUrl: 'sitedown.component.html',
    providers: [GlobalComponent, commonfunction]
})

export class SiteDownComponent implements OnInit {
    pageState: number = 9;
    objConstant = new Constant();
    constructor(private _globalFunc: GlobalComponent) {

    }

    ngOnInit() {
        console.log("123123");
        this._globalFunc.autoscroll();
        document.getElementById('login').style.display = 'inline';
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
        (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
        sessionStorage.setItem('pageState', '9');
    }
}

