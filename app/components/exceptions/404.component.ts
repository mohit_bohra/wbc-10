import { Component, OnInit } from '@angular/core';
import { GlobalComponent } from '../global/global.component';
import { Constant } from '../constants/constant';

@Component({
    selector: 'error404',
    templateUrl: '404.component.html',
    providers: [GlobalComponent]
})

export class NotFoundComponent implements OnInit {
    objConstant = new Constant();
    constructor(private _globalFunc: GlobalComponent) {

    }

    ngOnInit() {
        this._globalFunc.autoscroll();
        document.getElementById('login').style.display = 'inline';
        (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');
        (<HTMLLabelElement>document.getElementById('usernameMobile')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

    }
}

