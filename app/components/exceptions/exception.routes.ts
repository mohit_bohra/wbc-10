
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './404.component';
import  { SiteDownComponent } from './sitedown.component';

export const exceptionRoutes: Routes = [
  { path: '', component: NotFoundComponent },
  { path :'sitedown', component: SiteDownComponent}
  
];

@NgModule({
  imports: [RouterModule.forChild(exceptionRoutes)],
  exports: [RouterModule]
})
export class exceptionComponentModule { }
