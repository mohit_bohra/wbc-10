import { NgModule } from '@angular/core';
import { exceptionComponentModule } from './exception.routes';
import { NotFoundComponent } from './404.component';
import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../global/global.component';
import { Services } from '../../services/app.services';
import { SiteDownComponent }  from './sitedown.component';
import { CatalogueManagePendingHeaderModule } from '../cataloguemanagependingheader/cataloguemanagependingheader.module';

@NgModule({
  imports: [
    exceptionComponentModule,
    CatalogueManagePendingHeaderModule,
    CommonModule
  ],
  exports: [],
  declarations: [NotFoundComponent, SiteDownComponent],
  providers: [Services, GlobalComponent]
})
export class ExceptionModule { }