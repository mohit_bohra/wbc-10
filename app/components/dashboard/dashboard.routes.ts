import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
// route Configuration
const dashboardRoutes: Routes = [
    { path: '', component: DashboardComponent },
    { path: 'fulfilment', loadChildren:'./fulfilment/fulfilment.module#FulfilmentModule'}
    /* { path: 'fulfilmentlvlone', loadChildren:'./fulfilment/fulfilmentlvlone.module#FulfilmentlvloneModule'} */
];


@NgModule({
    imports: [RouterModule.forChild(dashboardRoutes)],
    exports: [RouterModule]
})
export class DashboardRoutes { }
