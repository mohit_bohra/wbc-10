import { Component } from '@angular/core';
import { Services } from '../../services/app.services';
import { GlobalComponent } from '../global/global.component';
import { commonfunction } from '../../services/commonfunction.services';
import { Constant } from '../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'dashboard.component.html',
    providers: [commonfunction]
})
export class DashboardComponent {
    public catalgoueDate: Date = new Date();
    objConstant = new Constant();
    whoImgUrl: any;
    catalogueCode: any;
    catalogueCodeSelected: any;
    catalogueList: any;
    catalogueListLength: any;
    catalogueUpdatedDate: any;
    catalogueno: string;
    showLoader: boolean;
    hideOrganisation: boolean = true;
    catalogueResult: boolean = true;
    printHeader: boolean = true;
    pageState: number = 27;
    customerName: string;
    customerDispayName: string;
    style: string;
    organisationList: any;
    organisationListChange: any;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disabledOrder: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    private _sub: any;
    disabledExportCatalogue: boolean;
    disabledUploadCatalogue: boolean;
    disabledfilfilment: boolean;
    imgName: string = "";

    //for catalogue Enquiry redevelopment
    catalogueTypes: any;
    catalogueType: string;
    cataloguemasterListLength: any;
    cataloguedetailListLength: any;
    catalogueDetailList: any;
    strInfoNotAvail: String;
    start: number;
    limit: number;
    count: number;
    hitNext: boolean;

    public todayDate: Date = new Date();

    homeUrl: any;
    custPermission: any[];

    moduleName: string;

    terminateFlg: boolean = false;

    siteMaintanceFlg: boolean;
    siteMaintanceFlg1: boolean;
    siteMSg: any;
    errorMsg: string;
     deploymentDetailsMsg: string;
    featureDetailsMsg: string;
    businessMessageDetailsMsg: string;
    deploymentDetailsMsglength:any;
    featureDetailsMsglength:any;
    businessMessageDetailsMsglength:any;

    constructor(private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router, public _cfunction: commonfunction) {
        // img urls
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
    }

    // function works on page load
    ngOnInit() {
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {
            this.showLoader = false;
            this.catalogueno = '';
            // user permission
            this.disabledCusChange = false;
            this.disabledPermission = false;
            this.disabledReports = false;
            this.disabledCatalogue = false;
            this.disabledOrder = false;
            this.disabledOrderRaise = false;
            this.disabledCatalogueRaise = false;
            this.disabledManageCatalogue = false;
            this.disabledExportCatalogue = false;
            this.disabledUploadCatalogue = false;
            this.disabledfilfilment = false;
            this.strInfoNotAvail = "-";
            this.catalogueType = "";

            // window scroll function
            this._globalFunc.autoscroll();

            // set header basket data
            this.start = 0;
            this.limit = this.objConstant.SCROLL_RECORD_LIMIT;
            this.count = 0;
            this.hitNext = false;
            sessionStorage.setItem('pageState', '27');
            this.moduleName = "dashboard";
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }

                this.imgName = this.getCustomerImage(params.orgType);

                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                let reports;
                let orders;
                let catalogues;
                let cataloguesRaise;
                let uploadCatalogue;
                let exportCatalogue;
                let filfilment;
                let accessMgmt;
                let redirct;
                let changeOrgAvailable = new Array;
                let catManagement;
                let dashboards;

                if (document.getElementById('catalogueArrow').classList.contains('glyphicon-menu-up') === true) {
                    let sublinks = document.getElementById('catalogueArrow');
                    sublinks.classList.remove('glyphicon-menu-up');
                    sublinks.classList.add('glyphicon-menu-down');
                    document.getElementById('catalogueRaiseMob').style.display = 'none';
                }

                let custPerm: any = [];
                let that = this;
                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).then(res => {
                    if (res == 'sessionActive') {
                        this.imgName = this.getCustomerImage(params.orgType);
                        this.custPermission = custPerm;
                        this.getConfigService(this.moduleName);
                    }
                }, reason => {
                    console.log(reason);
                });
                that = null;


                //this.disabledfilfilment = filfilment;
            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            // let i = 0;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);

            // logic for resize pop up 
            window.onresize = () => {
                if (screen == "desktop") {

                    if (window.innerWidth < this._globalFunc.stdWidth) {
                        // this.catalogueResult = false;

                    } else {
                        //  this.catalogueResult = true;
                    }
                } else {

                    if (window.innerWidth > this._globalFunc.stdWidth) {
                        //   this.catalogueResult = true;
                        document.getElementById("resultModalClose").click();
                    } else {
                        //  this.catalogueResult = false;

                    }
                }

                // get device to populate some part of ui
                screen = this._globalFunc.getDevice(window.innerWidth);

            };

        }

         //this.siteMaintanceFlg1 = sessionStorage.getItem('site');
        // if(sessionStorage.getItem('site') == "true"){
            
        //     this.siteMaintanceFlg1 = false;
        // }else { 
        //     this.siteMSg = "Site is under maintenance";
        //     this.siteMaintanceFlg1 = true;
        // }

    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    CheckUserhasPermission2AllSubtitle(permissionObj: any) {
        if (permissionObj.fulfilment == "none") { // && permissionObj.fulfilment == "none"
            return false;
        } else {
            return true;
        }
    }

    redirect_page(page: any, orgType: any) {
        this._router.navigate(['dashboard', orgType, 'fulfilment']);
    }

    redirect_home(page: any) {
        this._router.navigate([page]);
    }

    // navigate function
    redirect(page: any) {
        if (page == 'addproduct') {
            let url = this.homeUrl + 'catalogue/' + this.customerName + '/add';
            window.open(url);
        } else {
            this._router.navigate(page);
        }
    }
    // change organisation
    changeOrgType(page: any, orgType: any) {

        // reset field on change of organisation 
        this.catalogueno = '';
        this.catalogueList = [];
        this.catalogueListLength = 0;
        this.catalogueResult = true;
        this.catalogueType = '';
        this.catalogueCode = '';
        this.hitNext = false;
        this.start = 0;
        this.count = 0;
        this.toggleOrganisation();
        this._router.navigate(this._globalFunc.redirectToComponent('dashboard', orgType));
        //this._router.navigate([page, orgType]);
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
    }

    // change organisation div show and hide
    toggleOrganisation() {
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
        }
        else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
        }
    }

    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        return new Promise((resolve, reject) => {
            this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.
                    
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else{
                        reject('sessionInactive');
                        sessionStorage.clear();
                        this._router.navigate(['']);
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        console.log("Session terminated");
                        reject('sessionInactive');
                        this.terminateFlg = true;

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        resolve('sessionActive');
                        this.terminateFlg = false;
                        this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                        let changeOrgAvailable: any = [];
                        let orgImgUrl = "";
                        let orgApiNameTemp = "";
                        this.organisationList.forEach(function (item: any) {
                            if (item.customerName !== customerName) {
                                changeOrgAvailable.push(item);
                            } else {
                                orgImgUrl = item.customerName + ".png";
                                orgApiNameTemp = item.customerName;
                            }
                        });
                        this.organisationListChange = changeOrgAvailable;
                        this.orgimgUrl = orgImgUrl;
                        this.orgApiName = orgApiNameTemp;
                        // this.disabledCusChange = false;
                        if (this.organisationList.length == 1) {
                            this.disabledCusChange = true;
                        }

                        console.log("this.organisationList234");
                        console.log(this.organisationList);
                        let that = this;
                        let filfilment = true;
                        let dashboardMenuPermission = false;
                        this.organisationList.forEach(function (organisation: any) {

                            if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                                console.log("organisation : " + that.customerName);
                                console.log(organisation);
                                organisation.permissionRoleList.forEach(function (permRoleList: any) {
                                    console.log("11");
                                    console.log(permRoleList);
                                    permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                        console.log("22");
                                        if (permRoleDtlList.permissionGroup == 'dashboards') {
                                            permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                                if (permList.permissionName == 'dashboards') {// && permList.permissionStatus == 'active') {
                                                    dashboardMenuPermission = true;
                                                }

                                                switch (permList.permissionName) {
                                                    case "ordersFulfilmentDashboard":
                                                        console.log("55");
                                                        filfilment = false;
                                                        //alert(filfilment);
                                                        break;
                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                        that = null;
                        if (!dashboardMenuPermission) {
                            // redirect to order
                            this._router.navigate(["cataloguemanagement", customerName]);
                        }
                        this.disabledfilfilment = filfilment;
                        /*   } */

                    }
                }
                //alert(this.disabledfilfilment);
            });
        });
    }

    getConfigService(pageName: any) {
        this._postsService.siteMaintanceCall('1', 'all').subscribe(userData => {
            console.log(userData);
            console.log(userData.maintenanceDetails[0].message);
            console.log(userData.maintenanceDetails[0].start);
            console.log(userData.maintenanceDetails[0].status);
            console.log(userData.deploymentDetails);
            if (userData.deploymentDetails != undefined && userData.deploymentDetails[0].status != 'NO') {
                this.deploymentDetailsMsg = userData.deploymentDetails[0].message + ' ' + userData.deploymentDetails[0].start;
                 this.deploymentDetailsMsglength= this.deploymentDetailsMsg.length;
                  sessionStorage.setItem('deploymentDetailsMsglength', this.deploymentDetailsMsglength);
            } else {
                this.deploymentDetailsMsg = '';
            }


            console.log(userData.featureDetails);
            if (userData.featureDetails != undefined && userData.featureDetails[0].status != 'NO') {
                this.featureDetailsMsg = userData.featureDetails[0].message + ' ' + userData.featureDetails[0].start;
                    this.featureDetailsMsglength = this.featureDetailsMsg.length;
                sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
            } else {
                this.featureDetailsMsg = '';
            }

            console.log(userData.businessMessageDetails);
            if (userData.businessMessageDetails != undefined && userData.businessMessageDetails[0].status != 'NO') {
                this.businessMessageDetailsMsg = userData.businessMessageDetails[0].message + ' ' + userData.businessMessageDetails[0].start
                     this.businessMessageDetailsMsglength = this.businessMessageDetailsMsg.length;
                sessionStorage.setItem('businessMessageDetailsMsglength', this.businessMessageDetailsMsglength);;

            } else {
                this.businessMessageDetailsMsg = '';
            }

            (<HTMLLabelElement>document.getElementById('deploymentDetailsId')).textContent = this.deploymentDetailsMsg;
            (<HTMLLabelElement>document.getElementById('featureDetailsId')).textContent = this.featureDetailsMsg;
            (<HTMLLabelElement>document.getElementById('businessMessageDetailsId')).textContent = this.businessMessageDetailsMsg;
            if (userData.maintenanceDetails[0].moduleName == 'all') {
                if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                    this.siteMaintanceFlg = true;
                    this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                } else {
                    this.siteMaintanceFlg = false;
                    this._postsService.siteMaintanceCall(this.customerName, pageName).subscribe(userData => {
                        console.log(userData);
                        console.log(userData.maintenanceDetails[0].message);
                        console.log(userData.maintenanceDetails[0].start);
                        console.log(userData.maintenanceDetails[0].status);
                        if (userData.maintenanceDetails[0].moduleName == pageName) {
                            if (userData.maintenanceDetails[0].status.toLowerCase() == 'no') {
                                this.siteMaintanceFlg = true;
                                this.siteMSg = userData.maintenanceDetails[0].message + ' ' + userData.maintenanceDetails[0].end;
                            } else {
                                this.siteMaintanceFlg = false;
                            }
                        } else {
                            this.siteMaintanceFlg = false;
                        }
                        this.showLoader = false;
                    }, error => {
                        console.log(error);
                        if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                            this.siteMaintanceFlg = true;
                            this.siteMSg = "Site is under maintenance";
                        } else if (error.errorCode == '404.26.408') {
                            this.errorMsg = "User is Inactive";
                        }
                        else {
                            this.siteMaintanceFlg = false;
                        }
                        this.showLoader = false;
                    });
                }
            } else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        }, error => {
            console.log(error);
            if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                this.siteMaintanceFlg = true;
                this.siteMSg = "Site is under maintenance";
            } else if (error.errorCode == '404.26.408') {
                this.errorMsg = "User is Inactive";
            }
            else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        });
    }
    closePopup() {
        let sessionModal = document.getElementById('popup_SessionExpired');
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById('popup_SessionExpired').style.display = 'none';
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }
}
