import { NgModule } from '@angular/core';
import { FulfilmentRoutes } from './fulfilment.routes';
import { FulfilmentComponent } from './fulfilment.compoment';

import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../global/global.component';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../../navigationFulfilment/navigation.module';
import { DatepickerModule } from '../../datepicker/ng2-bootstrap';

@NgModule({
  imports: [
    FulfilmentRoutes,
    CommonModule,
    FormsModule,
    NavigationModule,
    DatepickerModule.forRoot()
  ],
  exports: [],
  declarations: [FulfilmentComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class FulfilmentModule { }
