import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FulfilmentlvltwoComponent } from './fulfilmentlvltwo.compoment';

// route Configuration
const fulfilmentlvltwoRoutes: Routes = [
  { path: '', component: FulfilmentlvltwoComponent }
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];

@NgModule({
  imports: [RouterModule.forChild(fulfilmentlvltwoRoutes)],
  exports: [RouterModule]
})
export class FulfilmentlvltwoRoutes { }
