import { Component, trigger } from '@angular/core';
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { GlobalComponent } from '../../../global/global.component';
import { Constant } from '../../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { document } from '../../../datepicker/utils/facade/browser';
declare let saveAs: any;

@Component({
    templateUrl: 'fulfilmentlvltwo.compoment.html',
})
export class FulfilmentlvltwoComponent {

    shipFromLocationdescp: string;
    getTotalforrows(arg0: any): any {
        console.log(arg0);
        let that = this;
        let orderTotal = 0;
        let confirmTotal = 0;
        let pickTotal = 0;
        let shipTotal = 0;
        arg0.forEach((item: any, index: any) => {
            console.log(item.quantityInfo.qtyOrdered);
            orderTotal = orderTotal + Number(item.quantityInfo.qtyOrdered ? item.quantityInfo.qtyOrdered : 0);
            confirmTotal = confirmTotal + Number(item.quantityInfo.qtyConfirmed ? item.quantityInfo.qtyConfirmed : 0);
            pickTotal = pickTotal + Number(item.quantityInfo.qtyPicked ? item.quantityInfo.qtyPicked : 0);
            shipTotal = shipTotal + Number(item.quantityInfo.qtyShipped ? item.quantityInfo.qtyShipped : 0);
        });
        console.log(orderTotal);
        this.orderedTotalQty = orderTotal;
        this.confirmedTotalQty = confirmTotal;
        this.pickedTotalQty = pickTotal;
        this.shippedTotalQty = shipTotal;
        //throw new Error("Method not implemented.");
    }
    orderedTotalQty: any;
    confirmedTotalQty: any;
    pickedTotalQty: any;
    shippedTotalQty: any;
    public catalgoueDate: Date = new Date();
    objConstant = new Constant();
    whoImgUrl: any;
    catalogueCode: any;
    catalogueCodeSelected: any;
    catalogueList: any;
    catalogueListLength: any;
    catalogueUpdatedDate: any;
    catalogueno: string;
    showLoader: boolean;
    hideOrganisation: boolean = true;
    catalogueResult: boolean = true;
    printHeader: boolean = true;
    pageState: number = 15;
    customerName: string;
    catagoryName: string;
    catagoryNameStr: string;
    uomName: string;
    uomNameStr: string;
    customerDispayName: string;
    style: string;
    organisationList: any;
    organisationListChange: any;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disabledOrder: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    private _sub: any;
    disabledExportCatalogue: boolean;
    disabledUploadCatalogue: boolean;
    disabledfilfilment: boolean;
    currDate: any;
    legacy: any;
    pinNumber: any;
    imgName: string = "";

    orderValidationmsgsList: any = [];

    //for catalogue Enquiry redevelopment
    catalogueTypes: any;
    catalogueType: string;
    cataloguemasterListLength: any;
    cataloguedetailListLength: any;
    catalogueDetailList: any;
    strInfoNotAvail: String;
    start: number;
    limit: number;
    count: number;
    hitNext: boolean;
    fulfilmentdataArr: any[];
    fulfilmentdataLen: number = 0;
    productId: any;
    productDescription: any;

    identifierProductSize: number;
    identifierOrderSize: number;
    identifierquantitySize: number;
    identifierpriceSize: number;
    identifierstatusSize: number;
    productHide: boolean;
    quantityHide: boolean;
    priceHide: boolean;
    statusHide: boolean;
    orderHide: boolean;
    attributeList: string;
    shipFromLocationId: any;
    channel: any;

    public todayDate: Date = new Date();

    homeUrl: any;
    custPermission: any[];

    //ngClass variables
    yellow_border_right_ordered: any;
    yellow_border_right_net: any;
    yellow_border_right_salesOrder: any;

    hideLogo: boolean = false;
    export: boolean = false;
    constructor(private _commFuc: commonfunction, private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router) {
        // img urls
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        this.legacy = sessionStorage.getItem("legacyId");
        this.pinNumber = this.productId = sessionStorage.getItem("pinNumber");
        this.productDescription = sessionStorage.getItem('productDescription');
        console.log(sessionStorage.getItem('orgType') + "orgtype")

        this.productId = sessionStorage.getItem("pinNumber");


        this.yellow_border_right_ordered = 'yellow-border-right';
        this.yellow_border_right_salesOrder = 'yellow-border-right';
        this.yellow_border_right_net = 'yellow-border-right';
        this.orderedTotalQty = 0;
        this.confirmedTotalQty = 0;
        this.pickedTotalQty = 0;
        this.shippedTotalQty = 0;
    }

    // function works on page load
    ngOnInit() {
        this._globalFunc.logger("Fullfillment Drill Down 2 Screen loading...");
        console.log(this._router.url);
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {
            // this.showLoader = false;
            this.catalogueno = '';
            // user permission
            this.disabledCusChange = false;
            this.disabledfilfilment = false;
            this.strInfoNotAvail = "-";
            this.catalogueType = "";


            this.identifierProductSize = 3;
            this.identifierOrderSize = 1;
            this.identifierquantitySize = 2;
            this.identifierpriceSize = 2;
            this.identifierstatusSize = 1;
            this.quantityHide = true;
            this.priceHide = true;
            this.statusHide = true;
            this.orderHide = true;
            this.productHide = true;

            // window scroll function
            this._globalFunc.autoscroll();

            // set header basket data
            this.start = 0;
            this.limit = 15;
            this.count = 0;
            this.hitNext = false;
            sessionStorage.setItem('pageState', '999');
            document.getElementById('diableLogo').style.cursor = "not-allowed";
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string, catagory: string, legacyId: any, uom: string, currDate: any, pinNumber: any, channel: any }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                this.legacy = params.legacyId;
                this.pinNumber = params.pinNumber;
                if (sessionStorage.getItem('pinNumber') == null || sessionStorage.getItem('pinNumber') == 'undefined') {
                    sessionStorage.setItem('pinNumber', params.pinNumber);
                }
                this.pinNumber = sessionStorage.getItem('pinNumber');

                if (sessionStorage.getItem('catagoryName') == null || sessionStorage.getItem('catagoryName') == 'undefined') {
                    sessionStorage.setItem('catagoryName', params.catagory);
                }

                this.catagoryName = sessionStorage.getItem('catagoryName');
                this.catagoryNameStr = sessionStorage.getItem('catagoryNameStr');

                if (sessionStorage.getItem('uomName') == null || sessionStorage.getItem('uomName') == 'undefined') {
                    sessionStorage.setItem('uomName', params.uom);
                }

                this.shipFromLocationId = sessionStorage.getItem('shipFromLocationId');
                this.shipFromLocationdescp = sessionStorage.getItem('shipFromLocationdescp');
                this.uomName = sessionStorage.getItem('uomName');
                this.uomNameStr = sessionStorage.getItem('uomNameStr');

                if (sessionStorage.getItem('channel') == null || sessionStorage.getItem('channel') == 'undefined') {
                    sessionStorage.setItem('channel', params.channel);

                }
                this.channel = sessionStorage.getItem('channel');

                console.log(this.objConstant.tactocal);
                if (sessionStorage.getItem('currDate') == null || sessionStorage.getItem('currDate') == 'undefined') {
                    sessionStorage.setItem('currDate', params.currDate);
                }
                this.currDate = sessionStorage.getItem('currDate');

                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                let reports;
                let orders;
                let catalogues;
                let cataloguesRaise;
                let uploadCatalogue;
                let exportCatalogue;
                let filfilment;
                let accessMgmt;
                let redirct;
                let changeOrgAvailable = new Array;
                let catManagement;
                // list of org assigned to user
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                this.orgApiName = this.customerName;

                document.getElementById('orderArrowSpan').style.display = 'block';
                document.getElementById('orderMob').style.display = 'block';
                document.getElementById('orderRaiseMob').style.display = 'none';

                document.getElementById('catalogueMob').style.display = 'block';
                document.getElementById('catalogueArrowSpan').style.display = 'none';
                document.getElementById('catalogueRaiseMob').style.display = 'none';
                document.getElementById('reportsMob').style.display = 'block';
                document.getElementById('permissonsMob').style.display = 'block';

                if (document.getElementById('orderArrow').classList.contains('glyphicon-menu-up') === true) {
                    let sublinks = document.getElementById('orderArrow');
                    sublinks.classList.remove('glyphicon-menu-up');
                    sublinks.classList.add('glyphicon-menu-down');
                    document.getElementById('orderRaiseMob').style.display = 'none';
                }

                if (document.getElementById('catalogueArrow').classList.contains('glyphicon-menu-up') === true) {
                    let sublinks = document.getElementById('catalogueArrow');
                    sublinks.classList.remove('glyphicon-menu-up');
                    sublinks.classList.add('glyphicon-menu-down');
                    document.getElementById('catalogueRaiseMob').style.display = 'none';
                }

                let custPerm: any = [];


                if (redirct == 1) {
                    redirct = 0;
                    this.redirect(['/cataloguemanagement', this.customerName]);
                }
                this.imgName = this.getCustomerImage(params.orgType);
                this.custPermission = custPerm;
                this.orgimgUrl = orgImgUrl;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = this.customerName;
                this.disabledfilfilment = filfilment;
            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            // let i = 0;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);

            // logic for resize pop up 
            window.onresize = () => {
                if (screen == "desktop") {

                    if (window.innerWidth < this._globalFunc.stdWidth) {
                        // this.catalogueResult = false;

                    } else {
                        //  this.catalogueResult = true;
                    }
                } else {

                    if (window.innerWidth > this._globalFunc.stdWidth) {
                        //   this.catalogueResult = true;
                        document.getElementById("resultModalClose").click();
                    } else {
                        //  this.catalogueResult = false;

                    }
                }

                // get device to populate some part of ui
                screen = this._globalFunc.getDevice(window.innerWidth);

            };
            this.getValdiationMsgs(this.orgApiName);



            this.getfulfilmentDetailsTwo();

        }
    }

    getValdiationMsgs(orgName: any) {
        this._globalFunc.logger("Calling validation message service.");
        this._postsService.getOrderValidationMessageforperiodic(this.orgApiName).subscribe(data => {
            console.log(data);
            this.orderValidationmsgsList = data.validationList;
            console.log(this.orderValidationmsgsList + "order validation message");
            this.getfulfilmentDetailsTwo();
        }, error => {
            console.log(error);
        });
    }
    //getfulfilmentData
    getfulfilmentDetailsTwo() {
        var orderValidationList: any;
        var validationCode: string;
        var rangeFlag: boolean;
        var rangeerrorflag: boolean;
        var validationMessage: string;
        this.showLoader = true;

        if (this.showLoader == true) {
            this.fulfilmentdataLen = 999;
        }
        let shipToDeliverAt = 'shipToDeliverAt';
        this._postsService.getfulfilmentDetailsTwo(this.orgApiName, this.currDate, shipToDeliverAt, this.catagoryNameStr, this.uomNameStr, this.productId, this.start, this.shipFromLocationId, this.channel).subscribe(data => {
            this._globalFunc.logger("Fetching data for Fullfillment Drill Down 2");
            this.fulfilmentdataArr = data.fulfilmentList;
            this.fulfilmentdataLen = this.fulfilmentdataArr.length;

            let dummyArr = this.fulfilmentdataArr;
            orderValidationList = this.orderValidationmsgsList;
            // let that = this;
            this.fulfilmentdataArr.forEach(function (item: any, idx: number) {

                if (item.statuInfo.validation || item.statuInfo.validation !== undefined) {
                    let attributeList = item.statuInfo.validation.split(";");
                    var statValColor = "";
                    var statValText = "";
                    console.log(item);
                    //  var validationCodeMsg:string;
                    rangeerrorflag = true;
                    attributeList.forEach(function (attribute: any) {
                        let attributeInfo = attribute.split(":");

                        if (attributeInfo[1] == 0) {
                            if (attributeInfo[0] == "range" && attributeInfo[1] == 0) {
                                rangeFlag = true;
                            }
                            if (attributeInfo[0] == "active" && rangeFlag == true) {
                                rangeerrorflag = false;
                            }
                            if (attributeInfo[0] == "case" && rangeFlag == true) {
                                rangeerrorflag = false;
                            }
                            if (attributeInfo[0] == "price" && rangeFlag == true) {
                                rangeerrorflag = false;
                            }

                            if (attributeInfo[1] == 0) {
                                let validationMessageExist = false;
                                console.log(orderValidationList + "orderValidationList");
                                orderValidationList.forEach(function (validationList: any) {
                                    validationCode = validationList.validationCode;
                                    validationMessage = validationList.validationMessage;
                                    if (validationMessage == "" || validationMessage == undefined) {
                                        validationMessage = "Message is not configured";
                                    }

                                    if (attributeInfo[0] == validationCode) {
                                        validationMessageExist = true;
                                    }

                                    if (attributeInfo[0] == validationCode && rangeerrorflag == true) {
                                        statValText = statValText + attributeInfo[0] + "  :  " + validationMessage + ";" + "\n";
                                        statValColor = 'bg-red txtclr-white';
                                    }
                                });

                                if (!validationMessageExist) {
                                    statValText = statValText + attributeInfo[0] + " : Business friendly message has not been configured;" + "\n";
                                    statValColor = 'bg-red txtclr-white';
                                }
                            }
                        }
                    });


                    if (statValText == "") {
                        statValColor = 'bg-green txtclr-white';
                        statValText = 'OK';

                    }


                } else {
                    statValText = "-";
                    statValColor = 'bg-grey';
                }
                item.statValColor = statValColor;
                item.statValText = statValText;
                dummyArr[idx].statValColor = statValColor;
                dummyArr[idx].statuInfo.statValColor = statValColor;
                dummyArr[idx].statuInfo.statValText = statValText;

            });
            console.log(dummyArr);
            this.fulfilmentdataArr = dummyArr;
            console.log("final Arr");
            console.log(this.fulfilmentdataArr);
            this.showLoader = false;
            this.getTotalforrows(this.fulfilmentdataArr);
        }, error => {
            console.log(error);
            if (error.errorCode == '404.31.401') {
                this.fulfilmentdataLen = 0;
                this.showLoader = false;
            } else {
                this._router.navigate(['/error404']);
            }
        });
    }



    onScrollDown() {
        let shipToDeliverAt = 'shipToDeliverAt';
        this.start = this.start + this.limit;
        if (this.count == this.limit && this.hitNext == true) {
            this.showLoader = true;
            this._postsService.getfulfilmentDetailsTwo(this.orgApiName, this.currDate, shipToDeliverAt, this.catagoryName, this.uomName, this.productId, this.start, this.shipFromLocationId, this.channel).subscribe(data => {
                this.showLoader = false;
                this.count = data.paginationMetaData.count;
                this.fulfilmentdataArr = this.fulfilmentdataArr.concat(data.fulfilmentdataArr);
            }, err => {
                this.catalogueno = '';
                this.showLoader = false;
                this.cataloguedetailListLength = 0;
                this._router.navigate(['/error404']);
            }
            );

        }

    }

    toggleArrow(divId: string) {
        document.getElementById(divId).classList.toggle("glyphicon-chevron-right");
        document.getElementById(divId).classList.toggle("glyphicon-chevron-left");
    }

    toggleTableColumns(tableId: string) {
        switch (tableId) {
            case "Order":
                if (this.orderHide == true) {
                    this.identifierOrderSize = 3;
                    this.orderHide = false;
                    this.yellow_border_right_salesOrder = '';
                } else {
                    this.identifierOrderSize = 1;
                    this.orderHide = true;
                    this.yellow_border_right_salesOrder = 'yellow-border-right';
                }
                this.toggleArrow('order_table');
                break;

            case "Product":
                if (this.productHide == true) {
                    this.identifierProductSize = 5;
                    this.productHide = false;
                } else {
                    this.identifierProductSize = 3;
                    this.productHide = true;
                }
                this.toggleArrow('product_table');
                break;

            case "Quantity":
                if (this.quantityHide == true) {
                    this.identifierquantitySize = 6;
                    this.quantityHide = false;
                    this.yellow_border_right_ordered = '';
                } else {
                    this.identifierquantitySize = 2;
                    this.quantityHide = true;
                    this.yellow_border_right_ordered = 'yellow-border-right';
                }
                this.toggleArrow('quantity_table');
                break;

            case "Price":
                if (this.priceHide == true) {
                    this.identifierpriceSize = 4;
                    this.priceHide = false;
                    this.yellow_border_right_net = '';
                } else {
                    this.identifierpriceSize = 2;
                    this.priceHide = true;
                    this.yellow_border_right_net = 'yellow-border-right';
                }
                this.toggleArrow('price_table');
                break;

            case "Status":
                if (this.statusHide == true) {
                    this.identifierstatusSize = 4;
                    this.statusHide = false;
                } else {
                    this.identifierstatusSize = 1;
                    this.statusHide = true;
                }
                this.toggleArrow('status_table');
                break;
        }
    }

    //printdata fro drilldown 2
    printdata() {
        this.export = false;
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        //var printContents = document.getElementById('exportable').innerHTML;
        //var printContents = "<table border=1>" + document.getElementById('fulfulmentleveltwo').innerHTML + "<table>";
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + '</body></html>');
            popup.document.close();

        }
    }

    exportData() {
        this.export = true;
        // return this._commFuc.exportData('tableContainer', this.customerName + 'ffDashboardLevelone');
        var blob = new Blob([document.getElementById('exportHeader').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, this.customerName + "_FulfilmentDrilldownLevelTwo.xls");

    }

    // navigate function
    redirect(page: any) {
        if (page == 'addproduct') {
            let url = this.homeUrl + 'catalogue/' + this.customerName + '/add';
            window.open(url);
        } else {
            this._router.navigate(page);
        }
    }

    goToLevelOne(url: any) {
        console.log("Url of: " + url)
        this._router.navigate([url[0], url[1], url[2], url[3], url[4], url[5], url[6]]);
    }

    // change organisation
    changeOrgType(page: any, orgType: any) {

        // reset field on change of organisation 
        this.catalogueno = '';
        this.catalogueList = [];
        this.catalogueListLength = 0;
        this.catalogueResult = true;
        this.catalogueType = '';
        this.catalogueCode = '';
        this.hitNext = false;
        this.start = 0;
        this.count = 0;
        this.toggleOrganisation();
        this._router.navigate([page, orgType]);


    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
        this._globalFunc.logger("Fullfillment Drill Down 2  screen unloading");
        this._globalFunc.sentLoggerBatchToServer();
    }
    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    // change organisation div show and hide
    toggleOrganisation() {

        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
        }
        else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
        }
    }

    //tooltip for validation messageCreatedAt
    formatSelection(val: any) {
        console.log(val);
        if (val != null) {
            return this._globalFunc.formatSelection(val);
        } else {
            return '-';
        }
    }
}
