import { NgModule, ErrorHandler } from '@angular/core';
import { FulfilmentlvltwoRoutes } from './fulfilmentlvltwo.routes';
import { FulfilmentlvltwoComponent } from './fulfilmentlvltwo.compoment';

import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../../global/global.component';
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../../../navigationFulfilment/navigation.module';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { TooltipModule } from "ngx-tooltip";


@NgModule({
  imports: [
    FulfilmentlvltwoRoutes,
    CommonModule,
    FormsModule,
    NavigationModule,
    InfiniteScrollModule,
    TooltipModule
  ],
  exports: [],
  declarations: [FulfilmentlvltwoComponent],
  providers: [Services, GlobalComponent, commonfunction]

})
export class FulfilmentlvltwoModule { }
