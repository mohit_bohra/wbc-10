import { Component } from '@angular/core';
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { GlobalComponent } from '../../../global/global.component';
import { Constant } from '../../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { document } from '../../../datepicker/utils/facade/browser';
declare let saveAs: any;

@Component({
    templateUrl: 'fulfilmentlvlone.component.html',
})
export class FulfilmentlvloneComponent {
    shipFromLocationdescp: string;
    shiptotalLines: any;
    shiptotalQty: any;
    confirmTotalLines: any;
    confirmTotalQty: any;
    rejectTotalQty: any;
    orderlinetotal: any;
    orderQtytotal: any;
    public catalgoueDate: Date = new Date();
    objConstant = new Constant();
    whoImgUrl: any;
    catalogueCode: any;
    catalogueCodeSelected: any;
    catalogueList: any;
    catalogueListLength: any;
    catalogueUpdatedDate: any;
    catalogueno: string;
    showLoader: boolean;
    hideOrganisation: boolean = true;
    catalogueResult: boolean = true;
    printHeader: boolean = true;
    pageState: number = 15;
    customerName: string;
    catagoryName: string;
    catagoryNameStr: string;
    uomName: string;
    uomNameStr: string;
    customerDispayName: string;
    style: string;
    organisationList: any;
    organisationListChange: any;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disabledOrder: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    private _sub: any;
    disabledExportCatalogue: boolean;
    disabledUploadCatalogue: boolean;
    disabledfilfilment: boolean;
    currDate: any;

    //for catalogue Enquiry redevelopment
    catalogueTypes: any;
    catalogueType: string;
    cataloguemasterListLength: any;
    cataloguedetailListLength: any;
    catalogueDetailList: any;
    strInfoNotAvail: String;
    start: number;
    limit: number;
    count: number;
    hitNext: boolean;
    fulfilmentdataArr: any[];
    fulfilmentdataLen: number = 0;
    shipFromLocationId: any;

    public todayDate: Date = new Date();

    homeUrl: any;
    custPermission: any[];

    msgJSON: any;
    imgName: string = "";
    channel: any;

    hideLogo: boolean = false;
    constructor(private _commFuc: commonfunction, private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router) {
        // img urls
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        this.orderQtytotal = 0;
        this.orderlinetotal = 0;
        this.rejectTotalQty = 0;
        this.confirmTotalQty = 0;
        this.confirmTotalLines = 0;
        this.shiptotalQty = 0;
        this.shiptotalLines = 0;

    }

    // function works on page load
    ngOnInit() {
        this._globalFunc.logger("Fullfillment Drill Down 1 Screen loading...");
        console.log(this._router.url);
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {
            this.msgJSON = {
                "width314": true,
                "title": "Session terminated",
                "message": '',
                "okButton": true,
                "okButtonLabel": "OK",
                "cancelButton": false,
                "cancelButtonLabel": "",
                "showPopup": false
            };

            // this.showLoader = false;
            this.catalogueno = '';
            // user permission
            this.disabledCusChange = false;
            this.disabledfilfilment = false;
            this.strInfoNotAvail = "-";
            this.catalogueType = "";

            // window scroll function
            this._globalFunc.autoscroll();

            // set header basket data
            this.start = 0;
            this.limit = this.objConstant.SCROLL_RECORD_LIMIT;
            this.count = 0;
            this.hitNext = false;
            sessionStorage.setItem('pageState', '999');
            document.getElementById('diableLogo').style.cursor = "not-allowed";
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string, catagory: string, uom: string, currDate: any, channel: any }) => {
                console.log(this._router.url);
                console.log(params);
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;

                console.log(sessionStorage.getItem('catagoryName'));
                if (sessionStorage.getItem('catagoryName') == null || sessionStorage.getItem('catagoryName') == 'undefined') {
                    sessionStorage.setItem('catagoryName', params.catagory);
                }
                this.catagoryName = sessionStorage.getItem('catagoryName');
                this.shipFromLocationId = sessionStorage.getItem('shipFromLocationId');
                this.shipFromLocationdescp = sessionStorage.getItem('shipFromLocationdescp');
                this.catagoryNameStr = sessionStorage.getItem('catagoryNameStr');

                if (sessionStorage.getItem('uomName') == null || sessionStorage.getItem('uomName') == 'undefined') {
                    sessionStorage.setItem('uomName', params.uom);
                }
                this.uomName = sessionStorage.getItem('uomName');
                this.uomNameStr = sessionStorage.getItem('uomNameStr');

                if (sessionStorage.getItem('channel') == null || sessionStorage.getItem('channel') == 'undefined') {
                    sessionStorage.setItem('channel', params.channel);
                }
                this.channel = sessionStorage.getItem('channel');


                console.log(this.objConstant.tactocal);
                if (sessionStorage.getItem('currDate') == null || sessionStorage.getItem('currDate') == 'undefined') {
                    sessionStorage.setItem('currDate', params.currDate);
                }
                this.currDate = sessionStorage.getItem('currDate');

                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                let reports;
                let orders;
                let catalogues;
                let cataloguesRaise;
                let uploadCatalogue;
                let exportCatalogue;
                let filfilment;
                let accessMgmt;
                let redirct;
                let changeOrgAvailable = new Array;
                let catManagement;
                // list of org assigned to user
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                this.orgApiName = this.customerName;

                document.getElementById('orderArrowSpan').style.display = 'block';
                document.getElementById('orderMob').style.display = 'block';
                document.getElementById('orderRaiseMob').style.display = 'none';

                document.getElementById('catalogueMob').style.display = 'block';
                document.getElementById('catalogueArrowSpan').style.display = 'none';
                document.getElementById('catalogueRaiseMob').style.display = 'none';
                document.getElementById('reportsMob').style.display = 'block';
                document.getElementById('permissonsMob').style.display = 'block';

                if (document.getElementById('orderArrow').classList.contains('glyphicon-menu-up') === true) {
                    let sublinks = document.getElementById('orderArrow');
                    sublinks.classList.remove('glyphicon-menu-up');
                    sublinks.classList.add('glyphicon-menu-down');
                    document.getElementById('orderRaiseMob').style.display = 'none';
                }

                if (document.getElementById('catalogueArrow').classList.contains('glyphicon-menu-up') === true) {
                    let sublinks = document.getElementById('catalogueArrow');
                    sublinks.classList.remove('glyphicon-menu-up');
                    sublinks.classList.add('glyphicon-menu-down');
                    document.getElementById('catalogueRaiseMob').style.display = 'none';
                }

                let custPerm: any = [];


                if (redirct == 1) {
                    redirct = 0;
                    this.redirect(['/cataloguemanagement', this.customerName]);
                }

                this.custPermission = custPerm;
                this.orgimgUrl = orgImgUrl;
                this.imgName = this.getCustomerImage(params.orgType);
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = this.customerName;


                this.orgApiName = this.customerName;
                this.disabledfilfilment = filfilment;
            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            // let i = 0;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);

            // logic for resize pop up 
            window.onresize = () => {
                if (screen == "desktop") {

                    if (window.innerWidth < this._globalFunc.stdWidth) {
                        // this.catalogueResult = false;

                    } else {
                        //  this.catalogueResult = true;
                    }
                } else {

                    if (window.innerWidth > this._globalFunc.stdWidth) {
                        //   this.catalogueResult = true;
                        document.getElementById("resultModalClose").click();
                    } else {
                        //  this.catalogueResult = false;

                    }
                }

                // get device to populate some part of ui
                screen = this._globalFunc.getDevice(window.innerWidth);

            };
            if (this.customerName != 'amazon') {
                this.getfulfilmentDetailsData(this.catagoryName, this.uomName);
            }
            if (this.customerName == 'amazon') {
                this.getfulfilmentDetailsDataforAmazon(this.catagoryName, this.uomName, this.channel);
            }
        }
    }

    //getfulfilmentData
    getfulfilmentDetailsData(catagoryNamestr: any, uomNameStr: any) {
        this.showLoader = true;
        if (this.showLoader == true) {
            this.fulfilmentdataLen = 999;
        }
        let shipToDeliverAt = 'shipToDeliverAt';
        console.log(this._commFuc.dateFormator(this.todayDate.toDateString(), 6));
        console.log(this.currDate);
        console.log("ABC : " + this.uomNameStr);
        this._postsService.getfulfilmentDetailsData(this.orgApiName, this.currDate, shipToDeliverAt, this.catagoryNameStr, this.uomNameStr, this.shipFromLocationId).subscribe(data => {
            this._globalFunc.logger("Fetching data for Fullfillment Drill Down 1");
            console.log(data);
            this.fulfilmentdataArr = data.fulfilmentList;
            this.fulfilmentdataLen = this.fulfilmentdataArr.length;
            console.log(this.fulfilmentdataArr);
            this.getTotalofAll(this.fulfilmentdataArr, 'get');
            this.showLoader = false;
        }, error => {
            console.log(error);
            if (error.errorCode == '404.31.401') {
                this.fulfilmentdataLen = 0;
                this.showLoader = false;
            } else {
                this._router.navigate(['/error404']);
            }
        });
    }


    getfulfilmentDetailsDataforAmazon(catagoryNamestr: any, uomNameStr: any, channel: any) {
        this.showLoader = true;
        if (this.showLoader == true) {
            this.fulfilmentdataLen = 999;
        }
        let shipToDeliverAt = 'shipToDeliverAt';
        console.log(this._commFuc.dateFormator(this.todayDate.toDateString(), 6));
        console.log(this.currDate);
        console.log("ABC : " + this.uomNameStr);
        this._postsService.getfulfilmentDetailsDataforAmazon(this.orgApiName, this.currDate, shipToDeliverAt, this.catagoryNameStr, this.uomNameStr, this.shipFromLocationId, this.channel).subscribe(data => {
            this._globalFunc.logger("Fetching data for Fullfillment Drill Down 1");
            console.log(data);
            this.fulfilmentdataArr = data.fulfilmentList;
            this.fulfilmentdataLen = this.fulfilmentdataArr.length;
            console.log(this.fulfilmentdataArr);
            this.getTotalofAll(this.fulfilmentdataArr, 'get');
            this.showLoader = false;
        }, error => {
            console.log(error);
            if (error.errorCode == '404.31.401') {
                this.fulfilmentdataLen = 0;
                this.showLoader = false;
            } else {
                this._router.navigate(['/error404']);
            }
        });
    }

    gotoLevelTwo(urlArr: any) {
        console.log((urlArr));

        var catastr = urlArr[4];
        console.log(catastr);
        let strCatArr = catastr.split("(");
        console.log(strCatArr.length);

        let strCat = strCatArr[0].trim();

        sessionStorage.setItem('catagoryNameStr', catastr);
        sessionStorage.setItem('productDescription', urlArr[9]);

        console.log(strCat);
        if (sessionStorage.getItem("catagoryName") !== null || sessionStorage.getItem("currDate") !== null || sessionStorage.getItem("uomName") !== null || sessionStorage.getItem("channel") != null) {
            sessionStorage.removeItem('catagoryName');
            sessionStorage.removeItem('uomName');
            sessionStorage.removeItem('currDate');
            //  sessionStorage.removeItem('channel');
        }
        var legacy = urlArr[5];
        sessionStorage.setItem('legacyId', legacy);

        var pin = urlArr[6];
        sessionStorage.setItem('pinNumber', pin);
        var channel = urlArr[10];
        sessionStorage.setItem('channel', channel);

        var uomstr = urlArr[7];
        console.log(uomstr);
        let struomArr = uomstr.split("(");
        let struom = struomArr[0].trim();
        sessionStorage.setItem('uomNameStr', uomstr);
        console.log(struom);

        this._postsService.validateUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).subscribe((userData: any) => {
            if (!userData.isSessionActive) { // false
                // User session has expired.

                if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                    console.log("Session terminated");
                    this.msgJSON = {
                        "width314": true,
                        "title": "Session terminated",
                        "message": userData.sessionMessage,
                        "okButton": true,
                        "okButtonLabel": "OK",
                        "cancelButton": false,
                        "cancelButtonLabel": "",
                        "showPopup": true
                    };

                    let dt = this._commFuc.getUserSession();
                    this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
                        console.log(userData);
                    }, (error: any) => {
                        console.log(error);
                    });

                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                }
                else {
                    sessionStorage.clear();
                    this._router.navigate(['']);
                }
            } else {
                if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                    console.log("Session terminated");
                    this.msgJSON = {
                        "width314": true,
                        "title": "Session terminated",
                        "message": userData.sessionMessage,
                        "okButton": true,
                        "okButtonLabel": "OK",
                        "cancelButton": false,
                        "cancelButtonLabel": "",
                        "showPopup": true
                    };

                    let dt = this._commFuc.getUserSession();
                    this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
                        console.log(userData);
                    }, (error: any) => {
                        console.log(error);
                    });

                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                } else {
                    // dashboard/mccolls/fulfilment/leveltwo/AM/CS/2018-05-02/100182489
                    // dashboard/mccolls/fulfilment/leveltwo/AM/CS/2018-05-02/100182489
                    let url = '';
                    sessionStorage.setItem("pageState", "999");
                    if (sessionStorage.getItem('orgType') == 'Amazon' || sessionStorage.getItem('orgType') == 'mccolls') {
                        url = this.homeUrl + urlArr[0] + '/' + urlArr[1] + '/' + urlArr[2] + '/' + urlArr[3] + '/' + strCat + '/' + struom + '/' + urlArr[8] + '/' + urlArr[6];
                        // this._router.navigate([urlArr[0],urlArr[1] ,urlArr[2],urlArr[3],strCat,struom,urlArr[8],urlArr[6]]);   
                    } else {
                        url = this.homeUrl + urlArr[0] + '/' + urlArr[1] + '/' + urlArr[2] + '/' + urlArr[3] + '/' + strCat + '/' + struom + '/' + urlArr[8] + '/' + urlArr[5];
                        // this._router.navigate([urlArr[0],urlArr[1] ,urlArr[2],urlArr[3],strCat,struom,urlArr[8],urlArr[5]]);
                    }
                    console.log(url);
                    window.open(url);
                }
            }
        });


    }

    getTotalofAll(jsonArry: any, status: any) {
        console.log(jsonArry);
        if (status == 'get') {
            jsonArry.forEach((items: any, index: any) => {
                console.log(items);
                console.log(index);
                console.log(items.orderRaised.totalQty);
                this.orderQtytotal = this.orderQtytotal + (items.orderRaised.totalQty ? items.orderRaised.totalQty : 0);
                this.orderlinetotal = this.orderlinetotal + (items.orderRaised.noOfLines ? items.orderRaised.noOfLines : 0);
                this.rejectTotalQty = this.rejectTotalQty + (items.orderRejected.totalQty ? items.orderRejected.totalQty : 0);
                this.confirmTotalQty = this.confirmTotalQty + (items.orderConfirmed.totalQty ? items.orderConfirmed.totalQty : 0);
                this.confirmTotalLines = this.confirmTotalLines + (items.orderConfirmed.noOfLines ? items.orderConfirmed.noOfLines : 0);
                this.shiptotalQty = this.shiptotalQty + (items.orderShipped.totalQty ? items.orderShipped.totalQty : 0);
                this.shiptotalLines = this.shiptotalLines + (items.orderShipped.noOfLines ? items.orderShipped.noOfLines : 0);
            });
        } else {
            this.orderQtytotal = 0;
            this.orderlinetotal = 0;
            this.rejectTotalQty = 0;
            this.confirmTotalQty = 0;
            this.confirmTotalLines = 0;
            this.shiptotalQty = 0;
            this.shiptotalLines = 0;
        }
        console.log(this.orderQtytotal);
    }

    //printdata
    printdata() {
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        //var printContents = "<table border=1>" + document.getElementById('fulfulmentlevelone').innerHTML + "<table>";
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + '</body></html>');
            popup.document.close();

        }
    }

    exportData() {
        var blob = new Blob([document.getElementById('exportHeader').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, this.customerName + "_FulfilmentDrilldownLevelone.xls");

    }

    // navigate function
    redirect(page: any) {
        if (page == 'addproduct') {
            let url = this.homeUrl + 'catalogue/' + this.customerName + '/add';
            window.open(url);
        } else {
            this._router.navigate(page);
        }
    }
    // change organisation
    changeOrgType(page: any, orgType: any) {

        // reset field on change of organisation 
        this.catalogueno = '';
        this.catalogueList = [];
        this.catalogueListLength = 0;
        this.catalogueResult = true;
        this.catalogueType = '';
        this.catalogueCode = '';
        this.hitNext = false;
        this.start = 0;
        this.count = 0;
        this.toggleOrganisation();
        this._router.navigate([page, orgType]);


    }
    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
        this._globalFunc.logger("Fullfillment Drill Down 1  screen unloading");
        this._globalFunc.sentLoggerBatchToServer();
    }
    // change organisation div show and hide
    toggleOrganisation() {
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('setBorder');
            border.classList.remove('searchborder');
        }
        else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('setBorder');
            border.classList.remove('searchborder');
        }
    }

    closePopup(popupName: string = 'popup_SessionExpired') {

        let sessionModal = document.getElementById(popupName);
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById(popupName).style.display = "none";
    }
}
