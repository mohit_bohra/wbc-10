import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FulfilmentlvloneComponent } from './fulfilmentlvlone.compoment';

// route Configuration
const fulfilmentlvloneRoutes: Routes = [
  { path: '', component: FulfilmentlvloneComponent }
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];

@NgModule({
  imports: [RouterModule.forChild(fulfilmentlvloneRoutes)],
  exports: [RouterModule]
})
export class FulfilmentlvloneRoutes { }
