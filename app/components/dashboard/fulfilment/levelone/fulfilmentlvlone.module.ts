import { NgModule } from '@angular/core';
import { FulfilmentlvloneRoutes } from './fulfilmentlvlone.routes';
import { FulfilmentlvloneComponent } from './fulfilmentlvlone.compoment';

import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../../../global/global.component';
import { Services } from '../../../../services/app.services';
import { commonfunction } from '../../../../services/commonfunction.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../../../navigationFulfilment/navigation.module';

@NgModule({
  imports: [
    FulfilmentlvloneRoutes,
    CommonModule,
    FormsModule,
    NavigationModule
  ],
  exports: [],
  declarations: [FulfilmentlvloneComponent],
  providers: [Services, GlobalComponent, commonfunction]
})
export class FulfilmentlvloneModule { }
