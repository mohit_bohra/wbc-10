import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FulfilmentComponent } from './fulfilment.compoment';

// route Configuration
const fulfilmentRoutes: Routes = [
  { path: '', component: FulfilmentComponent },
  { path: 'levelone', loadChildren:'./levelone/fulfilmentlvlone.module#FulfilmentlvloneModule'},
  { path: 'levelone/:catagory/:uom/:currDate', loadChildren:'./levelone/fulfilmentlvlone.module#FulfilmentlvloneModule'},
  { path: 'leveltwo', loadChildren:'./leveltwo/fulfilmentlvltwo.module#FulfilmentlvltwoModule'},
  { path: 'leveltwo/:catagory/:uom/:currDate/:legacyId', loadChildren:'./leveltwo/fulfilmentlvltwo.module#FulfilmentlvltwoModule'}
  //{ path: 'category/:name/:id/:isstorechanged', component: CategoryComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(fulfilmentRoutes)],
  exports: [RouterModule]
})
export class FulfilmentRoutes { }
