import { Component } from '@angular/core';
import { Services } from '../../../services/app.services';
import { commonfunction } from '../../../services/commonfunction.services';
import { GlobalComponent } from '../../global/global.component';
import { Constant } from '../../constants/constant';
import { Router, ActivatedRoute } from '@angular/router';
import { document } from '../../datepicker/utils/facade/browser';

@Component({
    templateUrl: 'fulfilment.component.html',
})
export class FulfilmentComponent {

    public showDatepicker: boolean = true;

    public catalgoueDate: Date = new Date();
    objConstant = new Constant();
    whoImgUrl: any;
    catalogueCode: any;
    catalogueCodeSelected: any;
    catalogueList: any;
    catalogueListLength: any;
    catalogueUpdatedDate: any;
    catalogueno: string;
    showLoader: boolean;
    hideOrganisation: boolean = true;
    catalogueResult: boolean = true;
    printHeader: boolean = true;
    pageState: number = 15;
    customerName: string;
    customerDispayName: string;
    style: string;
    organisationList: any;
    organisationListChange: any;
    orgimgUrl: string;
    orgApiName: string;
    disabledCusChange: boolean;
    disabledPermission: boolean;
    disabledReports: boolean;
    disabledCatalogue: boolean;
    disabledOrder: boolean;
    disabledOrderRaise: boolean;
    disabledCatalogueRaise: boolean;
    disabledManageCatalogue: boolean;
    private _sub: any;
    disabledExportCatalogue: boolean;
    disabledUploadCatalogue: boolean;
    disabledfilfilment: boolean;
    showFilterAttr: boolean;

    //for catalogue Enquiry redevelopment
    catalogueTypes: any;
    catalogueType: string;
    cataloguemasterListLength: any;
    cataloguedetailListLength: any;
    catalogueDetailList: any;
    strInfoNotAvail: String;
    start: number;
    limit: number;
    count: number;
    hitNext: boolean;
    fulfilmentdataArr: any[];
    fulfilmentdataLen: number = 0;
    currDate: any;
    searchType: any;
    shipFromLocationIdType: any;

    public todayDate: Date = new Date();

    homeUrl: any;
    custPermission: any[];
    shipFromLocationIDs: any = [];
    pendingCount: any;
    shipFromLocationId: any;
    channel: any;
    subtype: any;
    public deliverydate: Date = new Date();
    public maxDate: Date = new Date();

    //total to show All
    orderQtytotal: any;
    orderQtytotalTBXTB: any;
    orderlinetotal: any;
    orderlinetotalTBXTB: any;
    rejectTotalQty: any;
    rejectTotalQtyTBXTB: any;
    confirmTotalQty: any;
    confirmTotalQtyTBXTB: any;
    confirmTotalLines: any;
    confirmTotalLinesTBXTB: any;
    shiptotalQty: any;
    shiptotalQtyTBXTB: any;
    shiptotalLines: any;
    shiptotalLinesTBXTB: any;
    percentShipLines: any;
    percentShipLinesTBXTB: any;
    percentShipQty: any;
    percentShipQtyTBXTB: any;
    imgName: string = "";

    msgJSON: any;

    updateddate: any;
    channelSubTypeList: any = {};

    constructor(private _commFuc: commonfunction, private _postsService: Services, private _globalFunc: GlobalComponent, private _route: ActivatedRoute, private _router: Router) {
        // img urls
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.homeUrl = this.objConstant.HOME_URL;
        this.showLoader = false;
        this.deliverydate = new Date();
        this.maxDate = new Date();
        this.showFilterAttr = true;
        this.searchType = 'ShipToDeliverAt';
        this.orderQtytotal = 0;
        this.orderQtytotalTBXTB = 0;
        this.orderlinetotal = 0;
        this.orderlinetotalTBXTB = 0;
        this.rejectTotalQty = 0;
        this.rejectTotalQtyTBXTB = 0;
        this.confirmTotalQty = 0;
        this.confirmTotalQtyTBXTB = 0;
        this.confirmTotalLines = 0;
        this.confirmTotalLinesTBXTB = 0;
        this.shiptotalQty = 0;
        this.shiptotalQtyTBXTB = 0;
        this.shiptotalLines = 0;
        this.shiptotalLinesTBXTB = 0;
        this.percentShipLines = 0;
        this.percentShipLinesTBXTB = 0;
        this.percentShipQty = 0;
        this.percentShipQtyTBXTB = 0;
        this.pendingCount = 0;
        this.channelSubTypeList = this.objConstant.channelSubTypeList;

    }

    // function works on page load
    ngOnInit() {
        if (sessionStorage.getItem('name') == null || sessionStorage.getItem('name') == 'undefined') {
            this._router.navigate(['']);
        } else {

            this.msgJSON = {
                "width314": true,
                "title": "Session terminated",
                "message": '',
                "okButton": true,
                "okButtonLabel": "OK",
                "cancelButton": false,
                "cancelButtonLabel": "",
                "showPopup": false
            };

            // this.showLoader = false;
            this.catalogueno = '';
            // user permission
            this.disabledCusChange = false;
            this.disabledPermission = false;
            this.disabledReports = false;
            this.disabledCatalogue = false;
            this.disabledOrder = false;
            this.disabledOrderRaise = false;
            this.disabledCatalogueRaise = false;
            this.disabledManageCatalogue = false;
            this.disabledExportCatalogue = false;
            this.disabledUploadCatalogue = false;
            this.disabledfilfilment = false;
            this.strInfoNotAvail = "-";
            this.catalogueType = "";

            // window scroll function
            this._globalFunc.autoscroll();

            // set header basket data
            this.start = 0;
            this.limit = this.objConstant.SCROLL_RECORD_LIMIT;
            this.count = 0;
            this.hitNext = false;
            sessionStorage.setItem('pageState', '15');
            document.getElementById('login').style.display = 'inline';
            (<HTMLLabelElement>document.getElementById('username')).textContent = this.objConstant.NOTATION + sessionStorage.getItem('name');

            // subscribe to route params
            this._sub = this._route.params.subscribe((params: { orgType: string }) => {
                sessionStorage.setItem('orgType', params.orgType);
                this.customerName = params.orgType;
                this.customerDispayName = (params.orgType == 'mccolls') ? 'McColls' : params.orgType;
                if (this.customerName == 'mccolls') {
                    this.style = 'margin-zero';
                } else {
                    this.style = 'margin-zero product-title-top';
                }
                let cusType = params.orgType;
                let orgImgUrl;
                let orgApiNameTemp;
                // let reports;
                // let orders;
                // let catalogues;
                // let cataloguesRaise;
                // let uploadCatalogue;
                // let exportCatalogue;
                // let filfilment;
                // let accessMgmt;
                // let redirct;
                // let changeOrgAvailable = new Array;
                // let catManagement;
                // list of org assigned to user
                // this.organisationList = JSON.parse(sessionStorage.getItem('organisation'));

                // this.organisationList.forEach(function (item: any) {
                //     if (item.orgType !== cusType) {
                //         changeOrgAvailable.push(item);
                //     }
                // });
                // this.organisationListChange = changeOrgAvailable;
                // // this.disabledCusChange = false;
                // if (this.organisationList.length == 1) {
                //     this.disabledCusChange = true;
                // }

                // document.getElementById('orderArrowSpan').style.display = 'block';
                // document.getElementById('orderMob').style.display = 'block';
                // document.getElementById('orderRaiseMob').style.display = 'none';

                // document.getElementById('catalogueMob').style.display = 'block';
                // document.getElementById('catalogueArrowSpan').style.display = 'none';
                // document.getElementById('catalogueRaiseMob').style.display = 'none';
                // document.getElementById('reportsMob').style.display = 'block';
                // document.getElementById('permissonsMob').style.display = 'block';

                // if (document.getElementById('orderArrow').classList.contains('glyphicon-menu-up') === true) {
                //     let sublinks = document.getElementById('orderArrow');
                //     sublinks.classList.remove('glyphicon-menu-up');
                //     sublinks.classList.add('glyphicon-menu-down');
                //     document.getElementById('orderRaiseMob').style.display = 'none';
                // }

                // if (document.getElementById('catalogueArrow').classList.contains('glyphicon-menu-up') === true) {
                //     let sublinks = document.getElementById('catalogueArrow');
                //     sublinks.classList.remove('glyphicon-menu-up');
                //     sublinks.classList.add('glyphicon-menu-down');
                //     document.getElementById('catalogueRaiseMob').style.display = 'none';
                // }

                // let custPerm: any = [];

                // this.organisationList.forEach(function (item: any) {
                //     if (item.orgType === cusType) {
                //         // logic for permission set
                //         orgImgUrl = item.imgUrl;
                //         orgApiNameTemp = item.orgName;

                //         if ((item.permissions.menuPermission.orders).toLowerCase() == 'none') {
                //             orders = true;
                //             document.getElementById('orderMob').style.display = 'none';
                //             document.getElementById('orderRaiseMob').style.display = 'none';
                //         }

                //         if ((item.permissions.menuPermission.raiseNewOrder).toLowerCase() == 'none') {
                //             document.getElementById('orderArrowSpan').style.display = 'none';
                //             document.getElementById('orderRaiseMob').style.display = 'none';

                //         }

                //         if ((item.permissions.menuPermission.catalogueManagement).toLowerCase() == 'none') {
                //             catManagement = true;
                //         } else {
                //             catManagement = false;
                //         }


                //         if ((item.permissions.menuPermission.catalogueEnquiry).toLowerCase() == 'none') {

                //             catalogues = true;
                //             document.getElementById('catalogueMob').style.display = 'none';
                //             document.getElementById('catalogueRaiseMob').style.display = 'none';
                //             redirct = 1;
                //             if (sessionStorage.getItem('page_redirect') == '0') {
                //                 sessionStorage.removeItem('page_redirect');
                //             } else {
                //                 sessionStorage.setItem('page_redirect', '1');
                //             }


                //         } else {
                //             // write logic to show add new product

                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.catalogAdminPermission.addNewProducts).toLowerCase() == 'none') {
                //             cataloguesRaise = true;
                //             document.getElementById('catalogueArrowSpan').style.display = 'none';
                //             document.getElementById('catalogueRaiseMob').style.display = 'none';


                //         } else {
                //             // write logic to show add new product

                //             //document.getElementById('catalogueRaiseMob').style.display = 'block';
                //         }

                //         if ((item.permissions.catalogAdminPermission.uploadCatalogue).toLowerCase() == 'none') {
                //             uploadCatalogue = true;
                //         } else {
                //             uploadCatalogue = false;
                //         }
                //         // item.permissions.catalogAdminPermission.exportCatalogue = 'W';
                //         if ((item.permissions.catalogAdminPermission.exportCatalogue).toLowerCase() == 'none') {
                //             exportCatalogue = true;
                //         } else {
                //             exportCatalogue = false;
                //         }

                //         console.log(item.permissions.dashboardsPermission.fulfilment);
                //         if ((item.permissions.dashboardsPermission.fulfilment).toLowerCase() == 'none') {
                //             filfilment = true;
                //         } else {
                //             filfilment = false;
                //         }

                //         if ((item.permissions.menuPermission.reports).toLowerCase() == 'none') {
                //             reports = true;
                //             document.getElementById('reportsMob').style.display = 'none';

                //         }

                //         if ((item.permissions.menuPermission.accessMgmt).toLowerCase() == 'none') {
                //             accessMgmt = true;
                //             document.getElementById('permissonsMob').style.display = 'none';
                //         }

                //     }

                //     /* Check atleast permission should exist for customer otherwise hide customer */
                //     if (item.permissions.menuPermission.catalogueEnquiry == 'none' &&
                //         item.permissions.menuPermission.catalogueManagement == 'none' &&
                //         item.permissions.menuPermission.orders == 'none' &&
                //         item.permissions.menuPermission.reports == 'none' &&
                //         item.permissions.menuPermission.accessMgmt == 'none') {
                //         custPerm[item.orgType] = false;
                //     } else {
                //         custPerm[item.orgType] = true;
                //     }
                //     /* Check atleast permission should exist for customer otherwise hide customer - END */
                // });

                // if (redirct == 1) {
                //     redirct = 0;
                //     this.redirect(['/cataloguemanagement', this.customerName]);
                // }

                //this.custPermission = custPerm;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = this.customerName;
                this.imgName = this.getCustomerImage(params.orgType);
                this.getUserSession(cusType, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));
                console.log("aa");
                // this.disabledReports = reports;
                // this.disabledOrder = orders;
                // this.disabledOrderRaise = orders;
                // this.disabledCatalogue = catalogues;
                // this.disabledCatalogueRaise = cataloguesRaise;
                // this.disabledPermission = accessMgmt;
                // this.disabledManageCatalogue = catManagement;
                // this.disabledUploadCatalogue = uploadCatalogue;
                // this.disabledExportCatalogue = exportCatalogue;
                // this.disabledfilfilment = filfilment;h
                if (this.customerName != 'amazon') {
                    this.getfulfilmentData();
                }
                if (this.customerName == 'amazon') {
                    this.getfulfilmentDataforAmazon();
                }
                this.getShipFromLocationIdList();
            });

            // initial page width
            let wid: any;
            wid = window.innerWidth;
            // let i = 0;
            let screen: string;

            // get device to populate some part of ui
            screen = this._globalFunc.getDevice(wid);

            // logic for resize pop up 
            window.onresize = () => {
                if (screen == "desktop") {

                    if (window.innerWidth < this._globalFunc.stdWidth) {
                        // this.catalogueResult = false;

                    } else {
                        //  this.catalogueResult = true;
                    }
                } else {

                    if (window.innerWidth > this._globalFunc.stdWidth) {
                        //   this.catalogueResult = true;
                        document.getElementById("resultModalClose").click();
                    } else {
                        //  this.catalogueResult = false;

                    }
                }

                // get device to populate some part of ui
                screen = this._globalFunc.getDevice(window.innerWidth);

            };
            console.log("bb");

        }
    }

    showPopup() {
        if (this.showDatepicker == true) {
            this.showDatepicker = false;
        } else {
            this.showDatepicker = true;
        }
    }

    updatedate() {
        let sessionModal = document.getElementById('updateDate');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('updateDate').style.display = 'block';
    }
    onSelectionDone(e: any) {
        let shipToDeliverAt = 'shipToDeliverAt';
        let getAllShipFromLocationId = 'getAllShipFromLocationId';
        this.deliverydate = e;
        let selectedDate = this._commFuc.dateFormator(e.toDateString(), 6);
        sessionStorage.setItem('currDate', selectedDate);
        if (this.deliverydate == null || this.deliverydate == undefined) {
            this.showDatepicker = false;
        } else {
            this.showDatepicker = true;
        }
    }

    clearDate() {
        this.deliverydate = null;
        this.showDatepicker = true;
    }

    onChange(value: any) {
        if (value.target.value == 'ShipToDeliverAt') {
            this.showFilterAttr = true;
            this.currDate = new Date();
            this.currDate = this._commFuc.dateFormator(this.currDate.toDateString(), 6);
            sessionStorage.setItem('currDate', this.currDate);
            this.deliverydate = new Date();

        } else if (value.target.value == 'ShipFromLocation') {
            this.showFilterAttr = false;
        }
    }

    closePopup(popupName: string = 'updateDate') {

        let sessionModal = document.getElementById(popupName);
        sessionModal.classList.remove('out');
        sessionModal.classList.add('in');
        document.getElementById(popupName).style.display = "none";
    }
    //Returns data when fulfillment is loaded for the first time
    getfulfilmentData() {
        console.log("cc");
        let shipFromLocationId = 'shipFromLocationId';
        this.shipFromLocationIdType = 'All';
        this.shipFromLocationId = this.shipFromLocationIdType;
        this.showLoader = true;
        if (this.showLoader == true) {
            this.fulfilmentdataLen = 999;
        }

        sessionStorage.setItem('shipFromLocationId', 'All');
        sessionStorage.setItem('shipFromLocationdescp', '');
        let shipToDeliverAt = 'shipToDeliverAt';
        this.currDate = this._commFuc.dateFormator(this.todayDate.toDateString(), 6);
        sessionStorage.setItem('currDate', this.currDate);
        console.log("dd");
        this._postsService.getfulfilmentData(this.orgApiName, this.currDate, shipToDeliverAt, shipFromLocationId).subscribe((data: any) => {
            this._globalFunc.logger("Fetching data for Fullfillment Drill Down 0");
            this.pendingCount = data.pendingCount;
            console.log(data);
            this.fulfilmentdataArr = data.fulfilmentList;
            console.log(this.fulfilmentdataArr);
            this.fulfilmentdataLen = this.fulfilmentdataArr.length;
            this.showLoader = false;
            this.getTotalofAll(this.fulfilmentdataArr, 'get');

            this.getTotalofAllExcludingTbXtb(this.fulfilmentdataArr, 'get');
            this.updateddate = data.updatedDate;
            console.log(this.updateddate + "updatedDate");

        }, (error: any) => {
            if (error.errorCode == '404.31.401') {
                this.fulfilmentdataLen = 0;
                this.showLoader = false;
                this.pendingCount = 0;
                this.getTotalofAll(this.fulfilmentdataArr, 'reset');
                this.getTotalofAllExcludingTbXtb(this.fulfilmentdataArr, 'reset');
            } else {
                this._router.navigate(['/error404']);
            }
        });
    }
    //get data for amazon on load.
    getfulfilmentDataforAmazon() {
        console.log("cc");
        let shipFromLocationId = 'shipFromLocationId';
        this.shipFromLocationIdType = 'All';
        this.shipFromLocationId = this.shipFromLocationIdType;
        this.channel = "All";
        this.subtype = "All"
        this.showLoader = true;
        if (this.showLoader == true) {
            this.fulfilmentdataLen = 999;
        }

        sessionStorage.setItem('shipFromLocationId', 'All');
        sessionStorage.setItem('shipFromLocationdescp', '');
        let shipToDeliverAt = 'shipToDeliverAt';
        this.currDate = this._commFuc.dateFormator(this.todayDate.toDateString(), 6);
        sessionStorage.setItem('currDate', this.currDate);
        console.log("dd");
        this._postsService.getfulfilmentDataforAmazon(this.orgApiName, this.currDate, shipToDeliverAt, shipFromLocationId, this.channel).subscribe((data: any) => {
            this._globalFunc.logger("Fetching data for Fullfillment Drill Down 0");
            this.pendingCount = data.pendingCount;
            console.log(data);
            this.fulfilmentdataArr = data.fulfilmentList;
            console.log(this.fulfilmentdataArr);
            this.fulfilmentdataLen = this.fulfilmentdataArr.length;
            this.showLoader = false;
            this.getTotalofAll(this.fulfilmentdataArr, 'get');

            this.getTotalofAllExcludingTbXtb(this.fulfilmentdataArr, 'get');
            this.updateddate = data.updatedDate;
            console.log(this.updateddate + "updatedDate");

        }, (error: any) => {
            if (error.errorCode == '404.31.401') {
                this.fulfilmentdataLen = 0;
                this.showLoader = false;
                this.pendingCount = 0;
                this.getTotalofAll(this.fulfilmentdataArr, 'reset');
                this.getTotalofAllExcludingTbXtb(this.fulfilmentdataArr, 'reset');
            } else {
                this._router.navigate(['/error404']);
            }
        });
    }
    getTotalofAll(jsonArry: any, status: any) {
        console.log(jsonArry);
        this.orderQtytotal = 0;
        this.orderlinetotal = 0;
        this.rejectTotalQty = 0;
        this.confirmTotalQty = 0;
        this.confirmTotalLines = 0;
        this.shiptotalQty = 0;
        this.shiptotalLines = 0;
        this.percentShipLines = 0;
        this.percentShipQty = 0;
        if (status == 'get') {

            jsonArry.forEach((items: any, index: any) => {
                console.log(items);
                console.log(index);
                this.orderQtytotal = this.orderQtytotal + (items.orderRaised.totalQty ? items.orderRaised.totalQty : 0);
                this.orderlinetotal = this.orderlinetotal + (items.orderRaised.noOfLines ? items.orderRaised.noOfLines : 0);
                this.rejectTotalQty = this.rejectTotalQty + (items.orderRejected.totalQty ? items.orderRejected.totalQty : 0);
                this.confirmTotalQty = this.confirmTotalQty + (items.orderConfirmed.totalQty ? items.orderConfirmed.totalQty : 0);
                this.confirmTotalLines = this.confirmTotalLines + (items.orderConfirmed.noOfLines ? items.orderConfirmed.noOfLines : 0);
                this.shiptotalQty = this.shiptotalQty + (items.orderShipped.totalQty ? items.orderShipped.totalQty : 0);
                this.shiptotalLines = this.shiptotalLines + (items.orderShipped.noOfLines ? items.orderShipped.noOfLines : 0);
                /*if (items.orderRaised.noOfLines > 0 && (items.orderRaised.noOfLines != undefined || items.orderRaised.noOfLines != '')) {
                    if (items.orderShipped.noOfLines > 0 && (items.orderShipped.noOfLines != undefined || items.orderShipped.noOfLines != '')) {
                        this.percentShipLines = this.percentShipLines + (((items.orderShipped.noOfLines) / items.orderRaised.noOfLines) * 100);
                    } else {
                        this.percentShipLines = this.percentShipLines + 0;
                    }
                }

                if (items.orderRaised.totalQty > 0 && (items.orderRaised.totalQty != undefined || items.orderRaised.totalQty != '')) {
                    if (items.orderShipped.totalQty > 0 && (items.orderShipped.totalQty != undefined || items.orderShipped.totalQty != '')) {
                        this.percentShipQty = this.percentShipQty + (((items.orderShipped.totalQty) / items.orderRaised.totalQty) * 100);
                    } else {
                        this.percentShipQty = this.percentShipQty + 0;
                    }
            }*/
            });

            this.percentShipLines = (this.shiptotalLines / this.orderlinetotal) * 100;
            this.percentShipQty = (this.shiptotalQty / this.orderQtytotal) * 100;
            if (isNaN(this.percentShipLines)) {
                this.percentShipLines = 0;
            }

            if (isNaN(this.percentShipQty)) {
                this.percentShipQty = 0;
            }
        } else {
            this.orderQtytotal = 0;
            this.orderlinetotal = 0;
            this.rejectTotalQty = 0;
            this.confirmTotalQty = 0;
            this.confirmTotalLines = 0;
            this.shiptotalQty = 0;
            this.shiptotalLines = 0;
            this.percentShipLines = 0;
            this.percentShipQty = 0;
        }
    }



    getTotalofAllExcludingTbXtb(jsonArry: any, status: any) {
        console.log(jsonArry);
        this.orderQtytotalTBXTB = 0;
        this.orderlinetotalTBXTB = 0;
        this.rejectTotalQtyTBXTB = 0;
        this.confirmTotalQtyTBXTB = 0;
        this.confirmTotalLinesTBXTB = 0;
        this.shiptotalQtyTBXTB = 0;
        this.shiptotalLinesTBXTB = 0;
        this.percentShipLinesTBXTB = 0;
        this.percentShipQtyTBXTB = 0;
        if (status == 'get') {

            jsonArry.forEach((items: any, index: any) => {
                console.log(items);
                console.log(index);
                if (items.category !== 'TB(Tobacco)' && items.category !== 'XTB') {
                    console.log(items.category + "(items.category");
                    this.orderQtytotalTBXTB = this.orderQtytotalTBXTB + (items.orderRaised.totalQty ? items.orderRaised.totalQty : 0);
                    this.orderlinetotalTBXTB = this.orderlinetotalTBXTB + (items.orderRaised.noOfLines ? items.orderRaised.noOfLines : 0);
                    this.rejectTotalQtyTBXTB = this.rejectTotalQtyTBXTB + (items.orderRejected.totalQty ? items.orderRejected.totalQty : 0);
                    this.confirmTotalQtyTBXTB = this.confirmTotalQtyTBXTB + (items.orderConfirmed.totalQty ? items.orderConfirmed.totalQty : 0);
                    this.confirmTotalLinesTBXTB = this.confirmTotalLinesTBXTB + (items.orderConfirmed.noOfLines ? items.orderConfirmed.noOfLines : 0);
                    this.shiptotalQtyTBXTB = this.shiptotalQtyTBXTB + (items.orderShipped.totalQty ? items.orderShipped.totalQty : 0);
                    this.shiptotalLinesTBXTB = this.shiptotalLinesTBXTB + (items.orderShipped.noOfLines ? items.orderShipped.noOfLines : 0);
                    /*if (items.orderRaised.noOfLines > 0 && (items.orderRaised.noOfLines != undefined || items.orderRaised.noOfLines != '')) {
                        if (items.orderShipped.noOfLines > 0 && (items.orderShipped.noOfLines != undefined || items.orderShipped.noOfLines != '')) {
                            this.percentShipLines = this.percentShipLines + (((items.orderShipped.noOfLines) / items.orderRaised.noOfLines) * 100);
                        } else {
                            this.percentShipLines = this.percentShipLines + 0;
                        }
                    }
    
                    if (items.orderRaised.totalQty > 0 && (items.orderRaised.totalQty != undefined || items.orderRaised.totalQty != '')) {
                        if (items.orderShipped.totalQty > 0 && (items.orderShipped.totalQty != undefined || items.orderShipped.totalQty != '')) {
                            this.percentShipQty = this.percentShipQty + (((items.orderShipped.totalQty) / items.orderRaised.totalQty) * 100);
                        } else {
                            this.percentShipQty = this.percentShipQty + 0;
                        }
                }*/
                }
            });

            this.percentShipLinesTBXTB = (this.shiptotalLinesTBXTB / this.orderlinetotalTBXTB) * 100;
            this.percentShipQtyTBXTB = (this.shiptotalQtyTBXTB / this.orderQtytotalTBXTB) * 100;
            if (isNaN(this.percentShipLinesTBXTB)) {
                this.percentShipLinesTBXTB = 0;
            }

            if (isNaN(this.percentShipQtyTBXTB)) {
                this.percentShipQtyTBXTB = 0;
            }

        } else {
            this.orderQtytotalTBXTB = 0;
            this.orderlinetotalTBXTB = 0;
            this.rejectTotalQtyTBXTB = 0;
            this.confirmTotalQtyTBXTB = 0;
            this.confirmTotalLinesTBXTB = 0;
            this.shiptotalQtyTBXTB = 0;
            this.shiptotalLinesTBXTB = 0;
            this.percentShipLinesTBXTB = 0;
            this.percentShipQtyTBXTB = 0;
        }
    }

    // Returns data when date and location id are passed
    getNewFulFillmentData(value: any) {
        if (this.customerName == 'amazon') {
            this.getNewFulFillmentDataforAmazon(value);
        }
        if (this.customerName != 'amazon') {
            this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));

            let shipFromLocationIdText = 'shipFromLocationId';
            this.showLoader = true;
            if (this.showLoader == true) {
                this.fulfilmentdataLen = 999;
            }
            let shipToDeliverAt = 'shipToDeliverAt';
            console.log('form value : ' + value.fulfillmentdatefrom);
            if (value.fulfillmentdatefrom == undefined) {
                this.currDate = new Date();
                this.currDate = this._commFuc.dateFormator(this.currDate.toDateString(), 6);
            } else {
                if (sessionStorage.getItem('currDate') == null) {
                    this.currDate = new Date();
                    this.currDate = this._commFuc.dateFormator(this.currDate.toDateString(), 6);
                    sessionStorage.setItem('currDate', this.currDate);
                }
                this.currDate = sessionStorage.getItem('currDate');
            }
            //this.currDate = this._commFuc.dateFormator(value.fulfillmentdateinput.toDateString(), 6);
            if (value.shipFromLocationIdType == undefined) {
                this.shipFromLocationId = "All";
                sessionStorage.setItem('shipFromLocationdescp', '');
            } else {
                console.log(JSON.stringify(value.shipFromLocationIdType));
                let shiplocArr = value.shipFromLocationIdType.split("-");
                console.log(shiplocArr[0])
                console.log(shiplocArr[1])
                if (this.shipFromLocationId != "All") {
                    this.shipFromLocationId = shiplocArr[0];
                }

                sessionStorage.setItem('shipFromLocationId', shiplocArr[0]);
                this.shipFromLocationId = shiplocArr[0];

                if (shiplocArr[1] == undefined) {
                    sessionStorage.setItem('shipFromLocationdescp', "");
                } else {
                    sessionStorage.setItem('shipFromLocationdescp', shiplocArr[1]);
                }
            }

            if (this.shipFromLocationId == undefined || this.shipFromLocationId == "") {
                this.shipFromLocationId = "All";
                sessionStorage.setItem('shipFromLocationdescp', '');
                sessionStorage.setItem('shipFromLocationId', this.shipFromLocationId);
            }

            this._postsService.validateUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).subscribe((userData: any) => {
                if (!userData.isSessionActive) { // false
                    // User session has expired.

                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                        console.log("Session terminated");
                        this.msgJSON = {
                            "width314": true,
                            "title": "Session terminated",
                            "message": userData.sessionMessage,
                            "okButton": true,
                            "okButtonLabel": "OK",
                            "cancelButton": false,
                            "cancelButtonLabel": "",
                            "showPopup": true
                        };

                        let dt = this._commFuc.getUserSession();
                        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
                            console.log(userData);
                        }, (error: any) => {
                            console.log(error);
                        });

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    }
                    else {
                        sessionStorage.clear();
                        this._router.navigate(['']);
                    }
                } else {
                    if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                        console.log("Session terminated");
                        this.msgJSON = {
                            "width314": true,
                            "title": "Session terminated",
                            "message": userData.sessionMessage,
                            "okButton": true,
                            "okButtonLabel": "OK",
                            "cancelButton": false,
                            "cancelButtonLabel": "",
                            "showPopup": true
                        };

                        let dt = this._commFuc.getUserSession();
                        this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
                            console.log(userData);
                        }, (error: any) => {
                            console.log(error);
                        });

                        let sessionModal = document.getElementById('popup_SessionExpired');
                        sessionModal.classList.remove('in');
                        sessionModal.classList.add('out');
                        document.getElementById('popup_SessionExpired').style.display = 'block';
                    } else {
                        this._postsService.getNewFulFillmentData(this.orgApiName, this.currDate, shipToDeliverAt, shipFromLocationIdText, this.shipFromLocationId).subscribe(data => {
                            this.pendingCount = data.pendingCount;
                            this.fulfilmentdataArr = data.fulfilmentList;
                            console.log(this.fulfilmentdataArr);
                            this.getTotalofAll(this.fulfilmentdataArr, 'get');
                            this.getTotalofAllExcludingTbXtb(this.fulfilmentdataArr, 'get');
                            this.fulfilmentdataLen = this.fulfilmentdataArr.length;
                            this.showLoader = false;
                            this.updateddate = data.updatedDate;
                        }, error => {
                            if (error.errorCode == '404.31.401') {
                                this.fulfilmentdataLen = 0;
                                this.pendingCount = 0;
                                this.getTotalofAll(this.fulfilmentdataArr, 'reset');
                                this.getTotalofAllExcludingTbXtb(this.fulfilmentdataArr, 'reset');
                                this.showLoader = false;
                            } else {
                                this._router.navigate(['/error404']);
                            }
                        });
                    }
                }
            });
        }
    }

    // Returns Ship from location id's
    getShipFromLocationIdList() {
        let shipToDeliverAt = 'shipToDeliverAt';
        let getAllShipFromLocationId = 'getAllShipFromLocationId';
        this._postsService.getShipFromLocationIdList(this.orgApiName, this.currDate, shipToDeliverAt, getAllShipFromLocationId).subscribe(data => {
            // this.shipFromLocationIDs = data.shipFromLocationIds.split(',');
            this.shipFromLocationIDs = data.shipfromLocationIdList;
            console.log(this.shipFromLocationIDs);
        })
    }

    // getconfirmedVsOrdered - Order raised/Order confirmed.
    getconfirmedVsOrdered(orderRaisedAmt: any, orderConfirmAmt: any) {
        if (orderRaisedAmt > 0 && (orderRaisedAmt != undefined || orderRaisedAmt != '')) {
            if (orderConfirmAmt > 0 && (orderConfirmAmt != undefined || orderConfirmAmt != '')) {
                return (((orderConfirmAmt) / orderRaisedAmt) * 100).toFixed(2);
            } else {
                return '-';
            }
        } else {
            return '-';
        }
    }

    // getconfirmedVsOrdered - Order raised/Order Ship
    getshippedVsOrdered(orderRaisedAmt: any, orderShipAmt: any) {
        if (orderRaisedAmt > 0 && (orderRaisedAmt != undefined || orderRaisedAmt != '')) {
            if (orderShipAmt > 0 && (orderShipAmt != undefined || orderShipAmt != '')) {
                return (((orderShipAmt) / orderRaisedAmt) * 100).toFixed(2);
            } else {
                return '0';
            }
        } else {
            return '0';
        }
    }

    getShippedLines(shipLines: any, orderLines: any) {
        if (orderLines > 0 && (orderLines != undefined || orderLines != '')) {
            if (shipLines > 0 && (shipLines != undefined || shipLines != '')) {
                return (((shipLines) / orderLines) * 100).toFixed(2);
            } else {
                return '0';
            }
        } else {
            return '0';
        }
    }

    redirect_page(page: any, orgType: any) {
        this._router.navigate(['dashboard', orgType, 'fulfilment', 'levelone']);
    }

    gotoLevelone(urlArr: any) {
        console.log((urlArr));

        var catastr = urlArr[4];
        console.log(catastr);
        let strCatArr = catastr.split("(");
        console.log(strCatArr.length);
        let strCat = strCatArr[0].trim();
        sessionStorage.setItem('catagoryNameStr', catastr);

        console.log(strCat);
        if (sessionStorage.getItem("catagoryName") !== null || sessionStorage.getItem("currDate") !== null || sessionStorage.getItem("uomName") !== null || sessionStorage.getItem("channel") != null) {
            sessionStorage.removeItem('catagoryName');
            sessionStorage.removeItem('uomName');
            //   sessionStorage.removeItem('channel');
            // sessionStorage.removeItem('currDate');
        }
        var uomstr = urlArr[5];
        let struomArr = uomstr.split("(");
        let struom = struomArr[0].trim();
        sessionStorage.setItem('uomNameStr', uomstr);
        console.log(struom);
        var channel = urlArr[7];
        sessionStorage.setItem('channel', channel);


        this._postsService.validateUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).subscribe((userData: any) => {
            if (!userData.isSessionActive) { // false
                // User session has expired.
                sessionStorage.clear();
                this._router.navigate(['']);
            } else {
                if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg || userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                    console.log("Session terminated");
                    this.msgJSON = {
                        "width314": true,
                        "title": "Session terminated",
                        "message": userData.sessionMessage,
                        "okButton": true,
                        "okButtonLabel": "OK",
                        "cancelButton": false,
                        "cancelButtonLabel": "",
                        "showPopup": true
                    };

                    let dt = this._commFuc.getUserSession();
                    this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
                        console.log(userData);
                    }, (error: any) => {
                        console.log(error);
                    });

                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                } else {
                    // this._router.navigate([urlArr[0], urlArr[1], urlArr[2], urlArr[3], strCat, struom, urlArr[6]]);
                    let url = this.homeUrl + urlArr[0] + '/' + urlArr[1] + '/' + urlArr[2] + '/' + urlArr[3] + '/' + strCat + '/' + struom + '/' + urlArr[6];
                    sessionStorage.setItem("pageState", "999");
                    console.log(url);
                    window.open(url);
                }
            }
        });
    }


    //printdata
    printdata() {
        var printHeaderNav = document.getElementById('printHeader').innerHTML;
        //var printContents = document.getElementById('exportable').innerHTML;
        //var printContents = "<table border=1>" + document.getElementById('nisfeederexport').innerHTML + "<table>";
        if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + '</body></html>');
            popupWin.document.close();
            setTimeout(function () { popupWin.close(); }, 1000);
        } else {
            var popup = window.open('', '_blank');
            popup.document.open();
            popup.document.write('<html><head><link rel="stylesheet" type="text/css" href="app/css/print.css" /></head><body onload="window.print()">' + printHeaderNav + '</body></html>');
            popup.document.close();

        }
    }

    exportData() {
        return this._commFuc.exportData('exportHeader', this.customerName + 'fulfilmentDashboard');
    }

    // navigate function
    redirect(page: any) {
        if (page == 'addproduct') {
            let url = this.homeUrl + 'catalogue/' + this.customerName + '/add';
            window.open(url);
        } else {
            this._router.navigate(page);
        }
    }
    // change organisation
    changeOrgType(page: any, orgType: any) {

        // reset field on change of organisation 
        this.catalogueno = '';
        this.catalogueList = [];
        this.catalogueListLength = 0;
        this.catalogueResult = true;
        this.catalogueType = '';
        this.catalogueCode = '';
        this.hitNext = false;
        this.start = 0;
        this.count = 0;
        this.toggleOrganisation();
        this._router.navigate(this._globalFunc.redirectToComponent('fulfilment', orgType));
        this.currDate = new Date();
        this.currDate = this._commFuc.dateFormator(this.currDate.toDateString(), 10);
        document.getElementById('fulfillmentdateinput').value = this.currDate;
        this.deliverydate = new Date();


    }

    ngOnDestroy() {
        // clean sub to avoid memory leak
    }

    // change organisation div show and hide
    toggleOrganisation() {
        if (this.hideOrganisation) {
            this.hideOrganisation = false;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-down');
            orgTypeListingIcon.classList.add('glyphicon-menu-up');
            document.getElementById('circle-box-border').style.display = 'none';
            let border = document.getElementById('search-box-border');
            //  document.getElementById('fulfillmentdateinput').value =  this.currDate;

        }
        else {
            this.hideOrganisation = true;
            let orgTypeListingIcon = document.getElementById('changeIcon');
            orgTypeListingIcon.classList.remove('glyphicon-menu-up');
            orgTypeListingIcon.classList.add('glyphicon-menu-down');
            document.getElementById('circle-box-border').style.display = 'block';
            let border = document.getElementById('search-box-border');
        }
    }

    getCustomerImage(custName: string) {
        return this._globalFunc.getCustomerImage(custName);
    }


    //getfullfilment data for amazon.

    // Returns data when date and location id are passed
    getNewFulFillmentDataforAmazon(value: any) {
        this.getUserSession(this.orgApiName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'));

        let shipFromLocationIdText = 'shipFromLocationId';
        let channeltext = '&channel='
        this.showLoader = true;
        if (this.showLoader == true) {
            this.fulfilmentdataLen = 999;
        }
        let shipToDeliverAt = 'shipToDeliverAt';
        console.log('form value : ' + value.fulfillmentdatefrom);
        if (value.fulfillmentdatefrom == undefined) {
            this.currDate = new Date();
            this.currDate = this._commFuc.dateFormator(this.currDate.toDateString(), 6);
        } else {
            if (sessionStorage.getItem('currDate') == null) {
                this.currDate = new Date();
                this.currDate = this._commFuc.dateFormator(this.currDate.toDateString(), 6);
                sessionStorage.setItem('currDate', this.currDate);
            }
            this.currDate = sessionStorage.getItem('currDate');
        }
        //this.currDate = this._commFuc.dateFormator(value.fulfillmentdateinput.toDateString(), 6);
        if (value.shipFromLocationIdType == undefined) {
            this.shipFromLocationId = "All";
            sessionStorage.setItem('shipFromLocationdescp', '');
        } else {
            console.log(JSON.stringify(value.shipFromLocationIdType));
            let shiplocArr = value.shipFromLocationIdType.split("-");
            console.log(shiplocArr[0])
            console.log(shiplocArr[1])
            if (this.shipFromLocationId != "All") {
                this.shipFromLocationId = shiplocArr[0];
            }

            sessionStorage.setItem('shipFromLocationId', shiplocArr[0]);
            this.shipFromLocationId = shiplocArr[0];

            if (shiplocArr[1] == undefined) {
                sessionStorage.setItem('shipFromLocationdescp', "");
            } else {
                sessionStorage.setItem('shipFromLocationdescp', shiplocArr[1]);
            }
        }

        if (this.shipFromLocationId == undefined || this.shipFromLocationId == "") {
            this.shipFromLocationId = "All";
            sessionStorage.setItem('shipFromLocationdescp', '');
            sessionStorage.setItem('shipFromLocationId', this.shipFromLocationId);
        }

        if (value.channelSubType == "undefined" || value.channelSubType == "" || value.channelSubType == undefined) {
            this.channel = "All";
            this.subtype = "All";
            sessionStorage.setItem('channel', '');
            sessionStorage.setItem('channel', this.channel);
        }
        else {
            this.channel = value.channelSubType
            if (this.channel == 'MP8QW') {
                this.subtype = 'core';
            }
            if (this.channel == 'PRXKD') {
                this.subtype = 'Fresh';
            }
            if (this.channel == 'THSV2') {
                this.subtype = 'Prime Now';
            }
            if (this.channel == 'THSV3') {
                this.subtype = 'Pantry';
            }
        }

        this._postsService.validateUserSession(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId')).subscribe((userData: any) => {
            if (!userData.isSessionActive) { // false
                // User session has expired.

                if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsgForInactiveUser) {
                    console.log("Session terminated");
                    this.msgJSON = {
                        "width314": true,
                        "title": "Session terminated",
                        "message": userData.sessionMessage,
                        "okButton": true,
                        "okButtonLabel": "OK",
                        "cancelButton": false,
                        "cancelButtonLabel": "",
                        "showPopup": true
                    };

                    let dt = this._commFuc.getUserSession();
                    this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
                        console.log(userData);
                    }, (error: any) => {
                        console.log(error);
                    });

                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                }
                else {
                    sessionStorage.clear();
                    this._router.navigate(['']);
                }
            } else {
                if (userData.sessionMessage == this.objConstant.userSessionTerminatedMsg) {
                    console.log("Session terminated");
                    this.msgJSON = {
                        "width314": true,
                        "title": "Session terminated",
                        "message": userData.sessionMessage,
                        "okButton": true,
                        "okButtonLabel": "OK",
                        "cancelButton": false,
                        "cancelButtonLabel": "",
                        "showPopup": true
                    };

                    let dt = this._commFuc.getUserSession();
                    this._postsService.authLoginTracker(this.customerName, sessionStorage.getItem('loggedInUserGroupId'), sessionStorage.getItem('userId'), '', dt).subscribe((userData: any) => {
                        console.log(userData);
                    }, (error: any) => {
                        console.log(error);
                    });

                    let sessionModal = document.getElementById('popup_SessionExpired');
                    sessionModal.classList.remove('in');
                    sessionModal.classList.add('out');
                    document.getElementById('popup_SessionExpired').style.display = 'block';
                } else {
                    this._postsService.getNewFulFillmentDataforAmazon(this.orgApiName, this.currDate, shipToDeliverAt, shipFromLocationIdText, this.shipFromLocationId, channeltext, this.channel).subscribe(data => {
                        this.pendingCount = data.pendingCount;
                        this.fulfilmentdataArr = data.fulfilmentList;
                        console.log(this.fulfilmentdataArr);
                        this.getTotalofAll(this.fulfilmentdataArr, 'get');
                        this.getTotalofAllExcludingTbXtb(this.fulfilmentdataArr, 'get');
                        this.fulfilmentdataLen = this.fulfilmentdataArr.length;
                        this.showLoader = false;
                        this.updateddate = data.updatedDate;
                    }, error => {
                        if (error.errorCode == '404.31.401') {
                            this.fulfilmentdataLen = 0;
                            this.pendingCount = 0;
                            this.getTotalofAll(this.fulfilmentdataArr, 'reset');
                            this.getTotalofAllExcludingTbXtb(this.fulfilmentdataArr, 'reset');
                            this.showLoader = false;
                        } else {
                            this._router.navigate(['/error404']);
                        }
                    });
                }
            }
        });
    }
    getUserSession(customerName: any, loggedInUserGroup: any, loggedInUserId: any) {
        console.log("123123123");
        this._postsService.validateUserSession(customerName, loggedInUserGroup, loggedInUserId).subscribe((userData: any) => {
            if (!userData.isSessionActive) { // false
                // User session has expired.
                sessionStorage.clear();
                this._router.navigate(['']);
            } else {
                /*  if (userData.sessionMessage != "Session is Active") {
                     sessionStorage.setItem("permissionChangesTxt",userData.sessionMessage);
                     let sessionModal = document.getElementById('permissionChanges');
                     sessionModal.classList.remove('out');
                     sessionModal.classList.add('in');
                     document.getElementById('permissionChanges').style.display = 'block';
         
                     setTimeout(() => {   
                         sessionStorage.clear();
                         this._router.navigate(['']);            
                     }, this.objConstant.WARNING_TIMEOUT);
                 } else */ /* { */
                this.organisationList = userData.userPermissionJson.userRoleDetails.organizations;
                let changeOrgAvailable: any = [];
                let orgImgUrl = "";
                let orgApiNameTemp = "";
                this.organisationList.forEach(function (item: any) {
                    if (item.customerName !== customerName) {
                        changeOrgAvailable.push(item);
                    } else {
                        orgImgUrl = item.customerName + ".png";
                        orgApiNameTemp = item.customerName;
                    }
                });
                this.organisationListChange = changeOrgAvailable;
                this.orgimgUrl = orgImgUrl;
                this.orgApiName = orgApiNameTemp;
                // this.disabledCusChange = false;
                if (this.organisationList.length == 1) {
                    this.disabledCusChange = true;
                }

                console.log("this.organisationList234");
                console.log(this.organisationList);
                let that = this;
                let filfilment = true;
                let dashboardMenuPermission = false;
                this.organisationList.forEach(function (organisation: any) {

                    if (organisation.customerName.toUpperCase() == that.customerName.toUpperCase()) {
                        console.log("organisation : " + that.customerName);
                        console.log(organisation);
                        organisation.permissionRoleList.forEach(function (permRoleList: any) {
                            console.log("11");
                            console.log(permRoleList);
                            permRoleList.permissionDetailsList.forEach(function (permRoleDtlList: any) {
                                console.log("22");
                                if (permRoleDtlList.permissionGroup == 'dashboards') {
                                    permRoleDtlList.permissionNameList.forEach(function (permList: any) {
                                        if (permList.permissionName == 'dashboards') {// && permList.permissionStatus == 'active') {
                                            dashboardMenuPermission = true;
                                        }

                                        switch (permList.permissionName) {
                                            case "ordersFulfilmentDashboard":
                                                console.log("55");
                                                filfilment = false;
                                                //alert(filfilment);
                                                break;
                                        }
                                    });
                                }
                            });
                        });
                    }
                });
                that = null;
                if (dashboardMenuPermission && !filfilment) {
                    this._router.navigate(["dashboard", customerName, "fulfilment"]);
                } else {
                    this._router.navigate(["dashboard", customerName]);
                }

                this.disabledfilfilment = filfilment;
                /*   } */

            }
            //alert(this.disabledfilfilment);
        });
    }
}
