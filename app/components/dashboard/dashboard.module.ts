import { NgModule } from '@angular/core';
import { DashboardRoutes } from './dashboard.routes';
import { DashboardComponent } from './dashboard.component';

import { CommonModule } from '@angular/common';
import { GlobalComponent } from '../global/global.component';
import { Services } from '../../services/app.services';
import { FormsModule } from '@angular/forms';
import { NavigationModule } from '../navigationFulfilment/navigation.module';
import { commonfunction } from '../../services/commonfunction.services';

@NgModule({
  imports: [
    DashboardRoutes,
    CommonModule,
    FormsModule,
    NavigationModule
  ],
  exports: [],
  declarations: [DashboardComponent],
  providers: [Services, GlobalComponent,commonfunction]
})
export class DashboardModule { }
