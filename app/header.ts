import { Component, AfterViewInit } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Router } from '@angular/router';
import { GlobalComponent } from '../app/components/global/global.component';
import { Constant } from './components/constants/constant';
import { Services } from './services/app.services';
import { commonfunction } from './services/commonfunction.services';
import { DialogService } from './services/dialog.service';
@Component({
    selector: 'fountain-header',
    templateUrl: 'header.html',
    providers: [GlobalComponent, Services, commonfunction]
})
export class HeaderComponent implements AfterViewInit {


    permissionChangesTxt: string;
    pageState: string;
    contactUsUrl: string;
    objConstant = new Constant();
    whoImgUrl: any;
    errorMsg: string;
    showLoader: boolean;
    logout: boolean;
    currentUrl: any;
    globalComponent = new GlobalComponent();
    environment: any;
    environmentToDisplay: any;
    siteMaintanceFlg: boolean;
    siteMSg: string;
    businessMessageDetailsMsg: string;
    featureDetailsMsg: string;
    deploymentDetailsMsg: string;
    deploymentDetailsMsglength: any;
    businessMessageDetailsMsglength: any;
    featureDetailsMsglength: any;
    envFlag: boolean = false;


    constructor(private _router: Router, private _postsService: Services, public _cfunction: commonfunction, private dialogService: DialogService) {
        _router.events.subscribe((url: any) => this.currentUrl = url.url);
        this.whoImgUrl = this.objConstant.WHOIMG_URL;
        this.contactUsUrl = this.objConstant.CONTACT_US;
        this.pageState = sessionStorage.getItem("pageState");
        this.permissionChangesTxt = sessionStorage.getItem("permissionChangesTxt");
    }

    ngAfterViewInit() {
        // Hiding the hamburger menu
        // document.getElementById('navbar').style.display = "none";
    }

    // function worked on load
    ngOnInit() {
        this.pageState = sessionStorage.getItem("pageState");
        console.log('inside header load');
        this.logout = false;
        this.showLoader = false;

        // this.environment=  this.objConstant.ENVIRNMNT;
        if (this.objConstant.ENVIRNMNT != "PROD") {
            this.envFlag = true;
            this.environmentToDisplay = this.objConstant.ENVIRNMNT;
        } else {
            this.envFlag = false;
        }
        this.getsitemaintanceStatus();
    }

    canDeactivate(): Promise<boolean> | boolean {
        if(sessionStorage.getItem('unsavedChanges')!=='true'){
            return true;
        }else{
            return this.dialogService.confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
        }
    }

    getsitemaintanceStatus() {

        this._postsService.siteMaintanceCall('1', 'all').subscribe(userData => {
            console.log(userData);
            console.log(userData.maintenanceDetails[0].message);
            console.log(userData.maintenanceDetails[0].start);
            console.log(userData.maintenanceDetails[0].status);

            console.log(userData.deploymentDetails[0].status);
            if (userData.deploymentDetails[0].status != 'NO' && userData.deploymentDetails != undefined ) {
                this.deploymentDetailsMsg = userData.deploymentDetails[0].message ;
                this.deploymentDetailsMsglength = this.deploymentDetailsMsg.length;
                if (sessionStorage.getItem('deploymentDetailsMsglength') == undefined || sessionStorage.getItem('deploymentDetailsMsglength') == null) {
                    sessionStorage.setItem('deploymentDetailsMsglength', this.deploymentDetailsMsglength);
                    console.log(this.deploymentDetailsMsglength +"this.deploymentDetailsMsglength");
                } else {
                    this.deploymentDetailsMsglength = sessionStorage.getItem('deploymentDetailsMsglength');
                    console.log(this.deploymentDetailsMsglength +"this.deploymentDetailsMsglength");
                }

            } else {
                this.deploymentDetailsMsg = '';
            }


            console.log(userData.featureDetails);
            if (userData.featureDetails[0].status  != 'NO' && userData.featureDetails !=  undefined )  {
                this.featureDetailsMsg = userData.featureDetails[0].message;
                this.featureDetailsMsglength = this.featureDetailsMsg.length;
                if (sessionStorage.getItem('featureDetailsMsglength') == undefined || sessionStorage.getItem('featureDetailsMsglength') == null) { 
                     sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
                }
                else {
                    this.deploymentDetailsMsglength = sessionStorage.getItem('featureDetailsMsglength');
                }
            } else {
                this.featureDetailsMsg = '';
            }


            console.log(userData.businessMessageDetails);
            if (userData.businessMessageDetails[0].status != 'NO' && userData.businessMessageDetails != undefined) {
                this.businessMessageDetailsMsg = userData.businessMessageDetails[0].message ;
                this.businessMessageDetailsMsglength = this.businessMessageDetailsMsg.length;
                if (sessionStorage.getItem('businessMessageDetailsMsglength') == undefined || sessionStorage.getItem('businessMessageDetailsMsglength') == null) {                    sessionStorage.setItem('featureDetailsMsglength', this.featureDetailsMsglength);
                    sessionStorage.setItem('businessMessageDetailsMsglength', this.businessMessageDetailsMsglength);
                }
                else {
                   this.businessMessageDetailsMsglength = sessionStorage.getItem('businessMessageDetailsMsglength');
                }
            } else {
                this.businessMessageDetailsMsg = '';
            }


            this.showLoader = false;
        }, error => {
            console.log(error);
            if (error.httpResponseCode == '555' || error.httpResponseCode == '503' || error.httpResponseCode == '502') {
                this.siteMaintanceFlg = true;
                this.siteMSg = "Site is under maintenance";
            } else if (error.errorCode == '404.26.408') {
                this.errorMsg = "User is Inactive";
            }
            else {
                this.siteMaintanceFlg = false;
            }
            this.showLoader = false;
        });
    }
    // navigation function
    redirect(page: any) {
        this._router.navigate([page]);
    }

    navigateMobile(page: any) {
        let orgName = sessionStorage.getItem('orgType');
        this._router.navigate([page, orgName]);
    }
    /*menu*/
    menuClick() {
        if (document.getElementById('nav-toggle').classList.contains('active') === false) {
            document.getElementById('nav-toggle').classList.add('active');
        } else {
            document.getElementById('nav-toggle').classList.remove('active');
        }
    }

    menuClose() {
        if (document.getElementById('nav-toggle').classList.contains('active') === false) {

            document.getElementById('navbar').classList.add('in');
            document.getElementById('nav-toggle').classList.add('active');
        } else {

            document.getElementById('navbar').classList.remove('in');
            document.getElementById('nav-toggle').classList.remove('active');
        }
    }
    // toggle menus
    toggleOrder() {

        if (document.getElementById('orderArrow').classList.contains('glyphicon-menu-down') === true) {
            let sublinks = document.getElementById('orderArrow');
            sublinks.classList.remove('glyphicon-menu-down');
            sublinks.classList.add('glyphicon-menu-up');
            document.getElementById('orderRaiseMob').style.display = 'block';
        } else {

            let sublinks = document.getElementById('orderArrow');
            sublinks.classList.remove('glyphicon-menu-up');
            sublinks.classList.add('glyphicon-menu-down');
            document.getElementById('orderRaiseMob').style.display = 'none';
        }
    }

    // reset session to continue access
    resetSession() {
        if (sessionStorage.getItem('pageState') !== '0') {
            sessionStorage.setItem('timeoutState', '0');
            let sessionModal = document.getElementById('timeoutModal');
            sessionModal.classList.remove('in');
            sessionModal.classList.add('out');
            document.getElementById('timeoutModal').style.display = 'none';
        }
    }

    // to logout user
    timeOutSession() {
        sessionStorage.setItem('timeoutState', '0');
        let sessionModal = document.getElementById('timeoutModal');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('timeoutModal').style.display = 'none';
        this.logoutUser();
    }

    // toggle menus
    toggleCatalogue() {
        if (document.getElementById('catalogueArrow').classList.contains('glyphicon-menu-down') === true) {
            let sublinks = document.getElementById('catalogueArrow');
            sublinks.classList.remove('glyphicon-menu-down');
            sublinks.classList.add('glyphicon-menu-up');
            document.getElementById('catalogueRaiseMob').style.display = 'block';
        } else {

            let sublinks = document.getElementById('catalogueArrow');
            sublinks.classList.remove('glyphicon-menu-up');
            sublinks.classList.add('glyphicon-menu-down');
            document.getElementById('catalogueRaiseMob').style.display = 'none';
        }
    }

    keepMeonSamePage() {
        let sessionModal = document.getElementById('unsaveModal');
        sessionModal.classList.remove('in');
        sessionModal.classList.add('out');
        document.getElementById('unsaveModal').style.display = 'none';
    }

    navigatTopage() {
        this.keepMeonSamePage();
        sessionStorage.setItem("unsaveChagne", "false");
        let urlAr = JSON.parse(sessionStorage.getItem("urlArr"));
        let pageName = sessionStorage.getItem('pageName');
        let currentCustomer = sessionStorage.getItem('orgType');
        if (pageName == '') {
            this._router.navigate(['/']);
        } else if (pageName != 'customerselection') {
            this._router.navigate([pageName, currentCustomer]);
        } else {
            this._router.navigate([pageName]);
        }
    }
    // clear session of user

    checkUnsaved(){
        if(sessionStorage.getItem('unsavedChanges') == 'true'){
            sessionStorage.setItem('logout','true');
            this.canDeactivate();
        }else{
            this.logoutUser();
        }
    }
    logoutUser() {
        document.getElementById('login').style.display = 'none';
        let orgType = 'default';
        let groupId = '@all';
        let userId = sessionStorage.getItem("userId");
        let dt = this._cfunction.getUserSession();
        this._postsService.authLoginTracker(orgType, groupId, userId, '', dt).subscribe((userData: any) => {
            console.log(userData);
        }, (error: any) => {
            console.log(error);
        });
        sessionStorage.clear();
        this._router.navigate(['']);
    }

    changeModal() {
        sessionStorage.setItem('pageName', '');
        if (sessionStorage.getItem("unsaveChagne") == 'true') {
            let sessionModal = document.getElementById('unsaveModal');
            sessionModal.classList.remove('out');
            sessionModal.classList.add('in');
            document.getElementById('unsaveModal').style.display = 'block';

            setTimeout(() => {
            }, this.objConstant.WARNING_TIMEOUT);
        } else {
            this._router.navigate(['/']);
        }
    }
}
