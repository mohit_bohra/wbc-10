import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CanDeactivateGuard } from './services/can-deactivate-guard.service';
import { DialogService } from './services/dialog.service';
import { SiteGuardService } from './services/siteGuard.service';

@NgModule({
  imports: [RouterModule.forRoot([
    { path: '', component: HomeComponent },
    { path: 'customerselection', loadChildren: './components/customerselection/dashboard.module#DashboardModule',canActivate: [SiteGuardService] },
    { path: 'resetpassword', loadChildren: './components/usermanagement/resetpassword.module#ResetpasswordModule' },
    { path: 'orders/:orgType', loadChildren: './components/orders/orders.module#OrdersModule',canActivate: [SiteGuardService] },
    { path: 'catalogue/:orgType', loadChildren: './components/catalogue/catalogue.module#CatalogueModule',canActivate: [SiteGuardService] },
    { path: 'store/:orgType', loadChildren: './components/catalogue/store.module#StoreModule',canActivate: [SiteGuardService] },
    //{ path: 'addStore/:orgType', loadChildren: './components/catalogue/addstore.module#StoreModule' },

    ///// not in used { path: 'rbac/:orgType', loadChildren: './components/rbac/rbac.module#GroupsModule' },    
    { path: 'cataloguemanagement/:orgType', loadChildren: './components/cataloguemanagement/cataloguemanagement.module#CatalogueManagementModule',canActivate: [SiteGuardService] },
    { path: 'fulrangeexport/:orgType', loadChildren: './components/cataloguemanagement/fulrangeexport.module#fulrangeExportModule' },
    //{ path: 'fullrangeupload/:orgType', loadChildren: './components/fullrangeupload/fullrangeupload.module#FullRangeUploadModule' },
    { path: 'fullrangeexport/:orgType', loadChildren: './components/fullrangeexport/fullrangeexport.module#FullRangeExportModule' },
    { path: 'reports/:orgType', loadChildren: './components/reports/reports.module#ReportsModule',canActivate: [SiteGuardService] },
    { path: 'error404', loadChildren: './components/exceptions/exception.module#ExceptionModule' },
    { path: 'cookies', loadChildren: './components/staticpages/cookiesconditions/cookies.module#CookiesModule' },
    { path: 'tnc', loadChildren: './components/staticpages/termsconditions/terms.module#TermsModule' },
    { path: 'privacypolicy', loadChildren: './components/staticpages/privacypolicy/privacypolicy.module#PrivacyModule' },
    { path: 'dashboard/:orgType', loadChildren: './components/dashboard/dashboard.module#DashboardModule',canActivate: [SiteGuardService] },
    { path: 'groups/:orgType', loadChildren: './components/groups/groups.module#GroupsModule',canActivate: [SiteGuardService] }
   // { path: 'error/:orgType', loadChildren : './components/exceptions/exception.module#ExceptionModule'}
    
  ])],
  exports: [RouterModule],
  providers: [
    CanDeactivateGuard,
    DialogService,
    SiteGuardService
  ]
})
export class AppRoutingModule { }
