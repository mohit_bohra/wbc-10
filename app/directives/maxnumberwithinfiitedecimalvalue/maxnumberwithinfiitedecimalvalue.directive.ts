
import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[MaxNumberWithInfiiteDecimalValue]'
})
export class MaxNumberWithInfiiteDecimalValue {

  constructor(private el: ElementRef) { }

  @Input() MaxNumberWithInfiiteDecimalValue: number;

  @HostListener('keydown', ['$event']) onKeyDown(event: any) {
    let e = <KeyboardEvent> event;
    //console.log(event);
    //if (this.MaxNumberWithInfiiteDecimalValue) {
      if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
        // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
          // let it happen, don't do anything
          if(e.key == '.'){
              let curValue1 = event.target.value.toString() + e.key;
              if(curValue1.split(".").length > 2){
                  e.preventDefault();
              }
              //console.log("Dot occurance : "+curValue1.split(".").length);
          }
          //console.log("don't do anything");
          return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
            //console.log("Invalid Input");
        } else {
            let curValue = event.target.value.toString() + e.key;
            console.log("1 Dot occurance : "+curValue.split(".").length);
            //if(curValue.split(".").length > 1)
            
            
            //console.log("calculated Value : " + parseFloat(curValue));
            if(this.MaxNumberWithInfiiteDecimalValue < parseFloat(curValue)){
                e.preventDefault();
                //console.log("Invalid Input 1 ");
            } else {
                var valArr = curValue.split('.');
                //console.log("Input length : " + valArr.length);
                //console.log(valArr);
                if (valArr.length > 2) {
                    //console.log("123");
                    e.preventDefault();
                } else {
                    //console.log("valid Input : " + curValue + " : " + this.MaxNumberWithInfiiteDecimalValue);
                }
            }
            
        }
     // }
  }
}


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/