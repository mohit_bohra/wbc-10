import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaxNumberWithInfiiteDecimalValue } from './maxnumberwithinfiitedecimalvalue.directive';


@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [MaxNumberWithInfiiteDecimalValue],
  declarations: [MaxNumberWithInfiiteDecimalValue],
  providers: []
})
export class MaxNumberWithInfiiteDecimalValueModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MaxNumberWithInfiiteDecimalValueModule
    }
  }
}
