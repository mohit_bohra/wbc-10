import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OnlyNumber } from './onlynumber.directive';


@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [OnlyNumber],
  declarations: [OnlyNumber],
  providers: []
})
export class OnlyNumberModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: OnlyNumberModule
    }
  }
}
