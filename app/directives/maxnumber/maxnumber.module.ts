import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaxNumber } from './maxnumber.directive';


@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [MaxNumber],
  declarations: [MaxNumber],
  providers: []
})
export class MaxNumberModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MaxNumberModule
    }
  }
}
