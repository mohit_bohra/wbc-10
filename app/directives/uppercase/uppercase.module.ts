import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UppercaseDirective } from './uppercase.directive';


@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [UppercaseDirective],
  declarations: [UppercaseDirective],
  providers: []
})
export class UppercaseModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: UppercaseModule
    }
  }
}
