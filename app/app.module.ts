import { NgModule } from '@angular/core';
import { HashLocationStrategy, LocationStrategy, CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { RouterModule } from '@angular/router';

import { HttpModule, JsonpModule } from '@angular/http';
// Import Component
import { AppComponent } from './app.component';
//import { MainComponent }        from './main/main.component';
import { HeaderComponent } from './header';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './footer';


// Import Service
import { APP_CONFIG, OPAQUE_TOKEN } from './app.config';

// Import Module
import { AppRoutingModule } from './app-routing.module';
import { Ng2BootstrapModule } from './components/datepicker/ng2-bootstrap';

import { PagerService }  from './services/pager.service';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    AppRoutingModule,
    Ng2BootstrapModule.forRoot()
    // ....
  ],
  declarations: [
    AppComponent,
    // MainComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent


  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy }, { provide: OPAQUE_TOKEN, useValue: APP_CONFIG }, // provide for global variables
    PagerService
  ]
})
export class AppModule { }