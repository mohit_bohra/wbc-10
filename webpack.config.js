/**
 * webpack.config.js
 */

switch (process.env.NODE_ENV) {
  case 'prod':
  case 'production':
    module.exports = require('./config/webpack.prod');
    break;
  case 'dev':
  case 'development':
    break;
  default:
    module.exports = require('./config/webpack.dev');
}